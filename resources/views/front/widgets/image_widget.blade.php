@if(!empty($settings['image']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<div class="grid">
			<figure class="{{ !empty($settings['hoverstyle'])? 'effect-'.$settings['hoverstyle']:'' }}" style="
			{{ !empty($settings['backlightcolor'])?'background:'.$settings['backlightcolor'].';':'' }}
			{{ !empty($settings['border'])?'border:'.$settings['border'].'px solid;':'' }}
			{{ !empty($settings['textcolor'])?'color:'.$settings['textcolor'].';':'' }}
			{{ !empty($settings['bordercolor'])?'border-color:'.$settings['bordercolor'].';':'' }}
			{{ !empty($settings['borderradius'])?'border-radius:'.$settings['borderradius'].'px;overflow:hidden;':'' }}">
				<img class="img" src="{{ $settings['image'] }}" alt="">
				<figcaption class="{!! !empty($settings['showtitle']) && $settings['showtitle']=='on'? 'showtitle':'' !!} {!! !empty($settings['showdescription']) && $settings['showdescription']=='on'? 'showdescription':'' !!} ">
					@if(!empty($settings['title']))
						<h2 class="{!! !empty($settings['hidetitle']) && $settings['hidetitle']=='on'? 'hidden':'' !!}">{!! $settings['title'] !!}</h2>
					@endif
					@if(!empty($settings['description']))
						<div class="{!! !empty($settings['hidedescription']) && $settings['hidedescription']=='on'? 'hidden':'' !!}">{!! $settings['description'] !!}</div>
					@endif

				</figcaption>
				@if(!empty($settings['link']))
					<a href="{{ $settings['link'] }}" target="{{ $settings['linktarget'] }}">&nbsp;</a>
				@endif
			</figure>
		</div>
	</div>
	@if(!empty($settings['overlaycolor']))
	<style>
	.{{ $widget->type }}_{{ $widget->id }} .grid figure:before {
		content: "";
		background: {{ $settings['overlaycolor'] }};
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		z-index: 9;
		-webkit-transition: all 0.3s ease-in-out;
		-moz-transition: all 0.3s ease-in-out;
		-o-transition: all 0.3s ease-in-out;
		transition: all 0.3s ease-in-out;
		@if(!empty($settings['overlayonhover']))
			opacity:0;
		@endif
	}
	@if(!empty($settings['overlayonhover']))
		.{{ $widget->type }}_{{ $widget->id }} .grid figure:hover:before{
			opacity:1;
		}
	@else
		.{{ $widget->type }}_{{ $widget->id }} .grid figure:hover:before{
			opacity:0;
		}
	@endif

	</style>
	@endif
@endif
