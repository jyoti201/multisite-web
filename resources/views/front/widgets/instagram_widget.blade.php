@if(!empty($settings['clientId']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<div id="{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}" class="instagram-feed">
		</div>
	</div>
	<script type="text/javascript">
		var feed = new Instafeed({
			get: '{!! $settings['get'] !!}',
			@if($settings['get']=='tagged')
				tagName: '{!! $settings['tagName'] !!}',
			@elseif($settings['get']=='location')
				locationId: '{!! $settings['locationId'] !!}',
			@elseif($settings['get']=='user')
				userId: '{!! $settings['userId'] !!}',
			@endif
			target: '{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}',
			clientId: '{!! $settings['clientId'] !!}',
			accessToken: '{!! $settings['accessToken'] !!}',
			sortBy: '{!! $settings['sortBy'] !!}',
			links: '{!! $settings['links'] !!}',
			limit: '{!! $settings['limit'] !!}',
			resolution: '{!! $settings['resolution'] !!}',
			@if($settings['template']!='')
				template: '{!! $settings['template'] !!}'
			@endif
		});
		feed.run();
	</script>
@endif
