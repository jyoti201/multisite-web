@if(!empty($settings['type']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<!-- Hubspot -->
		@if(!empty($settings['notification_type']) && $settings['notification_type']=='hubspot')
		<div class="hidden">
			<iframe name="form_{{ $widget->id }}_hubspot-iframe" id="form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_hubspot-iframe"></iframe>
		</div>
		@endif
		<!-- Hubspot -->
		<div class="alert alert-success collapse mb-10" id="form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success"></div>

		<form class="row flex-wrap no-gutters form_{{ $widget->id }}" id="form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}" method="post"

		@if(!empty($settings['notification_type']) && $settings['notification_type']=='hubspot')
			{!! !empty($settings['submission_type']) && $settings['submission_type']=='thankyou'?'target="form_'.$widget->id.'_hubspot-iframe"':'' !!}

		@endif

		>
				@foreach($settings['type'] as $key=>$field)
				  <div class="form-group col-12 {{ !empty($settings['customcss'][$key])?$settings['customcss'][$key]:'' }}
				  @if($settings['type'][$key]=='checkboxes')
					checkboxgroup
				  @endif
				  @if($settings['type'][$key]=='radio')
					radiogroup
				  @endif
				  ">
					@if(!empty($settings['type'][$key]) && $settings['labelvisibility'][$key]!='hidden')
					<label for="{{ !empty($settings['name'][$key])?$settings['name'][$key]:'' }}">{{ !empty($settings['label'][$key])?$settings['label'][$key]:'' }}</label>
					@endif
					@if(!empty($settings['descriptionplacement'][$key]) && $settings['descriptionplacement'][$key] == 'above')
					<div class="help">{{ !empty($settings['description'][$key])?$settings['description'][$key]:'' }}</div>
					@endif
					{!! getformInput(!empty($settings['type'][$key])?$settings['type'][$key]:'', !empty($settings['name'][$key])?$settings['name'][$key]:'', !empty($settings['placeholder'][$key])?$settings['placeholder'][$key]:'', !empty($settings['value'][$key])?$settings['value'][$key]:'', true) !!}
					@if(!empty($settings['descriptionplacement'][$key]) && $settings['descriptionplacement'][$key] == 'below')
					<div class="help">{{ !empty($settings['description'][$key])?$settings['description'][$key]:'' }}</div>
					@endif
				  </div>
				@endforeach


				<!-- Hubspot -->
				@if(!empty($settings['notification_type']) && $settings['notification_type']=='hubspot')
				<div class="form-group collapse">
					<input type="text" class="form-control" name="email" value="{{ !empty($settings['recipients'])?$settings['recipients']:'' }}">
				</div>
				@endif

				<!-- Hubspot -->
				<input type="hidden" name="logo" value="{{ cleanHTML(getSetting('logo','header')) }}">
				<input type="hidden" name="url" value="{{ getSetting('prod_url','host') }}">
				<input type="hidden" name="message_body" value="{{ !empty($settings['message_body'])?$settings['message_body']:'No Message' }}">
				<div class="form-group col {{ !empty($settings['buttonalign'])?$settings['buttonalign']:'' }}">
				<button type="submit" class="btn btn-default {{ !empty($settings['buttonclass'])?$settings['buttonclass']:'' }}">{{ !empty($settings['buttontext'])?$settings['buttontext']:'Submit' }}</button>
				</div>
		</form>
	</div>
<style>
.form_{{ $widget->id }} .form-control{
	@if(!empty($settings['inputbgcolor']))
		background-color: {{ $settings['inputbgcolor'] }};
	@endif
	@if(!empty($settings['inputtxcolor']))
		color: {{ $settings['inputtxcolor'] }};
	@endif
	@if(!empty($settings['inputborderwidth-left']))
		border-left-width: {{ $settings['inputborderwidth-left'] }}px;
	@else
		border-left-width: 0;
	@endif
	@if(!empty($settings['inputborderwidth-right']))
		border-right-width: {{ $settings['inputborderwidth-right'] }}px;
	@else
		border-right-width: 0;
	@endif
	@if(!empty($settings['inputborderwidth-top']))
		border-top-width: {{ $settings['inputborderwidth-top'] }}px;
	@else
		border-top-width: 0;
	@endif
	@if(!empty($settings['inputborderwidth-bottom']))
		border-bottom-width: {{ $settings['inputborderwidth-bottom'] }}px;
	@else
		border-bottom-width: 0;
	@endif
	@if(!empty($settings['inputborderradius-left-top']))
		border-top-left-radius: {{ $settings['inputborderradius-left-top'] }}px;
	@else
		border-top-left-radius: 0;
	@endif
	@if(!empty($settings['inputborderradius-left-bottom']))
		border-bottom-left-radius: {{ $settings['inputborderradius-left-bottom'] }}px;
	@else
		border-bottom-left-radius: 0;
	@endif
	@if(!empty($settings['inputborderradius-right-top']))
		border-top-right-radius: {{ $settings['inputborderradius-right-top'] }}px;
	@else
		border-top-right-radius: 0;
	@endif
	@if(!empty($settings['inputborderradius-right-bottom']))
		border-bottom-right-radius: {{ $settings['inputborderradius-right-bottom'] }}px;
	@else
		border-bottom-right-radius: 0;
	@endif
	@if(!empty($settings['inputbordercolor-left']))
		border-left-color: {{ $settings['inputbordercolor-left'] }};
	@endif
	@if(!empty($settings['inputbordercolor-right']))
		border-right-color: {{ $settings['inputbordercolor-right'] }};
	@endif
	@if(!empty($settings['inputbordercolor-top']))
		border-top-color: {{ $settings['inputbordercolor-top'] }};
	@endif
	@if(!empty($settings['inputbordercolor-bottom']))
		border-bottom-color: {{ $settings['inputbordercolor-bottom'] }};
	@endif
	@if(!empty($settings['inputpadding-left']))
		padding-left: {{ $settings['inputpadding-left'] }}px;
	@endif
	@if(!empty($settings['inputpadding-right']))
		padding-right: {{ $settings['inputpadding-right'] }}px;
	@endif
	@if(!empty($settings['inputpadding-top']))
		padding-top: {{ $settings['inputpadding-top'] }}px;
	@endif
	@if(!empty($settings['inputpadding-bottom']))
		padding-bottom: {{ $settings['inputpadding-bottom'] }}px;
	@endif
	@if(!empty($settings['inputmargin-left']))
		margin-left: {{ $settings['inputmargin-left'] }}px;
	@endif
	@if(!empty($settings['inputmargin-right']))
		margin-right: {{ $settings['inputmargin-right'] }}px;
	@endif
	@if(!empty($settings['inputmargin-top']))
		margin-top: {{ $settings['inputmargin-top'] }}px;
	@endif
	@if(!empty($settings['inputmargin-bottom']))
		margin-bottom: {{ $settings['inputmargin-bottom'] }}px;
	@endif
	@if(!empty($settings['inputfont']))
		font-size: {{ $settings['inputfont'] }}px;
	@endif
	@if(!empty($settings['inputborderstyle']))
		border-style: {{ $settings['inputborderstyle'] }};
	@endif
	@if(!empty($settings['inputfont_family']))
		font-family: {{ $settings['inputfont_family'] }};
	@endif
	height: auto;
}
.form_{{ $widget->id }} button[type="submit"]{
	@if(!empty($settings['buttonbgcolor']))
		background-color: {{ $settings['buttonbgcolor'] }};
	@endif
	@if(!empty($settings['buttontxcolor']))
		color: {{ $settings['buttontxcolor'] }};
	@endif
	@if(!empty($settings['buttonborderwidth-left']))
		border-left-width: {{ $settings['buttonborderwidth-left'] }}px;
	@else
		border-left-width: 0;
	@endif
	@if(!empty($settings['buttonborderwidth-right']))
		border-right-width: {{ $settings['buttonborderwidth-right'] }}px;
	@else
		border-right-width: 0;
	@endif
	@if(!empty($settings['buttonborderwidth-top']))
		border-top-width: {{ $settings['buttonborderwidth-top'] }}px;
	@else
		border-top-width: 0;
	@endif
	@if(!empty($settings['buttonborderwidth-bottom']))
		border-bottom-width: {{ $settings['buttonborderwidth-bottom'] }}px;
	@else
		border-bottom-width: 0;
	@endif
	@if(!empty($settings['buttonborderradius-left-top']))
		border-top-left-radius: {{ $settings['buttonborderradius-left-top'] }}px;
	@else
		border-top-left-radius: 0;
	@endif
	@if(!empty($settings['buttonborderradius-left-bottom']))
		border-bottom-left-radius: {{ $settings['buttonborderradius-left-bottom'] }}px;
	@else
		border-bottom-left-radius: 0;
	@endif
	@if(!empty($settings['buttonborderradius-right-top']))
		border-top-right-radius: {{ $settings['buttonborderradius-right-top'] }}px;
	@else
		border-top-right-radius: 0;
	@endif
	@if(!empty($settings['buttonborderradius-right-bottom']))
		border-bottom-right-radius: {{ $settings['buttonborderradius-right-bottom'] }}px;
	@else
		border-bottom-right-radius: 0;
	@endif
	@if(!empty($settings['buttonbordercolor-left']))
		border-left-color: {{ $settings['buttonbordercolor-left'] }};
	@endif
	@if(!empty($settings['buttonbordercolor-right']))
		border-right-color: {{ $settings['buttonbordercolor-right'] }};
	@endif
	@if(!empty($settings['buttonbordercolor-top']))
		border-top-color: {{ $settings['buttonbordercolor-top'] }};
	@endif
	@if(!empty($settings['buttonbordercolor-bottom']))
		border-bottom-color: {{ $settings['buttonbordercolor-bottom'] }};
	@endif
	@if(!empty($settings['buttonpadding-left']))
		padding-left: {{ $settings['buttonpadding-left'] }}px;
	@endif
	@if(!empty($settings['buttonpadding-right']))
		padding-right: {{ $settings['buttonpadding-right'] }}px;
	@endif
	@if(!empty($settings['buttonpadding-top']))
		padding-top: {{ $settings['buttonpadding-top'] }}px;
	@endif
	@if(!empty($settings['buttonpadding-bottom']))
		padding-bottom: {{ $settings['buttonpadding-bottom'] }}px;
	@endif
	@if(!empty($settings['buttonmargin-left']))
		margin-left: {{ $settings['buttonmargin-left'] }}px;
	@endif
	@if(!empty($settings['buttonmargin-right']))
		margin-right: {{ $settings['buttonmargin-right'] }}px;
	@endif
	@if(!empty($settings['buttonmargin-top']))
		margin-top: {{ $settings['buttonmargin-top'] }}px;
	@endif
	@if(!empty($settings['buttonmargin-bottom']))
		margin-bottom: {{ $settings['buttonmargin-bottom'] }}px;
	@endif
	@if(!empty($settings['buttonfont']))
		font-size: {{ $settings['buttonfont'] }}px;
	@endif
	@if(!empty($settings['buttonborderstyle']))
		border-style: {{ $settings['buttonborderstyle'] }};
	@endif
	@if(!empty($settings['buttonfont_family']))
		font-family: {{ $settings['buttonfont_family'] }};
	@endif
	height: auto;
	@if(!empty($settings['buttonwidth']))
		width: {{ $settings['buttonwidth'] }};
	else:
		width: auto;
	@endif
	cursor: pointer;
}
</style>
<script>
jQuery(document).ready(function(){
	setInterval(function(){
		jQuery('.checkboxgroup').each(function(){
			var texts = '';
			jQuery(this).find('input[type="checkbox"]').each(function(){
				if(jQuery(this).is(':checked')){
					texts = texts+jQuery(this).val()+', ';
				}
			})
			jQuery(this).find('input[type="text"]').val(texts);
		})
	}, 100);
	jQuery("#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}").validate({
		rules:{
		  @foreach($settings['type'] as $key=>$field)
			"{{ !empty($settings['name'][$key])?$settings['name'][$key]:'' }}": {
				@if(!empty($settings['required'][$key]))
					required: true,
				@endif
			},
		  @endforeach
		},
		messages: {
			@foreach($settings['type'] as $key=>$field)
				"{{ !empty($settings['name'][$key])?$settings['name'][$key]:'' }}": {
					@if(!empty($settings['required'][$key]))
						required: "{{ !empty($settings['custommessage'][$key])?$settings['custommessage'][$key]:'This field is required' }}",
					@endif
				},
			@endforeach
		},
		submitHandler: function(form) {

			@if(!empty($settings['notification_type']) && $settings['notification_type']=='hubspot')
				form.submit();

				@if(!empty($settings['submission_type']) && $settings['submission_type']=='thankyou')
					jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').find('button[type="submit"]').prop('disabled', true);
					jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').html('Please wait...');
					jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').addClass('in');
					setTimeout(function(){
						jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').hide();
						jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').html('{!! !empty($settings['thankyou_message'])?$settings['thankyou_message']:'' !!}');
						jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').fadeIn();
						jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').find('button[type="submit"]').prop('disabled', false);
						jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').trigger('reset');
						@if(!empty($settings['hide_form']))
							jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').hide();
						@endif
					}, 1000);
				@endif
			@endif

			@if(!empty($settings['notification_type']) && $settings['notification_type']=='custom')
				jQuery.ajax({
						type: "POST",
						url: "https://mx.{{ config('app.basedomain') }}",
						data: { data: jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').serialize(), adminmail:'{{ !empty($settings['admin_email'])?$settings['admin_email']:Auth::user()->email }}', recipients:'{{ !empty($settings['recipients'])?$settings['recipients']:'' }}', from: '{{ !empty($settings['from'])?$settings['from']:Auth::user()->email }}', fromname: '{{ !empty($settings['fromname'])?$settings['fromname']:\Config::get('app.name') }}', subject:'{{ !empty($settings['subject'])?$settings['subject']:'' }}', replyto:'{{ !empty($settings['replyto'])?$settings['replyto']:'' }}' },
						dataType: "text",
						success: function(resultData){
							if(resultData=='success'){
								@if(!empty($settings['submission_type']) && $settings['submission_type']=='thankyou')
									jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').find('button[type="submit"]').prop('disabled', true);
									jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').html('Please wait...');
									jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').addClass('in');
									setTimeout(function(){
										jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').hide();
										jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').html('{!! !empty($settings['thankyou_message'])?$settings['thankyou_message']:'' !!}');
										jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_success').fadeIn();
										jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').find('button[type="submit"]').prop('disabled', false);
										jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').trigger('reset');
										@if(!empty($settings['hide_form']))
											jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').hide();
										@endif
									}, 1000);
									@if(getSetting('formEntrySave', 'firebase'))
												var rand = Math.floor((Math.random() * 10000000000000) + 1);
												var formRef = firebase.database().ref('formenties/{{ $widget->type }}_{{ $widget->id }}/'+rand);
												var json = {};
										    json.fields = [];
										    jQuery('#form_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }} .form-control').each(function(e){
										        var obj = {};
										        obj.name = jQuery(this).attr('name');
										        obj.value = jQuery(this).val();
										        json.fields.push(obj);
										    });
												formRef.set(json, function(error) {
														if (error) {
															console.log(error);
														} else {
															console.log('success');
														}
													});
									@endif
								@endif
							}
						}
				});
			@endif


		}
	});
})
</script>
@endif
