<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
	<div class="a2a_kit {{ !empty($settings['icon_size'])? 'a2a_kit_size_'.$settings['icon_size']:'' }} a2a_{{ !empty($settings['iconstyle'])?$settings['iconstyle']:'' }} {{ !empty($settings['floating_icon']) && $settings['floating_icon']=='yes'?'a2a_floating_style':'' }}" data-a2a-icon-color="{{ !empty($settings['iconbgcolor'])? $settings['iconbgcolor']:'' }},{{ !empty($settings['iconcolor'])? $settings['iconcolor']:'' }}" style="left:{{ !empty($settings['left'])?$settings['left']:'' }}px;top:{{ !empty($settings['top'])?$settings['top']:'' }}px">
		@if(!empty($settings['icon']) && sizeof($settings['icon'])>0)
			@foreach($settings['icon'] as $key=>$val)
				 <a class="a2a_button_{{ $val }} {{ !empty($settings['showcounter']) && $settings['showcounter']=='yes'?'a2a_counter':'' }}">
					@if(!empty($settings['customicon'][$key]))
						<img src="{{ $settings['customicon'][$key] }}" border="{{ !empty($settings['border'])? $settings['border']:'' }}" alt="{{ $val }}">
					@endif
				 </a>
			@endforeach
		@endif
		@if(!empty($settings['showmore']) && $settings['showmore']=='yes')
			<a class="a2a_dd {{ !empty($settings['showcounter']) && $settings['showcounter']=='yes'?'a2a_counter':'' }}" href="https://www.addtoany.com/share"></a>
		@endif
	</div>

</div>
<style type="text/css">
.{{ $widget->type }}_{{ $widget->id }} .a2a_svg, .{{ $widget->type }}_{{ $widget->id }} img{ border: {{ !empty($settings['border'])? $settings['border']:'' }}px solid !important;border-color: {{ !empty($settings['bordercolor'])? $settings['bordercolor']:'' }} !important; }
.{{ $widget->type }}_{{ $widget->id }} .a2a_svg { border-radius: {{ !empty($settings['borderradius'])? $settings['borderradius']:'' }} !important; }
.{{ $widget->type }}_{{ $widget->id }} img{
	width: {{ !empty($settings['custom_icon_width'])? $settings['custom_icon_width']:'27px' }};
	height: {{ !empty($settings['custom_icon_height'])? $settings['custom_icon_height']:'27px' }};
}
.a2a_count *{
	color:#000000;
}
</style>
