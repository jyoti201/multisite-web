@if(!empty($settings['title']))
	<div class="widget flex-wrap {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<div class="grid-items row flex-wrap no-gutters">
	@foreach($settings['title'] as $key=>$val)
		<div class="grid col-sm-{{ !empty($settings['colperrow'])? 12/$settings['colperrow']:'3' }}" style="{{ !empty($settings['spacing-left'])? 'padding-left:'.$settings['spacing-left'].'px;':'' }}{{ !empty($settings['spacing-right'])? 'padding-right:'.$settings['spacing-right'].'px;':'' }}{{ !empty($settings['spacing-top'])? 'padding-top:'.$settings['spacing-top'].'px;':'' }}{{ !empty($settings['spacing-bottom'])? 'padding-bottom:'.$settings['spacing-bottom'].'px;':'' }}">
				@if(!empty($settings['beforetext'][$key]))
					<div class="beforetext {!! !empty($settings['hidebeforetext']) && $settings['hidebeforetext']=='on'? 'hidden':'' !!}">{!! $settings['beforetext'][$key] !!}</div>
				@endif
			<figure class="{{ empty($settings['image'][$key])?'no-image ':'' }}{{ !empty($settings['hoverstyle'])? 'effect-'.$settings['hoverstyle']:'' }}" style="
			{{ !empty($settings['backlightcolor'])?'background:'.$settings['backlightcolor'].';':'' }}
			{{ !empty($settings['border'])?'border:'.$settings['border'].'px solid;':'' }}
			{{ !empty($settings['textcolor'])?'color:'.$settings['textcolor'].';':'' }}
			{{ !empty($settings['bordercolor'])?'border-color:'.$settings['bordercolor'].';':'' }}
			{{ !empty($settings['borderradius'])?'border-radius:'.$settings['borderradius'].'px;overflow:hidden;':'' }}">
			@if(!empty($settings['image'][$key]))
				<div class="img" style="background-image:url('{{ $settings['image'][$key] }}');{{ !empty($settings['height'])?'height:'.$settings['height'].'px;overflow:hiiden;':'height:250px;overflow:hidden;' }}"></div>
				@endif
				<figcaption class="{!! !empty($settings['showtitle']) && $settings['showtitle']=='on'? 'showtitle':'' !!} {!! !empty($settings['showdescription']) && $settings['showdescription']=='on'? 'showdescription':'' !!} ">
					@if(!empty($settings['title'][$key]))
						<h2 class="{!! !empty($settings['hidetitle']) && $settings['hidetitle']=='on'? 'hidden':'' !!}">{!! $settings['title'][$key] !!}</h2>
					@endif
					@if(!empty($settings['description'][$key]))
						<div class="{!! !empty($settings['hidedescription']) && $settings['hidedescription']=='on'? 'hidden':'' !!}">{!! $settings['description'][$key] !!}</div>
					@endif

				</figcaption>
				@if(!empty($settings['lightbox']) && !empty($settings['image'][$key]))
					<a href="{{ $settings['image'][$key] }}" class="image-popup" data-effect="mfp-zoom-in">&nbsp;</a>
				@elseif(!empty($settings['link'][$key]) && empty($settings['lightbox']))
					<a href="{{ $settings['link'][$key] }}" target="{{ $settings['linktarget'][$key] }}">&nbsp;</a>
				@endif
			</figure>
			@if(!empty($settings['aftertext'][$key]))
				<div class="aftertext {!! !empty($settings['hideaftertext']) && $settings['hideaftertext']=='on'? 'hidden':'' !!}">{!! $settings['aftertext'][$key] !!}</div>
			@endif
		</div>
	@endforeach
</div>
	@if(!empty($settings['overlaycolor']))
	<style>
	.{{ $widget->type }}_{{ $widget->id }} .grid figure:before {
		content: "";
		background: {{ $settings['overlaycolor'] }};
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		z-index: 9;
		-webkit-transition: all 0.3s ease-in-out;
		-moz-transition: all 0.3s ease-in-out;
		-o-transition: all 0.3s ease-in-out;
		transition: all 0.3s ease-in-out;
		@if(!empty($settings['overlayonhover']))
			opacity:0;
		@endif
	}
	@if(!empty($settings['overlayonhover']))
		.{{ $widget->type }}_{{ $widget->id }} .grid figure:hover:before{
			opacity:1;
		}
	@else
		.{{ $widget->type }}_{{ $widget->id }} .grid figure:hover:before{
			opacity:0;
		}
	@endif

	</style>
	@endif

	@if(!empty($settings['lightbox']))
	<script>
	 jQuery(document).ready(function($) {
			$('.image-popup').magnificPopup({
				type: 'image',
				removalDelay: 500,
				callbacks: {
					beforeOpen: function() {
						 this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
						 this.st.mainClass = this.st.el.attr('data-effect');
					}
				},
				fixedContentPos: false,
				fixedBgPos: true,
				closeOnContentClick: true,
				midClick: true,
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0,1]
				}
			});
		});
	</script>
	@endif
	</div>
@endif
