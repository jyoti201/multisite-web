@if(!empty($settings['style']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }} searchbox_{!! $settings['style'] !!}">
		<form action="{{ !empty($settings['searchpage'])?'/'.$settings['searchpage'].'/':'' }}">
			<div class="tipue_search_group">
			<input type="text" name="q" class="tipue_search_input" id="tipue_search_input_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}" pattern=".{3,}" title="At least 3 characters" placeholder="{{ !empty($settings['placeholder'])?$settings['placeholder']:'' }}" required>

			@if(!empty($settings['showicon']) && $settings['showicon']=='yes')
			<button type="submit" class="tipue_search_button">
			<div class="tipue_search_icon">&#9906;</div>
			</button>
			@else
			<button type="submit" class="btn btn-default">
			{{ !empty($settings['buttontext'])?$settings['buttontext']:'' }}
			</button>
			@endif

			</div>
		</form>
	</div>
<script>
var tipuesearch_stop_words = ["a", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "aren't", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "can't", "cannot", "could", "couldn't", "did", "didn't", "do", "does", "doesn't", "doing", "don't", "down", "during", "each", "few", "for", "from", "further", "had", "hadn't", "has", "hasn't", "have", "haven't", "having", "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how", "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "isn't", "it", "it's", "its", "itself", "let's", "me", "more", "most", "mustn't", "my", "myself", "no", "nor", "not", "of", "off", "on", "once", "only", "or", "other", "ought", "our", "ours", "ourselves", "out", "over", "own", "same", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "so", "some", "such", "than", "that", "that's", "the", "their", "theirs", "them", "themselves", "then", "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "wasn't", "we", "we'd", "we'll", "we're", "we've", "were", "weren't", "what", "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why", "why's", "with", "won't", "would", "wouldn't", "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself", "yourselves"];
var tipuesearch_replace = {'words': [
     {'word': 'tip', 'replace_with': 'tipue'},
     {'word': 'javscript', 'replace_with': 'javascript'},
     {'word': 'jqeury', 'replace_with': 'jquery'}
]};
var tipuesearch_weight = {'weight': [
     {'url': '/', 'score': 60},
]};
var tipuesearch_stem = {'words': [
     {'word': 'e-mail', 'stem': 'email'},
     {'word': 'javascript', 'stem': 'jquery'},
     {'word': 'javascript', 'stem': 'js'}
]};
var tipuesearch_related = {'Related': [
     {'search': 'tipue', 'related': 'Search', 'include': 1},
     {'search': 'tipue', 'related': 'jQuery'},
     {'search': 'tipue', 'related': 'Blog'},
     {'search': 'tipue', 'related': 'Support'},
     {'search': 'tipue search', 'related': 'Demo', 'include': 1},
     {'search': 'tipue search', 'related': 'Support'}
]};

var tipuesearch_string_1 = 'No title';
var tipuesearch_string_2 = 'Showing results for';
var tipuesearch_string_3 = 'Search instead for';
var tipuesearch_string_4 = '1 result';
var tipuesearch_string_5 = 'results';
var tipuesearch_string_6 = '<';
var tipuesearch_string_7 = '>';
var tipuesearch_string_8 = 'Nothing found.';
var tipuesearch_string_9 = 'Common words are largely ignored.';
var tipuesearch_string_10 = 'Related';
var tipuesearch_string_11 = 'Search should be one character or more.';
var tipuesearch_string_12 = 'Search should be';
var tipuesearch_string_13 = 'characters or more.';
var tipuesearch_string_14 = 'seconds';
var tipuesearch_string_15 = 'Open Image';
var tipuesearch_string_16 = 'Goto Page';
var startTimer = new Date().getTime();

var tipuesearch = {"pages": [
@foreach($pages as $page)
     {"title": "{{ $page->title }}", "text": "{{ removeHtmlTagsOfField($page->html) }}", "url": "/{{ $page->slug }}/"},
@endforeach
@foreach($posts as $post)
     {"title": "{{ $post->title }}", "text": "{{ removeHtmlTagsOfField($post->html) }}", "url": "/{{ $post->slug }}/"},
@endforeach
]};
var tipue_search_input = 'tipue_search_input_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}';
</script>

<script>
$(document).ready(function() {
     $('#tipue_search_input_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').tipuesearch();
});
</script>
@endif
