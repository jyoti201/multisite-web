@if(!empty($settings['noofposts']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<div id="{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}" class="blog-posts">
			@if(!empty($settings['categories']))
			{!! getBlogPosts($widget->id, implode(',', $settings['categories'])) !!}
			@else
			{!! getBlogPosts($widget->id, '') !!}
			@endif
		</div>
	</div>
@endif
