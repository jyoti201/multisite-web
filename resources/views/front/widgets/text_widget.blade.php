@if(!empty($settings['content']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		{!! $settings['content'] !!}
	</div>
	<style>
	.{{ $widget->type }}_{{ $widget->id }}{
		@if(!empty($settings['bgcolor']))
			background-color: {{ $settings['bgcolor'] }};
		@endif
		@if(!empty($settings['txcolor']))
			color: {{ $settings['txcolor'] }};
		@endif
		@if(!empty($settings['borderwidth-left']))
			border-left-width: {{ $settings['borderwidth-left'] }}px;
		@else
			border-left-width: 0;
		@endif
		@if(!empty($settings['borderwidth-right']))
			border-right-width: {{ $settings['borderwidth-right'] }}px;
		@else
			border-right-width: 0;
		@endif
		@if(!empty($settings['borderwidth-top']))
			border-top-width: {{ $settings['borderwidth-top'] }}px;
		@else
			border-top-width: 0;
		@endif
		@if(!empty($settings['borderwidth-bottom']))
			border-bottom-width: {{ $settings['borderwidth-bottom'] }}px;
		@else
			border-bottom-width: 0;
		@endif
		@if(!empty($settings['borderwidth-left-top']))
			border-top-left-radius: {{ $settings['borderwidth-left-top'] }}px;
		@else
			border-top-left-radius: 0;
		@endif
		@if(!empty($settings['borderwidth-left-bottom']))
			border-bottom-left-radius: {{ $settings['borderwidth-left-bottom'] }}px;
		@else
			border-bottom-left-radius: 0;
		@endif
		@if(!empty($settings['borderwidth-right-top']))
			border-top-right-radius: {{ $settings['borderwidth-right-top'] }}px;
		@else
			border-top-right-radius: 0;
		@endif
		@if(!empty($settings['borderwidth-right-bottom']))
			border-bottom-right-radius: {{ $settings['borderwidth-right-bottom'] }}px;
		@else
			border-bottom-right-radius: 0;
		@endif
		@if(!empty($settings['bordercolor-left']))
			border-left-color: {{ $settings['bordercolor-left'] }};
		@endif
		@if(!empty($settings['bordercolor-right']))
			border-right-color: {{ $settings['bordercolor-right'] }};
		@endif
		@if(!empty($settings['bordercolor-top']))
			border-top-color: {{ $settings['bordercolor-top'] }};
		@endif
		@if(!empty($settings['bordercolor-bottom']))
			border-bottom-color: {{ $settings['bordercolor-bottom'] }};
		@endif
		@if(!empty($settings['padding-left']))
			padding-left: {{ $settings['padding-left'] }}px;
		@endif
		@if(!empty($settings['padding-right']))
			padding-right: {{ $settings['padding-right'] }}px;
		@endif
		@if(!empty($settings['padding-top']))
			padding-top: {{ $settings['padding-top'] }}px;
		@endif
		@if(!empty($settings['padding-bottom']))
			padding-bottom: {{ $settings['padding-bottom'] }}px;
		@endif
		@if(!empty($settings['margin-left']))
			margin-left: {{ $settings['margin-left'] }}px;
		@endif
		@if(!empty($settings['margin-right']))
			margin-right: {{ $settings['margin-right'] }}px;
		@endif
		@if(!empty($settings['margin-top']))
			margin-top: {{ $settings['margin-top'] }}px;
		@endif
		@if(!empty($settings['margin-bottom']))
			margin-bottom: {{ $settings['margin-bottom'] }}px;
		@endif
		@if(!empty($settings['font']))
			font-size: {{ $settings['font'] }}px;
		@endif
		@if(!empty($settings['borderstyle']))
			border-style: {{ $settings['borderstyle'] }};
		@endif
		@if(!empty($settings['font_family']))
			font-family: {{ $settings['font_family'] }};
		@endif
	}
	</style>
@endif
