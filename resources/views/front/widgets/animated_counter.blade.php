@if(!empty($settings['type']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<div class="animated-counter {{ !empty($settings['columns']) && $settings['columns']>0?'row':'' }}">
			@foreach($settings['type'] as $key=>$val)
				<div class="counter-container
				@if(!empty($settings['columns']) && $settings['columns']>0)
					col-md-{{ 12/$settings['columns'] }}
				@endif
				{{ !empty($settings['class'][$key])? $settings['class'][$key]:'' }}">
					@if(!empty($settings['number'][$key]))
					<div class="counter" data-progress="{{ !empty($settings['progress'][$key])? $settings['progress'][$key]/100:'' }}" id="counter_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_{{ $key }}" style="{{ !empty($settings['width'])? 'width:'.$settings['width'].'px;':'' }}{{ !empty($settings['height'])? 'height:'.$settings['height'].'px;':'' }}">

					</div>
					<script>
					$(document).ready(function() {
						var a = 0;
						$(window).scroll(function() {
						var oTop = $('.{{ $widget->type }}_{{ $widget->id }}').offset().top - window.innerHeight;
						if (a == 0 && $(window).scrollTop() > oTop) {
								@if(!empty($settings['type'][$key]) && $settings['type'][$key]!='none')
									var bar = new ProgressBar.{{ !empty($settings['type'][$key])? $settings['type'][$key]:'' }}(counter_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_{{ $key }}, {
									  strokeWidth: {{ !empty($settings['strokeWidth'][$key])? $settings['strokeWidth'][$key]:'' }},
									  easing: '{{ !empty($settings['easing'][$key])? $settings['easing'][$key]:'' }}',
									  duration: {{ !empty($settings['duration'][$key])? $settings['duration'][$key]:'' }},
									  color: '{{ !empty($settings['color'][$key])? $settings['color'][$key]:'' }}',
									  trailColor: '{{ !empty($settings['trailColor'][$key])? $settings['trailColor'][$key]:'' }}',
									  trailWidth: {{ !empty($settings['trailWidth'][$key])? $settings['trailWidth'][$key]:'' }},
									  svgStyle: {width: '100%', height: '100%'},
									  from: {color: '{{ !empty($settings['color_from'][$key])? $settings['color_from'][$key]:'' }}'},
									  to: {color: '{{ !empty($settings['color_to'][$key])? $settings['color_to'][$key]:'' }}'},
									  step: (state, bar) => {
										@if(!empty($settings['gradient_color'][$key]) && $settings['gradient_color'][$key]=='yes')
											bar.path.setAttribute('stroke', state.color);
										@endif
										bar.setText('<span class="beforetext" style="{{ !empty($settings['before_text_font_family'])? 'font-family:'.$settings['before_text_font_family'].';':'' }}{{ !empty($settings['before_text_spacing'])? 'margin:'.$settings['before_text_spacing'].'px;':'' }}{!! !empty($settings['before_text_block'][$key])? 'display:block;':'' !!}{{ !empty($settings['before_text_font_size'])? 'font-size:'.$settings['before_text_font_size'].'px;':'' }}">{!! !empty($settings['before_text'][$key])? $settings['before_text'][$key]:'' !!}</span><span class="progressnumber" style="{!! !empty($settings['number_block'][$key])? 'display:block;':'' !!}{{ !empty($settings['number_font_family'])? 'font-family:'.$settings['number_font_family'].';':'' }}{{ !empty($settings['number_spacing'])? 'margin:'.$settings['number_spacing'].'px;':'' }}{{ !empty($settings['number_font_size'])? 'font-size:'.$settings['number_font_size'].'px;':'' }}">' + Math.round(bar.value() * {{ !empty($settings['number'][$key])? $settings['number'][$key]:'' }}) + '</span><span class="aftertext" style="{{ !empty($settings['after_text_font_family'])? 'font-family:'.$settings['after_text_font_family'].';':'' }}{{ !empty($settings['after_text_spacing'])? 'margin:'.$settings['after_text_spacing'].'px;':'' }}{!! !empty($settings['after_text_block'][$key])? 'display:block;':'' !!}{{ !empty($settings['after_text_font_size'])? 'font-size:'.$settings['after_text_font_size'].'px;':'' }}">{!! !empty($settings['after_text'][$key])? $settings['after_text'][$key]:'' !!}</span>');
									  }
									});
									bar.animate({{ !empty($settings['progress'][$key])? $settings['progress'][$key]/100:'' }});
								@endif
								@if(!empty($settings['type'][$key]) && $settings['type'][$key]=='none')
									var bar = new ProgressBar.Line(counter_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_{{ $key }}, {
									  strokeWidth: 0,
									  easing: '{{ !empty($settings['easing'][$key])? $settings['easing'][$key]:'' }}',
									  duration: {{ !empty($settings['duration'][$key])? $settings['duration'][$key]:'' }},
									  color: '{{ !empty($settings['color'][$key])? $settings['color'][$key]:'' }}',
									  trailColor: '{{ !empty($settings['trailColor'][$key])? $settings['trailColor'][$key]:'' }}',
									  trailWidth: 0,
									  svgStyle: {width: '100%', height: '100%'},
									  from: {color: '{{ !empty($settings['color_from'][$key])? $settings['color_from'][$key]:'' }}'},
									  to: {color: '{{ !empty($settings['color_to'][$key])? $settings['color_to'][$key]:'' }}'},
									  step: (state, bar) => {
										@if(!empty($settings['gradient_color'][$key]) && $settings['gradient_color'][$key]=='yes')
											bar.path.setAttribute('stroke', state.color);
										@endif
										bar.setText('<span class="beforetext" style="{{ !empty($settings['before_text_font_family'])? 'font-family:'.$settings['before_text_font_family'].';':'' }}{{ !empty($settings['before_text_spacing'])? 'margin:'.$settings['before_text_spacing'].'px;':'' }}{!! !empty($settings['before_text_block'][$key])? 'display:block;':'' !!}{{ !empty($settings['before_text_font_size'])? 'font-size:'.$settings['before_text_font_size'].'px;':'' }}">{!! !empty($settings['before_text'][$key])? $settings['before_text'][$key]:'' !!}</span><span class="progressnumber" style="{!! !empty($settings['number_block'][$key])? 'display:block;':'' !!}{{ !empty($settings['number_font_family'])? 'font-family:'.$settings['number_font_family'].';':'' }}{{ !empty($settings['number_spacing'])? 'margin:'.$settings['number_spacing'].'px;':'' }}{{ !empty($settings['number_font_size'])? 'font-size:'.$settings['number_font_size'].'px;':'' }}">' + Math.round(bar.value() * {{ !empty($settings['number'][$key])? $settings['number'][$key]:'' }}) + '</span><span class="aftertext" style="{{ !empty($settings['after_text_font_family'])? 'font-family:'.$settings['after_text_font_family'].';':'' }}{{ !empty($settings['after_text_spacing'])? 'margin:'.$settings['after_text_spacing'].'px;':'' }}{!! !empty($settings['after_text_block'][$key])? 'display:block;':'' !!}{{ !empty($settings['after_text_font_size'])? 'font-size:'.$settings['after_text_font_size'].'px;':'' }}">{!! !empty($settings['after_text'][$key])? $settings['after_text'][$key]:'' !!}</span>');
									  }
									});

									bar.animate({{ !empty($settings['progress'][$key])? $settings['progress'][$key]/100:'' }});
								@endif
						a = 1;
						  }

						});
					});
					</script>
					@endif
				</div>
			@endforeach
		</div>
	</div>

@endif
