@if(!empty($settings['random_id']) && sizeof($settings['random_id'])>0)
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
@if(!empty($settings['type']) && $settings['type']=='tabs')

			@if(!empty($settings['vertical']))
			<div class="row flex-column flex-md-row no-gutters">
				<div class="col-md-12" style="{{ !empty($settings['vertical_width'])? 'max-width:'.$settings['vertical_width'].'px' : '' }}">
		  @endif
				<ul class="{{ !empty($settings['tabs_position'])?$settings['tabs_position']:'' }} {{ !empty($settings['vertical'])?'flex-column':'' }} nav nav-tabs
				@if(!empty($settings['icon']))
					{{ !empty($settings['icon_position'])?'iconposition_'.$settings['icon_position']:'' }}
				@endif
				">
					@if(!empty($settings['random_id']) && sizeof($settings['random_id'])>0)
								@foreach($settings['random_id'] as $key=>$val)
								  <li class="nav-item">
								  <a class="no-animation {{ !empty($settings['collapsetab_active_'.$val])?'active':'' }}" data-toggle="tab" href="#{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_tab{{ $val }}"
								  @if(!empty($settings['collapsetab_unique_'.$val]))
									style="{{ !empty($settings['collapsetab_bgcolor'][$key])?'background-color:'.$settings['collapsetab_bgcolor'][$key].';':'' }}{{ !empty($settings['collapsetab_color'][$key])?'color:'.$settings['collapsetab_color'][$key].';':'' }}"
								  @endif
								  >{!! $settings['collapsetab_title'][$key] !!}
									@if(!empty($settings['icon']))
									<span class="arrows
									@if(!empty($settings['collapsetab_active_'.$val]))
										{{ !empty($settings['icon_active'])?$settings['icon_active']:'' }}
									@else
										{{ !empty($settings['icon'])?$settings['icon']:'' }}
									@endif
									"></span>
									@endif
								</a>



								  </li>
								@endforeach
					@endif
				</ul>
				@if(!empty($settings['vertical']))
					</div>
					<div class="col">
			  @endif
				<div class="tab-content">
					@if(!empty($settings['random_id']) && sizeof($settings['random_id'])>0)
								@foreach($settings['random_id'] as $key=>$val)
								<div id="{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_tab{{ $val }}" class="tab-pane fade {{ !empty($settings['collapsetab_active_'.$val])?'show active':'' }}"
									@if(!empty($settings['collapsetab_unique_'.$val]))
									style="{{ !empty($settings['collapsetab_bgcolor'][$key])?'background-color:'.$settings['collapsetab_bgcolor'][$key].';':'' }}"
									@endif
									>
									{!! ImplementWidgets($settings['collapsetab_content'][$key]) !!}
								</div>
								@endforeach
					@endif
				</div>
				@if(!empty($settings['vertical']))
					</div>
				</div>
				@endif
<script>
$(document).ready(function() {
	$('.{{ $widget->type }}_{{ $widget->id }} .nav-tabs')
	  .on('show.bs.tab', function(e) {
		$(e.target).find('.arrows').removeClass('{{ !empty($settings['icon'])?$settings['icon']:'' }}');
		$(e.target).find('.arrows').addClass('{{ !empty($settings['icon_active'])?$settings['icon_active']:'' }}');
	  })
	  .on('hide.bs.tab', function(e) {
		$(e.target).find('.arrows').removeClass('{{ !empty($settings['icon_active'])?$settings['icon_active']:'' }}');
		$(e.target).find('.arrows').addClass('{{ !empty($settings['icon'])?$settings['icon']:'' }}');
	  });
});
</script>
<style>
.{{ $widget->type }}_{{ $widget->id }} .nav.nav-tabs>li>a{
	@if(!empty($settings['heading_font_family']))
	  font-family: {{ $settings['heading_font_family'] }};
	@endif
	@if(!empty($settings['heading_font_weight']))
	  font-weight: {{ $settings['heading_font_weight'] }};
	@endif
	@if(!empty($settings['heading_padding-left']))
	  padding-left: {{ $settings['heading_padding-left'] }}px;
	@endif
	@if(!empty($settings['heading_padding-right']))
	  padding-right: {{ $settings['heading_padding-right'] }}px;
	@endif
	@if(!empty($settings['heading_padding-top']))
	  padding-top: {{ $settings['heading_padding-top'] }}px;
	@endif
	@if(!empty($settings['heading_padding-bottom']))
	  padding-bottom: {{ $settings['heading_padding-bottom'] }}px;
	@endif
	@if(!empty($settings['heading_txcolor']))
	  color: {{ $settings['heading_txcolor'] }};
	@endif
	@if(!empty($settings['heading_bgcolor']))
	  background-color: {{ $settings['heading_bgcolor'] }};
	@endif
	@if(!empty($settings['heading_borderwidth-left']))
	  border-left-width: {{ $settings['heading_borderwidth-left'] }}px;
	@else
	  border-left-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-right']))
	  border-right-width: {{ $settings['heading_borderwidth-right'] }}px;
	@else
	  border-right-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-top']))
	  border-top-width: {{ $settings['heading_borderwidth-top'] }}px;
	@else
	  border-top-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-bottom']))
	  border-bottom-width: {{ $settings['heading_borderwidth-bottom'] }}px;
	@else
	  border-bottom-width: 0;
	@endif
	@if(!empty($settings['heading_borderradius-left-top']))
	  border-top-left-radius: {{ $settings['heading_borderradius-left-top'] }}px;
	@else
	  border-top-left-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-left-bottom']))
	  border-bottom-left-radius: {{ $settings['heading_borderradius-left-bottom'] }}px;
	@else
	  border-bottom-left-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-right-top']))
	  border-top-right-radius: {{ $settings['heading_borderradius-right-top'] }}px;
	@else
	  border-top-right-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-right-bottom']))
	  border-bottom-right-radius: {{ $settings['heading_borderradius-right-bottom'] }}px;
	@else
	  border-bottom-right-radius: 0;
	@endif
	@if(!empty($settings['heading_bordercolor-left']))
	  border-left-color: {{ $settings['heading_bordercolor-left'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-right']))
	  border-right-color: {{ $settings['heading_bordercolor-right'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-top']))
	  border-top-color: {{ $settings['heading_bordercolor-top'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-bottom']))
	  border-bottom-color: {{ $settings['heading_bordercolor-bottom'] }};
	@endif
	@if(!empty($settings['heading_font']))
	  font-size: {{ $settings['heading_font'] }}px;
	@endif
	@if(!empty($settings['heading_borderstyle']))
	  border-style: {{ $settings['heading_borderstyle'] }};
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .nav.nav-tabs>li>a:hover,.{{ $widget->type }}_{{ $widget->id }} .nav.nav-tabs>li>a.active{
	@if(!empty($settings['heading_font_weight_hover']))
	  font-weight: {{ $settings['heading_font_weight_hover'] }};
	@endif
	@if(!empty($settings['heading_txcolor_hover']))
	  color: {{ $settings['heading_txcolor_hover'] }};
	@endif
	@if(!empty($settings['heading_bgcolor_hover']))
	  background-color: {{ $settings['heading_bgcolor_hover'] }};
	@endif
	@if(!empty($settings['heading_borderwidth-left_hover']))
	  border-left-width: {{ $settings['heading_borderwidth-left_hover'] }}px;
	@else
	  border-left-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-right_hover']))
	  border-right-width: {{ $settings['heading_borderwidth-right_hover'] }}px;
	@else
	  border-right-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-top_hover']))
	  border-top-width: {{ $settings['heading_borderwidth-top_hover'] }}px;
	@else
	  border-top-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-bottom_hover']))
	  border-bottom-width: {{ $settings['heading_borderwidth-bottom_hover'] }}px;
	@else
	  border-bottom-width: 0;
	@endif
	@if(!empty($settings['heading_borderradius-left-top_hover']))
	  border-top-left-radius: {{ $settings['heading_borderradius-left-top_hover'] }}px;
	@else
	  border-top-left-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-left-bottom_hover']))
	  border-bottom-left-radius: {{ $settings['heading_borderradius-left-bottom_hover'] }}px;
	@else
	  border-bottom-left-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-right-top_hover']))
	  border-top-right-radius: {{ $settings['heading_borderradius-right-top_hover'] }}px;
	@else
	  border-top-right-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-right-bottom_hover']))
	  border-bottom-right-radius: {{ $settings['heading_borderradius-right-bottom_hover'] }}px;
	@else
	  border-bottom-right-radius: 0;
	@endif
	@if(!empty($settings['heading_bordercolor-left_hover']))
	  border-left-color: {{ $settings['heading_bordercolor-left_hover'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-right_hover']))
	  border-right-color: {{ $settings['heading_bordercolor-right_hover'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-top_hover']))
	  border-top-color: {{ $settings['heading_bordercolor-top_hover'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-bottom_hover']))
	  border-bottom-color: {{ $settings['heading_bordercolor-bottom_hover'] }};
	@endif
	@if(!empty($settings['heading_font_hover']))
	  font-size: {{ $settings['heading_font_hover'] }}px;
	@endif
	@if(!empty($settings['heading_borderstyle_hover']))
	  border-style: {{ $settings['heading_borderstyle_hover'] }};
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .tab-content .tab-pane{
	@if(!empty($settings['content_font_family']))
	  font-family: {{ $settings['content_font_family'] }};
	@endif
	@if(!empty($settings['content_font_weight']))
	  font-weight: {{ $settings['content_font_weight'] }};
	@endif
	@if(!empty($settings['content_padding-left']))
	  padding-left: {{ $settings['content_padding-left'] }}px;
	@endif
	@if(!empty($settings['content_padding-right']))
	  padding-right: {{ $settings['content_padding-right'] }}px;
	@endif
	@if(!empty($settings['content_padding-top']))
	  padding-top: {{ $settings['content_padding-top'] }}px;
	@endif
	@if(!empty($settings['content_padding-bottom']))
	  padding-bottom: {{ $settings['content_padding-bottom'] }}px;
	@endif
	@if(!empty($settings['content_txcolor']))
	  color: {{ $settings['content_txcolor'] }};
	@endif
	@if(!empty($settings['content_bgcolor']))
	  background-color: {{ $settings['content_bgcolor'] }};
	@endif
	@if(!empty($settings['content_borderwidth-left']))
	  border-left-width: {{ $settings['content_borderwidth-left'] }}px;
	@else
	  border-left-width: 0;
	@endif
	@if(!empty($settings['content_borderwidth-right']))
	  border-right-width: {{ $settings['content_borderwidth-right'] }}px;
	@else
	  border-right-width: 0;
	@endif
	@if(!empty($settings['content_borderwidth-top']))
	  border-top-width: {{ $settings['content_borderwidth-top'] }}px;
	@else
	  border-top-width: 0;
	@endif
	@if(!empty($settings['content_borderwidth-bottom']))
	  border-bottom-width: {{ $settings['content_borderwidth-bottom'] }}px;
	@else
	  border-bottom-width: 0;
	@endif
	@if(!empty($settings['content_borderradius-left-top']))
	  border-top-left-radius: {{ $settings['content_borderradius-left-top'] }}px;
	@else
	  border-top-left-radius: 0;
	@endif
	@if(!empty($settings['content_borderradius-left-bottom']))
	  border-bottom-left-radius: {{ $settings['content_borderradius-left-bottom'] }}px;
	@else
	  border-bottom-left-radius: 0;
	@endif
	@if(!empty($settings['content_borderradius-right-top']))
	  border-top-right-radius: {{ $settings['content_borderradius-right-top'] }}px;
	@else
	  border-top-right-radius: 0;
	@endif
	@if(!empty($settings['content_borderradius-right-bottom']))
	  border-bottom-right-radius: {{ $settings['content_borderradius-right-bottom'] }}px;
	@else
	  border-bottom-right-radius: 0;
	@endif
	@if(!empty($settings['content_bordercolor-left']))
	  border-left-color: {{ $settings['content_bordercolor-left'] }};
	@endif
	@if(!empty($settings['content_bordercolor-right']))
	  border-right-color: {{ $settings['content_bordercolor-right'] }};
	@endif
	@if(!empty($settings['content_bordercolor-top']))
	  border-top-color: {{ $settings['content_bordercolor-top'] }};
	@endif
	@if(!empty($settings['content_bordercolor-bottom']))
	  border-bottom-color: {{ $settings['content_bordercolor-bottom'] }};
	@endif
	@if(!empty($settings['content_font']))
	  font-size: {{ $settings['content_font'] }}px;
	@endif
	@if(!empty($settings['content_borderstyle']))
	  border-style: {{ $settings['content_borderstyle'] }};
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .nav-tabs.iconposition_right li a .arrows{
		@if(!empty($settings['icon_size']))
			font-size: {{ $settings['icon_size'] }}px;
		@endif

}

@if(!empty($settings['icon']) && !empty($settings['icon_size']))
.{{ $widget->type }}_{{ $widget->id }} .nav.nav-tabs>li a{

	@if(!empty($settings['icon_position']) && $settings['icon_position']=='left')
				@if(!empty($settings['heading_padding-left']))
					padding-left: {{ $settings['heading_padding-left']+$settings['icon_size']+10 }}px;
				@else
					padding-left: {{ $settings['icon_size']+10 }}px;
				@endif
	@endif
	@if(!empty($settings['icon_position']) && $settings['icon_position']=='right')
		@if(!empty($settings['heading_padding-left']))
			padding-right: {{ $settings['heading_padding-left']+$settings['icon_size']+10 }}px;
		@else
			padding-right: {{ $settings['icon_size']+10 }}px;
		@endif
	@endif

}
@endif

.{{ $widget->type }}_{{ $widget->id }} .nav.nav-tabs>li a .arrows{
	@if(!empty($settings['icon_color']))
			color: {{ $settings['icon_color'] }};
		@endif
}
.{{ $widget->type }}_{{ $widget->id }} .nav.nav-tabs>li a.active .arrows{
	@if(!empty($settings['icon_color_active']))
			color: {{ $settings['icon_color_active'] }};
		@endif
}
</style>


@elseif(!empty($settings['type']) && $settings['type']=='collapse')


				<div class="bs-collapse
				@if(!empty($settings['icon']))
					{{ !empty($settings['icon_position'])?'iconposition_'.$settings['icon_position']:'' }}
				@endif
				" id="accordion_{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}">
					@if(!empty($settings['random_id']) && sizeof($settings['random_id'])>0)
								@foreach($settings['random_id'] as $key=>$val)
									  <div class="card">
									    <div class="card-header {{ !empty($settings['collapsetab_active_'.$val])?'active':'' }}"
										@if(!empty($settings['collapsetab_unique_'.$val]))
											style="{{ !empty($settings['collapsetab_bgcolor'][$key])?'background-color:'.$settings['collapsetab_bgcolor'][$key].';':'' }}"
										@endif
										>
									      <h4>
									        <a class="card-link no-animation" data-toggle="collapse" data-target="#{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_collapse{{ $val }}" aria-expanded="false" aria-controls="collapseTwo"
													@if(!empty($settings['collapsetab_unique_'.$val]))
														style="{{ !empty($settings['collapsetab_color'][$key])?'color:'.$settings['collapsetab_color'][$key].';':'' }}"
													@endif
													href="#{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_collapse{{ $val }}">{!! !empty($settings['collapsetab_title'][$key])?$settings['collapsetab_title'][$key]:'' !!}</a>
									      </h4>
												@if(!empty($settings['icon']))
												<span class="arrows
												@if(!empty($settings['collapsetab_active_'.$val]))
													{{ !empty($settings['icon_active'])?$settings['icon_active']:'' }}
												@else
													{{ !empty($settings['icon'])?$settings['icon']:'' }}
												@endif
												"></span>
												@endif
									    </div>
									    <div id="{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_collapse{{ $val }}" class="collapse {{ !empty($settings['collapsetab_active_'.$val])?'show':'' }}" {!! !empty($settings['group'])?'data-parent="#accordion_'.$widget->type.'_'.$widget->id.'"':'' !!}>
									      <div class="card-body"
										  @if(!empty($settings['collapsetab_unique_'.$val]))
											style="{{ !empty($settings['collapsetab_bgcolor'][$key])?'background-color:'.$settings['collapsetab_bgcolor'][$key].';':'' }}"
										  @endif
										  >{!! !empty($settings['collapsetab_content'][$key])?ImplementWidgets($settings['collapsetab_content'][$key]):'' !!}
										  </div>
									    </div>
									  </div>
								@endforeach
					@endif
				</div>

<script>
$(document).ready(function() {
	$('#accordion_{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}')
	  .on('show.bs.collapse', function(e) {
		$(e.target).prev().addClass('active');
		$(e.target).prev().find('.arrows').removeClass('{{ !empty($settings['icon'])?$settings['icon']:'' }}');
		$(e.target).prev().find('.arrows').addClass('{{ !empty($settings['icon_active'])?$settings['icon_active']:'' }}');
	  })
	  .on('hide.bs.collapse', function(e) {
		$(e.target).prev().removeClass('active');
		$(e.target).prev().find('.arrows').removeClass('{{ !empty($settings['icon_active'])?$settings['icon_active']:'' }}');
		$(e.target).prev().find('.arrows').addClass('{{ !empty($settings['icon'])?$settings['icon']:'' }}');
	  });
});
</script>
<style>
.{{ $widget->type }}_{{ $widget->id }} .bs-collapse{
	@if(!empty($settings['heading_borderwidth-left']) || !empty($settings['heading_borderwidth-right']) || !empty($settings['heading_borderwidth-top']) || !empty($settings['heading_borderwidth-bottom']))
	border: 0;
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .card>.card-header{
	@if(!empty($settings['heading_bgcolor']))
	  background-color: {{ $settings['heading_bgcolor'] }};
	@endif
	@if(!empty($settings['heading_borderwidth-left']))
	  border-left-width: {{ $settings['heading_borderwidth-left'] }}px;
	@else
	  border-left-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-right']))
	  border-right-width: {{ $settings['heading_borderwidth-right'] }}px;
	@else
	  border-right-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-top']))
	  border-top-width: {{ $settings['heading_borderwidth-top'] }}px;
	@else
	  border-top-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-bottom']))
	  border-bottom-width: {{ $settings['heading_borderwidth-bottom'] }}px;
	@else
	  border-bottom-width: 0;
	@endif
	@if(!empty($settings['heading_borderradius-left-top']))
	  border-top-left-radius: {{ $settings['heading_borderradius-left-top'] }}px;
	@else
	  border-top-left-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-left-bottom']))
	  border-bottom-left-radius: {{ $settings['heading_borderradius-left-bottom'] }}px;
	@else
	  border-bottom-left-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-right-top']))
	  border-top-right-radius: {{ $settings['heading_borderradius-right-top'] }}px;
	@else
	  border-top-right-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-right-bottom']))
	  border-bottom-right-radius: {{ $settings['heading_borderradius-right-bottom'] }}px;
	@else
	  border-bottom-right-radius: 0;
	@endif
	@if(!empty($settings['heading_bordercolor-left']))
	  border-left-color: {{ $settings['heading_bordercolor-left'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-right']))
	  border-right-color: {{ $settings['heading_bordercolor-right'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-top']))
	  border-top-color: {{ $settings['heading_bordercolor-top'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-bottom']))
	  border-bottom-color: {{ $settings['heading_bordercolor-bottom'] }};
	@endif

	@if(!empty($settings['heading_borderstyle']))
	  border-style: {{ $settings['heading_borderstyle'] }};
	@endif

	@if(!empty($settings['icon_position']) && $settings['icon_position']=='left')
				@if(!empty($settings['heading_padding-left']))
				  padding-left: {{ $settings['heading_padding-left']+$settings['icon_size']+10 }}px;
				@else
					padding-left: {{ $settings['icon_size']+10 }}px;
				@endif
	@endif
	@if(!empty($settings['icon_position']) && $settings['icon_position']=='right')
		@if(!empty($settings['heading_padding-left']))
			padding-right: {{ $settings['heading_padding-left']+$settings['icon_size']+10 }}px;
		@else
			padding-right: {{ $settings['icon_size']+10 }}px;
		@endif
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .card>.card-header.active{
	@if(!empty($settings['heading_bgcolor_hover']))
	  background-color: {{ $settings['heading_bgcolor_hover'] }};
	@endif
	@if(!empty($settings['heading_borderwidth-left_hover']))
	  border-left-width: {{ $settings['heading_borderwidth-left_hover'] }}px;
	@else
	  border-left-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-right_hover']))
	  border-right-width: {{ $settings['heading_borderwidth-right_hover'] }}px;
	@else
	  border-right-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-top_hover']))
	  border-top-width: {{ $settings['heading_borderwidth-top_hover'] }}px;
	@else
	  border-top-width: 0;
	@endif
	@if(!empty($settings['heading_borderwidth-bottom_hover']))
	  border-bottom-width: {{ $settings['heading_borderwidth-bottom_hover'] }}px;
	@else
	  border-bottom-width: 0;
	@endif
	@if(!empty($settings['heading_borderradius-left-top_hover']))
	  border-top-left-radius: {{ $settings['heading_borderradius-left-top_hover'] }}px;
	@else
	  border-top-left-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-left-bottom_hover']))
	  border-bottom-left-radius: {{ $settings['heading_borderradius-left-bottom_hover'] }}px;
	@else
	  border-bottom-left-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-right-top_hover']))
	  border-top-right-radius: {{ $settings['heading_borderradius-right-top_hover'] }}px;
	@else
	  border-top-right-radius: 0;
	@endif
	@if(!empty($settings['heading_borderradius-right-bottom_hover']))
	  border-bottom-right-radius: {{ $settings['heading_borderradius-right-bottom_hover'] }}px;
	@else
	  border-bottom-right-radius: 0;
	@endif
	@if(!empty($settings['heading_bordercolor-left_hover']))
	  border-left-color: {{ $settings['heading_bordercolor-left_hover'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-right_hover']))
	  border-right-color: {{ $settings['heading_bordercolor-right_hover'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-top_hover']))
	  border-top-color: {{ $settings['heading_bordercolor-top_hover'] }};
	@endif
	@if(!empty($settings['heading_bordercolor-bottom_hover']))
	  border-bottom-color: {{ $settings['heading_bordercolor-bottom_hover'] }};
	@endif
	@if(!empty($settings['heading_borderstyle_hover']))
	  border-style: {{ $settings['heading_borderstyle_hover'] }};
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .card>.card-header>h4>a{
	@if(!empty($settings['heading_font']))
	  font-size: {{ $settings['heading_font'] }}px;
	@endif
	@if(!empty($settings['heading_font_family']))
	  font-family: {{ $settings['heading_font_family'] }};
	@endif
	@if(!empty($settings['heading_font_weight']))
	  font-weight: {{ $settings['heading_font_weight'] }};
	@endif
	@if(!empty($settings['heading_padding-left']))
	  padding-left: {{ $settings['heading_padding-left'] }}px;
	@endif
	@if(!empty($settings['heading_padding-right']))
	  padding-right: {{ $settings['heading_padding-right'] }}px;
	@endif
	@if(!empty($settings['heading_padding-top']))
	  padding-top: {{ $settings['heading_padding-top'] }}px;
	@endif
	@if(!empty($settings['heading_padding-bottom']))
	  padding-bottom: {{ $settings['heading_padding-bottom'] }}px;
	@endif
	@if(!empty($settings['heading_txcolor']))
	  color: {{ $settings['heading_txcolor'] }};
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .card>.card-header.active>h4>a{
	@if(!empty($settings['heading_font_weight_hover']))
	  font-weight: {{ $settings['heading_font_weight_hover'] }};
	@endif
	@if(!empty($settings['heading_txcolor_hover']))
	  color: {{ $settings['heading_txcolor_hover'] }};
	@endif
	@if(!empty($settings['heading_font_hover']))
	  font-size: {{ $settings['heading_font_hover'] }}px;
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .card .card-body{
	@if(!empty($settings['content_font_family']))
	  font-family: {{ $settings['content_font_family'] }};
	@endif
	@if(!empty($settings['content_font_weight']))
	  font-weight: {{ $settings['content_font_weight'] }};
	@endif
	@if(!empty($settings['content_padding-left']))
	  padding-left: {{ $settings['content_padding-left'] }}px;
	@endif
	@if(!empty($settings['content_padding-right']))
	  padding-right: {{ $settings['content_padding-right'] }}px;
	@endif
	@if(!empty($settings['content_padding-top']))
	  padding-top: {{ $settings['content_padding-top'] }}px;
	@endif
	@if(!empty($settings['content_padding-bottom']))
	  padding-bottom: {{ $settings['content_padding-bottom'] }}px;
	@endif
	@if(!empty($settings['content_txcolor']))
	  color: {{ $settings['content_txcolor'] }};
	@endif
	@if(!empty($settings['content_bgcolor']))
	  background-color: {{ $settings['content_bgcolor'] }};
	@endif
	@if(!empty($settings['content_borderwidth-left']))
	  border-left-width: {{ $settings['content_borderwidth-left'] }}px;
	@else
	  border-left-width: 0;
	@endif
	@if(!empty($settings['content_borderwidth-right']))
	  border-right-width: {{ $settings['content_borderwidth-right'] }}px;
	@else
	  border-right-width: 0;
	@endif
	@if(!empty($settings['content_borderwidth-top']))
	  border-top-width: {{ $settings['content_borderwidth-top'] }}px;
	@else
	  border-top-width: 0;
	@endif
	@if(!empty($settings['content_borderwidth-bottom']))
	  border-bottom-width: {{ $settings['content_borderwidth-bottom'] }}px;
	@else
	  border-bottom-width: 0;
	@endif
	@if(!empty($settings['content_borderradius-left-top']))
	  border-top-left-radius: {{ $settings['content_borderradius-left-top'] }}px;
	@else
	  border-top-left-radius: 0;
	@endif
	@if(!empty($settings['content_borderradius-left-bottom']))
	  border-bottom-left-radius: {{ $settings['content_borderradius-left-bottom'] }}px;
	@else
	  border-bottom-left-radius: 0;
	@endif
	@if(!empty($settings['content_borderradius-right-top']))
	  border-top-right-radius: {{ $settings['content_borderradius-right-top'] }}px;
	@else
	  border-top-right-radius: 0;
	@endif
	@if(!empty($settings['content_borderradius-right-bottom']))
	  border-bottom-right-radius: {{ $settings['content_borderradius-right-bottom'] }}px;
	@else
	  border-bottom-right-radius: 0;
	@endif
	@if(!empty($settings['content_bordercolor-left']))
	  border-left-color: {{ $settings['content_bordercolor-left'] }};
	@endif
	@if(!empty($settings['content_bordercolor-right']))
	  border-right-color: {{ $settings['content_bordercolor-right'] }};
	@endif
	@if(!empty($settings['content_bordercolor-top']))
	  border-top-color: {{ $settings['content_bordercolor-top'] }};
	@endif
	@if(!empty($settings['content_bordercolor-bottom']))
	  border-bottom-color: {{ $settings['content_bordercolor-bottom'] }};
	@endif
	@if(!empty($settings['content_font']))
	  font-size: {{ $settings['content_font'] }}px;
	@endif
	@if(!empty($settings['content_borderstyle']))
	  border-style: {{ $settings['content_borderstyle'] }};
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .card .card-body *{
	@if(!empty($settings['content_font_family']))
	  font-family: {{ $settings['content_font_family'] }};
	@endif
	@if(!empty($settings['content_font_weight']))
	  font-weight: {{ $settings['content_font_weight'] }};
	@endif
}
.{{ $widget->type }}_{{ $widget->id }} .card .card-header .arrows{
		@if(!empty($settings['icon_size']))
			font-size: {{ $settings['icon_size'] }}px;
		@endif
		@if(!empty($settings['icon_color']))
			color: {{ $settings['icon_color'] }};
		@endif
}
.{{ $widget->type }}_{{ $widget->id }} .card .card-header.active .arrows{
	@if(!empty($settings['icon_color_active']))
			color: {{ $settings['icon_color_active'] }};
		@endif
}
.{{ $widget->type }}_{{ $widget->id }} .card{
	@if(!empty($settings['collapse_spacing-left']))
	  margin-left: {{ $settings['collapse_spacing-left'] }}px;
	@endif
	@if(!empty($settings['collapse_spacing-right']))
	  margin-right: {{ $settings['collapse_spacing-right'] }}px;
	@endif
	@if(!empty($settings['collapse_spacing-top']))
	  margin-top: {{ $settings['collapse_spacing-top'] }}px;
	@endif
	@if(!empty($settings['collapse_spacing-bottom']))
	  margin-bottom: {{ $settings['collapse_spacing-bottom'] }}px;
	@endif
}
</style>

@endif
	</div>

@endif
