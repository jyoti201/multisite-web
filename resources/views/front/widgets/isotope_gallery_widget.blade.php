@if(!empty($settings['title']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }} {{ !empty($settings['showtags'])?'hasfilter':'' }}" id="{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}">
	@if(!empty($settings['showtags']))
		<ul class="isotope-filter {{ !empty($settings['tagalign'])?$settings['tagalign']:'' }}">
			@if(!empty($settings['tag']) && sizeof($settings['tag'])>0)
					<li><a href="#" class="showall hidden" data-filter="*">Showall</a></li>
				@foreach($settings['tag'] as $key=>$val)
					<li><a href="#" data-filter=".{{ strtolower(preg_replace('/\s+/', '', $val)) }}">{{ $val }}</a></li>
				@endforeach
			@endif
		</ul>
	@endif
	<div class="grid-items row flex-wrap no-gutters">
	@foreach($settings['title'] as $key=>$val)
		<div class="grid grid-item col-{{ !empty($settings['width'][$key])?$settings['width'][$key]:'' }} {{ !empty($settings['imgtag'][$key])? str_replace(',',' ',strtolower(preg_replace('/\s+/', '', $settings['imgtag'][$key]))):'' }}
		@if(empty($settings['autowidth']))
		 col-sm-{{ !empty($settings['colperrow'])? 12/$settings['colperrow']:'3' }}
		@endif
		"  style="{{ !empty($settings['spacing-left'])? 'padding-left:'.$settings['spacing-left'].'px;':'' }}{{ !empty($settings['spacing-right'])? 'padding-right:'.$settings['spacing-right'].'px;':'' }}{{ !empty($settings['spacing-top'])? 'padding-top:'.$settings['spacing-top'].'px;':'' }}{{ !empty($settings['spacing-bottom'])? 'padding-bottom:'.$settings['spacing-bottom'].'px;':'' }}">
		@if(!empty($settings['beforetext'][$key]))
			<div class="beforetext {!! !empty($settings['hidebeforetext']) && $settings['hidebeforetext']=='on'? 'hidden':'' !!}">{!! $settings['beforetext'][$key] !!}</div>
		@endif
			<figure class="{{ empty($settings['image'][$key])?'no-image ':'' }}{{ !empty($settings['hoverstyle'])? 'effect-'.$settings['hoverstyle']:'' }}" style="
			{{ !empty($settings['backlightcolor'])?'background:'.$settings['backlightcolor'].';':'' }}
			{{ !empty($settings['border'])?'border:'.$settings['border'].'px solid;':'' }}
			{{ !empty($settings['textcolor'])?'color:'.$settings['textcolor'].';':'' }}
			{{ !empty($settings['bordercolor'])?'border-color:'.$settings['bordercolor'].';':'' }}
			{{ !empty($settings['borderradius'])?'border-radius:'.$settings['borderradius'].'px;overflow:hidden;':'' }}">
				@if(!empty($settings['image'][$key]))
				<img class="img" src="{{ $settings['image'][$key] }}">
				@endif
				<figcaption class="{!! !empty($settings['showtitle']) && $settings['showtitle']=='on'? 'showtitle':'' !!} {!! !empty($settings['showdescription']) && $settings['showdescription']=='on'? 'showdescription':'' !!} ">
					@if(!empty($settings['title'][$key]))
						<h2 class="{!! !empty($settings['hidetitle']) && $settings['hidetitle']=='on'? 'hidden':'' !!}">{!! $settings['title'][$key] !!}</h2>
					@endif
					@if(!empty($settings['description'][$key]))
						<div class="{!! !empty($settings['hidedescription']) && $settings['hidedescription']=='on'? 'hidden':'' !!}">{!! $settings['description'][$key] !!}</div>
					@endif

				</figcaption>
				@if(!empty($settings['lightbox']) && !empty($settings['image'][$key]))
					<a href="{{ $settings['image'][$key] }}" class="image-popup"  data-effect="mfp-zoom-in">&nbsp;</a>
				@elseif(!empty($settings['link'][$key]) && empty($settings['lightbox']))
					<a href="{{ $settings['link'][$key] }}" target="{{ $settings['linktarget'][$key] }}">&nbsp;</a>
				@endif
			</figure>
			@if(!empty($settings['aftertext'][$key]))
				<div class="aftertext {!! !empty($settings['hideaftertext']) && $settings['hideaftertext']=='on'? 'hidden':'' !!}">{!! $settings['aftertext'][$key] !!}</div>
			@endif
		</div>

	@endforeach
	</div>

	<script>
	var $grid = '';
	jQuery(document).ready(function($){
		$('#{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }} .grid-items').imagesLoaded().done( function( instance ) {
			setTimeout(function(){
				$grid = $('#{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }} .grid-items').isotope({
						itemSelector: '.grid-item',
						layoutMode: 'packery',
						percentPosition: true,
						masonry: {
							columnWidth: '.grid-item',
						}
					});
				$('#{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').addClass('loaded');

			 }, 500);
			 setTimeout(function(){
				$grid.isotope('layout');
				$('#{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }} .isotope-filter a.showall').trigger('click');
			}, 500);
			$('#{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }} .isotope-filter a').click(function(){
				$('#{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }} .isotope-filter a').removeClass('active');
				$(this).addClass('active');
			  var selector = $(this).attr('data-filter');
			  var $grid = $('#{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').isotope({
					  filter: selector,
						itemSelector: '.grid-item',
						layoutMode: 'packery',
						percentPosition: true,
						masonry: {
							columnWidth: '.grid-item',
						}
					});
			  return false;
			});

		});

	})
	</script>

	@if(!empty($settings['overlaycolor']))
	<style>
	.{{ $widget->type }}_{{ $widget->id }} .grid figure:before {
		content: "";
		background: {{ $settings['overlaycolor'] }};
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		z-index: 9;
		-webkit-transition: all 0.3s ease-in-out;
		-moz-transition: all 0.3s ease-in-out;
		-o-transition: all 0.3s ease-in-out;
		transition: all 0.3s ease-in-out;
		@if(!empty($settings['overlayonhover']))
			opacity:0;
		@endif
	}
	.{{ $widget->type }}_{{ $widget->id }} .isotope-filter li a{
		display: block;
		@if(!empty($settings['tagbgcolor']))
		  background-color: {{ $settings['tagbgcolor'] }};
		@endif
		@if(!empty($settings['tagtxcolor']))
		  color: {{ $settings['tagtxcolor'] }};
		@endif
		@if(!empty($settings['tagborderwidth-left']))
		  border-left-width: {{ $settings['tagborderwidth-left'] }}px;
		@else
		  border-left-width: 0;
		@endif
		@if(!empty($settings['tagborderwidth-right']))
		  border-right-width: {{ $settings['tagborderwidth-right'] }}px;
		@else
		  border-right-width: 0;
		@endif
		@if(!empty($settings['tagborderwidth-top']))
		  border-top-width: {{ $settings['tagborderwidth-top'] }}px;
		@else
		  border-top-width: 0;
		@endif
		@if(!empty($settings['tagborderwidth-bottom']))
		  border-bottom-width: {{ $settings['tagborderwidth-bottom'] }}px;
		@else
		  border-bottom-width: 0;
		@endif
		@if(!empty($settings['tagborderradius-left-top']))
		  border-top-left-radius: {{ $settings['tagborderradius-left-top'] }}px;
		@else
		  border-top-left-radius: 0;
		@endif
		@if(!empty($settings['tagborderradius-left-bottom']))
		  border-bottom-left-radius: {{ $settings['tagborderradius-left-bottom'] }}px;
		@else
		  border-bottom-left-radius: 0;
		@endif
		@if(!empty($settings['tagborderradius-right-top']))
		  border-top-right-radius: {{ $settings['tagborderradius-right-top'] }}px;
		@else
		  border-top-right-radius: 0;
		@endif
		@if(!empty($settings['tagborderradius-right-bottom']))
		  border-bottom-right-radius: {{ $settings['tagborderradius-right-bottom'] }}px;
		@else
		  border-bottom-right-radius: 0;
		@endif
		@if(!empty($settings['tagbordercolor-left']))
		  border-left-color: {{ $settings['tagbordercolor-left'] }};
		@endif
		@if(!empty($settings['tagbordercolor-right']))
		  border-right-color: {{ $settings['tagbordercolor-right'] }};
		@endif
		@if(!empty($settings['tagbordercolor-top']))
		  border-top-color: {{ $settings['tagbordercolor-top'] }};
		@endif
		@if(!empty($settings['tagbordercolor-bottom']))
		  border-bottom-color: {{ $settings['tagbordercolor-bottom'] }};
		@endif
		@if(!empty($settings['tagpadding-left']))
		  padding-left: {{ $settings['tagpadding-left'] }}px;
		@endif
		@if(!empty($settings['tagpadding-right']))
		  padding-right: {{ $settings['tagpadding-right'] }}px;
		@endif
		@if(!empty($settings['tagpadding-top']))
		  padding-top: {{ $settings['tagpadding-top'] }}px;
		@endif
		@if(!empty($settings['tagpadding-bottom']))
		  padding-bottom: {{ $settings['tagpadding-bottom'] }}px;
		@endif
		@if(!empty($settings['tagmargin-left']))
		  margin-left: {{ $settings['tagmargin-left'] }}px;
		@endif
		@if(!empty($settings['tagmargin-right']))
		  margin-right: {{ $settings['tagmargin-right'] }}px;
		@endif
		@if(!empty($settings['tagmargin-top']))
		  margin-top: {{ $settings['tagmargin-top'] }}px;
		@endif
		@if(!empty($settings['tagmargin-bottom']))
		  margin-bottom: {{ $settings['tagmargin-bottom'] }}px;
		@endif
		@if(!empty($settings['tagfont']))
		  font-size: {{ $settings['tagfont'] }}px;
		@endif
		@if(!empty($settings['tagborderstyle']))
		  border-style: {{ $settings['tagborderstyle'] }};
		@endif
		@if(!empty($settings['tagfont_family']))
		  font-family: {{ $settings['tagfont_family'] }};
		@endif
		height: auto;
		@if(!empty($settings['tagwidth']))
		  width: {{ $settings['tagwidth'] }};
		else:
		  width: auto;
		@endif
		cursor: pointer;
	}
	.{{ $widget->type }}_{{ $widget->id }} .isotope-filter li a:hover,.{{ $widget->type }}_{{ $widget->id }} .isotope-filter li a.active{
		opacity: .6;
	}
	@if(!empty($settings['overlayonhover']))
		.{{ $widget->type }}_{{ $widget->id }} .grid figure:hover:before{
			opacity:1;
		}
	@else
		.{{ $widget->type }}_{{ $widget->id }} .grid figure:hover:before{
			opacity:0;
		}
	@endif

	</style>
	@endif

	@if(!empty($settings['backlightcolor']))
	<style>
	.{{ $widget->type }}_{{ $widget->id }}:after{
		background: {{ $settings['backlightcolor'] }};
	}
	</style>
	@endif

	@if(!empty($settings['customwidth']))
	<style>
	@foreach($settings['image'] as $key=>$val)
		.col-{{ !empty($settings['width'][$key])?$settings['width'][$key]:'' }}{
			width: {{ !empty($settings['width'][$key])?$settings['width'][$key]:'' }}%;
		}
	@endforeach
	</style>
	@endif
	@if(!empty($settings['lightbox']))
	<script>
	 jQuery(document).ready(function($) {
			$('.{{ $widget->type }}_{{ $widget->id }} .image-popup').magnificPopup({
				type: 'image',
				removalDelay: 500,
				callbacks: {
					beforeOpen: function() {
						 this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
						 this.st.mainClass = this.st.el.attr('data-effect');
					}
				},
				fixedContentPos: false,
				fixedBgPos: true,
				closeOnContentClick: true,
				midClick: true,
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0,1]
				}
			});
		});
	</script>
	@endif
	</div>
@endif
