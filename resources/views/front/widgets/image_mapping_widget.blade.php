@if(!empty($settings['code']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<div id="image-map-pro-container{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}"></div>
	</div>

    <script>
        ;(function ($, window, document, undefined) {
            $(document).ready(function() {
			$('#image-map-pro-container{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').imageMapPro({!! $settings['code'] !!});
			 });
        })(jQuery, window, document);
    </script>
@endif
