@if(!empty($settings['html']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		{!! $settings['html'] !!}
	</div>
	<script>
	jQuery(document).ready(function(){
		jQuery('[contenteditable="true"]').each(function(){
			$(this).removeAttr('contenteditable');
		})
	})
	</script>
@endif
