@if(!empty($settings['title']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<div class="slick-carousel {{ !empty($settings['arrowposition'])? $settings['arrowposition']:'' }}" id="slick_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}">
			@foreach($settings['title'] as $key=>$val)
				<div class="item">
					<div class="grid" style="{{ !empty($settings['margin-left'])? 'margin-left:'.$settings['margin-left'].'px;':'' }}{{ !empty($settings['margin-right'])? 'margin-right:'.$settings['margin-right'].'px;':'' }}{{ !empty($settings['margin-top'])? 'margin-top:'.$settings['margin-top'].'px;':'' }}{{ !empty($settings['margin-bottom'])? 'margin-bottom:'.$settings['margin-bottom'].'px;':'' }}">
						@if(!empty($settings['beforetext'][$key]))
							<div class="beforetext {!! !empty($settings['hidebeforetext']) && $settings['hidebeforetext']=='on'? 'hidden':'' !!}">{!! $settings['beforetext'][$key] !!}</div>
						@endif
						<figure class="{{ empty($settings['image'][$key])?'no-image ':'' }}{{ !empty($settings['hoverstyle'])? 'effect-'.$settings['hoverstyle']:'' }}" style="
						{{ !empty($settings['backlightcolor'])?'background:'.$settings['backlightcolor'].';':'' }}
						{{ !empty($settings['border'])?'border:'.$settings['border'].'px solid;':'' }}
						{{ !empty($settings['textcolor'])?'color:'.$settings['textcolor'].';':'' }}
						{{ !empty($settings['bordercolor'])?'border-color:'.$settings['bordercolor'].';':'' }}
						{{ !empty($settings['borderradius'])?'border-radius:'.$settings['borderradius'].'px;overflow:hidden;':'' }}">
							@if(!empty($settings['image'][$key]))
							<img class="img" src='{{ $settings['image'][$key] }}' />
							@endif
							<figcaption class="{!! !empty($settings['showtitle']) && $settings['showtitle']=='on'? 'showtitle':'' !!} {!! !empty($settings['showdescription']) && $settings['showdescription']=='on'? 'showdescription':'' !!} ">
								@if(!empty($settings['title'][$key]))
									<h2 class="{!! !empty($settings['hidetitle']) && $settings['hidetitle']=='on'? 'hidden':'' !!}">{!! $settings['title'][$key] !!}</h2>
								@endif
								@if(!empty($settings['description'][$key]))
									<div class="{!! !empty($settings['hidedescription']) && $settings['hidedescription']=='on'? 'hidden':'' !!}">{!! $settings['description'][$key] !!}</div>
								@endif

							</figcaption>
							@if(!empty($settings['lightbox']) && !empty($settings['image'][$key]))
								<a href="{{ $settings['image'][$key] }}" class="image-popup" data-effect="mfp-zoom-in">&nbsp;</a>
							@elseif(!empty($settings['link'][$key]) && empty($settings['lightbox']))
								<a href="{{ $settings['link'][$key] }}" target="{{ $settings['linktarget'][$key] }}">&nbsp;</a>
							@endif
						</figure>
						@if(!empty($settings['aftertext'][$key]))
							<div class="aftertext {!! !empty($settings['hideaftertext']) && $settings['hideaftertext']=='on'? 'hidden':'' !!}">{!! $settings['aftertext'][$key] !!}</div>
						@endif
					</div>
				</div>
			@endforeach
		</div>

		<div id="slickarrows_{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}" class="slickarrows"></div>

		<div id="slickdots_{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}" class="slickdots"></div>


		@if(!empty($settings['overlaycolor']))
		<style>
		.{{ $widget->type }}_{{ $widget->id }} .grid figure:before {
			content: "";
			background: {{ $settings['overlaycolor'] }};
			position: absolute;
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
			z-index: 9;
			-webkit-transition: all 0.3s ease-in-out;
			-moz-transition: all 0.3s ease-in-out;
			-o-transition: all 0.3s ease-in-out;
			transition: all 0.3s ease-in-out;
			@if(!empty($settings['overlayonhover']))
				opacity:0;
			@endif
		}
		@if(!empty($settings['overlayonhover']))
			.{{ $widget->type }}_{{ $widget->id }} .grid figure:hover:before{
				opacity:1;
			}
		@else
			.{{ $widget->type }}_{{ $widget->id }} .grid figure:hover:before{
				opacity:0;
			}
		@endif

		</style>
		@endif
		@if(!empty($settings['lightbox']))
		<script>
		 jQuery(document).ready(function($) {
				$('.{{ $widget->type }}_{{ $widget->id }} .image-popup').magnificPopup({
					type: 'image',
					removalDelay: 500,
					callbacks: {
						beforeOpen: function() {
							 this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
							 this.st.mainClass = this.st.el.attr('data-effect');
						}
					},
					fixedContentPos: false,
					fixedBgPos: true,
					closeOnContentClick: true,
					midClick: true,
					gallery: {
						enabled: true,
						navigateByImgClick: true,
						preload: [0,1]
					}
				});
			});
		</script>
		@endif

        <script type="text/javascript">
            jQuery(document).ready(function($) {
								var slidesToShow = {{ !empty($settings['items'])?$settings['items']:'1' }};
								var slidesToScroll = {{ !empty($settings['items'])?$settings['items']:'1' }};

                $('#slick_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').slick({
									appendArrows: $('#slickarrows_{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}'),
									appendDots: $('#slickdots_{{ $widget->type }}_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}'),
									@if(!empty($settings['dots']))
									dots: {{ $settings['dots'] }},
									@endif
									@if(!empty($settings['loop']))
									infinite: {{ $settings['loop'] }},
									@endif
									speed: 300,
									slidesToShow: slidesToShow,
									slidesToScroll: slidesToScroll,
									@if(!empty($settings['nav']))
									arrows: {{ $settings['nav'] }},
									@endif
									@if(!empty($settings['nav']) && $settings['nav']=='true')
											prevArrow: '<button type="button" class="slick-prev">{!! !empty($settings['arrow_style_prev'])?$settings['arrow_style_prev']:'' !!}</button>',
											nextArrow: '<button type="button" class="slick-next">{!! !empty($settings['arrow_style_next'])?$settings['arrow_style_next']:'' !!}</button>',
									@endif
									@if(!empty($settings['autoplay']))
									autoplay: {{ $settings['autoplay'] }},
									@endif
									@if(!empty($settings['adaptiveHeight']))
									adaptiveHeight: {{ $settings['adaptiveHeight'] }},
									@endif
									@if(!empty($settings['autoplay']) && $settings['autoplay']=='true')
										@if(!empty($settings['autoplayTimeout']))
											autoplaySpeed: {{ $settings['autoplayTimeout'] }},
										@endif
										@if(!empty($settings['autoplayHoverPause']))
											pauseOnHover: {{ $settings['autoplayHoverPause'] }},
											pauseOnDotsHover: {{ $settings['autoplayHoverPause'] }},
										@endif
									@endif
									@if(!empty($settings['centerMode']))
										centerMode: {{ $settings['centerMode'] }},
									@endif
									@if(!empty($settings['centerMode']) && $settings['centerMode']=='true')
										@if(!empty($settings['centerPadding']))
												centerPadding: '{{ $settings['centerPadding'] }}px',
										@endif
									@endif
									@if(!empty($settings['initialSlide']))
										initialSlide: {{ $settings['initialSlide'] }},
									@endif
									@if(!empty($settings['speed']))
										speed: {{ $settings['speed'] }},
									@endif
									focusOnSelect: true,
									@if(!empty($settings['items']) && $settings['items']>1)
									responsive: [
									 {
										 breakpoint: 992,
										 settings: {
											 slidesToShow: slidesToShow,
											 slidesToScroll: slidesToScroll,
											 infinite: true,
											 dots: true
										 }
									 },
									 {
										 breakpoint: 767,
										 settings: {
											 slidesToShow: 2,
											 slidesToScroll: slidesToScroll,
											 @if(!empty($settings['centerMode']) && $settings['centerMode']=='true')
		 										@if(!empty($settings['centerPadding']))
		 												centerPadding: '0px',
		 										@endif
		 									@endif
										 }
									 },
									 {
										 breakpoint: 480,
										 settings: {
											 slidesToShow: 1,
											 slidesToScroll: 1,
											 @if(!empty($settings['centerMode']) && $settings['centerMode']=='true')
		 										@if(!empty($settings['centerPadding']))
		 												centerPadding: '0px',
		 										@endif
		 									@endif
										 }
									 }
									]
									@endif
								})
            });
        </script>
	</div>
@endif
