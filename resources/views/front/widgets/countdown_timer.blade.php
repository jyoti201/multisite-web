@if(!empty($settings['html']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		{!! $settings['html'] !!}
	</div>
	<script>
	$(document).ready(function() {
		$("#counter-{{ $widget->id }}").soon().create();
	});
	</script>

@endif
