@if(!empty($settings['image']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<div class="owl-carousel owl-theme {{ !empty($settings['arrowstyle'])? $settings['arrowstyle']:'' }}" id="owl_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}">
			@foreach($settings['image'] as $key=>$val)
				<div class="item">
					<div class="grid">
						<figure class="{{ !empty($settings['hoverstyle'])? 'effect-'.$settings['hoverstyle']:'' }}" style="
						{{ !empty($settings['overlaycolor'])?'background:'.$settings['overlaycolor'].';':'' }}
						{{ !empty($settings['border'])?'border:'.$settings['border'].'px solid;':'' }}
						{{ !empty($settings['textcolor'])?'color:'.$settings['textcolor'].';':'' }}
						{{ !empty($settings['bordercolor'])?'border-color:'.$settings['bordercolor'].';':'' }}
						{{ !empty($settings['borderradius'])?'border-radius:'.$settings['borderradius'].'px;overflow:hidden;':'' }}">
							<img class="img" src='{{ $val }}' />
							<figcaption>
								<div data-animation="{{ !empty($settings['animation'][$key])?'animated '.$settings['animation'][$key]:'' }} fast" class="figcaption {{ !empty($settings['animation'][$key])?'animated '.$settings['animation'][$key]:'' }} fast">
								@if(!empty($settings['title'][$key]))
									<h2>{!! $settings['title'][$key] !!}</h2>
								@endif
								@if(!empty($settings['description'][$key]))
									<p>{!! $settings['description'][$key] !!}</p>
								@endif
							  </div>
								@if(!empty($settings['lightbox']))
									<a href="{{ $val }}" class="image-popup">View more</a>
								@elseif(!empty($settings['link'][$key]) && empty($settings['lightbox']))
									<a href="{{ $settings['link'][$key] }}" target="{{ $settings['linktarget'][$key] }}">View more</a>
								@endif
							</figcaption>
						</figure>
					</div>
				</div>
			@endforeach
		</div>
		@if(!empty($settings['lightbox']))
		<script>
		 jQuery(document).ready(function($) {
				$('.image-popup').magnificPopup({
					type: 'image',
					closeOnContentClick: true,
					mainClass: 'mfp-fade',
					gallery: {
						enabled: true,
						navigateByImgClick: true,
						preload: [0,1] // Will preload 0 - before current, and 1 after the current image
					}
				});
			});
		</script>
		@endif

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('#owl_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').owlCarousel({
				    loop:{{ !empty($settings['loop'])? $settings['loop']:'false' }},
					center: true,
				    margin:0,
					rewind: {{ !empty($settings['rewind'])? $settings['rewind']:'false' }},
					autoplayTimeout: {{ !empty($settings['autoplayTimeout'])? $settings['autoplayTimeout']:'5000' }},
					autoplayHoverPause: {{ !empty($settings['autoplayHoverPause'])? $settings['autoplayHoverPause']:'false' }},
					smartSpeed: {{ !empty($settings['smartspeed'])? $settings['smartspeed']:'300' }},
				    nav:{{ !empty($settings['nav'])? $settings['nav']:'false' }},
				    autoplay:{{ !empty($settings['autoplay'])? $settings['autoplay']:'false' }},
					navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
					dots : {{ !empty($settings['dots'])? $settings['dots']:'false' }},
					autoHeight:{{ !empty($settings['autoheight'])? $settings['autoheight']:'false' }},
					{!! !empty($settings['animateout'])? 'animateOut: "'.$settings['animateout'].'",':'' !!}
					{!! !empty($settings['animatein'])? 'animateIn: "'.$settings['animatein'].'",':'' !!}
				    responsive:{
				        0:{
				            items:1
				        },
				        480:{
				            items:1
				        },
				        700:{
				            items:1
				        },
				        1000:{
				            items:1
				        },
				        1100:{
				            items:1
				        }
				    }
				})
				$('#owl_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').on('change.owl.carousel', function(event) {
						$(this).find('.figcaption').each(function(){
							$(this).attr('class', 'figcaption');
							$(this).hide();
						})
				})
				$('#owl_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}').on('translated.owl.carousel', function(event) {
						$(this).find('.owl-item:not(.active)').each(function(){
							$(this).attr('class', 'owl-item');
							$(this).css('left', '0');
						})
						$(this).find('.figcaption').each(function(){
							$(this).fadeIn();
							var classes = $(this).attr('data-animation');
							$(this).addClass(classes);
						})
				})
            });
        </script>
	</div>
@endif
