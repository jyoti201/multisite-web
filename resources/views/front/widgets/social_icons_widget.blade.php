@if(!empty($settings['iconstyle']) && $settings['iconstyle']=='custom')
<style>
.{{ $widget->type }}_{{ $widget->id }} .social .fa{
	background: {{ !empty($settings['bgcolor'])? $settings['bgcolor']:'' }};
  color: {{ !empty($settings['iconcolor'])? $settings['iconcolor']:'' }};
	border: {{ !empty($settings['border'])? $settings['border']:'' }}px solid {{ !empty($settings['bordercolor'])? $settings['bordercolor']:'' }};
	border-radius: {{ !empty($settings['borderradius'])? $settings['borderradius'].'px':'' }};
}
</style>
@endif
@if(!empty($settings['border']))
<style>
.{{ $widget->type }}_{{ $widget->id }} .social .fa{
	border: {{ !empty($settings['border'])? $settings['border']:'' }}px solid {{ !empty($settings['bordercolor'])? $settings['bordercolor']:'' }};
}
</style>
@endif
@if(!empty($settings['borderradius']))
<style>
.{{ $widget->type }}_{{ $widget->id }} .social .fa{
	border-radius: {{ !empty($settings['borderradius'])? $settings['borderradius'].'px':'' }};
}
</style>
@endif

@if(!empty($settings['height']) && !empty($settings['width']))
<style>
.{{ $widget->type }}_{{ $widget->id }} .social .fa{
	height: {{ !empty($settings['height'])? $settings['height'].'px':'' }};
	width: {{ !empty($settings['width'])? $settings['width'].'px':'' }};
	line-height: {{ !empty($settings['height'])? $settings['height'].'px':'' }};
}
</style>
@endif

<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
	<ul class="social {{ !empty($settings['dimension']) ? $settings['dimension']:'horizontal' }}">
		@if(!empty($settings['icon']) && sizeof($settings['icon'])>0)
			@foreach($settings['icon'] as $key=>$val)
				<li><a href="{{ !empty($settings['link'][$key]) ? $settings['link'][$key] : '' }}" class="socialicon {{ $val }} {{ !empty($settings['iconsize']) ? $settings['iconsize'] : '' }}" target="_blank"></a></li>
			@endforeach
		@endif
	</ul>
</div>
