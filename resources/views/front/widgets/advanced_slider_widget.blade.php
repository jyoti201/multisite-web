@if(!empty($settings['slideorder']))
	<div class="widget {{ $widget->type }} {{ $widget->type }}_{{ $widget->id }}">
		<div id="rev_slider_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" style="margin:0px auto;background-color:{!! !empty($settings['sliderbgcolor'])? $settings['sliderbgcolor']:'rgba(255,255,255,0)' !!};padding:0px;margin-top:0px;margin-bottom:0px;">
			<div id="rev_slider_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
				<ul>
					@foreach($settings['slideorder'] as $key=>$slide)
						@if(empty($settings['slide_'.$slide]['disabled']))
							<li data-index="rs-{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}{{ $slide }}" data-transition="{{ !empty($settings['slide_'.$slide]['slide_transitions'])?$settings['slide_'.$slide]['slide_transitions']:'fade' }}" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="default"  data-thumb=""  data-rotate="0"
							@if($slide==1)
								data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
							@endif
							@if(!empty($settings['slide_'.$slide]['filter']))
								 data-mediafilter="{!! $settings['slide_'.$slide]['filter'] !!}"
							@endif
							data-saveperformance="off"  data-title="Letters Fly In" data-description="">
								@if(!empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='bgimage')
									<img src="{!! !empty($settings['slide_'.$slide]['slider_bgimage'])? $settings['slide_'.$slide]['slider_bgimage']:'' !!}"  alt=""  data-bgposition="{!! !empty($settings['slide_'.$slide]['bg_position'])? $settings['slide_'.$slide]['bg_position']:'' !!}" data-bgfit="{!! !empty($settings['slide_'.$slide]['bg_fit'])? $settings['slide_'.$slide]['bg_fit']:'' !!}" data-bgrepeat="{!! !empty($settings['slide_'.$slide]['bg_repeat'])? $settings['slide_'.$slide]['bg_repeat']:'' !!}" data-bgparallax="10" class="rev-slidebg"
									@if(!empty($settings['slide_'.$slide]['kenburn']) && $settings['slide_'.$slide]['kenburn']=='on')
										data-kenburns="on" data-duration="{{ !empty($settings['slide_'.$slide]['kenburn_duration'])?$settings['slide_'.$slide]['kenburn_duration']:'10000' }}" data-ease="{{ !empty($settings['slide_'.$slide]['kenburn_easing'])?$settings['slide_'.$slide]['kenburn_easing']:'10000' }}" data-scalestart="{{ !empty($settings['slide_'.$slide]['kenburn_scale_from'])?$settings['slide_'.$slide]['kenburn_scale_from']:'0' }}" data-scaleend="{{ !empty($settings['slide_'.$slide]['kenburn_scale_to'])?$settings['slide_'.$slide]['kenburn_scale_to']:'0' }}" data-rotatestart="{{ !empty($settings['slide_'.$slide]['kenburn_rotation_from'])?$settings['slide_'.$slide]['kenburn_rotation_from']:'0' }}" data-rotateend="{{ !empty($settings['slide_'.$slide]['kenburn_rotation_to'])?$settings['slide_'.$slide]['kenburn_rotation_to']:'0' }}" data-blurstart="{{ !empty($settings['slide_'.$slide]['kenburn_blur_from'])?$settings['slide_'.$slide]['kenburn_blur_from']:'0' }}" data-blurend="{{ !empty($settings['slide_'.$slide]['kenburn_blur_to'])?$settings['slide_'.$slide]['kenburn_blur_to']:'0' }}" data-offsetstart="{{ !empty($settings['slide_'.$slide]['kenburn_h_offset_from'])?$settings['slide_'.$slide]['kenburn_h_offset_from']:'0' }} {{ !empty($settings['slide_'.$slide]['kenburn_v_offset_from'])?$settings['slide_'.$slide]['kenburn_v_offset_from']:'0' }}" data-offsetend="{{ !empty($settings['slide_'.$slide]['kenburn_h_offset_to'])?$settings['slide_'.$slide]['kenburn_h_offset_to']:'0' }} {{ !empty($settings['slide_'.$slide]['kenburn_v_offset_to'])?$settings['slide_'.$slide]['kenburn_v_offset_to']:'0' }}"
									@endif
									@if(!empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='overlay')
										data-overlaycolorfrom="{{ !empty($settings['slide_'.$slide]['bg_overlay_from'])?$settings['slide_'.$slide]['bg_overlay_from']:'' }}"
										data-overlaycolorto="{{ !empty($settings['slide_'.$slide]['bg_overlay_to'])?$settings['slide_'.$slide]['bg_overlay_to']:'' }}"
										data-overlaycolorangle="{{ !empty($settings['slide_'.$slide]['bg_overlay_angle'])?$settings['slide_'.$slide]['bg_overlay_angle']:'0' }}"
									@endif
									data-no-retina>
								@elseif(!empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='transparent')
									<img src="{{ asset('assets/public/images/transparent.png') }}" class="rev-slidebg">
								@elseif(!empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='colored')
									<img src="{{ asset('assets/public/images/transparent.png') }}" class="rev-slidebg"  data-bgcolor="{{ !empty($settings['slide_'.$slide]['slider_color'])?$settings['slide_'.$slide]['slider_color']:'' }}">
								@endif
								@if(!empty($settings['slide_'.$slide]['hidden_layer_id']))
									@foreach($settings['slide_'.$slide]['hidden_layer_id'] as $key=>$layer)
										<div {{ !empty($settings['slide_'.$slide]['hidden_layer_rel'][$key])? 'rel="'.$settings['slide_'.$slide]['hidden_layer_rel'][$key].'"':'' }}  {{ !empty($settings['slide_'.$slide]['hidden_layer_title'][$key])? 'alt="'.$settings['slide_'.$slide]['hidden_layer_title'][$key].'"':'' }} class="tp-caption NotGeneric-Title tp-resizeme {{ !empty($settings['slide_'.$slide]['hidden_layer_class'][$key])?$settings['slide_'.$slide]['hidden_layer_class'][$key]:'' }}"
											id="{{ !empty($settings['slide_'.$slide]['hidden_layer_id'][$key])?$settings['slide_'.$slide]['hidden_layer_id'][$key]:'' }}"
											data-x="left"
											data-hoffset="{{ !empty($settings['slide_'.$slide]['hidden_layer_hoffset'][$key])?$settings['slide_'.$slide]['hidden_layer_hoffset'][$key]:'' }}"
											data-y="top"
											@if(!empty($settings['slide_'.$slide]['hidden_layer_link'][$key]))
												data-actions='[{"event":"click", "action":"simplelink", "target":"{{ $settings['slide_'.$slide]['hidden_layer_target'][$key] }}", "url":"{{ addslashes($settings['slide_'.$slide]['hidden_layer_link'][$key]) }}"}]'
											@endif
											data-voffset="{{ !empty($settings['slide_'.$slide]['hidden_layer_voffset'][$key])?$settings['slide_'.$slide]['hidden_layer_voffset'][$key]:'' }}"
											data-width="['auto','auto','auto','auto']"
											data-height="['auto','auto','auto','auto']"
											data-type="{{ !empty($settings['slide_'.$slide]['hidden_layer_type'][$key])?$settings['slide_'.$slide]['hidden_layer_type'][$key]:'' }}"
											data-responsive_offset="on"
											data-frames='{{ !empty($settings['slide_'.$slide]['hidden_layer_animation'][$key])?str_replace(array('DELAY','SPEEDIN', 'SPEEDOUT'), array($settings['slide_'.$slide]['hidden_layer_start'][$key],!empty($settings['slide_'.$slide]['hidden_layer_animation_speed_in'][$key])?$settings['slide_'.$slide]['hidden_layer_animation_speed_in'][$key]:'300', !empty($settings['slide_'.$slide]['hidden_layer_animation_speed_out'][$key])?$settings['slide_'.$slide]['hidden_layer_animation_speed_out'][$key]:'300'), getOptions('layeranimation')[$settings['slide_'.$slide]['hidden_layer_animation'][$key]]['param']):getOptions('layeranimation')['noanimation']['param'] }}'
											data-paddingtop="[{{ !empty($settings['slide_'.$slide]['hidden_layer_ptop'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_ptop'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_ptop'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_ptop'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_ptop'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_ptop'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_ptop'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_ptop'][$key]):'0' }}]"
											data-paddingright="[{{ !empty($settings['slide_'.$slide]['hidden_layer_pright'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pright'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pright'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pright'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pright'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pright'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pright'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pright'][$key]):'0' }}]"
											data-paddingbottom="[{{ !empty($settings['slide_'.$slide]['hidden_layer_pbottom'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pbottom'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pbottom'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pbottom'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pbottom'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pbottom'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pbottom'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pbottom'][$key]):'0' }}]"
											data-paddingleft="[{{ !empty($settings['slide_'.$slide]['hidden_layer_pleft'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pleft'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pleft'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pleft'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pleft'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pleft'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pleft'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pleft'][$key]):'0' }}]"
											style="z-index: 5; white-space: nowrap; {{ !empty($settings['slide_'.$slide]['hidden_layer_fontsize'][$key])? 'font-size:'.$settings['slide_'.$slide]['hidden_layer_fontsize'][$key].';':'' }}
											{{ !empty($settings['slide_'.$slide]['hidden_layer_lineheight'][$key])? 'line-height:'.$settings['slide_'.$slide]['hidden_layer_lineheight'][$key].';':'' }}
											{{ !empty($settings['slide_'.$slide]['hidden_layer_color'][$key])? 'color:'.$settings['slide_'.$slide]['hidden_layer_color'][$key].';':'' }}
											{{ !empty($settings['slide_'.$slide]['hidden_layer_fontweight'][$key])? 'font-weight:'.$settings['slide_'.$slide]['hidden_layer_fontweight'][$key].';':'' }}
											{{ !empty($settings['slide_'.$slide]['hidden_layer_fontfamily'][$key])? 'font-family:"'.$settings['slide_'.$slide]['hidden_layer_fontfamily'][$key].'";':'' }}
											{{ !empty($settings['slide_'.$slide]['hidden_layer_bgcolor'][$key])? 'background-color:'.$settings['slide_'.$slide]['hidden_layer_bgcolor'][$key].';':'' }}
											">{!! !empty($settings['slide_'.$slide]['hidden_layer_text'][$key])?ImplementWidgets($settings['slide_'.$slide]['hidden_layer_text'][$key]):'' !!}

										</div>
									@endforeach
								@endif
							</li>
						@endif
					@endforeach
				</ul>
			</div>
		</div>
	</div>

<style>

</style>


	<script type="text/javascript">
	var tpj=$;
	var revapi116{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }};
	var revapi = tpj(document).ready(function() {
		if(tpj("#rev_slider_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}").revolution == undefined){
			revslider_showDoubleJqueryError("#rev_slider_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}");
		}else{
			revapi116{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }} = tpj("#rev_slider_{{ !empty($settings['uniqueid'])?$settings['uniqueid']:$widget->id }}").show().revolution({
				sliderType:"{{  getSliderSetting('sliderType', !empty($settings['sliderType'])? $settings['sliderType']:'') }}",
				jsFileLocation:"{{ asset('assets/public/js') }}/",
				sliderLayout:"{{  getSliderSetting('sliderLayout', !empty($settings['sliderLayout'])? $settings['sliderLayout']:'') }}",
				dottedOverlay:"{{ getSliderSetting('dottedoverlay', !empty($settings['dottedoverlay'])? $settings['dottedoverlay']:'') }}",
				delay:{{ getSliderSetting('delay', !empty($settings['delay'])? $settings['delay']:'') }},
				navigation: {
					keyboardNavigation:"off",
					keyboard_direction: "horizontal",
					mouseScrollNavigation:"off",
					onHoverStop:"off",
					touch:{
						touchenabled:"on",
						swipe_threshold: 75,
						swipe_min_touches: 1,
						swipe_direction: "horizontal",
						drag_block_vertical: false
					}
					,
					arrows: {
						style:"{{  getSliderSetting('arrow_style', !empty($settings['arrow_style'])? $settings['arrow_style']:'') }}",
						enable:{{ getSliderSetting('enablearrow', !empty($settings['enablearrow'])? $settings['enablearrow']:'') }},
						hide_onmobile:{{  getSliderSetting('hideonmobile', !empty($settings['hideonmobile'])? $settings['hideonmobile']:'') }},
						hide_under:{{ getSliderSetting('hideunder', !empty($settings['hideunder'])? $settings['hideunder']:'') }},
						hide_onleave:{{  getSliderSetting('hideonleave', !empty($settings['hideonleave'])? $settings['hideonleave']:'') }},
						hide_delay:200,
						hide_delay_mobile:1200,
						tmp:'',
						left: {
							h_align:"{{  getSliderSetting('l_halign', !empty($settings['l_halign'])? $settings['l_halign']:'') }}",
							v_align:"{{  getSliderSetting('l_valign', !empty($settings['l_valign'])? $settings['l_valign']:'') }}",
							h_offset:{{ getSliderSetting('l_horizoff', !empty($settings['l_horizoff'])? $settings['l_horizoff']:'') }},
							v_offset:{{ getSliderSetting('l_vertoff', !empty($settings['l_vertoff'])? $settings['l_vertoff']:'') }}
						},
						right: {
							h_align:"{{  getSliderSetting('r_halign', !empty($settings['r_halign'])? $settings['r_halign']:'') }}",
							v_align:"{{  getSliderSetting('r_valign', !empty($settings['r_valign'])? $settings['r_valign']:'') }}",
							h_offset:{{ getSliderSetting('r_horizoff', !empty($settings['r_horizoff'])? $settings['r_horizoff']:'') }},
							v_offset:{{ getSliderSetting('r_vertoff', !empty($settings['r_vertoff'])? $settings['r_vertoff']:'') }}
						}
					}
				},
				viewPort: {
					enable:true,
					outof:"pause",
					visible_area:"80%"
				},
				gridwidth:{{ getSliderSetting('gridwidth', !empty($settings['gridwidth'])? $settings['gridwidth']:'') }},
				gridheight:{{ getSliderSetting('gridheight', !empty($settings['gridheight'])? $settings['gridheight']:'') }},
				lazyType:"none",
				shadow:{{ getSliderSetting('shadow', !empty($settings['shadow'])? $settings['shadow']:'') }},
				spinner:"spinner{{ getSliderSetting('spinner', !empty($settings['spinner'])? $settings['spinner']:'') }}",
				stopLoop:"{{ getSliderSetting('stoploop', !empty($settings['stoploop'])? $settings['stoploop']:'') }}",
				stopAfterLoops:{{ getSliderSetting('stopafterloop', !empty($settings['stopafterloop'])? $settings['stopafterloop']:'') }},
				stopAtSlide:{{ getSliderSetting('stopatslide', !empty($settings['stopatslide'])? $settings['stopatslide']:'') }},
				shuffle:"off",
				autoHeight:"{{  getSliderSetting('autoheight', !empty($settings['autoheight'])? $settings['autoheight']:'') }}",
				hideThumbsOnMobile:"{{  getSliderSetting('hidethumbsonmobile', !empty($settings['hidethumbsonmobile'])? $settings['hidethumbsonmobile']:'') }}",
				hideSliderAtLimit:0,
				hideCaptionAtLimit:0,
				hideAllCaptionAtLilmit:0,
				debugMode:false,
				fallbacks: {
					simplifyAll:"off",
					nextSlideOnWindowFocus:"off",
					disableFocusListener:false,
				}
			});
		}

		revapi.one('revolution.slide.onloaded', function() {
			tpj('.tp-bgimg.overlay').each(function(){

				var s = tpj(this).data('overlaycolorfrom');
				var s2 = tpj(this).data('overlaycolorto');

				if(s!=s2){
				var deg = tpj(this).data('overlaycolorangle');
				tpj(this).html('<div class="overlayer" style="background:'+s+';background:-moz-linear-gradient('+deg+'deg, '+s+' 0%, '+s2+' 100%);background:-webkit-linear-gradient('+deg+'deg, '+s+' 0%, '+s2+' 100%);background:linear-gradient('+deg+'deg, '+s+' 0%, '+s2+' 100%);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="'+s+'",endColorstr="'+s2+'",GradientType=1)"></div>');
				}else{
				tpj(this).html('<div class="overlayer" style="background:'+s+';"></div>');
				}

			})
		})
	});	/*ready*/
	</script>
@endif
