@extends('layouts.login')

@section('content')


<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
	<div class=" card-box">
		<div class="panel-heading">
			<h3 class="text-center"> Sign Up to <strong class="text-custom">{{ config('app.name') }}</strong> </h3>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" method="POST" action="{{ route('register') }}">
				{{ csrf_field() }}
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
					<div class="col-xs-12">
						<input id="name" type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}" required autofocus>
						@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
					</div>
				</div>

				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					<div class="col-xs-12">
						<input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required>
						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>
				</div>

				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<div class="col-xs-12">
						<input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
						@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<div class="col-xs-12">
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
					</div>
				</div>
				<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
					<div class="col-xs-12">
						<select id="role" class="form-control" name="role" required>
							<option value="site-subscriber">Subscriber</option>
							<option value="site-editor">Editor</option>
							<option value="site-admin">Admin</option>
						</select>
						@if ($errors->has('role'))
							<span class="help-block">
								<strong>{{ $errors->first('role') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="form-group text-center m-t-40">
					<div class="col-xs-12">
						<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">
							Register
						</button>
					</div>
				</div>

			</form>

		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 text-center">
			<p>
				Already have account?<a href="{{ route('login') }}" class="text-primary m-l-5"><b>Sign In</b></a>
			</p>
		</div>
	</div>
</div>

@endsection
