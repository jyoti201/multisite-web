
<div class="menu menu-expand-md
@if(!empty(getSetting('shrink_header','header')))
	@if(!empty(getSetting('shrink_home_only','header')) && is_home($page->slug))
		shrinkable
	@elseif(empty(getSetting('shrink_home_only','header')))
		shrinkable
	@endif
@endif
@if(!empty(getSetting('transparent_header','header')))
	@if(!empty(getSetting('transparent_home_only','header')) && is_home($page->slug))
		transparent menu-absolute-top
	@elseif(empty(getSetting('transparent_home_only','header')))
		transparent menu-absolute-top
	@endif
@endif
@if(!empty(getSetting('static_header','header')))
	@if(!empty(getSetting('static_home_only','header')) && is_home($page->slug))
		sticky
	@elseif(empty(getSetting('static_home_only','header')))
		sticky
	@endif
@endif
menu-custom"
@if(!empty(getSetting('static_header','header')))
	@if(!empty(getSetting('static_home_only','header')) && is_home($page->slug))
		id="sticky-nav"
	@elseif(empty(getSetting('static_home_only','header')))
		id="sticky-nav"
	@endif
@endif
role="navigation" >
	<div class="is-container {!! !empty(getSetting('header_size','header')) ? 'is-content-'.getSetting('header_size','header') : '' !!}">
		@if(!empty(getSetting('header','header')) && getSetting('header','header')=='header1')
		<div class="row no-gutters align-items-center">
			<div class="col-12 col-md-auto d-flex align-items-center justify-content-between align-items-center mx-auto">
				<div class="menu-header mr-auto">
					<a class="menu-brand logo" href="/">
						@if(!empty(getSetting('logo','header')))
						<img src="{{ getSetting('logo','header') }}" class="img-responsive">
						@endif
					</a>
				</div>
				<button type="button" class="menu-toggler collapsed" data-toggle="collapse" data-target=".menu-mobile">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</button>
			</div>
			<div class="col">
				<div class="menu-collapse menu-desktop {{ !empty(getSetting('menustyle','header'))? getSetting('menustyle','header'): ''  }}">
					<ul class="menu-nav {{ !empty(getSetting('menu_location_menu1_align','header')) && getSetting('menu_location_menu1_align','header')=='right'? 'ml-auto': ''  }} {{ !empty(getSetting('menu_location_menu1_align','header')) && getSetting('menu_location_menu1_align','header')=='left'? 'mr-auto': ''  }} {{ !empty(getSetting('menu_location_menu1_align','header')) && getSetting('menu_location_menu1_align','header')=='center'? 'mx-auto': ''  }}">
						{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
					</ul>
				</div>
			</div>
		</div>
		<div class="row menu-mobile-wrapper">
			<div class="menu-mobile col-12 menu-collapse collapse">
			<ul class="menu-nav">
				{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
			</ul>
		</div>
		</div>
		@elseif(!empty(getSetting('header','header')) && getSetting('header','header')=='header2')
		<div class="row no-gutters align-items-center">
			<div class="col-md-5 col-12 order-1 order-md-0">
				<div class="menu-collapse menu-desktop {{ !empty(getSetting('menustyle','header'))? getSetting('menustyle','header'): ''  }}">
					<ul class="menu-nav  {{ !empty(getSetting('menu_location_menu1_align','header')) && getSetting('menu_location_menu1_align','header')=='right'? 'ml-auto': ''  }} {{ !empty(getSetting('menu_location_menu1_align','header')) && getSetting('menu_location_menu1_align','header')=='left'? 'mr-auto': ''  }} {{ !empty(getSetting('menu_location_menu1_align','header')) && getSetting('menu_location_menu1_align','header')=='center'? 'mx-auto': ''  }}">
						{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
					</ul>
				</div>
			</div>
			<div class="col d-flex d-md-block justify-content-between align-items-center mx-auto order-0 order-md-1 text-center">
				<a class="logo d-inline-block md-auto" href="/">
					@if(!empty(getSetting('logo','header')))
					<img src="{{ getSetting('logo','header') }}" class="img-responsive">
					@endif
				</a>
				<button type="button" class="menu-toggler collapsed" data-toggle="collapse" data-target=".menu-mobile">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</button>
			</div>
			<div class="col-md-5 col-12 order-2 order-md-3">
				<div class="menu-collapse menu-desktop {{ !empty(getSetting('menustyle','header'))? getSetting('menustyle','header'): ''  }}">
					<ul class="menu-nav  {{ !empty(getSetting('menu_location_menu2_align','header')) && getSetting('menu_location_menu2_align','header')=='right'? 'ml-auto': ''  }} {{ !empty(getSetting('menu_location_menu2_align','header')) && getSetting('menu_location_menu2_align','header')=='left'? 'mr-auto': ''  }} {{ !empty(getSetting('menu_location_menu2_align','header')) && getSetting('menu_location_menu2_align','header')=='center'? 'mx-auto': ''  }}">
						{!! getMenubyID(getSetting('menu_location_menu2','header')) !!}
					</ul>
				</div>
			</div>
		</div>
		<div class="row menu-mobile-wrapper">
			<div class="menu-mobile col-12 menu-collapse collapse">
			<ul class="menu-nav">
				{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
			</ul>
			<ul class="menu-nav">
				{!! getMenubyID(getSetting('menu_location_menu2','header')) !!}
			</ul>
		</div>
		</div>
		@elseif(!empty(getSetting('header','header')) && getSetting('header','header')=='header3')
		<div class="row no-gutters">
			<div class="col-8 col-sm-6 ">
				<a class="logo" href="/">
					@if(!empty(getSetting('logo','header')))
					<img src="{{ getSetting('logo','header') }}" class="img-responsive">
					@endif
				</a>
			</div>
			<div class="col-4 col-sm-6 text-right align-self-center">
				<a href="#" class="slide-toggle" id="slide-toggle">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</a>
			</div>
		</div>
		<div class="sliding" id="sliding">
			<div class="sliding-overlay"></div>
			<div class="sliding-menu">
				<a href="#" class="slide-toggle active" id="slide-toggle-close">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</a>
				@if(getSetting('slide_toggle_custom','header'))
					{!! ImplementWidgets(getSetting('slide_toggle_custom','header')) !!}
				@else
				<ul class="menu-nav">
				{!! getMenubyID(getSetting('menu_location_slide','header')) !!}
				</ul>
				@endif
			</div>
		</div>
		@elseif(!empty(getSetting('header','header')) && getSetting('header','header')=='header4')
		<div class="row no-gutters align-items-center">
			<div class="col-12 col-md-auto d-flex align-items-center justify-content-between align-items-center mx-auto">
				<a href="#" class="slide-toggle" id="slide-toggle">
						<span class="icon-bar top"></span>
						<span class="icon-bar middle"></span>
						<span class="icon-bar bottom"></span>
					</a>
				<div class="menu-header mr-auto">
					<a class="menu-brand logo" href="/">
						@if(!empty(getSetting('logo','header')))
						<img src="{{ getSetting('logo','header') }}" class="img-responsive">
						@endif
					</a>
				</div>
				<button type="button" class="menu-toggler collapsed" data-toggle="collapse" data-target=".menu-mobile">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</button>
			</div>
			<div class="col">
				<div class="menu-collapse menu-desktop {{ !empty(getSetting('menustyle','header'))? getSetting('menustyle','header'): ''  }}">
					<ul class="menu-nav {{ !empty(getSetting('menu_location_menu1_align','header')) && getSetting('menu_location_menu1_align','header')=='right'? 'ml-auto': ''  }} {{ !empty(getSetting('menu_location_menu1_align','header')) && getSetting('menu_location_menu1_align','header')=='left'? 'mr-auto': ''  }} {{ !empty(getSetting('menu_location_menu1_align','header')) && getSetting('menu_location_menu1_align','header')=='center'? 'mx-auto': ''  }}">
						{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
					</ul>
				</div>
			</div>
		</div>
		<div class="row menu-mobile-wrapper">
			<div class="menu-mobile col-12 menu-collapse collapse">
					<ul class="menu-nav">
						{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
					</ul>
			</div>
		</div>
		<div class="sliding" id="sliding">
			<div class="sliding-overlay"></div>
			<div class="sliding-menu">
				<a href="#" class="slide-toggle active" id="slide-toggle-close">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</a>
				@if(getSetting('slide_toggle_custom','header'))
					{!! ImplementWidgets(getSetting('slide_toggle_custom','header')) !!}
				@else
				<ul class="menu-nav">
				{!! getMenubyID(getSetting('menu_location_slide','header')) !!}
				</ul>
				@endif
			</div>
		</div>
		@endif
	</div>
</div>
