<div class="{columns}">
	<div class="blog">
		<a href="{link}">
		<div class="blog-image" style="background-image:url({featuredImageUrl})"><div class="date">{date}</div></div>
		<h4>{title}</h4>
		</a>
	</div>
</div>