

<div data-num="2" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/basic-02.png')}}" data-cat="120">
	<div class="row">
				<div class="col-md-12">
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				</div>
			</div>
</div>
<div data-num="8" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/basic-08.png')}}" data-cat="120">
		<div class="row">
			<div class="col-md-6">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
			</div>
			<div class="col-md-6">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
			</div>
		</div>
</div>




		<div data-num="5" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/basic-05.png')}}" data-cat="120">
	<div class="row">
					<div class="col-md-12">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/oleg-laptev-545268-unsplash-VD7ll2.jpg' )}}">
					</div>
				</div>
		</div>









		<div data-num="12" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/basic-12.png')}}" data-cat="120">
	<div class="row">
					<div class="col-md-12">
			    		<div class="spacer height-70"></div>
					</div>
				</div>
		</div>

		<div data-num="13" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/basic-13.png')}}" data-cat="120">
	<div class="row">
					<div class="col-md-12">
			    		<hr>
					</div>
				</div>
		</div>

		<div data-num="14" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/basic-14.png')}}" data-cat="120">
	<div class="row">
						<div class="col-md-6">
                            <div class="list">
                                <i class="icon ion-checkmark"></i>
                                <h3>List Item</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="list">
                                <i class="icon ion-checkmark"></i>
                                <h3>List Item</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
					</div>
		</div>

<div data-num="15" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/basic-15.png')}}" data-cat="120">
			    <div class="is-social">
                    <a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
				    <a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
				    <a href="mailto:you@example.com"><i class="icon ion-android-drafts"></i></a>
                </div>
		</div>

<div data-num="16" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/basic-16.png')}}" data-cat="120">
                <div class="is-rounded-button-medium" style="margin:1em 0">
               	    <a href="https://twitter.com/" style="background-color: #00bfff;"><i class="icon ion-social-twitter"></i></a>
               	    <a href="https://www.facebook.com/" style="background-color: #128BDB"><i class="icon ion-social-facebook"></i></a>
               	    <a href="mailto:you@example.com" style="background-color: #DF311F"><i class="icon ion-ios-email-outline"></i></a>
		        </div>&nbsp;'
		</div>

		/* Video */
<div data-num="17" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/element-video.png')}}" data-cat="120">
				<div class="embed-responsive embed-responsive-16by9">
            		<iframe width="560" height="315" src="//www.youtube.com/embed/P5yHEKqx86U?rel=0" frameborder="0" allowfullscreen=""></iframe>
            	</div>
        </div>

        /* Map */
<div data-num="18" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/element-map.png')}}" data-cat="120">

			    <div class="embed-responsive embed-responsive-16by9">
            		<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe>
            	</div>
        </div>











        //TODO END


		/* PRODUCTS */


		<div data-num="195" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-01.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48 is-title1-48 is-title-lite">CHOOSE YOUR PLAN</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h1 class="size-96 is-title-bold" style="color: #dedede; line-height:1">01</h1>
						<h3 class="size-24" style="font-weight: bold">LITE / $33</h3>
						<p style="border-bottom: 2.5px solid #000; width: 40px; display: inline-block; margin-top:0"></p>
						<p>Lorem Ipsum is dummy text of the printing industry.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Buy Now</a>
						</div>
					</div>
					<div class="col-md-4">
						<h1 class="size-96 is-title-bold" style="color: #dedede; line-height:1">02</h1>
						<h3 class="size-24" style="font-weight: bold">ADVANCED / $59</h3>
						<p style="border-bottom: 2.5px solid #000; width: 40px; display: inline-block; margin-top:0"></p>
						<p>Lorem Ipsum is dummy text of the printing industry.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Buy Now</a>
						</div>
					</div>
					<div class="col-md-4">
						<h1 class="size-96 is-title-bold" style="color: #dedede; line-height:1">03</h1>
						<h3 class="size-24" style="font-weight: bold">ULTIMATE / $77</h3>
						<p style="border-bottom: 2.5px solid #000; width: 40px; display: inline-block; margin-top:0"></p>
						<p>Lorem Ipsum is dummy text of the printing industry.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Buy Now</a>
						</div>
					</div>
				</div>
</div>

		<div data-num="196" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-02.png')}}" data-cat="107">
<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-48 is-title1-48 is-title-lite">SIMPLE PRICING</h1>
						<p style="border-bottom: 2px solid #000; width: 60px; display: inline-block;"></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-30  text-center">
								<h3 class="size-24 is-title-lite">STANDARD</h3>
								<p style="color: #e74c3c; font-size: 24px; line-height: 1.4">$<span class="size-80 is-title-lite" style="color: #e74c3c">29</span>/mo</p>
								<p>Lorem Ipsum is dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Buy Now</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-30  text-center">
								<h3 class="size-24 is-title-lite">DELUXE</h3>
								<p style="color: #e74c3c; font-size: 24px; line-height: 1.4">$<span class="size-80 is-title-lite" style="color: #e74c3c">59</span>/mo</p>
								<p>Lorem Ipsum is dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Buy Now</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-30  text-center">
								<h3 class="size-24 is-title-lite">ULTIMATE</h3>
								<p style="color: #e74c3c; font-size: 24px; line-height: 1.4">$<span class="size-80 is-title-lite" style="color: #e74c3c">79</span>/mo</p>
								<p>Lorem Ipsum is dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Buy Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="197" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-04.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-48 is-title1-48 is-title-lite">SUBSCRIPTION PLANS</h1>
						<p>Choose the right plan that works for you.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-30  text-center">
								<h1 class="size-80 is-title-lite" style="color: rgb(204, 204, 204) line-height: 1;">01</h1>
								<h3 class="size-24">BASIC / FREE</h3>
								<p>Lorem Ipsum is dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-light-text shadow-1" style="background-color: #27ae60">
							<div class="margin-30  text-center">
								<h1 class="size-80 is-title-lite" style="line-height:1">02</h1>
								<h3 class="size-24">DELUXE / $77</h3>
								<p>Lorem Ipsum is dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-light-text shadow-1" style="background-color: #f39c12">
							<div class="margin-30  text-center">
								<h1 class="size-80 is-title-lite" style="line-height:1">03</h1>
								<h3 class="size-24">PREMIUM / $89</h3>
								<p>Lorem Ipsum is dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="198" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-05.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48 default-font2" style="letter-spacing: 2px;">PRICING PLANS</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h2 class="size-64">$31</h2>
						<p class="size-16 default-font1">MONTHLY</p>
						<h3 class="size-24 default-font2" style="line-height: 2; letter-spacing: 2px;">STANDARD</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Get Started</a>
						</div>
					</div>
					<div class="col-md-4">
						<h2 class="size-64">$57</h2>
						<p class="size-16 default-font1">MONTHLY</p>
						<h3 class="size-24 default-font2" style="line-height: 2; letter-spacing: 2px;">PREMIUM</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Get Started</a>
						</div>
					</div>
					<div class="col-md-4">
						<h2 class="size-64">$62</h2>
						<p class="size-16 default-font1">MONTHLY</p>
						<h3 class="size-24 default-font2" style="line-height: 2; letter-spacing: 2px;">ULTIMATE</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Get Started</a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="199" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-06.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-64 is-title1-64 is-title-lite">PRICING PLANS</h1>
						<p class="size-21">Fair Prices. Excellent Services.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-70"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<div class="is-card is-card-circle is-light-text card" style="width:100px;height:100px;padding:20px;margin-top: 30px; background-color: #2980b9;">
									<div class="is-card-content-centered">
										<p class="size-46" style="margin:0; color: #fff">$<b style="color: #fff">55</b></p>
									</div>
								</div>
								<h3 class="size-24 is-title-lite" style="margin-top:25px">STANDARD</h3>
								<p>Lorem Ipsum is dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Purchase</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<div class="is-card is-card-circle is-light-text card" style="width:100px;height:100px;padding:20px;margin-top: 30px; background-color: #c0392b">
									<div class="is-card-content-centered">
										<p class="size-46" style="margin:0; color: #fff">$<b style="color: #fff">67</b></p>
									</div>
								</div>
								<h3 class="size-24 is-title-lite" style="margin-top:25px">DELUXE</h3>
								<p>Lorem Ipsum is dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Purchase</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<div class="is-card is-card-circle is-light-text card" style="width:100px;height:100px;padding:20px;margin-top: 30px; background-color: #8e44ad">
									<div class="is-card-content-centered">
										<p class="size-46" style="margin:0; color: #fff">$<b style="color: #fff">72</b></p>
									</div>
								</div>
								<h3 class="size-24 is-title-lite" style="margin-top:25px">PREMIUM</h3>
								<p>Lorem Ipsum is dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Purchase</a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="200" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-07.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-48 is-title1-48 is-title-lite">PLANS THAT MEET YOUR NEEDS</h1>
						<p class="size-21">Fair Prices. Excellent Services.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-70"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h1 class="size-32 is-title-lite">BASIC</h1>
								<p style="border-bottom: 2px solid #333; width: 30px; display: inline-block; margin-top: 0"></p>
								<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. Dolor sit amet.</p>
								<h4>$ <span class="size-80 is-title-bold" style="font-weight: 600">39</span></h4>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h1 class="size-32 is-title-lite">ADVANCED</h1>
								<p style="border-bottom: 2px solid #333; width: 30px; display: inline-block; margin-top: 0"></p>
								<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. Dolor sit amet.</p>
								<h4>$ <span class="size-80 is-title-bold" style="font-weight: 600">59</span></h4>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h1 class="size-32 is-title-lite">ULTIMATE</h1>
								<p style="border-bottom: 2px solid #333; width: 30px; display: inline-block; margin-top: 0"></p>
								<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. Dolor sit amet.</p>
								<h4>$ <span class="size-80 is-title-bold" style="font-weight: 600">79</span></h4>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="201" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-08.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48 is-title1-48 is-title-lite">Plans That Meet Your Needs</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h1 class="size-96 is-title-bold" style="color: #dedede;">01</h1>
						<h3 class="size-24" style="font-weight: bold">BASIC / <span style="color: rgb(27, 131, 223);">$55</span></h3>
						<p style="border-bottom: 2.5px solid #000; width: 40px; display: inline-block; margin-top:0"></p>
						<p>Lorem Ipsum is dummy text of the printing industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
						</div>
					</div>
					<div class="col-md-6">
						<h1 class="size-96 is-title-bold" style="color: #dedede;">02</h1>
						<h3 class="size-24" style="font-weight: bold">PREMIUM / <span style="color: rgb(27, 131, 223);">$77</span></h3>
						<p style="border-bottom: 2.5px solid #000; width: 40px; display: inline-block; margin-top:0"></p>
						<p>Lorem Ipsum is dummy text of the printing industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="202" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-09.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-64 is-title1-64 is-title-lite">Pricing Plans</h1>
						<p class="size-21">Choose the right plan that works for you. No hidden fees.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-30  text-center">
								<h1 class="size-64 is-title1-64 is-title-bold">$17</h1>
								<h2>BASIC</h2>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<div style="margin:2em 0 1em">
									<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Buy Now</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-30  text-center">
								<h1 class="size-64 is-title1-64 is-title-bold">$29</h1>
								<h2>PREMIUM</h2>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<div style="margin:2em 0 1em">
									<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Buy Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="203" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-12.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48 is-title1-48 is-title-lite">CHOOSE YOUR PLAN</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h1 class="size-64 is-title1-64 is-title-bold"><span style="font-size:30px">$</span>19</h1>
						<h3 class="size-18 is-title1-18 is-title-bold">Per Month</h3>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						<div style="margin:2em 0 1em">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small edit">Buy Now</a>
						</div>
					</div>
					<div class="col-md-4">
						<h1 class="size-64 is-title1-64 is-title-bold"><span style="font-size:30px">$</span>27</h1>
						<h3 class="size-18 is-title1-18 is-title-bold">Per Month</h3>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						<div style="margin:2em 0 1em">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small edit">Buy Now</a>
						</div>
					</div>
					<div class="col-md-4">
						<h1 class="size-64 is-title1-64 is-title-bold"><span style="font-size:30px">$</span>39</h1>
						<h3 class="size-18 is-title1-18 is-title-bold">Per Month</h3>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						<div style="margin:2em 0 1em">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small edit">Buy Now</a>
						</div>
					</div>
				</div>
		</div>


		<div data-num="204" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-10.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48 default-font2" style="letter-spacing: 2px;">SIMPLE PRICING</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h2 class="size-64">$31</h2>
						<p class="size-16 default-font1">MONTHLY</p>
						<h3 class="size-28 default-font2" style="line-height: 2;letter-spacing: 2px;">STANDARD</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Get Started</a>
						</div>
					</div>
					<div class="col-md-6">
						<h2 class="size-64">$57</h2>
						<p class="size-16 default-font1">MONTHLY</p>
						<h3 class="size-28 default-font2" style="line-height: 2;letter-spacing: 2px;">ULTIMATE </h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Get Started</a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="205" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-15.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48 is-title1-48 is-title-lite">PRICING PLANS</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-70"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h3>BASIC</h3>
						<p>Lorem Ipsum is dummy text of the printing and typesetting industry.</p>
						<p style="font-size: 24px; line-height: 1.4">$<span class="size-64 is-title-lite">34</span>/ month</p>
						<div style="margin:2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small">Choose Plan</a>
						</div>
					</div>
					<div class="col-md-6">
						<h3>PREMIUM</h3>
						<p>Lorem Ipsum is dummy text of the printing and typesetting industry.</p>
						<p style="font-size: 24px; line-height: 1.4">$<span class="size-64 is-title-lite">57</span>/ month</p>
						<div style="margin:2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small">Choose Plan</a>
						</div>
					</div>
				</div>
	</div>

		<div data-num="206" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-16.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48 is-title1-48 is-title-lite">PRICING PLANS</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-70"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h1 class="size-48 is-title1-48 is-title-bold" style="color: rgb(204, 204, 204);">FREE</h1>
						<h3 class="size-21">Try New Features</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<div style="margin:2em 0 1em">
							<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Get Started</a>
						</div>
					</div>
					<div class="col-md-4">
						<h1 class="size-48 is-title1-48 is-title-bold">$19</h1>
						<h3 class="size-21">Monthly</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<div style="margin:2em 0 1em">
							<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Buy Now</a>
						</div>
					</div>
					<div class="col-md-4">
						<h1 class="size-48 is-title1-48 is-title-bold">$227</h1>
						<h3 class="size-21">Yearly</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<div style="margin:2em 0 1em">
							<a href="#" class="is-btn is-btn-small is-btn-ghost1 is-upper edit">Buy Now</a>
						</div>
					</div>
				</div>
	</div>

		<div data-num="207" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-17.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-48 is-title1-48 is-title-lite">PRICING PLANS</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h1 class="size-32 is-title-lite">BASIC</h1>
								<h4>$ <span class="size-88">39</span></h4>
								<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-light-text shadow-1" style="background-color: #f39c12">
							<div class="margin-25  text-center">
								<h1 class="size-32 is-title-lite">PREMIUM</h1>
								<h4>$ <span class="size-88">59</span></h4>
								<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h1 class="size-32 is-title-lite">ULTIMATE</h1>
								<h4>$ <span class="size-88">99</span></h4>
								<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Select Plan</a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="208" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-18.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-48" style="letter-spacing: 2px;">SUBSCRIPTION  PLANS</h1>
						<p style="letter-spacing: 1px;">We make everything way easier for you.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h1 class="size-35" style="letter-spacing: 2px;">STARTER</h1>
								<h3 class="size-18" style="color: rgb(119, 119, 119) letter-spacing: 2px;">$19 / MONTH</h3>
								<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Choose Plan</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h1 class="size-35" style="letter-spacing: 2px;">PRO</h1>
								<h3 class="size-18" style="color: rgb(119, 119, 119) letter-spacing: 2px;">$59 / MONTH</h3>
								<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Choose Plan</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h1 class="size-35" style="letter-spacing: 2px;">BUSINESS</h1>
								<h3 class="size-18" style="color: rgb(119, 119, 119) letter-spacing: 2px;">$79 / MONTH</h3>
								<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
								<div style="margin:2.2em 0">
									<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Choose Plan</a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="209" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-19.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48" style="letter-spacing: 4px; text-align: center;">OUR PLANS</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-70"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h3 class="size-28" style="line-height: 2;letter-spacing: 2px;">STANDARD</h3>
								<h3 class="size-64" style="font-weight: bold;">$27</h3>
								<h3 class="size-18" style="font-weight: bold;">Per Month</h3>
								<p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry.</p>
								<div><a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small">Get Started</a></div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h3 class="size-28" style="line-height: 2;letter-spacing: 2px;">DELUXE</h3>
								<h3 class="size-64" style="font-weight: bold;">$39</h3>
								<h3 class="size-18" style="font-weight: bold;">Per Month</h3>
								<p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry.</p>
								<div><a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small">Get Started</a></div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-25  text-center">
								<h3 class="size-28" style="line-height: 2;letter-spacing: 2px;">ULTIMATE</h3>
								<h3 class="size-64" style="font-weight: bold;">$55</h3>
								<h3 class="size-18" style="font-weight: bold;">Per Month</h3>
								<p style="margin-top:0">Lorem Ipsum is simply dummy text of the printing industry.</p>
								<div><a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small">Get Started</a></div>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="210" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/pricing-20.png')}}" data-cat="107">
	<div class="row">
					<div class="col-md-6">
						<h3 class="size-28" style="line-height: 2;letter-spacing: 2px;">BASIC <span style="color: rgb(149, 149, 149);">PLAN</span></h3>
						<h3>$ <span class="size-64">39</span></h3>
						<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. Lorem ipsum dolor sit amet, vivamus ante.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Get Started</a>
						</div>
					</div>
					<div class="col-md-6">
						<h3 class="size-28" style="line-height: 2;letter-spacing: 2px;">PRO <span style="color: rgb(149, 149, 149);">PLAN</span></h3>
						<h3>$ <span style="font-size: 64px;">79</span></h3>
						<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. Lorem ipsum dolor sit amet, vivamus ante.</p>
						<div style="margin:2.2em 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Get Started</a>
						</div>
					</div>
				</div>
		</div>




			<div data-num="243" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-24.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="col-md-12">
		    			<h1 class="size-42" style="text-align: center; letter-spacing: 4px;">HAPPY CLIENTS</h1>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="spacer height-70"></div>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-md-6  text-center">
		    			<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-F0dm51.jpg' )}}">
						<p class="size-21" style="margin-bottom: 4px; letter-spacing: 2px;">MARY PALS</p>
						<p style="border-bottom: 1px solid #000; width: 40px; display: inline-block; margin-top: 0"></p>
						<p style="color: rgb(119, 119, 119);">Lorem Ipsum is simply dummy text of the printing industry.</p>
					</div>
					<div class="col-md-6  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-x5GRp1.jpg' )}}">
						<p class="size-21" style="margin-bottom: 4px; letter-spacing: 2px;">WILMA FINN</p>
						<p style="border-bottom: 1px solid #000; width: 40px; display: inline-block; margin-top: 0"></p>
						<p style="color: rgb(119, 119, 119);">Lorem Ipsum is simply dummy text of the printing industry.</p>
					</div>
				</div>
        </div>

<div data-num="244" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-25.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="col-md-12">
		    			<h1 class="size-28" style="text-transform: uppercase; letter-spacing: 4px; text-align: left;">TESTIMONIALS</h1>
						<p class="size-14" style="font-style: normal; letter-spacing: 2px; text-transform: uppercase; text-align: left;">Hear the interesting stories from our customers</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-S0R1k1.jpg' )}}">
						<p>Lorem Ipsum is simply dummy text of the printing industry. — Nelson Sand</p>
					</div>
					<div class="col-md-6">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-QqxYJ2.jpg' )}}">
						<p>Lorem Ipsum is simply dummy text of the printing industry. — Karin Sparks</p>
					</div>
				</div>
		</div>

<div data-num="245" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-26.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="col-md-4  text-center">
		    			<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-F6TCZ1.jpg' )}}">
		    		</div>
		    		<div class="col-md-8">
		    			<div style="margin-top:10px"><i class="icon ion-quote size-24" style="color: #888888"></i></div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<p class="size-14" style="color: #888888;">By George Howard</p>
					</div>
				</div>
		</div>

<div data-num="246" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-27.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="col-md-4  text-center">
		    			<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-Q1XSO1.jpg' )}}" style="border-radius: 500px">
		    		</div>
		    		<div class="col-md-8">
		    			<div style="margin-top:10px"><i class="icon ion-quote size-24" style="color: #888888"></i></div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<p class="size-14" style="color: #888888;">By Lucas Fulmer</p>
					</div>
				</div>
		</div>

<div data-num="247" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-28.png')}}" data-cat="110">

		    	<div class="row">
					<div class="col-md-12">
						<div class="quote">
							<i class="icon ion-quote"></i>
							<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<small>by Victoria Martin</small>
						</div>
					</div>
				</div>
		</div>

<div data-num="248" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-29.png')}}" data-cat="110">

		    	<div class="row">
					<div class="col-md-6">
						<div class="quote">
							<i class="icon ion-quote"></i>
							<p>Lorem Ipsum is dummy text of the printing and typesetting industry.</p>
							<small>by Cathy Hartman</small>
						</div>
					</div>
					<div class="col-md-6">
						<div class="quote">
							<i class="icon ion-quote"></i>
							<p>Lorem Ipsum is dummy text of the printing and typesetting industry</p>
							<small>by Josh Perkins</small>
						</div>
					</div>
				</div>
		</div>

<div data-num="249" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-30.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="col-md-12">
		    			<h1 class="size-48" style="letter-spacing: 3px;">OUR HAPPY CLIENTS</h1>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="spacer height-70"></div>
		    		</div>
		    	</div>
		    	<div class="row">
					<div class="col-md-6">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-akB4h1.jpg' )}}">
					</div>
					<div class="col-md-6">
						<div class="quote">
							<i class="icon ion-quote"></i>
							<p>Lorem Ipsum is dummy text of the printing and typesetting industry.</p>
							<small>by Jason Butterfield</small>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-30"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="quote">
							<i class="icon ion-quote"></i>
							<p>Lorem Ipsum is dummy text of the printing and typesetting industry.</p>
							<small>by Paula Johnson</small>
						</div>
					</div>
					<div class="col-md-6">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-R79NY2.jpg' )}}">
					</div>
				</div>
		</div>

<div data-num="250" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-31.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="col-md-12">
		    			<h1 class="size-48" style="letter-spacing: 3px;">OUR HAPPY CLIENTS</h1>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="spacer height-70"></div>
		    		</div>
		    	</div>
		    	<div class="row">
					<div class="col-md-6">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-ybeSl1.jpg' )}}" style="border-radius: 500px;">
					</div>
					<div class="col-md-6">
						<div class="quote">
							<i class="icon ion-quote"></i>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum dolor sit amet leo ante.</p>
							<small>by Amy Meyers </small>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-30"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="quote">
							<i class="icon ion-quote"></i>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum dolor sit amet leo ante.</p>
							<small>by Christopher Cook </small>
						</div>
					</div>
					<div class="col-md-6">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-KUCDN4.jpg' )}}" style="border-radius: 500px;">
					</div>
				</div>
		</div>


		<div data-num="251" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-05.png')}}" data-cat="110">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-48 is-title1-48 is-title-lite">HAPPIEST CUSTOMERS</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12  text-center">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/watch-1663246_1920-8wuCM1.jpg' )}}">
						<div class="spacer height-30"></div>
						<i class="icon ion-quote size-24"></i>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						<p class="size-14">by Your Name</p>
					</div>
					<div class="col-md-4  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-frUlS1.jpg' )}}">
						<div class="spacer height-30"></div>
						<i class="icon ion-quote size-24"></i>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						<p class="size-14">by Your Name</p>
					</div>
					<div class="col-md-4  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-RXypm2.jpg' )}}">
						<div class="spacer height-30"></div>
						<i class="icon ion-quote size-24"></i>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						<p class="size-14">by Your Name</p>
					</div>
				</div>
		</div>

		<div data-num="252" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-06.png')}}" data-cat="110">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-48 is-title1-48 is-title-lite">CUSTOMER TESTIMONIALS</h1>
						<p class="size-21">Hear the interesting stories from our lovely customers.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6  text-center">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-30  text-center">
								<i class="icon ion-chatbubble-working size-48"></i>
								<p>"Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s."</p>
								<p class="size-14">by Your Name</p>
							</div>
						</div>
					</div>
					<div class="col-md-6  text-center">
						<div class="is-card is-dark-text shadow-1">
							<div class="margin-30  text-center">
								<i class="icon ion-chatbubble-working size-48"></i>
								<p>"Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s."</p>
								<p class="size-14">by Your Name</p>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="253" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-07.png')}}" data-cat="110">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-80" style="letter-spacing: 11px;">WHAT PEOPLE SAY ABOUT US</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
						<p class="size-14" style="color: rgb(136, 136, 136);">— John Smith, Web Developer</p>
					</div>
					<div class="col-md-4">
						<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
						<p class="size-14" style="color: rgb(136, 136, 136);">— Dave Clark, Freelance Designer</p>
					</div>
					<div class="col-md-4">
						<p>Lorem Ipsum is simply dummy text. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
						<p class="size-14" style="color: rgb(136, 136, 136);">— Ellen Lage, Photographer</p>
					</div>
				</div>
		</div>

		<div data-num="254" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-08.png')}}" data-cat="110">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-48" style="letter-spacing: 8px;">CLIENTS REVIEWS</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-70"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/noah-buscher-548633-ExLNF1.jpg' )}}" style="border-radius: 500px;">
						<p style="color: #bdc3c7"><i class="icon ion-android-hangout size-32"></i></p>
						<p class="size-16">Lorem Ipsum is simply dummy text of the printing industry. — Dave Smith</p>
					</div>
					<div class="col-md-4  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/icons8-team-355990-Voloz1.jpg' )}}" style="border-radius: 500px;">
						<p style="color: #bdc3c7"><i class="icon ion-android-hangout size-32"></i></p>
						<p class="size-16">Lorem Ipsum is simply dummy text of the printing industry. — Clara Wang</p>
					</div>
					<div class="col-md-4  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/pexels-photo-girl-zZWwv1.jpg' )}}" style="border-radius: 500px;">
						<p style="color: #bdc3c7"><i class="icon ion-android-hangout size-32"></i></p>
						<p class="size-16">Lorem Ipsum is simply dummy text of the printing industry. — Jane Doe</p>
					</div>
				</div>
		</div>


			<div data-num="255" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-16.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="col-md-12  text-center">
		    			<h1 class="size-48" style="letter-spacing: 5px;">CLIENT REVIEWS</h1>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="spacer height-70"></div>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-md-4 text-center">
						<p class="size-21" style="margin-bottom:4px">JOHN WEBER</p>
						<p style="border-bottom: 1px solid #000; width: 40px; display: inline-block; margin-top: 0"></p>
						<p style="color: rgb(119, 119, 119);">Lorem Ipsum is simply dummy text of the printing industry.</p>
					</div>
					<div class="col-md-4 text-center">
						<p class="size-21" style="margin-bottom:4px">NICOLE HOLOWAY</p>
						<p style="border-bottom: 1px solid #000; width: 40px; display: inline-block; margin-top: 0"></p>
						<p style="color: rgb(119, 119, 119);">Lorem Ipsum is simply dummy text of the printing industry.</p>
					</div>
					<div class="col-md-4 text-center">
						<p class="size-21" style="margin-bottom:4px">CHRIS WILLIAMS</p>
						<p style="border-bottom: 1px solid #000; width: 40px; display: inline-block; margin-top: 0"></p>
						<p style="color: rgb(119, 119, 119);">Lorem Ipsum is simply dummy text of the printing industry.</p>
					</div>
				</div>
        </div>

<div data-num="256" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-17.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="col-md-6">
		    			<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-6PDKL1.jpg' )}}">
		    		</div>
		    		<div class="col-md-6">
		    			<div style="padding-left: 10px">
							<div class="spacer height-50"></div>
							<h1 class="size-32" style="text-align: center; font-weight: bold;">"Incredible services and awesome customer support."</h1>
							<div class="spacer height-70"></div>
							<p style="text-align: center;">BY THOMAS JONE<br>Project Manager at Company Name</p>
						</div>
					</div>
				</div>
		</div>

<div data-num="257" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-18.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="col-md-6">
		    			<div style="padding-right: 10px">
							<div class="spacer height-50"></div>
							<h1 class="size-38" style="text-align: center; font-weight: bold;">"Incredible services and awesome customer support."</h1>
							<div class="spacer height-70"></div>
							<p style="text-align: center;">BY MARGARET VICKREY&nbsp;<br>Web Developer at Company Name</p>
						</div>
					</div>
					<div class="col-md-6">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-OWu2H1.jpg' )}}">
					</div>
				</div>
		</div>

<div data-num="258" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/quotes-19.png')}}" data-cat="110">

		    	<div class="row">
		    		<div class="column center full">
		    			<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-TK4An1.jpg' )}}" style="border-radius: 500px">
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="column center full">
		    			<div class="quote">
							<i class="icon ion-quote"></i>
							<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
						</div>
						<p style="color: rgb(149, 149, 149);">By Christopher Maxime</p>
					</div>
				</div>
		</div>

		/* PARTNERS */


			<div data-num="259" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/partners-03.png')}}" data-cat="111">

				<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-64" style="letter-spacing: 7px; font-weight: 400;">OUR CLIENTS</h1>
						<p class="size-16" style="letter-spacing: 1px; color: rgb(136, 136, 136);">We are globally trusted by the world\'s best names.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/creative.png' )}}">
					</div>
					<div class="col-md-3  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/light-studio.png' )}}">
					</div>
					<div class="col-md-3  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/infinitech.png' )}}">
					</div>
					<div class="col-md-3  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/design-firm.png' )}}">
					</div>
				</div>
		</div>

		<div data-num="260" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/partners-05.png')}}" data-cat="111">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 style="letter-spacing: 7px;">OUR PARTNERS</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12  text-center">
						<div class="center" style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/sitepro.png' )}}">
						</div>
						<div class="center" style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/steady.png' )}}">
						</div>
						<div class="center" style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/creative.png' )}}">
						</div>
						<div class="center" style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/light-studio.png' )}}">
						</div>
						<div class="center" style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/infinitech.png' )}}">
						</div>
					</div>
				</div>
		</div>

		<div data-num="261" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/partners-06.png')}}" data-cat="111">
	<div class="row">
					<div class="col-md-12  text-center">
						<div class="display">
							<h1 class="size-64">Our Clients</h1>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12  text-center">
						<div style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/creative.png' )}}">
						</div>
						<div style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/steady.png' )}}">
						</div>
						<div style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/light-studio.png' )}}">
						</div>
						<div style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/sitepro.png' )}}">
						</div>
						<div style="display:inline-block;width:18%">
							<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/design-firm.png' )}}">
						</div>
					</div>
				</div>
		</div>

		<div data-num="262" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/partners-01.png')}}" data-cat="111">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48 is-title1-48 is-title-lite">OUR PARTNERS</h1>
						<p class="size-21">We are globally recognized and trusted by the world\'s best names.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/creative.png' )}}">
					</div>
					<div class="col-md-3">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/light-studio.png' )}}">
					</div>
					<div class="col-md-3">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/sitepro.png' )}}">
					</div>
					<div class="col-md-3">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/infinitech.png' )}}">
					</div>
				</div>
		</div>



		<div data-num="267" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/404-01.png')}}" data-cat="113">
	<div class="row">
					<div class="col-md-12  text-center">
						<p class="size-132" style="font-weight: bold; line-height: 1.4">404</p>
						<h1>PAGE NOT FOUND</h1>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12  text-center">
						<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Back to Home</a>
					</div>
				</div>
		</div>

		<div data-num="268" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/404-02.png')}}" data-cat="113">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-96 is-title1-96 is-title-bold">404</h1>
						<p class="size-24">Oops! The page you\'re looking for doesn\'t exist.<br>Click the link below to return home.</p>
						<p><a href="#">HOMEPAGE</a></p>
					</div>
				</div>
		</div>

		<div data-num="269" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/404-03.png')}}" data-cat="113">
	<div class="row">
					<div class="col-md-12  text-center">
						<i class="icon ion-alert-circled size-64"></i>
						<h1 class="size-48 is-title2-48 is-title-lite">Oops, page not found.</h1>
						<p>The page you are looking for might have been removed, had its name changed, or temporarily unavailable.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12  text-center">
						<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Homepage</a>
					</div>
				</div>
		</div>

		<div data-num="270" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/404-04.png')}}" data-cat="113">
	<div class="row">
					<div class="col-md-12">
						<i class="icon ion-android-sad size-64"></i>
						<h1 class="size-48 is-title1-48 is-title-lite">Something\'s wrong here... </h1>
						<p class="size-21">The page you requested couldn\'t be found. This could be a spelling error in the URL or a removed page.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small edit">Back to Home</a>
					</div>
				</div>
		</div>

		<div data-num="271" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/404-06.png')}}" data-cat="113">
	<div class="row">
					<div class="col-md-12  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/lost-2747289-ThbrT1.png' )}}">
						<p class="size-21" style="letter-spacing: 2px;">Sorry. The page you are looking for could not be found.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12  text-center">
						<div style="margin: 25px 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Back to Home</a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="272" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/404-07.png')}}" data-cat="113">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-80" style="letter-spacing: 3px; font-weight: bold">404 ERROR - PAGE NOT FOUND</h1>
						<p class="size-21">Sorry, the page could not be found. You might be able to find what you are looking for from the homepage.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div style="margin: 10px 0">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Back to Home</a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="273" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/404-08.png')}}" data-cat="113">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-196" style="letter-spacing: 12px; margin-bottom: 10px">404</h1>
						<h3 class="size-32" style="letter-spacing: 8px;">PAGE NOT FOUND</h3>
						<p class="size-21">The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>
					</div>
				</div>
		</div>

		<div data-num="274" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/404-09.png')}}" data-cat="113">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-220" style="letter-spacing: 12px; margin-bottom: 0">404</h1>
						<p>We are sorry, the page you are looking for could not be found. This could be a spelling error in the URL or a removed page.</p>
						<div style="margin: 35px 0">
							<a href="#" class="is-btn is-btn-ghost2 is-upper is-btn-small edit">Contact Us</a> &nbsp;
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Homepage</a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="275" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/comingsoon-01.png')}}" data-cat="114">
	<div class="row">
					<div class="col-md-12  text-center">
						<h3 class="size-32 is-title1-32 is-title-lite">STAY TUNED!</h3>
						<h1 class="size-64 is-title1-64 is-title-bold">OUR WEBSITE IS COMING VERY SOON</h1>
						<div class="spacer height-50"></div>
						<div class="is-social edit">
							<div class="size-21">
								<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
								<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
								<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="276" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/comingsoon-02.png')}}" data-cat="114">
	<div class="row">
					<div class="col-md-12  text-center">
						<i class="icon ion-android-bicycle size-64"></i>
						<h1 class="size-80 is-title2-80 is-title-lite">WE ARE COMING SOON</h1>
						<p class="size-21">Our website is under construction. We will be here with new awesome site.</p>
						<div style="margin: 3em 0">
							<a href="#" class="is-btn is-btn-ghost2 is-upper is-btn-small edit">Contact Us</a> &nbsp;
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Notify Me</a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="277" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/comingsoon-03.png')}}" data-cat="114">
	<div class="row">
					<div class="col-md-12  text-center">
						<i class="icon ion-laptop size-64"></i>
						<h1 class="size-48 is-title2-48 is-title-lite">SITE IS UNDER MAINTENANCE </h1>
						<p class="size-24">Please check back in sometime.</p>
						<div class="spacer height-50"></div>
						<div class="is-social edit size-21">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="278" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/comingsoon-04.png')}}" data-cat="114">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-48 is-title1-48 is-title-lite">MAINTENANCE MODE</h1>
						<p class="size-24">Our website is under maintenance. Please comeback later.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12  text-center">
						<p class="size-64 is-title1-64 is-title-bold">90%</p>
						<p>COMPLETED</p>
					</div>
				</div>
		</div>

		<div data-num="279" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/comingsoon-05.png')}}" data-cat="114">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-96" style="letter-spacing: 19px; margin-bottom: 10px;">COMING SOON</h1>
						<p style="text-transform: uppercase; letter-spacing: 2px;">CHECK BACK SOON FOR THE NEW AND IMPROVED SITE</p>
						<div class="spacer height-50"></div>
						<div class="is-social edit size-18">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="280" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/comingsoon-06.png')}}" data-cat="114">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-80" style="letter-spacing: 5px;">OUR SITE IS COMING VERY SOON</h1>
						<p>We are currently working on something awesome. We will be here soon.</p>
						<div class="is-social edit size-18" style="margin: 30px 0">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12  text-center">
						<a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small edit">Notify Me</a>
					</div>
				</div>
		</div>

		<div data-num="281" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/comingsoon-07.png')}}" data-cat="114">
	<div class="row">
					<div class="col-md-12">
						<div class="display">
							<h1 class="size-80" style="letter-spacing: 4px; text-align: center; color: rgb(209, 209, 209);">COMING SOON.</h1>
						</div>
						<p style="text-align: center;">Our website is under construction. We will be here with new awesome site.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class=" text-center"><a href="#" class="is-btn is-btn-ghost1 is-upper">Notify Me</a></div>
					</div>
				</div>
		</div>

		<div data-num="282" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/comingsoon-08.png')}}" data-cat="114">
	<div class="row">
					<div class="col-md-12  text-center">
						<p style="border-bottom: 2px solid #b9b6b6;width: 210px;display: inline-block;"></p>
						<h1 class="size-42" style="letter-spacing: 3px;">Sorry, our website is currently getting a face lift. Check back soon for the new awesome and improved site.</h1>
						<p style="border-bottom: 2px solid #b9b6b6; width: 210px; display: inline-block; margin-top: 20px"></p>
					</div>
				</div>
		</div>



		<div data-num="289" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-01.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-6">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe>
						</div>
					</div>
					<div class="col-md-6">
						<h1 class="size-32 is-title1-32 is-title-lite">FIND US HERE</h1>
						<p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
						<div class="is-social edit size-18">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="290" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-02.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-3  text-center">
								<h2 style="margin-top:0"><i class="icon ion-ios-alarm-outline size-64"></i></h2>
							</div>
							<div class="col-md-9">
								<h2 class="size-28" style="margin-top:10px">OPENING TIMES</h2>
								<p>Monday – Friday: 9:00 AM – 4:30 PM<br>
								'Saturday: 8:00 AM – 2:00 PM</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-3  text-center">
								<h2 style="margin-top:0"><i class="icon ion-ios-home-outline size-64"></i></h2>
							</div>
							<div class="col-md-9">
								<h2 class="size-28" style="margin-top:10px">FIND US HERE</h2>
								<p>123 Street Name, City.<br>State 12345.<br>
								'Phone: (123) 456 7890</p>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="291" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-04.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-4">
						<h3 class="size-18 is-title-lite">CONTACT US</h3>
						<p style="border-bottom: 1px solid #333; width: 50px; display: inline-block; margin-top: 0"></p>
						<p class="size-16">
							<strong>Your Company Name</strong><br>
							'12345 Street Name, City.
							'State 12345<br>
							'P: (123) 456 7890 / 456 7891.
						</p>
					</div>
					<div class="col-md-4">
						<h3 class="size-18 is-title-lite">OPENING HOURS</h3>
						<p style="border-bottom: 1px solid #333; width: 50px; display: inline-block; margin-top: 0"></p>
						<p class="size-16">
							'Monday - Friday: 9:00 AM - 10:00 PM<br>
							'Saturday: 10:00 AM - 11:00 PM
						</p>
					</div>
					<div class="col-md-4">
						<h3 class="size-18 is-title-lite">STAY UPDATED</h3>
						<p style="border-bottom: 1px solid #333; width: 50px; display: inline-block; margin-top: 0"></p>
						<p class="size-16">
							'Follow us on:<br>
							'Facebook: <a href="#">Company Name</a><br>
							'Twitter: <a href="#">@companyname</a>
						</p>
					</div>
				</div>
		</div>

		<div data-num="292" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-05.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-4">
						<i class="icon ion-map size-64" style="color: #e74c3c"></i>
						<p class="size-14" style="color: rgb(136, 136, 136);">OUR LOCATION</p>
						<p>
							'Your Company Name<br>
							'12345 Street Name, City.
							'P: (123) 456 7890
						</p>
					</div>
					<div class="col-md-4">
						<i class="icon ion-clock size-64" style="color: #e74c3c"></i>
						<p class="size-14" style="color: rgb(136, 136, 136);">OPENING HOURS</p>
						<p>
							'Monday - Friday: 9:00 AM - 10:00 PM<br>
							'Sat: 10:00 AM - 11:00 PM
						</p>
					</div>
					<div class="col-md-4">
						<i class="icon ion-chatbox-working size-64" style="color: #e74c3c"></i>
						<p class="size-14" style="color: rgb(136, 136, 136);">STAY UPDATED</p>
						<p>
							'Follow us on:<br>
							'Facebook: <a href="#" style="color: #333">Company Name</a><br>
							'Twitter: <a href="#" style="color: #333">@companyname</a>
						</p>
					</div>
				</div>
		</div>

		<div data-num="293" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-06.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-6">
						<h1 class="size-42 is-title-bold"  style="margin-top:0">Do you have something to say? Contact us!</h1>
					</div>
					<div class="col-md-6">
						<h1 class="size-32">YOUR COMPANY NAME</h1>
						<div class="spacer height-30"></div>
						<p>
							'12345 Street Name, City.
							'State 12345<br>
							'P: (123) 456 7890 / 456 7891. <br>
							'Email:<br><a href="#" style="color: #333">companyname@example.com</a>
						</p>
					</div>
				</div>
		</div>

		<div data-num="294" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-07.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-12  text-center">
						<h1 class="size-32">Have questions? Give us a call <span style="color: rgb(230, 126, 34);">0 123 456 78 90</span></h1>
						<div class="spacer height-50"></div>
						<div class="is-social edit">
							<div class="size-18">
								<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
								<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
								<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div data-num="295" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-08.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-12  text-center">
						<div class="display">
							<h1 style="letter-spacing: 4px">GET IN TOUCH</h1>
						</div>
						<p>
							'12345 STREET NAME, CITY.
							'STATE 12345<br>
							'PHONE: (123) 456 7890 / 456 7891
						</p>
						<div class="is-social edit size-16">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="296" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-09.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-4  text-center">
						<i class="icon ion-ios-book-outline size-64" style="color: rgb(136, 136, 136);"></i>
						<h6 style="letter-spacing: 2px; font-weight: normal">OUR LOCATION</h6>
						<p>12 Street Name, City</p>
					</div>
					<div class="col-md-4  text-center">
						<i class="icon ion-ios-telephone-outline size-64" style="color: rgb(136, 136, 136);"></i>
						<h6 style="letter-spacing: 2px; font-weight: normal">CALL US</h6>
						<p>(123) 456 7890 / 456 7891</p>
					</div>
					<div class="col-md-4  text-center">
						<i class="icon ion-ios-compose-outline size-64" style="color: rgb(136, 136, 136);"></i>
						<h6 style="letter-spacing: 2px; font-weight: normal">DROP US A LINE</h6>
						<p><a href="mailto:#" style="color: #333">first.last@example.com</a></p>
					</div>
				</div>
		</div>

		<div data-num="297" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-10.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-12">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-30"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h3 class="size-18 is-title-lite">CONTACT</h3>
						<p class="size-16"><b>Company Name</b><br>123 Street Name, City.</p>
					</div>
					<div class="col-md-4">
						<h3 class="size-18 is-title-lite">OPENING HOURS</h3>
						<p class="size-16">Monday - Friday: 8 AM - 5 PM<br>Saturday: 10 AM - 3 PM</p>
					</div>
					<div class="col-md-4">
						<h3 class="size-18 is-title-lite">STAY IN TOUCH</h3>
						<p class="size-16">Instagram: <a href="#">@companyname<br>Twitter: </a><a href="#">@companyname</a></p>
					</div>
				</div>
		</div>

		<div data-num="298" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-11.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-80" style="text-align: center; letter-spacing: 4px;">Hi... Let\'s get in touch!</h1>
						<p style="text-align: center;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
						<div class="spacer height-70"></div>
						<div class="is-social  text-center">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-android-drafts"></i></a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="299" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-12.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-12">
						<h2 style="text-transform: uppercase; letter-spacing: 6px; text-align: center;">GET CONNECTED</h2>
						<div class=" text-center">
							<p style="border-bottom: 2px solid #000;width: 50px;display: inline-block;margin-bottom:0"></p>
						</div>
						<div class="spacer height-50"></div>
						<p style="text-align: center;">
							<b>Your Company Name, Inc.</b> 12345 Street Name, City. State 12345.<br>
							'P: (123) 456 7890 / 456 7891
						</p>
						<div class="is-social  text-center">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-android-drafts"></i></a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="300" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-18.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-12  text-center">
						<img src="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/images/-5TpV91.jpg' )}}">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12  text-center">
						<h1 style="letter-spacing: 4px">CONTACT US</h1>
						<p style="letter-spacing: 1px;">
							'123 STREET NAME, CITY.
							'STATE 12345<br>
							'PHONE: (123) 456 7890
						</p>
						<div class="is-social edit size-16">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="301" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-14.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-48 is-title1-48 is-title-bold" style="letter-spacing: 2px;">DROP IN OUR OFFICE.</h1>
						<div class="spacer height-30"></div>
						<p><i class="icon ion-android-home size-24"></i>&nbsp; &nbsp;Company Name, Inc. 12345 Street, City. State 12345.<br><i class="icon ion-android-call size-24"></i>&nbsp; &nbsp;Phone: (123) 456 7890 / 456 7891</p>
					</div>
				</div>
		</div>

		<div data-num="302" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-15.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-60" style="letter-spacing: 1px; text-align: center; font-weight: bold;">Drop us a line to get the conversation started.</h1>
						<p style="text-align: center;">Please kindly write to us at companyname@example.com&nbsp;or call us on +123 4567 890</p>
						<div class=" text-center">
							<p style="border-bottom: 2px solid #000;width: 70px;display: inline-block;margin-bottom:0"></p>
						</div>
						<div class="spacer height-90"></div>
						<div class="is-social  text-center">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-android-drafts"></i></a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="303" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-16.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-12">
						<h1 class="size-72" style="letter-spacing: 4px; text-align: center;">CONTACT US</h1>
						<p style="text-align: center; letter-spacing: 3px; font-size: 19px !important;">(123) 456 7890</p>
						<p style="text-align: center; color: rgb(119, 119, 119);">Monday - Friday: 8:00 AM - 5:00 PM</p>
						<div class="spacer height-50"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class=" text-center">
							<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small">Drop us a line</a>
						</div>
					</div>
				</div>
		</div>

		<div data-num="304" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/contact-19.png')}}" data-cat="116">
	<div class="row">
					<div class="col-md-6">
						<p style="text-transform: uppercase; letter-spacing: 3px;">Talk To Us</p>
						<h1 style="font-weight: bold;">Get in touch.</h1>
						<div class="spacer height-90"></div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>
						<div class="spacer height-30"></div>
						<p><b>Phone:</b><br>(123) 456 7890 / 456 7891</p>
						<div class="spacer height-50"></div>
						<div class="size-18">
							<a href="https://twitter.com/"><i class="icon ion-social-twitter" style="margin-right: 1em"></i></a>
							<a href="https://www.facebook.com/"><i class="icon ion-social-facebook" style="margin-right: 1em"></i></a>
							<a href="mailto:you@example.com"><i class="icon ion-ios-email-outline"></i></a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="embed-responsive embed-responsive-16by9" style="height:360px"><iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&amp;hl=en&amp;sll=-7.981898,112.626504&amp;sspn=0.009084,0.016512&amp;oq=melbourne&amp;hnear=Melbourne+Victoria,+Australia&amp;t=m&amp;z=10&amp;output=embed"></iframe></div>
					</div>
				</div>
		</div>

		/* Buttons */

		<div data-num="305" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-01.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost2 is-upper">Read More</a> &nbsp;
                    <a href="#" class="is-btn is-btn-ghost1 is-upper">Buy Now</a>
                </div>
		</div>

		<div data-num="306" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-02.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost2 is-upper">Read More</a>
                </div>
		</div>

		<div data-num="307" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-03.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost1 is-upper">Buy Now</a>
                </div>
		</div>

		<div data-num="308" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-04.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost2 is-upper is-btn-small">Read More</a> &nbsp;
                    <a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small">Buy Now</a>
                </div>
		</div>

		<div data-num="309" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-05.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost2 is-upper is-btn-small">Buy Now</a>
                </div>
		</div>

		<div data-num="310" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-06.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small">Buy Now</a>
                </div>
		</div>

		<div data-num="311" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-07.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost2 is-upper is-rounded-30">Read More</a> &nbsp;
                    <a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30">Buy Now</a>
                </div>
		</div>

		<div data-num="312" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-08.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost2 is-upper is-rounded-30">Read More</a>
                </div>
		</div>

		<div data-num="313" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-09.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30">Buy Now</a>
                </div>
		</div>

		<div data-num="314" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-10.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost2 is-upper is-rounded-30 is-btn-small">Read More</a> &nbsp;
                    <a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small">Buy Now</a>
                </div>
		</div>

		<div data-num="315" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-11.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost2 is-upper is-rounded-30 is-btn-small">Read More</a>
                </div>
		</div>

		<div data-num="316" data-thumb="{{ asset('templates/'.$theme.'/private/snippets-assets/minimalist-basic/thumbnails/button-12.png')}}" data-cat="119">
	<div>
                    <a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small">Buy Now</a>
                </div>
		</div>

		{!! getWidgets() !!}

		{!! getButtons() !!}
