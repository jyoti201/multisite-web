<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
		<link href="{{ asset('assets/private/css/styles.css') }}" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
	@if (\Session::has('success'))
		<div class="alert alert-success">
			{!! \Session::get('success') !!}
		</div>
	@elseif(\Session::has('error'))
		<div class="alert alert-danger">
			{!! \Session::get('error') !!}
		</div>
	@endif
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Welcome
					<form class="text-center" method="post" action="{{ route("settings.saveHost") }}">
						{{ csrf_field() }}
						<input type="hidden" class="form-control" name="type" value="{{ getAdminsetting('host','type') }}">
						<input type="hidden" class="form-control" name="prod_url" value="https://{{ getAdminsetting('host','org') }}.github.io/{{ config('app.name') }}/">
						<input type="hidden" class="form-control" name="user" id="user" value="{{ getAdminsetting('host','user') }}">
						<input type="hidden" class="form-control" name="email" id="email" value="{{ getAdminsetting('host','email') }}">
						<input type="hidden" class="form-control" name="org" id="org" value="{{ getAdminsetting('host','org') }}">
						<input type="hidden" class="form-control" name="token" id="token" value="{{ getAdminsetting('host','token') }}">
						<button type="submit" class="btn btn-default btn-lg">Start Creating your Site</button>
						<input type="hidden" name="settingType" value="host">
					</form>
                </div>
            </div>
        </div>
    </body>
</html>
