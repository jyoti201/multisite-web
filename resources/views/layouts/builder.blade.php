<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
@if(!$preview)
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endif

	@if($preview)
    @include('admin._partials.metatags')
	@else
		 <title>{{ config('app.name', 'Laravel') }}</title>
	@endif

	<script src="{{ asset('assets/public/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/public/js/popper.min.js') }}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="{{ asset('assets/public/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/public/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/public/css/box.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/public/css/animate.min.css') }}" rel="stylesheet" type="text/css" />

  @if($preview)
  <link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />
    @if(WidgetExists('site_search'))
        <link href="{{ asset('assets/public/css/tipuesearch.css') }}" rel="stylesheet" type="text/css" />
    @endif
  <script src="https://cdn.jsdelivr.net/npm/js-cookie@beta/dist/js.cookie.min.js"></script>
  <script type="text/javascript" src="{{ asset('assets/public/js/jquery.magnific-popup.min.js') }}"></script>
  @endif


	@yield('styles')

	@yield('scripts')
	@if(!$preview)
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/private/css/modal.css') }}" />
	<link href="{{ asset('builder/contentbuilder/contentbuilder.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('builder/contentbox/contentbox.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href="{{ asset('assets/private/css/dropzone.css') }}" rel="stylesheet" type="text/css" />

	<link href="{{ asset('assets/private/css/builder.css') }}" rel="stylesheet">
	<script>var csrf_field = '{{csrf_field()}}';</script>

  <script>
  jQuery(document).ready(function(){
	jQuery('removable').each(function(){
		jQuery(this).attr('contenteditable', 'false');
	})
	jQuery( "removable" ).mouseover(function() {
	  jQuery(this).attr('contenteditable', 'false');
	});
    jQuery('#is-wrapper').on('click','.bootstrap removable .add_col',function(){
        jQuery(this).parents('.bootstrap').find('.row').append('<div class="col col-md-12"><removable contenteditable="false"><button  class="remove_col" contenteditable="false">Remove</button><button class="smaller_col" contenteditable="false">Smaller</button><button class="bigger_col" contenteditable="false">Bigger</button></removable><p>&nbsp;</p></div>');
    })
    jQuery('#is-wrapper').on('click','.bootstrap removable .smaller_col',function(){
        var classes = jQuery(this).parents('.col').attr('class');
        var classesNo = classes.replace('col col-md-','');
        if(classesNo>2){
          classesNo = parseInt(classesNo) - 1;
        }
        jQuery(this).parents('.col').attr('class', 'col col-md-'+classesNo);
    })
    jQuery('#is-wrapper').on('click','.bootstrap removable .bigger_col',function(){
        var classes = jQuery(this).parents('.col').attr('class');
        var classesNo = classes.replace('col col-md-','');
        if(classesNo<12){
          classesNo = parseInt(classesNo) + 1;
        }
        jQuery(this).parents('.col').attr('class', 'col col-md-'+classesNo);
    })
    jQuery('#is-wrapper').on('click','.bootstrap removable .remove_col',function(){
        jQuery(this).parents('.col').remove();
    })
	jQuery('.md-custom-overlay').on('click', function(){
		jQuery(jQuery(this).data('target')).removeClass('md-custom-show').removeClass('md-show');
	})




  })
  </script>
  <style>
  .builder_controls{
    width: 60px;
    bottom: 0;
    padding-top: 35px;
    position: fixed;
    right: 0;
    left: auto;
  }
  .builder_controls li{
    float: none;
display: block;
margin: 15px 0;
width: 100%;
  }
  .builder_controls .nav>li a{
    background-color: transparent !important;
    padding:0;
    font-size: 15px !important;
    display: block;
    color: #333 !important;
        line-height: 1.5;
  }
  .builder_controls ul>li:hover> a{
    color: #333 !important;
  }
  .btn .caret {
    transform: rotate(270deg);
}
.builder_controls li.dropdown {
    position: absolute;
    bottom: 10px;
    left: 12px;
}
.builder_controls li.dropdown .dropdown-menu{
  top: auto !important;
bottom: 0;
right: 100%;
left: auto !important;
border: 0;
border-radius: 0 !important;
transform: none !important;
}
.pagealert{
  left: 10px;
    top: 10px;
    bottom: auto;
    font-size: 15px !important;
        font-family: 'Varela Round', sans-serif;
}

#sticky-nav-sticky-wrapper.collide {
    transform: translateX(65px) translateY(0px);
}
#hide_show_builder {
  top: 0px;
  right: 0px;
  font-size: 16px;
  background-color: rgba(255,255,255);
  height: 27px;
  color: #000;
  width: 60px;
  position: fixed;
  z-index: 1000;
  text-align: center;
  left: auto;
}
.builder_controls .btn.btn-primary{
  padding: 5px px !important;
  width: 37px;
  text-align: center;
}
.builder_controls ul>li>.dropdown-menu li{
margin: 0;
}
.builder_controls ul>li>.dropdown-menu a{
padding: 8px 15px;
    font-family: "Open Sans", sans-serif !important;
}
body.control-collide{
  padding-left: 0;
}
.md-modal.stuckright{
  border-radius: 0px;
    right: 0 !important;
    left: auto !important;
    padding: 0;
    top: 0 !important;
    bottom: 0 !important;
    height: 100% !important;
    width: 500px;
    max-height: none !important;
}
.md-modal.stuckright .pading-margin .is-box-6,.md-modal.stuckright .colsettingonly  .is-box-6,.md-modal.stuckright #col-content-editbox-5 .is-box-6,.md-modal.stuckright #tab-content-editbox-10 .is-box-6,#md-editsection.stuckright .is-box-6{
  width: 100%;
}
.md-modal.stuckright .md-content{
  height: 100%;
}
.md-modal.stuckright{
  transform: none;
  transform: translateX(540px) !important;
  visibility: visible;
  display: block !important;
  margin-top: 0px;
  -webkit-transition: all 0.1s ease-in-out;
  transition: all 0.1s ease-in-out;
}
.md-modal.stuckright .md-content {
    border-radius: 0;
    transform: none !important;
    opacity: 1 !important;
}
.md-modal.stuckright.md-show{
  transform: translateX(0px) !important;
}
.md-modal.stuckright #tab-content-editbox-2 .is-box-2 {
    width: 25.666667%;
}
.md-modal.stuckright #tab-content-editbox-5 .is-box-4 {
    width: 24.333333%;
}
.md-modal.stuckright .is-tab-contents{
  max-height: none;
}
#md-editbox.stuckright {
  margin-top: 0px;
}
.saved-section .is-box-centered div:after{
  content: 'This is a Global Section. Please edit in Sections page';
display: block;
margin: 10px;
font-size: 13px;
}
  </style>
	@endif

	@if(!empty(getSetting('imported_font_link','styles')))
		@foreach(getSetting('imported_font_link','styles') as $links)
			<link href="{!! $links !!}" rel="stylesheet">
		@endforeach
	@endif



	<style>
	@if(!empty($page->sidebar) && $page->sidebar!='')
		.is-wrapper .is-section > .is-boxes{
			float:none;
			max-width:1170px;
			margin:0 auto;
		}
	@endif

  @if($preview)
  body{
    opacity: 0;
    transition: opacity 1s;
  }
  body.pageloaded{
    opacity: 1;
  }
  @endif
	</style>

	@if($preview)
	{!! !empty(getSetting('custom_code_header_others','custom')) ? getSetting('custom_code_header_others','custom') : '' !!}
	<script>
	{!! !empty(getSetting('custom_code_header_js','custom')) ? getSetting('custom_code_header_js','custom') : '' !!}
	</script>
	@endif

	<script type="text/javascript" src="{{ asset('assets/public/js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/public/js/jquery.twbsPagination.min.js') }}"></script>

	<link href="{{ asset('assets/public/css/aos.css') }}" rel="stylesheet" type="text/css" />

	@if($preview)
	<script src="{{ asset('assets/public/js/jquery.stellar.min.js') }}"></script>

	<script>
	jQuery(document).ready(function(){
		jQuery('.is-parallax').each(function(){
      jQuery(this).attr("data-stellar-background-ratio", jQuery(this).attr('data-background-ratio'));
			jQuery(this).attr("data-stellar-vertical-offset", jQuery(this).attr('data-vertical-offset'));
		})
		jQuery(window).stellar({
			horizontalScrolling: false
		});
    $('body').addClass('pageloaded');
    $('[data-link]').each(function(){
        if(jQuery(this).data('link')!=''){
          var target = '_self';
          if(jQuery(this).data('target')){
            target = '_blank';
          }
          jQuery(this).append('<a href="'+jQuery(this).data('link')+'" target="'+target+'" class="box_link">&nbsp;</a>')
        }
    })
	})
	</script>
	<style>
	.bootstrap removable{
		display: none;
	}
	</style>
	@endif

	@if(!empty($type) && $type=='popup')
	<style>
	.is-wrapper{
		position: relative;
    height: auto;
		@if(unserialize($page->settings)['width']!='')
			max-width: {{ unserialize($page->settings)['width'] }}px
		@endif
	}
  div#is-wrapper:before,div#is-wrapper:after{
      display: table;
      content: " ";
  }
  div#is-wrapper:after{
    clear: both;
  }
  .is-section-remove,.is-section-edit{
    display:none;
  }

	</style>
	@endif

  @if(!empty($type) && $type=='section')
  <style>
  .is-section-remove,.is-section-global,.is-section-duplicate{
    display:none;
  }
	</style>
  @endif

@if(getSetting('pagePrivacy', 'firebase') || getSetting('formEntrySave', 'firebase'))
<script src="https://www.gstatic.com/firebasejs/7.14.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.1/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.1/firebase-firestore.js"></script>
<script type = "text/javascript">
    var firebaseConfig = {
      apiKey: "{{ getSetting('apiKey', 'firebase') }}",
      authDomain: "{{ getSetting('authDomain', 'firebase') }}",
      databaseURL: "{{ getSetting('databaseURL', 'firebase') }}",
      projectId: "{{ getSetting('projectId', 'firebase') }}",
      storageBucket: "{{ getSetting('storageBucket', 'firebase') }}",
      messagingSenderId: "{{ getSetting('messagingSenderId', 'firebase') }}",
      appId: "{{ getSetting('appId', 'firebase') }}"
    };
    firebase.initializeApp(firebaseConfig);
</script>
@endif

  	@if($preview)
      @if(!empty($page->password))
      <script type = "text/javascript">
          var dbRef = firebase.database().ref('pages/{{ $page->id }}');
          var pass,html;
          dbRef.once('value').then(function(snapshot) {
              pass = snapshot.val().password;
              html = snapshot.val().html;
          });
          $(document).ready(function(){
              //console.log(accessCookie('page{{ $page->id }}'));
              if(accessCookie('page{{ $page->id }}')){
                $('#pagepwd').val(accessCookie('page{{ $page->id }}'));
                $('#pageloginremember').prop('checked', true);
              }
              $('#pagesubmitpass').on('click', function(){
                  $('#pagepasswordform .alert').remove();
                  if(window.btoa($('#pagepwd').val())==pass){
                      if($('#pageloginremember').is(":checked")){
                        createCookie('page{{ $page->id }}',$('#pagepwd').val(),7);
                      }else{
                        createCookie('page{{ $page->id }}',$('#pagepwd').val(),-1);
                      }

                      $(html).insertAfter('#pagepassform');
                      $('#pagepassform').remove();
                  }else{
                      $('#pagepasswordform').prepend('<div class="alert alert-danger">Password Mismatch!!</div>');
                  }
              })
          })
          function createCookie(cookieName,cookieValue,daysToExpire)
          {
                var date = new Date();
                date.setTime(date.getTime()+(daysToExpire*24*60*60*1000));
                document.cookie = cookieName + "=" + cookieValue + "; expires=" + date.toGMTString();
          }
      		function accessCookie(cookieName)
          {
                var name = cookieName + "=";
                var allCookieArray = document.cookie.split(';');
                for(var i=0; i<allCookieArray.length; i++)
                {
                  var temp = allCookieArray[i].trim();
                  if (temp.indexOf(name)==0)
                  return temp.substring(name.length,temp.length);
             	  }
              	return "";
          }
      </script>
      @endif
  	@endif

    @if($publish)
  	<link href="{{ asset('assets/theme.css') }}" rel="stylesheet">
    @else
  	<link href="{{ asset('html/assets/theme-demo.css') }}" rel="stylesheet">
    @endif


</head>
<body class="page-{{ $page->id }} {!! !empty(getSetting('header','header')) ? getSetting('header','header') : '' !!}">


@if(!$preview)
<a href="#" id="hide_show_builder"><i class="fa fa-angle-double-left"></i><i class="fa fa-angle-double-right active"></i></a>
<div id="sticky-nav-sticky-wrapper" class="builder_controls text-center">
	<div class="container-fluid">
		<ul class="nav">
			<li class="dropdown">
				<a href="javascript:void(0);" class="btn btn-primary" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-caret-left"></i></a>
				<ul class="dropdown-menu">
					<li><a href="{{ config('app.url', 'Laravel') }}">Dashboard</a></li>
					<li><a href="{{ route('settings.header') }}">Header</a></li>
					<li><a href="{{ route('menu.index') }}">Menu</a></li>
					<li><a href="{{ route('settings.styles') }}">Styles</a></li>
					<li><a href="{{ route('settings.footer') }}">Footer</a></li>
					<li><a href="{{ route('settings.custom') }}">Custom</a></li>
					<li><a href="{{URL::route('media.index')}}">Media</a></li>
					<li><a href="{{URL::route('pages.index')}}">Pages</a></li>
					<li><a href="{{URL::route('posts.index')}}">Posts</a></li>
          <li><a href="{{URL::route('sections.index')}}">Sections</a></li>
					<li><a href="{{URL::route('widgets.index')}}">Widgets</a></li>
				</ul>
			</li>

      @if($type=='page')
  			<li><a href="{{URL::route('pages.preview', $page->id)}}" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
      @elseif($type=='post')
  			<li><a href="{{URL::route('posts.preview', $page->id)}}" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
      @elseif($type=='section')
  			<li><a href="{{URL::route('sections.preview', $page->id)}}" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
  		@endif
      <li><a href="#" id="save-html"><i class="fa fa-save" aria-hidden="true"></i></a></li>
			<li><a href="#" id="view-html"><i class="cb-icon-code"></i></a></li>
		@if($type!='popup')
			@if(empty($page->sidebar) && $type!='section')
			<li><a href="#" id="add-section" title="Add Section"><i class="cb-icon-plus"></i></a></li>
      <li><a href="#" id="add-saved-section" title="Global Section" style="position: relative;"><i class="cb-icon-plus" style="color: rgb(0, 172, 214);"></i><i class="cb-icon-plus" style="position: absolute;left: 10px;top: 15px;"></i><i class="cb-icon-plus" style="position: absolute;left: -1px;top: 15px;"></i></a></li>
			@endif
		@endif


		</ul>
	</div>
</div>
@endif



@if (\Session::has('success') && !$preview)
    <div class="alert pagealert alert-success">
        {!! \Session::get('success') !!}
    </div>
	<script>
	$(document).ready(function(){
		setTimeout(function(){ $('.alert-success').fadeOut(); }, 2000);
	})
	</script>
@endif

<div id="is-wrapper" class="is-wrapper">
<div class="custom_code_body">
@if($preview)
	{!! !empty(getSetting('custom_code_body','custom')) ? filterOutput(getSetting('custom_code_body','custom')) : '' !!}
@endif
</div>
<div class="header">
@if(empty(getSetting('hide_header','header')))
  @yield('header')
@endif
</div>
@if($page->html)
	@if($preview)
    @if(!empty($page->password))
        <div id="pagepassform" class="is-section is-box is-bg-grey is-section-100">
            <div class="is-overlay">
              <div class="is-overlay-bg" style=""></div>
              <div class="is-overlay-color"></div>
              <div class="is-overlay-content"></div>
            </div>
            <div class="is-boxes">
              <div class="is-box-centered">
                <div class="is-container is-builder is-content-640">
                    <div class="row">
                        <div id="pagepasswordform" class="col-md-12">
                          <div class="form-group">
                          <label for="pwd">Password:</label>
                          <input type="password" class="form-control" id="pagepwd">
                          </div>
                          <div class="check" style="margin-bottom: 20px;">
                              <input type="checkbox" id="pageloginremember">
                              <label for="pageloginremember"> Remember me</label>
                          </div>
                          <button type="button" id="pagesubmitpass" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
    @else
		    {!! filterOutput($page->html) !!}
    @endif
	@else
		{!! $page->html !!}
	@endif
@else
	@if(!empty($page->sidebar) && $page->sidebar=='left')
	<div class="is-section is-light-text is-box is-bg-grey is-section-100">
		<div class="is-overlay">
			<div class="is-overlay-bg" style="background-image:url(http://admin.frezit.com/contentbuilder/contentbox/images/sample.jpg)"></div>
			<div class="is-overlay-color"></div>
			<div class="is-overlay-content"></div>
		</div>
		<div class="is-boxes">
			<div class="is-box-3">
				<div class="is-container is-builder container-fluid">
					<div class="row">
						<div class="col-md-12">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="is-box-9">
				<div class="is-container is-builder container-fluid">
				@yield('wrapper')
				</div>
			</div>
		</div>
	</div>
	@elseif(!empty($page->sidebar) && $page->sidebar=='right')
	<div class="is-section is-light-text is-box is-bg-grey is-section-100">
		<div class="is-overlay">
			<div class="is-overlay-bg" style="background-image:url(http://admin.frezit.com/contentbuilder/contentbox/images/sample.jpg)"></div>
			<div class="is-overlay-color"></div>
			<div class="is-overlay-content"></div>
		</div>
		<div class="is-boxes">
			<div class="is-box-9">
				<div class="is-container is-builder container-fluid">
				@yield('wrapper')
				</div>
			</div>
			<div class="is-box-3">
				<div class="is-container is-builder container-fluid">
					<div class="row">
						<div class="col-md-12">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante, consectetur sit amet vulputate vel, dapibus sit amet lectus.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@else
	<div class="is-section is-light-text is-box is-bg-grey is-section-100">
		<div class="is-overlay">
			<div class="is-overlay-bg" style="background-image:url(http://admin.frezit.com/contentbuilder/contentbox/images/sample.jpg)"></div>
			<div class="is-overlay-color"></div>
			<div class="is-overlay-content"></div>
		</div>
		<div class="is-boxes">
			<div class="is-box-centered is-opacity-90">
				<div class="is-container is-builder container-fluid">
				@yield('wrapper')
				</div>
			</div>
		</div>
	</div>
	@endif
@endif

@if($preview)
@yield('footer')
@endif

</div>

@if(!$preview)
<form id="form1" method="post" style="display:none" action="
@if($type=='page')
  {{ route("pages.savehtml") }}
@elseif($type=='popup')
  {{ route("popups.savehtml") }}
@elseif($type=='section')
  {{ route("sections.savehtml") }}
@elseif($type=='post')
  {{ route("posts.savehtml") }}
@endif
">
	{{csrf_field()}}
	<input type="hidden" id="pageid" name="pageid" value="{{ $page->id }}" />
	<input type="hidden" id="hidContent" name="hidContent" />
	<input type="submit" id="btnPost" value="submit" />
</form>
@endif



<div class="imageselect">
@if(!$preview)
	@include('admin._partials.popupuploader')
@endif
</div>


<div class="coverselect">
@if(!$preview)
	@include('admin._partials.coverimage')
@endif
</div>





<script src="{{ asset('assets/public/js/aos.js') }}"></script>
<script>
	AOS.init();
</script>

@if(!$preview)
<script src="{{ asset('assets/private/js/classie.js') }}"></script>
<script src="{{ asset('assets/private/js/modalEffects.js') }}"></script>
<script>
  var polyfilter_scriptpath = '{{ asset("assets/private/js") }}/';
</script>
<script src="{{ asset('assets/private/js/cssParser.js') }}"></script>
<script src="{{ asset('assets/private/js/css-filters-polyfill.js') }}"></script>



<script src="{{ asset('builder/contentbuilder/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('builder/contentbuilder/contentbuilder.js') }}" type="text/javascript"></script>
<script src="{{ asset('builder/contentbuilder/saveimages.js') }}" type="text/javascript"></script>
<script src="{{ asset('builder/contentbox/builderbox.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/private/plugins/simplelightbox/simple-lightbox.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/private/js/dh_dividers.js') }}" type="text/javascript"></script>

@endif
<script src="{{ asset('assets/public/js/box.js') }}" type="text/javascript"></script>
@if(!$preview)
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<script>
jQuery(document).ready(function($){
	$("#is-wrapper").contentbox({
		zoom: 0.85,
		snippetFile: '{{ route("pages.snippets") }}',
		contentHtmlStart: '<div class="is-container is-builder container-fluid"><div class="row"><div class="col-md-12">',
		contentHtmlEnd: '</div></div></div>',
		coverImageHandler: '{{ route("pages.savecover") }}',
		//largerImageHandler: 'saveimage-large.php', /* for uploading larger image */
		onRender: function () {
			$('a.is-lightbox').simpleLightbox({ closeText: '<i style="font-size:35px" class="icon ion-ios-close-empty"></i>', navText: ['<i class="icon ion-ios-arrow-left"></i>', '<i class="icon ion-ios-arrow-right"></i>'], disableScroll: false });
			$('.row').each(function(){
				if($(this).find('div[class*="col-"]').length>0){
					if($(this).attr('contenteditable')){
						$(this).removeAttr('contenteditable');
						$(this).find('div[class*="col-"]').attr('contenteditable', 'true');
					}
				}
			})
		},
		onImageBrowseClick: function(){
			$('#media-select').addClass('md-custom-show');
			$('#active-input').attr('value','txtImgUrl');

		},
		//imageselect: '{{ route("media.select") }}',
		iconselect: '{{ asset('builder/contentbox/ionicons/selecticon-dark.html') }}',
  //  onDrop: function () { $('.customTools').hide(); },
		snippetCustomCode: true,
		toolbar: 'bottom',
		snippetCategories: [
				[-1, "All"],
                [120, "Basic"],
				{!! getCustomSnippetCategories() !!}
                [119, "Buttons"],
                [116, "Contact"],
                [107, "Pricing"],
                [110, "Quotes"],
                [111, "Partners"],
                [113, "Page Not Found"],
                [114, "Coming Soon"],
                ]
	});
	$("#add-section").click(function () {
            $('#is-wrapper').data('contentbox').addSection(); //To add new section, call addSection().
            return false;
    });
	$('#hide_show_builder').on('click', function(){
		$('#sticky-nav-sticky-wrapper').toggleClass('collide');
    $('body').toggleClass('control-collide');
		$('#sticky-nav-sticky-wrapper-sticky-wrapper').toggleClass('collide');
		$('#hide_show_builder .fa').toggleClass('active');
		return false;
	})
	$("#view-html").click(function () {
		$('#is-wrapper').data('contentbox').viewHtml();
		return false;
	});
	$("#save-html").click(function () {
		/* $('img.ui-resizable').resizable( "destroy" ); */
		$('.aos-animate').each(function(){
			$(this).removeClass('aos-animate');
		})
		$("#is-wrapper").saveimages({
			handler: '{{ route("pages.saveimage") }}', // or saveimage.php - for saving embedded base64 image to image file
			onComplete: function () {
				//Save Content
				var sHTML = $('#is-wrapper').data('contentbox').html();
				$('#hidContent').val(sHTML);
				$('#btnPost').click();
			}
		});
		$("#is-wrapper").data('saveimages').save();
		return false;
	});
	$('.imageselect').on('click','.imageholder a',function(){
		$('#lnkImageSettings').trigger('click');
		$('body #chkCrop').prop('checked', true);
		$('#'+$('#active-input').val()).val($(this).attr('href'));
		$('#media-select').removeClass('md-custom-show');
		return false;
	})
	$('.coverselect').on('click','.imageholder a',function(){
		parent.applyBoxImage($(this).attr('href'));
		$('#cover-select').removeClass('md-custom-show');
		return false;
	})
	$('body').on('click', '#btnImgOk', function(){
		if($('input#chkCrop').is(':checked')){
		jQuery("#divToolImg").stop(true, true).fadeOut(0);
		jQuery("#divToolImgSettings").stop(true, true).fadeOut(0);
		jQuery('.overlay-bg').css('width', '100%');
		jQuery('.overlay-bg').css('height', '100%');
		//jQuery('body').css('overflow', 'hidden'); // This makes unwanted zoom-in in iOS Safari
		jQuery("#divToolImgLoader").css('top', jQuery('#divToolImg').css('top'));
		jQuery("#divToolImgLoader").css('left', jQuery('#divToolImg').css('left'));
		jQuery("#divToolImgLoader").css('display', 'block');
		}
	})
  /*
	$('body').on('click', '.is-boxes img', function(e){
		$(this).resizable({
			aspectRatio: true,
		});
		$(this).attr('draggable', false);
		e.stopPropagation();
	})
  */
	$(document).click(function(e) {
    /*$('img.ui-resizable').resizable( "destroy" ); */
		if(!$(e.target).hasClass('ui-draggable') && !$(e.target).parents().hasClass('ui-draggable')){
			$('.customTools').css('opacity', 0);
				$('.customTools').hide();
		}
	});

$(document).contextmenu(function() {
  /* $('img.ui-resizable').resizable( "destroy" ); */
  $('.customTools').css('opacity', 0);
    $('.customTools').hide();
    $('.row-tool').hide();
});


@if(sizeof($uploads)==15)

	$('#media-select .allmedia').scroll(function(){
		$('#media-select .allmedia').infiniteScroll({
		  path: '#media-select .pagination li a[rel="next"]',
		  append: '#media-select .allmedia .imageholder',
		  history: false,
		  prefill: true
		});
	})
	$('#cover-select .allmedia').scroll(function(){
		$('#cover-select .allmedia').infiniteScroll({
		  path: '#cover-select .pagination li a[rel="next"]',
		  append: '#cover-select .allmedia .imageholder',
		  history: false,
		  prefill: true
		});
	})

@endif

})
jQuery(document).ready(function($){
    jQuery('.md-modal').each(function(){
      if(jQuery(this).attr('id')!='md-widgetselect' && jQuery(this).attr('id')!='md-html' && jQuery(this).attr('id')!='md-view-html' && jQuery(this).attr('id')!='md-insertimage' && jQuery(this).attr('id')!='md-icon-select' && jQuery(this).attr('id')!='md-createlink' && jQuery(this).attr('id')!='md-delsectionconfirm' && jQuery(this).attr('id')!='md-delrowconfirm' && jQuery(this).attr('id')!='md-addsection' && jQuery(this).attr('id')!='md-addsavedsection' && jQuery(this).attr('id')!='md-makeglobal' && jQuery(this).attr('id')!='md-delcolconfirm' && jQuery(this).attr('id')!='md-customcolor'){
        jQuery(this).addClass('stuckright');
        jQuery(this).find('.md-title').append('<a class="lock-unlock" href="#"><i class="fas fa-lock"></i></a>');
      }
    })
    jQuery(document).on('click','*, .md-overlay',function(){
      setTimeout(function(){
        if(jQuery('.stuckright.md-show').length>0){
          jQuery('body').addClass('toolopen');
        }else{
          jQuery('body').removeClass('toolopen');
        }
      }, 400);
    })
    jQuery('body').on('click','.lock-unlock',function(){
      jQuery(this).parents('.md-modal').toggleClass('stuckright');
      jQuery(this).find('i').toggleClass('fa-lock');
      jQuery(this).find('i').toggleClass('fa-unlock-alt');
    })
})
</script>
<script src="{{ asset('builder/builderscript.js') }}" type="text/javascript"></script>

@endif
@if(!$preview)

	@include('admin._partials.buildercustomfeatures')

@endif
@if($preview)

  @if(WidgetExists('advanced_slider_widget'))
      <script type="text/javascript" src="{{ asset('assets/public/js/jquery.themepunch.tools.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/public/js/jquery.themepunch.revolution.min.js') }}"></script>
  @endif

  @if(WidgetExists('animated_counter'))
      <script src="{{ asset('assets/public/js/progressbar.min.js') }}"></script>
  @endif

  @if(WidgetExists('countdown_timer'))
      <script src="{{ asset('assets/public/js/soon.min.js') }}" data-auto="false"></script>
  @endif

  @if(WidgetExists('image_mapping_widget'))
      <script src="{{ asset('assets/public/js/image-map-pro.min.js') }}"></script>
  @endif

  @if(WidgetExists('image_scroller_widget'))
      <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  @endif

  @if(WidgetExists('instagram_widget'))
      <script src="{{ asset('assets/public/js/instafeed.min.js') }}"></script>
  @endif

  @if(WidgetExists('isotope_gallery_widget'))
      <script type="text/javascript" src="{{ asset('assets/public/js/imagesloaded.pkgd.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/public/js/isotope.pkgd.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/public/js/packery-mode.pkgd.min.js') }}"></script>
  @endif

  @if(WidgetExists('simple_slider_widget'))
      <script src="{{ asset('assets/public/js/owl.carousel.min.js') }}"></script>
  @endif

  @if(WidgetExists('site_search'))
      <script src="{{ asset('assets/public/js/tipuesearch.min.js') }}"></script>
  @endif

  @if(WidgetExists('social_share_widget'))
    <script async src="https://static.addtoany.com/menu/page.js"></script>
  @endif



	{!! !empty(getSetting('custom_code_footer','custom')) ? getSetting('custom_code_footer','custom') : '' !!}
  {!! getPopups() !!}
<script src="{{ asset('assets/public/js/jquery.fittext.min.js') }}"></script>
<script>
  $("h1, h2, h3, h4, h5, h6").each(function(){
        $(this).attr('data-maxfont', $(this).css('font-size'));
    })

  $(document).ready(function() {
   var url = window.location.href;
   url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
   url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
   url = url.substr(url.lastIndexOf("/") + 1);
   if(url == ''){
   url = '/';
   }
   $('#navbar-menu li').each(function(){
    var href = $(this).find('a').attr('href');
    if(url == href){
     var parentClass = $(this).parent('ul').attr('class');
     if(parentClass == 'sub-menu'){
      $(this).addClass('subactive');
      $(this).parents('.menu li').addClass('active');
     }else{
      $(this).addClass('active');
     }
    }
   });
if($(window).width()<768){
$("h1, h2, h3, h4, h5, h6").each(function(){
  var thisele = $(this);
  thisele.fitText(.9, {
     minFontSize: '16px',
     maxFontSize: thisele.data('maxfont')
   });
})
}

$("a:not(.card-link):not(.no-animation)").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
        window.location.hash = hash;
      });
    }
  });

  });
  $(window).resize(function() {
    if($(window).width()<768){
    $("h1, h2, h3, h4, h5, h6").each(function(){
      var thisele = $(this);
      thisele.fitText(.9, {
         minFontSize: '16px',
         maxFontSize: thisele.data('maxfont')
       });
    })
    }
});
</script>
<script src="{{ asset('assets/public/js/jquery.easeScroll.js') }}"></script>
<script>
$(document).ready(function(){
  $("html").easeScroll();
})
</script>
@endif
</body>
</html>
