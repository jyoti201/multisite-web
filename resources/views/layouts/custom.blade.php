	body{
		@if(!empty(getSetting('body_bgtype','styles')) && getSetting('body_bgtype','styles')=='bgimage')
			background-image: url({{ !empty(getSetting('body_bgimage','styles')) ? getSetting('body_bgimage','styles') : "" }});
			background-size: cover;
			background-repeat: no-repeat;
			background-position: top center;
		@elseif(!empty(getSetting('body_bgtype','styles')) && getSetting('body_bgtype','styles')=='bgcolor')
			background: {{ !empty(getSetting('body_bgcolor','styles')) ? getSetting('body_bgcolor','styles') : "rgba(255,255,255,1)" }};
		@elseif(!empty(getSetting('body_bgtype','styles')) && getSetting('body_bgtype','styles')=='bggradientcolor')
			background:{{ !empty(getSetting('body_bggradientcolor_from','styles')) ? getSetting('body_bggradientcolor_from','styles') : "" }};
			background:-moz-linear-gradient({{ !empty(getSetting('body_bggradientcolor_angle','styles')) ? getSetting('body_bggradientcolor_angle','styles') : "0" }}deg, {{ !empty(getSetting('body_bggradientcolor_from','styles')) ? getSetting('body_bggradientcolor_from','styles') : "" }} 0%, {{ !empty(getSetting('body_bggradientcolor_to','styles')) ? getSetting('body_bggradientcolor_to','styles') : "" }} 100%);
			background:-webkit-linear-gradient({{ !empty(getSetting('body_bggradientcolor_angle','styles')) ? getSetting('body_bggradientcolor_angle','styles') : "0" }}deg, {{ !empty(getSetting('body_bggradientcolor_from','styles')) ? getSetting('body_bggradientcolor_from','styles') : "" }} 0%, {{ !empty(getSetting('body_bggradientcolor_to','styles')) ? getSetting('body_bggradientcolor_to','styles') : "" }} 100%);
			background:linear-gradient({{ !empty(getSetting('body_bggradientcolor_angle','styles')) ? getSetting('body_bggradientcolor_angle','styles') : "0" }}deg, {{ !empty(getSetting('body_bggradientcolor_from','styles')) ? getSetting('body_bggradientcolor_from','styles') : "" }} 0%, {{ !empty(getSetting('body_bggradientcolor_to','styles')) ? getSetting('body_bggradientcolor_to','styles') : "" }} 100%);
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='{{ !empty(getSetting('body_bggradientcolor_from','styles')) ? getSetting('body_bggradientcolor_from','styles') : "" }}',endColorstr='{{ !empty(getSetting('body_bggradientcolor_to','styles')) ? getSetting('body_bggradientcolor_to','styles') : "" }}',GradientType=1);
		@endif
	}
	body{
		@if(!empty(getSetting('body_font','styles')))
			font-family: "{{ getSetting('body_font','styles') }}";
		@endif
		@if(!empty(getSetting('body_font_size','styles')))
			font-size: {{ getSetting('body_font_size','styles') }};
		@endif
		@if(!empty(getSetting('body_font_color','styles')))
			color: {{ getSetting('body_font_color','styles') }};
		@endif
		@if(!empty(getSetting('body_font_weight','styles')))
			font-weight: {{ getSetting('body_font_weight','styles') }};
		@endif
		@if(!empty(getSetting('body_line_height','styles')))
			line-height: {{ getSetting('body_line_height','styles') }};
		@endif
		@if(!empty(getSetting('body_letter_spacing','styles')))
			letter-spacing: {{ getSetting('body_letter_spacing','styles') }};
		@endif
	}
	p{
		@if(!empty(getSetting('p_font','styles')))
			font-family: "{{ getSetting('p_font','styles') }}";
		@endif
		@if(!empty(getSetting('p_font_size','styles')))
			font-size: {{ getSetting('p_font_size','styles') }};
		@endif
		@if(!empty(getSetting('p_font_color','styles')))
			color: {{ getSetting('p_font_color','styles') }};
		@endif
		@if(!empty(getSetting('p_font_weight','styles')))
			font-weight: {{ getSetting('p_font_weight','styles') }};
		@endif
		@if(!empty(getSetting('p_line_height','styles')))
			line-height: {{ getSetting('p_line_height','styles') }};
		@endif
		@if(!empty(getSetting('p_letter_spacing','styles')))
			letter-spacing: {{ getSetting('p_letter_spacing','styles') }};
		@endif
		@if(!empty(getSetting('p_margin_top','styles')))
			margin-top: {{ getSetting('p_margin_top','styles') }};
		@endif
		@if(!empty(getSetting('p_margin_bottom','styles')))
			margin-bottom: {{ getSetting('p_margin_bottom','styles') }};
		@endif
		@if(!empty(getSetting('p_padding_top','styles')))
			padding-top: {{ getSetting('p_padding_top','styles') }};
		@endif
		@if(!empty(getSetting('p_padding_bottom','styles')))
			padding-bottom: {{ getSetting('p_padding_bottom','styles') }};
		@endif
	}
	h1,.h1{
		@if(!empty(getSetting('h1_font','styles')))
			font-family: "{{ getSetting('h1_font','styles') }}";
		@endif
		@if(!empty(getSetting('h1_font_size','styles')))
			font-size: {{ getSetting('h1_font_size','styles') }};
		@endif
		@if(!empty(getSetting('h1_font_color','styles')))
			color: {{ getSetting('h1_font_color','styles') }};
		@endif
		@if(!empty(getSetting('h1_font_weight','styles')))
			font-weight: {{ getSetting('h1_font_weight','styles') }};
		@endif
		@if(!empty(getSetting('h1_line_height','styles')))
			line-height: {{ getSetting('h1_line_height','styles') }};
		@endif
		@if(!empty(getSetting('h1_letter_spacing','styles')))
			letter-spacing: {{ getSetting('h1_letter_spacing','styles') }};
		@endif
		@if(!empty(getSetting('h1_margin_top','styles')))
		  margin-top: {{ getSetting('h1_margin_top','styles') }};
		@endif
		@if(!empty(getSetting('h1_margin_bottom','styles')))
		  margin-bottom: {{ getSetting('h1_margin_bottom','styles') }};
		@endif
		@if(!empty(getSetting('h1_padding_top','styles')))
		  padding-top: {{ getSetting('h1_padding_top','styles') }};
		@endif
		@if(!empty(getSetting('h1_padding_bottom','styles')))
		  padding-bottom: {{ getSetting('h1_padding_bottom','styles') }};
		@endif
	}
	h2,.h2{
		@if(!empty(getSetting('h2_font','styles')))
			font-family: "{{ getSetting('h2_font','styles') }}";
		@endif
		@if(!empty(getSetting('h2_font_size','styles')))
			font-size: {{ getSetting('h2_font_size','styles') }};
		@endif
		@if(!empty(getSetting('h2_font_color','styles')))
			color: {{ getSetting('h2_font_color','styles') }};
		@endif
		@if(!empty(getSetting('h2_font_weight','styles')))
			font-weight: {{ getSetting('h2_font_weight','styles') }};
		@endif
		@if(!empty(getSetting('h2_line_height','styles')))
			line-height: {{ getSetting('h2_line_height','styles') }};
		@endif
		@if(!empty(getSetting('h2_letter_spacing','styles')))
			letter-spacing: {{ getSetting('h2_letter_spacing','styles') }};
		@endif
		@if(!empty(getSetting('h2_margin_top','styles')))
		  margin-top: {{ getSetting('h2_margin_top','styles') }};
		@endif
		@if(!empty(getSetting('h2_margin_bottom','styles')))
		  margin-bottom: {{ getSetting('h2_margin_bottom','styles') }};
		@endif
		@if(!empty(getSetting('h2_padding_top','styles')))
		  padding-top: {{ getSetting('h2_padding_top','styles') }};
		@endif
		@if(!empty(getSetting('h2_padding_bottom','styles')))
		  padding-bottom: {{ getSetting('h2_padding_bottom','styles') }};
		@endif
	}
	h3,.h3{
		@if(!empty(getSetting('h3_font','styles')))
			font-family: "{{ getSetting('h3_font','styles') }}";
		@endif
		@if(!empty(getSetting('h3_font_size','styles')))
			font-size: {{ getSetting('h3_font_size','styles') }};
		@endif
		@if(!empty(getSetting('h3_font_color','styles')))
			color: {{ getSetting('h3_font_color','styles') }};
		@endif
		@if(!empty(getSetting('h3_font_weight','styles')))
			font-weight: {{ getSetting('h3_font_weight','styles') }};
		@endif
		@if(!empty(getSetting('h3_line_height','styles')))
			line-height: {{ getSetting('h3_line_height','styles') }};
		@endif
		@if(!empty(getSetting('h3_letter_spacing','styles')))
			letter-spacing: {{ getSetting('h3_letter_spacing','styles') }};
		@endif
		@if(!empty(getSetting('h3_margin_top','styles')))
		  margin-top: {{ getSetting('h3_margin_top','styles') }};
		@endif
		@if(!empty(getSetting('h3_margin_bottom','styles')))
		  margin-bottom: {{ getSetting('h3_margin_bottom','styles') }};
		@endif
		@if(!empty(getSetting('h3_padding_top','styles')))
		  padding-top: {{ getSetting('h3_padding_top','styles') }};
		@endif
		@if(!empty(getSetting('h3_padding_bottom','styles')))
		  padding-bottom: {{ getSetting('h3_padding_bottom','styles') }};
		@endif
	}
	h4,.h4{
		@if(!empty(getSetting('h4_font','styles')))
			font-family: "{{ getSetting('h4_font','styles') }}";
		@endif
		@if(!empty(getSetting('h4_font_size','styles')))
			font-size: {{ getSetting('h4_font_size','styles') }};
		@endif
		@if(!empty(getSetting('h4_font_color','styles')))
			color: {{ getSetting('h4_font_color','styles') }};
		@endif
		@if(!empty(getSetting('h4_font_weight','styles')))
			font-weight: {{ getSetting('h4_font_weight','styles') }};
		@endif
		@if(!empty(getSetting('h4_line_height','styles')))
			line-height: {{ getSetting('h4_line_height','styles') }};
		@endif
		@if(!empty(getSetting('h4_letter_spacing','styles')))
			letter-spacing: {{ getSetting('h4_letter_spacing','styles') }};
		@endif
		@if(!empty(getSetting('h4_margin_top','styles')))
		  margin-top: {{ getSetting('h4_margin_top','styles') }};
		@endif
		@if(!empty(getSetting('h4_margin_bottom','styles')))
		  margin-bottom: {{ getSetting('h4_margin_bottom','styles') }};
		@endif
		@if(!empty(getSetting('h4_padding_top','styles')))
		  padding-top: {{ getSetting('h4_padding_top','styles') }};
		@endif
		@if(!empty(getSetting('h4_padding_bottom','styles')))
		  padding-bottom: {{ getSetting('h4_padding_bottom','styles') }};
		@endif
	}
	h5,.h5{
		@if(!empty(getSetting('h5_font','styles')))
			font-family: "{{ getSetting('h5_font','styles') }}";
		@endif
		@if(!empty(getSetting('h5_font_size','styles')))
			font-size: {{ getSetting('h5_font_size','styles') }};
		@endif
		@if(!empty(getSetting('h5_font_color','styles')))
			color: {{ getSetting('h5_font_color','styles') }};
		@endif
		@if(!empty(getSetting('h5_font_weight','styles')))
			font-weight: {{ getSetting('h5_font_weight','styles') }};
		@endif
		@if(!empty(getSetting('h5_line_height','styles')))
			line-height: {{ getSetting('h5_line_height','styles') }};
		@endif
		@if(!empty(getSetting('h5_letter_spacing','styles')))
			letter-spacing: {{ getSetting('h5_letter_spacing','styles') }};
		@endif
		@if(!empty(getSetting('h5_margin_top','styles')))
		  margin-top: {{ getSetting('h5_margin_top','styles') }};
		@endif
		@if(!empty(getSetting('h5_margin_bottom','styles')))
		  margin-bottom: {{ getSetting('h5_margin_bottom','styles') }};
		@endif
		@if(!empty(getSetting('h5_padding_top','styles')))
		  padding-top: {{ getSetting('h5_padding_top','styles') }};
		@endif
		@if(!empty(getSetting('h5_padding_bottom','styles')))
		  padding-bottom: {{ getSetting('h5_padding_bottom','styles') }};
		@endif
	}
	h6,.h6{
		@if(!empty(getSetting('h6_font','styles')))
			font-family: "{{ getSetting('h6_font','styles') }}";
		@endif
		@if(!empty(getSetting('h6_font_size','styles')))
			font-size: {{ getSetting('h6_font_size','styles') }};
		@endif
		@if(!empty(getSetting('h6_font_color','styles')))
			color: {{ getSetting('h6_font_color','styles') }};
		@endif
		@if(!empty(getSetting('h6_font_weight','styles')))
			font-weight: {{ getSetting('h6_font_weight','styles') }};
		@endif
		@if(!empty(getSetting('h6_line_height','styles')))
			line-height: {{ getSetting('h6_line_height','styles') }};
		@endif
		@if(!empty(getSetting('h6_letter_spacing','styles')))
			letter-spacing: {{ getSetting('h6_letter_spacing','styles') }};
		@endif
		@if(!empty(getSetting('h6_margin_top','styles')))
		  margin-top: {{ getSetting('h6_margin_top','styles') }};
		@endif
		@if(!empty(getSetting('h6_margin_bottom','styles')))
		  margin-bottom: {{ getSetting('h6_margin_bottom','styles') }};
		@endif
		@if(!empty(getSetting('h6_padding_top','styles')))
		  padding-top: {{ getSetting('h6_padding_top','styles') }};
		@endif
		@if(!empty(getSetting('h6_padding_bottom','styles')))
		  padding-bottom: {{ getSetting('h6_padding_bottom','styles') }};
		@endif
	}
	a:not(.button){
		@if(!empty(getSetting('link_font','styles')))
			font-family: "{{ getSetting('link_font','styles') }}";
		@endif
		@if(!empty(getSetting('link_font_size','styles')))
			font-size: {{ getSetting('link_font_size','styles') }};
		@endif
		@if(!empty(getSetting('link_font_color','styles')))
			color: {{ getSetting('link_font_color','styles') }};
		@endif
		@if(!empty(getSetting('link_font_weight','styles')))
			font-weight: {{ getSetting('link_font_weight','styles') }};
		@endif
		@if(!empty(getSetting('link_line_height','styles')))
			line-height: {{ getSetting('link_line_height','styles') }};
		@endif
		@if(!empty(getSetting('link_letter_spacing','styles')))
			letter-spacing: {{ getSetting('link_letter_spacing','styles') }};
		@endif
	}




	@if(!empty(getSetting('classid_font_name','styles')))
		@foreach(getSetting('classid_font_name','styles') as $key=>$font)
			@if($font!='')
				{{ $font }}{
					@if(!empty(getSetting('classid_font','styles')[$key]))
					font-family:"{{ getSetting('classid_font','styles')[$key] }}";
					@endif
					@if(!empty(getSetting('classid_font_size','styles')[$key]))
					font-size: {{ getSetting('classid_font_size','styles')[$key] }};
					@endif
					@if(!empty(getSetting('classid_font_color','styles')[$key]))
					color: {{ getSetting('classid_font_color','styles')[$key] }};
					@endif
					@if(!empty(getSetting('classid_font_weight','styles')[$key]))
					font-weight:{{ getSetting('classid_font_weight','styles')[$key] }};
					@endif
					@if(!empty(getSetting('classid_line_height','styles')[$key]))
					line-height:{{ getSetting('classid_line_height','styles')[$key] }};
					@endif
					@if(!empty(getSetting('classid_letter_spacing','styles')[$key]))
					letter-spacing:{{ getSetting('classid_letter_spacing','styles')[$key] }};
					@endif
				}
			@endif
		@endforeach
	@endif

	.menu-toggler{
		{{ !empty(getSetting('toggle_button_bg_color','header')) ? 'background-color:'.getSetting('toggle_button_bg_color','header').';' : '#333333' }}
		{{ !empty(getSetting('toggle_button_color','header')) ? 'color:'.getSetting('toggle_button_color','header').';' : '' }}
		position: relative;
	}
	.menu-toggler .icon-bar{
		background: {{ !empty(getSetting('toggle_button_color','header')) ? getSetting('toggle_button_color','header').';' : '#ffffff' }};
    display: block;
    height: 3px;
    margin: 4px 0;
    position: relative;
    transition: all .5s;
    width: 30px;
	}
	.menu-custom .menu-nav li a:not(.button){
		position:relative;
		{{ !empty(getSetting('header_menu_items_bg_color','header')) ? 'background-color:'.getSetting('header_menu_items_bg_color','header').';' : '' }}
		{{ !empty(getSetting('header_menu_items_color','header')) ? 'color:'.getSetting('header_menu_items_color','header').';' : '' }}
		{{ !empty(getSetting('header_menu_items_font_size','header')) ? 'font-size:'.getSetting('header_menu_items_font_size','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_font_family','header')) ? 'font-family:"'.getSetting('header_menu_items_font_family','header').'";' : '' }}

		{{ !empty(getSetting('header_menu_items_letter_spacing','header')) ? 'letter-spacing:'.getSetting('header_menu_items_letter_spacing','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_padding_left','header')) ? 'padding-left:'.getSetting('header_menu_items_padding_left','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_padding_right','header')) ? 'padding-right:'.getSetting('header_menu_items_padding_right','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_padding_top','header')) ? 'padding-top:'.getSetting('header_menu_items_padding_top','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_padding_bottom','header')) ? 'padding-bottom:'.getSetting('header_menu_items_padding_bottom','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_margin_left','header')) ? 'margin-left:'.getSetting('header_menu_items_margin_left','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_margin_right','header')) ? 'margin-right:'.getSetting('header_menu_items_margin_right','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_margin_top','header')) ? 'margin-top:'.getSetting('header_menu_items_margin_top','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_margin_bottom','header')) ? 'margin-bottom:'.getSetting('header_menu_items_margin_bottom','header').'px;' : '' }}

		{{ !empty(getSetting('header_menu_items_border_style','header')) ? 'border-style:'.getSetting('header_menu_items_border_style','header').';' : '' }}
		{{ !empty(getSetting('header_menu_items_border_width_left','header')) ? 'border-left-width:'.getSetting('header_menu_items_border_width_left','header').'px;' : 'border-left-width:0;' }}
		{{ !empty(getSetting('header_menu_items_border_width_right','header')) ? 'border-right-width:'.getSetting('header_menu_items_border_width_right','header').'px;' : 'border-right-width:0;' }}
		{{ !empty(getSetting('header_menu_items_border_width_top','header')) ? 'border-top-width:'.getSetting('header_menu_items_border_width_top','header').'px;' : 'border-top-width:0;' }}
		{{ !empty(getSetting('header_menu_items_border_width_bottom','header')) ? 'border-bottom-width:'.getSetting('header_menu_items_border_width_bottom','header').'px;' : 'border-bottom-width:0;' }}
		{{ !empty(getSetting('header_menu_items_border_radius_top_left','header')) ? 'border-top-left-radius:'.getSetting('header_menu_items_border_radius_top_left','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_border_radius_top_right','header')) ? 'border-top-right-radius:'.getSetting('header_menu_items_border_radius_top_right','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_border_radius_bottom_left','header')) ? 'border-bottom-left-radius:'.getSetting('header_menu_items_border_radius_bottom_left','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_border_radius_bottom_right','header')) ? 'border-bottom-right-radius:'.getSetting('header_menu_items_border_radius_bottom_right','header').'px;' : '' }}
		{{ !empty(getSetting('header_menu_items_text_transform','header')) ? 'text-transform:'.getSetting('header_menu_items_text_transform','header').';' : '' }}
	}
	.menu-custom .menu-nav li:hover > a:not(.button),.menu-custom .menu-nav li.active > a:not(.button){
		{{ !empty(getSetting('header_menu_items_bg_color_hover','header')) ? 'background-color:'.getSetting('header_menu_items_bg_color_hover','header').';' : '' }}
		{{ !empty(getSetting('header_menu_items_color_hover','header')) ? 'color:'.getSetting('header_menu_items_color_hover','header').';' : '' }}
	}
	.menu-custom .menu-nav>li>a:focus:not(.button), .menu-custom .menu-nav>li>a:hover:not(.button), .menu-custom .menu-nav li.active a:not(.button){
		{{ !empty(getSetting('header_menu_items_bg_color_hover','header')) ? 'background-color:'.getSetting('header_menu_items_bg_color_hover','header').';' : '' }}
		{{ !empty(getSetting('header_menu_items_color_hover','header')) ? 'color:'.getSetting('header_menu_items_color_hover','header').';' : '' }}
	}
	.menu-custom .menu-nav li ul.sub-menu li a:not(.button){
		{{ !empty(getSetting('header_submenu_items_bg_color','header')) ? 'background-color:'.getSetting('header_submenu_items_bg_color','header').';' : '' }}
		{{ !empty(getSetting('header_submenu_items_color','header')) ? 'color:'.getSetting('header_submenu_items_color','header').';' : '' }}
		{{ !empty(getSetting('header_submenu_items_font_size','header')) ? 'font-size:'.getSetting('header_submenu_items_font_size','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_font_family','header')) ? 'font-family:"'.getSetting('header_submenu_items_font_family','header').'";' : '' }}
		{{ !empty(getSetting('header_submenu_items_letter_spacing','header')) ? 'letter-spacing:'.getSetting('header_submenu_items_letter_spacing','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_padding_left','header')) ? 'padding-left:'.getSetting('header_submenu_items_padding_left','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_padding_right','header')) ? 'padding-right:'.getSetting('header_submenu_items_padding_right','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_padding_top','header')) ? 'padding-top:'.getSetting('header_submenu_items_padding_top','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_padding_bottom','header')) ? 'padding-bottom:'.getSetting('header_submenu_items_padding_bottom','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_margin_left','header')) ? 'margin-left:'.getSetting('header_submenu_items_margin_left','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_margin_right','header')) ? 'margin-right:'.getSetting('header_submenu_items_margin_right','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_margin_top','header')) ? 'margin-top:'.getSetting('header_submenu_items_margin_top','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_margin_bottom','header')) ? 'margin-bottom:'.getSetting('header_submenu_items_margin_bottom','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_border_style','header')) ? 'border-style:'.getSetting('header_submenu_items_border_style','header').';' : '' }}
		{{ !empty(getSetting('header_submenu_items_border_width_left','header')) ? 'border-left-width:'.getSetting('header_submenu_items_border_width_left','header').'px;' : 'border-left-width:0;' }}
		{{ !empty(getSetting('header_submenu_items_border_width_right','header')) ? 'border-right-width:'.getSetting('header_submenu_items_border_width_right','header').'px;' : 'border-right-width:0;' }}
		{{ !empty(getSetting('header_submenu_items_border_width_top','header')) ? 'border-top-width:'.getSetting('header_submenu_items_border_width_top','header').'px;' : 'border-top-width:0;' }}
		{{ !empty(getSetting('header_submenu_items_border_width_bottom','header')) ? 'border-bottom-width:'.getSetting('header_submenu_items_border_width_bottom','header').'px;' : 'border-bottom-width:0;' }}
		{{ !empty(getSetting('header_submenu_items_border_radius_top_left','header')) ? 'border-top-left-radius:'.getSetting('header_submenu_items_border_radius_top_left','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_border_radius_top_right','header')) ? 'border-top-right-radius:'.getSetting('header_submenu_items_border_radius_top_right','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_border_radius_bottom_left','header')) ? 'border-bottom-left-radius:'.getSetting('header_submenu_items_border_radius_bottom_left','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_border_radius_bottom_right','header')) ? 'border-bottom-right-radius:'.getSetting('header_submenu_items_border_radius_bottom_right','header').'px;' : '' }}
		{{ !empty(getSetting('header_submenu_items_text_transform','header')) ? 'text-transform:'.getSetting('header_submenu_items_text_transform','header').';' : '' }}
	}
	.menu-custom .menu-nav li ul.sub-menu li:hover > a:not(.button),.menu-custom .menu-nav li ul.sub-menu li.subactive > a:not(.button){
		{{ !empty(getSetting('header_submenu_items_bg_color_hover','header')) ? 'background-color:'.getSetting('header_submenu_items_bg_color_hover','header').';' : '' }}
		{{ !empty(getSetting('header_submenu_items_color_hover','header')) ? 'color:'.getSetting('header_submenu_items_color_hover','header').';' : '' }}
	}
	.menu-custom .menu-nav li ul.sub-menu li a:focus:not(.button), .menu-custom .menu-nav li ul.sub-menu li a:active:not(.button){
		{{ !empty(getSetting('header_submenu_items_bg_color_hover','header')) ? 'background-color:'.getSetting('header_submenu_items_bg_color_hover','header').';' : '' }}
		{{ !empty(getSetting('header_submenu_items_color_hover','header')) ? 'color:'.getSetting('header_submenu_items_color_hover','header').';' : '' }}
	}

	.menu-custom{
		{{ !empty(getSetting('header_spacing_left','header')) ? 'padding-left:'.getSetting('header_spacing_left','header').'vw;' : '' }}
		{{ !empty(getSetting('header_spacing_right','header')) ? 'padding-right:'.getSetting('header_spacing_right','header').'vw;' : '' }}
		{{ !empty(getSetting('header_spacing_top','header')) ? 'padding-top:'.getSetting('header_spacing_top','header').'vw;' : '' }}
		{{ !empty(getSetting('header_spacing_bottom','header')) ? 'padding-bottom:'.getSetting('header_spacing_bottom','header').'vw;' : '' }}
		{{ !empty(getSetting('header_margin_left','header')) ? 'margin-left:'.getSetting('header_margin_left','header').'vw;' : '' }}
		{{ !empty(getSetting('header_margin_right','header')) ? 'margin-right:'.getSetting('header_margin_right','header').'vw;' : '' }}
		{{ !empty(getSetting('header_margin_top','header')) ? 'margin-top:'.getSetting('header_margin_top','header').'vw;' : '' }}
		{{ !empty(getSetting('header_margin_bottom','header')) ? 'margin-bottom:'.getSetting('header_margin_bottom','header').'vw;' : '' }}
	}

	.menu-custom .menu-mobile .menu-nav li a:not(.button){
		{{ !empty(getSetting('header_menu_items_bg_color','header')) ? 'background-color:'.getSetting('header_menu_items_bg_color','header').';' : '' }}
		{{ !empty(getSetting('header_menu_items_color','header')) ? 'color:'.getSetting('header_menu_items_color','header').';' : '' }}
		{{ !empty(getSetting('toggle_menu_item_font_size','header')) ? 'font-size:'.getSetting('toggle_menu_item_font_size','header').'px;' : '' }}
	}

	.menu-custom .menu-mobile .menu-nav li a:not(.button):hover{
		{{ !empty(getSetting('header_menu_items_bg_color_hover','header')) ? 'background-color:'.getSetting('header_menu_items_bg_color_hover','header').';' : '' }}
		{{ !empty(getSetting('header_menu_items_color_hover','header')) ? 'color:'.getSetting('header_menu_items_color_hover','header').';' : '' }}
	}

	@for($i=20;$i<=500;$i=$i+10)
	.spacer.height-{{ $i }}{
		min-height: {{ $i }}px;
	}
	@endfor
	@for($i=0;$i<=50;$i++)
	.gutter-l-{{ $i }}{
		margin-left: {{ $i }}px;
		margin-right: {{ $i }}px;
	}
	.gutter-l-{{ $i }} [class*='col']{
		padding-left: {{ $i }}px;
		padding-right: {{ $i }}px;
	}
	@endfor
	@for($i=-100;$i<=100;$i++)
	[class*='col'].margin-t-{{ $i }}{margin-top: {{ $i }}vw;}
	[class*='col'].margin-b-{{ $i }}{margin-bottom: {{ $i }}vw;}
	[class*='col'].margin-l-{{ $i }}{margin-left: {{ $i }}vw;}
	[class*='col'].margin-r-{{ $i }}{margin-right: {{ $i }}vw;}
	.row.margin-t-{{ $i }}{margin-top: {{ $i }}vw;}
	.row.margin-b-{{ $i }}{margin-bottom: {{ $i }}vw;}
	.row.margin-l-{{ $i }}{margin-left: {{ $i }}vw;}
	.row.margin-r-{{ $i }}{margin-right: {{ $i }}vw;}
	@endfor
	@for($i=0;$i<=100;$i++)
	[class*='col'].padding-t-{{ $i }}{padding-top: {{ $i }}vw;}
	[class*='col'].padding-b-{{ $i }}{padding-bottom: {{ $i }}vw;}
	[class*='col'].padding-l-{{ $i }}{padding-left: {{ $i }}vw;}
	[class*='col'].padding-r-{{ $i }}{padding-right: {{ $i }}vw;}
	.row.padding-t-{{ $i }}{padding-top: {{ $i }}vw;}
	.row.padding-b-{{ $i }}{padding-bottom: {{ $i }}vw;}
	.row.padding-l-{{ $i }}{padding-left: {{ $i }}vw;}
	.row.padding-r-{{ $i }}{padding-right: {{ $i }}vw;}
	@endfor
	@for($i=0;$i<=50;$i++)
	.border-t-{{ $i }}{border-top-width: {{ $i }}px;}
	.border-b-{{ $i }}{border-bottom-width: {{ $i }}px;}
	.border-l-{{ $i }}{border-left-width: {{ $i }}px;}
	.border-r-{{ $i }}{border-right-width: {{ $i }}px;}
	@endfor
	@for($i=0;$i<=100;$i++)
	.border-radius-tlpx-{{ $i }}{border-top-left-radius: {{ $i }}px;}
	.border-radius-trpx-{{ $i }}{border-top-right-radius: {{ $i }}px;}
	.border-radius-blpx-{{ $i }}{border-bottom-left-radius: {{ $i }}px;}
	.border-radius-brpx-{{ $i }}{border-bottom-right-radius: {{ $i }}px;}
	@endfor
	@for($i=0;$i<=100;$i++)
	.border-radius-tlper-{{ $i }}{border-top-left-radius: {{ $i }}%;}
	.border-radius-trper-{{ $i }}{border-top-right-radius: {{ $i }}%;}
	.border-radius-blper-{{ $i }}{border-bottom-left-radius: {{ $i }}%;}
	.border-radius-brper-{{ $i }}{border-bottom-right-radius: {{ $i }}%;}
	@endfor
	.border-style-solid{
	border-style: solid;
	}
	.border-style-dashed{
	border-style: dashed;
	}
	.border-style-dotted{
	border-style: dotted;
	}
	.border-style-double{
	border-style: double;
	}
	.border-style-groove{
	border-style: groove;
	}
	.menu-nav{
		display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
		-webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-direction: row;
    flex-direction: row;
	}
.menu-nav>li>ul.sub-menu {
    visibility: hidden;
    opacity: 0;
    list-style: none;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    z-index: 10;
		position: absolute;
    padding: 0;
    margin: 0;
    width: 180px;
}
.menu-nav>li.menu-item:hover > ul.sub-menu {
    visibility: visible;
    opacity: 1;
}
.sliding-menu .menu-nav{
	display: block;
}
	@media (max-width: 767px){
		.menu-collapse{
			{{ !empty(getSetting('toggle_menu_bg_color','header')) ? 'background-color:'.getSetting('toggle_menu_bg_color','header').';' : '' }}
			-ms-flex-preferred-size: 100%;
	    flex-basis: 100%;
	    -webkit-box-flex: 1;
	    -ms-flex-positive: 1;
	    flex-grow: 1;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    align-items: center;
		}
		.menu-nav{
			 display: block;
		}
		.menu-nav>li>ul.sub-menu {
		    visibility: visible;
		    opacity: 1;
				position: relative;
				margin-left: 0;
				width: 100%;
		}
		.menu-custom{
			padding-left:0;
			padding-right:0;
			margin-left:0;
			margin-right:0;
		}
		img {
    	height: auto !important;
		}
		.button{
			margin-bottom: 10px;
		}
	}

	{!! !empty(getSetting('custom_code_header_css','custom')) ? getSetting('custom_code_header_css','custom') : '' !!}

	{!! getButtoncss() !!}
