<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <!-- Styles -->
    <link href="{{ asset('assets/private/css/styles.css') }}" rel="stylesheet">
	<!-- Scripts -->
	<script src="{{ asset('assets/private/js/modernizr.min.js') }}"></script>
</head>
<body class="fixed-left">
    <div class="account-pages"></div>
	<div class="clearfix"></div>
	<div class="wrapper-page">
		@yield('content')

	</div>

	<script>
            var resizefunc = [];
    </script>

</body>
</html>
