<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700&display=swap" rel="stylesheet">
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <!-- Styles -->
    <link href="{{ asset('assets/private/css/styles.css') }}" rel="stylesheet">
	<!-- Scripts -->
	<script src="{{ asset('assets/private/js/modernizr.min.js') }}"></script>
	<script>
            var resizefunc = [];
    </script>
    @if(!empty(getSetting('imported_font_link','styles')))
    	@foreach(getSetting('imported_font_link','styles') as $links)
    		<link href="{!! $links !!}" rel="stylesheet">
    	@endforeach
    @endif
	<script src="{{ asset('assets/public/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/public/js/popper.min.js') }}"></script>
  <script src="{{ asset('assets/public/js/bootstrap.min.js') }}"></script>

<!--
  <script src="{{ asset('assets/private/js/detect.js') }}"></script>
	<script src="{{ asset('assets/private/js/fastclick.js') }}"></script>

	<script src="{{ asset('assets/private/js/jquery.slimscroll.js') }}"></script>
	<script src="{{ asset('assets/private/js/jquery.blockUI.js') }}"></script>
-->
	<script src="{{ asset('assets/private/js/waves.js') }}"></script>
  <!--
	<script src="{{ asset('assets/private/js/wow.min.js') }}"></script>
	<script src="{{ asset('assets/private/js/jquery.nicescroll.js') }}"></script>
	<script src="{{ asset('assets/private/js/jquery.scrollTo.min.js') }}"></script>

	<script src="{{ asset('assets/private/plugins/peity/jquery.peity.min.js') }}"></script>

	<script src="{{ asset('assets/private/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
	<script src="{{ asset('assets/private/plugins/counterup/jquery.counterup.min.js') }}"></script>
-->
  <script type="text/javascript" src="{{ asset('assets/private/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
  <script src="{{ asset('assets/private/js/bootstrap-select.min.js') }}"></script>
<!--
	<script src="{{ asset('assets/private/plugins/raphael/raphael-min.js') }}"></script>

	<script src="{{ asset('assets/private/plugins/jquery-knob/jquery.knob.js') }}"></script>
-->
  <link href="{{ asset('assets/public/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/public/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />

  <link href="{{ asset('assets/private/css/dropzone.css') }}" rel="stylesheet" type="text/css" />
  <script src="{{ asset('assets/private/js/dropzone.js') }}" type="text/javascript"></script>

  <link href="{{ asset('assets/private/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/private/plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
  <script src="{{ asset('assets/private/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
  <script src="{{ asset('assets/private/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
<!--

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.counter').counterUp({
				delay: 100,
				time: 1200
			});

			$(".knob").knob();

		});
	</script>
-->
	<script>
	$(document).ready(function() {
		$(document).on("click", ".showhide", function(e) {
		   var checked = $(this).find("input:checkbox").is(":checked");
		   var showelement = $(this).data('trigger-show');
		   var hideelement = $(this).data('trigger-hide');
		   if (checked) {
			   $(this).find("input:checkbox").attr('checked','checked');
			   $('.'+showelement).show();
			   $('.'+hideelement).hide();
		   } else {
			   $(this).find("input:checkbox").removeAttr('checked');
			   $('.'+hideelement).show();
			   $('.'+showelement).hide();
		   }
		});
		var url = document.location.toString();
		if (url.match('#')) {
			$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
			$('#'+url.split('#')[1]).find('form').find('input[name="activetab"]').val('#'+url.split('#')[1]);
		}
		$('.nav-tabs a').on('shown.bs.tab', function (e) {
			$(e.target.hash).find('form').find('input[name="activetab"]').val(e.target.hash);
		})
		$('.md-custom-overlay').on('click', function(){
			$($(this).data('target')).removeClass('md-custom-show');
		})
	});
	</script>
  <script>
    $( function() {
      $( "#sortable" ).sortable({ handle: '.handle' });
      $( "#sortable" ).disableSelection();
    } );
  </script>
	<style>
	.navbar-nav.topnav>li{
		float:none;
		display:inline-block;
		vertical-align: middle;
	}
	.publish-message{
		float: left;
		margin-left: 10px;
		margin-top: 0px;
	}
	.publish-message .alert{
		margin-bottom:0;
		margin-bottom: 0;
		padding: 8px 18px;
	}
.panel-default > .panel-heading, .panel-footer{
    background-color: #36404a;
    color: #ffffff;
}
	</style>
  <style>
  .choosemedia .image{
  	    border: 3px dashed;
      min-height: 150px;
      position: relative;
  	background-size:cover;
  }
  .choosemedia .addedimage{
  		margin-top:5px;
  		margin-bottom:5px;
  		overflow:hidden;
  		padding-left: 0;
  		padding-top: 0;
  }
  .choosemedia .addedimage::before{
  	display: none;
  }

  .choosemedia .form-control{
  	margin-top:5px;
  }
  .choosemedia a{
  	    position: absolute;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      text-align: center;
      z-index: 99;
  }
  .choosemedia a .fa{
  	    position: absolute;
      font-size: 30px;
      color: #000000;
      top: 50%;
      transform: translateY(-50%) translateX(-50%);
  	-webkit-transform: translateY(-50%) translateX(-50%);
      left: 50%;
  }
  .choosemedia .image a.delete{
  	opacity:0;
  	 -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
  }
  .choosemedia .image:hover a{
  	opacity:1;
  }
  .choosemedia .image{
  	background-size:contain;
  	background-repeat:no-repeat;
  	background-position:center;
  	width: 100%;
  	top: 0;
  }
  .choosemedia .image.image-has a{
  	opacity:0;
  }
  .choosemedia .image .removeimage{
  	display:none;
  }
  .choosemedia .image.image-has .removeimage{
  	display:block;
  	opacity: 1;
      bottom: 0;
      position: absolute;
      top: auto;
      color: #ffffff;
      padding: 5px;
      background: rgba(0,0,0,.7);
  	margin-bottom: -35px;
  	 -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    z-index:100;
  }
  .choosemedia .image.image-has:hover .removeimage{
  	margin-bottom: 0px;
  }
  .choosemedia .image.image-has:hover a:not(.removeimage){
  	opacity: .8;
      background-color: #ffffff;
  }
  #preview{
  	background:url({{ asset('assets/images/preview.png') }}) no-repeat center;
  	min-height:100px;
  	border: 2px dashed #eaeaea;
  	margin-bottom:10px;
  }
  .mce-tinymce{
  	margin-top: 15px !important;
  }
.imagelist .control{
  position: relative;
    padding: 5px 15px;
    background: #36404A;
    cursor: pointer;
}
.imagelist .control .handle{
  cursor: move;
}
.imagelist .control i{
  font-size: 30px;
    color: #ffffff;
    position: relative;
    transform: none;
    top: 0;
    left: 0;
}
.imagelist .control a{
  position: relative;
}
.imagelist .control a.deletethumb{
  float: right;
}
.imagelist .imageholder{
  width: 20%;
  float: left;
}
.imagelist{
  margin-bottom: 20px;
}
.imagelist .form{
  width: 75%;
  float: right;
}
.ui-sortable-placeholder{
  background: #eaeaea;
  min-height: 50px;
  visibility: visible !important;
}
  </style>
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/private/css/modal.css') }}" />

	@yield('extra')
</head>
<body class="fixed-left-void">
    <div id="wrapper" class="forced enlarged">

	<div class="topbar">

                <div class="navbar-expand navbar-default" role="navigation">
                    <div class="container-fluid">
                        <div class="row">
                          <div class="topbar-left col-2">
                              <div class="text-center">
                                  <a href="{{ config('app.url', 'Laravel') }}" class="logo">{{ config('app.name', 'Laravel') }}</a>
                              </div>
                          </div>
                          <div class="col-10">
                            <div class="row align-items-center">
                                    <div class="pull-left">
                                        <button class="button-menu-mobile open-left waves-effect waves-light">
                                            <i class="md md-menu"></i>
                                        </button>
                                        <span class="clearfix"></span>
                                    </div>


                    							<ul class="nav navbar-nav topnav hidden-xs">
                    								<li><a href="{{ route('publish') }}"><i class="ti-cloud-up" style="font-size: 25px;margin-right: 5px;"></i> Publish Site</a></li>
                    								<li><a href="{{ route('download') }}"><i class="ti-cloud-down" style="font-size: 25px;margin-right: 5px;"></i> Download HTML</a></li>
                    							</ul>


                    							@if (\Session::has('publishsuccess'))
                    									<div class="publish-message">
                    										<div class="alert alert-success">
                    											<i class="glyphicon glyphicon-ok"></i> {!! \Session::get('publishsuccess') !!}
                    										</div>
                    									</div>
                    							@elseif(\Session::has('publisherror'))
                    									<div class="publish-message">
                    										<div class="alert alert-danger">
                    											{!! \Session::get('publisherror') !!}
                    										</div>
                    									</div>
                    							@endif


                                    <ul class="nav navbar-nav navbar-right  align-items-center ml-auto">
        							@guest
        								<li><a href="{{ route('login') }}">Login</a></li>
        								<li><a href="{{ route('register') }}">Register</a></li>
        							@else
                                        <li class="hidden-xs">
                                            <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                        </li>
                                        <li class="dropdown top-menu-item-xs">
                                            <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">{!! !empty(Auth::user()->avatar) ? '<img class="img-circle" src="'.Auth::user()->avatar.'">' : '<img class="img-circle" src="'.asset('assets/private/images/users/avatar.png').'">' !!} </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{ route('profile') }}"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                                <li class="divider"></li>
                                                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a>
        										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
        										</li>
                                            </ul>
                                        </li>

        							@endguest
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>









		<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
							<li class="has_sub">
                                <a href="javascript:void(0);"><i class="ti-camera"></i><span> Options </span> <span class="menu-arrow"></span></a>
								<ul class="list-unstyled">
                                    <li><a href="{{ route('settings.header') }}">Header</a></li>
									<li><a href="{{ route('menu.index') }}">Menu</a></li>
                                    <li><a href="{{ route('settings.styles') }}">Styles</a></li>
                                    <li><a href="{{ route('settings.footerbuilder') }}">Footer</a></li>
									<li><a href="{{ route('settings.custom') }}">Custom</a></li>
                                </ul>
                            </li>
							<li class="has_sub">
                                <a href="{{URL::route('media.index')}}"><i class="ti-image"></i><span> Media </span></a>
                            </li>
                            <li class="has_sub">
                                <a href="{{URL::route('pages.index')}}"><i class="ti-files"></i><span> Pages </span></a>
                            </li>
							<li class="has_sub">
                                <a href="javascript:void(0);"><i class="ti-files"></i><span> Posts </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
								<li><a href="{{URL::route('posts.index')}}">All Posts</a></li>
                                <li><a href="{{URL::route('categories.index')}}">Categories</a></li>
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="{{URL::route('sections.index')}}"><i class="ti-files"></i><span> Sections </span></a>
                            </li>
							<li class="has_sub">
                                <a href="javascript:void(0);"><i class="ti-wand"></i><span> Widgets </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
									<li><a href="{{URL::route('widgets.index')}}">All Widgets</a></li>
									<li><a href="{{URL::route('buttons.index')}}">Custom Buttons</a></li>
									<li><a href="{{URL::route('popups.index')}}">Pop Ups</a></li>
                                </ul>
                            </li>
							<li class="has_sub">
                                <a href="javascript:void(0);"><i class="ti-settings"></i><span> Settings </span> <span class="menu-arrow"></span></a>
								<ul class="list-unstyled">
									<li><a href="{{URL::route('themes.index')}}">Themes</a></li>
									<li><a href="{{ route('loaddemo') }}">Templates</a></li>
                                    <li><a href="{{ route('settings.general') }}">General</a></li>
									<li><a href="{{URL::route('settings.host')}}">Miscellaneous</a></li>
                                </ul>
                            </li>
                        </ul>
				</div>
			</div>
		</div>





		<div class="content-page">
			<div class="content">
				@yield('content')
			</div>
		</div>
    </div>


	<script>
	$(document).ready(function(){
		setTimeout(function(){ $('.publish-message').fadeOut(); }, 5000);

	})
	</script>
	@yield('footerextra')
	<script src="{{ asset('assets/private/js/jquery.core.js') }}"></script>
	<script src="{{ asset('assets/private/js/jquery.app.js') }}"></script>
  

  <script src="{{ asset('assets/private/js/classie.js') }}"></script>
  <script src="{{ asset('assets/private/js/modalEffects.js') }}"></script>
  <script src="{{ asset('assets/private/js/jquery-ui.js') }}"></script>
  <script src="{{ asset('assets/private/js/clipboard.min.js') }}"></script>

  <script>
    var polyfilter_scriptpath = '{{ asset("assets/private/js/") }}/';
    $(document).ready(function(){
      $('#wrapper').on('click', 'a[data-toggle="custommodal"]', function(){
    		var custommodal = $(this).attr('data-target');
    		$(custommodal).addClass('md-custom-show');
        return false;
    	})
      var clipboard = new ClipboardJS('.copyshortcode');
      clipboard.on('success', function(e) {
          $('.copyshortcode').popover();
          setTimeout(function(){
            $('.copyshortcode').popover('hide');
          }, 5000);
          e.clearSelection();
      });
    })
    $( function() {
    $(document).tooltip({
      position: {
        my: "center bottom-5",
        at: "center top"
      }
    });
  });
  </script>


</body>
</html>
