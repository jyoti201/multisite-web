@extends('layouts.app')

@section('extra')




@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<div class="btn-group pull-right">
					<form method="post" action="{{ URL::route("buttons.save") }}">
					{{csrf_field()}}
					<button type="submit" class="btn btn-default waves-effect waves-light btn-sm pull-right">Add New Button</button>
					<input type="hidden" name="builder_background-color" value="#cccccc">
					<input type="hidden" name="builder_background-color-from" value="#cccccc">
					<input type="hidden" name="builder_background-color-to" value="#cccccc">
					<input type="hidden" name="builder_color" value="#000000">
					<input type="hidden" name="builder_font-family" value="">
					<input type="hidden" name="builder_font-size" value="16">
					<input type="hidden" name="builder_gradientbg" value="false">
					<input type="hidden" name="builder_line-height" value="16">
					<input type="hidden" name="builder_letter-spacing" value="1">
					<input type="hidden" name="builder_word-spacing" value="0">
					<input type="hidden" name="builder_text-transform" value="">
					<input type="hidden" name="builder_font-weight" value="">
					<input type="hidden" name="builder_border-style" value="none">
					<input type="hidden" name="builder_border-color" value="#000000">
					<input type="hidden" name="builder_border-top-left-radius" value="0">
					<input type="hidden" name="builder_border-top-right-radius" value="0">
					<input type="hidden" name="builder_border-bottom-left-radius" value="0">
					<input type="hidden" name="builder_border-bottom-right-radius" value="0">
					<input type="hidden" name="builder_border-left-width" value="0">
					<input type="hidden" name="builder_border-right-width" value="0">
					<input type="hidden" name="builder_border-top-width" value="0">
					<input type="hidden" name="builder_border-bottom-width" value="0">
					<input type="hidden" name="builder_padding-left" value="20">
					<input type="hidden" name="builder_padding-right" value="20">
					<input type="hidden" name="builder_padding-top" value="10">
					<input type="hidden" name="builder_padding-bottom" value="10">
					<input type="hidden" name="builder_hover_gradientbg" value="false">
					<input type="hidden" name="builder_hover_background-color" value="#cccccc">
					<input type="hidden" name="builder_hover_background-color-from" value="#cccccc">
					<input type="hidden" name="builder_hover_background-color-to" value="#cccccc">
					<input type="hidden" name="builder_hover_color" value="#000000">
					<input type="hidden" name="builder_hover_font-family" value="">
					<input type="hidden" name="builder_hover_font-size" value="16">
					<input type="hidden" name="builder_hover_line-height" value="16">
					<input type="hidden" name="builder_hover_letter-spacing" value="1">
					<input type="hidden" name="builder_hover_word-spacing" value="0">
					<input type="hidden" name="builder_hover_text-transform" value="">
					<input type="hidden" name="builder_hover_font-weight" value="">
					<input type="hidden" name="builder_hover_border-style" value="none">
					<input type="hidden" name="builder_hover_border-color" value="#000000">
					<input type="hidden" name="builder_hover_border-top-left-radius" value="0">
					<input type="hidden" name="builder_hover_border-top-right-radius" value="0">
					<input type="hidden" name="builder_hover_border-bottom-left-radius" value="0">
					<input type="hidden" name="builder_hover_border-bottom-right-radius" value="0">
					<input type="hidden" name="builder_hover_border-left-width" value="0">
					<input type="hidden" name="builder_hover_border-right-width" value="0">
					<input type="hidden" name="builder_hover_border-top-width" value="0">
					<input type="hidden" name="builder_hover_border-bottom-width" value="0">
					<input type="hidden" name="builder_hover_padding-left" value="20">
					<input type="hidden" name="builder_hover_padding-right" value="20">
					<input type="hidden" name="builder_hover_padding-top" value="10">
					<input type="hidden" name="builder_hover_padding-bottom" value="10">
					<input type="hidden" name="button_class" value="button_{{ rand(100000,100000000000) }}">
					</form>
				</div>
				<h4 class="page-title">Buttons <span>Custom Buttons</span></h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
						<div class="alert alert-success">
							{!! \Session::get('success') !!}
						</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">

				<div class="card-box">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Title</th>
								<th>Shortcode</th>
								<th>Created on</th>
							</tr>
						</thead>
						<tbody>
							@foreach (json_decode($savedbuttons) as $button)
							<tr>
								<td>
								{{ $button->name }}
								<ul class="buttons">
									<li>
										<a href="{{ route('buttons.edit', $button->id) }}"><i class="fas fa-pencil-alt"></i></a>
									</li>
									<li><a href="{{ route('buttons.delete', $button->id) }}"> <i class="fa fa-remove"></i> </a></li>
								</ul>
								</td>
								<td>
										{{ $button->shortcode }}
								</td>
								<td width="180px">Published<br />{{ $button->created_at }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>

			</div>
    </div>
</div>

@endsection
