@extends('layouts.app')

@section('extra')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/codemirror.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/theme/ambiance.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/dialog/dialog.min.css" />
<style>
.grid figure{
	float:none;
}
.imageholder.selected .thumbnail{
	border: 2px dashed #cccccc;
}
.btn.insert{
	display:none;
}
.btn.insert.active{
	display:inline-block;
}

.panel-color .panel-title span{
	display: block;
    line-height: 1;
    font-size: 14px;
    text-transform: none;
    color: #5fbeaa;
}
#headerbuilder{
	margin: 60px 0;
}
</style>
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<button class="btn btn-default waves-effect waves-light btn-md pull-right" id="save">Update</button>
				<h4 class="page-title">Options <span>Header Settings</span></h4>

			</div>
    </div>
	@if (\Session::has('success'))
		<div class="row">
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
		</div>
			@endif
<form method="post" id="header-form" action="{{ route("settings.saveheader") }}" enctype="multipart/form-data">
	{{csrf_field()}}
	<input type="hidden" name="settingType" value="header">
	<div class="row">
		<div class="col-sm-12 m-t-15">
			<ul class="nav nav-tabs tabs">
				<li class="tab">
					<a href="#tab1" class="active" data-toggle="tab" aria-expanded="false">
						<span class="visible-xs"><i class="fa fa-home"></i></span>
						<span class="hidden-xs">Logos & Menu</span>
					</a>
				</li>
				<li class="tab">
					<a href="#tab2" data-toggle="tab" aria-expanded="false">
						<span class="visible-xs"><i class="fa fa-user"></i></span>
						<span class="hidden-xs">Header Settings</span>
					</a>
				</li>
				@if(!empty(getSetting('header','header')) && getSetting('header','header')=='header3' || !empty(getSetting('header','header')) && getSetting('header','header')=='header4')
				<li class="tab">
					<a href="#tab5" data-toggle="tab" aria-expanded="false">
						<span class="visible-xs"><i class="fa fa-cog"></i></span>
						<span class="hidden-xs">Slide Menu</span>
					</a>
				</li>
				@endif
				<li class="tab">
					<a href="#tab3" data-toggle="tab" aria-expanded="true">
						<span class="visible-xs"><i class="fas fa-bars"></i></span>
						<span class="hidden-xs">Menu Items</span>
					</a>
				</li>

				<li class="tab">
					<a href="#tab4" data-toggle="tab" aria-expanded="false">
						<span class="visible-xs"><i class="fa fa-cog"></i></span>
						<span class="hidden-xs">Addional Settings</span>
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">
					<div class="row">
						<div class="col-lg-5">
							<div class="row">
								<div class="col-sm-6 choosemedia">
									<div class="addedimage">
										@if(array_key_exists('logo', $savedsettings))
											@if($savedsettings['logo']!='')
											<div class="image image-has" style="background-image:url('{{ $savedsettings['logo'] }}')">
												<a href="#" class="removeimage">Remove</a>
												<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Logo</h5></i> </a>
											</div>
											<input type="hidden" name="logo" value="{{ $savedsettings['logo'] }}">
											@else
											<div class="image">
												<a href="#" class="removeimage">Remove</a>
												<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Logo</h5></i> </a>
											</div>
											<input type="hidden" name="logo">
											@endif
										@else
											<div class="image">
												<a href="#" class="removeimage">Remove</a>
												<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Logo</h5></i> </a>
											</div>
											<input type="hidden" name="logo">
										@endif
									</div>

								</div>

								<div class="col-sm-6 choosemedia">
									<div class="addedimage">
										@if(array_key_exists('favicon', $savedsettings))
											@if($savedsettings['favicon']!='')
											<div class="image image-has" style="background-image:url('{{ $savedsettings['favicon'] }}')">
												<a href="#" class="removeimage">Remove</a>
												<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Favicon</h5></i> </a>
											</div>
											<input type="hidden" name="favicon" value="{{ $savedsettings['favicon'] }}">
											@else
											<div class="image">
												<a href="#" class="removeimage">Remove</a>
												<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Favicon</h5></i> </a>
											</div>
											<input type="hidden" name="favicon">
											@endif
										@else
											<div class="image">
												<a href="#" class="removeimage">Remove</a>
												<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Favicon</h5></i> </a>
											</div>
											<input type="hidden" name="favicon">
										@endif
									</div>
								</div>

								@if(sizeof($settings['logos']))
									@foreach($settings['logos'] as $key=>$logo)
										<div class="col-sm-6 choosemedia">
											<div class="addedimage">
												@if(array_key_exists($key, $savedsettings) && $savedsettings[$key]!='')
													<div class="image image-has" style="background-image:url('{{ $savedsettings[$key] }}')">
														<a href="#" class="removeimage">Remove</a>
														<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>{{ $logo }}</h5></i> </a>
													</div>
													<input type="hidden" name="{{ $key }}" value="{{ $savedsettings[$key] }}">
												@else
													<div class="image">
														<a href="#" class="removeimage">Remove</a>
														<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>{{ $logo }}</h5></i> </a>
													</div>
													<input type="hidden" name="{{ $key }}" value="">
												@endif
											</div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
						<div class="col-lg-7">
							<div class="form-group m-b-0">
								@foreach($settings['menu_locations'] as $key=>$location)
									<div class="row">
											<div class="form-group col-8">
												<h4 class="text-muted m-b-15 font-15">{{ $location['title'] }}</h4>
												<select class="selectpicker" data-live-search="true"  data-style="btn-white" name="{{ $key }}">
													<option value="">Select Menu</option>
													@foreach($menus as $menu)
													<option value="{{ $menu->id }}" @if(!empty($savedsettings[$key]) && $savedsettings[$key] == $menu->id) {{ "selected" }} @endif>{{ $menu->name }}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group col-4">
												@if(!empty($location['align']) && $location['align']!='false')
												<h4 class="text-muted m-b-15 font-15">{{ $location['title'] }} Position</h4>
												<select class="selectpicker"  data-style="btn-white" name="{{ $key }}_align">
													<option value="">Select</option>
													@foreach(explode(':',$location['align']) as $val)
													<option value="{{ $val }}" {{ !empty($savedsettings[$key.'_align']) && $savedsettings[$key.'_align'] == $val?'selected':'' }}>{{ $val }}</option>
													@endforeach
												</select>
												@endif
											</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="tab2">
					<div class="form-group row">
						<div class="col-md-12">
							<div class="checkbox">
								<input id="hide_header" name="hide_header" type="checkbox"
								@if(!empty($savedsettings['hide_header']))
									checked
								@endif
								>
								<label for="hide_header">
								Hide Header
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
										@if(sizeof($settings['headers'])>0)
										<div class="form-group">
											<p class="text-muted m-b-15 font-15">Header Design</p>
											<select class="selectpicker" data-live-search="true" name="header"  data-style="btn-white">
												@foreach($settings['headers'] as $key=>$header)
												<option value="{{ $key }}"
												@if(!empty($savedsettings['header']) && $key==$savedsettings['header'])
													selected
												@endif
												>{{ $header }}</option>
												@endforeach
											</select>
										</div>
											@endif



											<div class="form-group">
												<p class="text-muted m-b-15 font-15">Header Size</p>
												<div class="row">
													<div class="col-md-12">
														<div class="radio switch">
															<input id="header_size_default" name="header_size" value="" type="radio"
															@if(empty($savedsettings['header_size']) || $savedsettings['header_size']=='')
																checked
															@endif
															>
															<label for="header_size_default">
															Default
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_380" name="header_size" value="380" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='380')
																checked
															@endif
															>
															<label for="header_size_380">
															380PX
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_500" name="header_size" value="500" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='500')
																checked
															@endif
															>
															<label for="header_size_500">
															500PX
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_640" name="header_size" value="640" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='640')
																checked
															@endif
															>
															<label for="header_size_640">
															640PX
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_800" name="header_size" value="800" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='800')
																checked
															@endif
															>
															<label for="header_size_800">
															640PX
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_980" name="header_size" value="980" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='980')
																checked
															@endif
															>
															<label for="header_size_980">
															980PX
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_1050" name="header_size" value="1050" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='1050')
																checked
															@endif
															>
															<label for="header_size_1050">
															1050PX
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_1100" name="header_size" value="1100" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='1100')
																checked
															@endif
															>
															<label for="header_size_1100">
															1100PX
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_1200" name="header_size" value="1200" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='1200')
																checked
															@endif
															>
															<label for="header_size_1200">
															1200PX
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_1300" name="header_size" value="1300" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='1300')
																checked
															@endif
															>
															<label for="header_size_1300">
															1300PX
															</label>
														</div>

														<div class="radio switch">
															<input id="header_size_100" name="header_size" value="100" type="radio"
															@if(!empty($savedsettings['header_size']) && $savedsettings['header_size']=='100')
																checked
															@endif
															>
															<label for="header_size_100">
															Full Width
															</label>
														</div>
													</div>
												</div>
											</div>


									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<p class="text-muted m-b-15 font-15">Header Margin (in vw)</p>
											<div class="row">
												<div class="col-md-3"><input type="number" min="0" step="0.1" name="header_margin_left" value="{{ !empty($savedsettings['header_margin_left'])?$savedsettings['header_margin_left']:'0' }}" class="form-control" rel="txtTooltip" title="Left" data-placement="top"></div>
												<div class="col-md-3"><input type="number" min="0" step="0.1" name="header_margin_right" value="{{ !empty($savedsettings['header_margin_right'])?$savedsettings['header_margin_right']:'0' }}" class="form-control" rel="txtTooltip" title="Right" data-placement="top"></div>
												<div class="col-md-3"><input type="number" min="0" step="0.1" name="header_margin_top" value="{{ !empty($savedsettings['header_margin_top'])?$savedsettings['header_margin_top']:'0' }}" class="form-control" rel="txtTooltip" title="Top" data-placement="top"></div>
												<div class="col-md-3"><input type="number" min="0" step="0.1" name="header_margin_bottom" value="{{ !empty($savedsettings['header_margin_bottom'])?$savedsettings['header_margin_bottom']:'0' }}" class="form-control" rel="txtTooltip" title="Bottom" data-placement="top"></div>
											</div>
										</div>
										<div class="form-group">
											<p class="text-muted m-b-15 font-15">Header Padding (in vw)</p>
											<div class="row">
												<div class="col-md-3"><input type="number" min="0" step="0.1" name="header_spacing_left" value="{{ !empty($savedsettings['header_spacing_left'])?$savedsettings['header_spacing_left']:'0' }}" class="form-control" rel="txtTooltip" title="Left" data-placement="top"></div>
												<div class="col-md-3"><input type="number" min="0" step="0.1" name="header_spacing_right" value="{{ !empty($savedsettings['header_spacing_right'])?$savedsettings['header_spacing_right']:'0' }}" class="form-control" rel="txtTooltip" title="Right" data-placement="top"></div>
												<div class="col-md-3"><input type="number" min="0" step="0.1" name="header_spacing_top" value="{{ !empty($savedsettings['header_spacing_top'])?$savedsettings['header_spacing_top']:'1' }}" class="form-control" rel="txtTooltip" title="Top" data-placement="top"></div>
												<div class="col-md-3"><input type="number" min="0" step="0.1" name="header_spacing_bottom" value="{{ !empty($savedsettings['header_spacing_bottom'])?$savedsettings['header_spacing_bottom']:'1' }}" class="form-control" rel="txtTooltip" title="Bottom" data-placement="top"></div>
											</div>
										</div>

										<div class="form-group">
											<p class="text-muted m-b-15 font-15">Header Background Color</p>
											<div data-color-format="rgba" data-color="{{ !empty($savedsettings['header_bg_color'])?$savedsettings['header_bg_color']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
												<input type="text" name="header_bg_color" value="{{ !empty($savedsettings['header_bg_color'])?$savedsettings['header_bg_color']:'rgba(255,255,255,1)' }}" class="form-control">
												<span class="input-group-btn add-on">
													<button class="btn btn-white" type="button">
														<i style="background-color: {{ !empty($savedsettings['header_bg_color'])?$savedsettings['header_bg_color']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
													</button>
												</span>
											</div>
										</div>

									</div>
								</div>





				</div>
				<div class="tab-pane" id="tab5">
						@if(!empty(getSetting('header','header')) && getSetting('header','header')=='header3' || !empty(getSetting('header','header')) && getSetting('header','header')=='header4')
						<div class="row">
								<div class="form-group col-sm-3">
									<p class="text-muted m-b-15 font-15">Slide Menu Width</p>
									<input type="number" min="100" name="slide_menu_width" value="{{ !empty($savedsettings['slide_menu_width'])?$savedsettings['slide_menu_width']:'250' }}" class="form-control">
								</div>
								<div class="form-group col-sm-3">
									<p class="text-muted m-b-15 font-15">Slide Menu Background Color</p>
									<div data-color-format="rgba" data-color="{{ !empty($savedsettings['slide_menu_bg_color'])?$savedsettings['slide_menu_bg_color']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
										<input type="text" name="slide_menu_bg_color" value="{{ !empty($savedsettings['slide_menu_bg_color'])?$savedsettings['slide_menu_bg_color']:'rgba(0,0,0,1)' }}" class="form-control">
										<span class="input-group-btn add-on">
											<button class="btn btn-white" type="button">
												<i style="background-color:{{ !empty($savedsettings['slide_menu_bg_color'])?$savedsettings['slide_menu_bg_color']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group col-sm-3">
									<p class="text-muted m-b-15 font-15">Slide Menu Text Color</p>
									<div data-color-format="rgba" data-color="{{ !empty($savedsettings['slide_menu_text_color'])?$savedsettings['slide_menu_text_color']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
										<input type="text" name="slide_menu_text_color" value="{{ !empty($savedsettings['slide_menu_text_color'])?$savedsettings['slide_menu_text_color']:'rgba(0,0,0,1)' }}" class="form-control">
										<span class="input-group-btn add-on">
											<button class="btn btn-white" type="button">
												<i style="background-color:{{ !empty($savedsettings['slide_menu_text_color'])?$savedsettings['slide_menu_text_color']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group col-sm-3">
									<p class="text-muted m-b-15 font-15">Slide Toggle Button Color</p>
									<div data-color-format="rgba" data-color="{{ !empty($savedsettings['slide_toggle_bg_color'])?$savedsettings['slide_toggle_bg_color']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
										<input type="text" name="slide_toggle_bg_color" value="{{ !empty($savedsettings['slide_toggle_bg_color'])?$savedsettings['slide_toggle_bg_color']:'rgba(0,0,0,1)' }}" class="form-control">
										<span class="input-group-btn add-on">
											<button class="btn btn-white" type="button">
												<i style="background-color:{{ !empty($savedsettings['slide_toggle_bg_color'])?$savedsettings['slide_toggle_bg_color']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
											</button>
										</span>
									</div>
								</div>
						</div>
						<div class="row">
							<div class="form-group  col-sm-12">
								<p class="text-muted m-b-15 font-15">Slide Menu Custom Content</p>
								<textarea id="slide_toggle_custom" name="slide_toggle_custom">{{ !empty($savedsettings['slide_toggle_custom'])?$savedsettings['slide_toggle_custom']:'' }}</textarea>
							</div>
						</div>
						@endif
				</div>
				<div class="tab-pane" id="tab3">

					<h4 class="text-muted m-b-15 font-15">Menu Style</h4>


					@if(sizeof($settings['menustyle'])>0)
									<div class="form-group">
										<p class="text-muted m-b-15 font-15">Preload Style</p>
										<select class="selectpicker" data-live-search="true" name="menustyle"  data-style="btn-white">
											<option value="">Default</option>
											@foreach($settings['menustyle'] as $key=>$menustyle)
											<option value="{{ $key }}"
											@if(!empty($savedsettings['menustyle']) && $key==$savedsettings['menustyle'])
												selected
											@endif
											>{{ $menustyle }}</option>
											@endforeach
										</select>
								</div>
					@endif


					<div class="accordion" id="accordion">
								<div class="card">
									<div class="card-header" id="headingOne">
											<a class="collapsed card-link" data-toggle="collapse" href="#collapseOne">
												 Toggle Button
											</a>
									</div>

									<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
										<div class="card-body">



											<div class="form-group">
												<p class="text-muted m-b-15 font-15">Toggle Button Background Color</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['toggle_button_bg_color'])?$savedsettings['toggle_button_bg_color']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="toggle_button_bg_color" value="{{ !empty($savedsettings['toggle_button_bg_color'])?$savedsettings['toggle_button_bg_color']:'rgba(0,0,0,1)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($savedsettings['toggle_button_bg_color'])?$savedsettings['toggle_button_bg_color']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group">
												<p class="text-muted m-b-15 font-15">Toggle Button Icon Color</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['toggle_button_color'])?$savedsettings['toggle_button_color']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="toggle_button_color" value="{{ !empty($savedsettings['toggle_button_color'])?$savedsettings['toggle_button_color']:'rgba(255,255,255,1)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($savedsettings['toggle_button_color'])?$savedsettings['toggle_button_color']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>



											<div class="form-group">
												<p class="text-muted m-b-15 font-15">Toggle Menu Background Color</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['toggle_menu_bg_color'])?$savedsettings['toggle_menu_bg_color']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="toggle_menu_bg_color" value="{{ !empty($savedsettings['toggle_menu_bg_color'])?$savedsettings['toggle_menu_bg_color']:'rgba(255,255,255,1)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($savedsettings['toggle_menu_bg_color'])?$savedsettings['toggle_menu_bg_color']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>

											<div class="form-group">
												<p class="text-muted m-b-15 font-15">Toggle Menu Item Font Size (px)</p>
												<input type="number" min="0" name="toggle_menu_item_font_size" value="{{ !empty($savedsettings['toggle_menu_item_font_size'])?$savedsettings['toggle_menu_item_font_size']:!empty($savedsettings['header_menu_items_font_size'])?$savedsettings['header_menu_items_font_size']:'' }}" class="form-control">
											</div>

											<div class="form-group">
												<p class="text-muted m-b-15 font-15">Toggle Menu Item Color</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['toggle_menu_item_color'])?$savedsettings['toggle_menu_item_color']:!empty($savedsettings['header_menu_items_color'])?$savedsettings['header_menu_items_color']:'' }}" class="colorpicker-rgba input-group">
													<input type="text" name="toggle_menu_item_color" value="{{ !empty($savedsettings['toggle_menu_item_color'])?$savedsettings['toggle_menu_item_color']:!empty($savedsettings['header_menu_items_color'])?$savedsettings['header_menu_items_color']:'' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($savedsettings['toggle_menu_item_color'])?$savedsettings['toggle_menu_item_color']:!empty($savedsettings['header_menu_items_color'])?$savedsettings['header_menu_items_color']:'' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>

											<div class="form-group">
											  <p class="text-muted m-b-15 font-15">Toggle Menu Item Background Color</p>
											  <div data-color-format="rgba" data-color="{{ !empty($savedsettings['toggle_menu_item_bg_color'])?$savedsettings['toggle_menu_item_bg_color']:!empty($savedsettings['header_menu_items_bg_color'])?$savedsettings['header_menu_items_bg_color']:'' }}" class="colorpicker-rgba input-group">
											    <input type="text" name="toggle_menu_item_bg_color" value="{{ !empty($savedsettings['toggle_menu_item_bg_color'])?$savedsettings['toggle_menu_item_bg_color']:!empty($savedsettings['header_menu_items_bg_color'])?$savedsettings['header_menu_items_bg_color']:'' }}" class="form-control">
											    <span class="input-group-btn add-on">
											      <button class="btn btn-white" type="button">
											        <i style="background-color:{{ !empty($savedsettings['toggle_menu_item_bg_color'])?$savedsettings['toggle_menu_item_bg_color']:!empty($savedsettings['header_menu_items_bg_color'])?$savedsettings['header_menu_items_bg_color']:'' }};margin-top: 2px;"></i>
											      </button>
											    </span>
											  </div>
											</div>



											<div class="form-group">
											  <p class="text-muted m-b-15 font-15">Toggle Menu Item Color (Hover)</p>
											  <div data-color-format="rgba" data-color="{{ !empty($savedsettings['toggle_menu_item_color_hover'])?$savedsettings['toggle_menu_item_color_hover']:!empty($savedsettings['header_menu_items_color_hover'])?$savedsettings['header_menu_items_color_hover']:'' }}" class="colorpicker-rgba input-group">
											    <input type="text" name="toggle_menu_item_color_hover" value="{{ !empty($savedsettings['toggle_menu_item_color_hover'])?$savedsettings['toggle_menu_item_color_hover']:!empty($savedsettings['header_menu_items_color_hover'])?$savedsettings['header_menu_items_color_hover']:'' }}" class="form-control">
											    <span class="input-group-btn add-on">
											      <button class="btn btn-white" type="button">
											        <i style="background-color:{{ !empty($savedsettings['toggle_menu_item_color_hover'])?$savedsettings['toggle_menu_item_color_hover']:!empty($savedsettings['header_menu_items_color_hover'])?$savedsettings['header_menu_items_color_hover']:'' }};margin-top: 2px;"></i>
											      </button>
											    </span>
											  </div>
											</div>

											<div class="form-group">
											  <p class="text-muted m-b-15 font-15">Toggle Menu Item Background Color (Hover)</p>
											  <div data-color-format="rgba" data-color="{{ !empty($savedsettings['toggle_menu_item_bg_color_hover'])?$savedsettings['toggle_menu_item_bg_color_hover']:!empty($savedsettings['header_menu_items_bg_color_hover'])?$savedsettings['header_menu_items_bg_color_hover']:'' }}" class="colorpicker-rgba input-group">
											    <input type="text" name="toggle_menu_item_bg_color_hover" value="{{ !empty($savedsettings['toggle_menu_item_bg_color_hover'])?$savedsettings['toggle_menu_item_bg_color_hover']:!empty($savedsettings['header_menu_items_bg_color_hover'])?$savedsettings['header_menu_items_bg_color_hover']:'' }}" class="form-control">
											    <span class="input-group-btn add-on">
											      <button class="btn btn-white" type="button">
											        <i style="background-color:{{ !empty($savedsettings['toggle_menu_item_bg_color_hover'])?$savedsettings['toggle_menu_item_bg_color_hover']:!empty($savedsettings['header_menu_items_bg_color_hover'])?$savedsettings['header_menu_items_bg_color_hover']:'' }};margin-top: 2px;"></i>
											      </button>
											    </span>
											  </div>
											</div>

										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header" id="headingTwo">
											<a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
												Menu items
											</a>
									</div>
									<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
										<div class="card-body row">



											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Background Color</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['header_menu_items_bg_color'])?$savedsettings['header_menu_items_bg_color']:'rgba(255,255,255,0)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="header_menu_items_bg_color" value="{{ !empty($savedsettings['header_menu_items_bg_color'])?$savedsettings['header_menu_items_bg_color']:'rgba(255,255,255,0)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color: {{ !empty($savedsettings['header_menu_items_bg_color'])?$savedsettings['header_menu_items_bg_color']:'rgba(255,255,255,0)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Background Color (Hover/Active)</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['header_menu_items_bg_color_hover'])?$savedsettings['header_menu_items_bg_color_hover']:'rgba(255,255,255,0)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="header_menu_items_bg_color_hover" value="{{ !empty($savedsettings['header_menu_items_bg_color_hover'])?$savedsettings['header_menu_items_bg_color_hover']:'rgba(255,255,255,0)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color: {{ !empty($savedsettings['header_menu_items_bg_color_hover'])?$savedsettings['header_menu_items_bg_color_hover']:'rgba(255,255,255,0)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Font Size (px)</p>
												<input type="number" min="0" name="header_menu_items_font_size" value="{{ !empty($savedsettings['header_menu_items_font_size'])?$savedsettings['header_menu_items_font_size']:'15' }}" class="form-control">
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Font Family</p>
												<select data-style="btn-white" class="form-control selectpicker" name="header_menu_items_font_family">
													<option value="">Default</option>
													@if(!empty(getSetting('imported_font_name','styles')))
														@foreach(getSetting('imported_font_name','styles') as $key=>$name)
															<option value="{!! $name !!}" {{ !empty($savedsettings['header_menu_items_font_family']) && $savedsettings['header_menu_items_font_family']==$name?'selected':'' }}>{{ $name }}</option>
														@endforeach
													@endif
											</select>
											</div>

											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Transform</p>
												<select data-style="btn-white" class="form-control selectpicker" name="header_menu_items_text_transform">
													<option value="">None</option>
													<option value="uppercase" {{ !empty($savedsettings['header_menu_items_text_transform']) && $savedsettings['header_menu_items_text_transform']=='uppercase'?'selected':'' }}>Uppercase</option>
													<option value="lowercase" {{ !empty($savedsettings['header_menu_items_text_transform']) && $savedsettings['header_menu_items_text_transform']=='lowercase'?'selected':'' }}>Lowercase</option>
											</select>
											</div>

											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Letter Spacing (px)</p>
												<input type="number" min="0" name="header_menu_items_letter_spacing" value="{{ !empty($savedsettings['header_menu_items_letter_spacing'])?$savedsettings['header_menu_items_letter_spacing']:'0' }}" class="form-control">
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Border Style</p>
												<select name="header_menu_items_border_style" data-style="btn-white" class="form-control selectpicker">
														<option value="solid" {{ !empty($savedsettings['header_menu_items_border_style']) && $savedsettings['header_menu_items_border_style']=='solid'?'selected':'' }}>Solid</option>
														<option value="dashed" {{ !empty($savedsettings['header_menu_items_border_style']) && $savedsettings['header_menu_items_border_style']=='dashed'?'selected':'' }}>Dashed</option>
														<option value="dotted" {{ !empty($savedsettings['header_menu_items_border_style']) && $savedsettings['header_menu_items_border_style']=='dotted'?'selected':'' }}>Dotted</option>
														<option value="double" {{ !empty($savedsettings['header_menu_items_border_style']) && $savedsettings['header_menu_items_border_style']=='double'?'selected':'' }}>Double</option>
														<option value="groove" {{ !empty($savedsettings['header_menu_items_border_style']) && $savedsettings['header_menu_items_border_style']=='groove'?'selected':'' }}>Groove</option>
														<option value="ridge" {{ !empty($savedsettings['header_menu_items_border_style']) && $savedsettings['header_menu_items_border_style']=='ridge'?'selected':'' }}>Ridge</option>
													</select>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Padding (px)</p>
												<div class="row">
														<div class="col-md-3"><input type="number" min="0" name="header_menu_items_padding_left" value="{{ !empty($savedsettings['header_menu_items_padding_left'])?$savedsettings['header_menu_items_padding_left']:'5' }}" class="form-control"></div>
														<div class="col-md-3"><input type="number" min="0" name="header_menu_items_padding_right" value="{{ !empty($savedsettings['header_menu_items_padding_right'])?$savedsettings['header_menu_items_padding_right']:'5' }}" class="form-control"></div>
														<div class="col-md-3"><input type="number" min="0" name="header_menu_items_padding_top" value="{{ !empty($savedsettings['header_menu_items_padding_top'])?$savedsettings['header_menu_items_padding_top']:'10' }}" class="form-control"></div>
														<div class="col-md-3"><input type="number" min="0" name="header_menu_items_padding_bottom" value="{{ !empty($savedsettings['header_menu_items_padding_bottom'])?$savedsettings['header_menu_items_padding_bottom']:'10' }}" class="form-control"></div>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Margin (px)</p>
												<div class="row">
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_margin_left" value="{{ !empty($savedsettings['header_menu_items_margin_left'])?$savedsettings['header_menu_items_margin_left']:'10' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_margin_right" value="{{ !empty($savedsettings['header_menu_items_margin_right'])?$savedsettings['header_menu_items_margin_right']:'10' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_margin_top" value="{{ !empty($savedsettings['header_menu_items_margin_top'])?$savedsettings['header_menu_items_margin_top']:'5' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_margin_bottom" value="{{ !empty($savedsettings['header_menu_items_margin_bottom'])?$savedsettings['header_menu_items_margin_bottom']:'5' }}" class="form-control"></div>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Border Width (px)</p>
												<div class="row">
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_border_width_left" value="{{ !empty($savedsettings['header_menu_items_border_width_left'])?$savedsettings['header_menu_items_border_width_left']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_border_width_right" value="{{ !empty($savedsettings['header_menu_items_border_width_right'])?$savedsettings['header_menu_items_border_width_right']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_border_width_top" value="{{ !empty($savedsettings['header_menu_items_border_width_top'])?$savedsettings['header_menu_items_border_width_top']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_border_width_bottom" value="{{ !empty($savedsettings['header_menu_items_border_width_bottom'])?$savedsettings['header_menu_items_border_width_bottom']:'0' }}" class="form-control"></div>
												</div>
											</div>

											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Border Radius (px)</p>
												<div class="row">
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_border_radius_top_left" value="{{ !empty($savedsettings['header_menu_items_border_radius_top_left'])?$savedsettings['header_menu_items_border_radius_top_left']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_border_radius_top_right" value="{{ !empty($savedsettings['header_menu_items_border_radius_top_right'])?$savedsettings['header_menu_items_border_radius_top_right']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_border_radius_bottom_left" value="{{ !empty($savedsettings['header_menu_items_border_radius_bottom_left'])?$savedsettings['header_menu_items_border_radius_bottom_left']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_menu_items_border_radius_bottom_right" value="{{ !empty($savedsettings['header_menu_items_border_radius_bottom_right'])?$savedsettings['header_menu_items_border_radius_bottom_right']:'0' }}" class="form-control"></div>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Color</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['header_menu_items_color'])?$savedsettings['header_menu_items_color']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="header_menu_items_color" value="{{ !empty($savedsettings['header_menu_items_color'])?$savedsettings['header_menu_items_color']:'rgba(0,0,0,1)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($savedsettings['header_menu_items_color'])?$savedsettings['header_menu_items_color']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Color (Hover/Active)</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['header_menu_items_color_hover'])?$savedsettings['header_menu_items_color_hover']:'rgba(0,0,0,.5)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="header_menu_items_color_hover" value="{{ !empty($savedsettings['header_menu_items_color_hover'])?$savedsettings['header_menu_items_color_hover']:'rgba(0,0,0,.5)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color: {{ !empty($savedsettings['header_menu_items_color_hover'])?$savedsettings['header_menu_items_color_hover']:'rgba(0,0,0,.5)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>



										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header" id="headingThree">
										<a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
											Sub Menu items
										</a>
									</div>
									<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
										<div class="card-body row">



											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Background Color</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['header_submenu_items_bg_color'])?$savedsettings['header_submenu_items_bg_color']:'rgba(255,255,255,0)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="header_submenu_items_bg_color" value="{{ !empty($savedsettings['header_submenu_items_bg_color'])?$savedsettings['header_submenu_items_bg_color']:'rgba(255,255,255,0)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color: {{ !empty($savedsettings['header_submenu_items_bg_color'])?$savedsettings['header_submenu_items_bg_color']:'rgba(255,255,255,0)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Background Color (Hover/Active)</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['header_submenu_items_bg_color_hover'])?$savedsettings['header_submenu_items_bg_color_hover']:'rgba(255,255,255,0)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="header_submenu_items_bg_color_hover" value="{{ !empty($savedsettings['header_submenu_items_bg_color_hover'])?$savedsettings['header_submenu_items_bg_color_hover']:'rgba(255,255,255,0)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color: {{ !empty($savedsettings['header_submenu_items_bg_color_hover'])?$savedsettings['header_submenu_items_bg_color_hover']:'rgba(255,255,255,0)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Font Size (px)</p>
												<input type="number" min="0" name="header_submenu_items_font_size" value="{{ !empty($savedsettings['header_submenu_items_font_size'])?$savedsettings['header_submenu_items_font_size']:'15' }}" class="form-control">
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Font Family</p>
												<select data-style="btn-white" class="form-control selectpicker" name="header_submenu_items_font_family">
													<option value="">Default</option>
													@if(!empty(getSetting('imported_font_name','styles')))
														@foreach(getSetting('imported_font_name','styles') as $key=>$name)
															<option value="{!! $name !!}" {{ !empty($savedsettings['header_submenu_items_font_family']) && $savedsettings['header_submenu_items_font_family']==$name?'selected':'' }}>{{ $name }}</option>
														@endforeach
													@endif
											</select>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Transform</p>
												<select data-style="btn-white" class="form-control selectpicker" name="header_submenu_items_text_transform">
													<option value="">None</option>
													<option value="uppercase" {{ !empty($savedsettings['header_submenu_items_text_transform']) && $savedsettings['header_submenu_items_text_transform']=='uppercase'?'selected':'' }}>Uppercase</option>
													<option value="lowercase" {{ !empty($savedsettings['header_submenu_items_text_transform']) && $savedsettings['header_submenu_items_text_transform']=='lowercase'?'selected':'' }}>Lowercase</option>
											</select>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Letter Spacing (px)</p>
												<input type="number" min="0" name="header_submenu_items_letter_spacing" value="{{ !empty($savedsettings['header_submenu_items_letter_spacing'])?$savedsettings['header_submenu_items_letter_spacing']:'0' }}" class="form-control">
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Border Style</p>
												<select name="header_submenu_items_border_style" data-style="btn-white" class="form-control selectpicker">
														<option value="solid" {{ !empty($savedsettings['header_submenu_items_border_style']) && $savedsettings['header_submenu_items_border_style']=='solid'?'selected':'' }}>Solid</option>
														<option value="dashed" {{ !empty($savedsettings['header_submenu_items_border_style']) && $savedsettings['header_submenu_items_border_style']=='dashed'?'selected':'' }}>Dashed</option>
														<option value="dotted" {{ !empty($savedsettings['header_submenu_items_border_style']) && $savedsettings['header_submenu_items_border_style']=='dotted'?'selected':'' }}>Dotted</option>
														<option value="double" {{ !empty($savedsettings['header_submenu_items_border_style']) && $savedsettings['header_submenu_items_border_style']=='double'?'selected':'' }}>Double</option>
														<option value="groove" {{ !empty($savedsettings['header_submenu_items_border_style']) && $savedsettings['header_submenu_items_border_style']=='groove'?'selected':'' }}>Groove</option>
														<option value="ridge" {{ !empty($savedsettings['header_submenu_items_border_style']) && $savedsettings['header_submenu_items_border_style']=='ridge'?'selected':'' }}>Ridge</option>
													</select>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Padding (px)</p>
												<div class="row">
														<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_padding_left" value="{{ !empty($savedsettings['header_submenu_items_padding_left'])?$savedsettings['header_submenu_items_padding_left']:'5' }}" class="form-control"></div>
														<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_padding_right" value="{{ !empty($savedsettings['header_submenu_items_padding_right'])?$savedsettings['header_submenu_items_padding_right']:'5' }}" class="form-control"></div>
														<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_padding_top" value="{{ !empty($savedsettings['header_submenu_items_padding_top'])?$savedsettings['header_submenu_items_padding_top']:'10' }}" class="form-control"></div>
														<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_padding_bottom" value="{{ !empty($savedsettings['header_submenu_items_padding_bottom'])?$savedsettings['header_submenu_items_padding_bottom']:'10' }}" class="form-control"></div>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Margin (px)</p>
												<div class="row">
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_margin_left" value="{{ !empty($savedsettings['header_submenu_items_margin_left'])?$savedsettings['header_submenu_items_margin_left']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_margin_right" value="{{ !empty($savedsettings['header_submenu_items_margin_right'])?$savedsettings['header_submenu_items_margin_right']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_margin_top" value="{{ !empty($savedsettings['header_submenu_items_margin_top'])?$savedsettings['header_submenu_items_margin_top']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_margin_bottom" value="{{ !empty($savedsettings['header_submenu_items_margin_bottom'])?$savedsettings['header_submenu_items_margin_bottom']:'0' }}" class="form-control"></div>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Border Width (px)</p>
												<div class="row">
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_border_width_left" value="{{ !empty($savedsettings['header_submenu_items_border_width_left'])?$savedsettings['header_submenu_items_border_width_left']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_border_width_right" value="{{ !empty($savedsettings['header_submenu_items_border_width_right'])?$savedsettings['header_submenu_items_border_width_right']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_border_width_top" value="{{ !empty($savedsettings['header_submenu_items_border_width_top'])?$savedsettings['header_submenu_items_border_width_top']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_border_width_bottom" value="{{ !empty($savedsettings['header_submenu_items_border_width_bottom'])?$savedsettings['header_submenu_items_border_width_bottom']:'0' }}" class="form-control"></div>
												</div>
											</div>

											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Border Radius (px)</p>
												<div class="row">
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_border_radius_top_left" value="{{ !empty($savedsettings['header_submenu_items_border_radius_top_left'])?$savedsettings['header_submenu_items_border_radius_top_left']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_border_radius_top_right" value="{{ !empty($savedsettings['header_submenu_items_border_radius_top_right'])?$savedsettings['header_submenu_items_border_radius_top_right']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_border_radius_bottom_left" value="{{ !empty($savedsettings['header_submenu_items_border_radius_bottom_left'])?$savedsettings['header_submenu_items_border_radius_bottom_left']:'0' }}" class="form-control"></div>
													<div class="col-md-3"><input type="number" min="0" name="header_submenu_items_border_radius_bottom_right" value="{{ !empty($savedsettings['header_submenu_items_border_radius_bottom_right'])?$savedsettings['header_submenu_items_border_radius_bottom_right']:'0' }}" class="form-control"></div>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Color</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['header_submenu_items_color'])?$savedsettings['header_submenu_items_color']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="header_submenu_items_color" value="{{ !empty($savedsettings['header_submenu_items_color'])?$savedsettings['header_submenu_items_color']:'rgba(0,0,0,1)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($savedsettings['header_submenu_items_color'])?$savedsettings['header_submenu_items_color']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 font-15">Text Color (Hover/Active)</p>
												<div data-color-format="rgba" data-color="{{ !empty($savedsettings['header_submenu_items_color_hover'])?$savedsettings['header_submenu_items_color_hover']:'rgba(0,0,0,.5)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="header_submenu_items_color_hover" value="{{ !empty($savedsettings['header_submenu_items_color_hover'])?$savedsettings['header_submenu_items_color_hover']:'rgba(0,0,0,.5)' }}" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color: {{ !empty($savedsettings['header_submenu_items_color_hover'])?$savedsettings['header_submenu_items_color_hover']:'rgba(0,0,0,.5)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>




										</div>
									</div>
								</div>
							</div>


				</div>
				<div class="tab-pane" id="tab4">
					@if(sizeof($settings['options'])>0)
						@foreach($settings['options'] as $key=>$header)
							<div class="form-group">
								<div class="form-group">
								<div class="checkbox">
									<input id="checkbox{{ $key }}" name="{{ $key }}" type="checkbox"
									@if(!empty($savedsettings[$key]))
										checked
									@endif
									>
									<label for="checkbox{{ $key }}">
									{{ $header['title'] }}
									</label>
								</div>
							</div>
								@foreach($header['extra'] as $key=>$extra)
									@if($extra['type'] == "checkbox")
									<div class="form-group">
										<div class="checkbox" style="margin-left: 20px;font-size: 13px;">
											<input id="checkbox{{ $key }}" name="{{ $key }}" type="checkbox"
											@if(!empty($savedsettings[$key]))
												checked
											@endif
											>
											<label for="checkbox{{ $key }}">
											{{ $extra['title'] }}
											</label>
										</div>
									</div>
									@elseif($extra['type'] == "picker")
									<div class="form-group">
										<p class="text-muted m-b-15 font-15">{{ $extra['title'] }}</p>
										<div data-color-format="rgba" data-color="@if(!empty($savedsettings[$key])) {{ $savedsettings[$key] }} @endif" class="colorpicker-rgba input-group">
											<input type="text" name="{{ $key }}" value="" class="form-control">
											<span class="input-group-btn add-on">
												<button class="btn btn-white" type="button">
													<i style="background-color: @if(!empty($savedsettings[$key])) {{ $savedsettings[$key] }} @endif;margin-top: 2px;"></i>
												</button>
											</span>
										</div>
									</div>
									@endif
								@endforeach
							</div>
						@endforeach
					@endif

				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<button type="submit" id="saveall" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
</div>
</form>
@include('admin._partials.popupuploader')
<script>
	var resizefunc = [];
</script>
<script>
$(document).ready(function() {
	$('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
	$('.colorpicker-rgba').colorpicker();
})
</script>
<script>
$(document).ready(function(){
	$('.addedimage a').on('click', function(){
		jQuery('.choosemedia').removeClass('selected');
		$(this).parents('.choosemedia').toggleClass('selected');
	})
	$('#media-select').on('click','.thumbnail a', function(){
		var image =$(this).attr('href');
		$('.choosemedia.selected').find('.image').css('background-image', 'url('+image+')');
		$('.choosemedia.selected').find('.image').addClass('image-has');
		$('.choosemedia.selected').find('.image').next('input[type="hidden"]').val(image);
		$('#media-select').removeClass('md-custom-show');
		return false;
	})
	$('.removeimage').click(function(){
		$(this).parents('.image').css('background-image', 'none');
		$(this).parents('.image').removeClass('image-has');
		$(this).parents('.image').next('input[type="hidden"]').val('');
		return false;
	})

	$('#save').click(function(){
		$('#header-form').submit();
	})
})
</script>
<script src="{{ asset('assets/private/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
$(document).ready(function () {
	if($("#slide_toggle_custom").length > 0){
		tinymce.init({
				selector: "textarea#slide_toggle_custom",
				theme: "modern",
				height:300,
				plugins: [
					"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
					"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
					"save table contextmenu directionality template paste textcolor",
					"fontawesome noneditable"
				],
				content_css: 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
				valid_elements : '*[*]',
				noneditable_noneditable_class: 'fa',
				toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons fontawesome",
				extended_valid_elements: 'span[*]',
				document_base_url: '',
				relative_urls : false,
				style_formats: [
					{title: 'Bold text', inline: 'b'},
					{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
					{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
					{title: 'Example 1', inline: 'span', classes: 'example1'},
					{title: 'Example 2', inline: 'span', classes: 'example2'},
					{title: 'Table styles'},
					{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
				]
			});
	}
});
</script>
@endsection
