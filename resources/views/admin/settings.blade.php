@extends('layouts.app')

@section('extra')

<style>
.hint{

}
</style>

@endsection

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Settings <span>Miscellaneous Settings</h4>
		</div>
		@if (\Session::has('success'))
			<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
			</div>
		@elseif(\Session::has('error'))
			<div class="col-sm-12 m-t-15">
				<div class="alert alert-danger">
					{!! \Session::get('error') !!}
				</div>
			</div>
		@endif
		<div class="col-sm-12 m-t-15">
			<ul class="nav nav-tabs tabs">
				<li class="tab">
					<a href="#tab1" class="active" data-toggle="tab" aria-expanded="false">
						<span>Live Setting</span>
					</a>
				</li>
				<li class="tab">
					<a href="#tab4" data-toggle="tab" aria-expanded="false">
						<span>Firebase</span>
					</a>
				</li>
				<li class="tab">
					<a href="#tab2" data-toggle="tab" aria-expanded="false">
						<span>Cloudflare</span>
					</a>
				</li>
				@if(checkCloudflareStatus()=='active')
				<li class="tab">
					<a href="#tab3" data-toggle="tab" aria-expanded="false">
						<span>Cloudflare Settings</span>
					</a>
				</li>
				@endif
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">
					<form method="post" action="{{ route("settings.saveHost") }}">
						{{ csrf_field() }}
								<div class="row">
									<div class="col-sm-6">
										<h4 class="text-muted m-b-15 m-t-15 font-15">Production Url:</h4>
										<input type="text" class="form-control" id="prod_url" name="prod_url" value="{{ !empty($savedsettings['prod_url'])? $savedsettings['prod_url'] : '' }}">
									</div>
								</div>
								<div class="row hidden">
									<div class="col-sm-6 m-t-15">
										<div class="card-box">
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Host:</h4>
												<select class="form-control" name="type" id="type">
													<option value="git" selected>Git</option>
												</select>
											</div>
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">User:</h4>
												<input type="text" class="form-control" name="user" id="user" value="{!! !empty($savedsettings['user'])?$savedsettings['user']:'' !!}">
											</div>
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Email:</h4>
												<input type="text" class="form-control" name="email" id="email" value="{!! !empty($savedsettings['email'])?$savedsettings['email']:'' !!}">
											</div>
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Org:</h4>
												<input type="text" class="form-control" name="org" id="org" value="{!! !empty($savedsettings['org'])?$savedsettings['org']:'' !!}">
											</div>
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Token:</h4>
												<input type="text" class="form-control" name="token" id="token" value="{!! !empty($savedsettings['token'])?$savedsettings['token']:'' !!}">
											</div>
											@if(!empty($savedsettings['repoid']))
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Repo ID:</h4>
												<input type="text" class="form-control" id="repoid" value="{!! $savedsettings['repoid'] !!}" readonly>
											</div>
											@endif
										</div>
									</div>
							</div>
						<div class="form-group m-t-15">
							@if(empty($savedsettings['repoid']))
							<button type="submit" class="btn btn-default">Update</button>
							@endif
							<a href="{{ route('deleteRepo') }}" class="btn btn-default">Reset</a>
							<input type="hidden" name="settingType" value="host">
						</div>
						</form>
				</div>
<div class="tab-pane" id="tab2">
	<form method="post" action="{{ route('settings.saveHost') }}">
			{{ csrf_field() }}
				<div class="tab-pane" id="tab2">
					<input type="hidden" name="activetab" value="">
					<div class="row cloudflaresettings">
						<div class="col-sm-6 m-t-15 m-b-15">
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Cloudflare Email<sup style="color:#ff0000">*</sup>:</h4>
									<input type="text" class="form-control" name="cloud_email" {{ !empty(getSetting('zone_id', 'cloudflare'))?'readonly':'' }} value="{{ getSetting('cloud_email', 'cloudflare') }}" required>
								</div>
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Cloudflare apikey<sup style="color:#ff0000">*</sup>:</h4>
									<input type="text" class="form-control" {{ !empty(getSetting('zone_id', 'cloudflare'))?'readonly':'' }} name="cloud_apikey" value="{{ getSetting('cloud_apikey', 'cloudflare') }}" required>
								</div>
								<div class="form-group {{ !empty(getSetting('zone_id', 'cloudflare'))?'':'hidden' }}">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Cloudflare Zone ID:</h4>
									<input type="text" readonly class="form-control" name="zone_id" value="{{ getSetting('zone_id', 'cloudflare') }}">
								</div>
								<div class="form-group">
									<input type="hidden" class="form-control" name="cloud_domain" value="{{ !empty($savedsettings['prod_url'])? $savedsettings['prod_url'] : '' }}">
									@if(!empty(getSetting('zone_id', 'cloudflare')))
									<input type="hidden" name="settingType" value="cloudflareDisconnect">
									<button type="submit" class="btn btn-default">Disconnect</button>
									@else
									<input type="hidden" name="settingType" value="cloudflareConnect">
									<button type="submit" class="btn btn-default">Connect</button>
									@endif
								</div>
						</div>
						<div class="col-sm-6 m-t-15 m-b-15">
							<div class="alert alert-success" role="alert">

							<h4>Status: {!! (checkCloudflareStatus()=='fail')?'<span style="color: red">Not Connected</span>':'' !!}
							@if(checkCloudflareStatus()!='fail')
									{!! (checkCloudflareStatus()=='active')?'<span style="color: green">Active</span>':'<span style="color: red">Not Active</span>' !!}
							@endif
							</h4>
							<p>Once Cloudflare is connected please make sure to add following nameservers to your domain regitrar. If status is "on" you will see another tab for Cloudflare settings where you can control your Cloudflare features from admin.</p>
							<p><i class="fa fa-server"></i> Nameserver 1<br />
								<input type="text" value="cheryl.ns.cloudflare.com" class="form-control" readonly>
							</p>
							<p><i class="fa fa-server"></i> Nameserver 2<br />
								<input type="text" value="sam.ns.cloudflare.com" class="form-control" readonly>
							</p>
							</div>
						</div>
					</div>
				</div>

</form>
</div>
@if(checkCloudflareStatus()=='active')

				<div class="tab-pane" id="tab3">
					<form method="post" action="{{ route('settings.saveHost') }}">
						<input type="hidden" name="activetab" value="">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12 m-b-15">
									<h4>Minify</h4>
									<label class="switch">
										<input type="checkbox" class="cloudflarecheck" name="cloudflare_minify_css" {{ !empty(getSetting('cloudflare_minify_css', 'cloudflare')) && getSetting('cloudflare_minify_css', 'cloudflare')=='on'?'checked':'' }}>
										<span class="slider red"></span><span class="label">CSS</span>
									</label>
									<label class="switch">
										<input type="checkbox" class="cloudflarecheck" name="cloudflare_minify_js" {{ !empty(getSetting('cloudflare_minify_js', 'cloudflare')) && getSetting('cloudflare_minify_js', 'cloudflare')=='on'?'checked':'' }}>
										<span class="slider red"></span><span class="label">JS</span>
									</label>
									<label class="switch">
										<input type="checkbox" class="cloudflarecheck" name="cloudflare_minify_html" {{ !empty(getSetting('cloudflare_minify_html', 'cloudflare')) && getSetting('cloudflare_minify_html', 'cloudflare')=='on'?'checked':'' }}>
										<span class="slider red"></span><span class="label">HTML</span>
									</label>
								</div>
							</div>
							<input type="hidden" name="settingType" value="cloudflareMinify">
					</form>
					<form method="post" action="{{ route('settings.saveHost') }}">
						<input type="hidden" name="activetab" value="">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12 m-b-15">
									<h4>Development Mode</h4>
									<label class="switch">
										<input type="checkbox" class="cloudflarecheck" name="cloudflare_developement_mode" {{ !empty(getSetting('cloudflare_developement_mode', 'cloudflare')) && getSetting('cloudflare_developement_mode', 'cloudflare')=='on'?'checked':'' }}>
										<span class="slider red"></span><span class="label">On</span>
									</label>
								</div>
							</div>
							<input type="hidden" name="settingType" value="cloudflareDevelopement">
					</form>
					<form method="post" action="{{ route('settings.saveHost') }}">
						<input type="hidden" name="activetab" value="">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12 m-b-15">
									<h4>Enable Free SSL</h4>
									<label class="switch">
										<input type="checkbox" class="cloudflarecheck" name="cloudflare_ssl" {{ !empty(getSetting('cloudflare_ssl', 'cloudflare')) && getSetting('cloudflare_ssl', 'cloudflare')=='on'?'checked':'' }}>
										<span class="slider red"></span><span class="label">On</span>
									</label>
								</div>
							</div>
							<input type="hidden" name="settingType" value="cloudflareSSL">
					</form>
					<form method="post" action="{{ route('settings.saveHost') }}">
						<input type="hidden" name="activetab" value="">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12 m-b-15">
									<h4>Always Use HTTPS</h4>
									<label class="switch">
										<input type="checkbox" class="cloudflarecheck" name="cloudflare_ssl_always" {{ !empty(getSetting('cloudflare_ssl_always', 'cloudflare')) && getSetting('cloudflare_ssl_always', 'cloudflare')=='on'?'checked':'' }}>
										<span class="slider red"></span><span class="label">On</span>
									</label>
								</div>
							</div>
							<input type="hidden" name="settingType" value="cloudflareAlwaysSSl">
					</form>
					<form method="post" action="{{ route('settings.saveHost') }}">
						<input type="hidden" name="activetab" value="">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12 m-b-15">
									<h4>Force HTTPS</h4>
									<label class="switch">
										<input type="checkbox" class="cloudflarecheck" name="cloudflare_force_ssl" {{ !empty(getSetting('cloudflare_force_ssl', 'cloudflare')) && getSetting('cloudflare_force_ssl', 'cloudflare')=='on'?'checked':'' }}>
										<span class="slider red"></span><span class="label">On</span>
									</label>
								</div>
							</div>
							<input type="hidden" name="settingType" value="cloudflareForcessl">
					</form>
					<form method="post" action="{{ route('settings.saveHost') }}">
						<input type="hidden" name="activetab" value="">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12 m-b-15">
									<h4>Clear Cache</h4>
									<input type="submit" value="Purge All Cache" class="btn btn-danger">
								</div>
							</div>
							<input type="hidden" name="settingType" value="cloudflarePurgeCache">
					</form>
				</div>

@endif
					<div class="tab-pane" id="tab4">
					<form method="post" action="{{ route("settings.saveHost") }}">
						<input type="hidden" name="activetab" value="">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-sm-6 m-b-15">
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">apiKey<sup style="color:#ff0000">*</sup>:</h4>
									<input type="text" class="form-control" name="apiKey" id="apiKey" value="{{ getSetting('apiKey', 'firebase') }}" required>
								</div>
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">authDomain<sup style="color:#ff0000">*</sup>:</h4>
									<input type="text" class="form-control" name="authDomain" id="authDomain" value="{{ getSetting('authDomain', 'firebase') }}" required>
								</div>
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">databaseURL<sup style="color:#ff0000">*</sup>:</h4>
									<input type="text" class="form-control" name="databaseURL" id="databaseURL" value="{{ getSetting('databaseURL', 'firebase') }}" required>
								</div>
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">projectId<sup style="color:#ff0000">*</sup>:</h4>
									<input type="text" class="form-control" name="projectId" id="projectId" value="{{ getSetting('projectId', 'firebase') }}" required>
								</div>
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">storageBucket<sup style="color:#ff0000">*</sup>:</h4>
									<input type="text" class="form-control" name="storageBucket" id="storageBucket" value="{{ getSetting('storageBucket', 'firebase') }}" required>
								</div>
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">messagingSenderId<sup style="color:#ff0000">*</sup>:</h4>
									<input type="text" class="form-control" name="messagingSenderId" id="messagingSenderId" value="{{ getSetting('messagingSenderId', 'firebase') }}" required>
								</div>
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">appId<sup style="color:#ff0000">*</sup>:</h4>
									<input type="text" class="form-control" name="appId" id="appId" value="{{ getSetting('appId', 'firebase') }}" required>
								</div>
								<div class="form-group">
									<input type="hidden" name="settingType" value="firebase">
									<button type="submit" class="btn btn-default">Submit</button>
								</div>
							</div>
							<div class="col-sm-6 m-b-15">
									@if(!empty(getSetting('databaseURL', 'firebase')))
									<h4 class="text-muted font-15">Features:</h4>
									<div class="row">
											<div class="col-sm-6">
												<div class="checkbox">
													<input id="pagePrivacy" name="pagePrivacy" type="checkbox" {{ !empty(getSetting('pagePrivacy', 'firebase')) && getSetting('pagePrivacy', 'firebase')=='on'?'checked':'' }}>
													<label for="pagePrivacy">
													Enable Page Privacy
													</label>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="checkbox">
													<input id="formEntrySave" name="formEntrySave" type="checkbox" {{ !empty(getSetting('formEntrySave', 'firebase')) && getSetting('formEntrySave', 'firebase')=='on'?'checked':'' }}>
													<label for="formEntrySave">
													Enable Form Entry Save
													</label>
												</div>
											</div>
									</div>
									@endif

							</div>
						</div>
					</form>
					</div>
			</div>
		</div>


	</div>

</div>
<script>
$(document).ready(function(){
		$('.cloudflarecheck').change(function(){
			$(this).parents('form').submit();
		})
})
</script>
@endsection
