@extends('layouts.app')

@section('extra')
<style>
.gal-detail{
	padding-bottom: 0;
}
.gal-detail .theme{
	    padding: 10px;
    background: #fafafa;
    border-top: 1px solid #eaeaea;
}
.gal-detail .theme h4{
	margin:0;
	margin-top: 6px;
}
.thumb{
	border-radius: 0;
	    border: 1px solid #eaeaea;
		box-shadow:none;
}
</style>

@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">All Templates</h4>
		</div>
		@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
						<div class="alert alert-success">
							{!! \Session::get('success') !!}
						</div>
				</div>
			@endif
	@if(!empty(getAdminsetting('templates','')))
		@foreach(getAdminsetting('templates','') as $template)

      <div class="col-sm-6 col-lg-4 col-md-4">
  			<div class="gal-detail thumb">
  				<img src="http://web.duktix.com/templates/Default/screenshot.png" class="thumb-img" alt="work-thumbnail">
  				<div class="row theme">
					<div class="col-6 align-self-center">
						<h4>{{ $template->site_title }}</h4>
					</div>
					<div class="col-6  align-self-center text-right">
					  <form method="POST" action="{{ route('importDemo') }}">
						{{ csrf_field() }}
						<input type="hidden" name="url" value="{{ $template->url }}">
						<input type="hidden" name="host" value="{{ $template->host_user }}">
						<button type="submit" class="btn btn-info btn-rounded waves-effect waves-light"><span class="btn-label"><i class="fa fa-check"></i></span>Import</button>
					  </form>
  					</div>
  				</div>
  			</div>
  		</div>

    @endforeach
@endif
	</div>
</div><!-- /.modal -->

@endsection
