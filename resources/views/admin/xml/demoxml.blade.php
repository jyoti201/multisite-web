<data>
	<posts>
	  @foreach ($posts as $post)
		  <post>
			  <title>{!! $post->title !!}</title>
			  <slug>{!! $post->slug !!}</slug>
			  <htmlcontent><![CDATA[ {!! removeAppurl($post->html) !!} ]]></htmlcontent>
			  <sidebar>{!! $post->sidebar !!}</sidebar>
			  <meta_title>{!! $post->meta_title !!}</meta_title>
			  <meta_description>{!! $post->meta_description !!}</meta_description>
			  <meta_keywords>{!! $post->meta_keywords !!}</meta_keywords>
			  <custom_css>{!! $post->custom_css !!}</custom_css>
			  <featured_image>{!! $post->featured_image !!}</featured_image>
			  <excerpt>{!! $post->excerpt !!}</excerpt>
			  <created_at>{!! $post->created_at !!}</created_at>
			  <updated_at>{!! $post->updated_at !!}</updated_at>
			  <deleted_at>{!! $post->deleted_at !!}</deleted_at>
		  </post>
	  @endforeach
	</posts>

	<pages>
		@foreach ($pages as $page)
		  <page>
			  <title>{!! $page->title !!}</title>
			  <slug>{!! $page->slug !!}</slug>
			  <htmlcontent><![CDATA[ {!! removeAppurl($page->html) !!} ]]></htmlcontent>
			  <sidebar>{!! $page->sidebar !!}</sidebar>
			  <meta_title>{!! $page->meta_title !!}</meta_title>
			  <meta_description>{!! $page->meta_description !!}</meta_description>
			  <meta_keywords>{!! $page->meta_keywords !!}</meta_keywords>
			  <custom_css>{!! $page->custom_css !!}</custom_css>
			  <excerpt>{!! $page->excerpt !!}</excerpt>
			  <created_at>{!! $page->created_at !!}</created_at>
			  <updated_at>{!! $page->updated_at !!}</updated_at>
			  <deleted_at>{!! $page->deleted_at !!}</deleted_at>
		  </page>
		@endforeach
	</pages>

	<widgets>
		@foreach ($widgets as $widget)
		  <widget>
			  <id>{!! $widget->id !!}</id>
			  <type>{!! $widget->type !!}</type>
			  <title>{!! $widget->title !!}</title>
			  <shortcode>{!! $widget->shortcode !!}</shortcode>
			  <setting><![CDATA[ {!! $widget->setting !!} ]]></setting>
			  <created_at>{!! $widget->created_at !!}</created_at>
			  <updated_at>{!! $widget->updated_at !!}</updated_at>
		  </widget>
		@endforeach
	</widgets>

	<menus>
		@foreach ($menus as $menu)
		  <menu>
			  <id>{!! $menu->id !!}</id>
			  <name>{!! $menu->name !!}</name>
			  <menu><![CDATA[ {!! $menu->menu !!} ]]></menu>
			  <order><![CDATA[ {!! $menu->order !!} ]]></order>
			  <created_at>{!! $menu->created_at !!}</created_at>
			  <updated_at>{!! $menu->updated_at !!}</updated_at>
		  </menu>
		@endforeach
	</menus>

	<uploads>
		@foreach ($uploads as $upload)
		  <upload>
			  <id>{!! $upload->id !!}</id>
			  <title>{!! $upload->title !!}</title>
			  <description>{!! $upload->description !!}</description>
			  <slug>{!! $upload->slug !!}</slug>
			  <type>{!! $upload->type !!}</type>
				<downloadurl>{{ getSetting('prod_url', 'host') }}{!! str_replace('/html','', $upload->slug) !!}</downloadurl>
			  <created_at>{!! $upload->created_at !!}</created_at>
			  <updated_at>{!! $upload->updated_at !!}</updated_at>
		  </upload>
		@endforeach
	</uploads>

	<buttons>
			@foreach ($buttons as $button)
			  <button>
				  <id>{!! $button->id !!}</id>
				  <name>{!! $button->name !!}</name>
				  <shortcode>{!! $button->shortcode !!}</shortcode>
				  <setting><![CDATA[ {!! $button->setting !!} ]]></setting>
				  <created_at>{!! $button->created_at !!}</created_at>
				  <updated_at>{!! $button->updated_at !!}</updated_at>
			  </button>
			@endforeach
		</buttons>
		
		
		<popups>
			@foreach ($popups as $popup)
			  <popup>
				  <id>{!! $popup->id !!}</id>
				  <title>{!! $popup->title !!}</title>
				  <class>{!! $popup->class !!}</class>
				  <htmlcontent><![CDATA[ {!! removeAppurl($popup->html) !!} ]]></htmlcontent>
				  <settings><![CDATA[{!! $popup->settings !!} ]]></settings>
				  <created_at>{!! $popup->created_at !!}</created_at>
				  <updated_at>{!! $popup->updated_at !!}</updated_at>
				  <deleted_at>{!! $popup->deleted_at !!}</deleted_at>
			  </popup>
			@endforeach
		</popups>


	<settings>
		@foreach ($settings as $setting)
			@if($setting->type!='host')
		  <setting>
			  <type>{!! $setting->type !!}</type>
			  <setting><![CDATA[{!! json_encode(unserialize($setting->setting)) !!}]]></setting>
			  <created_at>{!! $setting->created_at !!}</created_at>
			  <updated_at>{!! $setting->updated_at !!}</updated_at>
		  </setting>
			@endif
		@endforeach
	</settings>
</data>
