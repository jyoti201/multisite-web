@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<form method="post" id="widgetform" action="{{ route('widget.save') }}">
{{csrf_field()}}
<input type="hidden" name="id" value="{{ $widget->id }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right m-b-20">
							<button class="btn btn-primary waves-effect waves-light btn-lg" type="button" data-toggle="collapse" data-target="#settings" aria-expanded="false" aria-controls="Settings">
						     Setting
						  </button>
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
						</div>
				</div>
				<div class="collapse" id="settings">
					<div class="card-box">
						<div class="row">
								<div class="col-md-6">
									<h4 class="text-muted m-b-15 font-15">Items Setting</h4>
									<div class=" m-t-15">
											<div class="form-group">
												<p class="text-muted m-b-15 m-t-15 font-15">Hover Style</p>
												<select data-style="btn-white" class="form-control selectpicker" id="hoverstyle" name="hoverstyle">
													<option value="">None</option>
													<option value="lily" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='lily'? 'selected':'' }}>Lily</option>
													<option value="sadie" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='sadie'? 'selected':'' }}>Sadie</option>
													<option value="honey" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='honey'? 'selected':'' }}>Honey</option>
													<option value="layla" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='layla'? 'selected':'' }}>Layla</option>
													<option value="zoe" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='zoe'? 'selected':'' }}>Zoe</option>
													<option value="oscar" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='oscar'? 'selected':'' }}>Oscar</option>
													<option value="marley" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='marley'? 'selected':'' }}>Marley</option>
													<option value="ruby" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='ruby'? 'selected':'' }}>Ruby</option>
													<option value="roxy" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='roxy'? 'selected':'' }}>Roxy</option>
													<option value="bubba" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='bubba'? 'selected':'' }}>Bubba</option>
													<option value="romeo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='romeo'? 'selected':'' }}>Romeo</option>
													<option value="dexter" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='dexter'? 'selected':'' }}>Dexter</option>
													<option value="sarah" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='sarah'? 'selected':'' }}>Sarah</option>
													<option value="chico" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='chico'? 'selected':'' }}>Chico</option>
													<option value="milo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='milo'? 'selected':'' }}>Milo</option>
													<option value="julia" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='julia'? 'selected':'' }}>Julia</option>
													<option value="goliath" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='goliath'? 'selected':'' }}>Goliath</option>
													<option value="hera" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='hera'? 'selected':'' }}>Hera</option>
													<option value="winston" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='winston'? 'selected':'' }}>Winston</option>
													<option value="selena" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='selena'? 'selected':'' }}>Selena</option>
													<option value="terry" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='terry'? 'selected':'' }}>Terry</option>
													<option value="phoebe" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='phoebe'? 'selected':'' }}>Phoebe</option>
													<option value="apollo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='apollo'? 'selected':'' }}>Apollo</option>
													<option value="kira" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='kira'? 'selected':'' }}>Kira</option>
													<option value="steve" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='steve'? 'selected':'' }}>Steve</option>
													<option value="moses" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='moses'? 'selected':'' }}>Moses</option>
													<option value="jazz" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='jazz'? 'selected':'' }}>Jazz</option>
													<option value="ming" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='ming'? 'selected':'' }}>Ming</option>
													<option value="lexi" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='lexi'? 'selected':'' }}>Lexi</option>
													<option value="duke" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='duke'? 'selected':'' }}>Duke</option>
												</select>
											</div>
											<div class="form-group">
												<p class="text-muted m-b-15 m-t-15 font-15">Border</p>
												<input type="number" class="form-control" id="border" min="0" name="border" value="{{ !empty($settings['border'])? $settings['border']:'0' }}">
											</div>
											<div class="form-group">
												<p class="text-muted m-b-15 m-t-15 font-15">
													Backlight Color
												</p>
												<div data-color-format="rgba" data-color="{{ !empty($settings['backlightcolor'])? $settings['backlightcolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="backlightcolor" value="" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($settings['backlightcolor'])? $settings['backlightcolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group">
												<p class="text-muted m-b-15 m-t-15 font-15">
													Overlay Color
												</p>
												<div data-color-format="rgba" data-color="{{ !empty($settings['overlaycolor'])? $settings['overlaycolor']:'rgba(0,0,0,0)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="overlaycolor" value="" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($settings['overlaycolor'])? $settings['overlaycolor']:'rgba(0,0,0,0)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group">
												<p class="text-muted m-b-15 m-t-15 font-15">
													Text Color
												</p>
												<div data-color-format="rgba" data-color="{{ !empty($settings['textcolor'])? $settings['textcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="textcolor" readonly="readonly" value="" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($settings['textcolor'])? $settings['textcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group">
												<p class="text-muted m-b-15 m-t-15 font-15">
													Border Color
												</p>
												<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
													<input type="text" name="bordercolor" readonly="readonly" value="" class="form-control">
													<span class="input-group-btn add-on">
														<button class="btn btn-white" type="button">
															<i style="background-color:{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
														</button>
													</span>
												</div>
											</div>
											<div class="form-group">
												<p class="text-muted m-b-15 m-t-15 font-15">
													Border Radius
												</p>
												<input type="number" class="form-control" min="0" id="borderradius" name="borderradius" value="{{ !empty($settings['borderradius'])? $settings['borderradius']:'0' }}">
											</div>
											<div class="form-group">
												<p class="text-muted m-b-15 m-t-15 font-15">
													Items
												</p>
												<input type="number" class="form-control" min="1" id="items" name="items" value="{{ !empty($settings['items'])? $settings['items']:'1' }}">
											</div>
                      <div class="form-group">
														<h4 class="text-muted m-b-15 m-t-15 font-15">Items Margin (in px)</h4>
														<div class="row">
															<div class="col-sm-3">
																<input type="number" class="form-control" min="0" name="margin-left" value="{{ !empty($settings['margin-left'])? $settings['margin-left']:'0' }}" rel="txtTooltip" title="Left" data-placement="top">
															</div>
															<div class="col-sm-3">
																<input type="number" class="form-control" min="0" name="margin-right" value="{{ !empty($settings['margin-right'])? $settings['margin-right']:'0' }}" rel="txtTooltip" title="Right" data-placement="top">
															</div>
															<div class="col-sm-3">
																<input type="number" class="form-control" min="0" name="margin-top" value="{{ !empty($settings['margin-top'])? $settings['margin-top']:'0' }}" rel="txtTooltip" title="Top" data-placement="top">
															</div>
															<div class="col-sm-3">
																<input type="number" class="form-control" min="0" name="margin-bottom" value="{{ !empty($settings['margin-bottom'])? $settings['margin-bottom']:'0' }}" rel="txtTooltip" title="Bottom" data-placement="top">
															</div>
														</div>
														<div class="help"></div>
													</div>
      									<div class="form-group mr-auto">
      										<div class="checkbox">
      											<input id="lightbox" type="checkbox" name="lightbox" {{ !empty($settings['lightbox'])? 'checked':'' }}>
      											<label for="lightbox">
      												Show Image in Lightbox
      											</label>
      										</div>
      									</div>
      									<div class="form-group mr-auto">
      										<div class="checkbox">
      											<input id="overlayonhover" type="checkbox" name="overlayonhover" {{ !empty($settings['overlayonhover'])? 'checked':'' }}>
      											<label for="overlayonhover">
      												Show Overlay on hover
      											</label>
      										</div>
      									</div>
      									<div class="form-group mr-auto">
      										<div class="checkbox">
      											<input id="hidetitle" type="checkbox" name="hidetitle" {{ !empty($settings['hidetitle'])? 'checked':'' }}>
      											<label for="hidetitle">
      												Hide Title
      											</label>
      										</div>
      									</div>
      									<div class="form-group mr-auto">
      										<div class="checkbox">
      											<input id="hidedescription" type="checkbox" name="hidedescription" {{ !empty($settings['hidedescription'])? 'checked':'' }}>
      											<label for="hidedescription">
      												Hide Description
      											</label>
      										</div>
      									</div>
                        <div class="form-group mr-auto">
      										<div class="checkbox">
      											<input id="hidebeforetext" type="checkbox" name="hidebeforetext" {{ !empty($settings['hidebeforetext'])? 'checked':'' }}>
      											<label for="hidebeforetext">
      												Hide Before Text
      											</label>
      										</div>
      									</div>
                        <div class="form-group mr-auto">
      										<div class="checkbox">
      											<input id="hideaftertext" type="checkbox" name="hideaftertext" {{ !empty($settings['hideaftertext'])? 'checked':'' }}>
      											<label for="hideaftertext">
      												Hide After Text
      											</label>
      										</div>
      									</div>
      									<div class="form-group mr-auto">
      										<div class="checkbox">
      											<input id="showtitle" type="checkbox" name="showtitle" {{ !empty($settings['showtitle'])? 'checked':'' }}>
      											<label for="showtitle">
      												Show Title on Hover
      											</label>
      										</div>
      									</div>
      									<div class="form-group mr-auto">
      										<div class="checkbox">
      											<input id="showdescription" type="checkbox" name="showdescription" {{ !empty($settings['showdescription'])? 'checked':'' }}>
      											<label for="showdescription">
      												Show Description on Hover
      											</label>
      										</div>
      									</div>
										</div>
								</div>
								<div class="col-md-6">
									<h4 class="text-muted m-b-15 font-15">Scroller Setting</h4>
									<div class="m-t-15">
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Loop
											</p>
											<select data-style="btn-white" class="form-control selectpicker" id="loop" name="loop">
												<option value="false" {{ !empty($settings['loop']) && $settings['loop']=='false'? 'selected':'' }}>False</option>
												<option value="true" {{ !empty($settings['loop']) && $settings['loop']=='true'? 'selected':'' }}>True</option>
											</select>
										</div>
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Animation Speed
											</p>
											<input type="number" class="form-control" min="0" id="speed" name="speed" value="{{ !empty($settings['speed'])? $settings['speed']:'300' }}">
										</div>
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Show Arrows
											</p>
											<select data-style="btn-white" class="form-control selectpicker" id="nav" name="nav">
												<option value="false" {{ !empty($settings['nav']) && $settings['nav']=='false'? 'selected':'' }}>False</option>
												<option value="true" {{ !empty($settings['nav']) && $settings['nav']=='true'? 'selected':'' }}>True</option>
											</select>
										</div>
										@if(!empty($settings['nav']) && $settings['nav']=='true')
										<div class="d-md-flex no-gutters">
											<div class="form-group col-6" style="padding-right: 10px;">
												<p class="text-muted m-b-15 m-t-15 font-15">
													Prev Arrow html
												</p>
												<input type="text" class="form-control" id="arrow_style_prev" name="arrow_style_prev" value="{{ !empty($settings['arrow_style_prev'])? $settings['arrow_style_prev']:'<i class="fas fa-angle-left"></i>' }}">
											</div>
											<div class="form-group col-6">
												<p class="text-muted m-b-15 m-t-15 font-15">
													Next Arrow html
												</p>
												<input type="text" class="form-control" id="arrow_style_next" name="arrow_style_next" value="{{ !empty($settings['arrow_style_next'])? $settings['arrow_style_next']:'<i class="fas fa-angle-right"></i>' }}">
											</div>
										</div>
										@endif
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Autoplay
											</p>
											<select data-style="btn-white" class="form-control selectpicker" id="autoplay" name="autoplay">
												<option value="false" {{ !empty($settings['autoplay']) && $settings['autoplay']=='false'? 'selected':'' }}>False</option>
												<option value="true" {{ !empty($settings['autoplay']) && $settings['autoplay']=='true'? 'selected':'' }}>True</option>
											</select>
										</div>
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Show Dots
											</p>
											<select data-style="btn-white" class="form-control selectpicker" id="dots" name="dots">
												<option value="false" {{ !empty($settings['dots']) && $settings['dots']=='false'? 'selected':'' }}>False</option>
												<option value="true" {{ !empty($settings['dots']) && $settings['dots']=='true'? 'selected':'' }}>True</option>
											</select>
										</div>
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Adaptive Height
											</p>
											<select data-style="btn-white" class="form-control selectpicker" id="adaptiveHeight" name="adaptiveHeight">
												<option value="false" {{ !empty($settings['adaptiveHeight']) && $settings['adaptiveHeight']=='false'? 'selected':'' }}>False</option>
												<option value="true" {{ !empty($settings['adaptiveHeight']) && $settings['adaptiveHeight']=='true'? 'selected':'' }}>True</option>
											</select>
										</div>
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Autoplay Hover Pause
											</p>
											<select data-style="btn-white" class="form-control selectpicker" id="autoplayHoverPause" name="autoplayHoverPause">
												<option value="false" {{ !empty($settings['autoplayHoverPause']) && $settings['autoplayHoverPause']=='false'? 'selected':'' }}>False</option>
												<option value="true" {{ !empty($settings['autoplayHoverPause']) && $settings['autoplayHoverPause']=='true'? 'selected':'' }}>True</option>
											</select>
										</div>
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Autoplay Timeout
											</p>
											<input type="number" class="form-control" min="0" id="autoplayTimeout" name="autoplayTimeout" value="{{ !empty($settings['autoplayTimeout'])? $settings['autoplayTimeout']:'5000' }}">
										</div>

										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Center Mode
											</p>
											<select data-style="btn-white" class="form-control selectpicker" id="centerMode" name="centerMode">
												<option value="false" {{ !empty($settings['centerMode']) && $settings['centerMode']=='false'? 'selected':'' }}>False</option>
												<option value="true" {{ !empty($settings['centerMode']) && $settings['centerMode']=='true'? 'selected':'' }}>True</option>
											</select>
										</div>

										@if(!empty($settings['centerMode']) && $settings['centerMode']=='true')
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Center Padding
											</p>
											<input type="number" class="form-control" min="0" id="centerPadding" name="centerPadding" value="{{ !empty($settings['centerPadding'])? $settings['centerPadding']:'10' }}">
										</div>
										@endif

										@if(!empty($settings['centerMode']) && $settings['centerMode']=='true')
										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Initial Slide
											</p>
											<select data-style="btn-white" class="form-control selectpicker" id="initialSlide" name="initialSlide">
												@if(!empty($settings['title']) && sizeof($settings['title'])>0)
													@foreach($settings['title'] as $key=>$val)
														<option value="{{ $key }}" {{ !empty($settings['initialSlide']) && $settings['initialSlide']==$key? 'selected':'' }}>{{ $val }}</option>
													@endforeach
												@else
															<option value="0">Add Slides</option>
												@endif
											</select>
										</div>
										@endif

										<div class="form-group">
											<p class="text-muted m-b-15 m-t-15 font-15">
												Arrows Position
											</p>
											<select data-style="btn-white" class="form-control selectpicker" id="arrowposition" name="arrowposition">
												<option value="">Default</option>
												<option value="arrows_bottom_left" {{ !empty($settings['arrowposition']) && $settings['arrowposition']=='arrows_bottom_left'? 'selected':'' }}>Bottom Left</option>
												<option value="arrows_bottom_center" {{ !empty($settings['arrowposition']) && $settings['arrowposition']=='arrows_bottom_center'? 'selected':'' }}>Bottom Center</option>
												<option value="arrows_bottom_right" {{ !empty($settings['arrowposition']) && $settings['arrowposition']=='arrows_bottom_right'? 'selected':'' }}>Bottom Right</option>
												<option value="arrows_top_left" {{ !empty($settings['arrowposition']) && $settings['arrowposition']=='arrows_top_left'? 'selected':'' }}>Top Left</option>
												<option value="arrows_top_center" {{ !empty($settings['arrowposition']) && $settings['arrowposition']=='arrows_top_center'? 'selected':'' }}>Top Center</option>
												<option value="arrows_top_right" {{ !empty($settings['arrowposition']) && $settings['arrowposition']=='arrows_top_right'? 'selected':'' }}>Top Right</option>
												<option value="arrows_middle" {{ !empty($settings['arrowposition']) && $settings['arrowposition']=='arrows_middle'? 'selected':'' }}>Middle</option>
											</select>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
				<div class="card-box">
					<div class="row choosemedia" id="sortable">
						@if(!empty($settings['title']))
							@foreach($settings['title'] as $key=>$val)
              <div class="col-sm-12 imagelist">
                <div class="control" data-toggle="collapse" data-target="#list{{ $key }}">
                    <span class="handle"><i class="fas fa-arrows-alt"></i></span>
                    <a href="#" class="deletethumb"><i class="fa fa-remove"></i></a>
                </div>
                <div class="collapse show" id="list{{ $key }}">
                      <div class="imageholder">
                        <textarea class="form-control" placeholder="Before Text" name="beforetext[]">{!! !empty($settings['beforetext'][$key])?$settings['beforetext'][$key]:'' !!}</textarea>
                          <div class="addedimage">
                            <div class="image {!! !empty($settings['image'][$key])?'image-has':'' !!}" style="background-image:url('{!! !empty($settings['image'][$key])?$settings['image'][$key]:'' !!}')"><input type="hidden" name="image[]" value="{!! !empty($settings['image'][$key])?$settings['image'][$key]:'' !!}"><a href="#" class="changeimage"><i class="fa fa-camera"></i></a><a href="#" class="removeimage">Remove</a></div>
                          </div>
                          <textarea class="form-control" placeholder="After Text" name="aftertext[]">{!! !empty($settings['aftertext'][$key])?$settings['aftertext'][$key]:'' !!}</textarea>
                      </div>
                      <div class="form">
                          <input type="text" class="form-control" name="title[]" value="{!! !empty($settings['title'][$key])?$settings['title'][$key]:'' !!}" placeholder="Title">
                          <textarea class="form-control advtextarea" placeholder="Description" name="description[]">{!! !empty($settings['description'][$key])?$settings['description'][$key]:'' !!}</textarea>
                          <input type="text" class="form-control" name="link[]" value="{!! !empty($settings['link'][$key])?$settings['link'][$key]:'' !!}" placeholder="Link">
                          <select data-style="btn-white" class="form-control selectpicker" name="linktarget[]">
                            <option value="_self" {{ !empty($settings['linktarget'][$key]) && $settings['linktarget'][$key]=='_self'? 'selected':'' }}>None</option>
                            <option value="_blank" {{ !empty($settings['linktarget'][$key]) && $settings['linktarget'][$key]=='_blank'? 'selected':'' }}>New Tab</option>
                          </select>
                      </div>
                </div>
              </div>
							@endforeach
						@endif
						<div class="col-sm-12 addedimage adder"><div class="image"><a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-plus-square"></i></a></div></div>
					</div>
				</div>





			</div>

    </div>
</div>
</form>
@include('admin._partials.popupuploader')
<script src="{{ asset('assets/private/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
$(document).ready(function(){
	initTinymce();
	setInterval(function(){
		if($('.allmedia .imageholder.selected').length>0){
			if($('#media-select .md-custom-footer').hasClass('half')){
			}else{
				$('#media-select .md-custom-footer').addClass('half');
				$('#media-select .md-custom-footer .insert').addClass('active');
				$('#media-select .md-custom-footer').prepend('<button type="button" class="btn btn-default waves-effect insert" data-dismiss="modal">Insert Media</button>');
			}
		}else{
			$('#media-select .md-custom-footer').removeClass('half');
			$('#media-select .md-custom-footer .insert').remove();
			$('#media-select .md-custom-footer .insert').removeClass('active');
		}
	}, 50);

	$('.allmedia').on('click','.imageholder a',function(){
		if($(this).parents('#media-select').hasClass('changeimage')){
			$('.addedimage.selected').find('input[name="image[]"]').val($(this).attr('href'));
			$('.addedimage.selected').find('.image').css('background-image','url("'+$(this).attr('href')+'")');
			$(this).parents('#media-select').removeClass('changeimage');
      $('.addedimage.selected').find('.image').addClass('image-has');
			$('.addedimage').removeClass('selected');
			$('#media-select').removeClass('md-custom-show');
		}else{
			$(this).parents('.imageholder').toggleClass('selected');
		}

		return false;
	})
	$('.colorpicker-rgba').colorpicker();
	$('#clickpreview').click(function(){
		$.ajax({
            type: "GET",
            url: '{{URL::route('widget.preview')}}',
            data: $("#widgetform").serialize(),
            success: function( msg ) {
				$('#preview').html(msg);
            }
        });
	})
	$('#media-select').on('click','.insert', function(){
		var html ="";
		$('.allmedia .imageholder.selected a').each(function(){
			html = html+'<div class="col-sm-12 imagelist"><div class="control"><span class="handle"><i class="fas fa-arrows-alt"></i></span><a href="#" class="deletethumb"><i class="fa fa-remove"></i></a></div><div class="imageholder"><textarea class="form-control" placeholder="Before Text" name="beforetext[]"></textarea><div class="addedimage"><div class="image image-has" style="background-image:url(\''+$(this).attr('href')+'\')"><input type="hidden" name="image[]" value="'+$(this).attr('href')+'"><a href="#" class="changeimage"><i class="fa fa-camera"></i></a><a href="#" class="removeimage">Remove</a></div></div><textarea class="form-control" placeholder="After Text" name="aftertext[]"></textarea></div><div class="form"><input type="text" class="form-control" name="title[]" placeholder="Title"><textarea class="form-control advtextarea" placeholder="Description" name="description[]"></textarea><input type="text" class="form-control" name="link[]" value="" placeholder="Link"><select class="form-control selectpicker" data-style="btn-white" name="linktarget[]"><option value="_self">None</option><option value="_blank">New Tab</option></select></div></div>';
		})
		$('.choosemedia').prepend(html);
		$('.allmedia .imageholder').removeClass('selected');
		$('#media-select').removeClass('md-custom-show');
		initTinymce();
		$('.selectpicker').selectpicker();
	})
	$('.choosemedia').on('click','.deletethumb', function(){
    var r = confirm("Are you sure?");
    if (r == true) {
      $(this).parents('.imagelist').remove();
  		return false;
    } else {
      return false;
    }
	})
	$('.choosemedia').on('click','.changeimage', function(){
		$('.allmedia .imageholder').removeClass('selected');
		$('#media-select').addClass('md-custom-show');
		$(this).parents('.addedimage').addClass('selected');
		$('#media-select').addClass('changeimage');
		return false;
	})
	$('.removeimage').click(function(){
		$(this).parents('.image').css('background-image', 'none');
		$(this).parents('.image').removeClass('image-has');
		$(this).parents('.image').find('input[type="hidden"]').val('');
		return false;
	})
	$('#save').click(function(){
		$('#widgetform').submit();
	})
})
</script>
<script>
function initTinymce(){
tinymce.init({
		selector: ".advtextarea",
		theme: "modern",
		height:100,
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality template paste textcolor",
			"fontawesome noneditable"
		],
		content_css: 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
		valid_elements : '*[*]',
		noneditable_noneditable_class: 'fa',
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons fontawesome",
		extended_valid_elements: 'span[*]',
		style_formats: [
			{title: 'Bold text', inline: 'b'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		]
	});
}
</script>
@endsection
