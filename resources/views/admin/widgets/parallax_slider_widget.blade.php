@extends('layouts.app')

@section('extra')

<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />

<style>
.choosemedia .image{
	    border: 3px dashed;
    min-height: 150px;
    position: relative;
	background-size:cover;
}
.choosemedia .addedimage{
		margin-bottom:15px;
}
.choosemedia .form-control{
	margin-top:5px;
}
.choosemedia a{
	    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    text-align: center;
    z-index: 99;
}
.choosemedia a .fa{
	    position: absolute;
    font-size: 45px;
    color: #000000;
    top: 50%;
    transform: translateY(-50%) translateX(-50%);
	-webkit-transform: translateY(-50%) translateX(-50%);
    left: 50%;
}
#preview{
	background:url({{ asset('assets/public/images/preview.png') }}) no-repeat center;
	min-height:100px;
	border: 2px dashed #eaeaea;
	margin-bottom:10px;
}
.grid figure{
	float:none;
}
.imageholder.selected .thumbnail{
	border: 2px dashed #cccccc;
}
.btn.insert.active{
	display:inline-block;
}
.choosemedia .image a.delete{
	opacity:0;
	 -webkit-transition: all .2s ease-in-out;
  -moz-transition: all .2s ease-in-out;
  -o-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
}
.choosemedia .image:hover a{
	opacity:1;
}
</style>
<script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
</script>
@endsection

@section('content')
<form method="post" id="widgetform" action="{{ route('widget.save') }}">
{{csrf_field()}}
<input type="hidden" name="id" value="{{ $widget->id }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif

			<div class="col-sm-12 m-t-15">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right m-b-20">
							<button class="btn btn-primary waves-effect waves-light btn-lg" type="button" data-toggle="collapse" data-target="#settings" aria-expanded="false" aria-controls="Settings">
						     Setting
						  </button>
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
						</div>
				</div>
				<div class="card-box">
					<div class="row choosemedia" id="sortable">
						@if(!empty($settings['image']))
							@foreach($settings['image'] as $key=>$val)
								<div class="col-sm-3 addedimage">
								<div class="image" style="background-image:url('{{ $val }}')"><input type="hidden" name="image[]" value="{{ $val }}"><a href="#" class="changeimage"><i class="fa fa-camera"></i></a></div><input type="text" class="form-control" name="title[]" value="{!! !empty($settings['title'][$key])?$settings['title'][$key]:'' !!}" placeholder="Title"><textarea class="form-control" placeholder="Description" name="description[]">{!! !empty($settings['description'][$key])?$settings['description'][$key]:'' !!}</textarea><input type="text" class="form-control" name="link[]" value="{!! !empty($settings['link'][$key])?$settings['link'][$key]:'' !!}" placeholder="Link">
								<select class="form-control" name="linktarget[]">
									<option value="_self" {{ !empty($settings['linktarget'][$key]) && $settings['linktarget'][$key]=='_self'? 'selected':'' }}>None</option>
									<option value="_blank" {{ !empty($settings['linktarget'][$key]) && $settings['linktarget'][$key]=='_blank'? 'selected':'' }}>New Tab</option>
								</select>
								<select name="animation[]" class="form-control js--animations">
								  <option value="">Select Animation</option>
								  <optgroup label="Attention Seekers">
								    <option value="bounce" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounce'? 'selected':'' }}>bounce</option>
								    <option value="flash" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='flash'? 'selected':'' }}>flash</option>
								    <option value="pulse" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='pulse'? 'selected':'' }}>pulse</option>
								    <option value="rubberBand" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rubberBand'? 'selected':'' }}>rubberBand</option>
								    <option value="shake" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='shake'? 'selected':'' }}>shake</option>
								    <option value="swing" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='swing'? 'selected':'' }}>swing</option>
								    <option value="tada" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='tada'? 'selected':'' }}>tada</option>
								    <option value="wobble" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='wobble'? 'selected':'' }}>wobble</option>
								    <option value="jello" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='jello'? 'selected':'' }}>jello</option>
								  </optgroup>

								  <optgroup label="Bouncing Entrances">
								    <option value="bounceIn" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceIn'? 'selected':'' }}>bounceIn</option>
								    <option value="bounceInDown" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceInDown'? 'selected':'' }}>bounceInDown</option>
								    <option value="bounceInLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceInLeft'? 'selected':'' }}>bounceInLeft</option>
								    <option value="bounceInRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceInRight'? 'selected':'' }}>bounceInRight</option>
								    <option value="bounceInUp" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceInUp'? 'selected':'' }}>bounceInUp</option>
								  </optgroup>

								  <optgroup label="Bouncing Exits">
								    <option value="bounceOut" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceOut'? 'selected':'' }}>bounceOut</option>
								    <option value="bounceOutDown" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceOutDown'? 'selected':'' }}>bounceOutDown</option>
								    <option value="bounceOutLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceOutLeft'? 'selected':'' }}>bounceOutLeft</option>
								    <option value="bounceOutRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceOutRight'? 'selected':'' }}>bounceOutRight</option>
								    <option value="bounceOutUp" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='bounceOutUp'? 'selected':'' }}>bounceOutUp</option>
								  </optgroup>

								  <optgroup label="Fading Entrances">
								    <option value="fadeIn" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeIn'? 'selected':'' }}>fadeIn</option>
								    <option value="fadeInDown" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeInDown'? 'selected':'' }}>fadeInDown</option>
								    <option value="fadeInDownBig" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeInDownBig'? 'selected':'' }}>fadeInDownBig</option>
								    <option value="fadeInLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeInLeft'? 'selected':'' }}>fadeInLeft</option>
								    <option value="fadeInLeftBig" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeInLeftBig'? 'selected':'' }}>fadeInLeftBig</option>
								    <option value="fadeInRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeInRight'? 'selected':'' }}>fadeInRight</option>
								    <option value="fadeInRightBig" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeInRightBig'? 'selected':'' }}>fadeInRightBig</option>
								    <option value="fadeInUp" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeInUp'? 'selected':'' }}>fadeInUp</option>
								    <option value="fadeInUpBig" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeInUpBig'? 'selected':'' }}>fadeInUpBig</option>
								  </optgroup>

								  <optgroup label="Fading Exits">
								    <option value="fadeOut" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeOut'? 'selected':'' }}>fadeOut</option>
								    <option value="fadeOutDown" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeOutDown'? 'selected':'' }}>fadeOutDown</option>
								    <option value="fadeOutDownBig" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeOutDownBig'? 'selected':'' }}>fadeOutDownBig</option>
								    <option value="fadeOutLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeOutLeft'? 'selected':'' }}>fadeOutLeft</option>
								    <option value="fadeOutLeftBig" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeOutLeftBig'? 'selected':'' }}>fadeOutLeftBig</option>
								    <option value="fadeOutRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeOutRight'? 'selected':'' }}>fadeOutRight</option>
								    <option value="fadeOutRightBig" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeOutRightBig'? 'selected':'' }}>fadeOutRightBig
								    </option>
								    <option value="fadeOutUp" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeOutUp'? 'selected':'' }}>fadeOutUp</option>
								    <option value="fadeOutUpBig" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='fadeOutUpBig'? 'selected':'' }}>fadeOutUpBig</option>
								  </optgroup>

								  <optgroup label="Flippers">
								    <option value="flip" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='flip'? 'selected':'' }}>flip</option>
								    <option value="flipInX" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='flipInX'? 'selected':'' }}>flipInX</option>
								    <option value="flipInY" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='flipInY'? 'selected':'' }}>flipInY</option>
								    <option value="flipOutX" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='flipOutX'? 'selected':'' }}>flipOutX</option>
								    <option value="flipOutY" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='flipOutY'? 'selected':'' }}>flipOutY</option>
								  </optgroup>

								  <optgroup label="Lightspeed">
								    <option value="lightSpeedIn" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='lightSpeedIn'? 'selected':'' }}>lightSpeedIn</option>
								    <option value="lightSpeedOut" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='lightSpeedOut'? 'selected':'' }}>lightSpeedOut</option>
								  </optgroup>

								  <optgroup label="Rotating Entrances">
								    <option value="rotateIn" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateIn'? 'selected':'' }}>rotateIn</option>
								    <option value="rotateInDownLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateInDownLeft'? 'selected':'' }}>rotateInDownLeft
								    </option>
								    <option value="rotateInDownRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateInDownRight'? 'selected':'' }}>rotateInDownRight
								    </option>
								    <option value="rotateInUpLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateInUpLeft'? 'selected':'' }}>rotateInUpLeft</option>
								    <option value="rotateInUpRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateInUpRight'? 'selected':'' }}>rotateInUpRight
								    </option>
								  </optgroup>

								  <optgroup label="Rotating Exits">
								    <option value="rotateOut" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateOut'? 'selected':'' }}>rotateOut</option>
								    <option value="rotateOutDownLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateOutDownLeft'? 'selected':'' }}>rotateOutDownLeft
								    </option>
								    <option value="rotateOutDownRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateOutDownRight'? 'selected':'' }}>
								      rotateOutDownRight
								    </option>
								    <option value="rotateOutUpLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateOutUpLeft'? 'selected':'' }}>rotateOutUpLeft
								    </option>
								    <option value="rotateOutUpRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rotateOutUpRight'? 'selected':'' }}>rotateOutUpRight
								    </option>
								  </optgroup>

								  <optgroup label="Sliding Entrances">
								    <option value="slideInUp" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='slideInUp'? 'selected':'' }}>slideInUp</option>
								    <option value="slideInDown" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='slideInDown'? 'selected':'' }}>slideInDown</option>
								    <option value="slideInLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='slideInLeft'? 'selected':'' }}>slideInLeft</option>
								    <option value="slideInRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='slideInRight'? 'selected':'' }}>slideInRight</option>

								  </optgroup>
								  <optgroup label="Sliding Exits">
								    <option value="slideOutUp" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='slideOutUp'? 'selected':'' }}>slideOutUp</option>
								    <option value="slideOutDown" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='slideOutDown'? 'selected':'' }}>slideOutDown</option>
								    <option value="slideOutLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='slideOutLeft'? 'selected':'' }}>slideOutLeft</option>
								    <option value="slideOutRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='slideOutRight'? 'selected':'' }}>slideOutRight</option>

								  </optgroup>

								  <optgroup label="Zoom Entrances">
								    <option value="zoomIn" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomIn'? 'selected':'' }}>zoomIn</option>
								    <option value="zoomInDown" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomInDown'? 'selected':'' }}>zoomInDown</option>
								    <option value="zoomInLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomInLeft'? 'selected':'' }}>zoomInLeft</option>
								    <option value="zoomInRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomInRight'? 'selected':'' }}>zoomInRight</option>
								    <option value="zoomInUp" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomInUp'? 'selected':'' }}>zoomInUp</option>
								  </optgroup>

								  <optgroup label="Zoom Exits">
								    <option value="zoomOut" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomOut'? 'selected':'' }}>zoomOut</option>
								    <option value="zoomOutDown" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomOutDown'? 'selected':'' }}>zoomOutDown</option>
								    <option value="zoomOutLeft" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomOutLeft'? 'selected':'' }}>zoomOutLeft</option>
								    <option value="zoomOutRight" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomOutRight'? 'selected':'' }}>zoomOutRight</option>
								    <option value="zoomOutUp" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='zoomOutUp'? 'selected':'' }}>zoomOutUp</option>
								  </optgroup>

								  <optgroup label="Specials">
								    <option value="hinge" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='hinge'? 'selected':'' }}>hinge</option>
								    <option value="rollIn" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rollIn'? 'selected':'' }}>rollIn</option>
								    <option value="rollOut" {{ !empty($settings['animation'][$key]) && $settings['animation'][$key]=='rollOut'? 'selected':'' }}>rollOut</option>
								  </optgroup>
								</select>
								<button type="button" class="btn btn-primary btn-block waves-effect waves-light btn-sm deletethumb">Delete</button>
								</div>
							@endforeach
						@endif
						<div class="col-sm-3 addedimage"><div class="image"><a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-plus-square"></i></a></div></div>
					</div>
				</div>
				<h4 class="page-title">Items Setting</h4>
				<div class="card-box  m-t-15">

						<div class="form-group">
							<label for="hoverstyle">
								Hover Style
							</label>
							<select class="form-control" id="hoverstyle" name="hoverstyle">
								<option value="">None</option>
								<option value="lily" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='lily'? 'selected':'' }}>Lily</option>
								<option value="sadie" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='sadie'? 'selected':'' }}>Sadie</option>
								<option value="honey" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='honey'? 'selected':'' }}>Honey</option>
								<option value="layla" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='layla'? 'selected':'' }}>Layla</option>
								<option value="zoe" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='zoe'? 'selected':'' }}>Zoe</option>
								<option value="oscar" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='oscar'? 'selected':'' }}>Oscar</option>
								<option value="marley" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='marley'? 'selected':'' }}>Marley</option>
								<option value="ruby" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='ruby'? 'selected':'' }}>Ruby</option>
								<option value="roxy" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='roxy'? 'selected':'' }}>Roxy</option>
								<option value="bubba" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='bubba'? 'selected':'' }}>Bubba</option>
								<option value="romeo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='romeo'? 'selected':'' }}>Romeo</option>
								<option value="dexter" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='dexter'? 'selected':'' }}>Dexter</option>
								<option value="sarah" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='sarah'? 'selected':'' }}>Sarah</option>
								<option value="chico" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='chico'? 'selected':'' }}>Chico</option>
								<option value="milo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='milo'? 'selected':'' }}>Milo</option>
								<option value="julia" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='julia'? 'selected':'' }}>Julia</option>
								<option value="goliath" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='goliath'? 'selected':'' }}>Goliath</option>
								<option value="hera" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='hera'? 'selected':'' }}>Hera</option>
								<option value="winston" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='winston'? 'selected':'' }}>Winston</option>
								<option value="selena" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='selena'? 'selected':'' }}>Selena</option>
								<option value="terry" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='terry'? 'selected':'' }}>Terry</option>
								<option value="phoebe" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='phoebe'? 'selected':'' }}>Phoebe</option>
								<option value="apollo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='apollo'? 'selected':'' }}>Apollo</option>
								<option value="kira" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='kira'? 'selected':'' }}>Kira</option>
								<option value="steve" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='steve'? 'selected':'' }}>Steve</option>
								<option value="moses" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='moses'? 'selected':'' }}>Moses</option>
								<option value="jazz" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='jazz'? 'selected':'' }}>Jazz</option>
								<option value="ming" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='ming'? 'selected':'' }}>Ming</option>
								<option value="lexi" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='lexi'? 'selected':'' }}>Lexi</option>
								<option value="duke" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='duke'? 'selected':'' }}>Duke</option>
							</select>
						</div>
						<div class="form-group">
							<label for="border">
								Border
							</label>
							<input type="number" class="form-control" id="border" min="0" name="border" value="{{ !empty($settings['border'])? $settings['border']:'0' }}">
						</div>
						<div class="form-group">
							<label for="border">
								Overlay Color
							</label>
							<div data-color-format="rgba" data-color="{{ !empty($settings['overlaycolor'])? $settings['overlaycolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="overlaycolor" readonly="readonly" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['overlaycolor'])? $settings['overlaycolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="border">
								Text Color
							</label>
							<div data-color-format="rgba" data-color="{{ !empty($settings['textcolor'])? $settings['textcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="textcolor" readonly="readonly" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['textcolor'])? $settings['textcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="border">
								Border Color
							</label>
							<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="bordercolor" readonly="readonly" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="borderradius">
								Border Radius
							</label>
							<input type="number" class="form-control" min="0" id="borderradius" name="borderradius" value="{{ !empty($settings['borderradius'])? $settings['borderradius']:'0' }}">
						</div>
						<div class="form-group">
							<div class="checkbox">
								<input id="lightbox" type="checkbox" name="lightbox" {{ !empty($settings['lightbox'])? 'checked':'' }}>
								<label for="lightbox">
									Show Image in Lightbox
								</label>
							</div>
						</div>
					</div>



					<h4 class="page-title">Slider Setting</h4>

					<div class="card-box  m-t-15">
						<div class="form-group">
							<label for="loop">
								Loop
							</label>
							<select class="form-control" id="loop" name="loop">
								<option value="false" {{ !empty($settings['loop']) && $settings['loop']=='false'? 'selected':'' }}>False</option>
								<option value="true" {{ !empty($settings['loop']) && $settings['loop']=='true'? 'selected':'' }}>True</option>
							</select>
						</div>
						<div class="form-group">
							<label for="autoheight">
								Adaptive Height
							</label>
							<select class="form-control" id="autoheight" name="autoheight">
								<option value="false" {{ !empty($settings['autoheight']) && $settings['autoheight']=='false'? 'selected':'' }}>False</option>
								<option value="true" {{ !empty($settings['autoheight']) && $settings['autoheight']=='true'? 'selected':'' }}>True</option>
							</select>
						</div>
						<div class="form-group">
							<label for="loop">
								Show Arrows
							</label>
							<select class="form-control" id="nav" name="nav">
								<option value="false" {{ !empty($settings['nav']) && $settings['nav']=='false'? 'selected':'' }}>False</option>
								<option value="true" {{ !empty($settings['nav']) && $settings['nav']=='true'? 'selected':'' }}>True</option>
							</select>
						</div>
						<div class="form-group">
							<label for="loop">
								Autoplay
							</label>
							<select class="form-control" id="autoplay" name="autoplay">
								<option value="false" {{ !empty($settings['autoplay']) && $settings['autoplay']=='false'? 'selected':'' }}>False</option>
								<option value="true" {{ !empty($settings['autoplay']) && $settings['autoplay']=='true'? 'selected':'' }}>True</option>
							</select>
						</div>


						<div class="form-group">
							<label for="loop">
								Rewind
							</label>
							<select class="form-control" id="rewind" name="rewind">
								<option value="false" {{ !empty($settings['rewind']) && $settings['rewind']=='false'? 'selected':'' }}>False</option>
								<option value="true" {{ !empty($settings['rewind']) && $settings['rewind']=='true'? 'selected':'' }}>True</option>
							</select>
						</div>
						<div class="form-group">
							<label for="loop">
								Autoplay Hover Pause
							</label>
							<select class="form-control" id="autoplayHoverPause" name="autoplayHoverPause">
								<option value="false" {{ !empty($settings['autoplayHoverPause']) && $settings['autoplayHoverPause']=='false'? 'selected':'' }}>False</option>
								<option value="true" {{ !empty($settings['autoplayHoverPause']) && $settings['autoplayHoverPause']=='true'? 'selected':'' }}>True</option>
							</select>
						</div>
						<div class="form-group">
							<label for="smartspeed">
								Sliding Speed
							</label>
							<input type="number" class="form-control" min="0" id="smartspeed" name="smartspeed" value="{{ !empty($settings['smartspeed'])? $settings['smartspeed']:'300' }}">
						</div>
						<div class="form-group">
							<label for="autoplayTimeout">
								Autoplay Timeout
							</label>
							<input type="number" class="form-control" min="0" id="autoplayTimeout" name="autoplayTimeout" value="{{ !empty($settings['autoplayTimeout'])? $settings['autoplayTimeout']:'5000' }}">
						</div>


						<div class="form-group">
							<label for="loop">
								Show Dots
							</label>
							<select class="form-control" id="dots" name="dots">
								<option value="false" {{ !empty($settings['dots']) && $settings['dots']=='false'? 'selected':'' }}>False</option>
								<option value="true" {{ !empty($settings['dots']) && $settings['dots']=='true'? 'selected':'' }}>True</option>
							</select>
						</div>
						<div class="form-group">
							<label for="border">
								Arrows Style
							</label>
							<select class="form-control" id="arrowstyle" name="arrowstyle">
								<option value="">Default</option>
								<option value="arrows_bottom_left" {{ !empty($settings['arrowstyle']) && $settings['arrowstyle']=='arrows_bottom_left'? 'selected':'' }}>Bottom Left</option>
								<option value="arrows_bottom_center" {{ !empty($settings['arrowstyle']) && $settings['arrowstyle']=='arrows_bottom_center'? 'selected':'' }}>Bottom Center</option>
								<option value="arrows_bottom_right" {{ !empty($settings['arrowstyle']) && $settings['arrowstyle']=='arrows_bottom_right'? 'selected':'' }}>Bottom Right</option>
								<option value="arrows_top_left" {{ !empty($settings['arrowstyle']) && $settings['arrowstyle']=='arrows_top_left'? 'selected':'' }}>Top Left</option>
								<option value="arrows_top_center" {{ !empty($settings['arrowstyle']) && $settings['arrowstyle']=='arrows_top_center'? 'selected':'' }}>Top Center</option>
								<option value="arrows_top_right" {{ !empty($settings['arrowstyle']) && $settings['arrowstyle']=='arrows_top_right'? 'selected':'' }}>Top Right</option>
								<option value="arrows_middle" {{ !empty($settings['arrowstyle']) && $settings['arrowstyle']=='arrows_middle'? 'selected':'' }}>Middle</option>
							</select>
						</div>
						<div class="form-group">
							<label for="animatein">
								Animation In
							</label>
							<select name="animatein" class="form-control js--animations">
								<option value="">Default</option>
								<optgroup label="Attention Seekers">
									<option value="bounce" {{ !empty($settings['animatein']) && $settings['animatein']=='bounce'? 'selected':'' }}>bounce</option>
									<option value="flash" {{ !empty($settings['animatein']) && $settings['animatein']=='flash'? 'selected':'' }}>flash</option>
									<option value="pulse" {{ !empty($settings['animatein']) && $settings['animatein']=='pulse'? 'selected':'' }}>pulse</option>
									<option value="rubberBand" {{ !empty($settings['animatein']) && $settings['animatein']=='rubberBand'? 'selected':'' }}>rubberBand</option>
									<option value="shake" {{ !empty($settings['animatein']) && $settings['animatein']=='shake'? 'selected':'' }}>shake</option>
									<option value="swing" {{ !empty($settings['animatein']) && $settings['animatein']=='swing'? 'selected':'' }}>swing</option>
									<option value="tada" {{ !empty($settings['animatein']) && $settings['animatein']=='tada'? 'selected':'' }}>tada</option>
									<option value="wobble" {{ !empty($settings['animatein']) && $settings['animatein']=='wobble'? 'selected':'' }}>wobble</option>
									<option value="jello" {{ !empty($settings['animatein']) && $settings['animatein']=='jello'? 'selected':'' }}>jello</option>
								</optgroup>

								<optgroup label="Bouncing Entrances">
									<option value="bounceIn" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceIn'? 'selected':'' }}>bounceIn</option>
									<option value="bounceInDown" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceInDown'? 'selected':'' }}>bounceInDown</option>
									<option value="bounceInLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceInLeft'? 'selected':'' }}>bounceInLeft</option>
									<option value="bounceInRight" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceInRight'? 'selected':'' }}>bounceInRight</option>
									<option value="bounceInUp" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceInUp'? 'selected':'' }}>bounceInUp</option>
								</optgroup>

								<optgroup label="Bouncing Exits">
									<option value="bounceOut" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceOut'? 'selected':'' }}>bounceOut</option>
									<option value="bounceOutDown" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceOutDown'? 'selected':'' }}>bounceOutDown</option>
									<option value="bounceOutLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceOutLeft'? 'selected':'' }}>bounceOutLeft</option>
									<option value="bounceOutRight" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceOutRight'? 'selected':'' }}>bounceOutRight</option>
									<option value="bounceOutUp" {{ !empty($settings['animatein']) && $settings['animatein']=='bounceOutUp'? 'selected':'' }}>bounceOutUp</option>
								</optgroup>

								<optgroup label="Fading Entrances">
									<option value="fadeIn" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeIn'? 'selected':'' }}>fadeIn</option>
									<option value="fadeInDown" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeInDown'? 'selected':'' }}>fadeInDown</option>
									<option value="fadeInDownBig" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeInDownBig'? 'selected':'' }}>fadeInDownBig</option>
									<option value="fadeInLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeInLeft'? 'selected':'' }}>fadeInLeft</option>
									<option value="fadeInLeftBig" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeInLeftBig'? 'selected':'' }}>fadeInLeftBig</option>
									<option value="fadeInRight" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeInRight'? 'selected':'' }}>fadeInRight</option>
									<option value="fadeInRightBig" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeInRightBig'? 'selected':'' }}>fadeInRightBig</option>
									<option value="fadeInUp" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeInUp'? 'selected':'' }}>fadeInUp</option>
									<option value="fadeInUpBig" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeInUpBig'? 'selected':'' }}>fadeInUpBig</option>
								</optgroup>

								<optgroup label="Fading Exits">
									<option value="fadeOut" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeOut'? 'selected':'' }}>fadeOut</option>
									<option value="fadeOutDown" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeOutDown'? 'selected':'' }}>fadeOutDown</option>
									<option value="fadeOutDownBig" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeOutDownBig'? 'selected':'' }}>fadeOutDownBig</option>
									<option value="fadeOutLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeOutLeft'? 'selected':'' }}>fadeOutLeft</option>
									<option value="fadeOutLeftBig" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeOutLeftBig'? 'selected':'' }}>fadeOutLeftBig</option>
									<option value="fadeOutRight" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeOutRight'? 'selected':'' }}>fadeOutRight</option>
									<option value="fadeOutRightBig" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeOutRightBig'? 'selected':'' }}>fadeOutRightBig
									</option>
									<option value="fadeOutUp" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeOutUp'? 'selected':'' }}>fadeOutUp</option>
									<option value="fadeOutUpBig" {{ !empty($settings['animatein']) && $settings['animatein']=='fadeOutUpBig'? 'selected':'' }}>fadeOutUpBig</option>
								</optgroup>

								<optgroup label="Flippers">
									<option value="flip" {{ !empty($settings['animatein']) && $settings['animatein']=='flip'? 'selected':'' }}>flip</option>
									<option value="flipInX" {{ !empty($settings['animatein']) && $settings['animatein']=='flipInX'? 'selected':'' }}>flipInX</option>
									<option value="flipInY" {{ !empty($settings['animatein']) && $settings['animatein']=='flipInY'? 'selected':'' }}>flipInY</option>
									<option value="flipOutX" {{ !empty($settings['animatein']) && $settings['animatein']=='flipOutX'? 'selected':'' }}>flipOutX</option>
									<option value="flipOutY" {{ !empty($settings['animatein']) && $settings['animatein']=='flipOutY'? 'selected':'' }}>flipOutY</option>
								</optgroup>

								<optgroup label="Lightspeed">
									<option value="lightSpeedIn" {{ !empty($settings['animatein']) && $settings['animatein']=='lightSpeedIn'? 'selected':'' }}>lightSpeedIn</option>
									<option value="lightSpeedOut" {{ !empty($settings['animatein']) && $settings['animatein']=='lightSpeedOut'? 'selected':'' }}>lightSpeedOut</option>
								</optgroup>

								<optgroup label="Rotating Entrances">
									<option value="rotateIn" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateIn'? 'selected':'' }}>rotateIn</option>
									<option value="rotateInDownLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateInDownLeft'? 'selected':'' }}>rotateInDownLeft
									</option>
									<option value="rotateInDownRight" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateInDownRight'? 'selected':'' }}>rotateInDownRight
									</option>
									<option value="rotateInUpLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateInUpLeft'? 'selected':'' }}>rotateInUpLeft</option>
									<option value="rotateInUpRight" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateInUpRight'? 'selected':'' }}>rotateInUpRight
									</option>
								</optgroup>

								<optgroup label="Rotating Exits">
									<option value="rotateOut" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateOut'? 'selected':'' }}>rotateOut</option>
									<option value="rotateOutDownLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateOutDownLeft'? 'selected':'' }}>rotateOutDownLeft
									</option>
									<option value="rotateOutDownRight" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateOutDownRight'? 'selected':'' }}>
										rotateOutDownRight
									</option>
									<option value="rotateOutUpLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateOutUpLeft'? 'selected':'' }}>rotateOutUpLeft
									</option>
									<option value="rotateOutUpRight" {{ !empty($settings['animatein']) && $settings['animatein']=='rotateOutUpRight'? 'selected':'' }}>rotateOutUpRight
									</option>
								</optgroup>

								<optgroup label="Sliding Entrances">
									<option value="slideInUp" {{ !empty($settings['animatein']) && $settings['animatein']=='slideInUp'? 'selected':'' }}>slideInUp</option>
									<option value="slideInDown" {{ !empty($settings['animatein']) && $settings['animatein']=='slideInDown'? 'selected':'' }}>slideInDown</option>
									<option value="slideInLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='slideInLeft'? 'selected':'' }}>slideInLeft</option>
									<option value="slideInRight" {{ !empty($settings['animatein']) && $settings['animatein']=='slideInRight'? 'selected':'' }}>slideInRight</option>

								</optgroup>
								<optgroup label="Sliding Exits">
									<option value="slideOutUp" {{ !empty($settings['animatein']) && $settings['animatein']=='slideOutUp'? 'selected':'' }}>slideOutUp</option>
									<option value="slideOutDown" {{ !empty($settings['animatein']) && $settings['animatein']=='slideOutDown'? 'selected':'' }}>slideOutDown</option>
									<option value="slideOutLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='slideOutLeft'? 'selected':'' }}>slideOutLeft</option>
									<option value="slideOutRight" {{ !empty($settings['animatein']) && $settings['animatein']=='slideOutRight'? 'selected':'' }}>slideOutRight</option>

								</optgroup>

								<optgroup label="Zoom Entrances">
									<option value="zoomIn" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomIn'? 'selected':'' }}>zoomIn</option>
									<option value="zoomInDown" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomInDown'? 'selected':'' }}>zoomInDown</option>
									<option value="zoomInLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomInLeft'? 'selected':'' }}>zoomInLeft</option>
									<option value="zoomInRight" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomInRight'? 'selected':'' }}>zoomInRight</option>
									<option value="zoomInUp" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomInUp'? 'selected':'' }}>zoomInUp</option>
								</optgroup>

								<optgroup label="Zoom Exits">
									<option value="zoomOut" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomOut'? 'selected':'' }}>zoomOut</option>
									<option value="zoomOutDown" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomOutDown'? 'selected':'' }}>zoomOutDown</option>
									<option value="zoomOutLeft" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomOutLeft'? 'selected':'' }}>zoomOutLeft</option>
									<option value="zoomOutRight" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomOutRight'? 'selected':'' }}>zoomOutRight</option>
									<option value="zoomOutUp" {{ !empty($settings['animatein']) && $settings['animatein']=='zoomOutUp'? 'selected':'' }}>zoomOutUp</option>
								</optgroup>

								<optgroup label="Specials">
									<option value="hinge" {{ !empty($settings['animatein']) && $settings['animatein']=='hinge'? 'selected':'' }}>hinge</option>
									<option value="rollIn" {{ !empty($settings['animatein']) && $settings['animatein']=='rollIn'? 'selected':'' }}>rollIn</option>
									<option value="rollOut" {{ !empty($settings['animatein']) && $settings['animatein']=='rollOut'? 'selected':'' }}>rollOut</option>
								</optgroup>
							</select>
						</div>
						<div class="form-group">
							<label for="animateout">
								Animation Out
							</label>
							<select name="animateout" class="form-control js--animations">
								<option value="">Default</option>
								<optgroup label="Attention Seekers">
									<option value="bounce" {{ !empty($settings['animateout']) && $settings['animateout']=='bounce'? 'selected':'' }}>bounce</option>
									<option value="flash" {{ !empty($settings['animateout']) && $settings['animateout']=='flash'? 'selected':'' }}>flash</option>
									<option value="pulse" {{ !empty($settings['animateout']) && $settings['animateout']=='pulse'? 'selected':'' }}>pulse</option>
									<option value="rubberBand" {{ !empty($settings['animateout']) && $settings['animateout']=='rubberBand'? 'selected':'' }}>rubberBand</option>
									<option value="shake" {{ !empty($settings['animateout']) && $settings['animateout']=='shake'? 'selected':'' }}>shake</option>
									<option value="swing" {{ !empty($settings['animateout']) && $settings['animateout']=='swing'? 'selected':'' }}>swing</option>
									<option value="tada" {{ !empty($settings['animateout']) && $settings['animateout']=='tada'? 'selected':'' }}>tada</option>
									<option value="wobble" {{ !empty($settings['animateout']) && $settings['animateout']=='wobble'? 'selected':'' }}>wobble</option>
									<option value="jello" {{ !empty($settings['animateout']) && $settings['animateout']=='jello'? 'selected':'' }}>jello</option>
								</optgroup>

								<optgroup label="Bouncing Entrances">
									<option value="bounceIn" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceIn'? 'selected':'' }}>bounceIn</option>
									<option value="bounceInDown" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceInDown'? 'selected':'' }}>bounceInDown</option>
									<option value="bounceInLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceInLeft'? 'selected':'' }}>bounceInLeft</option>
									<option value="bounceInRight" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceInRight'? 'selected':'' }}>bounceInRight</option>
									<option value="bounceInUp" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceInUp'? 'selected':'' }}>bounceInUp</option>
								</optgroup>

								<optgroup label="Bouncing Exits">
									<option value="bounceOut" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceOut'? 'selected':'' }}>bounceOut</option>
									<option value="bounceOutDown" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceOutDown'? 'selected':'' }}>bounceOutDown</option>
									<option value="bounceOutLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceOutLeft'? 'selected':'' }}>bounceOutLeft</option>
									<option value="bounceOutRight" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceOutRight'? 'selected':'' }}>bounceOutRight</option>
									<option value="bounceOutUp" {{ !empty($settings['animateout']) && $settings['animateout']=='bounceOutUp'? 'selected':'' }}>bounceOutUp</option>
								</optgroup>

								<optgroup label="Fading Entrances">
									<option value="fadeIn" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeIn'? 'selected':'' }}>fadeIn</option>
									<option value="fadeInDown" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeInDown'? 'selected':'' }}>fadeInDown</option>
									<option value="fadeInDownBig" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeInDownBig'? 'selected':'' }}>fadeInDownBig</option>
									<option value="fadeInLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeInLeft'? 'selected':'' }}>fadeInLeft</option>
									<option value="fadeInLeftBig" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeInLeftBig'? 'selected':'' }}>fadeInLeftBig</option>
									<option value="fadeInRight" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeInRight'? 'selected':'' }}>fadeInRight</option>
									<option value="fadeInRightBig" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeInRightBig'? 'selected':'' }}>fadeInRightBig</option>
									<option value="fadeInUp" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeInUp'? 'selected':'' }}>fadeInUp</option>
									<option value="fadeInUpBig" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeInUpBig'? 'selected':'' }}>fadeInUpBig</option>
								</optgroup>

								<optgroup label="Fading Exits">
									<option value="fadeOut" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeOut'? 'selected':'' }}>fadeOut</option>
									<option value="fadeOutDown" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeOutDown'? 'selected':'' }}>fadeOutDown</option>
									<option value="fadeOutDownBig" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeOutDownBig'? 'selected':'' }}>fadeOutDownBig</option>
									<option value="fadeOutLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeOutLeft'? 'selected':'' }}>fadeOutLeft</option>
									<option value="fadeOutLeftBig" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeOutLeftBig'? 'selected':'' }}>fadeOutLeftBig</option>
									<option value="fadeOutRight" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeOutRight'? 'selected':'' }}>fadeOutRight</option>
									<option value="fadeOutRightBig" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeOutRightBig'? 'selected':'' }}>fadeOutRightBig
									</option>
									<option value="fadeOutUp" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeOutUp'? 'selected':'' }}>fadeOutUp</option>
									<option value="fadeOutUpBig" {{ !empty($settings['animateout']) && $settings['animateout']=='fadeOutUpBig'? 'selected':'' }}>fadeOutUpBig</option>
								</optgroup>

								<optgroup label="Flippers">
									<option value="flip" {{ !empty($settings['animateout']) && $settings['animateout']=='flip'? 'selected':'' }}>flip</option>
									<option value="flipInX" {{ !empty($settings['animateout']) && $settings['animateout']=='flipInX'? 'selected':'' }}>flipInX</option>
									<option value="flipInY" {{ !empty($settings['animateout']) && $settings['animateout']=='flipInY'? 'selected':'' }}>flipInY</option>
									<option value="flipOutX" {{ !empty($settings['animateout']) && $settings['animateout']=='flipOutX'? 'selected':'' }}>flipOutX</option>
									<option value="flipOutY" {{ !empty($settings['animateout']) && $settings['animateout']=='flipOutY'? 'selected':'' }}>flipOutY</option>
								</optgroup>

								<optgroup label="Lightspeed">
									<option value="lightSpeedIn" {{ !empty($settings['animateout']) && $settings['animateout']=='lightSpeedIn'? 'selected':'' }}>lightSpeedIn</option>
									<option value="lightSpeedOut" {{ !empty($settings['animateout']) && $settings['animateout']=='lightSpeedOut'? 'selected':'' }}>lightSpeedOut</option>
								</optgroup>

								<optgroup label="Rotating Entrances">
									<option value="rotateIn" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateIn'? 'selected':'' }}>rotateIn</option>
									<option value="rotateInDownLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateInDownLeft'? 'selected':'' }}>rotateInDownLeft
									</option>
									<option value="rotateInDownRight" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateInDownRight'? 'selected':'' }}>rotateInDownRight
									</option>
									<option value="rotateInUpLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateInUpLeft'? 'selected':'' }}>rotateInUpLeft</option>
									<option value="rotateInUpRight" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateInUpRight'? 'selected':'' }}>rotateInUpRight
									</option>
								</optgroup>

								<optgroup label="Rotating Exits">
									<option value="rotateOut" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateOut'? 'selected':'' }}>rotateOut</option>
									<option value="rotateOutDownLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateOutDownLeft'? 'selected':'' }}>rotateOutDownLeft
									</option>
									<option value="rotateOutDownRight" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateOutDownRight'? 'selected':'' }}>
										rotateOutDownRight
									</option>
									<option value="rotateOutUpLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateOutUpLeft'? 'selected':'' }}>rotateOutUpLeft
									</option>
									<option value="rotateOutUpRight" {{ !empty($settings['animateout']) && $settings['animateout']=='rotateOutUpRight'? 'selected':'' }}>rotateOutUpRight
									</option>
								</optgroup>

								<optgroup label="Sliding Entrances">
									<option value="slideInUp" {{ !empty($settings['animateout']) && $settings['animateout']=='slideInUp'? 'selected':'' }}>slideInUp</option>
									<option value="slideInDown" {{ !empty($settings['animateout']) && $settings['animateout']=='slideInDown'? 'selected':'' }}>slideInDown</option>
									<option value="slideInLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='slideInLeft'? 'selected':'' }}>slideInLeft</option>
									<option value="slideInRight" {{ !empty($settings['animateout']) && $settings['animateout']=='slideInRight'? 'selected':'' }}>slideInRight</option>

								</optgroup>
								<optgroup label="Sliding Exits">
									<option value="slideOutUp" {{ !empty($settings['animateout']) && $settings['animateout']=='slideOutUp'? 'selected':'' }}>slideOutUp</option>
									<option value="slideOutDown" {{ !empty($settings['animateout']) && $settings['animateout']=='slideOutDown'? 'selected':'' }}>slideOutDown</option>
									<option value="slideOutLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='slideOutLeft'? 'selected':'' }}>slideOutLeft</option>
									<option value="slideOutRight" {{ !empty($settings['animateout']) && $settings['animateout']=='slideOutRight'? 'selected':'' }}>slideOutRight</option>

								</optgroup>

								<optgroup label="Zoom Entrances">
									<option value="zoomIn" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomIn'? 'selected':'' }}>zoomIn</option>
									<option value="zoomInDown" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomInDown'? 'selected':'' }}>zoomInDown</option>
									<option value="zoomInLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomInLeft'? 'selected':'' }}>zoomInLeft</option>
									<option value="zoomInRight" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomInRight'? 'selected':'' }}>zoomInRight</option>
									<option value="zoomInUp" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomInUp'? 'selected':'' }}>zoomInUp</option>
								</optgroup>

								<optgroup label="Zoom Exits">
									<option value="zoomOut" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomOut'? 'selected':'' }}>zoomOut</option>
									<option value="zoomOutDown" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomOutDown'? 'selected':'' }}>zoomOutDown</option>
									<option value="zoomOutLeft" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomOutLeft'? 'selected':'' }}>zoomOutLeft</option>
									<option value="zoomOutRight" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomOutRight'? 'selected':'' }}>zoomOutRight</option>
									<option value="zoomOutUp" {{ !empty($settings['animateout']) && $settings['animateout']=='zoomOutUp'? 'selected':'' }}>zoomOutUp</option>
								</optgroup>

								<optgroup label="Specials">
									<option value="hinge" {{ !empty($settings['animateout']) && $settings['animateout']=='hinge'? 'selected':'' }}>hinge</option>
									<option value="rollIn" {{ !empty($settings['animateout']) && $settings['animateout']=='rollIn'? 'selected':'' }}>rollIn</option>
									<option value="rollOut" {{ !empty($settings['animateout']) && $settings['animateout']=='rollOut'? 'selected':'' }}>rollOut</option>
								</optgroup>
							</select>
						</div>
					</div>
			</div>


    </div>
</div>
</form>
@include('admin._partials.popupuploader')
<script>
$(document).ready(function(){
	setInterval(function(){
		if($('.allmedia .imageholder.selected').length>0){
			if($('#media-select .md-custom-footer').hasClass('half')){
			}else{
				$('#media-select .md-custom-footer').addClass('half');
				$('#media-select .md-custom-footer .insert').addClass('active');
				$('#media-select .md-custom-footer').prepend('<button type="button" class="btn btn-default waves-effect insert" data-dismiss="modal">Insert Media</button>');
			}
		}else{
			$('#media-select .md-custom-footer').removeClass('half');
			$('#media-select .md-custom-footer .insert').remove();
			$('#media-select .md-custom-footer .insert').removeClass('active');
		}
	}, 50);
	$('#media-select .md-custom-footer').addClass('half');
	$('#media-select .md-custom-footer').prepend('<button type="button" class="btn btn-default waves-effect insert" data-dismiss="modal">Insert Media</button>');
	$('.allmedia').on('click','.imageholder a',function(){
		if($(this).parents('#media-select').hasClass('changeimage')){
			$('.addedimage.selected').find('input[name="image[]"]').val($(this).attr('href'));
			$('.addedimage.selected').find('.image').css('background-image','url("'+$(this).attr('href')+'")');
			$(this).parents('#media-select').removeClass('changeimage');
			$('.addedimage').removeClass('selected');
			$('#media-select').removeClass('md-custom-show');
		}else{
			$(this).parents('.imageholder').toggleClass('selected');
		}
		return false;
	})
	$('.colorpicker-rgba').colorpicker();
	$('#clickpreview').click(function(){
		$.ajax({
            type: "GET",
            url: '{{URL::route('widget.preview')}}',
            data: $("#widgetform").serialize(),
            success: function( msg ) {
				$('#preview').html(msg);
            }
        });
	})
	$('#media-select').on('click','.insert', function(){
		var html ="";
		$('.allmedia .imageholder.selected a').each(function(){
			html = html+'<div class="col-sm-3 addedimage"><div class="image" style="background-image:url(\''+$(this).attr('href')+'\')"><input type="hidden" name="image[]" value="'+$(this).attr('href')+'"><a href="#" class="changeimage"><i class="fa fa-camera"></i></a></div><input type="text" class="form-control" name="title[]" placeholder="Title"><textarea class="form-control" placeholder="Description" name="description[]"></textarea><input type="text" class="form-control" placeholder="Link" name="link[]" value=""><select class="form-control" name="linktarget[]"><option value="_self">None</option><option value="_blank">New Tab</option></select><select name="animation[]" class="form-control js--animations"><option value="">Select Animation</option><optgroup label="Attention Seekers"><option value="bounce">bounce</option><option value="flash">flash</option><option value="pulse">pulse</option><option value="rubberBand">rubberBand</option><option value="shake">shake</option><option value="swing">swing</option><option value="tada">tada</option><option value="wobble">wobble</option><option value="jello">jello</option></optgroup><optgroup label="Bouncing Entrances"><option value="bounceIn">bounceIn</option><option value="bounceInDown">bounceInDown</option><option value="bounceInLeft">bounceInLeft</option><option value="bounceInRight">bounceInRight</option><option value="bounceInUp">bounceInUp</option></optgroup><optgroup label="Bouncing Exits"><option value="bounceOut">bounceOut</option><option value="bounceOutDown">bounceOutDown</option><option value="bounceOutLeft">bounceOutLeft</option><option value="bounceOutRight">bounceOutRight</option><option value="bounceOutUp">bounceOutUp</option></optgroup><optgroup label="Fading Entrances"><option value="fadeIn">fadeIn</option><option value="fadeInDown">fadeInDown</option><option value="fadeInDownBig">fadeInDownBig</option><option value="fadeInLeft">fadeInLeft</option><option value="fadeInLeftBig">fadeInLeftBig</option><option value="fadeInRight">fadeInRight</option><option value="fadeInRightBig">fadeInRightBig</option><option value="fadeInUp">fadeInUp</option><option value="fadeInUpBig">fadeInUpBig</option></optgroup><optgroup label="Fading Exits"><option value="fadeOut">fadeOut</option><option value="fadeOutDown">fadeOutDown</option><option value="fadeOutDownBig">fadeOutDownBig</option><option value="fadeOutLeft">fadeOutLeft</option><option value="fadeOutLeftBig">fadeOutLeftBig</option><option value="fadeOutRight">fadeOutRight</option><option value="fadeOutRightBig">fadeOutRightBig</option><option value="fadeOutUp">fadeOutUp</option><option value="fadeOutUpBig">fadeOutUpBig</option></optgroup><optgroup label="Flippers"><option value="flip">flip</option><option value="flipInX">flipInX</option><option value="flipInY">flipInY</option><option value="flipOutX">flipOutX</option><option value="flipOutY">flipOutY</option></optgroup><optgroup label="Lightspeed"><option value="lightSpeedIn">lightSpeedIn</option><option value="lightSpeedOut">lightSpeedOut</option></optgroup><optgroup label="Rotating Entrances"><option value="rotateIn">rotateIn</option><option value="rotateInDownLeft">rotateInDownLeft</option><option value="rotateInDownRight">rotateInDownRight</option><option value="rotateInUpLeft">rotateInUpLeft</option><option value="rotateInUpRight">rotateInUpRight</option></optgroup><optgroup label="Rotating Exits"><option value="rotateOut">rotateOut</option><option value="rotateOutDownLeft">rotateOutDownLeft</option><option value="rotateOutDownRight">rotateOutDownRight</option><option value="rotateOutUpLeft">rotateOutUpLeft</option><option value="rotateOutUpRight">rotateOutUpRight</option></optgroup><optgroup label="Sliding Entrances"><option value="slideInUp">slideInUp</option><option value="slideInDown">slideInDown</option><option value="slideInLeft">slideInLeft</option><option value="slideInRight">slideInRight</option></optgroup><optgroup label="Sliding Exits"><option value="slideOutUp">slideOutUp</option><option value="slideOutDown">slideOutDown</option><option value="slideOutLeft">slideOutLeft</option><option value="slideOutRight">slideOutRight</option></optgroup><optgroup label="Zoom Entrances"><option value="zoomIn">zoomIn</option><option value="zoomInDown">zoomInDown</option><option value="zoomInLeft">zoomInLeft</option><option value="zoomInRight">zoomInRight</option><option value="zoomInUp">zoomInUp</option></optgroup><optgroup label="Zoom Exits"><option value="zoomOut">zoomOut</option><option value="zoomOutDown">zoomOutDown</option><option value="zoomOutLeft">zoomOutLeft</option><option value="zoomOutRight">zoomOutRight</option><option value="zoomOutUp">zoomOutUp</option></optgroup><optgroup label="Specials"><option value="hinge">hinge</option><option value="rollIn">rollIn</option><option value="rollOut">rollOut</option></optgroup></select><button type="button" class="btn btn-primary btn-block waves-effect waves-light btn-sm deletethumb">Delete</button></div>';
		})
		$('.choosemedia').prepend(html);
		$('.allmedia .imageholder').removeClass('selected');
		$('#media-select').removeClass('md-custom-show');
	})
	$('.choosemedia').on('click','.deletethumb', function(){
		$(this).parents('.addedimage').remove();
		return false;
	})
	$('.choosemedia').on('click','.changeimage', function(){
		$('.allmedia .imageholder').removeClass('selected');
		$('#media-select').addClass('md-custom-show');
		$(this).parents('.addedimage').addClass('selected');
		$('#media-select').addClass('changeimage');
		return false;
	})
	$('#save').click(function(){
		$('#widgetform').submit();
	})
})
</script>
@endsection
