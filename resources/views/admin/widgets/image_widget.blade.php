@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />


<style>
.choosemedia{

    position: relative;
}
.choosemedia.has-image{
	display:inline-block;
}
.choosemedia .image{
	position: relative;
	top: 0;
	width: auto;
	max-width: 100%;
}
.choosemedia a{
	    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    text-align: center;
    z-index: 99;
}
.choosemedia a .fa{
	    position: absolute;
    font-size: 45px;
    color: #000000;
    top: 50%;
    transform: translateY(-50%) translateX(-50%);
	-webkit-transform: translateY(-50%) translateX(-50%);
    left: 50%;
}
img{
	max-width: 100%;
}
#preview{
	background:url({{ asset('assets/private/images/preview.png') }}) no-repeat center;
	min-height:100px;
	border: 2px dashed #eaeaea;
	margin-bottom:10px;
}
.grid figure{
	float:none;
}
</style>
@endsection

@section('content')
<form method="post" id="widgetform" action="{{ route('widget.save') }}">
	{{csrf_field()}}
	<input type="hidden" name="id" value="{{ $widget->id }}">
	<input type="hidden" name="image" value="{{ !empty($settings['image']) ? $settings['image'] : '' }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif


			<div class="col-sm-8 m-t-15">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right m-b-20">
							<button class="btn btn-primary waves-effect waves-light btn-lg" type="button" data-toggle="collapse" data-target="#settings" aria-expanded="false" aria-controls="Settings">
								 Settings
							</button>
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
						</div>
				</div>
				<div class="collapse" id="settings">
						<div class="card-box">
							<div class="form-group">
								<div class="checkbox">
									<input id="hidetitle" type="checkbox" name="hidetitle" {{ !empty($settings['hidetitle'])? 'checked':'' }}>
									<label for="hidetitle">
										Hide Title
									</label>
								</div>
							</div>
							<div class="form-group">
								<div class="checkbox">
									<input id="hidedescription" type="checkbox" name="hidedescription" {{ !empty($settings['hidedescription'])? 'checked':'' }}>
									<label for="hidedescription">
										Hide Description
									</label>
								</div>
							</div>
						<div class="form-group">
							<div class="checkbox">
								<input id="showtitle" type="checkbox" name="showtitle" {{ !empty($settings['showtitle'])? 'checked':'' }}>
								<label for="showtitle">
									Show Title on Hover
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="checkbox">
								<input id="showdescription" type="checkbox" name="showdescription" {{ !empty($settings['showdescription'])? 'checked':'' }}>
								<label for="showdescription">
									Show Description on Hover
								</label>
							</div>
						</div>
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">
								Hover Style
							</h4>
							<select class="form-control selectpicker" data-style="btn-white" id="hoverstyle" name="hoverstyle">
								<option value="">None</option>
								<option value="lily" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='lily'? 'selected':'' }}>Lily</option>
								<option value="sadie" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='sadie'? 'selected':'' }}>Sadie</option>
								<option value="honey" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='honey'? 'selected':'' }}>Honey</option>
								<option value="layla" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='layla'? 'selected':'' }}>Layla</option>
								<option value="zoe" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='zoe'? 'selected':'' }}>Zoe</option>
								<option value="oscar" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='oscar'? 'selected':'' }}>Oscar</option>
								<option value="marley" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='marley'? 'selected':'' }}>Marley</option>
								<option value="ruby" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='ruby'? 'selected':'' }}>Ruby</option>
								<option value="roxy" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='roxy'? 'selected':'' }}>Roxy</option>
								<option value="bubba" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='bubba'? 'selected':'' }}>Bubba</option>
								<option value="romeo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='romeo'? 'selected':'' }}>Romeo</option>
								<option value="dexter" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='dexter'? 'selected':'' }}>Dexter</option>
								<option value="sarah" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='sarah'? 'selected':'' }}>Sarah</option>
								<option value="chico" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='chico'? 'selected':'' }}>Chico</option>
								<option value="milo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='milo'? 'selected':'' }}>Milo</option>
								<option value="julia" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='julia'? 'selected':'' }}>Julia</option>
								<option value="goliath" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='goliath'? 'selected':'' }}>Goliath</option>
								<option value="hera" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='hera'? 'selected':'' }}>Hera</option>
								<option value="winston" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='winston'? 'selected':'' }}>Winston</option>
								<option value="selena" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='selena'? 'selected':'' }}>Selena</option>
								<option value="terry" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='terry'? 'selected':'' }}>Terry</option>
								<option value="phoebe" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='phoebe'? 'selected':'' }}>Phoebe</option>
								<option value="apollo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='apollo'? 'selected':'' }}>Apollo</option>
								<option value="kira" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='kira'? 'selected':'' }}>Kira</option>
								<option value="steve" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='steve'? 'selected':'' }}>Steve</option>
								<option value="moses" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='moses'? 'selected':'' }}>Moses</option>
								<option value="jazz" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='jazz'? 'selected':'' }}>Jazz</option>
								<option value="ming" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='ming'? 'selected':'' }}>Ming</option>
								<option value="lexi" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='lexi'? 'selected':'' }}>Lexi</option>
								<option value="duke" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='duke'? 'selected':'' }}>Duke</option>
							</select>
						</div>
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">
								Border
							</h4>
							<input type="number" class="form-control" id="border" min="0" name="border" value="{{ !empty($settings['border'])? $settings['border']:'0' }}">
						</div>
						<div class="form-group">
								<h4 class="text-muted m-b-15 m-t-15 font-15">
									Backlight Color
								</h4>
								<div data-color-format="rgba" data-color="{{ !empty($settings['backlightcolor'])? $settings['backlightcolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
									<input type="text" name="backlightcolor" value="" class="form-control">
									<span class="input-group-btn add-on">
										<button class="btn btn-white" type="button">
											<i style="background-color:{{ !empty($settings['backlightcolor'])? $settings['backlightcolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="form-group">
								<h4 class="text-muted m-b-15 m-t-15 font-15">
									Overlay Color
								</h4>
								<div data-color-format="rgba" data-color="{{ !empty($settings['overlaycolor'])? $settings['overlaycolor']:'rgba(0,0,0,0)' }}" class="colorpicker-rgba input-group">
									<input type="text" name="overlaycolor" value="" class="form-control">
									<span class="input-group-btn add-on">
										<button class="btn btn-white" type="button">
											<i style="background-color:{{ !empty($settings['overlaycolor'])? $settings['overlaycolor']:'rgba(0,0,0,0)' }};margin-top: 2px;"></i>
										</button>
									</span>
								</div>
							</div>
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">
								Text Color
							</h4>
							<div data-color-format="rgba" data-color="{{ !empty($settings['textcolor'])? $settings['textcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="textcolor" readonly="readonly" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['textcolor'])? $settings['textcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">
								Border Color
							</h4>
							<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="bordercolor" readonly="readonly" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">
								Border Radius
							</h4>
							<input type="number" class="form-control" min="0" id="borderradius" name="borderradius" value="{{ !empty($settings['borderradius'])? $settings['borderradius']:'0' }}">
						</div>
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">
								Link
							</h4>
							<input type="text" class="form-control" id="link" name="link" value="{{ !empty($settings['link'])? $settings['link']:'' }}">
						</div>
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">
								Link Target
							</h4>
							<select class="form-control selectpicker" data-style="btn-white" id="linktarget" name="linktarget">
								<option value="_self" {{ !empty($settings['linktarget']) && $settings['linktarget']=='_self'? 'selected':'' }}>None</option>
								<option value="_blank" {{ !empty($settings['linktarget']) && $settings['linktarget']=='_blank'? 'selected':'' }}>New Tab</option>
							</select>
						</div>
						<div class="form-group">
								<div class="checkbox">
									<input id="overlayonhover" type="checkbox" name="overlayonhover" {{ !empty($settings['overlayonhover'])? 'checked':'' }}>
									<label for="overlayonhover">
										Show Overlay on hover
									</label>
								</div>
							</div>
						</div>
				</div>
				<div class="card-box">
					<div class="choosemedia {{ !empty($settings['image']) ? 'has-image' : '' }}">
						<div class="image">{!! !empty($settings['image']) ? '<img src="'.$settings['image'].'">' : '' !!}</div>
						<a href="javascript:void(0)" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"></i></a>
					</div>
				</div>
			</div>
			<div class="col-sm-4 m-t-15">
				<div class="card-box">
						<div id="preview">

						</div>
						<p class="text-center"><a href="javascript:void(0)" id="clickpreview" class="btn btn-primary btn-sm">Click to Preview</a></p>
				</div>
			</div>
    </div>
</div>


	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-8 m-t-15">
				<div class="card-box">
					<div class="form-group">
						<h4 class="text-muted m-b-15 m-t-15 font-15">Title:</h4>
						<input type="text" class="form-control" id="title" name="title" value="{{ !empty($settings['title'])?$settings['title']:'' }}">
					 </div>
					<div class="form-group">
						<h4 class="text-muted m-b-15 m-t-15 font-15">Description:</h4>
						<textarea type="text" class="form-control" name="description" id="description">{{ !empty($settings['description']) ? $settings['description'] : '' }}</textarea>
					 </div>
				</div>

				</div>
			</div>
		</div>
	</div>
</form>


@include('admin._partials.popupuploader')

<script>
$(document).ready(function(){
	$('.allmedia').on('click','.imageholder a',function(){
		$('.choosemedia').removeClass('has-image');
		$('input[name="image"]').val($(this).attr('href'));
		$('.choosemedia').addClass('has-image');
		$('.choosemedia .image').html('<img src="'+$(this).attr('href')+'" class="img-responsive">');
		$('#media-select').removeClass('md-custom-show');
		return false;
	})
	$('.colorpicker-rgba').colorpicker();
	$('#clickpreview').click(function(){
		$.ajax({
            type: "POST",
            url: '{{URL::route('widget.preview')}}',
            data: $("#widgetform").serialize(),
            success: function( msg ) {
				$('#preview').html(msg);
            }
        });
	})
	$('#save').click(function(){
		$('#widgetform').submit();
	})
})
</script>
<script src="{{ asset('assets/private/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
$(document).ready(function(){
tinymce.init({
		selector: "textarea",
		theme: "modern",
		height:300,
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality template paste textcolor",
			"fontawesome noneditable"
		],
		content_css: 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
		valid_elements : '*[*]',
		noneditable_noneditable_class: 'fa',
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons fontawesome",
		extended_valid_elements: 'span[*]',
		style_formats: [
			{title: 'Bold text', inline: 'b'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		]
	});
})
</script>
@endsection
