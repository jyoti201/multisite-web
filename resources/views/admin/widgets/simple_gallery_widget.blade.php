@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />


@endsection

@section('content')
<form method="post" id="widgetform" action="{{ route('widget.save') }}">
{{csrf_field()}}
<input type="hidden" name="id" value="{{ $widget->id }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif

			<div class="col-sm-12 m-t-15">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right m-b-20">
						<button class="btn btn-primary waves-effect waves-light btn-lg" type="button" data-toggle="collapse" data-target="#settings" aria-expanded="false" aria-controls="Settings">
						     Setting
						  </button>
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
						</div>
				</div>
				<div class="collapse" id="settings">
					<div class="card-box">
						<div class="form-group">
							<label for="hoverstyle">
								Hover Style
							</label>
							<select  data-style="btn-white" class="form-control selectpicker" id="hoverstyle" name="hoverstyle">
								<option value="">None</option>
								<option value="lily" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='lily'? 'selected':'' }}>Lily</option>
								<option value="sadie" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='sadie'? 'selected':'' }}>Sadie</option>
								<option value="honey" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='honey'? 'selected':'' }}>Honey</option>
								<option value="layla" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='layla'? 'selected':'' }}>Layla</option>
								<option value="zoe" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='zoe'? 'selected':'' }}>Zoe</option>
								<option value="oscar" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='oscar'? 'selected':'' }}>Oscar</option>
								<option value="marley" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='marley'? 'selected':'' }}>Marley</option>
								<option value="ruby" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='ruby'? 'selected':'' }}>Ruby</option>
								<option value="roxy" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='roxy'? 'selected':'' }}>Roxy</option>
								<option value="bubba" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='bubba'? 'selected':'' }}>Bubba</option>
								<option value="romeo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='romeo'? 'selected':'' }}>Romeo</option>
								<option value="dexter" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='dexter'? 'selected':'' }}>Dexter</option>
								<option value="sarah" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='sarah'? 'selected':'' }}>Sarah</option>
								<option value="chico" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='chico'? 'selected':'' }}>Chico</option>
								<option value="milo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='milo'? 'selected':'' }}>Milo</option>
								<option value="julia" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='julia'? 'selected':'' }}>Julia</option>
								<option value="goliath" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='goliath'? 'selected':'' }}>Goliath</option>
								<option value="hera" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='hera'? 'selected':'' }}>Hera</option>
								<option value="winston" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='winston'? 'selected':'' }}>Winston</option>
								<option value="selena" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='selena'? 'selected':'' }}>Selena</option>
								<option value="terry" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='terry'? 'selected':'' }}>Terry</option>
								<option value="phoebe" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='phoebe'? 'selected':'' }}>Phoebe</option>
								<option value="apollo" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='apollo'? 'selected':'' }}>Apollo</option>
								<option value="kira" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='kira'? 'selected':'' }}>Kira</option>
								<option value="steve" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='steve'? 'selected':'' }}>Steve</option>
								<option value="moses" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='moses'? 'selected':'' }}>Moses</option>
								<option value="jazz" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='jazz'? 'selected':'' }}>Jazz</option>
								<option value="ming" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='ming'? 'selected':'' }}>Ming</option>
								<option value="lexi" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='lexi'? 'selected':'' }}>Lexi</option>
								<option value="duke" {{ !empty($settings['hoverstyle']) && $settings['hoverstyle']=='duke'? 'selected':'' }}>Duke</option>
							</select>
						</div>
						<div class="form-group">
							<label for="border">
								Border
							</label>
							<input type="number" class="form-control" id="border" min="0" name="border" value="{{ !empty($settings['border'])? $settings['border']:'0' }}">
						</div>
						<div class="form-group">
							<label for="border">
								Backlight Color
							</label>
							<div data-color-format="rgba" data-color="{{ !empty($settings['backlightcolor'])? $settings['backlightcolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="backlightcolor" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['backlightcolor'])? $settings['backlightcolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="border">
								Overlay Color
							</label>
							<div data-color-format="rgba" data-color="{{ !empty($settings['overlaycolor'])? $settings['overlaycolor']:'rgba(0,0,0,0)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="overlaycolor" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['overlaycolor'])? $settings['overlaycolor']:'rgba(0,0,0,0)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="border">
								Text Color
							</label>
							<div data-color-format="rgba" data-color="{{ !empty($settings['textcolor'])? $settings['textcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="textcolor" readonly="readonly" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['textcolor'])? $settings['textcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="border">
								Border Color
							</label>
							<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="bordercolor" readonly="readonly" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="borderradius">
								Border Radius
							</label>
							<input type="number" class="form-control" min="0" id="borderradius" name="borderradius" value="{{ !empty($settings['borderradius'])? $settings['borderradius']:'0' }}">
						</div>
            <div class="form-group">
                  <h4 class="text-muted m-b-15 m-t-15 font-15">Items Spacing (in px)</h4>
                  <div class="row">
                    <div class="col-sm-3">
                      <input type="number" class="form-control" min="0" name="spacing-left" value="{{ !empty($settings['spacing-left'])? $settings['spacing-left']:'0' }}" rel="txtTooltip" title="Left" data-placement="top">
                    </div>
                    <div class="col-sm-3">
                      <input type="number" class="form-control" min="0" name="spacing-right" value="{{ !empty($settings['spacing-right'])? $settings['spacing-right']:'0' }}" rel="txtTooltip" title="Right" data-placement="top">
                    </div>
                    <div class="col-sm-3">
                      <input type="number" class="form-control" min="0" name="spacing-top" value="{{ !empty($settings['spacing-top'])? $settings['spacing-top']:'0' }}" rel="txtTooltip" title="Top" data-placement="top">
                    </div>
                    <div class="col-sm-3">
                      <input type="number" class="form-control" min="0" name="spacing-bottom" value="{{ !empty($settings['spacing-bottom'])? $settings['spacing-bottom']:'0' }}" rel="txtTooltip" title="Bottom" data-placement="top">
                    </div>
                  </div>
                  <div class="help"></div>
                </div>
						<div class="form-group">
							<label for="colperrow">
								Columns per row
							</label>
							<select data-style="btn-white" class="form-control selectpicker" id="colperrow" name="colperrow">
								<option value="1" {{ !empty($settings['colperrow']) && $settings['colperrow']=='1'? 'selected':'' }}>1</option>
								<option value="2" {{ !empty($settings['colperrow']) && $settings['colperrow']=='2'? 'selected':'' }}>2</option>
								<option value="3" {{ !empty($settings['colperrow']) && $settings['colperrow']=='3'? 'selected':'' }}>3</option>
								<option value="4" {{ !empty($settings['colperrow']) && $settings['colperrow']=='4'? 'selected':'' }}>4</option>
								<option value="6" {{ !empty($settings['colperrow']) && $settings['colperrow']=='6'? 'selected':'' }}>6</option>
							</select>
						</div>
						<div class="form-group">
							<label for="height">
								Height
							</label>
							<input type="number" class="form-control" min="1" id="height" name="height" value="{{ !empty($settings['height'])? $settings['height']:'250' }}">
						</div>
						<div class="form-group">
							<div class="checkbox">
								<input id="hidetitle" type="checkbox" name="hidetitle" {{ !empty($settings['hidetitle'])? 'checked':'' }}>
								<label for="hidetitle">
									Hide Title
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="checkbox">
								<input id="hidedescription" type="checkbox" name="hidedescription" {{ !empty($settings['hidedescription'])? 'checked':'' }}>
								<label for="hidedescription">
									Hide Description
								</label>
							</div>
						</div>
            <div class="form-group mr-auto">
              <div class="checkbox">
                <input id="hidebeforetext" type="checkbox" name="hidebeforetext" {{ !empty($settings['hidebeforetext'])? 'checked':'' }}>
                <label for="hidebeforetext">
                  Hide Before Text
                </label>
              </div>
            </div>
            <div class="form-group mr-auto">
              <div class="checkbox">
                <input id="hideaftertext" type="checkbox" name="hideaftertext" {{ !empty($settings['hideaftertext'])? 'checked':'' }}>
                <label for="hideaftertext">
                  Hide After Text
                </label>
              </div>
            </div>
						<div class="form-group">
							<div class="checkbox">
								<input id="showtitle" type="checkbox" name="showtitle" {{ !empty($settings['showtitle'])? 'checked':'' }}>
								<label for="showtitle">
									Show Title on Hover
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="checkbox">
								<input id="overlayonhover" type="checkbox" name="overlayonhover" {{ !empty($settings['overlayonhover'])? 'checked':'' }}>
								<label for="overlayonhover">
									Show Overlay on hover
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="checkbox">
								<input id="showdescription" type="checkbox" name="showdescription" {{ !empty($settings['showdescription'])? 'checked':'' }}>
								<label for="showdescription">
									Show Description on Hover
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="checkbox">
								<input id="lightbox" type="checkbox" name="lightbox" {{ !empty($settings['lightbox'])? 'checked':'' }}>
								<label for="lightbox">
									Show Image in Lightbox
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="card-box">
					<div class="row choosemedia" id="sortable">
						@if(!empty($settings['title']))
							@foreach($settings['title'] as $key=>$val)
								<div class="col-sm-12 imagelist">
                  <div class="control" data-toggle="collapse" data-target="#list{{ $key }}">
									    <span class="handle"><i class="fas fa-arrows-alt"></i></span>
                      <a href="#" class="deletethumb"><i class="fa fa-remove"></i></a>
                  </div>
                  <div class="collapse show" id="list{{ $key }}">
                        <div class="imageholder">
                            <textarea class="form-control" placeholder="Before Text" name="beforetext[]">{!! !empty($settings['beforetext'][$key])?$settings['beforetext'][$key]:'' !!}</textarea>
                            <div class="addedimage">
              								<div class="image {!! !empty($settings['image'][$key])?'image-has':'' !!}" style="background-image:url('{!! !empty($settings['image'][$key])?$settings['image'][$key]:'' !!}')">
                                <input type="hidden" name="image[]" value="{!! !empty($settings['image'][$key])?$settings['image'][$key]:'' !!}">
                                <a href="#" class="changeimage"><i class="fa fa-camera"></i></a><a href="#" class="removeimage">Remove</a>
                              </div>
                            </div>
                            <textarea class="form-control" placeholder="After Text" name="aftertext[]">{!! !empty($settings['aftertext'][$key])?$settings['aftertext'][$key]:'' !!}</textarea>
                        </div>
                        <div class="form">
                            <input type="text" class="form-control" name="title[]" value="{!! !empty($settings['title'][$key])?$settings['title'][$key]:'' !!}" placeholder="Title">
                            <textarea class="form-control advtextarea" placeholder="Description" name="description[]">{!! !empty($settings['description'][$key])?$settings['description'][$key]:'' !!}</textarea>
                            <input type="text" class="form-control" name="link[]" value="{!! !empty($settings['link'][$key])?$settings['link'][$key]:'' !!}" placeholder="Link">
            								<select data-style="btn-white" class="form-control selectpicker" name="linktarget[]">
            									<option value="_self" {{ !empty($settings['linktarget'][$key]) && $settings['linktarget'][$key]=='_self'? 'selected':'' }}>None</option>
            									<option value="_blank" {{ !empty($settings['linktarget'][$key]) && $settings['linktarget'][$key]=='_blank'? 'selected':'' }}>New Tab</option>
            								</select>
                        </div>
                  </div>
								</div>
							@endforeach
						@endif
						<div class="col-sm-12 addedimage adder"><div class="image"><a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-plus-square"></i></a></div></div>
					</div>
				</div>
			</div>

    </div>
</div>
</form>
@include('admin._partials.popupuploader')
<script src="{{ asset('assets/private/plugins/tinymce/tinymce.min.js') }}"></script>

<script>
$(document).ready(function(){
	initTinymce();
	setInterval(function(){
		if($('.allmedia .imageholder.selected').length>0){
			if($('#media-select .md-custom-footer').hasClass('half')){
			}else{
				$('#media-select .md-custom-footer').addClass('half');
				$('#media-select .md-custom-footer .insert').addClass('active');
				$('#media-select .md-custom-footer').prepend('<button type="button" class="btn btn-default waves-effect insert" data-dismiss="modal">Insert Media</button>');
			}
		}else{
			$('#media-select .md-custom-footer').removeClass('half');
			$('#media-select .md-custom-footer .insert').remove();
			$('#media-select .md-custom-footer .insert').removeClass('active');
		}
	}, 50);
	$('#media-select .md-custom-footer').addClass('half');
	$('#media-select .md-custom-footer').prepend('<button type="button" class="btn btn-default waves-effect insert" data-dismiss="modal">Insert Media</button>');
	$('.allmedia').on('click','.imageholder a',function(){
		if($(this).parents('#media-select').hasClass('changeimage')){
			$('.addedimage.selected').find('input[name="image[]"]').val($(this).attr('href'));
			$('.addedimage.selected').find('.image').css('background-image','url("'+$(this).attr('href')+'")');
			$(this).parents('#media-select').removeClass('changeimage');
      $('.addedimage.selected').find('.image').addClass('image-has');
			$('.addedimage').removeClass('selected');
			$('#media-select').removeClass('md-custom-show');
		}else{
			$(this).parents('.imageholder').toggleClass('selected');
		}
		return false;
	})
	$('.colorpicker-rgba').colorpicker();
	$('#clickpreview').click(function(){
		$.ajax({
            type: "GET",
            url: '{{URL::route('widget.preview')}}',
            data: $("#widgetform").serialize(),
            success: function( msg ) {
				$('#preview').html(msg);
            }
        });
	})
	$('#media-select').on('click','.insert', function(){
		var html ="";
		$('.allmedia .imageholder.selected a').each(function(){
			html = html+'<div class="col-sm-12 imagelist"><div class="control"><span class="handle"><i class="fas fa-arrows-alt"></i></span><a href="#" class="deletethumb"><i class="fa fa-remove"></i></a></div><div class="imageholder"><textarea class="form-control" placeholder="Before Text" name="beforetext[]"></textarea><div class="addedimage"><div class="image image-has" style="background-image:url(\''+$(this).attr('href')+'\')"><input type="hidden" name="image[]" value="'+$(this).attr('href')+'"><a href="#" class="changeimage"><i class="fa fa-camera"></i></a><a href="#" class="removeimage">Remove</a></div></div><textarea class="form-control" placeholder="After Text" name="aftertext[]"></textarea></div><div class="form"><input type="text" class="form-control" name="title[]" placeholder="Title"><textarea class="form-control advtextarea" placeholder="Description" name="description[]"></textarea><input type="text" class="form-control" name="link[]" value="" placeholder="Link"><select class="form-control selectpicker" data-style="btn-white" name="linktarget[]"><option value="_self">None</option><option value="_blank">New Tab</option></select></div></div>';
		})
		$('.choosemedia').prepend(html);
		$('.allmedia .imageholder').removeClass('selected');
		$('#media-select').removeClass('md-custom-show');
		initTinymce();
		$('.selectpicker').selectpicker();
	})
	$('.choosemedia').on('click','.changeimage', function(){
		$('.allmedia .imageholder').removeClass('selected');
		$('#media-select').addClass('md-custom-show');
		$(this).parents('.addedimage').addClass('selected');
		$('#media-select').addClass('changeimage');
		return false;
	})
	$('.choosemedia').on('click','.deletethumb', function(){
    var r = confirm("Are you sure?");
    if (r == true) {
      $(this).parents('.imagelist').remove();
  		return false;
    } else {
      return false;
    }
	})
	$('#save').click(function(){
		$('#widgetform').submit();
	})
	$('.removeimage').click(function(){
		$(this).parents('.image').css('background-image', 'none');
		$(this).parents('.image').removeClass('image-has');
		$(this).parents('.image').find('input[type="hidden"]').val('');
		return false;
	})
})
function initTinymce(){
tinymce.init({
		selector: ".advtextarea",
		theme: "modern",
		height:100,
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality template paste textcolor",
			"fontawesome noneditable"
		],
		content_css: 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
		valid_elements : '*[*]',
		noneditable_noneditable_class: 'fa',
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons fontawesome",
		extended_valid_elements: 'span[*]',
		style_formats: [
			{title: 'Bold text', inline: 'b'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		]
	});
}
</script>
@endsection
