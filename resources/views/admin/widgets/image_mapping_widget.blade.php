@extends('layouts.app')

@section('extra')
<link rel="stylesheet" href="{{ asset('assets/private/plugins/image-map-pro/squares.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/image-map-pro/squares-editor.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/image-map-pro/squares-controls.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/image-map-pro/wcp-editor.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/image-map-pro/wcp-editor-controls.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/image-map-pro/image-map-pro-editor.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/image-map-pro/image-map-pro.css') }}">
<style>
#wcp-editor-button-new,#wcp-editor-button-save,#wcp-editor-button-load,#wcp-editor-extra-main-buttons{
	display:none !important;
}
#wcp-editor-left {
    width: 320px;
}
#wcp-editor-right {
    height: auto;
}
#wcp-editor {
    min-height: 700px;
}
#wcp-editor-textarea-export{

}
#wcp-editor-right {
    width: 150px;
}
</style>
<script>
$(document).ready(function(){

	/* Preload Saved Map */
	setTimeout(function(){
		$('div[data-wcp-form-tab-button-name="image"]').trigger('click');
		$('#wcp-editor-extra-main-buttons .wcp-editor-extra-main-button').each(function(){
			if($(this).attr('data-wcp-editor-extra-main-button-name')=='import'){
				$(this).trigger('click');
				$('#wcp-editor-modal').hide();
				$('#wcp-editor-textarea-import').val('{!! !empty($settings['code'])?$settings['code']:'' !!}')
				$('#wcp-editor-confirm-import').trigger('click');
			}
		})
	}, 500);

	$('#save').on('click', function(){
		$('#wcp-editor-extra-main-buttons .wcp-editor-extra-main-button').each(function(){
			if($(this).attr('data-wcp-editor-extra-main-button-name')=='export'){
				$(this).trigger('click');
				$('#wcp-editor-modal').hide();
				$('input[name="code"]').val($('#wcp-editor-textarea-export').val());
				$('#widgetform').submit();
			}
		})
	})
})
</script>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right">
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg m-b-20">Update</button>
						</div>
				</div>
				<div class="card-box" style="padding:0">
					<div id="wcp-editor">

					</div>
				</div>
			</div>

			<form method="post" id="widgetform" action="{{ route('widget.save') }}">
					{{csrf_field()}}
					<input type="hidden" name="id" value="{{ $widget->id }}">
					<input type="hidden" name="code" value="">
			</form>
    </div>
</div>

<script src="{{ asset('assets/private/plugins/image-map-pro/squares-renderer.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/squares.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/squares-elements-jquery.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/squares-controls.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/wcp-editor.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/wcp-editor-controls.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/wcp-compress.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/wcp-icons.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/image-map-pro-defaults.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/image-map-pro-editor.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/image-map-pro-editor-content.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/image-map-pro-editor-local-storage.js') }}"></script>
<script src="{{ asset('assets/private/plugins/image-map-pro/image-map-pro-editor-init-jquery.js') }}"></script>
<script>
var t = jQuery;
var o = document;
t(o).ready(function() {
	var e= {
		id:811, editor: {
			previewMode: 0, selected_shape: "poly-974", tool: "select"
		},
		runtime:{
			is_fullscreen:0
		}
		, general: {
			name: "{{ $widget->title }}", width: 909, height: 557, naturalWidth: 2000, naturalHeight: 1237, preserve_quality: 0
		}
		, image: {
			url: ""
		}
		, tooltips: {
			constrain_tooltips: 0, fullscreen_tooltips: "mobile-only"
		}
		, spots:[ ]
	}
	;
	t.image_map_pro_init_editor(e, t.WCPEditorSettings)
}
)
</script>
<script src="{{ asset('assets/private/plugins/image-map-pro/image-map-pro.js') }}"></script>

@endsection
