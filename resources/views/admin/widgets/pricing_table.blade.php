@extends('layouts.app')

@section('extra')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/pricetable/css/creator.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/pricetable/css/stepper.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/pricetable/css/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/pricetable/css/spectrum.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/pricetable/css/prism.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/pricetable/css/snippet.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/pricetable/css/icheck.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/pricetable/css/square.css') }}">
@if(!empty(getSetting('imported_font_link','styles')))
	@foreach(getSetting('imported_font_link','styles') as $links)
		<link href="{!! $links !!}" rel="stylesheet">
	@endforeach
@endif
<script>
//Initialize Data
var table = [];
var currentSelected = 1 ;
var currentItems = 0;
var currentTemplate = {{ !empty($settings['template_select'])?$settings['template_select']:0 }};
var newTemplate = 0;
var currentScheme = 0;
var currentFeatures = 0;
var currentCurrency = '{{ !empty($settings['currency_select'])?$settings['currency_select']:'$' }}';
var currentHover = '{{ !empty($settings['hover_select'])?$settings['hover_select']:'' }}';
var newHover = '';
var currentPer = '{{ !empty($settings['per_select'])?$settings['per_select']:'PER MONTH' }}';
var clicked = false;
var currentId;
var currentIcon;
var itemColor;
var sortingCtrl = 1;
var arr = '{{ !empty($settings['zoom_items'])?$settings['zoom_items']:'' }}';
var zoomedItems = arr.split(",");
@if(!empty($settings['first_button']) && $settings['first_button']=='on')
	var button_1 = true;
@else
	var button_1 = false;
@endif

@if(!empty($settings['second_button']) && $settings['second_button']=='on')
	var button_2 = true;
@else
	var button_2 = false;
@endif

@if(!empty($settings['hover-effect']) && $settings['hover-effect']=='on')
	var hover = true;
@else
	var hover = false;
@endif

var creatorColor;

@if(!empty($settings['spacing']) && $settings['spacing']=='on')
	var spaces = false;
@else
	var spaces = true;
@endif

var currentFont = 0;
var show = true;


var fonts = [
		{
			"name": "Default",
			"url": "Inherit",
			"size": "50%"
		},
		@if(!empty(getSetting('imported_font_name','styles')))
			@foreach(getSetting('imported_font_name','styles') as $key=>$val)
				{
					"name": "{{ $val }}",
					"url": "{{ $val }}",
					"size": "50%"
				},
			@endforeach
		@endif
	];

 var templates = [
        {
            "name": "Standard",
            "html": "<div class='ptc-header'><div class='handle'><div class='handle-bg'><i class='fa fa-bars'></i></div></div><div class='ptc-note ptc-hide'><div class='ptc-note-icon'><i class='fa fa-cog'></i></div><div class='ptc-note-exposed'></div><div class='ptc-note-fold'></div></div><div class='ptc-stars'></div><h1 class='ptc-title' contenteditable='true' spellcheck='false'>TITLE</h1><div class='ptc-arrow-down ptc-first'></div></div><div class='ptc-price' spellcheck='false'><span class='ptc-total'><sup class='ptc-currency'>$</sup></span><span class='ptc-total' contenteditable='true'>99.99</span><p class='ptc-text'>PER MONTH</p><a class='ptc-button ptc-show-inline' href='#'>SIGN UP</a><div class='ptc-arrow-down ptc-second'></div></div><div class='ptc-content'><ul class='ptc-features'></ul></div><div class='ptc-footer'><a class='ptc-button ptc-show-inline' href='#'>SIGN UP</a></div>"

        },
        {
            "name": "Rounded",
            "html": "<div class='ptc-header ptc-price' contenteditable='true' spellcheck='false'><div class='handle'><div class='handle-bg'><i class='fa fa-bars'></i></div></div><div class='ptc-note ptc-hide' contenteditable='false'><div class='ptc-note-icon'><i class='fa fa-cog'></i></div><div class='ptc-note-exposed'></div><div class='ptc-note-fold'></div></div><h1 class='ptc-title' contenteditable='true' spellcheck='false'>TITLE</h1><span class='ptc-total'><sup class='ptc-currency'>$</sup>99.99</span><p class='ptc-text'>PER MONTH</p><a class='ptc-button ptc-show-inline' href='#'>SIGN UP</a><div class='ptc-stars'></div></div><div class='ptc-content'><ul class='ptc-features'></ul></div><div class='ptc-footer'><a class='ptc-button ptc-show-inline' href='#'>SIGN UP</a></div>"
       },
        {
            "name": "Plain",
            "html": "<div class='ptc-header' contenteditable='true' spellcheck='false'><div class='handle'><div class='handle-bg'><i class='fa fa-bars'></i></div></div><div class='ptc-note ptc-hide' contenteditable='false'><div class='ptc-note-icon'><i class='fa fa-cog'></i></div></div><h1 class='ptc-title' contenteditable='true' spellcheck='false'>TITLE</h1><span class='ptc-price ptc-total' contenteditable='false'><sup class='ptc-currency' contenteditable='true'>$</sup><span contenteditable='true'>99.99</span></span><p class='ptc-text' contenteditable='false'>PER MONTH</p><div class='ptc-stars'></div></div><div class='ptc-footer'><a class='ptc-button ptc-show-inline' href='#'>SIGN UP</a></div><div class='ptc-content'><ul class='ptc-features'></ul></div>"
        }
    ];


</script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/jquery-ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/selectbox.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/impromptu.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/owlCarousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/stepper.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/enscroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/spectrumColorpicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/iCheck.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/prism.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/htmlClean.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/vkBeautify.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/syntaxHighlighter.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/pricetable/js/creator.js') }}"></script>
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />
<style>
#pricingtablehtml{
	overflow: hidden;
}
.nav-content .btn{
	font-size: 16px;
    padding: 10px;
    height: 50px;
    line-height: 30px;
		color: #fff !important;
}
.nav-content .btn:hover{
	opacity: .8;
}
.wrapper {
    padding-top: 90px;
    padding-bottom: 50px;
}
</style>
@endsection

@section('content')
<div class="container-fluid">

    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
						<div class="d-flex justify-content-between">
								<div class="left">
											<div class="shortcodeplaceholder">
													<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
													<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
															<i class="fa fa-copy"></i>
													</button>
											</div>
								</div>
								<div class="right">
									<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg m-b-20">Update</button>
								</div>
						</div>
			</div>
			<div class="col-sm-12 m-t-15">

				<div class="card-box">
					<form method="post" id="widgetform" action="{{ route('widget.save') }}">
						{{csrf_field()}}
						<input type="hidden" name="id" value="{{ $widget->id }}">
						<input type="hidden" name="html" value="{{ $widget->html }}">

					<div id="pricingtablehtml" class="pricing_table_widget">
									<aside class="">
										<i class="toggle fa fa-bars"></i>
										<div class="nav-content" tabindex="0">
											<div class="nav-comp"><button type="button" class="btn btn-add">INSERT TABLE</button></div>

											<div class="nav-comp template">
												<select id="template_select" name="template_select">
													<option value="0" {{ !empty($settings['template_select']) && $settings['template_select']==0?'selected':'' }}>Standard</option>
													<option value="1" {{ !empty($settings['template_select']) && $settings['template_select']==1?'selected':'' }}>Rounded</option>
													<option value="2" {{ !empty($settings['template_select']) && $settings['template_select']==2?'selected':'' }}>Plain</option>
												</select>
											</div>

											<div class="nav-comp">
												<div class="owl-carousel owl-fonts">

												</div>
											</div>


											<div class="nav-comp">
												<div class="owl-carousel owl-schemes">

												</div>
											</div>
											<div class="nav-comp">
												<img style="min-width:16px;min-height:16px;box-sizing:unset;box-shadow:none;background:unset;padding:0 6px 0 0;cursor:pointer;" src="chrome-extension://ohcpnigalekghcmgcdcenkpelffpdolg/img/icon16.png" title="Select with ColorPick Eyedropper - See advanced option: &quot;Add eyedropper near input[type=color] fields on websites&quot;" class="colorpick-eyedropper-input-trigger"><input id="colorpicker_hue" type="color" class="color-box-hue" colorpick-eyedropper-active="true" style="display: none;">
												<p class="inline">SINGLE COLOR SCHEME</p>
											</div>
											<div class="nav-comp">
												<select id="currency_select" name="currency_select" sb="32938322">
													<option value="$" {{ !empty($settings['currency_select']) && $settings['currency_select']=='$'?'selected':'' }}>Dollar - $</option>
													<option value="€" {{ !empty($settings['currency_select']) && $settings['currency_select']=='€'?'selected':'' }}>Euro - €</option>
													<option value="£" {{ !empty($settings['currency_select']) && $settings['currency_select']=='£'?'selected':'' }}>Pound - £</option>
													<option value="¢" {{ !empty($settings['currency_select']) && $settings['currency_select']=='¢'?'selected':'' }}>Cent - ¢</option>
												</select>
											</div>
											<div class="nav-comp">
												<select id="per_select" name="per_select" sb="60524589">
													<option value="PER YEAR" {{ !empty($settings['per_select']) && $settings['per_select']=='PER YEAR'?'selected':'' }}>Per Year</option>
													<option value="PER MONTH" {{ !empty($settings['per_select']) && $settings['per_select']=='PER MONTH'?'selected':'' }}>Per Month</option>
													<option value="PER WEEK" {{ !empty($settings['per_select']) && $settings['per_select']=='PER WEEK'?'selected':'' }}>Per Week</option>
													<option value="PER DAY" {{ !empty($settings['per_select']) && $settings['per_select']=='PER DAY'?'selected':'' }}>Per Day</option>
												</select>
											</div>
											<div class="nav-comp"><button type="button" class="btn btn-feature">INSERT FEATURE</button></div>
												<div class="nav-comp">
													<div class="icheckbox_square-grey" style="position: relative;">
													<input type="checkbox" name="spacing" class="spacing" {{ !empty($settings['spacing']) && $settings['spacing']=='on'?'checked':'' }}>
													</div>
													<p class="inline">NO SPACES</p>
												</div>
												<div class="nav-comp">
													<div class="icheckbox_square-grey" style="position: relative;">
													<input type="checkbox" name="hover-effect" class="hover-effect inline" {{ !empty($settings['hover-effect']) && $settings['hover-effect']=='on'?'checked':'' }}>
													</div>
													<p class="inline">HOVER EFFECT</p>
												</div>
												<div class="nav-comp hover-select"  {!! !empty($settings['hover-effect']) && $settings['hover-effect']=='on'?'style="display:block"':'' !!}>
													<select id="hover_select" name="hover_select">
														<option value="">Choose Effect</option>
														<option value="wobble-vertical" {{ !empty($settings['hover_select']) && $settings['hover_select']=='wobble-vertical'?'selected':'' }}>Wobble Vertical</option>
														<option value="wobble-horizontal" {{ !empty($settings['hover_select']) && $settings['hover_select']=='wobble-horizontal'?'selected':'' }}>Wobble Horizontal</option>
														<option value="wobble-to-bottom-right" {{ !empty($settings['hover_select']) && $settings['hover_select']=='wobble-to-bottom-right'?'selected':'' }}>Wobble To Bottom Right</option>
														<option value="wobble-to-top-right" {{ !empty($settings['hover_select']) && $settings['hover_select']=='wobble-to-top-right'?'selected':'' }}>Wobble To Top Right</option>
														<option value="grow" {{ !empty($settings['hover_select']) && $settings['hover_select']=='grow'?'selected':'' }}>Grow</option>
														<option value="shrink" {{ !empty($settings['hover_select']) && $settings['hover_select']=='shrink'?'selected':'' }}>Shrink</option>
														<option value="rotate" {{ !empty($settings['hover_select']) && $settings['hover_select']=='rotate'?'selected':'' }}>Rotate</option>
														<option value="grow-rotate" {{ !empty($settings['hover_select']) && $settings['hover_select']=='grow-rotate'?'selected':'' }}>Grow Rotate</option>
														<option value="float" {{ !empty($settings['hover_select']) && $settings['hover_select']=='float'?'selected':'' }}>Float</option>
														<option value="sink" {{ !empty($settings['hover_select']) && $settings['hover_select']=='sink'?'selected':'' }}>Sink</option>
														<option value="buzz-out" {{ !empty($settings['hover_select']) && $settings['hover_select']=='buzz-out'?'selected':'' }}>Buzz Out</option>
													</select>
												</div>
												<div class="nav-comp">
													<div class="icheckbox_square-grey" style="position: relative;">
													<input type="checkbox" name="first_button" class="button-1 inline" {{ !empty($settings['first_button']) && $settings['first_button']=='on'?'checked':'' }}></div>
													<p class="inline">FIRST BUTTON</p>
												</div>
												<div class="nav-comp">
													<div class="icheckbox_square-grey" style="position: relative;">
													<input type="checkbox" name="second_button" class="button-2 inline" {{ !empty($settings['second_button']) && $settings['second_button']=='on'?'checked':'' }}>
													</div>
													<p class="inline">SECOND BUTTON</p>
												</div>
											<div class="divider"></div>
											<div class="item-options" style="display: block;">
												<div class="nav-comp">
													<h1>SELECTED TABLE OPTIONS</h1>
												</div>
												<div class="nav-comp"><img style="min-width:16px;min-height:16px;box-sizing:unset;box-shadow:none;background:unset;padding:0 6px 0 0;cursor:pointer;" src="chrome-extension://ohcpnigalekghcmgcdcenkpelffpdolg/img/icon16.png" title="Select with ColorPick Eyedropper - See advanced option: &quot;Add eyedropper near input[type=color] fields on websites&quot;" class="colorpick-eyedropper-input-trigger">
													<input id="colorpicker" type="color"  class="color-box-hue" colorpick-eyedropper-active="true" style="display: none;">
													<p class="inline">SINGLE COLOR SCHEME</p>
												</div>
												<div class="nav-comp">
													<div class="stepper-wrap" style="margin: 0px 25px 0px 0px;"><input type="text" name="rating" class="rating inline" data-limit="[0,6]" style="margin: 0px;"></div>
													<p class="inline">STAR RATING</p>
												</div>
												<div class="nav-comp">
													<div class="icheckbox_square-grey" style="position: relative;">
														<input type="checkbox" name="note" class="note inline">
													</div>
													<p class="inline">NOTE</p>
												</div>
												<div class="nav-comp">
													<ul class="icon-set" id="scrollbox" tabindex="0" style="width: 240px; padding-right: 15px; outline: none; overflow: hidden; display: none;">
														<li data-icon="fa fa-adjust"></li>
														<li data-icon="fa fa-adn"></li>
														<li data-icon="fa fa-align-center"></li>
														<li data-icon="fa fa-align-justify"></li>
														<li data-icon="fa fa-align-left"></li>
														<li data-icon="fa fa-align-right"></li>
														<li data-icon="fa fa-ambulance"></li>
														<li data-icon="fa fa-anchor"></li>
														<li data-icon="fa fa-android"></li>
														<li data-icon="fa fa-angellist"></li>
														<li data-icon="fa fa-angle-double-down"></li>
														<li data-icon="fa fa-angle-double-left"></li>
														<li data-icon="fa fa-angle-double-right"></li>
														<li data-icon="fa fa-angle-double-up"></li>
														<li data-icon="fa fa-angle-down"></li>
														<li data-icon="fa fa-angle-left"></li>
														<li data-icon="fa fa-angle-right"></li>
														<li data-icon="fa fa-angle-up"></li>
														<li data-icon="fa fa-apple"></li>
														<li data-icon="fa fa-archive"></li>
														<li data-icon="fa fa-area-chart"></li>
														<li data-icon="fa fa-arrow-circle-down"></li>
														<li data-icon="fa fa-arrow-circle-left"></li>
														<li data-icon="fa fa-arrow-circle-o-down"></li>
														<li data-icon="fa fa-arrow-circle-o-left"></li>
														<li data-icon="fa fa-arrow-circle-o-right"></li>
														<li data-icon="fa fa-arrow-circle-o-up"></li>
														<li data-icon="fa fa-arrow-circle-right"></li>
														<li data-icon="fa fa-arrow-circle-up"></li>
														<li data-icon="fa fa-arrow-down"></li>
														<li data-icon="fa fa-arrow-left"></li>
														<li data-icon="fa fa-arrow-right"></li>
														<li data-icon="fa fa-arrow-up"></li>
														<li data-icon="fa fa-arrows"></li>
														<li data-icon="fa fa-arrows-alt"></li>
														<li data-icon="fa fa-arrows-h"></li>
														<li data-icon="fa fa-arrows-v"></li>
														<li data-icon="fa fa-asterisk"></li>
														<li data-icon="fa fa-at"></li>
														<li data-icon="fa fa-automobile"></li>
														<li data-icon="fa fa-backward"></li>
														<li data-icon="fa fa-ban"></li>
														<li data-icon="fa fa-bank"></li>
														<li data-icon="fa fa-bar-chart"></li>
														<li data-icon="fa fa-bar-chart-o"></li>
														<li data-icon="fa fa-barcode"></li>
														<li data-icon="fa fa-bars"></li>
														<li data-icon="fa fa-beer"></li>
														<li data-icon="fa fa-behance"></li>
														<li data-icon="fa fa-behance-square"></li>
														<li data-icon="fa fa-bell"></li>
														<li data-icon="fa fa-bell-o"></li>
														<li data-icon="fa fa-bell-slash"></li>
														<li data-icon="fa fa-bell-slash-o"></li>
														<li data-icon="fa fa-bicycle"></li>
														<li data-icon="fa fa-binoculars"></li>
														<li data-icon="fa fa-birthday-cake"></li>
														<li data-icon="fa fa-bitbucket"></li>
														<li data-icon="fa fa-bitbucket-square"></li>
														<li data-icon="fa fa-bitcoin"></li>
														<li data-icon="fa fa-bold"></li>
														<li data-icon="fa fa-bolt"></li>
														<li data-icon="fa fa-bomb"></li>
														<li data-icon="fa fa-book"></li>
														<li data-icon="fa fa-bookmark"></li>
														<li data-icon="fa fa-bookmark-o"></li>
														<li data-icon="fa fa-briefcase"></li>
														<li data-icon="fa fa-btc"></li>
														<li data-icon="fa fa-bug"></li>
														<li data-icon="fa fa-building"></li>
														<li data-icon="fa fa-building-o"></li>
														<li data-icon="fa fa-bullhorn"></li>
														<li data-icon="fa fa-bullseye"></li>
														<li data-icon="fa fa-bus"></li>
														<li data-icon="fa fa-cab"></li>
														<li data-icon="fa fa-calculator"></li>
														<li data-icon="fa fa-calendar"></li>
														<li data-icon="fa fa-calendar-o"></li>
														<li data-icon="fa fa-camera"></li>
														<li data-icon="fa fa-camera-retro"></li>
														<li data-icon="fa fa-car"></li>
														<li data-icon="fa fa-caret-down"></li>
														<li data-icon="fa fa-caret-left"></li>
														<li data-icon="fa fa-caret-right"></li>
														<li data-icon="fa fa-caret-square-o-down"></li>
														<li data-icon="fa fa-caret-square-o-left"></li>
														<li data-icon="fa fa-caret-square-o-right"></li>
														<li data-icon="fa fa-caret-square-o-up"></li>
														<li data-icon="fa fa-caret-up"></li>
														<li data-icon="fa fa-cc"></li>
														<li data-icon="fa fa-cc-amex"></li>
														<li data-icon="fa fa-cc-discover"></li>
														<li data-icon="fa fa-cc-mastercard"></li>
														<li data-icon="fa fa-cc-paypal"></li>
														<li data-icon="fa fa-cc-stripe"></li>
														<li data-icon="fa fa-cc-visa"></li>
														<li data-icon="fa fa-certificate"></li>
														<li data-icon="fa fa-chain"></li>
														<li data-icon="fa fa-chain-broken"></li>
														<li data-icon="fa fa-check"></li>
														<li data-icon="fa fa-check-circle"></li>
														<li data-icon="fa fa-check-circle-o"></li>
														<li data-icon="fa fa-check-square"></li>
														<li data-icon="fa fa-check-square-o"></li>
														<li data-icon="fa fa-chevron-circle-down"></li>
														<li data-icon="fa fa-chevron-circle-left"></li>
														<li data-icon="fa fa-chevron-circle-right"></li>
														<li data-icon="fa fa-chevron-circle-up"></li>
														<li data-icon="fa fa-chevron-down"></li>
														<li data-icon="fa fa-chevron-left"></li>
														<li data-icon="fa fa-chevron-right"></li>
														<li data-icon="fa fa-chevron-up"></li>
														<li data-icon="fa fa-child"></li>
														<li data-icon="fa fa-circle"></li>
														<li data-icon="fa fa-circle-o"></li>
														<li data-icon="fa fa-circle-o-notch"></li>
														<li data-icon="fa fa-circle-thin"></li>
														<li data-icon="fa fa-clipboard"></li>
														<li data-icon="fa fa-clock-o"></li>
														<li data-icon="fa fa-close"></li>
														<li data-icon="fa fa-cloud"></li>
														<li data-icon="fa fa-cloud-download"></li>
														<li data-icon="fa fa-cloud-upload"></li>
														<li data-icon="fa fa-cny"></li>
														<li data-icon="fa fa-code"></li>
														<li data-icon="fa fa-code-fork"></li>
														<li data-icon="fa fa-codepen"></li>
														<li data-icon="fa fa-coffee"></li>
														<li data-icon="fa fa-cog"></li>
														<li data-icon="fa fa-cogs"></li>
														<li data-icon="fa fa-columns"></li>
														<li data-icon="fa fa-comment"></li>
														<li data-icon="fa fa-comment-o"></li>
														<li data-icon="fa fa-comments"></li>
														<li data-icon="fa fa-comments-o"></li>
														<li data-icon="fa fa-compass"></li>
														<li data-icon="fa fa-compress"></li>
														<li data-icon="fa fa-copy"></li>
														<li data-icon="fa fa-copyright"></li>
														<li data-icon="fa fa-credit-card"></li>
														<li data-icon="fa fa-crop"></li>
														<li data-icon="fa fa-crosshairs"></li>
														<li data-icon="fa fa-css3"></li>
														<li data-icon="fa fa-cube"></li>
														<li data-icon="fa fa-cubes"></li>
														<li data-icon="fa fa-cut"></li>
														<li data-icon="fa fa-cutlery"></li>
														<li data-icon="fa fa-dashboard"></li>
														<li data-icon="fa fa-database"></li>
														<li data-icon="fa fa-dedent"></li>
														<li data-icon="fa fa-delicious"></li>
														<li data-icon="fa fa-desktop"></li>
														<li data-icon="fa fa-deviantart"></li>
														<li data-icon="fa fa-digg"></li>
														<li data-icon="fa fa-dollar"></li>
														<li data-icon="fa fa-dot-circle-o"></li>
														<li data-icon="fa fa-download"></li>
														<li data-icon="fa fa-dribbble"></li>
														<li data-icon="fa fa-dropbox"></li>
														<li data-icon="fa fa-drupal"></li>
														<li data-icon="fa fa-edit"></li>
														<li data-icon="fa fa-eject"></li>
														<li data-icon="fa fa-ellipsis-h"></li>
														<li data-icon="fa fa-ellipsis-v"></li>
														<li data-icon="fa fa-empire"></li>
														<li data-icon="fa fa-envelope"></li>
														<li data-icon="fa fa-envelope-o"></li>
														<li data-icon="fa fa-envelope-square"></li>
														<li data-icon="fa fa-eraser"></li>
														<li data-icon="fa fa-eur"></li>
														<li data-icon="fa fa-euro"></li>
														<li data-icon="fa fa-exchange"></li>
														<li data-icon="fa fa-exclamation"></li>
														<li data-icon="fa fa-exclamation-circle"></li>
														<li data-icon="fa fa-exclamation-triangle"></li>
														<li data-icon="fa fa-expand"></li>
														<li data-icon="fa fa-external-link"></li>
														<li data-icon="fa fa-external-link-square"></li>
														<li data-icon="fa fa-eye"></li>
														<li data-icon="fa fa-eye-slash"></li>
														<li data-icon="fa fa-eyedropper"></li>
														<li data-icon="fa fa-facebook"></li>
														<li data-icon="fa fa-facebook-square"></li>
														<li data-icon="fa fa-fast-backward"></li>
														<li data-icon="fa fa-fast-forward"></li>
														<li data-icon="fa fa-fax"></li>
														<li data-icon="fa fa-female"></li>
														<li data-icon="fa fa-fighter-jet"></li>
														<li data-icon="fa fa-file"></li>
														<li data-icon="fa fa-file-archive-o"></li>
														<li data-icon="fa fa-file-audio-o"></li>
														<li data-icon="fa fa-file-code-o"></li>
														<li data-icon="fa fa-file-excel-o"></li>
														<li data-icon="fa fa-file-image-o"></li>
														<li data-icon="fa fa-file-movie-o"></li>
														<li data-icon="fa fa-file-o"></li>
														<li data-icon="fa fa-file-pdf-o"></li>
														<li data-icon="fa fa-file-photo-o"></li>
														<li data-icon="fa fa-file-picture-o"></li>
														<li data-icon="fa fa-file-powerpoint-o"></li>
														<li data-icon="fa fa-file-sound-o"></li>
														<li data-icon="fa fa-file-text"></li>
														<li data-icon="fa fa-file-text-o"></li>
														<li data-icon="fa fa-file-video-o"></li>
														<li data-icon="fa fa-file-word-o"></li>
														<li data-icon="fa fa-file-zip-o"></li>
														<li data-icon="fa fa-files-o"></li>
														<li data-icon="fa fa-film"></li>
														<li data-icon="fa fa-filter"></li>
														<li data-icon="fa fa-fire"></li>
														<li data-icon="fa fa-fire-extinguisher"></li>
														<li data-icon="fa fa-flag"></li>
														<li data-icon="fa fa-flag-checkered"></li>
														<li data-icon="fa fa-flag-o"></li>
														<li data-icon="fa fa-flash"></li>
														<li data-icon="fa fa-flask"></li>
														<li data-icon="fa fa-flickr"></li>
														<li data-icon="fa fa-floppy-o"></li>
														<li data-icon="fa fa-folder"></li>
														<li data-icon="fa fa-folder-o"></li>
														<li data-icon="fa fa-folder-open"></li>
														<li data-icon="fa fa-folder-open-o"></li>
														<li data-icon="fa fa-font"></li>
														<li data-icon="fa fa-forward"></li>
														<li data-icon="fa fa-foursquare"></li>
														<li data-icon="fa fa-frown-o"></li>
														<li data-icon="fa fa-futbol-o"></li>
														<li data-icon="fa fa-gamepad"></li>
														<li data-icon="fa fa-gavel"></li>
														<li data-icon="fa fa-gbp"></li>
														<li data-icon="fa fa-ge"></li>
														<li data-icon="fa fa-gear"></li>
														<li data-icon="fa fa-gears"></li>
														<li data-icon="fa fa-gift"></li>
														<li data-icon="fa fa-git"></li>
														<li data-icon="fa fa-git-square"></li>
														<li data-icon="fa fa-github"></li>
														<li data-icon="fa fa-github-alt"></li>
														<li data-icon="fa fa-github-square"></li>
														<li data-icon="fa fa-gittip"></li>
														<li data-icon="fa fa-glass"></li>
														<li data-icon="fa fa-globe"></li>
														<li data-icon="fa fa-google"></li>
														<li data-icon="fa fa-google-plus"></li>
														<li data-icon="fa fa-google-plus-square"></li>
														<li data-icon="fa fa-google-wallet"></li>
														<li data-icon="fa fa-graduation-cap"></li>
														<li data-icon="fa fa-group"></li>
														<li data-icon="fa fa-h-square"></li>
														<li data-icon="fa fa-hacker-news"></li>
														<li data-icon="fa fa-hand-o-down"></li>
														<li data-icon="fa fa-hand-o-left"></li>
														<li data-icon="fa fa-hand-o-right"></li>
														<li data-icon="fa fa-hand-o-up"></li>
														<li data-icon="fa fa-hdd-o"></li>
														<li data-icon="fa fa-header"></li>
														<li data-icon="fa fa-headphones"></li>
														<li data-icon="fa fa-heart"></li>
														<li data-icon="fa fa-heart-o"></li>
														<li data-icon="fa fa-history"></li>
														<li data-icon="fa fa-home"></li>
														<li data-icon="fa fa-hospital-o"></li>
														<li data-icon="fa fa-html5"></li>
														<li data-icon="fa fa-ils"></li>
														<li data-icon="fa fa-image"></li>
														<li data-icon="fa fa-inbox"></li>
														<li data-icon="fa fa-indent"></li>
														<li data-icon="fa fa-info"></li>
														<li data-icon="fa fa-info-circle"></li>
														<li data-icon="fa fa-inr"></li>
														<li data-icon="fa fa-instagram"></li>
														<li data-icon="fa fa-institution"></li>
														<li data-icon="fa fa-ioxhost"></li>
														<li data-icon="fa fa-italic"></li>
														<li data-icon="fa fa-joomla"></li>
														<li data-icon="fa fa-jpy"></li>
														<li data-icon="fa fa-jsfiddle"></li>
														<li data-icon="fa fa-key"></li>
														<li data-icon="fa fa-keyboard-o"></li>
														<li data-icon="fa fa-krw"></li>
														<li data-icon="fa fa-language"></li>
														<li data-icon="fa fa-laptop"></li>
														<li data-icon="fa fa-lastfm"></li>
														<li data-icon="fa fa-lastfm-square"></li>
														<li data-icon="fa fa-leaf"></li>
														<li data-icon="fa fa-legal"></li>
														<li data-icon="fa fa-lemon-o"></li>
														<li data-icon="fa fa-level-down"></li>
														<li data-icon="fa fa-level-up"></li>
														<li data-icon="fa fa-life-bouy"></li>
														<li data-icon="fa fa-life-buoy"></li>
														<li data-icon="fa fa-life-ring"></li>
														<li data-icon="fa fa-life-saver"></li>
														<li data-icon="fa fa-lightbulb-o"></li>
														<li data-icon="fa fa-line-chart"></li>
														<li data-icon="fa fa-link"></li>
														<li data-icon="fa fa-linkedin"></li>
														<li data-icon="fa fa-linkedin-square"></li>
														<li data-icon="fa fa-linux"></li>
														<li data-icon="fa fa-list"></li>
														<li data-icon="fa fa-list-alt"></li>
														<li data-icon="fa fa-list-ol"></li>
														<li data-icon="fa fa-list-ul"></li>
														<li data-icon="fa fa-location-arrow"></li>
														<li data-icon="fa fa-lock"></li>
														<li data-icon="fa fa-long-arrow-down"></li>
														<li data-icon="fa fa-long-arrow-left"></li>
														<li data-icon="fa fa-long-arrow-right"></li>
														<li data-icon="fa fa-long-arrow-up"></li>
														<li data-icon="fa fa-magic"></li>
														<li data-icon="fa fa-magnet"></li>
														<li data-icon="fa fa-mail-forward"></li>
														<li data-icon="fa fa-mail-reply"></li>
														<li data-icon="fa fa-mail-reply-all"></li>
														<li data-icon="fa fa-male"></li>
														<li data-icon="fa fa-map-marker"></li>
														<li data-icon="fa fa-maxcdn"></li>
														<li data-icon="fa fa-meanpath"></li>
														<li data-icon="fa fa-medkit"></li>
														<li data-icon="fa fa-meh-o"></li>
														<li data-icon="fa fa-microphone"></li>
														<li data-icon="fa fa-microphone-slash"></li>
														<li data-icon="fa fa-minus"></li>
														<li data-icon="fa fa-minus-circle"></li>
														<li data-icon="fa fa-minus-square"></li>
														<li data-icon="fa fa-minus-square-o"></li>
														<li data-icon="fa fa-mobile"></li>
														<li data-icon="fa fa-mobile-phone"></li>
														<li data-icon="fa fa-money"></li>
														<li data-icon="fa fa-moon-o"></li>
														<li data-icon="fa fa-mortar-board"></li>
														<li data-icon="fa fa-music"></li>
														<li data-icon="fa fa-navicon"></li>
														<li data-icon="fa fa-newspaper-o"></li>
														<li data-icon="fa fa-openid"></li>
														<li data-icon="fa fa-outdent"></li>
														<li data-icon="fa fa-pagelines"></li>
														<li data-icon="fa fa-paint-brush"></li>
														<li data-icon="fa fa-paper-plane"></li>
														<li data-icon="fa fa-paper-plane-o"></li>
														<li data-icon="fa fa-paperclip"></li>
														<li data-icon="fa fa-paragraph"></li>
														<li data-icon="fa fa-paste"></li>
														<li data-icon="fa fa-pause"></li>
														<li data-icon="fa fa-paw"></li>
														<li data-icon="fa fa-paypal"></li>
														<li data-icon="fa fa-pencil"></li>
														<li data-icon="fa fa-pencil-square"></li>
														<li data-icon="fa fa-pencil-square-o"></li>
														<li data-icon="fa fa-phone"></li>
														<li data-icon="fa fa-phone-square"></li>
														<li data-icon="fa fa-photo"></li>
														<li data-icon="fa fa-picture-o"></li>
														<li data-icon="fa fa-pie-chart"></li>
														<li data-icon="fa fa-pied-piper"></li>
														<li data-icon="fa fa-pied-piper-alt"></li>
														<li data-icon="fa fa-pinterest"></li>
														<li data-icon="fa fa-pinterest-square"></li>
														<li data-icon="fa fa-plane"></li>
														<li data-icon="fa fa-play"></li>
														<li data-icon="fa fa-play-circle"></li>
														<li data-icon="fa fa-play-circle-o"></li>
														<li data-icon="fa fa-plug"></li>
														<li data-icon="fa fa-plus"></li>
														<li data-icon="fa fa-plus-circle"></li>
														<li data-icon="fa fa-plus-square"></li>
														<li data-icon="fa fa-plus-square-o"></li>
														<li data-icon="fa fa-power-off"></li>
														<li data-icon="fa fa-print"></li>
														<li data-icon="fa fa-puzzle-piece"></li>
														<li data-icon="fa fa-qq"></li>
														<li data-icon="fa fa-qrcode"></li>
														<li data-icon="fa fa-question"></li>
														<li data-icon="fa fa-question-circle"></li>
														<li data-icon="fa fa-quote-left"></li>
														<li data-icon="fa fa-quote-right"></li>
														<li data-icon="fa fa-ra"></li>
														<li data-icon="fa fa-random"></li>
														<li data-icon="fa fa-rebel"></li>
														<li data-icon="fa fa-recycle"></li>
														<li data-icon="fa fa-reddit"></li>
														<li data-icon="fa fa-reddit-square"></li>
														<li data-icon="fa fa-refresh"></li>
														<li data-icon="fa fa-remove"></li>
														<li data-icon="fa fa-renren"></li>
														<li data-icon="fa fa-reorder"></li>
														<li data-icon="fa fa-repeat"></li>
														<li data-icon="fa fa-reply"></li>
														<li data-icon="fa fa-reply-all"></li>
														<li data-icon="fa fa-retweet"></li>
														<li data-icon="fa fa-rmb"></li>
														<li data-icon="fa fa-road"></li>
														<li data-icon="fa fa-rocket"></li>
														<li data-icon="fa fa-rotate-left"></li>
														<li data-icon="fa fa-rotate-right"></li>
														<li data-icon="fa fa-rouble"></li>
														<li data-icon="fa fa-rss"></li>
														<li data-icon="fa fa-rss-square"></li>
														<li data-icon="fa fa-rub"></li>
														<li data-icon="fa fa-ruble"></li>
														<li data-icon="fa fa-rupee"></li>
														<li data-icon="fa fa-save"></li>
														<li data-icon="fa fa-scissors"></li>
														<li data-icon="fa fa-search"></li>
														<li data-icon="fa fa-search-minus"></li>
														<li data-icon="fa fa-search-plus"></li>
														<li data-icon="fa fa-send"></li>
														<li data-icon="fa fa-send-o"></li>
														<li data-icon="fa fa-share"></li>
														<li data-icon="fa fa-share-alt"></li>
														<li data-icon="fa fa-share-alt-square"></li>
														<li data-icon="fa fa-share-square"></li>
														<li data-icon="fa fa-share-square-o"></li>
														<li data-icon="fa fa-shekel"></li>
														<li data-icon="fa fa-sheqel"></li>
														<li data-icon="fa fa-shield"></li>
														<li data-icon="fa fa-shopping-cart"></li>
														<li data-icon="fa fa-sign-in"></li>
														<li data-icon="fa fa-sign-out"></li>
														<li data-icon="fa fa-signal"></li>
														<li data-icon="fa fa-sitemap"></li>
														<li data-icon="fa fa-skype"></li>
														<li data-icon="fa fa-slack"></li>
														<li data-icon="fa fa-sliders"></li>
														<li data-icon="fa fa-slideshare"></li>
														<li data-icon="fa fa-smile-o"></li>
														<li data-icon="fa fa-soccer-ball-o"></li>
														<li data-icon="fa fa-sort"></li>
														<li data-icon="fa fa-sort-alpha-asc"></li>
														<li data-icon="fa fa-sort-alpha-desc"></li>
														<li data-icon="fa fa-sort-amount-asc"></li>
														<li data-icon="fa fa-sort-amount-desc"></li>
														<li data-icon="fa fa-sort-asc"></li>
														<li data-icon="fa fa-sort-desc"></li>
														<li data-icon="fa fa-sort-down"></li>
														<li data-icon="fa fa-sort-numeric-asc"></li>
														<li data-icon="fa fa-sort-numeric-desc"></li>
														<li data-icon="fa fa-sort-up"></li>
														<li data-icon="fa fa-soundcloud"></li>
														<li data-icon="fa fa-space-shuttle"></li>
														<li data-icon="fa fa-spinner"></li>
														<li data-icon="fa fa-spoon"></li>
														<li data-icon="fa fa-spotify"></li>
														<li data-icon="fa fa-square"></li>
														<li data-icon="fa fa-square-o"></li>
														<li data-icon="fa fa-stack-exchange"></li>
														<li data-icon="fa fa-stack-overflow"></li>
														<li data-icon="fa fa-star"></li>
														<li data-icon="fa fa-star-half"></li>
														<li data-icon="fa fa-star-half-empty"></li>
														<li data-icon="fa fa-star-half-full"></li>
														<li data-icon="fa fa-star-half-o"></li>
														<li data-icon="fa fa-star-o"></li>
														<li data-icon="fa fa-steam"></li>
														<li data-icon="fa fa-steam-square"></li>
														<li data-icon="fa fa-step-backward"></li>
														<li data-icon="fa fa-step-forward"></li>
														<li data-icon="fa fa-stethoscope"></li>
														<li data-icon="fa fa-stop"></li>
														<li data-icon="fa fa-strikethrough"></li>
														<li data-icon="fa fa-stumbleupon"></li>
														<li data-icon="fa fa-stumbleupon-circle"></li>
														<li data-icon="fa fa-subscript"></li>
														<li data-icon="fa fa-suitcase"></li>
														<li data-icon="fa fa-sun-o"></li>
														<li data-icon="fa fa-superscript"></li>
														<li data-icon="fa fa-support"></li>
														<li data-icon="fa fa-table"></li>
														<li data-icon="fa fa-tablet"></li>
														<li data-icon="fa fa-tachometer"></li>
														<li data-icon="fa fa-tag"></li>
														<li data-icon="fa fa-tags"></li>
														<li data-icon="fa fa-tasks"></li>
														<li data-icon="fa fa-taxi"></li>
														<li data-icon="fa fa-tencent-weibo"></li>
														<li data-icon="fa fa-terminal"></li>
														<li data-icon="fa fa-text-height"></li>
														<li data-icon="fa fa-text-width"></li>
														<li data-icon="fa fa-th"></li>
														<li data-icon="fa fa-th-large"></li>
														<li data-icon="fa fa-th-list"></li>
														<li data-icon="fa fa-thumb-tack"></li>
														<li data-icon="fa fa-thumbs-down"></li>
														<li data-icon="fa fa-thumbs-o-down"></li>
														<li data-icon="fa fa-thumbs-o-up"></li>
														<li data-icon="fa fa-thumbs-up"></li>
														<li data-icon="fa fa-ticket"></li>
														<li data-icon="fa fa-times"></li>
														<li data-icon="fa fa-times-circle"></li>
														<li data-icon="fa fa-times-circle-o"></li>
														<li data-icon="fa fa-tint"></li>
														<li data-icon="fa fa-toggle-down"></li>
														<li data-icon="fa fa-toggle-left"></li>
														<li data-icon="fa fa-toggle-off"></li>
														<li data-icon="fa fa-toggle-on"></li>
														<li data-icon="fa fa-toggle-right"></li>
														<li data-icon="fa fa-toggle-up"></li>
														<li data-icon="fa fa-trash"></li>
														<li data-icon="fa fa-trash-o"></li>
														<li data-icon="fa fa-tree"></li>
														<li data-icon="fa fa-trello"></li>
														<li data-icon="fa fa-trophy"></li>
														<li data-icon="fa fa-truck"></li>
														<li data-icon="fa fa-try"></li>
														<li data-icon="fa fa-tty"></li>
														<li data-icon="fa fa-tumblr"></li>
														<li data-icon="fa fa-tumblr-square"></li>
														<li data-icon="fa fa-turkish-lira"></li>
														<li data-icon="fa fa-twitch"></li>
														<li data-icon="fa fa-twitter"></li>
														<li data-icon="fa fa-twitter-square"></li>
														<li data-icon="fa fa-umbrella"></li>
														<li data-icon="fa fa-underline"></li>
														<li data-icon="fa fa-undo"></li>
														<li data-icon="fa fa-university"></li>
														<li data-icon="fa fa-unlink"></li>
														<li data-icon="fa fa-unlock"></li>
														<li data-icon="fa fa-unlock-alt"></li>
														<li data-icon="fa fa-unsorted"></li>
														<li data-icon="fa fa-upload"></li>
														<li data-icon="fa fa-usd"></li>
														<li data-icon="fa fa-user"></li>
														<li data-icon="fa fa-user-md"></li>
														<li data-icon="fa fa-users"></li>
														<li data-icon="fa fa-video-camera"></li>
														<li data-icon="fa fa-vimeo-square"></li>
														<li data-icon="fa fa-vine"></li>
														<li data-icon="fa fa-vk"></li>
														<li data-icon="fa fa-volume-down"></li>
														<li data-icon="fa fa-volume-off"></li>
														<li data-icon="fa fa-volume-up"></li>
														<li data-icon="fa fa-warning"></li>
														<li data-icon="fa fa-wechat"></li>
														<li data-icon="fa fa-weibo"></li>
														<li data-icon="fa fa-weixin"></li>
														<li data-icon="fa fa-wheelchair"></li>
														<li data-icon="fa fa-wifi"></li>
														<li data-icon="fa fa-windows"></li>
														<li data-icon="fa fa-won"></li>
														<li data-icon="fa fa-wordpress"></li>
														<li data-icon="fa fa-wrench"></li>
														<li data-icon="fa fa-xing"></li>
														<li data-icon="fa fa-xing-square"></li>
														<li data-icon="fa fa-yahoo"></li>
														<li data-icon="fa fa-yelp"></li>
														<li data-icon="fa fa-yen"></li>
														<li data-icon="fa fa-youtube"></li>
														<li data-icon="fa fa-youtube-play"></li>
														<li data-icon="fa fa-youtube-square"></li>
													</ul>

												</div>
												<div class="nav-comp">
													<div class="icheckbox_square-grey" style="position: relative;">
														<input type="checkbox" name="zoom" class="zoom inline">
														<input type="hidden" name="zoom_items" value="{{ !empty($settings['zoom_items'])?$settings['zoom_items']:'' }}">
													</div>
													<p class="inline">ZOOM ITEM</p>
												</div>
											</div>
										</div>
										</form>
									</aside>
									<div class="main" style="min-height: 573px;">

										<div class="wrapper">
											<div class="ptc-editor">
													@if(!empty($settings['html']))
														{!! preg_replace('#<span class="fa fa-star">(.*?)</span>#', '', $settings['html']) !!}
													@else
														<div class="ptc-tables"></div>
													@endif
											</div>
										</div>
									</div>
					</div>
				</div>
			</div>
    </div>
</div>

<script>
$(document).ready(function(){

	$('#save').click(function(){
		$('aside .toggle').trigger('click');
		setTimeout(function(){
				$('input[name="html"]').val($('.ptc-editor').html());
				$('#widgetform').submit();
		})
	})
	setInterval(function(){
		var count = 1;
		var items = new Array;
		$('.ptc-table').each(function(){
			if($(this).hasClass('ptc-zoom')){
				items.push('#'+$(this).attr('id'));
			}
			count++;
		})
		$('input[name="zoom_items"]').val(items.join(','));
	}, 10);
})
</script>
@endsection
