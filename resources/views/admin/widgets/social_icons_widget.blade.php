@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<form method="post" id="widgetform" action="{{ route('widget.save') }}">
  {{csrf_field()}}
  <input type="hidden" name="id" value="{{ $widget->id }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="row">

						<div class="col-sm-12">
              <div class="d-flex justify-content-between">
  								<div class="left">
  											<div class="shortcodeplaceholder">
  													<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
  													<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
  															<i class="fa fa-copy"></i>
  													</button>
  											</div>
  								</div>
  								<div class="right m-b-20">
                    <button class="btn btn-primary waves-effect waves-light btn-lg" type="button" data-toggle="collapse" data-target="#settings" aria-expanded="false" aria-controls="Settings">
      						     Setting
      						  </button>
  									<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
  								</div>
  						</div>
              <div class="collapse" id="settings">
      					<div class="card-box">
                  <div class="form-group">
										<label for="direction">
											Direction:
										</label>
										<select data-style="btn-white" class="form-control selectpicker" id="dimension" name="direction">
											<option value="horizontal" {{ !empty($settings['direction']) && $settings['direction']=='horizontal'? 'selected':'' }}>Horizontal</option>
											<option value="vertical" {{ !empty($settings['direction']) && $settings['direction']=='vertical'? 'selected':'' }}>Vertical</option>
										</select>
									</div>
                  <div class="form-group m-t-15">
										<label for="iconstyle">
											Icon Style:
										</label>
										<select data-style="btn-white" class="form-control selectpicker" id="iconstyle" name="iconstyle">
											<option value="">Default</option>
											<option value="custom" {{ !empty($settings['iconstyle']) && $settings['iconstyle']=='custom'? 'selected':'' }}>Custom</option>
										</select>
									</div>
                  <div class="form-group m-t-15">
										<label for="iconsize">
											Icon Size:
										</label>
										<select data-style="btn-white" class="form-control selectpicker" id="iconsize" name="iconsize">
											<option value="fa-xs" {{ !empty($settings['iconsize']) && $settings['iconsize']=='fa-xs'? 'selected':'' }}>Extra Small</option>
                      <option value="fa-sm" {{ !empty($settings['iconsize']) && $settings['iconsize']=='fa-sm'? 'selected':'' }}>Small</option>
                      <option value="fa-lg" {{ !empty($settings['iconsize']) && $settings['iconsize']=='fa-lg'? 'selected':'' }}>Large</option>
                      <option value="fa-2x" {{ !empty($settings['iconsize']) && $settings['iconsize']=='fa-2x'? 'selected':'' }}>2x</option>
                      <option value="fa-3x" {{ !empty($settings['iconsize']) && $settings['iconsize']=='fa-3x'? 'selected':'' }}>3x</option>
                      <option value="fa-5x" {{ !empty($settings['iconsize']) && $settings['iconsize']=='fa-5x'? 'selected':'' }}>5x</option>
                      <option value="fa-7x" {{ !empty($settings['iconsize']) && $settings['iconsize']=='fa-7x'? 'selected':'' }}>7x</option>
                      <option value="fa-10x" {{ !empty($settings['iconsize']) && $settings['iconsize']=='fa-10x'? 'selected':'' }}>10x</option>
										</select>
									</div>
                  <div class="form-group m-t-15">
                      <label for="dimension">
                        Dimension:
                      </label>
                      <div class="row">
                          <div class="col-sm-6">
                              Width (in px): <input class="form-control" type="number" id="width" name="width"  value="{{ !empty($settings['width'])?$settings['width']:'40' }}">
                          </div>
                          <div class="col-sm-6">
                              Height (in px): <input class="form-control" type="number" id="height" name="height"  value="{{ !empty($settings['height'])?$settings['height']:'40' }}">
                          </div>
                      </div>
                  </div>
                  <div class="form-group m-t-15">
										<label for="border">
											Border:
										</label>
										<input class="form-control" type="number" id="border" name="border"  value="{{ !empty($settings['border'])?$settings['border']:'0' }}">
									</div>
									<div class="form-group">
										<label for="bordercolor">Border Color:</label>
										<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
											<input type="text" name="bordercolor" value="" class="form-control">
											<span class="input-group-btn add-on">
												<button class="btn btn-white" type="button">
													<i style="background-color:{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
												</button>
											</span>
										</div>
									</div>
                  <div class="form-group m-t-15">
										<label for="borderradius">
											Border Radius (in px):
										</label>
										<input class="form-control" type="number" id="borderradius" name="borderradius"  value="{{ !empty($settings['borderradius'])?$settings['borderradius']:'0' }}">
									</div>
                  @if(!empty($settings['iconstyle']) && $settings['iconstyle']=='custom')
									<div class="form-group">
										<label for="bgcolor">Background Color:</label>
										<div data-color-format="rgba" data-color="{{ !empty($settings['bgcolor'])? $settings['bgcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
											<input type="text" name="bgcolor" readonly="readonly" value="" class="form-control">
											<span class="input-group-btn add-on">
												<button class="btn btn-white" type="button">
													<i style="background-color:{{ !empty($settings['bgcolor'])? $settings['bgcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="form-group">
										<label for="iconcolor">Icon Color:</label>
										<div data-color-format="rgba" data-color="{{ !empty($settings['iconcolor'])? $settings['iconcolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
											<input type="text" name="iconcolor" readonly="readonly" value="" class="form-control">
											<span class="input-group-btn add-on">
												<button class="btn btn-white" type="button">
													<i style="background-color:{{ !empty($settings['iconcolor'])? $settings['iconcolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
												</button>
											</span>
										</div>
									</div>
									@endif
                </div>
              </div>
							<div class="card-box">

								<div class="repeaters">
									<a href="" class="btn btn-white btn-lg addcustom">Add Social</a>
									@if(!empty($settings['icon']) && sizeof($settings['icon'])>0)
										@foreach($settings['icon'] as $key=>$val)
											<div class="repeat row m-t-15">
												<div class="col-sm-5">
													<select data-style="btn-white" class="form-control selectpicker" name="icon[]">
														<option value="">None</option>
														<option value="fab fa-facebook-f" {{ $val=='fab fa-facebook-f'? 'selected':'' }}>Facebook</option>
														<option value="fab fa-twitter" {{ $val=='fab fa-twitter'? 'selected':'' }}>Twitter</option>
														<option value="fab fa-google" {{ $val=='fab fa-google'? 'selected':'' }}>Google</option>
														<option value="fab fa-linkedin-in" {{ $val=='fab fa-linkedin-in'? 'selected':'' }}>Linkedin</option>
														<option value="fab fa-youtube" {{ $val=='fab fa-youtube'? 'selected':'' }}>Youtube</option>
														<option value="fab fa-instagram" {{ $val=='fab fa-instagram'? 'selected':'' }}>Instagram</option>
														<option value="fab fa-pinterest" {{ $val=='fab fa-pinterest'? 'selected':'' }}>Pinterest</option>
														<option value="fab fa-snapchat-ghost" {{ $val=='fab fa-snapchat-ghost'? 'selected':'' }}>Snapchat Ghost</option>
														<option value="fab fa-skype" {{ $val=='fab fa-skype'? 'selected':'' }}>Skype</option>
														<option value="fab fa-android" {{ $val=='fab fa-android'? 'selected':'' }}>Android</option>
														<option value="fab fa-dribbble" {{ $val=='fab fa-dribbble'? 'selected':'' }}>Dribbble</option>
														<option value="fab fa-vimeo-v" {{ $val=='fab fa-vimeo-v'? 'selected':'' }}>Vimeo</option>
														<option value="fab fa-tumblr" {{ $val=='fab fa-tumblr'? 'selected':'' }}>Tumblr</option>
														<option value="fab fa-vine" {{ $val=='fab fa-vine'? 'selected':'' }}>Vine</option>
														<option value="fab fa-foursquare" {{ $val=='fab fa-foursquare'? 'selected':'' }}>Foursquare</option>
														<option value="fab fa-stumbleupon" {{ $val=='fab fa-stumbleupon'? 'selected':'' }}>Stumbleupon</option>
														<option value="fab fa-flickr" {{ $val=='fab fa-flickr'? 'selected':'' }}>Flickr</option>
														<option value="fab fa-yahoo" {{ $val=='fab fa-yahoo'? 'selected':'' }}>Yahoo</option>
														<option value="fab fa-reddit-alien" {{ $val=='fab fa-reddit-alien'? 'selected':'' }}>Reddit</option>
														<option value="fas fa-rss" {{ $val=='fas fa-rss'? 'selected':'' }}>Rss</option>
													</select>
												</div>
												<div class="col-sm-6">
													<input type="text" name="link[]" value="{{ !empty($settings['link'][$key]) ? $settings['link'][$key] : '' }}" class="form-control" placeholder="Link">
												</div>
												<div class="col-sm-1">
													<button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button>
												</div>
											</div>
										@endforeach
									@endif
									</div>



							</div>
						</div>


				</div>
			</div>
    </div>
</div>
</form>
<script>
jQuery(document).ready(function(){
	jQuery('.repeaters').on('click', '.btn-repeat-add', function(){
		jQuery(this).parents('.repeat').clone().addClass('newrow').insertAfter(jQuery(this).parents('.repeat'));
	})
	jQuery('.colorpicker-rgba').colorpicker();
	jQuery('.repeaters').on('click', '.addcustom', function(){
		var html = '<div class="repeat row m-t-15"><div class="col-sm-5"><select data-style="btn-white" class="form-control selectpicker" name="icon[]"><option value="">None</option><option value="fab fa-facebook-f">Facebook</option><option value="fab fa-twitter">Twitter</option><option value="fab fa-google">Google</option><option value="fab fa-linkedin-in">Linkedin</option><option value="fab fa-youtube">Youtube</option><option value="fab fa-instagram">Instagram</option><option value="fab fa-pinterest">Pinterest</option><option value="fab fa-snapchat-ghost">Snapchat Ghost</option><option value="fab fa-skype">Skype</option><option value="fab fa-android">Android</option><option value="fab fa-dribbble">Dribbble</option><option value="fab fa-vimeo-v">Vimeo</option><option value="fab fa-tumblr">Tumblr</option><option value="fab fa-vine">Vine</option><option value="fab fa-foursquare">Foursquare</option><option value="fab fa-stumbleupon">Stumbleupon</option><option value="fab fa-flickr">Flickr</option><option value="fab fa-yahoo">Yahoo</option><option value="fab fa-reddit-alien">Reddit</option><option value="fas fa-rss">Rss</option></select></div><div class="col-sm-6"><input type="text" name="link[]" value="" class="form-control" placeholder="Link"></div><div class="col-sm-1"><button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button></div></div>';
		jQuery(this).parents('.repeaters').append(html);
    $('.selectpicker').selectpicker();
		return false;
	})
	jQuery('.repeaters').on('click', '.btn-repeat-remove', function(){
		jQuery(this).parents('.repeat').remove();
	})
	jQuery('#save').click(function(){
		$('#widgetform').submit();
	})
})
</script>
@endsection
