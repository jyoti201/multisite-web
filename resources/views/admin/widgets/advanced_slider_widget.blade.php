@extends('layouts.app')

@section('extra')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="{{ asset('assets/private/plugins/revolution/css/jquery.ui.ruler.css') }}">
<script src="{{ asset('assets/private/plugins/revolution/js/jquery.ui.ruler.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/revolution/fonts/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/revolution/css/settings.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/revolution/css/layers.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/revolution/css/navigation.css') }}">
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/revolution/css/admin.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/plugins/revolution/css/edit_layers.css') }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://rawgit.com/tovic/color-picker/master/color-picker.min.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://rawgit.com/tovic/color-picker/master/color-picker.min.js"></script>


<style>
.revslide .radio-enabled h4{
	margin:0;
}
.revslide .slidethumbs{
	list-style: none;
    margin: 0;
    padding: 0;
		margin-bottom: 5px;
}
.revslide .slidethumbs li{
	display: inline-block;
    width: 180px;
	height:130px;
	background-size:cover;
	background-position:center;
	position:relative;
	    vertical-align: top;
			    margin-left: 0px;
}
.revslide .slidethumbs li.disabled:after{
	content: "\f05e";
    font-size: 20px;
    color: #ff0000;
    font-family: "FontAwesome";
    position: absolute;
    left: 5px;
    top: 5px;
}
.slidethumbs li.active {
    border: 7px dashed #36404a;
}
.revslide .slidethumbs li .slidecount{
	background: #000000;
    color: #ffffff;
		text-align: left;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
		padding: 5px;
}
.revslide .slidethumbs li .slidecount .opensetting{
	left: auto;
right: 5px;
top: 4px;
bottom: 0px;
color: #ffffff;
line-height: 1;
font-size: 23px;
}
.revslide .slidethumbs li .slidecount .opensetting:hover{
opacity: .7;
}
.revslide .slidethumbs li .slidecount .opensetting:after{
	display: none;
}
.revslide .slidethumbs li .slidecount .opensetting .arrow{

}
.revslide .slidethumbs li .slidecount .slidesettings{
	position: absolute;
	    top: 100%;
	    left: 0;
	    right: 0;
	    background: rgba(0,0,0,.8);
	    z-index: 999;
}
.revslide .slidethumbs li .slidecount .slidesettings a {
    position: relative;
    color: #ffffff;
    display: block;
    line-height: 1;
    font-size: 15px;
    text-align: left;
		    padding: 5px 10px;
}
.revslide .slidethumbs li .slidecount .slidesettings a:hover{
	opacity: .7;
}
.revslide .slidethumbs li .slidecount .slidesettings a:after{
	display: none;
}
.revslide .slidethumbs li img{
	max-width:100%;
}
.revslide .nav>li.active{
	border:0;
}
.revslide .editor_wrapper{
	position:relative;
	padding:0;
}
.revslide .editor{
	position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    padding: 30px;
		padding-bottom: 0;
    padding-right: 0;
}
.revslide .editor .line{
	position:absolute;
}
.revslide .ruler{
	cursor:pointer;
}
.revslide .editor .ef-ruler .stage {
    width: 100%;
    height: 100%;
	position:relative;
}
.revslide .editor_wrapper{
	height:{{ getSliderSetting('gridheight', !empty($settings['gridheight'])? $settings['gridheight']:'')+25 }}px;
		overflow: scroll;
}
.revslide .editor{
}
.revslide .buttons{
	list-style:none;
	text-align:right;
	margin:0;
}
.revslide .buttons li{
	display:inline-block;
	margin-left: -3px;
}
.revslide .buttons li button{
	font-size: 23px;
    color: #ffffff;
    display: block;
    width: 50px;
    text-align: center;
    height: 50px;
    line-height: 57px;
    background: transparent;
    border-style: none;
    border-left: 1px solid #000000;
}
.revslide .buttons li button:hover{
	background:#319cf4;
}
.revslide .tab-content{
	padding-top:20px;
}
.revslide h4{
	font-weight:600;
}
.revslide .form-control-short{
	display:inline-block;
	max-width:100px;
}
.revslide .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.revslide .switch input {display:none;}



/* Rounded sliders */
.revslide .slider.round {
  border-radius: 34px;
}

.revslide .slider.round:before {
  border-radius: 50%;
}
.revslide .tp-mask-wrap:hover{
	cursor:move;
	border:2px solid #3097D1;
	margin-top:-2px;
	margin-left:-2px;
}
.revslide .tp-mask-wrap.selected{
	margin-top:-2px;
	margin-left:-2px;
	border:2px dashed #3097D1;
}

.tab-content{
    padding:30px 0;
	padding-top:15px;
}

.revslide .slidethumbs li.new{
	border-left: 1px dashed #cccccc;
    border-top: 1px dashed #cccccc;
    border-right: 1px dashed;
}
.revslide .slidethumbs li a{
	font-size: 35px;
    color: #000000;
    text-align: center;
    line-height: 3.2;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
}
.revslide .slidethumbs li a.openslide{

}
.revslide .slidethumbs li:first-child{
	margin-left:0;
}
.revslide .inline-field{
	display: inline-block;
    margin: 5px 10px;
	position:relative;
	padding-left: 26px;
}
.revslide .inline-field label{
	margin-right: 6px;
	position: absolute;
	left: 0;
	top: 50%;
	transform: translateY(-50%);
}
.revslide .inline-field input{
	width: 70px;
	background: #cccccc;
    border-style: none;
    border-radius: 5px;
    color: #000000;
    padding: 5px 10px;
    font-family: arial;
    height: auto;
}
.revslide .inline-field select{
	width: 70px;
	padding: 6px;
    background: #cccccc;
    border-radius: 5px;
    border: 0;
    height: auto;
}
.revslide .inline-field .short{
	margin-right:2px;
}
.revslide .inline-field .form-control{
	display:inline-block;
}
.revslide .layer_controls{
    top: auto;
    left: 0;
    right: 0;
	background: #000000;
    color: #ffffff;
	padding: 5px 0;
}
.revslide .ef-ruler{
	border-top: 1px solid #cccccc;
	overflow: visible;
}
.revslide .layer_controls .btn{
	background: #ffffff;
    color: #000000;
    font-weight: 600;
}
.revslide .textlayer{
	position: absolute;
    left: 50%;
    top: 100%;
    z-index: 999;
    width: 100%;
    max-width: 300px;
    height: 95px;
    transform: translateX(-50%);
	    padding: 10px;
	display:none;
	background:#000000;
	color: #ffffff;
}
.revslide .radio-enabled .form-control{
	max-width:400px;
	margin-left:10px;
	display:inline-block;
}
.revslide .radio-enabled h4{
	display:inline-block;
	width: 240px;
}
.revslide .layer_controls ul{
	list-style:none;
	    margin-bottom: 0;
}
.revslide .layer_controls ul li{
	display:inline-block;
}
.revslide .layer_controls ul li ul{
	position: absolute;
    z-index: 999;
    background: #000000;
    padding: 0;
	border-bottom-left-radius:3px;
	border-bottom-right-radius:3px;
	display:none;
}
.revslide .layer_controls ul li:hover ul{
	display:block;
}
.revslide .layer_controls ul li ul li{
	display:block;
}
.revslide .layer_controls ul li ul li a{
	display: block;
    padding: 5px 40px;
    color: #999999;
    font-weight: bold;
	text-decoration:none;
}
.revslide .layer_controls ul li ul li a:hover{
	color:#ffffff;
}








.revslide .vertical-tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 29%;
}

/* Style the buttons that are used to open the tab content */
.revslide .vertical-tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 10px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
.revslide .vertical-tab button:hover {
    background-color: #ddd;
}

/* Create an active/current "tab button" class */
.revslide .vertical-tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.revslide .tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 35%;
    border-left: none;
	border-right:none;
	display:none;
}
.revslide .tabcontent.active{
	display:block;
}
.revslide .tabcontent ul{
	    list-style: none;
    padding: 20px 10px;
    margin: 0;
}
.revslide .selected-transitions{
	    float: left;
    padding: 0px;
    border: 1px solid #ccc;
    width: 35%;
    min-height: 300px;
}
.revslide .selected-transitions ul{
	padding: 0;
    list-style: none;
}
.revslide .selected-transitions ul li{
	background: #f1f1f1;
    padding: 10px 15px;
    border-top: 1px dashed #ffffff;
}
.revslide .frames ul{
	list-style:none;
	    padding: 0;
}
.revslide .frames ul li{
	padding:10px;
}
.revslide .frames ul li:nth-child(even){
	background:#ffffff;
}
.revslide .frames ul li:nth-child(odd){
	background:#f1f1f1;
}
.revslide .frames ul li table{
	width:100%;
}
.revslide .frames ul li table td{
	padding:2px 10px;
}
.revslide .frames ul li table td input{
	padding:5px 10px;
	width:100px;
}
.slide{
	display:none;
}
.slide.active{
	display:block;
}
.slidethumbs li.active a:after{
	content:'';
	position:absolute;
	left:0;
	right:0;
	top:0;
	bottom:0;
	background-color:rgba(0,0,0,.6);
}
.tab-content{
	box-shadow:none;
}
.source-tabs li.sourcehidden{
	display:none;
}
</style>
<script>
  $( function() {
    $( "#sortableslides" ).sortable();
    $( "#sortableslides" ).disableSelection();
  } );

function initCP(){
	var availableTags = [
		@if(!empty(getSetting('imported_font_name','styles')))
			@foreach(getSetting('imported_font_name','styles') as $val)
				"{{ $val }}",
			@endforeach
		@endif
	];
	$("input[name='layer_fontfamily']").autocomplete({
		source: availableTags,
		minLength: 0
	}).focus(function() {
			$(this).autocomplete('search', $(this).val())
	});

	    var source = document.querySelectorAll('.colorpicker');
	    for (var i = 0, j = source.length; i < j; ++i) {
	        (new CP(source[i])).on('change', function(r, g, b, a) {
	            this.source.value = this.color(r, g, b, a);
							this.source.dispatchEvent(new Event('change'));
	        });
	    }
}
</script>
@endsection

@section('content')
<form method="post" id="widgetform" action="{{ route('widget.save') }}">
	{{csrf_field()}}
	<input type="hidden" name="id" value="{{ $widget->id }}">
	<input type="hidden" name="activeslide" value="{{ !empty(\Session::get('activeslide'))?\Session::get('activeslide'):'1' }}">
<div class="container-fluid revslide">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right m-b-20">
							<button class="btn btn-primary waves-effect waves-light btn-lg" type="button" data-toggle="collapse" data-target="#settings" aria-expanded="false" aria-controls="SliderSettings">
						    Slider Setting
						  </button>
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
						</div>
				</div>



				<div class="collapse" id="settings">
				<div class="card-box">
						<ul  class="nav nav-pills">
							<li><a  class="active" href="#1c" data-toggle="tab">General</a></li>
							<li><a href="#2c" data-toggle="tab">Navigation</a></li>
						</ul>
						<div class="tab-content clearfix">
							<div class="tab-pane active" id="1c">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<h4>Dimension</h4>
											<input type="text" name="gridwidth" class="form-control form-control-short" placeholder="Width" value="{{ getSliderSetting('gridwidth', !empty($settings['gridwidth'])? $settings['gridwidth']:'') }}"> X <input type="text" name="gridheight" class="form-control form-control-short" placeholder="Height"  value="{{ getSliderSetting('gridheight', !empty($settings['gridheight'])? $settings['gridheight']:'') }}">
										</div>
										<div class="form-group">
											<h4>Slider Background Color</h4>
											<div data-color-format="rgba" data-color="{!! !empty($settings['sliderbgcolor'])? $settings['sliderbgcolor']:'rgba(255,255,255,0)' !!}" class="colorpicker-rgba input-group colorpicker-element">
												<input type="text" name="sliderbgcolor" value="" class="form-control">
												<span class="input-group-btn add-on">
													<button class="btn btn-white" type="button">
														<i style="background-color:{!! !empty($settings['sliderbgcolor'])? $settings['sliderbgcolor']:'rgba(255,255,255,0)' !!};margin-top: 2px;"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="form-group">
											<h4>Slider Type</h4>
											<select name="sliderType" class="form-control selectpicker">
												@foreach(getOptions('sliderType') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('sliderType', !empty($settings['sliderType'])? $settings['sliderType']:'')== $key ? 'selected':'' }}>{{ $option }}</option>
												@endforeach
											</select>
										</div>

										<div class="form-group">
											<h4>Slider Layout</h4>
											<select name="sliderLayout" class="form-control selectpicker">
												@foreach(getOptions('sliderLayout') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('sliderLayout', !empty($settings['sliderLayout'])? $settings['sliderLayout']:'')== $key ? 'selected':'' }} >{{ $option }}</option>
												@endforeach
											</select>
										</div>

										<div class="form-group">
											<h4>Dotted Overlay</h4>
											<select name="dottedoverlay" class="form-control selectpicker">
												@foreach(getOptions('dottedoverlay') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('dottedoverlay', !empty($settings['dottedoverlay'])? $settings['dottedoverlay']:'')== $key ? 'selected':'' }} >{{ $option }}</option>
												@endforeach
											</select>
										</div>

										<div class="form-group">
											<h4>Default Slide Duration</h4>
											<input type="text" name="delay" class="form-control" placeholder="" value="{{ getSliderSetting('delay', !empty($settings['delay'])? $settings['delay']:'') }}">
										</div>

										<div class="form-group">
											<h4>Shadow Type</h4>
											<select name="shadow" class="form-control selectpicker">
												@foreach(getOptions('shadow') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('shadow', !empty($settings['shadow'])? $settings['shadow']:'')== $key ? 'selected':'' }}>{{ $option }}</option>
												@endforeach
											</select>
										</div>

										<div class="form-group">
											<h4>Spinner</h4>
											<select name="spinner" class="form-control selectpicker">
												@foreach(getOptions('spinner') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('spinner', !empty($settings['spinner'])? $settings['spinner']:'')== $key ? 'selected':'' }}>{{ $option }}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<h4>Stop Loop</h4>
											<label class="switch">
												<input type="checkbox" name="stoploop" {{  getSliderSetting('stoploop', !empty($settings['stoploop'])? $settings['stoploop']:'off')== 'off' ? '':'checked' }}>
												<span class="slider"></span>
											</label>
										</div>

										<div class="form-group">
											<h4>Stop After Loops</h4>
											<input type="number" name="stopafterloop" class="form-control" placeholder="" value="{{ getSliderSetting('stopafterloop', !empty($settings['stopafterloop'])? $settings['stopafterloop']:'') }}">
										</div>

										<div class="form-group">
											<h4>Stop At Slide</h4>
											<input type="number" name="stopatslide" class="form-control" placeholder="" value="{{ getSliderSetting('stopatslide', !empty($settings['stopatslide'])? $settings['stopatslide']:'') }}">
										</div>

										<div class="form-group">
											<h4>Auto Height</h4>
											<label class="switch">
												<input type="checkbox" name="autoheight" {{  getSliderSetting('autoheight', !empty($settings['autoheight'])? $settings['autoheight']:'off')== 'off' ? '':'checked' }}>
												<span class="slider"></span>
											</label>
										</div>

										<div class="form-group">
											<h4>Hide Thumbs On Mobile</h4>
											<label class="switch">
												<input type="checkbox" name="hidethumbsonmobile" {{  getSliderSetting('hidethumbsonmobile', !empty($settings['hidethumbsonmobile'])? $settings['hidethumbsonmobile']:'off')== 'off' ? '':'checked' }}>
												<span class="slider"></span>
											</label>
										</div>
									</div>
								</div>

							</div>
							<div class="tab-pane" id="2c">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<h4>Enable Arrows</h4>
											<select name="enablearrow" class="form-control selectpicker">
												<option value="true" {{  getSliderSetting('enablearrow', !empty($settings['enablearrow'])? $settings['enablearrow']:'')== 'true' ? 'selected':'' }}>True</option>
												<option value="false" {{  getSliderSetting('enablearrow', !empty($settings['enablearrow'])? $settings['enablearrow']:'')== 'false' ? 'selected':'' }}>False</option>
											</select>
										</div>
										<div class="form-group">
											<h4>Arrows Style</h4>
											<select name="arrow_style" class="form-control selectpicker">
												@foreach(getOptions('arrow_style') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('arrow_style', !empty($settings['arrow_style'])? $settings['arrow_style']:'round')== $key ? 'selected':'' }}>{{ $option }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<h4>Hide Arrows on Mobile</h4>
											<select name="hideonmobile" class="form-control selectpicker">
												<option value="true" {{  getSliderSetting('hideonmobile', !empty($settings['hideonmobile'])? $settings['hideonmobile']:'')== 'true' ? 'selected':'' }}>True</option>
												<option value="false" {{  getSliderSetting('hideonmobile', !empty($settings['hideonmobile'])? $settings['hideonmobile']:'')== 'false' ? 'selected':'' }}>False</option>
											</select>
										</div>
										<div class="form-group">
											<h4>Hide Arrows Under</h4>
											<input type="text" name="hideunder" class="form-control" placeholder="" value="{{ getSliderSetting('hideunder', !empty($settings['hideunder'])? $settings['hideunder']:'') }}">
										</div>
										<div class="form-group">
											<h4>Hide Arrows On Leave</h4>
											<select name="hideonleave" class="form-control selectpicker">
												<option value="true" {{  getSliderSetting('hideonleave', !empty($settings['hideonleave'])? $settings['hideonleave']:'')== 'true' ? 'selected':'' }}>True</option>
												<option value="false" {{  getSliderSetting('hideonleave', !empty($settings['hideonleave'])? $settings['hideonleave']:'')== 'false' ? 'selected':'' }}>False</option>
											</select>
										</div>
									</div>
									<div class="col-sm-6">
										<h3>Left Arrow Position</h3>
										<div class="form-group">
											<h4>Horizontal Align</h4>
											<select name="l_halign" class="form-control selectpicker">
												@foreach(getOptions('l_halign') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('l_halign', !empty($settings['l_halign'])? $settings['l_halign']:'')== $key ? 'selected':'' }}>{{ $option }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<h4>Vertical Align</h4>
											<select name="l_valign" class="form-control selectpicker">
												@foreach(getOptions('l_valign') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('l_valign', !empty($settings['l_valign'])? $settings['l_valign']:'')== $key ? 'selected':'' }}>{{ $option }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<h4>Horizontal Offset</h4>
											<input type="text" name="l_horizoff" class="form-control" placeholder="20px" value="{{ getSliderSetting('l_horizoff', !empty($settings['l_horizoff'])? $settings['l_horizoff']:'') }}">
										</div>
										<div class="form-group">
											<h4>Vertical Offset</h4>
											<input type="text" name="l_vertoff" class="form-control" placeholder="0px" value="{{ getSliderSetting('l_vertoff', !empty($settings['l_vertoff'])? $settings['l_vertoff']:'') }}">
										</div>
										<h3 style="margin-top:30px;">Right Arrow Position</h3>
										<div class="form-group">
											<h4>Horizontal Align</h4>
											<select name="r_halign" class="form-control selectpicker">
												@foreach(getOptions('r_halign') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('r_halign', !empty($settings['r_halign'])? $settings['r_halign']:'')== $key ? 'selected':'' }}>{{ $option }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<h4>Vertical Align</h4>
											<select name="r_valign" class="form-control selectpicker">
												@foreach(getOptions('r_valign') as $key=>$option)
													<option value="{{ $key }}" {{  getSliderSetting('r_valign', !empty($settings['r_valign'])? $settings['r_valign']:'')== $key ? 'selected':'' }}>{{ $option }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<h4>Horizontal Offset</h4>
											<input type="text" name="r_horizoff" class="form-control" placeholder="20px" value="{{ getSliderSetting('r_horizoff', !empty($settings['r_horizoff'])? $settings['r_horizoff']:'') }}">
										</div>
										<div class="form-group">
											<h4>Vertical Offset</h4>
											<input type="text" name="r_vertoff" class="form-control" placeholder="0px" value="{{ getSliderSetting('r_vertoff', !empty($settings['r_vertoff'])? $settings['r_vertoff']:'') }}">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>




				<div class="card-box">

					<div class="row">
						<div class="col-sm-12">
							<div id="allslides" class="slides block">
								<ul class="slidethumbs" id="sortableslides">
									@if(!empty($settings['slideorder']))
										@foreach($settings['slideorder'] as $key=>$slide)
										<li class="{{ $key==0?'active':'' }} {{ !empty($settings['slide_'.$slide]['disabled']) && $settings['slide_'.$slide]['disabled']=='on'? 'disabled':'' }}" style="background-image:url('{!! !empty($settings['slide_'.$slide]['slider_bgimage'])? $settings['slide_'.$slide]['slider_bgimage']:'' !!}')">
											<a href="#{{ $slide }}" class="openslide" id="slide_link_{{ $slide }}" rel="slide_{{ $slide }}"></a>
											<div class="slidecount">#Slide{{ $key+1 }} <a href="#" class="opensetting" data-toggle="collapse" data-target="#slidesettings{{ $slide }}"><i class="far fa-caret-square-down"></i></a><div class="slidesettings collapse" id="slidesettings{{ $slide }}"><a href="#" class="duplicateslide" data-slide="{{ $slide }}">Duplicate</a><a href="#" class="disableslide" data-slide="{{ $slide }}">{{ !empty($settings['slide_'.$slide]['disabled']) && $settings['slide_'.$slide]['disabled']=='on'? 'Enable':'Disable' }}</a></div></div>
											<input type="hidden" name="slideorder[]" value="{{ $slide }}">
											<div class="hidden">
											<input type="checkbox" class="disablecheck" name="slide_{{ $slide }}[disabled]" {{ !empty($settings['slide_'.$slide]['disabled']) && $settings['slide_'.$slide]['disabled']=='on'? 'checked':'' }}>
											</div>
										</li>
										@endforeach
									@endif
								</ul>
								<ul class="slidethumbs">
									<li class="new">
										<a href="#" id="newslide"><span class="far fa-plus-square"></span></a>
										<div class="slidecount">
											New Slide
										</div>
									</li>
								</ul>
								<input type="hidden" id="slides" name="slides" value="{{ !empty($settings['slides'])?$settings['slides']:'0' }}">
							</div>
						</div>
					</div>
				</div>




				@if(!empty($settings['slideorder']))
					@foreach($settings['slideorder'] as $key=>$slide)
						<div class="slide {{ $slide==1?'active':'' }}" id="slide_{{ $slide }}">

						<div class="card-box">
								<div class="row">
									<div class="col-sm-12">
										<div class="settings block">
											<div style="margin-bottom:10px;padding:0;">
													<ul  class="nav nav-pills">
														<li><a href="#slide_{{ $slide }}1a" data-toggle="tab"><i style="height:45px;line-height:3" class="rs-mini-layer-icon eg-icon-picture-1 rs-toolbar-icon active"></i> Main Background</a></li>
														<li><a href="#slide_{{ $slide }}3a" data-toggle="tab"><i style="height:45px" class="rs-mini-layer-icon rs-icon-chooser-3 rs-toolbar-icon"></i> Slide Animation</a></li>
													</ul>
											</div>
											<div class="tab-content clearfix">
												<div class="tab-pane active" id="slide_{{ $slide }}1a">
													<ul class="nav source-tabs nav-tabs tabs">
														<li class="tab">
															<a href="#slide_{{ $slide }}source" data-toggle="tab" aria-expanded="false" class="active">
																<span class="visible-xs"><i class="fa fa-home"></i></span>
																<span class="hidden-xs">Source</span>
															</a>
														</li>
														<li class="tab sourcehidden" {!! !empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='bgimage'? 'style="display:block"':'' !!}>
															<a href="#slide_{{ $slide }}sourcesettings" data-toggle="tab" aria-expanded="false" class="">
																<span class="visible-xs"><i class="fa fa-user"></i></span>
																<span class="hidden-xs">Source Settings</span>
															</a>
														</li>
														<li class="tab sourcehidden" {!! !empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='bgimage'? 'style="display:block"':'' !!}>
															<a href="#slide_{{ $slide }}kenburn" data-toggle="tab" aria-expanded="false" class="">
																<span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
																<span class="hidden-xs">Kenburn</span>
															</a>
														</li>
													</ul>
													<div class="tab-content">
														<div class="tab-pane active" id="slide_{{ $slide }}source">
															<div class="form-group radio-enabled">
																<div class="radio">
																<input type="radio" id="{{ $slide }}_bgimage" name="slide_{{ $slide }}[slider_bgtype]" value="bgimage" {{ !empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='bgimage'? 'checked':'' }}>
																<label for="{{ $slide }}_bgimage">Background Image</label>
																</div>
																<div style="padding-left: 25px;"><a href="#" class="fileupload btn btn-primary waves-effect waves-light" data-toggle="custommodal" data-target="#media-select"><i class="ion-upload m-r-5"></i>Add Image</a><input type="text" name="slide_{{ $slide }}[slider_bgimage]" class="form-control" placeholder="" value="{{ getSliderSetting('bgimage', !empty($settings['slide_'.$slide]['slider_bgimage'])? $settings['slide_'.$slide]['slider_bgimage']:'') }}"></div>
															</div>
															<div class="form-group radio-enabled">
																<div class="radio"><input type="radio" id="{{ $slide }}_trans" name="slide_{{ $slide }}[slider_bgtype]" value="transparent" {{ !empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='transparent'? 'checked':'' }}><label for="{{ $slide }}_trans">Transparent</label></div>
															</div>
															<div class="form-group radio-enabled">
																<div class="radio"><input type="radio" id="{{ $slide }}_colored" name="slide_{{ $slide }}[slider_bgtype]" value="colored" {{ !empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='colored'? 'checked':'' }}><label for="{{ $slide }}_colored">Colored</label></div>
																<div style="padding-left: 15px;    max-width: 400px;">
																<div data-color-format="rgba" data-color="{{ !empty($settings['slide_'.$slide]['slider_color'])?$settings['slide_'.$slide]['slider_color']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
																	<input type="text" name="slide_{{ $slide }}[slider_color]" value="{{ !empty($settings['slide_'.$slide]['slider_color'])?$settings['slide_'.$slide]['slider_color']:'rgba(255,255,255,1)' }}" class="form-control">
																	<span class="input-group-btn add-on">
																		<button class="btn btn-white" type="button">
																			<i style="background-color:{{ !empty($settings['slide_'.$slide]['slider_color'])?$settings['slide_'.$slide]['slider_color']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
																		</button>
																	</span>
																</div>
																</div>
															</div>
														</div>
														<div class="tab-pane" id="slide_{{ $slide }}sourcesettings">
															<div class="form-group">
																<label>Background Fit</label>
																<select  data-style="btn-white" name="slide_{{ $slide }}[bg_fit]" class="form-control selectpicker">
																	<option value="cover" {{ !empty($settings['slide_'.$slide]['bg_fit']) && $settings['slide_'.$slide]['bg_fit']=='cover'? 'selected':'' }}>Cover</option>
																	<option value="contain" {{ !empty($settings['slide_'.$slide]['bg_fit']) && $settings['slide_'.$slide]['bg_fit']=='contain'? 'selected':'' }}>Contain</option>
																	<option value="normal" {{ !empty($settings['slide_'.$slide]['bg_fit']) && $settings['slide_'.$slide]['bg_fit']=='normal'? 'selected':'' }}>Normal</option>
																</select>
															</div>
															<div class="form-group">
																<label>Background Position</label>
																<select  data-style="btn-white" name="slide_{{ $slide }}[bg_position]" class="form-control selectpicker">
																	<option value="center center" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='center center'? 'selected':'' }}>center center</option>
																	<option value="center top" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='center top'? 'selected':'' }}>center top</option>
																	<option value="center right" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='center right'? 'selected':'' }}>center right</option>
																	<option value="center bottom" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='center bottom'? 'selected':'' }}>center bottom</option>
																	<option value="left top" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='left top'? 'selected':'' }}>left top</option>
																	<option value="left center" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='left center'? 'selected':'' }}>left center</option>
																	<option value="left bottom" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='left bottom'? 'selected':'' }}>left bottom</option>
																	<option value="right top" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='right top'? 'selected':'' }}>right top</option>
																	<option value="right center" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='right center'? 'selected':'' }}>right center</option>
																	<option value="right bottom" {{ !empty($settings['slide_'.$slide]['bg_position']) && $settings['slide_'.$slide]['bg_position']=='right bottom'? 'selected':'' }}>right bottom</option>
																</select>
															</div>
															<div class="form-group">
																<label>Background Repeat</label>
																<select data-style="btn-white" name="slide_{{ $slide }}[bg_repeat]" class="form-control selectpicker">
																	<option value="no-repeat" {{ !empty($settings['slide_'.$slide]['bg_repeat']) && $settings['slide_'.$slide]['bg_repeat']=='no-repeat'? 'selected':'' }}>no-repeat</option>
																	<option value="repeat" {{ !empty($settings['slide_'.$slide]['bg_repeat']) && $settings['slide_'.$slide]['bg_repeat']=='repeat'? 'selected':'' }}>repeat</option>
																	<option value="repeat-x" {{ !empty($settings['slide_'.$slide]['bg_repeat']) && $settings['slide_'.$slide]['bg_repeat']=='repeat-x'? 'selected':'' }}>repeat-x</option>
																	<option value="repeat-y" {{ !empty($settings['slide_'.$slide]['bg_repeat']) && $settings['slide_'.$slide]['bg_repeat']=='repeat-y'? 'selected':'' }}>repeat-y</option>
																</select>
															</div>
															<div class="form-group">
																<label>Background Filter</label>
																<select  data-style="btn-white" name="slide_{{ $slide }}[filter]" class="selectpicker form-control">
																	<option value="">None</option>
																	<option value="overlay" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='overlay'? 'selected':'' }}>Overlay</option>
																	<option value="_1977" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='_1977'? 'selected':'' }}>1977</option>
																	<option value="aden" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='aden'? 'selected':'' }}>aden</option>
																	<option value="brooklyn" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='brooklyn'? 'selected':'' }}>brooklyn</option>
																	<option value="clarendon" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='clarendon'? 'selected':'' }}>clarendon</option>
																	<option value="earlybird" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='earlybird'? 'selected':'' }}>earlybird</option>
																	<option value="gingham" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='gingham'? 'selected':'' }}>gingham</option>
																	<option value="hudson" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='hudson'? 'selected':'' }}>hudson</option>
																	<option value="inkwell" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='inkwell'? 'selected':'' }}>inkwell</option>
																	<option value="lark" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='lark'? 'selected':'' }}>lark</option>
																	<option value="lofi" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='lofi'? 'selected':'' }}>lofi</option>
																	<option value="mayfair" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='mayfair'? 'selected':'' }}>mayfair</option>
																	<option value="moon" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='moon'? 'selected':'' }}>moon</option>
																	<option value="nashville" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='nashville'? 'selected':'' }}>nashville</option>
																	<option value="perpetua" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='perpetua'? 'selected':'' }}>perpetua</option>
																	<option value="reyes" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='reyes'? 'selected':'' }}>reyes</option>
																	<option value="rise" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='rise'? 'selected':'' }}>rise</option>
																	<option value="slumber" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='slumber'? 'selected':'' }}>slumber</option>
																	<option value="toaster" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='toaster'? 'selected':'' }}>toaster</option>
																	<option value="walden" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='walden'? 'selected':'' }}>walden</option>
																	<option value="willow" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='willow'? 'selected':'' }}>willow</option>
																	<option value="xpro2" {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='xpro2'? 'selected':'' }}>xpro2</option>
																</select>
															</div>
															<div class="form-group collapse {{ !empty($settings['slide_'.$slide]['filter']) && $settings['slide_'.$slide]['filter']=='overlay'?'show':'' }}">
																<label>Background Overlay</label>
																<label style="display:block;">From</label>
																<div data-color-format="rgba" data-color="{{ !empty($settings['slide_'.$slide]['bg_overlay_from'])?$settings['slide_'.$slide]['bg_overlay_from']:'rgba(0,0,0,.5)' }}" class="colorpicker-rgba input-group">
																	<input type="text" name="slide_{{ $slide }}[bg_overlay_from]" value="{{ !empty($settings['slide_'.$slide]['bg_overlay_from'])?$settings['slide_'.$slide]['bg_overlay_from']:'rgba(0,0,0,.5)' }}" class="form-control">
																	<span class="input-group-btn add-on">
																		<button class="btn btn-white" type="button">
																			<i style="background-color:{{ !empty($settings['slide_'.$slide]['bg_overlay_from'])?$settings['slide_'.$slide]['bg_overlay_from']:'rgba(0,0,0,.5)' }};margin-top: 2px;"></i>
																		</button>
																	</span>
																</div>
																<label>To</label>
																<div data-color-format="rgba" data-color="{{ !empty($settings['slide_'.$slide]['bg_overlay_to'])?$settings['slide_'.$slide]['bg_overlay_to']:'rgba(0,0,0,.5)' }}" class="colorpicker-rgba input-group">
																	<input type="text" name="slide_{{ $slide }}[bg_overlay_to]" value="{{ !empty($settings['slide_'.$slide]['bg_overlay_to'])?$settings['slide_'.$slide]['bg_overlay_to']:'rgba(0,0,0,.5)' }}" class="form-control">
																	<span class="input-group-btn add-on">
																		<button class="btn btn-white" type="button">
																			<i style="background-color:{{ !empty($settings['slide_'.$slide]['bg_overlay_to'])?$settings['slide_'.$slide]['bg_overlay_to']:'rgba(0,0,0,.5)' }};margin-top: 2px;"></i>
																		</button>
																	</span>
																</div>
																<label>Angle</label>
																<input type="number" class="form-control" name="slide_{{ $slide }}[bg_overlay_angle]" min="0" value="{{ !empty($settings['slide_'.$slide]['bg_overlay_angle'])?$settings['slide_'.$slide]['bg_overlay_angle']:'0' }}">
															</div>
														</div>
														<div class="tab-pane" id="slide_{{ $slide }}kenburn">
																<div class="form-group">
																	<label style="vertical-align: middle;margin-right:10px">Ken Burns / Pan Zoom:</label>
																	<label class="switch" style="vertical-align: middle;">
																	  <input type="checkbox" id="{{ $slide }}_kenburn" name="slide_{{ $slide }}[kenburn]" {{ !empty($settings['slide_'.$slide]['kenburn']) && $settings['slide_'.$slide]['kenburn']=='on'? 'checked':'' }}>
																	  <span class="slider"></span>
																	</label>
																</div>
																<div id="{{ $slide }}_kenburn_slide" class="collapse {{ !empty($settings['slide_'.$slide]['kenburn']) && $settings['slide_'.$slide]['kenburn']=='on'? 'show':'' }}">
																	<div class="form-group row">
																		<label class="col-md-2">Scale: (in %):</label>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_scale_from]" class="form-control" value="{{ !empty($settings['slide_'.$slide]['kenburn_scale_from'])?$settings['slide_'.$slide]['kenburn_scale_from']:'100' }}" placeholder="From">
																		</div>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_scale_to]" class="form-control" placeholder="To" value="{{ !empty($settings['slide_'.$slide]['kenburn_scale_to'])?$settings['slide_'.$slide]['kenburn_scale_to']:'100' }}">
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-md-2">Horizontal Offsets (+/-):</label>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_h_offset_from]" class="form-control" value="{{ !empty($settings['slide_'.$slide]['kenburn_h_offset_from'])?$settings['slide_'.$slide]['kenburn_h_offset_from']:'0' }}" placeholder="From">
																		</div>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_h_offset_to]" value="{{ !empty($settings['slide_'.$slide]['kenburn_h_offset_to'])?$settings['slide_'.$slide]['kenburn_h_offset_to']:'0' }}" class="form-control" placeholder="To">
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-md-2">Vertical Offsets (+/-):</label>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_v_offset_from]" class="form-control" value="{{ !empty($settings['slide_'.$slide]['kenburn_v_offset_from'])?$settings['slide_'.$slide]['kenburn_v_offset_from']:'0' }}" placeholder="From">
																		</div>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_v_offset_to]" class="form-control" value="{{ !empty($settings['slide_'.$slide]['kenburn_v_offset_to'])?$settings['slide_'.$slide]['kenburn_v_offset_to']:'0' }}" placeholder="To">
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-md-2">Rotation:</label>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_rotation_from]" class="form-control" placeholder="From" value="{{ !empty($settings['slide_'.$slide]['kenburn_rotation_from'])?$settings['slide_'.$slide]['kenburn_rotation_from']:'0' }}">
																		</div>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_rotation_to]" value="{{ !empty($settings['slide_'.$slide]['kenburn_rotation_to'])?$settings['slide_'.$slide]['kenburn_rotation_to']:'0' }}" class="form-control" placeholder="To">
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-md-2">Blur Filter:</label>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_blur_from]" class="form-control" placeholder="From" value="{{ !empty($settings['slide_'.$slide]['kenburn_blur_from'])?$settings['slide_'.$slide]['kenburn_blur_from']:'0' }}">
																		</div>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_blur_to]" class="form-control" value="{{ !empty($settings['slide_'.$slide]['kenburn_blur_to'])?$settings['slide_'.$slide]['kenburn_blur_to']:'0' }}" placeholder="To">
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-md-2">Easing:</label>
																		<div class="col-md-3">
																		<select data-style="btn-white" name="slide_{{ $slide }}[kenburn_easing]" id="{{ $slide }}_kenburn_easing" class="form-control selectpicker" style="display:inline-block;max-width:130px;margin-right:5px">
																			<option selected="selected" value="Linear.easeNone" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Linear.easeNone'?'selected':'' !!}>Linear.easeNone</option>
																			<option value="Power0.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Linear.easeNone'?'selected':'' !!}>Power0.easeIn  (linear)</option>
																			<option value="Power0.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power0.easeInOut'?'selected':'' !!}>Power0.easeInOut  (linear)</option>
																			<option value="Power0.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power0.easeOut'?'selected':'' !!}>Power0.easeOut  (linear)</option>
																			<option value="Power1.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power1.easeIn'?'selected':'' !!}>Power1.easeIn</option>
																			<option value="Power1.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power1.easeInOut'?'selected':'' !!}>Power1.easeInOut</option>
																			<option value="Power1.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power1.easeOut'?'selected':'' !!}>Power1.easeOut</option>
																			<option value="Power2.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power2.easeIn'?'selected':'' !!}>Power2.easeIn</option>
																			<option value="Power2.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power2.easeInOut'?'selected':'' !!}>Power2.easeInOut</option>
																			<option value="Power2.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power2.easeOut'?'selected':'' !!}>Power2.easeOut</option>
																			<option value="Power3.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power3.easeIn'?'selected':'' !!}>Power3.easeIn</option>
																			<option value="Power3.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power3.easeInOut'?'selected':'' !!}>Power3.easeInOut</option>
																			<option value="Power3.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power3.easeOut'?'selected':'' !!}>Power3.easeOut</option>
																			<option value="Power4.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power4.easeIn'?'selected':'' !!}>Power4.easeIn</option>
																			<option value="Power4.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power4.easeInOut'?'selected':'' !!}>Power4.easeInOut</option>
																			<option value="Power4.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Power4.easeOut'?'selected':'' !!}>Power4.easeOut</option>
																			<option value="Back.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Back.easeIn'?'selected':'' !!}>Back.easeIn</option>
																			<option value="Back.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Back.easeInOut'?'selected':'' !!}>Back.easeInOut</option>
																			<option value="Back.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Back.easeOut'?'selected':'' !!}>Back.easeOut</option>
																			<option value="Bounce.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Bounce.easeIn'?'selected':'' !!}>Bounce.easeIn</option>
																			<option value="Bounce.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Bounce.easeInOut'?'selected':'' !!}>Bounce.easeInOut</option>
																			<option value="Bounce.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Bounce.easeOut'?'selected':'' !!}>Bounce.easeOut</option>
																			<option value="Circ.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Circ.easeIn'?'selected':'' !!}>Circ.easeIn</option>
																			<option value="Circ.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Circ.easeInOut'?'selected':'' !!}>Circ.easeInOut</option>
																			<option value="Circ.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Circ.easeOut'?'selected':'' !!}>Circ.easeOut</option>
																			<option value="Elastic.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Elastic.easeIn'?'selected':'' !!}>Elastic.easeIn</option>
																			<option value="Elastic.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Elastic.easeInOut'?'selected':'' !!}>Elastic.easeInOut</option>
																			<option value="Elastic.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Elastic.easeOut'?'selected':'' !!}>Elastic.easeOut</option>
																			<option value="Expo.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Expo.easeIn'?'selected':'' !!}>Expo.easeIn</option>
																			<option value="Expo.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Expo.easeInOut'?'selected':'' !!}>Expo.easeInOut</option>
																			<option value="Expo.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Expo.easeOut'?'selected':'' !!}>Expo.easeOut</option>
																			<option value="Sine.easeIn" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Sine.easeIn'?'selected':'' !!}>Sine.easeIn</option>
																			<option value="Sine.easeInOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Sine.easeInOut'?'selected':'' !!}>Sine.easeInOut</option>
																			<option value="Sine.easeOut" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='Sine.easeOut'?'selected':'' !!}>Sine.easeOut</option>
																			<option value="SlowMo.ease" {!! !empty($settings['slide_'.$slide]['kenburn_easing']) && $settings['slide_'.$slide]['kenburn_easing']=='SlowMo.ease'?'selected':'' !!}>SlowMo.ease</option>
																		</select>
																		</div>
																	</div>
																	<div class="form-group row">
																		<label class="col-md-2">Duration (in ms):</label>
																		<div class="col-md-3">
																		<input type="text" name="slide_{{ $slide }}[kenburn_duration]" class="form-control" placeholder="" value="{{ !empty($settings['slide_'.$slide]['kenburn_duration'])?$settings['slide_'.$slide]['kenburn_duration']:'10000' }}">
																		</div>
																	</div>
																</div>
														</div>
													</div>


												</div>
												<div class="tab-pane" id="slide_{{ $slide }}3a">






													<div class="vertical-tab">
														@foreach(getOptions('slideanimation') as $key=>$val)
															<button class="tablinks " rel='slide_{{ $slide }}_{{ strtolower(str_replace(' ', '', $key)) }}'>{{ $key }}</button>
														@endforeach
													</div>
													@foreach(getOptions('slideanimation') as $key=>$val)
													<div id="slide_{{ $slide }}_{{ strtolower(str_replace(' ', '', $key)) }}" class="tabcontent {{ strtolower(str_replace(' ', '', $key))=='basics'?'active':'' }}">
														<ul>
														@foreach($val as $key=>$val)
															<li>
															<div class="checkbox">
															<input type="checkbox" name="transitions[]" id="slide_{{ $slide }}_{{ $key }}" value="{{ $key }}">
															<label for="slide_{{ $slide }}_{{ $key }}">
																{{ $val }}
															</label>
															</div>
															</li>
														@endforeach
														</ul>
													</div>
													@endforeach
													<div class="selected-transitions">
														<ul>

														</ul>
														<input type="hidden" name="slide_{{ $slide }}[slide_transitions]" value="{{ !empty($settings['slide_'.$slide]['slide_transitions'])?$settings['slide_'.$slide]['slide_transitions']:'fade' }}">
													</div>
													<script>
													$(document).ready(function(){
														$('input[name="slide_{{ $slide }}[slider_bgtype]"]').click(function(){
															if($('input[name="slide_{{ $slide }}[slider_bgtype]"]:checked').val()=='bgimage'){
																$('.source-tabs li.sourcehidden').show();
															}else{
																$('.source-tabs li.sourcehidden').hide();
															}
														})
														$('#{{ $slide }}_kenburn').click(function(){
															if($(this).is(':checked')){
																$('#{{ $slide }}_kenburn_slide').addClass('show');
															}else{
																$('#{{ $slide }}_kenburn_slide').removeClass('show');
															}
														})
														setTimeout(function(){
															var selected = $('input[name="slide_{{ $slide }}[slide_transitions]"]').val().split(',');
															var html = '';
															for(var i=0;i<selected.length;i++){
																html = html+"<li>"+selected[i]+"</li>";
																$('#slide_{{ $slide }}_'+selected[i]).prop('checked', true);
															}
															$('#slide_{{ $slide }} .selected-transitions ul').html(html);

														}, 200);
														$('#slide_{{ $slide }} .tablinks').click(function(){
															var i, tabcontent, tablinks;
															$('#slide_{{ $slide }} .tablinks').removeClass('active');
															$('#slide_{{ $slide }} .tabcontent').hide();
															$(this).addClass('active');
															$('#'+$(this).attr('rel')).show();
															return false;
														})
														$('#slide_{{ $slide }} input[name="transitions[]"]').click(function(){
															$('#slide_{{ $slide }} input[name="slide_{{ $slide }}[slide_transitions]"]').val($('#slide_{{ $slide }} input[name="transitions[]"]:checked').map(function() { return this.value;}).get().join(','));
															setTimeout(function(){
																var selected = $('#slide_{{ $slide }} input[name="slide_{{ $slide }}[slide_transitions]"]').val().split(',');
																var html = '';
																for(var i=0;i<selected.length;i++){
																	html = html+"<li>"+selected[i]+"</li>";
																}
																$('#slide_{{ $slide }} .selected-transitions ul').html(html);
															}, 200);
														})
													})
													</script>







												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-box">
							<div class="row">
								<div class="col-sm-12">
									<div class="styles block" id="styles_{{ $slide }}">
										<ul  class="nav nav-pills">
											<li><a  href="#slide_{{ $slide }}_1b" data-toggle="tab"><i style="height:45px" class="active rs-mini-layer-icon rs-icon-droplet rs-toolbar-icon"></i> Style</a></li>
											<li><a href="#slide_{{ $slide }}_2b" data-toggle="tab"><i style="height:45px" class="rs-mini-layer-icon rs-icon-edit-basic rs-toolbar-icon"></i> Attributes</a></li>
											<li><a href="#slide_{{ $slide }}_3b" data-toggle="tab"><i style="height:45px" class="rs-mini-layer-icon rs-icon-chooser-2 rs-toolbar-icon"></i> Animation</a></li>
											<li><a href="#slide_{{ $slide }}_4b" data-toggle="tab"><i style="height:45px; font-size:16px;line-height:3;" class="rs-mini-layer-icon eg-icon-link rs-toolbar-icon"></i> Action</a></li>
										</ul>
										<div class="tab-content clearfix">
											<div class="tab-pane active" id="slide_{{ $slide }}_1b">
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon rs-icon-fontsize rs-toolbar-icon tipsy_enabled_top" style="margin-right:6px" original-title="Font Size (px)"></i></label><input type="text" class="form-control" name="layer_fontsize">
												</div>
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon rs-icon-lineheight rs-toolbar-icon tipsy_enabled_top" style="margin-right:11px" original-title="Line Height (px)"></i></label><input type="text" class="form-control" name="layer_lineheight">
												</div>
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon rs-icon-color rs-toolbar-icon tipsy_enabled_top" original-title="Font Color"></i></label><input type="text" class="form-control colorpicker" name="layer_color" style="width:100px">
												</div>
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon rs-icon-fontweight rs-toolbar-icon tipsy_enabled_top" original-title="Font Weight"></i></label>
													<select style="width:61px" class="form-control" name="layer_fontweight">
														@foreach(getOptions('fontweight') as $key=>$option)
															<option value="{{ $key }}">{{ $option }}</option>
														@endforeach
													</select>
												</div>
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon rs-icon-fontfamily rs-toolbar-icon tipsy_enabled_top" style="margin-right:10px" original-title="Font Family"></i></label><input type="text" class="form-control" name="layer_fontfamily" style="width:100px">
												</div>
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon rs-icon-bg rs-toolbar-icon tipsy_enabled_top" style="margin-right:10px" original-title="Background Color"></i></label><input type="text" class="form-control colorpicker" name="layer_bgcolor">
												</div>
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon rs-icon-padding rs-toolbar-icon tipsy_enabled_top" style="margin-right:10px" original-title="Padding"></i></label><input type="text" class="short form-control" name="layer_pleft" placeholder="Left"><input class="short form-control" type="text" name="layer_pright" placeholder="Right"><input class="short form-control" type="text" name="layer_ptop" placeholder="Top"><input class="short form-control" type="text" name="layer_pbottom" placeholder="Bottom">
												</div>
												<hr />
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon eg-icon-arrow-combo rs-toolbar-icon" style="margin-right:3px;transform: rotateZ(90deg);-webkit-transform: rotateZ(90deg);"></i></label>
													<select name="layer_halign" class="form-control">
														@foreach(getOptions('halign') as $key=>$option)
															<option value="{{ $key }}">{{ $option }}</option>
														@endforeach
													</select>
												</div>
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon rs-icon-xoffset rs-toolbar-icon tipsy_enabled_top" style="margin-right:8px" original-title="Horizontal Offset from Aligned Position (px)"></i></label><input type="text" name="layer_hoffset" class="form-control">
												</div>
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon eg-icon-arrow-combo rs-toolbar-icon" style="margin-right:3px"></i></label>
													<select name="layer_valign" class="form-control">
														@foreach(getOptions('valign') as $key=>$option)
															<option value="{{ $key }}">{{ $option }}</option>
														@endforeach
													</select>
												</div>
												<div class="inline-field">
													<label><i class="rs-mini-layer-icon rs-icon-yoffset rs-toolbar-icon tipsy_enabled_top" style="margin-right:4px" original-title="Vertical Offset from Aligned Position (px)"></i></label><input type="text" name="layer_voffset" class="form-control">
												</div>
											</div>
											<div class="tab-pane" id="slide_{{ $slide }}_2b">
												<div class="inline-field">
													<label>Rel</label><input type="text" name="layer_rel" class="form-control">
												</div>
												<div class="inline-field">
													<label>Title</label><input type="text" name="layer_title" class="form-control">
												</div>
												<div class="inline-field">
													<label>Class</label><input type="text" name="layer_class" class="form-control">
												</div>
											</div>
											<div class="tab-pane" id="slide_{{ $slide }}_3b">
												<div class="inline-field">
													<label>Animation</label>
													<select name="layer_animation" class="form-control" style="width: 220px;">
														@foreach(getOptions('layeranimation') as $key=>$option)
															<option value="{{ $key }}">{{ $option['label'] }}</option>
														@endforeach
													</select>
												</div>
												<div class="inline-field">
													<label>Animation Speed (in)</label>
													<input type="text" name="layer_animation_speed_in" class="form-control">
												</div>
												<div class="inline-field">
													<label>Animation Speed (out)</label>
													<input type="text" name="layer_animation_speed_out" class="form-control">
												</div>
											</div>
											<div class="tab-pane" id="slide_{{ $slide }}_4b">
												<div class="inline-field">
													<label>Target</label>
													<select name="layer_target" class="form-control" style="width: 100px;">
														<option value="_self">Self</option>
														<option value="_blank">New Tab</option>
													</select>
												</div>
												<div class="inline-field">
													<label>Link</label>
													<input type="text" name="layer_link" class="form-control" style="width: 150px;">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="layer_controls">
										<div class="col-sm-12 text-center">
											<ul>
												<li>
													<button id="addnewlayer_html" class="btn">Add Layer</button>
												</li>
												<li>
													<button id="editlayer" class="btn"><i class="fas fa-pencil-alt"></i></button>
												</li>
												<li>
													<button id="deletelayer" class="btn"><i class="fas fa-trash-alt"></i></button>
												</li>
												<!-- <li>
													<a href="#" id="duplicatelayer" class="btn"><i class="glyphicon glyphicon-duplicate"></i></a>
												</li> -->
												<li>
													<button id="savelayer" class="btn"><i class="fas fa-save"></i></button>
												</li>
											</ul>
											<textarea id="textlayer_{{ $slide }}" class="textlayer"></textarea>
										</div>
									</div>
									<div class="editor_wrapper block">

										<div class="editor" id="editor_{{ $slide }}">

											<div style="width:{{ getSliderSetting('gridwidth', !empty($settings['gridwidth'])? $settings['gridwidth'].'px':'') }};height:{{ getSliderSetting('gridheight', !empty($settings['gridheight'])? $settings['gridheight'].'px':'') }}">

										<div id="rev_slider_{{ $slide }}_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
											<div id="rev_slider_{{ $slide }}" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
												<ul>
													<li  data-index="rs-1{{ $slide }}" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="default"  data-thumb=""  data-rotate="0"
													data-saveperformance="off"  data-title="" data-description="" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7">
														@if(!empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='bgimage')
															<img src="{!! !empty($settings['slide_'.$slide]['slider_bgimage'])? $settings['slide_'.$slide]['slider_bgimage']:'' !!}"  alt=""  data-bgposition="{!! !empty($settings['slide_'.$slide]['bg_position'])? $settings['slide_'.$slide]['bg_position']:'' !!}" data-bgfit="{!! !empty($settings['slide_'.$slide]['bg_fit'])? $settings['slide_'.$slide]['bg_fit']:'' !!}" data-bgrepeat="{!! !empty($settings['slide_'.$slide]['bg_repeat'])? $settings['slide_'.$slide]['bg_repeat']:'' !!}" data-bgparallax="10" class="rev-slidebg"
															data-no-retina>
														@elseif(!empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='transparent')
															<img src="assets/images/transparent.png" class="rev-slidebg">
														@elseif(!empty($settings['slide_'.$slide]['slider_bgtype']) && $settings['slide_'.$slide]['slider_bgtype']=='colored')
															<img src="{{ asset('assets/widgets/images/transparent.png') }}" class="rev-slidebg"  data-bgcolor="{{ !empty($settings['slide_'.$slide]['slider_color'])?$settings['slide_'.$slide]['slider_color']:'' }}">
														@endif
														@if(!empty($settings['slide_'.$slide]['hidden_layer_id']))
															@foreach($settings['slide_'.$slide]['hidden_layer_id'] as $key=>$layer)
																<div {{ !empty($settings['slide_'.$slide]['hidden_layer_rel'][$key])? 'rel="'.$settings['slide_'.$slide]['hidden_layer_rel'][$key].'"':'' }}  {{ !empty($settings['slide_'.$slide]['hidden_layer_title'][$key])? 'alt="'.$settings['slide_'.$slide]['hidden_layer_title'][$key].'"':'' }} class="tp-caption NotGeneric-Title tp-resizeme {{ !empty($settings['slide_'.$slide]['hidden_layer_class'][$key])?$settings['slide_'.$slide]['hidden_layer_class'][$key]:'' }}"
																	id="{{ !empty($settings['slide_'.$slide]['hidden_layer_id'][$key])?$settings['slide_'.$slide]['hidden_layer_id'][$key]:'' }}"
																	data-id="{{ !empty($settings['slide_'.$slide]['hidden_layer_id'][$key])?$settings['slide_'.$slide]['hidden_layer_id'][$key]:'' }}"
																	data-x="{{ !empty($settings['slide_'.$slide]['hidden_layer_halign'][$key])?$settings['slide_'.$slide]['hidden_layer_halign'][$key]:'left' }}"
																	data-hoffset="{{ !empty($settings['slide_'.$slide]['hidden_layer_hoffset'][$key])?$settings['slide_'.$slide]['hidden_layer_hoffset'][$key]:'' }}"
																	data-y="{{ !empty($settings['slide_'.$slide]['hidden_layer_valign'][$key])?$settings['slide_'.$slide]['hidden_layer_valign'][$key]:'top' }}"
																	data-voffset="{{ !empty($settings['slide_'.$slide]['hidden_layer_voffset'][$key])?$settings['slide_'.$slide]['hidden_layer_voffset'][$key]:'' }}"
																	data-width="['auto','auto','auto','auto']"
																	data-height="['auto','auto','auto','auto']"
																	data-type="{{ !empty($settings['slide_'.$slide]['hidden_layer_type'][$key])?$settings['slide_'.$slide]['hidden_layer_type'][$key]:'' }}"
																	data-responsive_offset="on"
																	data-frames='{{ !empty($settings['slide_'.$slide]['hidden_layer_animation'][$key])?str_replace(array('DELAY','SPEEDIN', 'SPEEDOUT'), array($settings['slide_'.$slide]['hidden_layer_start'][$key],!empty($settings['slide_'.$slide]['hidden_layer_animation_speed_in'][$key])?$settings['slide_'.$slide]['hidden_layer_animation_speed_in'][$key]:'300', !empty($settings['slide_'.$slide]['hidden_layer_animation_speed_out'][$key])?$settings['slide_'.$slide]['hidden_layer_animation_speed_out'][$key]:'300'), getOptions('layeranimation')[$settings['slide_'.$slide]['hidden_layer_animation'][$key]]['param']):getOptions('layeranimation')['noanimation']['param'] }}'
																	data-paddingtop="[{{ !empty($settings['slide_'.$slide]['hidden_layer_ptop'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_ptop'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_ptop'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_ptop'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_ptop'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_ptop'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_ptop'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_ptop'][$key]):'0' }}]"
																	data-paddingright="[{{ !empty($settings['slide_'.$slide]['hidden_layer_pright'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pright'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pright'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pright'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pright'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pright'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pright'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pright'][$key]):'0' }}]"
																	data-paddingbottom="[{{ !empty($settings['slide_'.$slide]['hidden_layer_pbottom'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pbottom'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pbottom'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pbottom'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pbottom'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pbottom'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pbottom'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pbottom'][$key]):'0' }}]"
																	data-paddingleft="[{{ !empty($settings['slide_'.$slide]['hidden_layer_pleft'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pleft'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pleft'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pleft'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pleft'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pleft'][$key]):'0' }},{{ !empty($settings['slide_'.$slide]['hidden_layer_pleft'][$key])? str_replace('px','',$settings['slide_'.$slide]['hidden_layer_pleft'][$key]):'0' }}]"
																	style="z-index: 5; white-space: nowrap; {{ !empty($settings['slide_'.$slide]['hidden_layer_fontsize'][$key])? 'font-size:'.$settings['slide_'.$slide]['hidden_layer_fontsize'][$key].';':'' }}
																	{{ !empty($settings['slide_'.$slide]['hidden_layer_lineheight'][$key])? 'line-height:'.$settings['slide_'.$slide]['hidden_layer_lineheight'][$key].';':'' }}
																	{{ !empty($settings['slide_'.$slide]['hidden_layer_color'][$key])? 'color:'.$settings['slide_'.$slide]['hidden_layer_color'][$key].';':'' }}
																	{{ !empty($settings['slide_'.$slide]['hidden_layer_fontweight'][$key])? 'font-weight:'.$settings['slide_'.$slide]['hidden_layer_fontweight'][$key].';':'' }}
																	{{ !empty($settings['slide_'.$slide]['hidden_layer_fontfamily'][$key])? 'font-family:"'.$settings['slide_'.$slide]['hidden_layer_fontfamily'][$key].'";':'' }}
																	{{ !empty($settings['slide_'.$slide]['hidden_layer_bgcolor'][$key])? 'background-color:'.$settings['slide_'.$slide]['hidden_layer_bgcolor'][$key].';':'' }}
																	">{!! !empty($settings['slide_'.$slide]['hidden_layer_text'][$key])?$settings['slide_'.$slide]['hidden_layer_text'][$key]:'' !!}

																</div>
															@endforeach
														@endif
													</li>
												</ul>
											</div>
										</div>

	</div>

										</div>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<div class="ibox float-e-margins">
												<div class="ibox-content" style="padding:0;">
													<div class="frames">
														<ul id="frames_{{ $slide }}">
															<li>
																<table>
																<tr>
																	<td width="100">Type</td>
																	<td width="400">Layer</td>
																	<td>Starts at</td>
																</tr>
																</table>
															</li>
															@if(!empty($settings['slide_'.$slide]['hidden_layer_id']))
																@foreach($settings['slide_'.$slide]['hidden_layer_id'] as $key=>$layer)
																	<li data-rel="{{ !empty($settings['slide_'.$slide]['hidden_layer_id'][$key])?$settings['slide_'.$slide]['hidden_layer_id'][$key]:'' }}">
																		<table><tr><td width="100">{{ !empty($settings['slide_'.$slide]['hidden_layer_type'][$key])?$settings['slide_'.$slide]['hidden_layer_type'][$key]:'' }}</td>
																		<td width="400">{{ !empty($settings['slide_'.$slide]['hidden_layer_text'][$key])?$settings['slide_'.$slide]['hidden_layer_text'][$key]:'' }}</td>
																		<td><input type="text" name="slide_{{ $slide }}[hidden_layer_start][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_start'][$key])?$settings['slide_'.$slide]['hidden_layer_start'][$key]:'' }}"></td></tr></table>
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_id][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_id'][$key])?$settings['slide_'.$slide]['hidden_layer_id'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_type][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_type'][$key])?$settings['slide_'.$slide]['hidden_layer_type'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_rel][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_rel'][$key])?$settings['slide_'.$slide]['hidden_layer_rel'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_title][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_title'][$key])?$settings['slide_'.$slide]['hidden_layer_title'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_class][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_class'][$key])?$settings['slide_'.$slide]['hidden_layer_class'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_halign][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_halign'][$key])?$settings['slide_'.$slide]['hidden_layer_halign'][$key]:'left' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_hoffset][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_hoffset'][$key])?$settings['slide_'.$slide]['hidden_layer_hoffset'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_valign][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_valign'][$key])?$settings['slide_'.$slide]['hidden_layer_valign'][$key]:'top' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_voffset][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_voffset'][$key])?$settings['slide_'.$slide]['hidden_layer_voffset'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_fontsize][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_fontsize'][$key])?$settings['slide_'.$slide]['hidden_layer_fontsize'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_lineheight][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_lineheight'][$key])?$settings['slide_'.$slide]['hidden_layer_lineheight'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_color][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_color'][$key])?$settings['slide_'.$slide]['hidden_layer_color'][$key]:'#ffffff' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_fontweight][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_fontweight'][$key])?$settings['slide_'.$slide]['hidden_layer_fontweight'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_fontfamily][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_fontfamily'][$key])?$settings['slide_'.$slide]['hidden_layer_fontfamily'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_bgcolor][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_bgcolor'][$key])?$settings['slide_'.$slide]['hidden_layer_bgcolor'][$key]:'#00000000' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_pleft][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_pleft'][$key])?$settings['slide_'.$slide]['hidden_layer_pleft'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_pright][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_pright'][$key])?$settings['slide_'.$slide]['hidden_layer_pright'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_ptop][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_ptop'][$key])?$settings['slide_'.$slide]['hidden_layer_ptop'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_pbottom][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_pbottom'][$key])?$settings['slide_'.$slide]['hidden_layer_pbottom'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_text][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_text'][$key])?$settings['slide_'.$slide]['hidden_layer_text'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_animation][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_animation'][$key])?$settings['slide_'.$slide]['hidden_layer_animation'][$key]:'noanimation' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_animation_speed_in][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_animation_speed_in'][$key])?$settings['slide_'.$slide]['hidden_layer_animation_speed_in'][$key]:'300' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_animation_speed_out][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_animation_speed_out'][$key])?$settings['slide_'.$slide]['hidden_layer_animation_speed_out'][$key]:'300' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_target][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_target'][$key])?$settings['slide_'.$slide]['hidden_layer_target'][$key]:'' }}">
																		<input type="hidden" name="slide_{{ $slide }}[hidden_layer_link][]" value="{{ !empty($settings['slide_'.$slide]['hidden_layer_link'][$key])?$settings['slide_'.$slide]['hidden_layer_link'][$key]:'' }}">
																	</li>
																@endforeach
															@endif
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						</div>
						<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
						<script type="text/javascript" src="{{ asset('assets/private/plugins/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

						<script type="text/javascript">
						var tpj=$;
						var revapi116{{ $slide }};
						var revapi = tpj(document).ready(function() {
							if(tpj("#rev_slider_{{ $slide }}").revolution == undefined){
								revslider_showDoubleJqueryError("#rev_slider_{{ $slide }}");
							}else{
								revapi116{{ $slide }} = tpj("#rev_slider_{{ $slide }}").show().revolution({
									sliderType:"{{  getSliderSetting('sliderType', !empty($settings['sliderType'])? $settings['sliderType']:'') }}",
									jsFileLocation:"{{ asset('assets/private/plugins/revolution/js') }}/",
									sliderLayout:"auto",
									dottedOverlay:"{{ getSliderSetting('dottedoverlay', !empty($settings['dottedoverlay'])? $settings['dottedoverlay']:'') }}",
									delay:{{ getSliderSetting('delay', !empty($settings['delay'])? $settings['delay']:'') }},
									navigation: {
										keyboardNavigation:"off",
										keyboard_direction: "horizontal",
										mouseScrollNavigation:"off",
										onHoverStop:"off",
										touch:{
											touchenabled:"on",
											swipe_threshold: 75,
											swipe_min_touches: 1,
											swipe_direction: "horizontal",
											drag_block_vertical: false
										}
										,
										arrows: {
											style:"{{  getSliderSetting('arrow_style', !empty($settings['arrow_style'])? $settings['arrow_style']:'') }}",
											enable:false,
											hide_onmobile:{{  getSliderSetting('hideonmobile', !empty($settings['hideonmobile'])? $settings['hideonmobile']:'') }},
											hide_under:{{ getSliderSetting('hideunder', !empty($settings['hideunder'])? $settings['hideunder']:'') }},
											hide_onleave:{{  getSliderSetting('hideonleave', !empty($settings['hideonleave'])? $settings['hideonleave']:'') }},
											hide_delay:200,
											hide_delay_mobile:1200,
											tmp:'',
											left: {
												h_align:"{{  getSliderSetting('l_halign', !empty($settings['l_halign'])? $settings['l_halign']:'') }}",
												v_align:"{{  getSliderSetting('l_valign', !empty($settings['l_valign'])? $settings['l_valign']:'') }}",
												h_offset:{{ getSliderSetting('l_horizoff', !empty($settings['l_horizoff'])? $settings['l_horizoff']:'') }},
												v_offset:{{ getSliderSetting('l_vertoff', !empty($settings['l_vertoff'])? $settings['l_vertoff']:'') }}
											},
											right: {
												h_align:"{{  getSliderSetting('r_halign', !empty($settings['r_halign'])? $settings['r_halign']:'') }}",
												v_align:"{{  getSliderSetting('r_valign', !empty($settings['r_valign'])? $settings['r_valign']:'') }}",
												h_offset:{{ getSliderSetting('r_horizoff', !empty($settings['r_horizoff'])? $settings['r_horizoff']:'') }},
												v_offset:{{ getSliderSetting('r_vertoff', !empty($settings['r_vertoff'])? $settings['r_vertoff']:'') }}
											}
										}
									},
									viewPort: {
										enable:true,
										outof:"pause",
										visible_area:"80%"
									},
									gridwidth:{{ getSliderSetting('gridwidth', !empty($settings['gridwidth'])? $settings['gridwidth']:'') }},
									gridheight:{{ getSliderSetting('gridheight', !empty($settings['gridheight'])? $settings['gridheight']:'') }},
									lazyType:"none",
									shadow:{{ getSliderSetting('shadow', !empty($settings['shadow'])? $settings['shadow']:'') }},
									spinner:"{{ getSliderSetting('spinner', !empty($settings['spinner'])? $settings['spinner']:'') }}",
									stopLoop:"{{ getSliderSetting('stoploop', !empty($settings['stoploop'])? $settings['stoploop']:'') }}",
									stopAfterLoops:{{ getSliderSetting('stopafterloop', !empty($settings['stopafterloop'])? $settings['stopafterloop']:'') }},
									stopAtSlide:{{ getSliderSetting('stopatslide', !empty($settings['stopatslide'])? $settings['stopatslide']:'') }},
									shuffle:"off",
									autoHeight:"{{  getSliderSetting('autoheight', !empty($settings['autoheight'])? $settings['autoheight']:'') }}",
									hideThumbsOnMobile:"{{  getSliderSetting('hidethumbsonmobile', !empty($settings['hidethumbsonmobile'])? $settings['hidethumbsonmobile']:'') }}",
									hideSliderAtLimit:0,
									hideCaptionAtLimit:0,
									hideAllCaptionAtLilmit:0,
									debugMode:false,
									fallbacks: {
										simplifyAll:"off",
										nextSlideOnWindowFocus:"off",
										disableFocusListener:false,
									}
								});
							}
						});	/*ready*/
						</script>
						<script>
						$(document).ready(function(){
								$('#slide_{{ $slide }} #editor_{{ $slide }}').ruler();
								$('#slide_{{ $slide }} .ruler.top').click(function(e){
									var pos = parseFloat(e.pageX) - parseFloat($('#slide_{{ $slide }} #editor_{{ $slide }}').offset().left);
									$('#slide_{{ $slide }} #editor_{{ $slide }}').append("<span class='line' style='top:0;bottom:0;width:1px;background:#000000;left:"+pos+"px'></span>");
								})
								$('#slide_{{ $slide }} .ruler.left').click(function(e){
									var pos = parseFloat(e.pageY) - parseFloat($('#slide_{{ $slide }} #editor_{{ $slide }}').offset().top);
									$('#slide_{{ $slide }} #editor_{{ $slide }}').append("<span class='line' style='left:0;right:0;height:1px;background:#000000;top:"+pos+"px'></span>");
								})
								$(function(){


									$("#slide_{{ $slide }} #editor_{{ $slide }}").on('click','.tp-mask-wrap',function(){
										$("#slide_{{ $slide }} .tp-mask-wrap").removeClass('selected');
										$("#frames_{{ $slide }} li").removeClass('selected');
										jQuery(this).addClass('selected');
										$("#frames_{{ $slide }} li[data-rel='"+jQuery(this).find('.tp-caption').attr('data-id')+"']").addClass('selected');

										$("#slide_{{ $slide }} .tp-mask-wrap.selected" ).draggable({
												containment: "#rev_slider_{{ $slide }}_wrapper",
												stop: function getNewPos(e, ui) {
													e.stopPropagation();
													var $this = $(this);
													var thisPos = $this.position();
													var parentPos = $this.parent('li').position();

													var position = $this.offsetRelative("li");

													var x = position.left;
													var y = position.top;



													var savedpositionX = {{ !empty($settings['slide_'.$slide]['hidden_layer_hoffset'][$key])?$settings['slide_'.$slide]['hidden_layer_hoffset'][$key]:'1' }};
													var savedpositionY = {{ !empty($settings['slide_'.$slide]['hidden_layer_voffset'][$key])?$settings['slide_'.$slide]['hidden_layer_voffset'][$key]:'1' }};

													x = parseInt(x);
													y = parseInt(y);


													if($('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_layer_halign][]"]').val()=='right'){
														var right = parseInt($('input[name="gridwidth"]').val())-$this.width();
														x = right - x;
													}else if($('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_layer_halign][]"]').val()=='center'){
														var right = parseInt(parseInt($('input[name="gridwidth"]').val()-$this.width())/2);
														x = right - x;
													}

													if($('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_layer_valign][]"]').val()=='bottom'){
														var bottom = parseInt($('input[name="gridheight"]').val())-$this.height();
														y = bottom - y;
													}else if($('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_layer_valign][]"]').val()=='middle'){
														var bottom = parseInt(parseInt($('input[name="gridheight"]').val()-$this.height())/2);
														y = bottom - y;
													}

													console.log(x+' '+y);


													$('#slide_{{ $slide }} #styles_{{ $slide }} input[name="layer_hoffset"]').val(x);
													$('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_layer_hoffset][]"]').val(x);
													$("#slide_{{ $slide }} .tp-mask-wrap.selected .tp-caption").attr('data-hoffset', x);

													$('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_layer_voffset][]"]').val(y);
													$('#slide_{{ $slide }} #styles_{{ $slide }} input[name="layer_voffset"]').val(y);
													$("#slide_{{ $slide }} .tp-mask-wrap.selected .tp-caption").attr('data-voffset', y);
												}
											});
										$('#slide_{{ $slide }} #textlayer_{{ $slide }}').val($('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_layer_text][]"]').val());
										$('#slide_{{ $slide }} #styles_{{ $slide }} .form-control').each(function(){
											if($(this).prop('type')=='text'){
												$(this).val('');
												$(this).val($('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_'+$(this).attr('name')+'][]"]').val());
											}else if($(this).prop('type')=='select-one'){
												$(this).find("option").each(function(){
													$(this).removeAttr('selected');
												});
												var val = $('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_'+$(this).attr('name')+'][]"]').val();
												$(this).find("option[value='"+val+"']").prop("selected", true);
											}
										})
										initCP();
									})

									$(document).on("click", function(e) {
										if ($(e.target).is("#textlayer_{{ $slide }}") === false) {
										  $("#textlayer_{{ $slide }}").hide();
										  $('#slide_{{ $slide }} #savelayer').prop("disabled", true);
										  $('#slide_{{ $slide }} #editlayer').prop("disabled", false);
										}
									 });
									$('#slide_{{ $slide }} #styles_{{ $slide }} select.form-control').change(function(){
										$('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_'+$(this).attr('name')+'][]"]').val($(this).val());
									})
									$('#slide_{{ $slide }} #styles_{{ $slide }} input.form-control').bind('change blur paste keyup',function(){
										$('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_{{ $slide }}[hidden_'+$(this).attr('name')+'][]"]').val($(this).val());
									})
								});





								var slide = {{ $slide }};

								$('#slide_{{ $slide }} #addnewlayer_html').click(function(){
									var btn = $(this);
									btn.prop('disabled', true);
									var lastframe= parseFloat($('#slide_{{ $slide }} #frames_{{ $slide }} li').eq($('#slide_{{ $slide }} #frames_{{ $slide }} li').length-1).find('input[type="text"]').val());
									if($('#slide_{{ $slide }} #frames_{{ $slide }} li').length==1){
										lastframe = 1;
									}else{
										lastframe= parseFloat($('#slide_{{ $slide }} #frames_{{ $slide }} li').eq($('#slide_{{ $slide }} #frames_{{ $slide }} li').length-1).find('input[type="text"]').val())+300;
									}
									if($('#slide_{{ $slide }} #frames_{{ $slide }} li').length-1>0){
										var layer_split = $('#slide_{{ $slide }} #frames_{{ $slide }} li').eq($('#slide_{{ $slide }} #frames_{{ $slide }} li').length-1).find('input[name="slide_{{ $slide }}[hidden_layer_id][]"]').val().split('-');
										var layer = parseInt(layer_split[layer_split.length-1])+1;
									}else{
										var layer = 0;
									}
									$.ajax({
										type: "GET",
										url: '{{URL::route('widget.addlayer')}}',
										data: { _token: $('input[name="_token"]').val(), slide: slide, layer: layer, timeline: lastframe, type: 'text' },
										success: function( msg ) {
											var obj = jQuery.parseJSON(msg);
											$('#slide_{{ $slide }} .tp-revslider-mainul > li').append(obj.container);
											$('#slide_{{ $slide }} #frames_{{ $slide }}').append(obj.element);
											btn.prop('disabled', false);
											//$('#save').trigger('click');
										}
									});

									return false;
								})
								$('#slide_{{ $slide }} #addnewlayer_btn').click(function(){
									var lastframe= parseFloat($('#slide_{{ $slide }} #frames_{{ $slide }} li').eq($('#slide_{{ $slide }} #frames_{{ $slide }} li').length-1).find('input[type="text"]').val())+300;
									if($('#slide_{{ $slide }} #frames_{{ $slide }} li').length==1){
										lastframe = 1;
									}
									if($('#slide_{{ $slide }} #frames_{{ $slide }} li').length-1>0){
										var layer_split = $('#slide_{{ $slide }} #frames_{{ $slide }} li').eq($('#slide_{{ $slide }} #frames_{{ $slide }} li').length-1).find('input[name="slide_{{ $slide }}[hidden_layer_id][]"]').val().split('-');
										var layer = parseInt(layer_split[layer_split.length-1])+1;
									}else{
										var layer = 0;
									}
									$.ajax({
										type: "GET",
										url: '{{URL::route('widget.addlayer')}}',
										data: { _token: $('input[name="_token"]').val(), slide: slide, layer: layer, timeline: lastframe, type: 'button' },
										success: function( msg ) {
											var obj = jQuery.parseJSON(msg);
											$('#slide_{{ $slide }} .tp-revslider-mainul > li').append(obj.container);
											$('#slide_{{ $slide }} #frames_{{ $slide }}').append(obj.element);
										}
									});
									return false;
								})
								$('#slide_{{ $slide }} #editlayer').click(function(e){
									$('#slide_{{ $slide }} #textlayer_{{ $slide }}').val($('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_'+slide+'[hidden_layer_text][]"]').val());
									$('#slide_{{ $slide }} #textlayer_{{ $slide }}').show();
									$(this).prop("disabled", true);
									$('#slide_{{ $slide }} #savelayer').prop("disabled", false);
									e.stopPropagation();
									return false;
								})


								$('#slide_{{ $slide }} #savelayer').click(function(e){
									$('#slide_{{ $slide }} #frames_{{ $slide }} li.selected input[name="slide_'+slide+'[hidden_layer_text][]"]').val($('#slide_{{ $slide }} #textlayer_{{ $slide }}').val());
									$("#slide_{{ $slide }} .tp-mask-wrap.selected .tp-caption").html($('#slide_{{ $slide }} #textlayer_{{ $slide }}').val());
									$('#slide_{{ $slide }} #textlayer_{{ $slide }}').hide();
									$(this).prop("disabled", true);
									$('#slide_{{ $slide }} #editlayer').prop("disabled", false);
									return false;
								})
								$('#slide_{{ $slide }} #deletelayer').click(function(e){
									var btn = $(this);
									btn.prop('disabled', true);
									$('#slide_{{ $slide }} #frames_{{ $slide }} li.selected').remove();
									$("#slide_{{ $slide }} .tp-mask-wrap.selected").remove();
									setTimeout(function(){
										btn.prop('disabled', false);
									},200);
									return false;
								})
								$('#slide_{{ $slide }} #duplicatelayer').click(function(e){
									var nextslide = parseFloat($('#slide_{{ $slide }} #frames_{{ $slide }} li').length)+1;
									$('#slide_{{ $slide }} #frames_{{ $slide }} li.selected').clone().removeClass('selected').attr('data-rel', 'slide-'+slide+'-layer-'+nextslide).appendTo($('#slide_{{ $slide }} #frames_{{ $slide }}'));
									$('#slide_{{ $slide }} #frames_{{ $slide }} li[data-rel="slide-slide_'+slide+'[-layer-'+nextslide+'"]').find('input[name="'+slide+'hidden_layer_id][]"]').val('slide-'+slide+'-layer-'+nextslide);
									var layer = parseFloat($('#slide_{{ $slide }} #frames_{{ $slide }} li').length)-1;
									$("#slide_{{ $slide }} .tp-mask-wrap.selected").clone().find('.tp-caption').attr('data-id', 'slide-'+slide+'-layer-'+layer).parents('.tp-mask-wrap.selected').removeClass('selected').appendTo($("#slide_{{ $slide }} .tp-mask-wrap.selected").parent('li'));
									return false;
								})
						})
						</script>

				@endforeach
			@endif



















			</div>










    </div>
</div>
</form>


@include('admin._partials.popupuploader')

<script>
(function($){
    $.fn.offsetRelative = function(top){
        var $this = $(this);
        var $parent = $this.offsetParent();
        var offset = $this.position();
        if(!top) return offset; // Didn't pass a 'top' element
        else if($parent.get(0).tagName == "BODY") return offset; // Reached top of document
        else if($(top,$parent).length) return offset; // Parent element contains the 'top' element we want the offset to be relative to
        else if($parent[0] == $(top)[0]) return offset; // Reached the 'top' element we want the offset to be relative to
        else { // Get parent's relative offset
            var parent_offset = $parent.offsetRelative(top);
            offset.top += parent_offset.top;
            offset.left += parent_offset.left;
            return offset;
        }
    };
    $.fn.positionRelative = function(top){
        return $(this).offsetRelative(top);
    };



}(jQuery));
$(document).ready(function(){

		$('#media-select .md-custom-footer').append('<input type="hidden" id="selectedimage_input">');
		$('.colorpicker-rgba').colorpicker();
		$('#save').click(function(){
			$('#widgetform').submit();
		})
		$('#newslide').click(function(){
			$('#slides').val(parseInt($('#slides').val())+1);
			$('#allslides').append('<input type="hidden" name="slideorder[]" value="'+parseInt($('#slides').val())+1+'">');
			$('#widgetform').submit();
		})
		$('.duplicateslide').click(function(){
			var selectedslide = $(this).data('slide');
			var nextslide = parseInt($('#slides').val())+1;
			$('#slides').val(nextslide);
			$('#allslides').append('<input type="hidden" name="slideorder[]" value="'+nextslide+'">');
			$("#slide_"+selectedslide+" input, #slide_"+selectedslide+" select").each(function(){
				if($(this).attr('type')=='checkbox' && $(this).prop('type')=='radio'){
					var newvalue = $("input[name='"+$(this).attr('name')+"']:checked").val().replace('slide_'+selectedslide, 'slide_'+nextslide);
				}else{
					var newvalue = $(this).val().replace('slide_'+selectedslide, 'slide_'+nextslide);
				}
				var newname = $(this).attr('name').replace('slide_'+selectedslide, 'slide_'+nextslide);
				$('#allslides').append('<input type="hidden" name="'+newname+'" value="'+newvalue+'">');
			})

			setTimeout(function(){
				$('#widgetform').submit();
			}, 100);
			return false;
		})
		$('.disableslide').click(function(){
				var selectedslide = $(this).data('slide');
				if($(this).parents('li').find('.disablecheck').is(":checked")){
					$(this).parents('li').find('.disablecheck').prop('checked', false);
					$(this).parents('li').removeClass('disabled');
					$(this).html('Disable');
				}else{
					$(this).parents('li').find('.disablecheck').prop('checked', true);
					$(this).parents('li').addClass('disabled');
					$(this).html('Enable');
				}
				return false;
		})
		$('.fileupload').on('click', function(){
			$('#selectedimage_input').val('');
			$('#selectedimage_input').val($(this).next('input[type="text"]').attr('name'));
		})
		$('.allmedia').on('click','.imageholder a',function(){
			$('input[name="'+$('#selectedimage_input').val()+'"]').val($(this).attr('href'));
			$('#media-select').removeClass('md-custom-show');
			return false;
		})
		$('.slidethumbs .openslide').click(function(){
			$('.slidethumbs li').removeClass('active');
			$(this).parents('li').addClass('active');
			$('.slide').hide();
			$('input[name="activeslide"]').val($(this).attr('rel').replace('slide_',''));
			$('#'+$(this).attr('rel')).fadeIn();
			$('.editor').ruler();
			setTimeout(function() {
                 $(window).trigger("resize");
          },200);
		})
})
$(window).bind("load", function() {
	var hash = location.hash.substr(1);
	if(hash!=''){
	$('.openslide[href="#'+hash+'"]').trigger('click');
	}
})
@if (\Session::has('activeslide'))
$(document).ready(function(){
	$('.slidethumbs #slide_link_{{ !empty(\Session::get('activeslide'))?\Session::get('activeslide'):'1' }}').trigger('click');
})
@endif
</script>
@endsection
