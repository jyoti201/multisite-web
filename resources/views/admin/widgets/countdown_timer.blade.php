@extends('layouts.app')

@section('extra')

<link rel="stylesheet" href="{{ asset('assets/private/plugins/soon-countdown/extras/builder/css/soon.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/hljs.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/hljs_theme.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/spectrum.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/jquery.datetimepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/styles.css') }}" type="text/css">
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />

<style>
.choosemedia{
	    border: 3px dashed;
    min-height: 300px;
    position: relative;
}
.choosemedia.has-image{
	display:inline-block;
}
.choosemedia a{
	    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    text-align: center;
    z-index: 99;
}
.choosemedia a .fa{
	    position: absolute;
    font-size: 45px;
    color: #000000;
    top: 50%;
    transform: translateY(-50%) translateX(-50%);
	-webkit-transform: translateY(-50%) translateX(-50%);
    left: 50%;
}
#preview{
	background:url({{ asset('assets/images/preview.png') }}) no-repeat center;
	min-height:100px;
	border: 2px dashed #eaeaea;
	margin-bottom:10px;
}
.grid figure{
	float:none;
}
.soon[data-layout*=group]{
	padding: 1em 20px !important;
}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right">
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg m-b-20">Update</button>
						</div>
				</div>
				<div class="card-box">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
								<div class="builder">
								<div class="builder-demo"></div>
								<form class="builder-presets" action=""></form>
								<form class="builder-form" method="post" id="widgetform" action="{{ route('widget.save') }}">
									{{csrf_field()}}
									<input type="hidden" name="preset" value="{{ !empty($settings['preset']) ? $settings['preset'] : '' }}">
									<input type="hidden" name="html" value="{{ !empty($settings['html']) ? $settings['html'] : '' }}">
									<input type="hidden" name="id" value="{{ $widget->id }}">
								<h1></h1>
								<div class="btn-group"></div>
								<div class="inner"></div>
								</form>
								</div>
								</div>

<script>
$(document).ready(function(){
	$('.checkable').find('input[type="checkbox"]').prop('checked', false);
	setTimeout(function(){
			@if(!empty($settings))
				@foreach($settings as $key => $value)
					@if($key!='html' && $key!='preset' && $key!='_token' && $key!='id')
						if($('input[name="{{ $key }}"]').length > 0){
							$('input[name="{{ $key }}"]').val({!! json_encode($value) !!});
							$('.checkable').find('input[name="{{ $key }}"]').prop('checked', true);
						}
						if($('select[name="{{ $key }}"]').length > 0){
							$('select[name="{{ $key }}"]').val({!! json_encode($value) !!});
							$('select[name="{{ $key }}"]').trigger('change');
						}
						if($('textarea[name="{{ $key }}"]').length > 0){
							$('textarea[name="{{ $key }}"]').val({!! json_encode($value) !!});
						}
					@endif
				@endforeach
				@if(!empty($settings['preset']))
					$('.builder-presets').html("Selected Style: <strong>{{ $settings['preset'] }}</strong>");
				@endif
				@if(!empty($settings['html']))
					var checked = $('.checkable').find('input:checked');
					checked.trigger('click');
					checked.trigger('click');
				@endif
			@endif
	}, 500);
})
</script>

							</div>
						</div>
				</div>
			</div>
    </div>
</div>

<script>

    var soonId = 'counter-{{ $widget->id }}';

    var soonInit = function(){};

    var soonGeneratorFormatter = function(container,source) {

        if (typeof hljs !== 'undefined') {
            container.innerHTML = hljs.highlight('html',source).value;
        }

    };

    var soonStorage = {

        save:function(data,cb) {
            localStorage.setItem('soon',JSON.stringify(data));
            if (cb){cb();}
        },

        load:function(cb){
            var data;
            try {
                data = JSON.parse(localStorage.getItem('soon'));
            }
            catch(e){}
            cb(data);
        }

    };

</script>
<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/js/soon.min.js') }}" data-auto="false"></script>


<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/jquery.datetimepicker.js') }}"></script>
<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/spectrum.js') }}"></script>
<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/hljs.js') }}"></script>
<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/generator.js') }}"></script>
<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/presets.js') }}"></script>
<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/fields.js') }}"></script>
<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/styler.js') }}"></script>
<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/renderer.js') }}"></script>
<script src="{{ asset('assets/private/plugins/soon-countdown/extras/builder/app/main.js') }}"></script>
<script>
$(document).ready(function(){
		$('#save').on('click', function(){
				@if(!empty($settings['preset']))
					$('#widgetform input[name="preset"]').val('{{ $settings['preset'] }}');
				@else
					$('#widgetform input[name="preset"]').val($('#presets').val());
				@endif
				$('#widgetform input[name="html"]').val($('code.html').text());
				$('#widgetform').submit();
		})
})
</script>
@endsection
