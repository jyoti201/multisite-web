@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />

<style>
.repeaters .repeat img{
	width: 35px;margin-left: 10px;vertical-align: text-top;
}
.repeaters .repeat .fa{
	margin-left: 5px;
}
.repeaters .repeat .choosecustomicon {
	vertical-align: top;
}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="row">
					<form method="post" id="widgetform" action="{{ route('widget.save') }}">
						{{csrf_field()}}
						<input type="hidden" name="id" value="{{ $widget->id }}">
						<div class="col-sm-12">
							<div class="d-flex justify-content-between">
									<div class="left">
												<div class="shortcodeplaceholder">
														<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
														<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
																<i class="fa fa-copy"></i>
														</button>
												</div>
									</div>
									<div class="right">
										<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg m-b-20">Update</button>
									</div>
							</div>
							<div class="card-box">

								<div class="repeaters">
									<a href="" class="btn btn-white btn-lg addcustom">Add Social</a>
									@if(!empty($settings['icon']) && sizeof($settings['icon'])>0)
										@foreach($settings['icon'] as $key=>$val)
											<div class="repeat row m-t-15">
												<div class="col-sm-1"><button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button></div>
												<div class="col-sm-5">
													<select class="form-control" name="icon[]">
														<option value="">None</option>
														<option value="facebook" {{ $val=='facebook'? 'selected':'' }}>Facebook</option>
														<option value="twitter" {{ $val=='twitter'? 'selected':'' }}>Twitter</option>
														<option value="email" {{ $val=='email'? 'selected':'' }}>Email</option>
														<option value="pinterest" {{ $val=='pinterest'? 'selected':'' }}>Pinterest</option>
														<option value="linkedin" {{ $val=='linkedin'? 'selected':'' }}>LinkedIn</option>
														<option value="reddit" {{ $val=='reddit'? 'selected':'' }}>Reddit</option>
														<option value="whatsapp" {{ $val=='whatsapp'? 'selected':'' }}>WhatsApp</option>
														<option value="gmail" {{ $val=='gmail'? 'selected':'' }}>Gmail</option>
														<option value="telegram" {{ $val=='telegram'? 'selected':'' }}>Telegram</option>
														<option value="pocket" {{ $val=='pocket'? 'selected':'' }}>Pocket</option>
														<option value="mix" {{ $val=='mix'? 'selected':'' }}>Mix</option>
														<option value="tumblr" {{ $val=='tumblr'? 'selected':'' }}>Tumblr</option>
														<option value="amazon" {{ $val=='amazon'? 'selected':'' }}>Amazon Wish List</option>
														<option value="aol" {{ $val=='aol'? 'selected':'' }}>AOL Mail</option>
														<option value="balatarin" {{ $val=='balatarin'? 'selected':'' }}>Balatarin</option>
														<option value="bibsonomy" {{ $val=='bibsonomy'? 'selected':'' }}>BibSonomy</option>
														<option value="bitty" {{ $val=='bitty'? 'selected':'' }}>Bitty Browser</option>
														<option value="blinklist" {{ $val=='blinklist'? 'selected':'' }}>Blinklist</option>
														<option value="blogger" {{ $val=='blogger'? 'selected':'' }}>Blogger</option>
														<option value="blogmarks" {{ $val=='blogmarks'? 'selected':'' }}>BlogMarks</option>
														<option value="bookmarks_fr" {{ $val=='bookmarks_fr'? 'selected':'' }}>Bookmarks.fr</option>
														<option value="box" {{ $val=='box'? 'selected':'' }}>Box.net</option>
														<option value="buffer" {{ $val=='buffer'? 'selected':'' }}>Buffer</option>
														<option value="care2" {{ $val=='care2'? 'selected':'' }}>Care2 News</option>
														<option value="citeulike" {{ $val=='citeulike'? 'selected':'' }}>CiteULike</option>
														<option value="link" {{ $val=='link'? 'selected':'' }}>Copy Link</option>
														<option value="designfloat" {{ $val=='designfloat'? 'selected':'' }}>Design Float</option>
														<option value="diary_ru" {{ $val=='diary_ru'? 'selected':'' }}>Diary.Ru</option>
														<option value="diaspora" {{ $val=='diaspora'? 'selected':'' }}>Diaspora</option>
														<option value="digg" {{ $val=='digg'? 'selected':'' }}>Digg</option>
														<option value="diigo" {{ $val=='diigo'? 'selected':'' }}>Diigo</option>
														<option value="douban" {{ $val=='douban'? 'selected':'' }}>Douban</option>
														<option value="draugiem" {{ $val=='draugiem'? 'selected':'' }}>Draugiem</option>
														<option value="dzone" {{ $val=='dzone'? 'selected':'' }}>DZone</option>
														<option value="evernote" {{ $val=='evernote'? 'selected':'' }}>Evernote</option>
														<option value="facebook_messenger" {{ $val=='facebook_messenger'? 'selected':'' }}>Facebook Messenger</option>
														<option value="fark" {{ $val=='fark'? 'selected':'' }}>Fark</option>
														<option value="flipboard" {{ $val=='flipboard'? 'selected':'' }}>Flipboard</option>
														<option value="folkd" {{ $val=='folkd'? 'selected':'' }}>Folkd</option>
														<option value="google" {{ $val=='google'? 'selected':'' }}>Google Bookmarks</option>
														<option value="google_classroom" {{ $val=='google_classroom'? 'selected':'' }}>Google Classroom</option>
														<option value="google_plus" {{ $val=='google_plus'? 'selected':'' }}>Google+</option>
														<option value="y18" {{ $val=='y18'? 'selected':'' }}>Hacker News</option>
														<option value="hatena" {{ $val=='hatena'? 'selected':'' }}>Hatena</option>
														<option value="houzz" {{ $val=='houzz'? 'selected':'' }}>Houzz</option>
														<option value="instapaper" {{ $val=='instapaper'? 'selected':'' }}>Instapaper</option>
														<option value="kakao" {{ $val=='kakao'? 'selected':'' }}>Kakao</option>
														<option value="kik" {{ $val=='kik'? 'selected':'' }}>Kik</option>
														<option value="kindle" {{ $val=='kindle'? 'selected':'' }}>Kindle It</option>
														<option value="known" {{ $val=='known'? 'selected':'' }}>Known</option>
														<option value="line" {{ $val=='line'? 'selected':'' }}>Line</option>
														<option value="livejournal" {{ $val=='livejournal'? 'selected':'' }}>LiveJournal</option>
														<option value="mail_ru" {{ $val=='mail_ru'? 'selected':'' }}>Mail.Ru</option>
														<option value="mastodon" {{ $val=='mastodon'? 'selected':'' }}>Mastodon</option>
														<option value="mendeley" {{ $val=='mendeley'? 'selected':'' }}>Mendeley</option>
														<option value="meneame" {{ $val=='meneame'? 'selected':'' }}>Meneame</option>
														<option value="mewe" {{ $val=='mewe'? 'selected':'' }}>MeWe</option>
														<option value="mixi" {{ $val=='mixi'? 'selected':'' }}>Mixi</option>
														<option value="myspace" {{ $val=='myspace'? 'selected':'' }}>MySpace</option>
														<option value="netvouz" {{ $val=='netvouz'? 'selected':'' }}>Netvouz</option>
														<option value="odnoklassniki" {{ $val=='odnoklassniki'? 'selected':'' }}>Odnoklassniki</option>
														<option value="outlook_com" {{ $val=='outlook_com'? 'selected':'' }}>Outlook.com</option>
														<option value="papaly" {{ $val=='papaly'? 'selected':'' }}>Papaly</option>
														<option value="pinboard" {{ $val=='pinboard'? 'selected':'' }}>Pinboard</option>
														<option value="plurk" {{ $val=='plurk'? 'selected':'' }}>Plurk</option>
														<option value="print" {{ $val=='print'? 'selected':'' }}>Print</option>
														<option value="printfriendly" {{ $val=='printfriendly'? 'selected':'' }}>PrintFriendly</option>
														<option value="protopage" {{ $val=='protopage'? 'selected':'' }}>Protopage Bookmarks</option>
														<option value="pusha" {{ $val=='pusha'? 'selected':'' }}>Pusha</option>
														<option value="qzone" {{ $val=='qzone'? 'selected':'' }}>Qzone</option>
														<option value="rediff" {{ $val=='rediff'? 'selected':'' }}>Rediff MyPage</option>
														<option value="refind" {{ $val=='refind'? 'selected':'' }}>Refind</option>
														<option value="renren" {{ $val=='renren'? 'selected':'' }}>Renren</option>
														<option value="sina_weibo" {{ $val=='sina_weibo'? 'selected':'' }}>Sina Weibo</option>
														<option value="sitejot" {{ $val=='sitejot'? 'selected':'' }}>SiteJot</option>
														<option value="skype" {{ $val=='skype'? 'selected':'' }}>Skype</option>
														<option value="slashdot" {{ $val=='slashdot'? 'selected':'' }}>Slashdot</option>
														<option value="sms" {{ $val=='sms'? 'selected':'' }}>SMS</option>
														<option value="stocktwits" {{ $val=='stocktwits'? 'selected':'' }}>StockTwits</option>
														<option value="svejo" {{ $val=='svejo'? 'selected':'' }}>Svejo</option>
														<option value="symbaloo" {{ $val=='symbaloo'? 'selected':'' }}>Symbaloo Bookmarks</option>
														<option value="threema" {{ $val=='threema'? 'selected':'' }}>Threema</option>
														<option value="trello" {{ $val=='trello'? 'selected':'' }}>Trello</option>
														<option value="tuenti" {{ $val=='tuenti'? 'selected':'' }}>Tuenti</option>
														<option value="twiddla" {{ $val=='twiddla'? 'selected':'' }}>Twiddla</option>
														<option value="typepad" {{ $val=='typepad'? 'selected':'' }}>TypePad Post</option>
														<option value="viadeo" {{ $val=='viadeo'? 'selected':'' }}>Viadeo</option>
														<option value="viber" {{ $val=='viber'? 'selected':'' }}>Viber</option>
														<option value="vk" {{ $val=='vk'? 'selected':'' }}>VK</option>
														<option value="wanelo" {{ $val=='wanelo'? 'selected':'' }}>Wanelo</option>
														<option value="wechat" {{ $val=='wechat'? 'selected':'' }}>WeChat</option>
														<option value="wordpress" {{ $val=='wordpress'? 'selected':'' }}>WordPress</option>
														<option value="wykop" {{ $val=='wykop'? 'selected':'' }}>Wykop</option>
														<option value="xing" {{ $val=='xing'? 'selected':'' }}>XING</option>
														<option value="yahoo" {{ $val=='yahoo'? 'selected':'' }}>Yahoo Mail</option>
														<option value="yoolink" {{ $val=='yoolink'? 'selected':'' }}>Yoolink</option>
														<option value="yummly" {{ $val=='yummly'? 'selected':'' }}>Yummly</option>
													</select>
												</div>
												<div class="col-sm-6">
													<a href="#" class="choosecustomicon btn btn-primary waves-effect waves-light" data-toggle="custommodal" data-target="#media-select"><i class="ion-upload m-r-5"></i>Custom Icon</a>
													<span class="removecustomicon">
													@if(!empty($settings['customicon'][$key]))
														<img src="{{ $settings['customicon'][$key] }}" border="{{ !empty($settings['border'])? $settings['border']:'' }}" alt="'.$val.'" width="{{ !empty($settings['custom_icon_width'])? $settings['custom_icon_width']:'' }}" height="{{ !empty($settings['custom_icon_height'])? $settings['custom_icon_height']:'' }}">
														<a href="#" class="removeicon"><i class="fa fa-times"></i></a>
													@endif
													</span>
													<input type="hidden" name="customicon[]" value="{{ !empty($settings['customicon'][$key])?$settings['customicon'][$key]:'' }}" class="form-control">
												</div>
											</div>
										@endforeach
									@endif
									</div>
									<div class="form-group m-t-15">
										<div class="checkbox">
											<input id="showmore" name="showmore" type="checkbox" value="yes" {{ !empty($settings['showmore']) && $settings['showmore']=='yes'?'checked':'' }}>
											<label for="showmore">
												Show more
											</label>
										</div>
									</div>
									<div class="form-group m-t-15">
										<div class="checkbox">
											<input id="showcounter" name="showcounter" type="checkbox" value="yes" {{ !empty($settings['showcounter']) && $settings['showcounter']=='yes'?'checked':'' }}>
											<label for="showcounter">
												Show Counter
											</label>
										</div>
									</div>
									<div class="form-group m-t-15">
										<label for="icon_size">
											Icon Size (if no custom icon):
										</label>
										<select class="form-control" id="icon_size" name="icon_size">
											<option value="">Default</option>
											<option value="32" {{ !empty($settings['icon_size']) && $settings['icon_size']=='32'? 'selected':'' }}>32</option>
											<option value="64" {{ !empty($settings['icon_size']) && $settings['icon_size']=='64'? 'selected':'' }}>64</option>
										</select>
									</div>
									<div class="form-group m-t-15">
										<label for="iconstyle">
											Custom icon Width:
										</label>
										<input type="text" name="custom_icon_width" value="{{ !empty($settings['custom_icon_width'])? $settings['custom_icon_width']:'' }}" placeholder="in px" class="form-control">
									</div>
									<div class="form-group m-t-15">
										<label for="iconstyle">
											Custom icon Height:
										</label>
										<input type="text" name="custom_icon_height" value="{{ !empty($settings['custom_icon_height'])? $settings['custom_icon_height']:'' }}" placeholder="in px" class="form-control">
									</div>
									<div class="form-group m-t-15">
										<label for="border">
											Border:
										</label>
										<input class="form-control" type="number" id="border" name="border"  value="{{ !empty($settings['border'])?$settings['border']:'0' }}">
									</div>
									<div class="form-group m-t-15">
										<label for="borderradius">
											Border Radius:
										</label>
										<input class="form-control" type="number" id="borderradius" name="borderradius"  value="{{ !empty($settings['borderradius'])?$settings['borderradius']:'0' }}">
									</div>
									<div class="form-group">
										<label for="iconcolor">Icon Background Color:</label>
										<div data-color-format="rgb" data-color="{{ !empty($settings['iconbgcolor'])? $settings['iconbgcolor']:'rgba(0,0,0)' }}" class="colorpicker-default input-group">
											<input type="text" name="iconbgcolor" value="" class="form-control">
											<span class="input-group-btn add-on">
												<button class="btn btn-white" type="button">
													<i style="background-color:{{ !empty($settings['iconbgcolor'])? $settings['iconbgcolor']:'rgba(0,0,0)' }};margin-top: 2px;"></i>
												</button>
											</span>
										</div>
										<small>Use transparent for default color or remove the color code
									</div>
									<div class="form-group">
										<label for="iconcolor">Icon Color:</label>
										<div data-color-format="rgb" data-color="{{ !empty($settings['iconcolor'])? $settings['iconcolor']:'rgba(255,255,255)' }}" class="colorpicker-default input-group">
											<input type="text" name="iconcolor" value="" class="form-control">
											<span class="input-group-btn add-on">
												<button class="btn btn-white" type="button">
													<i style="background-color:{{ !empty($settings['iconcolor'])? $settings['iconcolor']:'rgba(255,255,255)' }};margin-top: 2px;"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="form-group">
										<label for="bordercolor">Border Color:</label>
										<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
											<input type="text" name="bordercolor" value="" class="form-control">
											<span class="input-group-btn add-on">
												<button class="btn btn-white" type="button">
													<i style="background-color:{{ !empty($settings['bordercolor'])? $settings['bordercolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="form-group m-t-15">
										<label for="style">
											Icon Style:
										</label>
										<select class="form-control" id="iconstyle" name="iconstyle">
											<option value="default_style" {{ !empty($settings['iconstyle']) && $settings['iconstyle']=='default_style'? 'selected':'' }}>Default</option>
											<option value="vertical_style" {{ !empty($settings['iconstyle']) && $settings['iconstyle']=='vertical_style'? 'selected':'' }}>Vertical</option>
										</select>
									</div>
									<div class="form-group m-t-15">
										<div class="checkbox">
											<input id="floating_icon" name="floating_icon" type="checkbox" value="yes" {{ !empty($settings['floating_icon']) && $settings['floating_icon']=='yes'?'checked':'' }}>
											<label for="floating_icon">
												Floating Icon
											</label>
										</div>
									</div>
									@if(!empty($settings['floating_icon']) && $settings['floating_icon']=='yes')
									<div class="form-group m-t-15">
										<label for="left">
											Left:
										</label>
										<input class="form-control" type="number" id="left" name="left"  value="{{ !empty($settings['left'])?$settings['left']:'0' }}">
									</div>
									<div class="form-group m-t-15">
										<label for="top">
											Top:
										</label>
										<input class="form-control" type="number" id="top" name="top"  value="{{ !empty($settings['top'])?$settings['top']:'150' }}">
									</div>
									@endif

							</div>
						</div>
					</form>

				</div>
			</div>
    </div>
</div>
@include('admin._partials.popupuploader')
<script>
jQuery(document).ready(function(){
	jQuery('.repeaters').on('click', '.btn-repeat-add', function(){
		jQuery(this).parents('.repeat').clone().addClass('newrow').insertAfter(jQuery(this).parents('.repeat'));
	})
	$('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
	jQuery('.colorpicker-rgba').colorpicker();
	jQuery('.repeaters').on('click', '.addcustom', function(){
		var html = '<div class="repeat row m-t-15"><div class="col-sm-1"><button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button></div><div class="col-sm-5"><select class="form-control" name="icon[]"><option value="">None</option><option value="facebook">Facebook</option><option value="twitter">Twitter</option><option value="email">Email</option><option value="pinterest">Pinterest</option><option value="linkedin">LinkedIn</option><option value="reddit">Reddit</option><option value="whatsapp">WhatsApp</option><option value="gmail">Gmail</option><option value="telegram">Telegram</option><option value="pocket">Pocket</option><option value="mix">Mix</option><option value="tumblr">Tumblr</option><option value="amazon">Amazon Wish List</option><option value="aol">AOL Mail</option><option value="balatarin">Balatarin</option><option value="bibsonomy">BibSonomy</option><option value="bitty">Bitty Browser</option><option value="blinklist">Blinklist</option><option value="blogger">Blogger</option><option value="blogmarks">BlogMarks</option><option value="bookmarks_fr">Bookmarks.fr</option><option value="box">Box.net</option><option value="buffer">Buffer</option><option value="care2">Care2 News</option><option value="citeulike">CiteULike</option><option value="link">Copy Link</option><option value="designfloat">Design Float</option><option value="diary_ru">Diary.Ru</option><option value="diaspora">Diaspora</option><option value="digg">Digg</option><option value="diigo">Diigo</option><option value="douban">Douban</option><option value="draugiem">Draugiem</option><option value="dzone">DZone</option><option value="evernote">Evernote</option><option value="facebook_messenger">Facebook Messenger</option><option value="fark">Fark</option><option value="flipboard">Flipboard</option><option value="folkd">Folkd</option><option value="google">Google Bookmarks</option><option value="google_classroom">Google Classroom</option><option value="google_plus">Google+</option><option value="y18">Hacker News</option><option value="hatena">Hatena</option><option value="houzz">Houzz</option><option value="instapaper">Instapaper</option><option value="kakao">Kakao</option><option value="kik">Kik</option><option value="kindle">Kindle It</option><option value="known">Known</option><option value="line">Line</option><option value="livejournal">LiveJournal</option><option value="mail_ru">Mail.Ru</option><option value="mastodon">Mastodon</option><option value="mendeley">Mendeley</option><option value="meneame">Meneame</option><option value="mewe">MeWe</option><option value="mixi">Mixi</option><option value="myspace">MySpace</option><option value="netvouz">Netvouz</option><option value="odnoklassniki">Odnoklassniki</option><option value="outlook_com">Outlook.com</option><option value="papaly">Papaly</option><option value="pinboard">Pinboard</option><option value="plurk">Plurk</option><option value="print">Print</option><option value="printfriendly">PrintFriendly</option><option value="protopage">Protopage Bookmarks</option><option value="pusha">Pusha</option><option value="qzone">Qzone</option><option value="rediff">Rediff MyPage</option><option value="refind">Refind</option><option value="renren">Renren</option><option value="sina_weibo">Sina Weibo</option><option value="sitejot">SiteJot</option><option value="skype">Skype</option><option value="slashdot">Slashdot</option><option value="sms">SMS</option><option value="stocktwits">StockTwits</option><option value="svejo">Svejo</option><option value="symbaloo">Symbaloo Bookmarks</option><option value="threema">Threema</option><option value="trello">Trello</option><option value="tuenti">Tuenti</option><option value="twiddla">Twiddla</option><option value="typepad">TypePad Post</option><option value="viadeo">Viadeo</option><option value="viber">Viber</option><option value="vk">VK</option><option value="wanelo">Wanelo</option><option value="wechat">WeChat</option><option value="wordpress">WordPress</option><option value="wykop">Wykop</option><option value="xing">XING</option><option value="yahoo">Yahoo Mail</option><option value="yoolink">Yoolink</option><option value="yummly">Yummly</option></select></div><div class="col-sm-6"><a href="#" class="choosecustomicon btn btn-primary waves-effect waves-light" data-toggle="custommodal" data-target="#media-select"><i class="ion-upload m-r-5"></i>Custom Icon</a><span class="removecustomicon"></span><input type="hidden" name="customicon[]" value="" class="form-control"></div></div>';
		jQuery(this).parents('.repeaters').append(html);
		return false;
	})
	jQuery('.repeaters').on('click', '.btn-repeat-remove', function(){
		jQuery(this).parents('.repeat').remove();
	})
	jQuery('#save').click(function(){
		$('#widgetform').submit();
	})
	$('.repeaters').on('click', '.choosecustomicon', function(){
		$('.repeat').removeClass('active');
		$(this).parents('.repeat').addClass('active');
	})
	$('.allmedia').on('click','.imageholder a',function(){
		$('.repeat.active').find('input[name="customicon[]"]').val($(this).attr('href'));
		$('.repeat.active').find('.removecustomicon').html('<img src="'+$(this).attr('href')+'"><a href="#" class="removeicon"><i class="fa fa-times"></i></a>');
		$('#media-select').removeClass('md-custom-show');
		return false;
	})
	$('.repeaters').on('click', '.removeicon', function(){
		$(this).parents('.removecustomicon').next('input[name="customicon[]"]').val('');
		$(this).parents('.removecustomicon').html('');
		return false;
	})
})
</script>
@endsection
