@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />

<style>
.get{
	display:none;
}
</style>
@endsection

@section('content')
<form method="post" action="{{ route('widget.save') }}">
{{csrf_field()}}
<input type="hidden" name="id" value="{{ $widget->id }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right">
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg m-b-20">Update</button>
						</div>
				</div>
				<div class="card-box">
					<div class="form-group m-t-15">
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Client ID:
						</h4>
						<input class="form-control" type="text" id="clientId" name="clientId"  value="{{ !empty($settings['clientId'])?$settings['clientId']:'' }}">
						<div class="hint">Get Client ID from <a href="https://weblizar.com/get-instagram-client-id/" target="_blank">here</a></div>
					</div>
					<div class="form-group m-t-15">
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Access Token:
						</h4>
						<input class="form-control" type="text" id="accessToken" name="accessToken"  value="{{ !empty($settings['accessToken'])?$settings['accessToken']:'' }}">
						<div class="hint">Get Access Token from <a href="http://instagram.pixelunion.net/" target="_blank">here</a></div>
					</div>
					<div class="form-group m-t-15">
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Fetches:
						</h4>
						<select class="form-control selectpicker" data-style="btn-white" id="get" name="get">
							<option value="popular" {{ !empty($settings['get']) && $settings['get']=='popular'? 'selected':'' }}>Popular</option>
							<option value="tagged" {{ !empty($settings['get']) && $settings['get']=='tagged'? 'selected':'' }}>Tagged</option>
							<option value="location" {{ !empty($settings['get']) && $settings['get']=='location'? 'selected':'' }}>Location</option>
							<option value="user" {{ !empty($settings['get']) && $settings['get']=='user'? 'selected':'' }}>User</option>
						</select>
					</div>
					<div id="tagged" class="form-group get m-t-15" {!! !empty($settings['get']) && $settings['get']=='tagged'? 'style="display:block"':'' !!}>
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Tag Name:
						</h4>
						<input class="form-control" type="text" id="tagName" name="tagName"  value="{{ !empty($settings['tagName'])?$settings['tagName']:'' }}">
					</div>
					<div id="location" class="form-group get m-t-15" {!! !empty($settings['get']) && $settings['get']=='location'? 'style="display:block"':'' !!}>
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Location ID:
						</h4>
						<input class="form-control" type="text" id="locationId" name="locationId"  value="{{ !empty($settings['locationId'])?$settings['locationId']:'' }}">
					</div>
					<div id="user" class="form-group get m-t-15" {!! !empty($settings['get']) && $settings['get']=='user'? 'style="display:block"':'' !!}>
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								User ID:
						</h4>
						<input class="form-control" type="text" id="userId" name="userId"  value="{{ !empty($settings['userId'])?$settings['userId']:'' }}">
						<div class="hint">Get User Id from <a href="https://codeofaninja.com/tools/find-instagram-user-id" target="_blank">here</a></div>
					</div>
					<div class="form-group m-t-15">
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Sort By:
						</h4>
						<select class="form-control selectpicker" data-style="btn-white" id="sortBy" name="sortBy">
							<option value="none" {{ !empty($settings['sortBy']) && $settings['sortBy']=='none'? 'selected':'' }}>None</option>
							<option value="most-recent" {{ !empty($settings['sortBy']) && $settings['sortBy']=='most-recent'? 'selected':'' }}>Most Recent</option>
							<option value="least-recent" {{ !empty($settings['sortBy']) && $settings['sortBy']=='least-recent'? 'selected':'' }}>Least Recent</option>
							<option value="most-liked" {{ !empty($settings['sortBy']) && $settings['sortBy']=='most-liked'? 'selected':'' }}>Most Liked</option>
							<option value="least-liked" {{ !empty($settings['sortBy']) && $settings['sortBy']=='least-liked'? 'selected':'' }}>Least Liked</option>
							<option value="most-commented" {{ !empty($settings['sortBy']) && $settings['sortBy']=='most-commented'? 'selected':'' }}>Most Commented</option>
							<option value="least-commented" {{ !empty($settings['sortBy']) && $settings['sortBy']=='least-commented'? 'selected':'' }}>Least Commented</option>
							<option value="random" {{ !empty($settings['sortBy']) && $settings['sortBy']=='random'? 'selected':'' }}>Random</option>
						</select>
					</div>
					<div class="form-group m-t-15">
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Links:
						</h4>
						<select class="form-control selectpicker" id="links" data-style="btn-white" name="links">
							<option value="1" {{ !empty($settings['links']) && $settings['links']=='1'? 'selected':'' }}>Yes</option>
							<option value="0" selected>No</option>
						</select>
					</div>
					<div class="form-group m-t-15">
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Limit:
						</h4>
						<input class="form-control" type="number" id="limit" name="limit"  value="{{ !empty($settings['limit'])?$settings['limit']:'20' }}">
					</div>
					<div class="form-group m-t-15">
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Resolution:
						</h4>
						<select class="form-control selectpicker" data-style="btn-white" id="resolution" name="resolution">
							<option value="thumbnail" {{ !empty($settings['resolution']) && $settings['resolution']=='thumbnail'? 'selected':'' }}>Thumbnail</option>
							<option value="low_resolution" {{ !empty($settings['resolution']) && $settings['resolution']=='low_resolution'? 'selected':'' }}>Low Resolution</option>
							<option value="standard_resolution" {{ !empty($settings['resolution']) && $settings['resolution']=='standard_resolution'? 'selected':'' }}>Standard Resolution</option>
						</select>
					</div>
					<div class="form-group m-t-15">
						<h4 class="text-muted m-b-15 m-t-15 font-15">
								Template:
						</h4>
						<textarea class="form-control m-b-10" id="template" name="template">{!! !empty($settings['template'])?$settings['template']:'' !!}</textarea>
						<div class="hint">

							<strong>@{{type}}</strong> - the image's type. Can be image or video.<br />
							<strong>@{{width}}</strong> - contains the image's width, in pixels.<br />
							<strong>@{{height}}</strong> - contains the image's height, in pixels.<br />
							<strong>@{{orientation}}</strong> - contains the image's orientation. Can be square, portrait, or landscape.<br />
							<strong>@{{link}}</strong> - URL to view the image on Instagram's website.<br />
							<strong>@{{image}}</strong> - URL of the image source. The size is inherited from the resolution option.<br />
							<strong>@{{id}}</strong> - Unique ID of the image. Useful if you want to use iPhone hooks to open the images directly in the Instagram app.<br />
							<strong>@{{caption}}</strong> - Image's caption text. Defaults to empty string if there isn't one.<br />
							<strong>@{{likes}}</strong> - Number of likes the image has.<br />
							<strong>@{{comments}}</strong> - Number of comments the image has.<br />
							<strong>@{{location}}</strong> - Name of the location associated with the image. Defaults to empty string if there isn't one.<br />
							<strong>@{{model}}</strong> - Full JSON object of the image. If you want to get a property of the image that isn't listed above you access it using dot-notation. (ex: @{{model.filter}} would get the filter used.)
						</div>
					</div>
				</div>
			</div>
    </div>
</div>
<div class="container-fluid">
	<button type="submit" class="btn btn-primary waves-effect waves-light btn-lg">Save</button>
</div>
</form>
<script>
jQuery(document).ready(function(){
	jQuery('#get').change(function(){
		jQuery('.get').hide();
		jQuery('#'+jQuery(this).val()).show();
	})
})
</script>
@endsection
