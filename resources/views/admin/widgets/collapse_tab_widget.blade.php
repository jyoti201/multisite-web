@extends('layouts.app')

@section('extra')

<style>
.panel{
	margin-bottom: 0;
}
.panel-heading {
  padding:0;
}

</style>
<script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
</script>
@endsection

@section('content')
<form method="post" action="{{ route('widget.save') }}">
{{csrf_field()}}
<input type="hidden" name="id" value="{{ $widget->id }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right m-b-20">
							<button class="btn btn-primary waves-effect waves-light btn-lg" type="button" data-toggle="collapse" data-target="#settings" aria-expanded="false" aria-controls="SliderSettings">
						    Settings
						  </button>
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
						</div>
				</div>
				<div class="collapse" id="settings">
					<div class="card-box">
					<div class="row">
							<div class="col-md-6">
								 <h4 class="text-muted m-b-15 font-15">Header Style <label class="switch showhide" style="float: right" data-trigger-show="headerstyle_hover" data-trigger-hide="headerstyle"><input type="checkbox"><span class="slider red"></span><span class="label">Hover/Active</span></label></h4>
								 <div class="headerstyle">
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15">Heading Background Color</p>
										  <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bgcolor'])? $settings['heading_bgcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
										    <input type="text" name="heading_bgcolor" value="" class="form-control">
										    <span class="input-group-btn add-on">
										      <button class="btn btn-white" type="button">
										        <i style="background-color:{{ !empty($settings['heading_bgcolor'])? $settings['heading_bgcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
										      </button>
										    </span>
										  </div>
										</div>
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15">Heading Text Color</p>
										  <div data-color-format="rgba" data-color="{{ !empty($settings['heading_txcolor'])? $settings['heading_txcolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
										    <input type="text" name="heading_txcolor" value="" class="form-control">
										    <span class="input-group-btn add-on">
										      <button class="btn btn-white" type="button">
										        <i style="background-color:{{ !empty($settings['heading_txcolor'])? $settings['heading_txcolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
										      </button>
										    </span>
										  </div>
										</div>
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15">Heading Border Width</p>
										  <div class="row">
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_borderwidth-left" value="{!! !empty($settings['heading_borderwidth-left'])?$settings['heading_borderwidth-left']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
										    </div>
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_borderwidth-right" value="{!! !empty($settings['heading_borderwidth-right'])?$settings['heading_borderwidth-right']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
										    </div>
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_borderwidth-top" value="{!! !empty($settings['heading_borderwidth-top'])?$settings['heading_borderwidth-top']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
										    </div>
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_borderwidth-bottom" value="{!! !empty($settings['heading_borderwidth-bottom'])?$settings['heading_borderwidth-bottom']:'0' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
										    </div>
										  </div>
										</div>
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15"> Heading Border Radius</p>
										  <div class="row">
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_borderradius-left-top" value="{!! !empty($settings['heading_borderradius-left-top'])?$settings['heading_borderradius-left-top']:'0' !!}" rel="txtTooltip" title="Left Top" data-placement="top">
										    </div>
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_borderradius-left-bottom" value="{!! !empty($settings['heading_borderradius-left-bottom'])?$settings['heading_borderradius-left-bottom']:'0' !!}" rel="txtTooltip" title="Left Bottom" data-placement="top">
										    </div>
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_borderradius-right-top" value="{!! !empty($settings['heading_borderradius-right-top'])?$settings['heading_borderradius-right-top']:'0' !!}" rel="txtTooltip" title="Right Top" data-placement="top">
										    </div>
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_borderradius-right-bottom" value="{!! !empty($settings['heading_borderradius-right-bottom'])?$settings['heading_borderradius-right-bottom']:'0' !!}" rel="txtTooltip" title="Right Bottom" data-placement="top">
										    </div>
										  </div>
										</div>
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15"> Heading Border Style</p>
										  <select  data-style="btn-white" name="heading_borderstyle" class="form-control selectpicker">
										    <option value="solid" {{ !empty($settings['heading_borderstyle']) && $settings['heading_borderstyle']=='solid'?'selected':'' }}>Solid</option>
										    <option value="dashed" {{ !empty($settings['heading_borderstyle']) && $settings['heading_borderstyle']=='dashed'?'selected':'' }}>Dashed</option>
										    <option value="dotted" {{ !empty($settings['heading_borderstyle']) && $settings['heading_borderstyle']=='dotted'?'selected':'' }}>Dotted</option>
										    <option value="double" {{ !empty($settings['heading_borderstyle']) && $settings['heading_borderstyle']=='double'?'selected':'' }}>Double</option>
										    <option value="groove" {{ !empty($settings['heading_borderstyle']) && $settings['heading_borderstyle']=='groove'?'selected':'' }}>Groove</option>
										    <option value="ridge" {{ !empty($settings['heading_borderstyle']) && $settings['heading_borderstyle']=='ridge'?'selected':'' }}>Ridge</option>
										  </select>
										</div>
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15"> Heading Padding</p>
										  <div class="row">
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_padding-left" value="{!! !empty($settings['heading_padding-left'])?$settings['heading_padding-left']:'10' !!}" rel="txtTooltip" title="Left" data-placement="top">
										    </div>
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_padding-right" value="{!! !empty($settings['heading_padding-right'])?$settings['heading_padding-right']:'10' !!}" rel="txtTooltip" title="Right" data-placement="top">
										    </div>
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_padding-top" value="{!! !empty($settings['heading_padding-top'])?$settings['heading_padding-top']:'10' !!}" rel="txtTooltip" title="Top" data-placement="top">
										    </div>
										    <div class="col-sm-3">
										      <input type="number" class="form-control" min="0" name="heading_padding-bottom" value="{!! !empty($settings['heading_padding-bottom'])?$settings['heading_padding-bottom']:'10' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
										    </div>
										  </div>
										</div>
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15"> Heading Font Size</p>
										  <input type="number" class="form-control" min="0" name="heading_font" value="{!! !empty($settings['heading_font'])?$settings['heading_font']:'15' !!}">
										</div>
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15">Font Family</p>
										  <select  data-style="btn-white" name="heading_font_family" class="form-control input selectpicker">
										    <option value="">Default</option>
										    @if(!empty(getSetting('imported_font_name','styles')))
										      @foreach(getSetting('imported_font_name','styles') as $val)
										        <option value="{{ $val }}" {!! !empty($settings['heading_font_family']) && $settings['heading_font_family']==$val?'selected':'' !!}>{{ $val }}</option>
										      @endforeach
										    @endif
										  </select>
										</div>
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15">Font Weight</p>
										  <select  data-style="btn-white" name="heading_font_weight" class="form-control input selectpicker">
										    <option value="">Default</option>
										    <option value="300" {!! !empty($settings['heading_font_weight']) && $settings['heading_font_weight']=='300'?'selected':'' !!}>300</option>
												<option value="400" {!! !empty($settings['heading_font_weight']) && $settings['heading_font_weight']=='400'?'selected':'' !!}>400</option>
												<option value="600" {!! !empty($settings['heading_font_weight']) && $settings['heading_font_weight']=='600'?'selected':'' !!}>600</option>
												<option value="700" {!! !empty($settings['heading_font_weight']) && $settings['heading_font_weight']=='700'?'selected':'' !!}>700</option>
												<option value="800" {!! !empty($settings['heading_font_weight']) && $settings['heading_font_weight']=='800'?'selected':'' !!}>800</option>
										  </select>
										</div>
										<div class="forminput-group">
										  <p class="text-muted m-b-15 m-t-15 font-15"> Heading Border Color</p>
										  <div class="row">
										    <div class="col-sm-6 m-t-10">
										      <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bordercolor-left'])? $settings['heading_bordercolor-left']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
										        <input type="text" name="heading_bordercolor-left" value="" class="form-control" rel="txtTooltip" title="Left" data-placement="top">
										        <span class="input-group-btn add-on">
										          <button class="btn btn-white" type="button">
										            <i style="background-color:{{ !empty($settings['heading_bordercolor-left'])? $settings['heading_bordercolor-left']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
										          </button>
										        </span>
										      </div>
										    </div>
										    <div class="col-sm-6 m-t-10">
										      <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bordercolor-right'])? $settings['heading_bordercolor-right']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
										        <input type="text" name="heading_bordercolor-right" value="" class="form-control" rel="txtTooltip" title="Right" data-placement="top">
										        <span class="input-group-btn add-on">
										          <button class="btn btn-white" type="button">
										            <i style="background-color:{{ !empty($settings['heading_bordercolor-right'])? $settings['heading_bordercolor-right']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
										          </button>
										        </span>
										      </div>
										    </div>
										    <div class="col-sm-6 m-t-10">
										      <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bordercolor-top'])? $settings['heading_bordercolor-top']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
										        <input type="text" name="heading_bordercolor-top" value="" class="form-control" rel="txtTooltip" title="Top" data-placement="top">
										        <span class="input-group-btn add-on">
										          <button class="btn btn-white" type="button">
										            <i style="background-color:{{ !empty($settings['heading_bordercolor-top'])? $settings['heading_bordercolor-top']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
										          </button>
										        </span>
										      </div>
										    </div>
										    <div class="col-sm-6 m-t-10">
										      <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bordercolor-bottom'])? $settings['heading_bordercolor-bottom']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
										        <input type="text" name="heading_bordercolor-bottom" value="" class="form-control" rel="txtTooltip" title="Bottom" data-placement="top">
										        <span class="input-group-btn add-on">
										          <button class="btn btn-white" type="button">
										            <i style="background-color:{{ !empty($settings['heading_bordercolor-bottom'])? $settings['heading_bordercolor-bottom']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
										          </button>
										        </span>
										      </div>
										    </div>
										  </div>
										</div>
								</div>
								<div class="headerstyle_hover collapse">
									<div class="forminput-group">
									  <p class="text-muted m-b-15 m-t-15 font-15">Heading Background Color</p>
									  <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bgcolor_hover'])? $settings['heading_bgcolor_hover']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
									    <input type="text" name="heading_bgcolor_hover" value="" class="form-control">
									    <span class="input-group-btn add-on">
									      <button class="btn btn-white" type="button">
									        <i style="background-color:{{ !empty($settings['heading_bgcolor_hover'])? $settings['heading_bgcolor_hover']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									      </button>
									    </span>
									  </div>
									</div>
									<div class="forminput-group">
									  <p class="text-muted m-b-15 m-t-15 font-15">Heading Text Color</p>
									  <div data-color-format="rgba" data-color="{{ !empty($settings['heading_txcolor_hover'])? $settings['heading_txcolor_hover']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
									    <input type="text" name="heading_txcolor_hover" value="" class="form-control">
									    <span class="input-group-btn add-on">
									      <button class="btn btn-white" type="button">
									        <i style="background-color:{{ !empty($settings['heading_txcolor_hover'])? $settings['heading_txcolor_hover']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
									      </button>
									    </span>
									  </div>
									</div>
									<div class="forminput-group">
									  <p class="text-muted m-b-15 m-t-15 font-15">Heading Border Width</p>
									  <div class="row">
									    <div class="col-sm-3">
									      <input type="number" class="form-control" min="0" name="heading_borderwidth-left_hover" value="{!! !empty($settings['heading_borderwidth-left_hover'])?$settings['heading_borderwidth-left_hover']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
									    </div>
									    <div class="col-sm-3">
									      <input type="number" class="form-control" min="0" name="heading_borderwidth-right_hover" value="{!! !empty($settings['heading_borderwidth-right_hover'])?$settings['heading_borderwidth-right_hover']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
									    </div>
									    <div class="col-sm-3">
									      <input type="number" class="form-control" min="0" name="heading_borderwidth-top_hover" value="{!! !empty($settings['heading_borderwidth-top_hover'])?$settings['heading_borderwidth-top_hover']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
									    </div>
									    <div class="col-sm-3">
									      <input type="number" class="form-control" min="0" name="heading_borderwidth-bottom_hover" value="{!! !empty($settings['heading_borderwidth-bottom_hover'])?$settings['heading_borderwidth-bottom_hover']:'0' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
									    </div>
									  </div>
									</div>
									<div class="forminput-group">
									  <p class="text-muted m-b-15 m-t-15 font-15"> Heading Border Radius</p>
									  <div class="row">
									    <div class="col-sm-3">
									      <input type="number" class="form-control" min="0" name="heading_borderradius-left-top_hover" value="{!! !empty($settings['heading_borderradius-left-top_hover'])?$settings['heading_borderradius-left-top_hover']:'0' !!}" rel="txtTooltip" title="Left Top" data-placement="top">
									    </div>
									    <div class="col-sm-3">
									      <input type="number" class="form-control" min="0" name="heading_borderradius-left-bottom_hover" value="{!! !empty($settings['heading_borderradius-left-bottom_hover'])?$settings['heading_borderradius-left-bottom_hover']:'0' !!}" rel="txtTooltip" title="Left Bottom" data-placement="top">
									    </div>
									    <div class="col-sm-3">
									      <input type="number" class="form-control" min="0" name="heading_borderradius-right-top_hover" value="{!! !empty($settings['heading_borderradius-right-top_hover'])?$settings['heading_borderradius-right-top_hover']:'0' !!}" rel="txtTooltip" title="Right Top" data-placement="top">
									    </div>
									    <div class="col-sm-3">
									      <input type="number" class="form-control" min="0" name="heading_borderradius-right-bottom_hover" value="{!! !empty($settings['heading_borderradius-right-bottom_hover'])?$settings['heading_borderradius-right-bottom_hover']:'0' !!}" rel="txtTooltip" title="Right Bottom" data-placement="top">
									    </div>
									  </div>
									</div>
									<div class="forminput-group">
									  <p class="text-muted m-b-15 m-t-15 font-15"> Heading Border Style</p>
									  <select  data-style="btn-white" name="heading_borderstyle_hover" class="form-control selectpicker">
									    <option value="solid" {{ !empty($settings['heading_borderstyle_hover']) && $settings['heading_borderstyle_hover']=='solid'?'selected':'' }}>Solid</option>
									    <option value="dashed" {{ !empty($settings['heading_borderstyle_hover']) && $settings['heading_borderstyle_hover']=='dashed'?'selected':'' }}>Dashed</option>
									    <option value="dotted" {{ !empty($settings['heading_borderstyle_hover']) && $settings['heading_borderstyle_hover']=='dotted'?'selected':'' }}>Dotted</option>
									    <option value="double" {{ !empty($settings['heading_borderstyle_hover']) && $settings['heading_borderstyle_hover']=='double'?'selected':'' }}>Double</option>
									    <option value="groove" {{ !empty($settings['heading_borderstyle_hover']) && $settings['heading_borderstyle_hover']=='groove'?'selected':'' }}>Groove</option>
									    <option value="ridge" {{ !empty($settings['heading_borderstyle_hover']) && $settings['heading_borderstyle_hover']=='ridge'?'selected':'' }}>Ridge</option>
									  </select>
									</div>
									<div class="forminput-group">
										<p class="text-muted m-b-15 m-t-15 font-15">Font Weight</p>
										<select  data-style="btn-white" name="heading_font_weight_hover" class="form-control input selectpicker">
											<option value="">Default</option>
											<option value="300" {!! !empty($settings['heading_font_weight_hover']) && $settings['heading_font_weight_hover']=='300'?'selected':'' !!}>300</option>
											<option value="400" {!! !empty($settings['heading_font_weight_hover']) && $settings['heading_font_weight_hover']=='400'?'selected':'' !!}>400</option>
											<option value="600" {!! !empty($settings['heading_font_weight_hover']) && $settings['heading_font_weight_hover']=='600'?'selected':'' !!}>600</option>
											<option value="700" {!! !empty($settings['heading_font_weight_hover']) && $settings['heading_font_weight_hover']=='700'?'selected':'' !!}>700</option>
											<option value="800" {!! !empty($settings['heading_font_weight_hover']) && $settings['heading_font_weight_hover']=='800'?'selected':'' !!}>800</option>
										</select>
									</div>
									<div class="forminput-group">
									  <p class="text-muted m-b-15 m-t-15 font-15"> Heading Border Color</p>
									  <div class="row">
									    <div class="col-sm-6 m-t-10">
									      <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bordercolor-left_hover'])? $settings['heading_bordercolor-left_hover']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
									        <input type="text" name="heading_bordercolor-left_hover" value="" class="form-control" rel="txtTooltip" title="Left" data-placement="top">
									        <span class="input-group-btn add-on">
									          <button class="btn btn-white" type="button">
									            <i style="background-color:{{ !empty($settings['heading_bordercolor-left_hover'])? $settings['heading_bordercolor-left_hover']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									          </button>
									        </span>
									      </div>
									    </div>
									    <div class="col-sm-6 m-t-10">
									      <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bordercolor-right_hover'])? $settings['heading_bordercolor-right_hover']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
									        <input type="text" name="heading_bordercolor-right_hover" value="" class="form-control" rel="txtTooltip" title="Right" data-placement="top">
									        <span class="input-group-btn add-on">
									          <button class="btn btn-white" type="button">
									            <i style="background-color:{{ !empty($settings['heading_bordercolor-right_hover'])? $settings['heading_bordercolor-right_hover']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									          </button>
									        </span>
									      </div>
									    </div>
									    <div class="col-sm-6 m-t-10">
									      <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bordercolor-top_hover'])? $settings['heading_bordercolor-top_hover']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
									        <input type="text" name="heading_bordercolor-top_hover" value="" class="form-control" rel="txtTooltip" title="Top" data-placement="top">
									        <span class="input-group-btn add-on">
									          <button class="btn btn-white" type="button">
									            <i style="background-color:{{ !empty($settings['heading_bordercolor-top_hover'])? $settings['heading_bordercolor-top_hover']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									          </button>
									        </span>
									      </div>
									    </div>
									    <div class="col-sm-6 m-t-10">
									      <div data-color-format="rgba" data-color="{{ !empty($settings['heading_bordercolor-bottom_hover'])? $settings['heading_bordercolor-bottom_hover']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
									        <input type="text" name="heading_bordercolor-bottom_hover" value="" class="form-control" rel="txtTooltip" title="Bottom" data-placement="top">
									        <span class="input-group-btn add-on">
									          <button class="btn btn-white" type="button">
									            <i style="background-color:{{ !empty($settings['heading_bordercolor-bottom_hover'])? $settings['heading_bordercolor-bottom_hover']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									          </button>
									        </span>
									      </div>
									    </div>
									  </div>
									</div>
								</div>
					</div>
					<div class="col-md-6">
						<h4 class="text-muted m-b-15 m-t-15 font-15">Content Style</h4>
						<div class="contentstyle">
							 <div class="forminput-group">
						     <p class="text-muted m-b-15 m-t-15 font-15">Content Background Color</p>
						     <div data-color-format="rgba" data-color="{{ !empty($settings['content_bgcolor'])? $settings['content_bgcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
						       <input type="text" name="content_bgcolor" value="" class="form-control">
						       <span class="input-group-btn add-on">
						         <button class="btn btn-white" type="button">
						           <i style="background-color:{{ !empty($settings['content_bgcolor'])? $settings['content_bgcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
						         </button>
						       </span>
						     </div>
						   </div>
						   <div class="forminput-group">
						     <p class="text-muted m-b-15 m-t-15 font-15">Content Text Color</p>
						     <div data-color-format="rgba" data-color="{{ !empty($settings['content_txcolor'])? $settings['content_txcolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
						       <input type="text" name="content_txcolor" value="" class="form-control">
						       <span class="input-group-btn add-on">
						         <button class="btn btn-white" type="button">
						           <i style="background-color:{{ !empty($settings['content_txcolor'])? $settings['content_txcolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
						         </button>
						       </span>
						     </div>
						   </div>
						   <div class="forminput-group">
						     <p class="text-muted m-b-15 m-t-15 font-15">Content Border Width</p>
						     <div class="row">
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_borderwidth-left" value="{!! !empty($settings['content_borderwidth-left'])?$settings['content_borderwidth-left']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
						       </div>
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_borderwidth-right" value="{!! !empty($settings['content_borderwidth-right'])?$settings['content_borderwidth-right']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
						       </div>
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_borderwidth-top" value="{!! !empty($settings['content_borderwidth-top'])?$settings['content_borderwidth-top']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
						       </div>
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_borderwidth-bottom" value="{!! !empty($settings['content_borderwidth-bottom'])?$settings['content_borderwidth-bottom']:'0' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
						       </div>
						     </div>
						   </div>
						   <div class="forminput-group">
						     <p class="text-muted m-b-15 m-t-15 font-15"> Content Border Radius</p>
						     <div class="row">
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_borderradius-left-top" value="{!! !empty($settings['content_borderradius-left-top'])?$settings['content_borderradius-left-top']:'0' !!}" rel="txtTooltip" title="Left Top" data-placement="top">
						       </div>
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_borderradius-left-bottom" value="{!! !empty($settings['content_borderradius-left-bottom'])?$settings['content_borderradius-left-bottom']:'0' !!}" rel="txtTooltip" title="Left Bottom" data-placement="top">
						       </div>
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_borderradius-right-top" value="{!! !empty($settings['content_borderradius-right-top'])?$settings['content_borderradius-right-top']:'0' !!}" rel="txtTooltip" title="Right Top" data-placement="top">
						       </div>
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_borderradius-right-bottom" value="{!! !empty($settings['content_borderradius-right-bottom'])?$settings['content_borderradius-right-bottom']:'0' !!}" rel="txtTooltip" title="Right Bottom" data-placement="top">
						       </div>
						     </div>
						   </div>
						   <div class="forminput-group">
						     <p class="text-muted m-b-15 m-t-15 font-15"> Content Border Style</p>
						     <select  data-style="btn-white" name="content_borderstyle" class="form-control selectpicker">
						       <option value="solid" {{ !empty($settings['content_borderstyle']) && $settings['content_borderstyle']=='solid'?'selected':'' }}>Solid</option>
						       <option value="dashed" {{ !empty($settings['content_borderstyle']) && $settings['content_borderstyle']=='dashed'?'selected':'' }}>Dashed</option>
						       <option value="dotted" {{ !empty($settings['content_borderstyle']) && $settings['content_borderstyle']=='dotted'?'selected':'' }}>Dotted</option>
						       <option value="double" {{ !empty($settings['content_borderstyle']) && $settings['content_borderstyle']=='double'?'selected':'' }}>Double</option>
						       <option value="groove" {{ !empty($settings['content_borderstyle']) && $settings['content_borderstyle']=='groove'?'selected':'' }}>Groove</option>
						       <option value="ridge" {{ !empty($settings['content_borderstyle']) && $settings['content_borderstyle']=='ridge'?'selected':'' }}>Ridge</option>
						     </select>
						   </div>
						   <div class="forminput-group">
						     <p class="text-muted m-b-15 m-t-15 font-15"> Content Padding</p>
						     <div class="row">
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_padding-left" value="{!! !empty($settings['content_padding-left'])?$settings['content_padding-left']:'10' !!}" rel="txtTooltip" title="Left" data-placement="top">
						       </div>
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_padding-right" value="{!! !empty($settings['content_padding-right'])?$settings['content_padding-right']:'10' !!}" rel="txtTooltip" title="Right" data-placement="top">
						       </div>
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_padding-top" value="{!! !empty($settings['content_padding-top'])?$settings['content_padding-top']:'10' !!}" rel="txtTooltip" title="Top" data-placement="top">
						       </div>
						       <div class="col-sm-3">
						         <input type="number" class="form-control" min="0" name="content_padding-bottom" value="{!! !empty($settings['content_padding-bottom'])?$settings['content_padding-bottom']:'10' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
						       </div>
						     </div>
						   </div>
						   <div class="forminput-group">
						     <p class="text-muted m-b-15 m-t-15 font-15"> Content Font Size</p>
						     <input type="number" class="form-control" min="0" name="content_font" value="{!! !empty($settings['content_font'])?$settings['content_font']:'15' !!}">
						   </div>
						   <div class="forminput-group">
						     <p class="text-muted m-b-15 m-t-15 font-15">Font Family</p>
						     <select  data-style="btn-white" name="content_font_family" class="form-control input selectpicker">
						       <option value="">Default</option>
						       @if(!empty(getSetting('imported_font_name','styles')))
						         @foreach(getSetting('imported_font_name','styles') as $val)
						           <option value="{{ $val }}" {!! !empty($settings['content_font_family']) && $settings['content_font_family']==$val?'selected':'' !!}>{{ $val }}</option>
						         @endforeach
						       @endif
						     </select>
						   </div>
							 <div class="forminput-group">
								 <p class="text-muted m-b-15 m-t-15 font-15">Font Weight</p>
								 <select  data-style="btn-white" name="content_font_weight" class="form-control input selectpicker">
									 <option value="">Default</option>
									 <option value="300" {!! !empty($settings['content_font_weight']) && $settings['content_font_weight']=='300'?'selected':'' !!}>300</option>
									 <option value="400" {!! !empty($settings['content_font_weight']) && $settings['content_font_weight']=='400'?'selected':'' !!}>400</option>
									 <option value="600" {!! !empty($settings['content_font_weight']) && $settings['content_font_weight']=='600'?'selected':'' !!}>600</option>
									 <option value="700" {!! !empty($settings['content_font_weight']) && $settings['content_font_weight']=='700'?'selected':'' !!}>700</option>
									 <option value="800" {!! !empty($settings['content_font_weight']) && $settings['content_font_weight']=='800'?'selected':'' !!}>800</option>
								 </select>
							 </div>
						   <div class="forminput-group">
						     <p class="text-muted m-b-15 m-t-15 font-15"> Content Border Color</p>
						     <div class="row">
						       <div class="col-sm-6 m-t-10">
						         <div data-color-format="rgba" data-color="{{ !empty($settings['content_bordercolor-left'])? $settings['content_bordercolor-left']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
						           <input type="text" name="content_bordercolor-left" value="" class="form-control" rel="txtTooltip" title="Left" data-placement="top">
						           <span class="input-group-btn add-on">
						             <button class="btn btn-white" type="button">
						               <i style="background-color:{{ !empty($settings['content_bordercolor-left'])? $settings['content_bordercolor-left']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
						             </button>
						           </span>
						         </div>
						       </div>
						       <div class="col-sm-6 m-t-10">
						         <div data-color-format="rgba" data-color="{{ !empty($settings['content_bordercolor-right'])? $settings['content_bordercolor-right']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
						           <input type="text" name="content_bordercolor-right" value="" class="form-control" rel="txtTooltip" title="Right" data-placement="top">
						           <span class="input-group-btn add-on">
						             <button class="btn btn-white" type="button">
						               <i style="background-color:{{ !empty($settings['content_bordercolor-right'])? $settings['content_bordercolor-right']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
						             </button>
						           </span>
						         </div>
						       </div>
						       <div class="col-sm-6 m-t-10">
						         <div data-color-format="rgba" data-color="{{ !empty($settings['content_bordercolor-top'])? $settings['content_bordercolor-top']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
						           <input type="text" name="content_bordercolor-top" value="" class="form-control" rel="txtTooltip" title="Top" data-placement="top">
						           <span class="input-group-btn add-on">
						             <button class="btn btn-white" type="button">
						               <i style="background-color:{{ !empty($settings['content_bordercolor-top'])? $settings['content_bordercolor-top']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
						             </button>
						           </span>
						         </div>
						       </div>
						       <div class="col-sm-6 m-t-10">
						         <div data-color-format="rgba" data-color="{{ !empty($settings['content_bordercolor-bottom'])? $settings['content_bordercolor-bottom']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
						           <input type="text" name="content_bordercolor-bottom" value="" class="form-control" rel="txtTooltip" title="Bottom" data-placement="top">
						           <span class="input-group-btn add-on">
						             <button class="btn btn-white" type="button">
						               <i style="background-color:{{ !empty($settings['content_bordercolor-bottom'])? $settings['content_bordercolor-bottom']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
						             </button>
						           </span>
						         </div>
						       </div>
						     </div>
						   </div>
						 </div>
					</div>
					</div>




									</div>
				</div>
				<div class="card-box">

					<div class="forminput-group m-b-20">
						<h4 class="text-muted m-b-15 m-t-15 font-15">Type</h4>
						<select name="type" class="form-control input selectpicker">
							<option value="tabs" {!! !empty($settings['type']) && $settings['type']=='tabs'?'selected':'' !!}>Tabs</option>
							<option value="collapse" {!! !empty($settings['type']) && $settings['type']=='collapse'?'selected':'' !!}>Collapse</option>
						</select>
					</div>
					
					@if(!empty($settings['type']) && $settings['type']=='collapse')
					<div class="row">
						<div class="col-md-2 forminput-group m-b-20">
							<div class="checkbox" style="margin:0;">
								<input type="checkbox" id="collapsegroup" name="group" {{ !empty($settings['group'])? 'checked' : '' }}>
								<label for="collapsegroup">Group</label>
							</div>
						</div>
						<div class="col forminput-group m-b-20">
							<div class="d-flex align-items-center">
							 <div class="left col-2">
						     Collapse Spacing
							 </div>
							 <div class="right col-9">
								 <div class="row">
								   <div class="col-sm-3">
									 <input type="number" class="form-control" min="0" name="collapse_spacing-left" value="{!! !empty($settings['collapse_spacing-left'])?$settings['collapse_spacing-left']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
								   </div>
								   <div class="col-sm-3">
									 <input type="number" class="form-control" min="0" name="collapse_spacing-right" value="{!! !empty($settings['collapse_spacing-right'])?$settings['collapse_spacing-right']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
								   </div>
								   <div class="col-sm-3">
									 <input type="number" class="form-control" min="0" name="collapse_spacing-top" value="{!! !empty($settings['collapse_spacing-top'])?$settings['collapse_spacing-top']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
								   </div>
								   <div class="col-sm-3">
									 <input type="number" class="form-control" min="0" name="collapse_spacing-bottom" value="{!! !empty($settings['collapse_spacing-bottom'])?$settings['collapse_spacing-bottom']:'0' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
								   </div>
								 </div>
							 </div>
						   </div>
						</div>
					</div>
					@endif
					
					@if(!empty($settings['type']) && $settings['type']=='tabs')
					<div class="row">
						<div class="col-md-3 forminput-group m-b-20">
							<div class="d-flex align-items-center">
								<div class="left" style="margin-right: 15px;">
									Align
								</div>
								<div class="right col-10">
									<select  data-style="btn-white" name="tabs_position" class="form-control selectpicker">
								    <option value="justify-content-left" {{ !empty($settings['tabs_position']) && $settings['tabs_position']=='justify-content-left'?'selected':'' }}>Left</option>
										<option value="justify-content-center" {{ !empty($settings['tabs_position']) && $settings['tabs_position']=='justify-content-center'?'selected':'' }}>Center</option>
								    <option value="justify-content-right" {{ !empty($settings['tabs_position']) && $settings['tabs_position']=='justify-content-right'?'selected':'' }}>Right</option>
								  </select>
								</div>
							</div>
						</div>
							<div class="col-md-2 forminput-group m-b-20">
								<div class="checkbox" style="margin:0;">
									<input type="checkbox" id="verticaltabs" name="vertical" {{ !empty($settings['vertical'])? 'checked' : '' }}>
									<label for="verticaltabs">Vertical</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="d-flex align-items-center">
									<div class="left col-4">
										Vertical Tabs Width (px)
									</div>
									<div class="right col-8">
										<input type="number" name="vertical_width" value="{{ !empty($settings['vertical_width'])? $settings['vertical_width'] : '200' }}" class="form-control">
									</div>
								</div>
							</div>
					</div>
					@endif
				</div>

				<div class="card-box">
					<a href="#" class="btn btn-white btn-lg" id="addsection">Add Section</a>
					<div class="repeaters" id="sortable">
			@if(!empty($settings['random_id']) && sizeof($settings['random_id'])>0)
						@foreach($settings['random_id'] as $key=>$val)
						<div class="repeat row m-t-15">
							<div class="col-md-12 panel panel-default">
							  <div class="panel-heading">
								<h4 class="panel-title">
								  <a data-toggle="collapse" href="#collapse{{ $key }}" class="collapsed" aria-expanded="false">#{{ !empty($settings['collapsetab_title'][$key])? $settings['collapsetab_title'][$key] : 'Title' }}</a>
								</h4>
							  </div>
							  <div id="collapse{{ $key }}" class="panel-collapse collapse" aria-expanded="false">
									<div class="col-sm-12 m-t-10">
										<input type="text" name="collapsetab_title[]" value="{{ !empty($settings['collapsetab_title'][$key])? $settings['collapsetab_title'][$key] : '' }}" class="form-control" placeholder="Title">
									</div>
									<div class="col-sm-2 m-t-10">
										<div class="checkbox" style="margin:0;">
										  <input type="checkbox" id="collapseactive{{ $val }}" name="collapsetab_active_{{ $val }}" {{ !empty($settings['collapsetab_active_'.$val])? 'checked' : '' }}>
										  <label for="collapseactive{{ $val }}">Active</label>
										</div>
									</div>
									<div class="col-sm-2 m-t-10">
										<div class="checkbox" style="margin:0;">
										  <input type="checkbox" id="collapseunique{{ $val }}" name="collapsetab_unique_{{ $val }}" {{ !empty($settings['collapsetab_unique_'.$val])? 'checked' : '' }}>
										  <label for="collapseunique{{ $val }}">Unique</label>
										</div>
									</div>
									<div class="col-sm-4 m-t-10">
										<div data-color-format="rgba" data-color="{{ !empty($settings['collapsetab_bgcolor'][$key])? $settings['collapsetab_bgcolor'][$key] : 'rgba(255,255,255,0)' }}" class="colorpicker-rgba input-group">
											<input type="text" value="{{ !empty($settings['collapsetab_bgcolor'][$key])? $settings['collapsetab_bgcolor'][$key] : 'rgba(255,255,255,0)' }}" name="collapsetab_bgcolor[]" class="form-control" placeholder="Background Color">
											<span class="input-group-btn add-on">
											<button class="btn btn-white" type="button">
											<i style="background-color: {{ !empty($settings['collapsetab_bgcolor'][$key])? $settings['collapsetab_bgcolor'][$key] : 'rgba(255,255,255,0)' }};margin-top: 2px;"></i>
											</button>
											</span>
										</div>
									</div>
									<div class="col-sm-4 m-t-10">
										<div data-color-format="rgba" data-color="{{ !empty($settings['collapsetab_color'][$key])? $settings['collapsetab_color'][$key] : 'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
											<input type="text" value="{{ !empty($settings['collapsetab_color'][$key])? $settings['collapsetab_color'][$key] : 'rgba(0,0,0,1)' }}" name="collapsetab_color[]" class="form-control" placeholder="Text Color">
											<span class="input-group-btn add-on">
											<button class="btn btn-white" type="button">
											<i style="background-color: {{ !empty($settings['collapsetab_color'][$key])? $settings['collapsetab_color'][$key] : 'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
											</button>
											</span>
										</div>
									</div>
									<div class="col-sm-12 m-t-10">
										<textarea class="content" name="collapsetab_content[]" placeholder="Content">{{ !empty($settings['collapsetab_content'][$key])? $settings['collapsetab_content'][$key] : '' }}</textarea>
									</div>
									<div class="col-sm-12 m-t-10">
										<button type="button" class="btn btn-white waves-effect btn-repeat-remove">Remove</button>
									</div>
							  </div>
							</div>
							<input type="hidden" name="random_id[]" value="{{ !empty($settings['random_id'][$key])? $settings['random_id'][$key] : '' }}">
						</div>
						@endforeach
                @endif


					</div>
</div>


<div class="card-box">
					<div class="forminput-group m-b-20">
					  <h4 class="text-muted m-b-15 m-t-15 font-15">Icon <label class="switch showhide" style="margin-left: 10px;" data-trigger-show="icon_active" data-trigger-hide="icon_select"><input type="checkbox"><span class="slider red"></span><span class="label">Active</span></label></h4>

					  <div class="icons icon_select">


@foreach(getIcons() as $key=>$icon)
<div class="radio" style="margin:0;"><input type="radio" id="{{ $key }}" value="{{ $icon }}" name="icon" {{ !empty($settings['icon']) && $settings['icon']==$icon? 'checked' : '' }}><label for="{{ $key }}"><i class="{{ $icon }}"></i></label></div>
@endforeach


					  </div>
					  <div class="icons icon_active collapse">

@foreach(getIcons() as $key=>$icon)
<div class="radio" style="margin:0;"><input type="radio" id="{{ $key }}_active" value="{{ $icon }}" name="icon_active" {{ !empty($settings['icon_active']) && $settings['icon_active']==$icon? 'checked' : '' }}><label for="{{ $key }}_active"><i class="{{ $icon }}"></i></label></div>
@endforeach

					  </div>


					</div>
					<div class="forminput-group m-b-20">
					  <h4 class="text-muted m-b-15 m-t-15 font-15">Icon Position</h4>
					  <select  data-style="btn-white" name="icon_position" class="form-control selectpicker">
					    <option value="left" {{ !empty($settings['icon_position']) && $settings['icon_position']=='left'?'selected':'' }}>Left</option>
					    <option value="right" {{ !empty($settings['icon_position']) && $settings['icon_position']=='right'?'selected':'' }}>Right</option>
					  </select>
					</div>

					<div class="forminput-group">
						<h4 class="text-muted m-b-15 m-t-15 font-15">Icon Color <label class="switch showhide" style="margin-left: 10px;" data-trigger-show="icon_color_active" data-trigger-hide="icon_color"><input type="checkbox"><span class="slider red"></span><span class="label">Active</span></label></h4>
						<div class="icon_color">
							<div data-color-format="rgba" data-color="{{ !empty($settings['icon_color'])? $settings['icon_color']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="icon_color" value="{{ !empty($settings['icon_color'])? $settings['icon_color']:'rgba(0,0,0,1)' }}" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['icon_color'])? $settings['icon_color']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="icon_color_active collapse">
							<div data-color-format="rgba" data-color="{{ !empty($settings['icon_color_active'])? $settings['icon_color_active']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="icon_color_active" value="{{ !empty($settings['icon_color_active'])? $settings['icon_color_active']:'rgba(0,0,0,1)' }}" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['icon_color_active'])? $settings['icon_color_active']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
					</div>

					<div class="forminput-group">
						<h4 class="text-muted m-b-15 m-t-15 font-15">Icon Size</h4>
						<input type="number" class="form-control" min="0" name="icon_size" value="{!! !empty($settings['icon_size'])?$settings['icon_size']:'15' !!}" rel="txtTooltip" title="Left" data-placement="top">
					</div>

				</div>




			</div>
    </div>
</div>
<div class="container-fluid">

</div>
</form>
<script src="{{ asset('assets/private/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
$(document).ready(function () {

	tinymceInit();

	jQuery('#addsection').on('click', function(){
		var rand = Math.floor((Math.random() * 10000000) + 1);
		var html = '<div class="repeat row m-t-15"><div class="col-md-12 panel panel-default"><div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#collapse'+rand+'" class="collapsed" aria-expanded="false">#Title</a></h4></div><div id="collapse'+rand+'" class="panel-collapse collapse" aria-expanded="false"><div class="col-sm-12 m-t-10"><input type="text" name="collapsetab_title[]" value="" class="form-control" placeholder="Title"></div><div class="col-sm-2 m-t-10"><div class="checkbox" style="margin:0;"><input type="checkbox" id="collapseactive'+rand+'" name="collapsetab_active_'+rand+'"><label for="collapseopen'+rand+'">Active</p></div></div><div class="col-sm-2 m-t-10"><div class="checkbox" style="margin:0;"><input type="checkbox" id="collapseunique'+rand+'" name="collapsetab_unique_'+rand+'"><label for="collapseunique'+rand+'">Unique</p></div></div><div class="col-sm-4 m-t-10"><div data-color-format="rgba" data-color="rgba(255,255,255,0)" class="colorpicker-rgba input-group"><input type="text" value="rgba(255,255,255,0)" name="collapsetab_bgcolor[]" class="form-control" placeholder="Background Color"><span class="input-group-btn add-on"><button class="btn btn-white" type="button"><i style="background-color: rgba(255,255,255,0);margin-top: 2px;"></i></button></span></div></div><div class="col-sm-4 m-t-10"><div data-color-format="rgba" data-color="rgba(0,0,0,1)" class="colorpicker-rgba input-group"><input type="text" value="rgba(0,0,0,1)" name="collapsetab_color[]" class="form-control" placeholder="Text Color"><span class="input-group-btn add-on"><button class="btn btn-white" type="button"><i style="background-color: rgba(0,0,0,1);margin-top: 2px;"></i></button></span></div></div><div class="col-sm-12 m-t-10"><textarea class="content" name="collapsetab_content[]" placeholder="Content"></textarea></div><div class="col-sm-12 m-t-10"><button type="button" class="btn btn-white waves-effect btn-repeat-remove">Remove</button></div></div></div><input type="hidden" name="random_id[]" value="'+rand+'"></div>';
		jQuery(this).next('.repeaters').prepend(html);
		tinymceInit();
		$('.selectpicker').selectpicker();
		return false;
	})
	jQuery('.repeaters').on('click', '.btn-repeat-remove', function(){
		jQuery(this).parents('.repeat').remove();
		return false;
	})
});
function tinymceInit(){
	$('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
	$('.colorpicker-rgba').colorpicker();
	if($(".content").length > 0){
		tinymce.init({
				selector: "textarea.content",
				theme: "modern",
				height:300,
				plugins: [
					"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
					"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
					"save table contextmenu directionality template paste textcolor",
					"fontawesome noneditable"
				],
				content_css: 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
				valid_elements : '*[*]',
				noneditable_noneditable_class: 'fa',
				toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons fontawesome",
				extended_valid_elements: 'span[*]',
				style_formats: [
					{title: 'Bold text', inline: 'b'},
					{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
					{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
					{title: 'Example 1', inline: 'span', classes: 'example1'},
					{title: 'Example 2', inline: 'span', classes: 'example2'},
					{title: 'Table styles'},
					{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
				]
			});
	}

	$('.headerstyle .form-control').on('change paste keyup', function(){
		var inputname = $(this).attr('name');
		var inputclass = $(this).attr('class');
		if($(this).prop('type')=='select-one'){
			$('.headerstyle_hover select[name="'+inputname+'_hover"').val($(this).val());
			$('.headerstyle_hover select[name="'+inputname+'_hover"').trigger('change');
		}else{
			$('.headerstyle_hover input[name="'+inputname+'_hover"').val($(this).val());
			$('.headerstyle_hover input[name="'+inputname+'_hover"').trigger('change');
		}
	})

}
</script>
@endsection
