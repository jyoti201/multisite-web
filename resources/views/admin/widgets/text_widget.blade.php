@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<form method="post" action="{{ route('widget.save') }}">
{{csrf_field()}}
<input type="hidden" name="id" value="{{ $widget->id }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
        <div class="d-flex justify-content-between">
            <div class="left">
                  <div class="shortcodeplaceholder">
                      <input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
                      <button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
                          <i class="fa fa-copy"></i>
                      </button>
                  </div>
            </div>
            <div class="right">
              <button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg m-b-20">Update</button>
            </div>
        </div>
				<div class="card-box">
						<textarea id="content" name="content">{{ !empty($settings['content']) ? $settings['content'] :'' }}</textarea>

						<div class="forminput-group">
							<label> Background Color</label>
							<div data-color-format="rgba" data-color="{{ !empty($settings['bgcolor'])? $settings['bgcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="bgcolor" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['bgcolor'])? $settings['bgcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="forminput-group">
							<label> Text Color</label>
							<div data-color-format="rgba" data-color="{{ !empty($settings['txcolor'])? $settings['txcolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
								<input type="text" name="txcolor" value="" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color:{{ !empty($settings['txcolor'])? $settings['txcolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="forminput-group">
							<label> Border Width</label>
							<div class="row">
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="borderwidth-left" value="{!! !empty($settings['borderwidth-left'])?$settings['borderwidth-left']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="borderwidth-right" value="{!! !empty($settings['borderwidth-right'])?$settings['borderwidth-right']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="borderwidth-top" value="{!! !empty($settings['borderwidth-top'])?$settings['borderwidth-top']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="borderwidth-bottom" value="{!! !empty($settings['borderwidth-bottom'])?$settings['borderwidth-bottom']:'0' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
								</div>
							</div>
						</div>
						<div class="forminput-group">
							<label> Border Radius</label>
							<div class="row">
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="borderradius-left-top" value="{!! !empty($settings['borderradius-left-top'])?$settings['borderradius-left-top']:'0' !!}" rel="txtTooltip" title="Left Top" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="borderradius-left-bottom" value="{!! !empty($settings['borderradius-left-bottom'])?$settings['borderradius-left-bottom']:'0' !!}" rel="txtTooltip" title="Left Bottom" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="borderradius-right-top" value="{!! !empty($settings['borderradius-right-top'])?$settings['borderradius-right-top']:'0' !!}" rel="txtTooltip" title="Right Top" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="borderradius-right-bottom" value="{!! !empty($settings['borderradius-right-bottom'])?$settings['borderradius-right-bottom']:'0' !!}" rel="txtTooltip" title="Right Bottom" data-placement="top">
								</div>
							</div>
						</div>
						<div class="forminput-group">
							<label> Border Style</label>
							<select name="borderstyle" class="form-control">
								<option value="solid" {{ !empty($settings['borderstyle']) && $settings['borderstyle']=='solid'?'selected':'' }}>Solid</option>
								<option value="dashed" {{ !empty($settings['borderstyle']) && $settings['borderstyle']=='dashed'?'selected':'' }}>Dashed</option>
								<option value="dotted" {{ !empty($settings['borderstyle']) && $settings['borderstyle']=='dotted'?'selected':'' }}>Dotted</option>
								<option value="double" {{ !empty($settings['borderstyle']) && $settings['borderstyle']=='double'?'selected':'' }}>Double</option>
								<option value="groove" {{ !empty($settings['borderstyle']) && $settings['borderstyle']=='groove'?'selected':'' }}>Groove</option>
								<option value="ridge" {{ !empty($settings['borderstyle']) && $settings['borderstyle']=='ridge'?'selected':'' }}>Ridge</option>
							</select>
						</div>
						<div class="forminput-group">
							<label> Padding</label>
							<div class="row">
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="padding-left" value="{!! !empty($settings['padding-left'])?$settings['padding-left']:'10' !!}" rel="txtTooltip" title="Left" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="padding-right" value="{!! !empty($settings['padding-right'])?$settings['padding-right']:'10' !!}" rel="txtTooltip" title="Right" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="padding-top" value="{!! !empty($settings['padding-top'])?$settings['padding-top']:'10' !!}" rel="txtTooltip" title="Top" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="padding-bottom" value="{!! !empty($settings['padding-bottom'])?$settings['padding-bottom']:'10' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
								</div>
							</div>
						</div>
						<div class="forminput-group">
							<label> Margin</label>
							<div class="row">
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="margin-left" value="{!! !empty($settings['margin-left'])?$settings['margin-left']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="margin-right" value="{!! !empty($settings['margin-right'])?$settings['margin-right']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="margin-top" value="{!! !empty($settings['margin-top'])?$settings['margin-top']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
								</div>
								<div class="col-sm-3">
									<input type="number" class="form-control" min="0" name="margin-bottom" value="{!! !empty($settings['margin-bottom'])?$settings['margin-bottom']:'10' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
								</div>
							</div>
							<div class="help"></div>
						</div>
						<div class="forminput-group">
							<label> Font Size</label>
							<input type="number" class="form-control" min="0" name="font" value="{!! !empty($settings['font'])?$settings['font']:'15' !!}">
						</div>
						<div class="forminput-group">
							<label>Font Family</label>
							<select name="font_family" class="form-control input">
							  <option value="">Default</option>
								@if(!empty(getSetting('imported_font_name','styles')))
									@foreach(getSetting('imported_font_name','styles') as $val)
										<option value="{{ $val }}" {!! !empty($settings['font_family']) && $settings['font_family']==$val?'selected':'' !!}>{{ $val }}</option>
									@endforeach
								@endif
							</select>
						</div>
						<div class="forminput-group">
							<label> Border Color</label>
							<div class="row">
								<div class="col-sm-6 m-t-10">
									<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor-left'])? $settings['bordercolor-left']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
										<input type="text" name="bordercolor-left" value="" class="form-control" rel="txtTooltip" title="Left" data-placement="top">
										<span class="input-group-btn add-on">
											<button class="btn btn-white" type="button">
												<i style="background-color:{{ !empty($settings['bordercolor-left'])? $settings['bordercolor-left']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-sm-6 m-t-10">
									<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor-right'])? $settings['bordercolor-right']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
										<input type="text" name="bordercolor-right" value="" class="form-control" rel="txtTooltip" title="Right" data-placement="top">
										<span class="input-group-btn add-on">
											<button class="btn btn-white" type="button">
												<i style="background-color:{{ !empty($settings['bordercolor-right'])? $settings['bordercolor-right']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-sm-6 m-t-10">
									<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor-top'])? $settings['bordercolor-top']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
										<input type="text" name="bordercolor-top" value="" class="form-control" rel="txtTooltip" title="Top" data-placement="top">
										<span class="input-group-btn add-on">
											<button class="btn btn-white" type="button">
												<i style="background-color:{{ !empty($settings['bordercolor-top'])? $settings['bordercolor-top']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-sm-6 m-t-10">
									<div data-color-format="rgba" data-color="{{ !empty($settings['bordercolor-bottom'])? $settings['bordercolor-bottom']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
										<input type="text" name="bordercolor-bottom" value="" class="form-control" rel="txtTooltip" title="Bottom" data-placement="top">
										<span class="input-group-btn add-on">
											<button class="btn btn-white" type="button">
												<i style="background-color:{{ !empty($settings['bordercolor-bottom'])? $settings['bordercolor-bottom']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
											</button>
										</span>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>

    </div>
</div>
<div class="container-fluid">

</div>
</form>
<script src="{{ asset('assets/private/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
$(document).ready(function () {
	$('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
	$('.colorpicker-rgba').colorpicker();
	if($("#content").length > 0){
		tinymce.init({
				selector: "textarea#content",
				theme: "modern",
				height:300,
				plugins: [
					"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
					"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
					"save table contextmenu directionality template paste textcolor",
					"fontawesome noneditable"
				],
				content_css: 'https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
				valid_elements : '*[*]',
				noneditable_noneditable_class: 'fa',
				toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons fontawesome",
				extended_valid_elements: 'span[*]',
				style_formats: [
					{title: 'Bold text', inline: 'b'},
					{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
					{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
					{title: 'Example 1', inline: 'span', classes: 'example1'},
					{title: 'Example 2', inline: 'span', classes: 'example2'},
					{title: 'Table styles'},
					{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
				]
			});
	}
});
</script>
@endsection
