@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<form method="post" action="{{ route('widget.save') }}">
{{csrf_field()}}
<input type="hidden" name="id" value="{{ $widget->id }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
        <div class="d-flex justify-content-between">
            <div class="left">
                  <div class="shortcodeplaceholder">
                      <input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
                      <button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
                          <i class="fa fa-copy"></i>
                      </button>
                  </div>
            </div>
            <div class="right">
              <button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg m-b-20">Update</button>
            </div>
        </div>
				<div class="card-box">
					<div class="form-group">
						<label for="style">Search result Layout:</label>
						<select data-style="btn-white" class="form-control selectpicker" id="style" name="style">
							<option value="layout1" {{ !empty($settings['style']) && $settings['style']=='layout1'? 'selected':'' }}>Layout 1</option>
							<option value="layout2" {{ !empty($settings['style']) && $settings['style']=='layout2'? 'selected':'' }}>Layout 2</option>
							<option value="layout3" {{ !empty($settings['style']) && $settings['style']=='layout3'? 'selected':'' }}>Layout 3</option>
							<option value="layout4" {{ !empty($settings['style']) && $settings['style']=='layout4'? 'selected':'' }}>Layout 4</option>
							<option value="layout5" {{ !empty($settings['style']) && $settings['style']=='layout5'? 'selected':'' }}>Layout 5</option>
						</select>
					</div>
					<div class="form-group">
						<label for="searchpage">Search Page:</label>
						<select data-style="btn-white" class="form-control selectpicker" id="searchpage" name="searchpage">
							<option value="">None</option>
						@foreach($pages as $page)
							<option value="{{ $page->slug }}" {{ !empty($settings['searchpage']) && $settings['searchpage']==$page->slug? 'selected':'' }}>{{ $page->title }}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="style">Search input placeholder Text:</label>
						<input type="text" class="form-control" name="placeholder" value="{{ !empty($settings['placeholder'])?$settings['placeholder']:'' }}">
					</div>
					<div class="form-group">
						<label for="style">Search button Text:</label>
						<input type="text" class="form-control" name="buttontext" value="{{ !empty($settings['buttontext'])?$settings['buttontext']:'' }}">
					</div>
					<div class="form-group">
						<div class="checkbox">
							<input id="showicon" name="showicon" type="checkbox" value="yes" {{ !empty($settings['showicon']) && $settings['showicon']=='yes'?'checked':'' }}>
							<label for="showicon">
								Show Icon
							</label>
						</div>
					</div>
				</div>
			</div>
    </div>
</div>
</form>

@endsection
