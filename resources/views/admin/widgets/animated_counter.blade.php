@extends('layouts.app')

@section('extra')

<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />
<style>
.choosemedia{
	    border: 3px dashed;
    min-height: 300px;
    position: relative;
}
.choosemedia.has-image{
	display:inline-block;
}
.choosemedia a{
	    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    text-align: center;
    z-index: 99;
}
.choosemedia a .fa{
	    position: absolute;
    font-size: 45px;
    color: #000000;
    top: 50%;
    transform: translateY(-50%) translateX(-50%);
	-webkit-transform: translateY(-50%) translateX(-50%);
    left: 50%;
}
#preview{
	background:url({{ asset('assets/public/images/preview.png') }}) no-repeat center;
	min-height:100px;
	border: 2px dashed #eaeaea;
	margin-bottom:10px;
}
@media (min-width: 992px){
.repeaters .newrow{
	width: 48%;
float: left;
margin-right: 2%;
}
}
.grid figure{
	float:none;
}
.btn-repeat-remove{
	display:none;
}
.newrow .btn-repeat-remove{
	display:block;
}
</style>
<script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
</script>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<form method="post" id="widgetform" action="{{ route('widget.save') }}">
				{{csrf_field()}}
				<input type="hidden" name="id" value="{{ $widget->id }}">
				<div class="d-flex justify-content-between">
						<div class="left">
									<div class="shortcodeplaceholder">
											<input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
											<button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
													<i class="fa fa-copy"></i>
											</button>
									</div>
						</div>
						<div class="right m-b-20">
							<button class="btn btn-primary waves-effect waves-light btn-lg" type="button" data-toggle="collapse" data-target="#settings" aria-expanded="false" aria-controls="Settings">
						     Counter Setting
						  </button>
							<button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
						</div>
				</div>
				<div class="collapse" id="settings">
								<div class="card-box">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Display in Columns</h4>
												<input class="form-control" type="number" id="columns" max="6" min="0" name="columns"  value="{{ !empty($settings['columns'])?$settings['columns']:'0' }}">
											</div>
											<div class="form-group m-t-15">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Width (in px)</h4>
												<input class="form-control" type="number" id="width" name="width"  value="{{ !empty($settings['width'])?$settings['width']:'200' }}">
											</div>
											<div class="form-group m-t-15">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Height (in px)</h4>
												<input class="form-control" type="number" id="height" name="height"  value="{{ !empty($settings['height'])?$settings['height']:'200' }}">
											</div>
											<div class="form-group m-t-15">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Font Size (in px)</h4>
												<div class="row">
													<div class="col-md-4">
															<p class="text-muted m-b-15 font-15">Progress Number</p>
															<input class="form-control" type="number" id="number_font_size" name="number_font_size"  value="{{ !empty($settings['number_font_size'])?$settings['number_font_size']:'15' }}">
													</div>
													<div class="col-md-4">
														<p class="text-muted m-b-15 font-15">Before Text</p>
														<input class="form-control" type="number" id="before_text_font_size" name="before_text_font_size"  value="{{ !empty($settings['before_text_font_size'])?$settings['before_text_font_size']:'20' }}">
													</div>
													<div class="col-md-4">
														<p class="text-muted m-b-15 font-15">After Text</p>
														<input class="form-control" type="number" id="after_text_font_size" name="after_text_font_size"  value="{{ !empty($settings['after_text_font_size'])?$settings['after_text_font_size']:'20' }}">
													</div>
												</div>
											</div>
											<div class="form-group m-t-15">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Spacing (in px)</h4>
												<div class="row">
													<div class="col-md-4">
															<p class="text-muted m-b-15 font-15">Progress Number</p>
															<input class="form-control" type="number" id="number_spacing" name="number_spacing"  value="{{ !empty($settings['number_spacing'])?$settings['number_spacing']:'0' }}">
													</div>
													<div class="col-md-4">
														<p class="text-muted m-b-15 font-15">Before Text (in px)</p>
														<input class="form-control" type="number" id="before_text_spacing" name="before_text_spacing"  value="{{ !empty($settings['before_text_spacing'])?$settings['before_text_spacing']:'0' }}">
													</div>
													<div class="col-md-4">
														<p class="text-muted m-b-15 font-15">After Text (in px)</p>
														<input class="form-control" type="number" id="after_text_spacing" name="after_text_spacing"  value="{{ !empty($settings['after_text_spacing'])?$settings['after_text_spacing']:'0' }}">
													</div>
												</div>
											</div>
											<div class="form-group m-t-15">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Font Family</h4>
												<div class="row">
													<div class="col-md-4">
															<p class="text-muted m-b-15 font-15">Progress Number</p>
															<select data-style="btn-white" name="number_font_family" class="form-control input selectpicker">
															  <option value="">Default</option>
																@if(!empty(getSetting('imported_font_name','styles')))
																	@foreach(getSetting('imported_font_name','styles') as $val)
																		<option value="{{ $val }}" {!! !empty($settings['number_font_family']) && $settings['number_font_family']==$val?'selected':'' !!}>{{ $val }}</option>
																	@endforeach
																@endif
															</select>
													</div>
													<div class="col-md-4">
														<p class="text-muted m-b-15 font-15">Before Text</p>
														<select data-style="btn-white" name="before_text_font_family" class="form-control input selectpicker">
															<option value="">Default</option>
															@if(!empty(getSetting('imported_font_name','styles')))
																@foreach(getSetting('imported_font_name','styles') as $val)
																	<option value="{{ $val }}" {!! !empty($settings['before_text_font_family']) && $settings['before_text_font_family']==$val?'selected':'' !!}>{{ $val }}</option>
																@endforeach
															@endif
														</select>
													</div>
													<div class="col-md-4">
														<p class="text-muted m-b-15 font-15">After Text</p>
														<select data-style="btn-white" name="after_text_font_family" class="form-control input selectpicker">
															<option value="">Default</option>
															@if(!empty(getSetting('imported_font_name','styles')))
																@foreach(getSetting('imported_font_name','styles') as $val)
																	<option value="{{ $val }}" {!! !empty($settings['after_text_font_family']) && $settings['after_text_font_family']==$val?'selected':'' !!}>{{ $val }}</option>
																@endforeach
															@endif
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
				</div>
				<div class="card-box">
						<div class="container-fluid">
							<div class="row no-gutters">
								<div class="col-md-12"><a href="" class="btn btn-white btn-lg addcounter">Add Counter</a></div>
								<div class="col-md-12 repeaters" id="sortable">
									@if(!empty($settings['type']))
										@foreach($settings['type'] as $key=>$val)
											<div class="repeat newrow m-t-15">
												<div class="panel-group">
  													<div class="panel panel-default">
																<div class="panel-heading">
														      <h4 class="panel-title">
														        <a data-toggle="collapse" href="#counter{{ $key }}">#Counter {{ $key+1 }}</a>
														      </h4>
														    </div>

																<div id="counter{{ $key }}" class="well panel-collapse collapse">
																	<div class="row m-t-15">
																		<div class="col-sm-6">
																			<label>Type</label>
																			<select data-style="btn-white" name="type[]" class="form-control selectpicker">
																				<option value="none" {{ !empty($settings['type'][$key]) && $settings['type'][$key]=='none'? 'selected':'' }}>None</option>
																				<option value="Line" {{ !empty($settings['type'][$key]) && $settings['type'][$key]=='Line'? 'selected':'' }}>Line</option>
																				<option value="Circle" {{ !empty($settings['type'][$key]) && $settings['type'][$key]=='Circle'? 'selected':'' }}>Circle</option>
																				<option value="SemiCircle" {{ !empty($settings['type'][$key]) && $settings['type'][$key]=='SemiCircle'? 'selected':'' }}>SemiCircle</option>
																				<option value="Path" {{ !empty($settings['type'][$key]) && $settings['type'][$key]=='Path'? 'selected':'' }}>Custom</option>
																			</select>
																		</div>
																		<div class="col-sm-6">
																			<label>Easing</label>
																			<select data-style="btn-white" name="easing[]" class="form-control selectpicker">
																				<option value="linear" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='linear'? 'selected':'' }}>linear</option>
																				<option value="swing" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='swing'? 'selected':'' }}>swing</option>
																				<option value="_default" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='_default'? 'selected':'' }}>_default</option>
																				<option value="easeInQuad" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInQuad'? 'selected':'' }}>easeInQuad</option>
																				<option value="easeOutQuad" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutQuad'? 'selected':'' }}>easeOutQuad</option>
																				<option value="easeInOutQuad" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutQuad'? 'selected':'' }}>easeInOutQuad</option>
																				<option value="easeInCubic" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInCubic'? 'selected':'' }}>easeInCubic</option>
																				<option value="easeOutCubic" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutCubic'? 'selected':'' }}>easeOutCubic</option>
																				<option value="easeInOutCubic" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutCubic'? 'selected':'' }}>easeInOutCubic</option>
																				<option value="easeInQuart" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInQuart'? 'selected':'' }}>easeInQuart</option>
																				<option value="easeOutQuart" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutQuart'? 'selected':'' }}>easeOutQuart</option>
																				<option value="easeInOutQuart" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutQuart'? 'selected':'' }}>easeInOutQuart</option>
																				<option value="easeInQuint" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInQuint'? 'selected':'' }}>easeInQuint</option>
																				<option value="easeOutQuint" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutQuint'? 'selected':'' }}>easeOutQuint</option>
																				<option value="easeInOutQuint" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutQuint'? 'selected':'' }}>easeInOutQuint</option>
																				<option value="easeInExpo" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInExpo'? 'selected':'' }}>easeInExpo</option>
																				<option value="easeOutExpo" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutExpo'? 'selected':'' }}>easeOutExpo</option>
																				<option value="easeInOutExpo" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutExpo'? 'selected':'' }}>easeInOutExpo</option>
																				<option value="easeInSine" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInSine'? 'selected':'' }}>easeInSine</option>
																				<option value="easeOutSine" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutSine'? 'selected':'' }}>easeOutSine</option>
																				<option value="easeInOutSine" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutSine'? 'selected':'' }}>easeInOutSine</option>
																				<option value="easeInCirc" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInCirc'? 'selected':'' }}>easeInCirc</option>
																				<option value="easeOutCirc" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutCirc'? 'selected':'' }}>easeOutCirc</option>
																				<option value="easeInOutCirc" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutCirc'? 'selected':'' }}>easeInOutCirc</option>
																				<option value="easeInElastic" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInElastic'? 'selected':'' }}>easeInElastic</option>
																				<option value="easeOutElastic" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutElastic'? 'selected':'' }}>easeOutElastic</option>
																				<option value="easeInOutElastic" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutElastic'? 'selected':'' }}>easeInOutElastic</option>
																				<option value="easeInBack" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInBack'? 'selected':'' }}>easeInBack</option>
																				<option value="easeOutBack" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutBack'? 'selected':'' }}>easeOutBack</option>
																				<option value="easeInOutBack" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutBack'? 'selected':'' }}>easeInOutBack</option>
																				<option value="easeInBounce" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInBounce'? 'selected':'' }}>easeInBounce</option>
																				<option value="easeOutBounce" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeOutBounce'? 'selected':'' }}>easeOutBounce</option>
																				<option value="easeInOutBounce" {{ !empty($settings['easing'][$key]) && $settings['easing'][$key]=='easeInOutBounce'? 'selected':'' }}>easeInOutBounce</option>
																			</select>
																		</div>
																	</div>
																	<div class="row m-t-15">
																		<div class="col-sm-12">
																			<label>Before text</label>
																			<input type="text" name="before_text[]" class="form-control" placeholder="Before text" value="{{ !empty($settings['before_text'][$key])? $settings['before_text'][$key]:'' }}">
																			<div class="checkbox checkbox-primary">
																				<input name="before_text_block[]" id="before_text_block{{ $key }}" value="yes" type="checkbox" {{ !empty($settings['before_text_block'][$key]) && $settings['before_text_block'][$key]=='yes'? 'checked':'' }}>
																				<label for="before_text_block{{ $key }}">Block</label>
																			</div>
																		</div>
																	</div>
																	<div class="row m-t-15">
																		<div class="col-sm-12">
																			<label>Number</label>
																			<input type="number" name="number[]" placeholder="Number" class="form-control" value="{{ !empty($settings['number'][$key])? $settings['number'][$key]:'' }}">
																			<div class="checkbox checkbox-primary">
																				<input name="number_block[]" id="number_block{{ $key }}" value="yes" type="checkbox" {{ !empty($settings['number_block'][$key]) && $settings['number_block'][$key]=='yes'? 'checked':'' }}>
																				<label for="number_block{{ $key }}">Block</label>
																			</div>
																		</div>
																	</div>
																	<div class="row m-t-15">
																		<div class="col-sm-12">
																			<label>After text</label>
																			<input type="text" placeholder="After text" name="after_text[]" class="form-control" value="{{ !empty($settings['after_text'][$key])? $settings['after_text'][$key]:'' }}">
																			<div class="checkbox checkbox-primary">
																				<input name="after_text_block[]" id="after_text_block{{ $key }}" value="yes" type="checkbox" {{ !empty($settings['after_text_block'][$key]) && $settings['after_text_block'][$key]=='yes'? 'checked':'' }}>
																				<label for="after_text_block{{ $key }}">Block</label>
																			</div>
																		</div>
																	</div>
																	<div class="row m-t-15">
																		<div class="col-sm-6">
																			<label>Duration</label>
																			<input type="number" min="0" name="duration[]" class="form-control" placeholder="Duration" value="{{ !empty($settings['duration'][$key])? $settings['duration'][$key]:'1400' }}">
																		</div>
																		<div class="col-sm-6">
																			<label>Stroke Width</label>
																			<input type="number" min="0" name="strokeWidth[]" class="form-control"  placeholder="Stroke Width" value="{{ !empty($settings['strokeWidth'][$key])? $settings['strokeWidth'][$key]:'0' }}">
																		</div>
																	</div>
																	<div class="row m-t-15">
																		<div class="col-sm-6">
																			<label>Trail Width</label>
																			<input type="number" min="0" name="trailWidth[]" class="form-control"  placeholder="Trail Width" value="{{ !empty($settings['trailWidth'][$key])? $settings['trailWidth'][$key]:'0' }}">
																		</div>
																		<div class="col-sm-6">
																			<label>Trail Color</label>
																			<div data-color-format="rgb" data-color="{{ !empty($settings['trailColor'][$key])? $settings['trailColor'][$key]:'rgba(0,0,0)' }}" class="colorpicker-default input-group">
																				<input type="text" name="trailColor[]" class="form-control"  placeholder="Trail Color" value="{{ !empty($settings['trailColor'][$key])? $settings['trailColor'][$key]:'' }}">
																				<span class="input-group-btn add-on">
																					<button class="btn btn-white" type="button">
																						<i style="background-color:{{ !empty($settings['trailColor'][$key])? $settings['trailColor'][$key]:'rgba(0,0,0)' }};margin-top: 2px;"></i>
																					</button>
																				</span>
																			</div>
																		</div>
																	</div>
																	<div class="row m-t-15">
																		<div class="col-sm-12">
																			<label>Loader Solid Color</label>
																			<div data-color-format="rgb" data-color="{{ !empty($settings['color'][$key])? $settings['color'][$key]:'rgba(0,0,0)' }}" class="colorpicker-default input-group">
																				<input type="text" name="color[]" class="form-control"  placeholder="Color" value="{{ !empty($settings['color'][$key])? $settings['color'][$key]:'' }}">
																				<span class="input-group-btn add-on">
																					<button class="btn btn-white" type="button">
																						<i style="background-color:{{ !empty($settings['color'][$key])? $settings['color'][$key]:'rgba(0,0,0)' }};margin-top: 2px;"></i>
																					</button>
																				</span>
																			</div>
																		</div>
																	</div>
																	<div class="row m-t-15">
																		<div class="col-sm-12">
																			<div class="checkbox checkbox-primary">
																				<input name="gradient_color[]" id="gradient_color{{ $key }}" value="yes" type="checkbox" {{ !empty($settings['gradient_color'][$key]) && $settings['gradient_color'][$key]=='yes'? 'checked':'' }}>
																				<label for="gradient_color{{ $key }}">Loader Gradient Color</label>
																			</div>
																		</div>
																	</div>
																	<div class="row m-t-15">
																		<div class="col-sm-6">
																			<label>Loader Color From</label>
																			<div data-color-format="rgb" data-color="{{ !empty($settings['color_from'][$key])? $settings['color_from'][$key]:'rgba(0,0,0)' }}" class="colorpicker-default input-group">
																				<input type="text" name="color_from[]" class="form-control"  placeholder="Loader Color From" value="{{ !empty($settings['color_from'][$key])? $settings['color_from'][$key]:'' }}">
																				<span class="input-group-btn add-on">
																					<button class="btn btn-white" type="button">
																						<i style="background-color:{{ !empty($settings['color_from'][$key])? $settings['color_from'][$key]:'rgba(0,0,0)' }};margin-top: 2px;"></i>
																					</button>
																				</span>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<label>Loader Color To</label>
																			<div data-color-format="rgb" data-color="{{ !empty($settings['color_to'][$key])? $settings['color_to'][$key]:'rgba(0,0,0)' }}" class="colorpicker-default input-group">
																				<input type="text" name="color_to[]" class="form-control"  placeholder="Loader Color To" value="{{ !empty($settings['color_to'][$key])? $settings['color_to'][$key]:'' }}">
																				<span class="input-group-btn add-on">
																					<button class="btn btn-white" type="button">
																						<i style="background-color:{{ !empty($settings['color_to'][$key])? $settings['color_to'][$key]:'rgba(0,0,0)' }};margin-top: 2px;"></i>
																					</button>
																				</span>
																			</div>
																		</div>
																	</div>
																	<div class="row m-t-15">
																		<div class="col-sm-6">
																			<label>Progress Percentage</label>
																			<input type="number" max="100" min="0" name="progress[]" class="form-control"  placeholder="Progress Percentage" value="{{ !empty($settings['progress'][$key])? $settings['progress'][$key]:'100' }}">
																		</div>
																		<div class="col-sm-6">
																			<label>Custom Classes</label>
																			<input type="text" name="class[]" class="form-control"  placeholder="Class" value="{{ !empty($settings['class'][$key])? $settings['class'][$key]:'' }}">
																		</div>
																	</div>
																	<div class="row  m-t-15">
																		<div class="col-sm-12">
																			<button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:inline-block">Del</button> <button type="button" class="btn btn-white waves-effect btn-repeat-duplicate" style="display:inline-block">Duplicate</button>
																		</div>
																	</div>
																</div>

												</div>
											</div>



											</div>
										@endforeach
									@endif
								</div>



							</div>
						</div>
				</div>

				</form>
			</div>
    </div>
</div>

<script>
$(document).ready(function(){
		$('#save').on('click', function(){
				$('#widgetform').submit();
		})
})
</script>
<script>
jQuery(document).ready(function(){
	jQuery('.addcounter').on('click', function(){
		var rand = Math.floor((Math.random() * 100000) + 1);
		var html = '<div class="repeat newrow m-t-15"><div class="well">';
		html+='<div class="row m-t-15"><div class="col-sm-6"><label>Type</label><select data-style="btn-white" name="type[]" class="form-control selectpicker"><option value="none">None</option><option value="Line">Line</option><option value="Circle">Circle</option><option value="SemiCircle">SemiCircle</option><option value="Path">Custom</option></select></div>';
		html+='<div class="col-sm-6"><label>Easing</label><select data-style="btn-white" name="easing[]" class="form-control selectpicker"><option value="linear">linear</option><option value="swing">swing</option><option value="_default">_default</option><option value="easeInQuad">easeInQuad</option><option value="easeOutQuad">easeOutQuad</option><option value="easeInOutQuad">easeInOutQuad</option><option value="easeInCubic">easeInCubic</option><option value="easeOutCubic">easeOutCubic</option><option value="easeInOutCubic">easeInOutCubic</option><option value="easeInQuart">easeInQuart</option><option value="easeOutQuart">easeOutQuart</option><option value="easeInOutQuart">easeInOutQuart</option><option value="easeInQuint">easeInQuint</option><option value="easeOutQuint">easeOutQuint</option><option value="easeInOutQuint">easeInOutQuint</option><option value="easeInExpo">easeInExpo</option><option value="easeOutExpo">easeOutExpo</option><option value="easeInOutExpo">easeInOutExpo</option><option value="easeInSine">easeInSine</option><option value="easeOutSine">easeOutSine</option><option value="easeInOutSine">easeInOutSine</option><option value="easeInCirc">easeInCirc</option><option value="easeOutCirc">easeOutCirc</option><option value="easeInOutCirc">easeInOutCirc</option><option value="easeInElastic">easeInElastic</option><option value="easeOutElastic">easeOutElastic</option><option value="easeInOutElastic">easeInOutElastic</option><option value="easeInBack">easeInBack</option><option value="easeOutBack">easeOutBack</option><option value="easeInOutBack">easeInOutBack</option><option value="easeInBounce">easeInBounce</option><option value="easeOutBounce">easeOutBounce</option><option value="easeInOutBounce">easeInOutBounce</option></select></div></div>';
		html+='<div class="row m-t-15"><div class="col-sm-12"><label>Before text</label><input type="text" name="before_text[]" class="form-control" placeholder="Before text"><div class="checkbox checkbox-primary"><input name="before_text_block[]" id="before_text_block'+rand+'" value="yes" type="checkbox"><label for="before_text_block'+rand+'">Block</label></div></div></div>';
		html+='<div class="row m-t-15"><div class="col-sm-12"><label>Number</label><input type="number" min="0" name="number[]" placeholder="Number" value="200" class="form-control"><div class="checkbox checkbox-primary"><input name="number_block[]" id="number_block'+rand+'" value="yes" type="checkbox"><label for="number_block'+rand+'">Block</label></div></div></div>';
		html+='<div class="row m-t-15"><div class="col-sm-12"><label>After text</label><input type="text" placeholder="After text" name="after_text[]" class="form-control"><div class="checkbox checkbox-primary"><input name="after_text_block[]" id="after_text_block'+rand+'" value="yes" type="checkbox"><label for="after_text_block'+rand+'">Block</label></div></div></div>';
		html+='<div class="row m-t-15"><div class="col-sm-6"><label>Duration</label><input type="number" min="0" name="duration[]" class="form-control" placeholder="Duration" value="1400"></div>';
		html+='<div class="col-sm-6"><label>Stroke Width</label><input type="number" min="0" name="strokeWidth[]" class="form-control"  placeholder="Stroke Width" value="2"></div></div>';
		html+='<div class="row m-t-15"><div class="col-sm-6"><label>Trail Width</label><input type="number" min="0" name="trailWidth[]" class="form-control"  placeholder="Trail Width" value="1"></div>';
		html+='<div class="col-sm-6"><label>Trail Color</label><div data-color-format="rgb" data-color="rgba(0,0,0)" class="colorpicker-default input-group"><input type="text" name="trailColor[]" class="form-control"  placeholder="Trail Color"><span class="input-group-btn add-on"><button class="btn btn-white" type="button"><i style="background-color:rgba(0,0,0);margin-top: 2px;"></i></button></span></div></div></div>';
		html+='<div class="row m-t-15"><div class="col-sm-12"><label>Loader Solid Color</label><div data-color-format="rgb" data-color="rgba(0,0,0)" class="colorpicker-default input-group"><input type="text" name="color[]" class="form-control"  placeholder="Loader Solid Color"><span class="input-group-btn add-on"><button class="btn btn-white" type="button"><i style="background-color:rgba(0,0,0);margin-top: 2px;"></i></button></span></div></div></div>';
		html+='<div class="row m-t-15"><div class="col-sm-12"><div class="checkbox checkbox-primary"><input name="gradient_color[]" id="gradient'+rand+'" value="yes" type="checkbox"><label for="gradient'+rand+'">Loader Gradient Color</label></div></div></div>';
		html+='<div class="row m-t-15"><div class="col-sm-6"><label>Loader Color From</label><div data-color-format="rgb" data-color="rgba(0,0,0)" class="colorpicker-default input-group"><input type="text" name="color_from[]" class="form-control"  placeholder="Loader Color From"><span class="input-group-btn add-on"><button class="btn btn-white" type="button"><i style="background-color:rgba(0,0,0);margin-top: 2px;"></i></button></span></div></div>';
		html+='<div class="col-sm-6"><label>Loader Color To</label><div data-color-format="rgb" data-color="rgba(0,0,0)" class="colorpicker-default input-group"><input type="text" name="color_to[]" class="form-control"  placeholder="Loader Color To"><span class="input-group-btn add-on"><button class="btn btn-white" type="button"><i style="background-color:rgba(0,0,0);margin-top: 2px;"></i></button></span></div></div></div>';
		html+='<div class="row m-t-15"><div class="col-sm-6"><label>Progress Percentage</label><input type="number" max="100" min="0" value="100" name="progress[]" class="form-control"  placeholder="Progress Percentage"></div>';
		html+='<div class="col-sm-6"><label>Custom Classes</label><input type="text" name="class[]" class="form-control"  placeholder="Class"></div></div>';
		html+='<div class="row  m-t-15"><div class="col-sm-12"><button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button></div></div>';
		html+='</div></div>';
		jQuery('.repeaters').append(html);
		$('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
		$('.selectpicker').selectpicker();
		return false;
	})
	jQuery('.repeaters').on('click', '.btn-repeat-remove', function(){
		jQuery(this).parents('.repeat').remove();
	})
	jQuery('.repeaters').on('click', '.btn-repeat-duplicate', function(){
		jQuery(this).parents('.repeat').clone().appendTo(".repeaters").find('.panel-title a').append('<span>(Duplicate)</span>')
	})
	$('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
})
</script>
@endsection
