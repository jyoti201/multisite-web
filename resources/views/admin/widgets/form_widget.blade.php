@extends('layouts.app')

@section('extra')
<link rel="stylesheet" href="{{ asset('assets/private/plugins/grapes/css/grapes.min.css') }}">
<script src="{{ asset('assets/private/plugins/bootstrap-formhelpers/bootstrap-formhelpers-selectbox.js') }}" type="text/javascript"></script>
<link href="{{ asset('assets/private/plugins/bootstrap-formhelpers/bootstrap-formhelpers.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://unpkg.com/grapesjs-preset-newsletter@0.2.15/dist/grapesjs-preset-newsletter.css">
<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />

<script>
  $( function() {
    $( "#sortable1, #sortable2, #sortable3" ).sortable({
		connectWith: ".connectedSortable",
		placeholder: "highlight",
		helper: "clone",
		start: function (event, ui) {
			$(ui.item).show();
			clone = $(ui.item).clone();
			before = $(ui.item).prev();
			parent = $(ui.item).parent();
		},
		receive: function (event, ui) {//only when dropped from one to another!
			if (before.length) before.after(clone);
			else parent.prepend(clone);
			var rand = 'field_'+Math.floor(Math.random()*(5000-10+1)+10);
			$(ui.item).find('input[name="name[]"]').val(rand).attr('id', rand);
		}
    }).disableSelection();


  });
 </script>
<style>
.nav-pills > li > a{
	    background: #eaeaea;
	color:#000000 !important;
}
.collapsable .panel{
	background-color: transparent;
	padding-bottom: 5px;
	    margin-bottom: 0;
}
.nav-pills > li > a:hover,.nav-pills > li > a.active{
	color:#ffffff !important;
	background-color: #5fbeaa !important;
}
.content ul{
	list-style:none;
	padding:0;
	margin:20px 0;
	padding-bottom:20px;
}
#sortable1 li{
	display:inline-block;
	    margin: 3px;
}
.content ul.nav-pills{
	margin-bottom:0 !important;
}
.content ul.nav-pills li{
	margin: 3px 5px;
}
.collapsable{
	display:none;
}
#sortable3 li{
	display:block !important;
	float:none;
	    margin-bottom: 10px;
}
#sortable3 li .collapsable{
	display:block;
	background-color: #f4f8fb;
}
#sortable3 li.highlight{
	background:#eaeaea;
	min-height:100px;
}
#sortable3 li .card-header{
	position:relative;
	cursor:pointer;
}
#sortable3 li:hover .card-header:after,#sortable3 li .card-header.active:after{
	content:'';
	left:0;
	right:0;
	top:0;
	bottom:-5px;
	position:absolute;
	background-color:rgba(0,0,0,.1);
}
#sortable3 li .card-body{
	display:none;
	    margin: 15px;
		background:#ffffff;
}
.collapsable .card-header .form-group{
	margin-bottom:0;
}
.collapsable{
	position:relative;
}
.collapsable .controls{
	padding: 10px 20px;
    background: #D2E0EB;
    cursor: move;
}
.collapsable .controls a{
	    float: right;
    margin-left: 10px;
}
.collapsable label{
	display:block;
}
.collapsable .help{
	    font-size: 12px;
    margin: 7px 0;
	font-weight: normal;
}
.required span{
	color:#ff0000;
}
.collapsable .help.above{
	display:none;
}
.not-visible{
	visibility:hidden;height:0px;
	position:absolute;
}
#gjs{
	position:relative;
}
#builder .card-header{
  padding: 15px;
}
.gjs-pn-views {
    right: 0;
    width: 19%;
	    padding: 4px;
}
.gjs-pn-options {
    right: 19%;
	    z-index: 9;
		padding: 4px;
}
.gjs-pn-commands {
    width: auto;
    left: 115px;
}
.gjs-cv-canvas{
	width: 78%;
}
.gjs-pn-views-container{
	    width: 22%;
}
.gjs-block-label,.gjs-editor,.gjs-clm-tags{
    font-size: inherit;
}
.gjs-block-label,.gjs-sm-properties{
	font-size: 14px;
}
.newsletters{
	max-height:300px;
	overflow-y:auto;
}
.newsletters .grid{
	    min-height: 190px;
    background-size: cover;
    background-position: top center;
    display: inline-block;
    width: 12.2%;
	position:relative;
	margin-bottom:3px;
}
.newsletters .grid a.btn{
	position:relative;
	border-radius:0;
	border:0;
}
.panel-group .card-header {
    padding: 0;
}
#sortable3 div.btn{
  display: none;
}
.newsletters .grid:before{
	content:'';
	position:absolute;
	left:0;
	right:0;
	top:0;
	bottom:0;
	background-color:rgba(0,0,0,.6);
	-webkit-transition: all .2s ease-in-out;
  -moz-transition: all .2s ease-in-out;
  -o-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
}
.newsletters .grid:hover:before{
	background-color:rgba(0,0,0,0);
}
@media (max-width: 1200px){
	.gjs-block {
		width: 100%;
	}
}
#preview .form-control{
	@if(!empty($settings['inputbgcolor']))
		background-color: {{ $settings['inputbgcolor'] }};
	@endif
	@if(!empty($settings['inputtxcolor']))
		color: {{ $settings['inputtxcolor'] }};
	@endif
	@if(!empty($settings['borderwidth']))
		border: {{ $settings['borderwidth'] }}px solid;
	@endif
	@if(!empty($settings['inputbordercolor']))
		border-color: {{ $settings['inputbordercolor'] }};
	@endif
}
</style>
<script>
jQuery(document).ready(function(){
	jQuery('#sortable3').on('click', 'li .card-header', function(){
		if(jQuery(this).hasClass('active')){
			jQuery(this).next('.card-body').slideUp();
			jQuery(this).removeClass('active');
		}else{
			jQuery(this).next('.card-body').slideDown();
			jQuery(this).addClass('active');
		}
	})
})
</script>


<script src="{{ asset('assets/private/plugins/grapes/grapes.min.js') }}"></script>
<script src="https://unpkg.com/grapesjs-preset-newsletter@0.2.15/dist/grapesjs-preset-newsletter.min.js"></script>
<script src="{{ asset('assets/private/plugins/grapes/grapesjs-blocks-basic.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/grapes/grapesjs-blocks-flexbox.min.js') }}"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
  var editor = grapesjs.init({
      container : '#gjs',
      fromElement: false,
	  components: jQuery.trim(jQuery('input[name="message_body"]').val()),
	  plugins: ['gjs-preset-newsletter', 'gjs-blocks-basic', 'gjs-blocks-flexbox','gjs-plugin-ckeditor'],
      pluginsOpts: {
        'gjs-preset-newsletter': {
          modalTitleImport: 'Import template',
        }
      },
		storageManager: {
			id: '',
			autosave: false,
			autoload: false,
			type: 'remote',
			stepsBeforeSave: 1,
			urlStore: '',
			urlLoad: '',
			params: {}, // For custom values on requests,
			contentTypeJson: true,
			storeComponents: true, // Enable/Disable storing of components in JSON format
			storeStyles: true, // Enable/Disable storing of rules/style in JSON format
			storeHtml: true, // Enable/Disable storing of components as HTML string
			storeCss: true, // Enable/Disable storing of rules/style as CSS string
		},
  });
  jQuery('.select_template').on('click', function(){
		var html = "";
		jQuery.get(jQuery(this).attr('data-rel'), function(response) {
			html = response;
			if(html!=''){
				editor = grapesjs.init({
				  container : '#gjs',
				  fromElement: false,
				  components: jQuery.trim(html),
				  plugins: ['gjs-preset-newsletter', 'gjs-blocks-basic', 'gjs-blocks-flexbox'],
				  pluginsOpts: {
					'gjs-preset-newsletter': {
					  modalTitleImport: 'Import template',
					}
				  },
					storageManager: {
						id: '',
						autosave: false,
						autoload: false,
						type: 'remote',
						stepsBeforeSave: 1,
						urlStore: '',
						urlLoad: '',
						params: {}, // For custom values on requests,
						contentTypeJson: true,
						storeComponents: true, // Enable/Disable storing of components in JSON format
						storeStyles: true, // Enable/Disable storing of rules/style in JSON format
						storeHtml: true, // Enable/Disable storing of components as HTML string
						storeCss: true, // Enable/Disable storing of rules/style as CSS string
					},
			  });
			}
		});
		return false;
	})
  jQuery('#save').on('click', function(){
	  var html = editor.Commands.get('gjs-get-inlined-html');
	  jQuery('input[name="message_body"]').val(html.run(editor));
  })

})
</script>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>

			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
      <div class="col-sm-8">
			<form method="post" id="widgetform" action="{{ route('widget.save') }}">
				{{csrf_field()}}
				<input type="hidden" name="id" value="{{ $widget->id }}">
				<input type="hidden" name="widgettype" value="form_widget">
				<div class="m-t-15">
          <div class="d-flex justify-content-between">
              <div class="left">
                    <div class="shortcodeplaceholder">
                        <input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
                        <button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
                            <i class="fa fa-copy"></i>
                        </button>
                    </div>
              </div>
              <div class="right m-b-20">
                <button class="btn btn-primary waves-effect waves-light btn-lg" type="button" data-toggle="collapse" data-target="#styles" aria-expanded="false" aria-controls="Settings">
  						     Common Styles
  						  </button>
                <button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
              </div>
          </div>
          <div class="collapse" id="styles">
              <div class="card-box">
                      <div id="accordion">
                        <div class="card">
                          <div class="card-header" id="headingOne">
                              <a href="#collapseOne" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Input Styles
                              </a>
                          </div>

                          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                  <div class="row">
                                        <div class="form-group col-md-6">
                                          <p class="text-muted m-b-15 font-15">Input Background Color</p>
                                          <div data-color-format="rgba" data-color="{{ !empty($settings['inputbgcolor'])? $settings['inputbgcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                            <input type="text" name="inputbgcolor" value="" class="form-control">
                                            <span class="input-group-btn add-on">
                                              <button class="btn btn-white" type="button">
                                                <i style="background-color:{{ !empty($settings['inputbgcolor'])? $settings['inputbgcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                              </button>
                                            </span>
                                          </div>
                                          <div class="help"></div>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <p class="text-muted m-b-15 font-15">Input Text Color</p>
                                          <div data-color-format="rgba" data-color="{{ !empty($settings['inputtxcolor'])? $settings['inputtxcolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
                                            <input type="text" name="inputtxcolor" value="" class="form-control">
                                            <span class="input-group-btn add-on">
                                              <button class="btn btn-white" type="button">
                                                <i style="background-color:{{ !empty($settings['inputtxcolor'])? $settings['inputtxcolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
                                              </button>
                                            </span>
                                          </div>
                                          <div class="help"></div>
                                        </div>
                                  </div>
                                  <div class="row">
                                        <div class="form-group col-md-6">
                                          <p class="text-muted m-b-15 font-15">Input Border Width</p>
                                          <div class="row">
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputborderwidth-left" value="{!! !empty($settings['inputborderwidth-left'])?$settings['inputborderwidth-left']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputborderwidth-right" value="{!! !empty($settings['inputborderwidth-right'])?$settings['inputborderwidth-right']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputborderwidth-top" value="{!! !empty($settings['inputborderwidth-top'])?$settings['inputborderwidth-top']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputborderwidth-bottom" value="{!! !empty($settings['inputborderwidth-bottom'])?$settings['inputborderwidth-bottom']:'0' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
                                            </div>
                                          </div>
                                          <div class="help"></div>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <p class="text-muted m-b-15 font-15">Input Border Radius</p>
                                          <div class="row">
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputborderradius-left-top" value="{!! !empty($settings['inputborderradius-left-top'])?$settings['inputborderradius-left-top']:'0' !!}" rel="txtTooltip" title="Left Top" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputborderradius-left-bottom" value="{!! !empty($settings['inputborderradius-left-bottom'])?$settings['inputborderradius-left-bottom']:'0' !!}" rel="txtTooltip" title="Left Bottom" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputborderradius-right-top" value="{!! !empty($settings['inputborderradius-right-top'])?$settings['inputborderradius-right-top']:'0' !!}" rel="txtTooltip" title="Right Top" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputborderradius-right-bottom" value="{!! !empty($settings['inputborderradius-right-bottom'])?$settings['inputborderradius-right-bottom']:'0' !!}" rel="txtTooltip" title="Right Bottom" data-placement="top">
                                            </div>
                                          </div>
                                          <div class="help"></div>
                                        </div>
                                  </div>
                                  <div class="row">
                                        <div class="form-group col-md-4">
                                          <p class="text-muted m-b-15 font-15">Input Border Style</p>
                                          <select data-style="btn-white" name="inputborderstyle" class="form-control selectpicker">
                                            <option value="solid" {{ !empty($settings['inputborderstyle']) && $settings['inputborderstyle']=='solid'?'selected':'' }}>Solid</option>
                                            <option value="dashed" {{ !empty($settings['inputborderstyle']) && $settings['inputborderstyle']=='dashed'?'selected':'' }}>Dashed</option>
                                            <option value="dotted" {{ !empty($settings['inputborderstyle']) && $settings['inputborderstyle']=='dotted'?'selected':'' }}>Dotted</option>
                                            <option value="double" {{ !empty($settings['inputborderstyle']) && $settings['inputborderstyle']=='double'?'selected':'' }}>Double</option>
                                            <option value="groove" {{ !empty($settings['inputborderstyle']) && $settings['inputborderstyle']=='groove'?'selected':'' }}>Groove</option>
                                            <option value="ridge" {{ !empty($settings['inputborderstyle']) && $settings['inputborderstyle']=='ridge'?'selected':'' }}>Ridge</option>
                                          </select>
                                          <div class="help"></div>
                                        </div>
                                        <div class="form-group col-md-4">
                                          <p class="text-muted m-b-15 font-15">Input Font Size</p>
                                          <input type="number" class="form-control" min="0" name="inputfont" value="{!! !empty($settings['inputfont'])?$settings['inputfont']:'15' !!}">
                                          <div class="help"></div>
                                        </div>
                                        <div class="form-group col-md-4">
                                          <p class="text-muted m-b-15 font-15">Input Font Family</p>
                                          <select data-style="btn-white" name="inputfont_family" class="form-control input selectpicker">
                                            <option value="">Default</option>
                                            @if(!empty(getSetting('imported_font_name','styles')))
                                              @foreach(getSetting('imported_font_name','styles') as $val)
                                                <option value="{{ $val }}" {!! !empty($settings['inputfont_family']) && $settings['inputfont_family']==$val?'selected':'' !!}>{{ $val }}</option>
                                              @endforeach
                                            @endif
                                          </select>
                                        </div>

                                  </div>
                                  <div class="row">
                                        <div class="form-group col-md-6">
                                          <p class="text-muted m-b-15 font-15">Input Padding</p>
                                          <div class="row">
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputpadding-left" value="{!! !empty($settings['inputpadding-left'])?$settings['inputpadding-left']:'10' !!}" rel="txtTooltip" title="Left" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputpadding-right" value="{!! !empty($settings['inputpadding-right'])?$settings['inputpadding-right']:'10' !!}" rel="txtTooltip" title="Right" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputpadding-top" value="{!! !empty($settings['inputpadding-top'])?$settings['inputpadding-top']:'10' !!}" rel="txtTooltip" title="Top" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputpadding-bottom" value="{!! !empty($settings['inputpadding-bottom'])?$settings['inputpadding-bottom']:'10' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
                                            </div>
                                          </div>
                                          <div class="help"></div>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <p class="text-muted m-b-15 font-15">Input Margin</p>
                                          <div class="row">
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputmargin-left" value="{!! !empty($settings['inputmargin-left'])?$settings['inputmargin-left']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputmargin-right" value="{!! !empty($settings['inputmargin-right'])?$settings['inputmargin-right']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputmargin-top" value="{!! !empty($settings['inputmargin-top'])?$settings['inputmargin-top']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
                                            </div>
                                            <div class="col-sm-3">
                                              <input type="number" class="form-control" min="0" name="inputmargin-bottom" value="{!! !empty($settings['inputmargin-bottom'])?$settings['inputmargin-bottom']:'10' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
                                            </div>
                                          </div>
                                          <div class="help"></div>
                                        </div>

                                  </div>
                                  <div class="row">

                                        <div class="form-group col-md-12">
                                          <p class="text-muted m-b-15 font-15">Input Border Color</p>
                                          <div class="row">
                                            <div class="col-sm-6 m-t-10">
                                              <div data-color-format="rgba" data-color="{{ !empty($settings['inputbordercolor-left'])? $settings['inputbordercolor-left']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                                <input type="text" name="inputbordercolor-left" value="" class="form-control" rel="txtTooltip" title="Left" data-placement="top">
                                                <span class="input-group-btn add-on">
                                                  <button class="btn btn-white" type="button">
                                                    <i style="background-color:{{ !empty($settings['inputbordercolor-left'])? $settings['inputbordercolor-left']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                                  </button>
                                                </span>
                                              </div>
                                            </div>
                                            <div class="col-sm-6 m-t-10">
                                              <div data-color-format="rgba" data-color="{{ !empty($settings['inputbordercolor-right'])? $settings['inputbordercolor-right']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                                <input type="text" name="inputbordercolor-right" value="" class="form-control" rel="txtTooltip" title="Right" data-placement="top">
                                                <span class="input-group-btn add-on">
                                                  <button class="btn btn-white" type="button">
                                                    <i style="background-color:{{ !empty($settings['inputbordercolor-right'])? $settings['inputbordercolor-right']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                                  </button>
                                                </span>
                                              </div>
                                            </div>
                                            <div class="col-sm-6 m-t-10">
                                              <div data-color-format="rgba" data-color="{{ !empty($settings['inputbordercolor-top'])? $settings['inputbordercolor-top']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                                <input type="text" name="inputbordercolor-top" value="" class="form-control" rel="txtTooltip" title="Top" data-placement="top">
                                                <span class="input-group-btn add-on">
                                                  <button class="btn btn-white" type="button">
                                                    <i style="background-color:{{ !empty($settings['inputbordercolor-top'])? $settings['inputbordercolor-top']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                                  </button>
                                                </span>
                                              </div>
                                            </div>
                                            <div class="col-sm-6 m-t-10">
                                              <div data-color-format="rgba" data-color="{{ !empty($settings['inputbordercolor-bottom'])? $settings['inputbordercolor-bottom']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                                <input type="text" name="inputbordercolor-bottom" value="" class="form-control" rel="txtTooltip" title="Bottom" data-placement="top">
                                                <span class="input-group-btn add-on">
                                                  <button class="btn btn-white" type="button">
                                                    <i style="background-color:{{ !empty($settings['inputbordercolor-bottom'])? $settings['inputbordercolor-bottom']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                                  </button>
                                                </span>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="help"></div>
                                        </div>
                                  </div>
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingTwo">
                              <a href="#collapseTwo" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Button Styles
                              </a>
                          </div>
                          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                    <div class="row">
                                    <div class="form-group col-sm-6">
                                      <div class="row">
                                        <div class="col-sm-6">
                                          <p class="text-muted m-b-15 font-15">Button Align</p>
                                          <select data-style="btn-white" name="buttonalign" class="form-control selectpicker">
                                            <option value="text-left" {{ !empty($settings['buttonalign']) && $settings['buttonalign']=='text-left'?'selected':'' }}>Left</option>
                                            <option value="text-center" {{ !empty($settings['buttonalign']) && $settings['buttonalign']=='text-center'?'selected':'' }}>Center</option>
                                            <option value="text-right" {{ !empty($settings['buttonalign']) && $settings['buttonalign']=='text-right'?'selected':'' }}>Right</option>
                                          </select>
                                        </div>
                                        <div class="col-sm-6">
                                          <p class="text-muted m-b-15 font-15">Button Width</p>
                                          <input type="text" class="form-control" placeholder="in px or %" name="buttonwidth" value="{!! !empty($settings['buttonwidth'])?$settings['buttonwidth']:'' !!}">
                                          <div class="help"></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                      <div class="row">
                                        <div class="col-sm-6">
                                          <p class="text-muted m-b-15 font-15">Button Background Color</p>
                                          <div data-color-format="rgba" data-color="{{ !empty($settings['buttonbgcolor'])? $settings['buttonbgcolor']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                            <input type="text" name="buttonbgcolor" value="" class="form-control">
                                            <span class="input-group-btn add-on">
                                              <button class="btn btn-white" type="button">
                                                <i style="background-color:{{ !empty($settings['buttonbgcolor'])? $settings['buttonbgcolor']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                              </button>
                                            </span>
                                          </div>
                                          <div class="help"></div>
                                        </div>
                                        <div class="col-sm-6">
                                          <p class="text-muted m-b-15 font-15">Button Text Color</p>
                                          <div data-color-format="rgba" data-color="{{ !empty($settings['buttontxcolor'])? $settings['buttontxcolor']:'rgba(0,0,0,1)' }}" class="colorpicker-rgba input-group">
                                            <input type="text" name="buttontxcolor" value="" class="form-control">
                                            <span class="input-group-btn add-on">
                                              <button class="btn btn-white" type="button">
                                                <i style="background-color:{{ !empty($settings['buttontxcolor'])? $settings['buttontxcolor']:'rgba(0,0,0,1)' }};margin-top: 2px;"></i>
                                              </button>
                                            </span>
                                          </div>
                                          <div class="help"></div>
                                        </div>
                                      </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="form-group col-sm-6">
                                      <p class="text-muted m-b-15 font-15">Button Border Width</p>
                                      <div class="row">
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonborderwidth-left" value="{!! !empty($settings['buttonborderwidth-left'])?$settings['buttonborderwidth-left']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonborderwidth-right" value="{!! !empty($settings['buttonborderwidth-right'])?$settings['buttonborderwidth-right']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonborderwidth-top" value="{!! !empty($settings['buttonborderwidth-top'])?$settings['buttonborderwidth-top']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonborderwidth-bottom" value="{!! !empty($settings['buttonborderwidth-bottom'])?$settings['buttonborderwidth-bottom']:'0' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
                                        </div>
                                      </div>
                                      <div class="help"></div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                      <p class="text-muted m-b-15 font-15">Button Border Radius (in px)</p>
                                      <div class="row">
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonborderradius-left-top" value="{!! !empty($settings['buttonborderradius-left-top'])?$settings['buttonborderradius-left-top']:'0' !!}" rel="txtTooltip" title="Left Top" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonborderradius-left-bottom" value="{!! !empty($settings['buttonborderradius-left-bottom'])?$settings['buttonborderradius-left-bottom']:'0' !!}" rel="txtTooltip" title="Left Bottom" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonborderradius-right-top" value="{!! !empty($settings['buttonborderradius-right-top'])?$settings['buttonborderradius-right-top']:'0' !!}" rel="txtTooltip" title="Right Top" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonborderradius-right-bottom" value="{!! !empty($settings['buttonborderradius-right-bottom'])?$settings['buttonborderradius-right-bottom']:'0' !!}" rel="txtTooltip" title="Right Bottom" data-placement="top">
                                        </div>
                                      </div>
                                      <div class="help"></div>
                                    </div>
                                    </div>

                                    <div class="row">
                                    <div class="form-group col-md-4">
                                      <p class="text-muted m-b-15 font-15">Button Border Style</p>
                                      <select data-style="btn-white" name="buttonborderstyle" class="form-control selectpicker">
                                        <option value="solid" {{ !empty($settings['buttonborderstyle']) && $settings['buttonborderstyle']=='solid'?'selected':'' }}>Solid</option>
                                        <option value="dashed" {{ !empty($settings['buttonborderstyle']) && $settings['buttonborderstyle']=='dashed'?'selected':'' }}>Dashed</option>
                                        <option value="dotted" {{ !empty($settings['buttonborderstyle']) && $settings['buttonborderstyle']=='dotted'?'selected':'' }}>Dotted</option>
                                        <option value="double" {{ !empty($settings['buttonborderstyle']) && $settings['buttonborderstyle']=='double'?'selected':'' }}>Double</option>
                                        <option value="groove" {{ !empty($settings['buttonborderstyle']) && $settings['buttonborderstyle']=='groove'?'selected':'' }}>Groove</option>
                                        <option value="ridge" {{ !empty($settings['buttonborderstyle']) && $settings['buttonborderstyle']=='ridge'?'selected':'' }}>Ridge</option>
                                      </select>
                                      <div class="help"></div>
                                    </div>
                                    <div class="form-group col-md-4">
                                      <p class="text-muted m-b-15 font-15">Button Font Size (in px)</p>
                                      <input type="number" class="form-control" min="0" name="buttonfont" value="{!! !empty($settings['buttonfont'])?$settings['buttonfont']:'15' !!}">
                                      <div class="help"></div>
                                    </div>
                                    <div class="form-group col-md-4">
                                      <p class="text-muted m-b-15 font-15">Button Font Family</p>
                                      <select data-style="btn-white" name="buttonfont_family" class="form-control input selectpicker">
                                        <option value="">Default</option>
                                        @if(!empty(getSetting('imported_font_name','styles')))
                                          @foreach(getSetting('imported_font_name','styles') as $val)
                                            <option value="{{ $val }}" {!! !empty($settings['buttonfont_family']) && $settings['buttonfont_family']==$val?'selected':'' !!}>{{ $val }}</option>
                                          @endforeach
                                        @endif
                                      </select>
                                    </div>

                                    </div>
                                    <div class="row">
                                    <div class="form-group col-md-6">
                                      <p class="text-muted m-b-15 font-15">Button Padding (in px)</p>
                                      <div class="row">
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonpadding-left" value="{!! !empty($settings['buttonpadding-left'])?$settings['buttonpadding-left']:'10' !!}" rel="txtTooltip" title="Left" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonpadding-right" value="{!! !empty($settings['buttonpadding-right'])?$settings['buttonpadding-right']:'10' !!}" rel="txtTooltip" title="Right" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonpadding-top" value="{!! !empty($settings['buttonpadding-top'])?$settings['buttonpadding-top']:'10' !!}" rel="txtTooltip" title="Top" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonpadding-bottom" value="{!! !empty($settings['buttonpadding-bottom'])?$settings['buttonpadding-bottom']:'10' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
                                        </div>
                                      </div>
                                      <div class="help"></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                      <p class="text-muted m-b-15 font-15">Button Margin (in px)</p>
                                      <div class="row">
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonmargin-left" value="{!! !empty($settings['buttonmargin-left'])?$settings['buttonmargin-left']:'0' !!}" rel="txtTooltip" title="Left" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonmargin-right" value="{!! !empty($settings['buttonmargin-right'])?$settings['buttonmargin-right']:'0' !!}" rel="txtTooltip" title="Right" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonmargin-top" value="{!! !empty($settings['buttonmargin-top'])?$settings['buttonmargin-top']:'0' !!}" rel="txtTooltip" title="Top" data-placement="top">
                                        </div>
                                        <div class="col-sm-3">
                                          <input type="number" class="form-control" min="0" name="buttonmargin-bottom" value="{!! !empty($settings['buttonmargin-bottom'])?$settings['buttonmargin-bottom']:'10' !!}" rel="txtTooltip" title="Bottom" data-placement="top">
                                        </div>
                                      </div>
                                      <div class="help"></div>
                                    </div>

                                    </div>
                                    <div class="row">

                                    <div class="form-group col-md-12">
                                      <p class="text-muted m-b-15 font-15">Button Border Color</p>
                                      <div class="row">
                                        <div class="col-sm-6 m-t-10">
                                          <div data-color-format="rgba" data-color="{{ !empty($settings['buttonbordercolor-left'])? $settings['buttonbordercolor-left']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                            <input type="text" name="buttonbordercolor-left" value="" class="form-control" rel="txtTooltip" title="Left" data-placement="top">
                                            <span class="input-group-btn add-on">
                                              <button class="btn btn-white" type="button">
                                                <i style="background-color:{{ !empty($settings['buttonbordercolor-left'])? $settings['buttonbordercolor-left']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                              </button>
                                            </span>
                                          </div>
                                        </div>
                                        <div class="col-sm-6 m-t-10">
                                          <div data-color-format="rgba" data-color="{{ !empty($settings['buttonbordercolor-right'])? $settings['buttonbordercolor-right']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                            <input type="text" name="buttonbordercolor-right" value="" class="form-control" rel="txtTooltip" title="Right" data-placement="top">
                                            <span class="input-group-btn add-on">
                                              <button class="btn btn-white" type="button">
                                                <i style="background-color:{{ !empty($settings['buttonbordercolor-right'])? $settings['buttonbordercolor-right']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                              </button>
                                            </span>
                                          </div>
                                        </div>
                                        <div class="col-sm-6 m-t-10">
                                          <div data-color-format="rgba" data-color="{{ !empty($settings['buttonbordercolor-top'])? $settings['buttonbordercolor-top']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                            <input type="text" name="buttonbordercolor-top" value="" class="form-control" rel="txtTooltip" title="Top" data-placement="top">
                                            <span class="input-group-btn add-on">
                                              <button class="btn btn-white" type="button">
                                                <i style="background-color:{{ !empty($settings['buttonbordercolor-top'])? $settings['buttonbordercolor-top']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                              </button>
                                            </span>
                                          </div>
                                        </div>
                                        <div class="col-sm-6 m-t-10">
                                          <div data-color-format="rgba" data-color="{{ !empty($settings['buttonbordercolor-bottom'])? $settings['buttonbordercolor-bottom']:'rgba(255,255,255,1)' }}" class="colorpicker-rgba input-group">
                                            <input type="text" name="buttonbordercolor-bottom" value="" class="form-control" rel="txtTooltip" title="Bottom" data-placement="top">
                                            <span class="input-group-btn add-on">
                                              <button class="btn btn-white" type="button">
                                                <i style="background-color:{{ !empty($settings['buttonbordercolor-bottom'])? $settings['buttonbordercolor-bottom']:'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                                              </button>
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="help"></div>
                                    </div>
                                    </div>
                            </div>
                          </div>
                        </div>
                      </div>
              </div>
          </div>
					<div class="card-box">
						<ul class="nav nav-pills m-t-0 m-b-20">
							<li>
								<a href="#builder"  class="active" data-toggle="tab" aria-expanded="true">Form Builder</a>
							</li>
							<li class="">
								<a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a>
							</li>
							<li class="">
								<a href="#preview" data-toggle="tab" aria-expanded="false">Preview</a>
							</li>
							<li class="">
								<a href="#template" data-toggle="tab" aria-expanded="false">Email Template</a>
							</li>
						</ul>
						<div class="tab-content br-n pn">
							<div id="builder" class="tab-pane active">
								<ul id="sortable3" class="connectedSortable" style="min-height:400px">
									@if(!empty($settings['type']))
										@foreach($settings['type'] as $key=>$field)
											<li class="ui-sortable-handle">
												<input type="hidden" name="type[]" class="form-control" value="{{ !empty($settings['type'][$key])?$settings['type'][$key]:'' }}">
												<input type="hidden" name="title[]" class="form-control" value="{{ !empty($settings['title'][$key])?$settings['title'][$key]:'' }}">
												<input type="hidden" name="name[]" class="form-control" value="{{ !empty($settings['name'][$key])?$settings['name'][$key]:'' }}">
												<div class="collapsable">
													<div class="controls">{{ !empty($settings['title'][$key])?$settings['title'][$key]:'' }}<a href="#" class="button delete"><i class="fa fa-times fa-lg"></i></a><a href="#" class="button duplicate"><i class="fa fa-files-o fa-lg"></i></a></div>
													<div class="card">
														<div class="card-header">
															<div class="form-group">
																<label class="field_label" {!! !empty($settings['labelvisibility'][$key]) && $settings['labelvisibility'][$key] == 'hidden'?'style="display:none"':'' !!}>{{ !empty($settings['label'][$key])?$settings['label'][$key]:'' }}
																</label>
																<div class="help above" {!! !empty($settings['descriptionplacement'][$key]) && $settings['descriptionplacement'][$key] == 'above'?'style="display:block"':'style="display:none"' !!}>{{ !empty($settings['description'][$key])?$settings['description'][$key]:'' }}</div>
																{!! getformInput(!empty($settings['type'][$key])?$settings['type'][$key]:'', '', !empty($settings['placeholder'][$key])?$settings['placeholder'][$key]:'', !empty($settings['value'][$key])?$settings['value'][$key]:'', false) !!}
																<div class="help below" {!! !empty($settings['descriptionplacement'][$key]) && $settings['descriptionplacement'][$key] == 'below'?'style="display:block"':'style="display:none"' !!}>{{ !empty($settings['description'][$key])?$settings['description'][$key]:'' }}</div>
															</div>
														</div>
														<div class="card-body">
															@if(!empty($settings['type'][$key]) && $settings['type'][$key]=='html')
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">HTML</h4><textarea class="form-control value" name="value[]">{{ !empty($settings['value'][$key])?$settings['value'][$key]:'' }}</textarea></div>
															@else
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">Default Value</h4><input type="text" class="form-control value" name="value[]" value="{{ !empty($settings['value'][$key])?$settings['value'][$key]:'' }}"></div>
															@endif
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">Field Label</h4><input type="text" class="form-control" name="label[]" value="{{ !empty($settings['label'][$key])?$settings['label'][$key]:'' }}"></div>
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">Placeholder</h4><input type="text" class="form-control" name="placeholder[]" value="{{ !empty($settings['placeholder'][$key])?$settings['placeholder'][$key]:'' }}"></div>
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">Description</h4><textarea class="form-control" name="description[]">{{ !empty($settings['description'][$key])?$settings['description'][$key]:'' }}</textarea></div>
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">Maximum Characters</h4><input type="text" class="form-control" name="maxchar[]" value="{{ !empty($settings['maxchar'][$key])?$settings['maxchar'][$key]:'' }}"></div>
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15"><input type="hidden" name="required[]" value=""><input type="checkbox" class="required" {{ !empty($settings['required'][$key])?'checked':'' }}> Required</h4></div>
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">Field Label Visibility</h4><select data-style="btn-white" class="form-control selectpicker" name="labelvisibility[]"><option value="visible" {{ !empty($settings['labelvisibility'][$key]) && $settings['labelvisibility'][$key]=='visible'?'selected':'' }}>Visible</option><option value="hidden" {{ !empty($settings['labelvisibility'][$key]) && $settings['labelvisibility'][$key]=='hidden'?'selected':'' }}>Hidden</option></select></div>
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">Description Placement</h4><select data-style="btn-white" class="form-control selectpicker" name="descriptionplacement[]"><option value="below" {{ !empty($settings['descriptionplacement'][$key]) && $settings['descriptionplacement'][$key]=='below'?'selected':'' }}>Below</option><option value="above" {{ !empty($settings['descriptionplacement'][$key]) && $settings['descriptionplacement'][$key]=='above'?'selected':'' }}>Above</option></select></div>
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">Custom Validation Message</h4><input type="text" class="form-control" name="custommessage[]" value="{{ !empty($settings['custommessage'][$key])?$settings['custommessage'][$key]:'' }}"></div>
															<div class="form-group"><h4 class="text-muted m-b-15 m-t-15 font-15">Custom CSS Class</h4><input type="text" class="form-control" name="customcss[]" value="{{ !empty($settings['customcss'][$key])?$settings['customcss'][$key]:'' }}"></div>
														</div>
													</div>
												</div>
											</li>
										@endforeach
									@endif
								</ul>
							</div>
							<div id="settings" class="tab-pane">
								<div class="row">
									<div class="col-md-12">
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Form name</h4>
												<input type="text" class="form-control" name="formname" value="{{ !empty($settings['formname'])?$settings['formname']:'Form_'.$widget->id }}" readonly>
												<div class="help"></div>
											</div>
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Submission Type</h4>
												<div class="radio"><input name="submission_type" id="submission_type_thankyou" type="radio" value="thankyou" checked> <label for="submission_type_thankyou">Thank you Message</label></div>
												<div class="radio"><input name="submission_type" id="submission_type_redirect" type="radio" value="redirect"> <label for="submission_type_redirect">Redirect</label></div>
											</div>
											<div class="form-group submission_type submission_type_thankyou collapse in">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Thank you Message</h4>
												<textarea name="thankyou_message" class="form-control">{{ !empty($settings['thankyou_message'])?$settings['thankyou_message']:'Thank you for your submission.' }}</textarea>
												<div class="checkbox"><input name="hide_form" id="hide_form" type="checkbox" {{ !empty($settings['hide_form'])?'checked':'' }}> <label for="hide_form">Hide form after submission</label></div>
												<div class="help"></div>
											</div>

											<div class="form-group submission_type submission_type_redirect collapse">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Redirect</h4>
												<input type="text" class="form-control" name="redirect" value="{!! !empty($settings['redirect'])?$settings['redirect']:'' !!}">
												<div class="help"></div>
											</div>

											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Submit Button Text</h4>
												<input type="text" class="form-control" name="buttontext" value="{{ !empty($settings['buttontext'])?$settings['buttontext']:'Submit' }}">
												<div class="help"></div>
											</div>
											<div class="form-group">
												<h4 class="text-muted m-b-15 m-t-15 font-15">Submit Button Classes</h4>
												<input type="text" class="form-control" name="buttonclass" value="{{ !empty($settings['buttonclass'])?$settings['buttonclass']:'' }}">
												<div class="help"></div>
											</div>



									</div>
								</div>
							</div>
							<div id="preview" class="tab-pane">
								<div class="row">
									<div class="col-md-12">
                    <div class="row flex-wrap no-gutters">
										@if(!empty($settings['type']))
														@foreach($settings['type'] as $key=>$field)
														  <div class="form-group col-12 {{ !empty($settings['customcss'][$key])?$settings['customcss'][$key]:'' }}">
															@if(!empty($settings['type'][$key]) && $settings['type'][$key]!='hidden')
															<label for="{{ !empty($settings['name'][$key])?$settings['name'][$key]:'' }}">{{ !empty($settings['label'][$key])?$settings['label'][$key]:'' }}</label>
															@endif
															@if(!empty($settings['descriptionplacement'][$key]) && $settings['descriptionplacement'][$key] == 'above')
															<div class="help">{{ !empty($settings['description'][$key])?$settings['description'][$key]:'' }}</div>
															@endif
															{!! getformInput(!empty($settings['type'][$key])?$settings['type'][$key]:'', !empty($settings['name'][$key])?$settings['name'][$key]:'', !empty($settings['placeholder'][$key])?$settings['placeholder'][$key]:'', !empty($settings['value'][$key])?$settings['value'][$key]:'', true) !!}
															@if(!empty($settings['descriptionplacement'][$key]) && $settings['descriptionplacement'][$key] == 'below')
															<div class="help">{{ !empty($settings['description'][$key])?$settings['description'][$key]:'' }}</div>
															@endif
														  </div>
														@endforeach
														<div class="form-group col">
														<button type="button" class="btn btn-default {{ !empty($settings['buttonclass'])?$settings['buttonclass']:'' }}">{{ !empty($settings['buttontext'])?$settings['buttontext']:'Submit' }}</button>
														</div>
										@endif
                    </div>
									</div>
								</div>
							</div>
							<div id="template" class="tab-pane">
								<div class="row">
									<div class="col-md-12">
                    @if(!empty($settings['type']))
										<div class="form-group">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Fields to use in email template</h4>
                      @foreach($settings['type'] as $key=>$field)
												[{{ $settings['name'][$key] }}]&nbsp;
											@endforeach
										</div>
                    @endif

										<div class="form-group">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Notification Type</h4>
											<div class="radio"><input name="notification_type" id="notification_type_custom" value="custom" type="radio" {{ !empty($settings['notification_type']) && $settings['notification_type']=='custom'?'checked':'' }} {{ empty($settings['notification_type'])?'checked':'' }}> <label for="notification_type_custom">Custom</label></div>
											<div class="radio"><input name="notification_type" id="notification_type_hubspot" value="hubspot" type="radio" {{ !empty($settings['notification_type']) && $settings['notification_type']=='hubspot'?'checked':'' }}> <label for="notification_type_hubspot">Hubspot</label></div>
										</div>

										<div class="form-group">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Recipients (Welcome Email)</h4>
											<input type="text" class="form-control" name="recipients" value="{{ !empty($settings['recipients'])?$settings['recipients']:Auth::user()->email }}">
											<div class="help">Design your welcome email below</div>
										</div>
										<div class="form-group">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Admin Email</h4>
											<input type="text" class="form-control" name="admin_email" value="{{ !empty($settings['admin_email'])?$settings['admin_email']:Auth::user()->email }}">
											<div class="help"></div>
										</div>
										<div class="form-group collapse notification_type {{ !empty($settings['notification_type']) && $settings['notification_type']=='custom'?'in':'' }}{{ empty($settings['notification_type'])?'in':'' }} custom">
											<h4 class="text-muted m-b-15 m-t-15 font-15">From</h4>
											<input type="text" class="form-control" name="from" value="{{ !empty($settings['from'])?$settings['from']:Auth::user()->email }}">
											<div class="help"></div>
										</div>

										<div class="form-group collapse notification_type {{ !empty($settings['notification_type']) && $settings['notification_type']=='custom'?'in':'' }}{{ empty($settings['notification_type'])?'in':'' }} custom">
											<h4 class="text-muted m-b-15 m-t-15 font-15">From Name</h4>
											<input type="text" class="form-control" name="fromname" value="{{ !empty($settings['fromname'])?$settings['fromname']:\Config::get('app.name') }}">
											<div class="help"></div>
										</div>

										<div class="form-group collapse notification_type {{ !empty($settings['notification_type']) && $settings['notification_type']=='custom'?'in':'' }}{{ empty($settings['notification_type'])?'in':'' }} custom">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Subject</h4>
											<input type="text" class="form-control" name="subject" value="{{ !empty($settings['subject'])?$settings['subject']:'New Form Submission from Form_'.$widget->id }}">
											<div class="help"></div>
										</div>

										<div class="form-group collapse notification_type {{ !empty($settings['notification_type']) && $settings['notification_type']=='custom'?'in':'' }}{{ empty($settings['notification_type'])?'in':'' }} custom">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Reply To</h4>
											<input type="text" class="form-control" name="replyto" value="{{ !empty($settings['replyto'])?$settings['replyto']:'' }}">
											<div class="help"></div>
										</div>

										<div class="form-group collapse notification_type {{ !empty($settings['notification_type']) && $settings['notification_type']=='hubspot'?'in':'' }} hubspot">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Hubspot Api Key:</h4>
											<input type="text" class="form-control" name="hubspotkey" id="hubspotkey" value="{!! !empty($settings['hubspotkey'])?$settings['hubspotkey']:'' !!}">
										</div>
										<div class="form-group collapse notification_type {{ !empty($settings['notification_type']) && $settings['notification_type']=='hubspot'?'in':'' }} hubspot">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Hubspot guid</h4>
											<input type="text" class="form-control" name="guid" value="{{ !empty($settings['guid'])?$settings['guid']:'' }}">
											<div class="help"></div>
										</div>
										<div class="form-group collapse notification_type {{ !empty($settings['notification_type']) && $settings['notification_type']=='hubspot'?'in':'' }} hubspot">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Hubspot portalId</h4>
											<input type="text" class="form-control" name="portalid" value="{{ !empty($settings['portalid'])?$settings['portalid']:'' }}">
											<div class="help"></div>
										</div>


										<input type="hidden" class="form-control" name="message_body" value="{{ !empty($settings['message_body'])?$settings['message_body']:'' }}">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				</form>
        </div>
				<div class="col-sm-4 m-t-15">
					<div class="card-box">
							<div class="panel-group" id="accordion" style="margin-bottom:0;">
							  <div class="card">
								<div class="card-header">
									<a data-toggle="collapse" data-parent="#accordion" href="#standard">Standard Fields</a>
								</div>
								<div id="standard" class="collapse show">
                  <div class="card-body">
      									<ul id="sortable1" class="connectedSortable row">
      									  @foreach(getformFields() as $key=>$val)
      										@if($key=='standard')
      											@foreach($val as $key2=>$value)
      											<li>
      												<div class="btn btn-white">{{ $value['title'] }}</div>
      												<input type="hidden" name="type[]" class="form-control" value="{{ $key2 }}">
      												<input type="hidden" name="title[]" class="form-control" value="{{ $value['title'] }}">
      												<input type="hidden" name="name[]" class="form-control" value="">
      												<div class="collapsable">
      												<div class="controls">{{ $value['title'] }}<a href="#" class="button delete"><i class="fa fa-times fa-lg"></i></i></a><a  href="#" class="button duplicate"><i class="fa fa-files-o fa-lg"></i></a></div>
      												{!! $value['html'] !!}
      												</div>
      											</li>
      											@endforeach
      										@endif
      									  @endforeach
      									</ul>
                  </div>
								</div>
							  </div>
							  <div class="card">
								<div class="card-header">
									<a data-toggle="collapse" data-parent="#accordion" href="#advance">Advance Fields</a>
								</div>
								<div id="advance" class="collapse">
                  <div class="card-body">
									<ul id="sortable2" class="connectedSortable">
									  <li class="ui-state-default">Item 1</li>
									  <li class="ui-state-default">Item 2</li>
									  <li class="ui-state-default">Item 3</li>
									  <li class="ui-state-default">Item 4</li>
									  <li class="ui-state-default">Item 5</li>
									</ul>
                  </div>
								</div>
							  </div>
							</div>
					</div>
				</div>
				<div class="col-sm-12 m-t-15">
				<button data-toggle="collapse" class="m-b-30 btn btn-white waves-effect waves-light btn-lg" data-target="#newsletters">Use Our Beautiful Prebuilt Templates</button>

				<div id="newsletters" class="newsletters collapse">
				@for($i=1;$i<=17;$i++)
					<div class="grid" style="background-image:url('//mx.frezit.com/newsletters/{{ $i }}/{{ $i }}.jpg')">
						<a href="#" data-rel="//mx.frezit.com/newsletters/{{ $i }}/index.html" class="select_template btn btn-white btn-default">Use</a>
					</div>
				@endfor
				</div>
				<div class="alert alert-danger fade in alert-dismissible">
					Please use encoded images. <a href="https://www.base64-image.de/" target="_blank">Click Here</a> to convert image
				</div>
				<div id="gjs">

										</div>
				</div>
    </div>
</div>
<script>
jQuery(document).ready(function(){
	jQuery('input[name="notification_type"]').click(function(){
		jQuery('.notification_type').removeClass('in');
		jQuery('.'+jQuery(this).val()).addClass('in');
	})
	jQuery('.colorpicker-rgba').colorpicker();
	jQuery('input[name="label[]"]').bind("change keyup input",function() {
		jQuery(this).parents('li').find('.field_label').text(jQuery(this).val());
	});
	jQuery('input[name="placeholder[]"]').bind("change keyup input",function() {
		jQuery(this).parents('li').find('.placeholder').attr('placeholder', jQuery(this).val());
	});
	jQuery('.value').bind("change keyup input",function() {
		jQuery(this).parents('li').find('.placeholder').val(jQuery(this).val());
	});
	jQuery('textarea[name="description[]"]').bind("change keyup input",function() {
		jQuery(this).parents('li').find('.help').html(jQuery(this).val());
	});
	jQuery('#sortable3').on('change', 'select[name="labelvisibility[]"]', function(){
		if(jQuery(this).val()=='hidden'){
			jQuery(this).parents('li').find('.field_label').hide();
		}else{
			jQuery(this).parents('li').find('.field_label').show();
		}
	})
	jQuery('input[name="submission_type"]').change(function(){
		jQuery('.submission_type').removeClass('in');
		if(jQuery(this).val()=='thankyou'){
			jQuery('.submission_type_thankyou').addClass('in');
		}else if(jQuery(this).val()=='redirect'){
			jQuery('.submission_type_redirect').addClass('in');
		}
	})
	jQuery('#sortable3').on('click', '.delete', function(){
		jQuery(this).parents('li').remove();
		return false;
	})
  jQuery('#sortable3').on('click', '.delete', function(){
		jQuery(this).parents('li').remove();
		return false;
	})
	jQuery('#sortable3').on('change', 'select[name="descriptionplacement[]"]', function(){
		jQuery(this).parents('li').find('.help').hide();
		if(jQuery(this).val()=='above'){
			jQuery(this).parents('li').find('.help.above').show();
		}else{
			jQuery(this).parents('li').find('.help.below').show();
		}
	})
	var c = setInterval(function(){
		jQuery('#sortable3 li').each(function(){
			if(jQuery(this).find('.required').is(':checked')){
				if(!jQuery(this).find('.field_label').hasClass('required')){
				jQuery(this).find('.required').prev('input[name="required[]"]').val('on');
				jQuery(this).find('.field_label').addClass('required');
				jQuery(this).find('.field_label').append(' <span>*</span>');
				}
			}else{
				jQuery(this).find('.required').prev('input[name="required[]"]').val('');
				jQuery(this).find('.field_label').removeClass('required');
				jQuery(this).find('.field_label span').remove();
			}
		})
	}, 100)
})
</script>
@endsection
