@extends('layouts.app')

@section('extra')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/codemirror.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/theme/ambiance.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/dialog/dialog.min.css" />

@endsection

@section('content')
<form method="post" action="{{ route('widget.save') }}">
{{csrf_field()}}
<input type="hidden" name="id" value="{{ $widget->id }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ getWidgetTitle($widget->type) }}</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-8 m-t-15">
        <div class="d-flex justify-content-between">
            <div class="left">
                  <div class="shortcodeplaceholder">
                      <input type="text" id="shortcode" class="form-control" value="{{ $widget->shortcode }}" readonly>
                      <button type="button" class="copyshortcode" data-clipboard-target="#shortcode"  data-toggle="popover" data-placement="bottom" data-content="Copied!">
                          <i class="fa fa-copy"></i>
                      </button>
                  </div>
            </div>
            <div class="right">
              <button type="submit" id="save" class="btn btn-primary waves-effect waves-light btn-lg m-b-20">Update</button>
            </div>
        </div>
				<div class="card-box">
					<div class="form-group m-t-15">

            <h4 class="text-muted m-b-15 m-t-15 font-15">Blog Style</h4>

							<select class="selectpicker" data-live-search="true" name="blogstyle">
								<option value="">Default</option>
								@if(sizeof($themesettings['blogtemplates']))
									@foreach($themesettings['blogtemplates'] as $key=>$template)

										<option value="{{ $key }}" {{ !empty($settings['blogstyle']) && $key == $settings['blogstyle']?'selected':'' }}>{{ $template }}</option>

									@endforeach
								@endif
							</select>

					</div>
					<div class="form-group m-t-15">
            <h4 class="text-muted m-b-15 m-t-15 font-15">Number of posts</h4>
						<input class="form-control" type="text" id="noofposts" name="noofposts"  value="{{ !empty($settings['noofposts'])?$settings['noofposts']:'10' }}">
					</div>
					<div class="form-group m-t-15">
            <h4 class="text-muted m-b-15 m-t-15 font-15">Number of Columns</h4>
						<input class="form-control" type="number" min="1" max="12" id="noofcolumns" name="noofcolumns"  value="{{ !empty($settings['noofcolumns'])?$settings['noofcolumns']:'3' }}">
					</div>
					<div class="form-group m-t-15">
						<div class="checkbox">
						  <input type="checkbox" id="showfeatured" name="showfeatured" value="yes" {{ !empty($settings['showfeatured'])?'checked':'' }}><label for="showfeatured">Show Featured Image</label>
						</div>
					</div>
					<div class="form-group m-t-15">
						<div class="checkbox">
						  <input type="checkbox" id="showdate" name="showdate" value="yes" {{ !empty($settings['showdate'])?'checked':'' }}><label for="showdate">Show Date</label>
						</div>
					</div>
					<div class="form-group m-t-15">
						<div class="checkbox">
						  <input type="checkbox" id="showexcerpt" name="showexcerpt" value="yes" {{ !empty($settings['showexcerpt'])?'checked':'' }}><label for="showexcerpt">Show Excerpt</label>
						</div>
					</div>
					<div class="form-group m-t-15">
						<div class="checkbox">
						  <input type="checkbox" id="showpagination" name="showpagination" value="yes" {{ !empty($settings['showpagination'])?'checked':'' }}><label for="showpagination">Show Pagination</label>
						</div>
					</div>
					<div class="form-group m-t-15">
            <h4 class="text-muted m-b-15 m-t-15 font-15">Excerpt Length</h4>
						<input class="form-control" type="text" id="excerptlength" name="excerptlength"  value="{{ !empty($settings['excerptlength'])?$settings['excerptlength']:'100' }}">
					</div>
					<div class="form-group m-t-15">
            <h4 class="text-muted m-b-15 m-t-15 font-15">Posts per page</h4>
						<input class="form-control" type="text" id="perpage" name="perpage"  value="{{ !empty($settings['perpage'])?$settings['perpage']:'10' }}">
					</div>

					<div class="form-group  m-t-15">
            <h4 class="text-muted m-b-15 m-t-15 font-15">Categories</h4>
            @if(sizeof($categories)>0)
						@foreach($categories as $category)
						<div class="checkbox">
							<input name="categories[]" id="checkbox{{ $category->slug }}" type="checkbox" value="{{ $category->slug }}" {{ !empty($settings['categories']) && in_array($category->slug, $settings['categories'])?'checked':'' }}>
							<label for="checkbox{{ $category->slug }}">
								{{ $category->title }}
							</label>
						</div>
						@endforeach
            @else
            No Category
            @endif
					</div>

					<div class="form-group m-t-15">
            <h4 class="text-muted m-b-15 m-t-15 font-15">Date Format</h4>
						<input class="form-control" type="text" id="dateformat" name="dateformat"  value="{!! !empty($settings['dateformat'])?$settings['dateformat']:'d/m/Y' !!}">
					</div>
					<div class="form-group m-t-15">
            <h4 class="text-muted m-b-15 m-t-15 font-15">Read More</h4>
						<input class="form-control" type="text" id="readmore" name="readmore"  value="{!! !empty($settings['readmore'])?$settings['readmore']:'' !!}">
						<div class="hint">{readmore}, {link}</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4 m-t-15">
				<div class="card-box">
						<div class="panel panel-default">
							<div class="panel-heading padded">
								<h3 class="panel-title">Custom Template</h3>
							</div>
							<div class="panel-body p-0 code-edit-wrap">
								<textarea id="customtemplate" class="form-control" name="customtemplate">{{ !empty($settings['customtemplate']) ? $settings['customtemplate'] :'' }}</textarea>
							</div>
						</div>
						<small>Tags: <br /> {title}, {columns}, {date}, {featuredImage}, {readmore}, {excerpt}, {link}, {featuredImageUrl}</small>
				</div>
			</div>
    </div>
</div>
</form>
<script>
jQuery(document).ready(function(){
	jQuery('#get').change(function(){
		jQuery('.get').hide();
		jQuery('#'+jQuery(this).val()).show();
	})
})
</script>
<script>
	var resizefunc = [];
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/codemirror.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/mode/xml/xml.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/dialog/dialog.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/search/searchcursor.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/search/search.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/search/matchesonscrollbar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/search/jump-to-line.min.js"></script>
<script>
!function($) {
    "use strict";

    var CodeEditor = function() {};

    CodeEditor.prototype.getSelectedRange = function(editor) {
        return { from: editor.getCursor(true), to: editor.getCursor(false) };
    },
    CodeEditor.prototype.autoFormatSelection = function(editor) {
        var range = this.getSelectedRange(editor);
        editor.autoFormatRange(range.from, range.to);
    },
    CodeEditor.prototype.commentSelection = function(isComment, editor) {
        var range = this.getSelectedRange(editor);
        editor.commentRange(isComment, range.from, range.to);
    },
    CodeEditor.prototype.init = function() {
        var $this = this;
        //init plugin
        CodeMirror.fromTextArea(document.getElementById("customtemplate"), {
            mode: {name: "xml", alignCDATA: true},
            lineNumbers: true
        });
    },
    //init
    $.CodeEditor = new CodeEditor, $.CodeEditor.Constructor = CodeEditor
}(window.jQuery),

//initializing
function($) {
    "use strict";
    $.CodeEditor.init()
}(window.jQuery);
</script>
@endsection
