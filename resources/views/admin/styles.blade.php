@extends('layouts.app')

@section('extra')

<style>
.body_option{
	display: none;
}
</style>
@endsection

@section('content')

<form role="form" id="savefont" method="post" action="{{ route("settings.saveStyles") }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Options <span>Common Styles</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
						<div class="alert alert-success">
							{!! \Session::get('success') !!}
						</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<ul class="nav nav-tabs tabs">
					<li class="active tab">
						<a href="#tab1" data-toggle="tab" aria-expanded="false">
							<span>Body Background</span>
						</a>
					</li>
					<li class="tab">
						<a href="#tab2" data-toggle="tab" aria-expanded="false">
							<span>Fonts</span>
						</a>
					</li>
					<li class="tab">
						<a href="#tab3" data-toggle="tab" aria-expanded="false">
							<span>Default Elements (Global)</span>
						</a>
					</li>
					<li class="tab">
						<a href="#tab4" data-toggle="tab" aria-expanded="false">
							<span>Custom Classes (Global)</span>
						</a>
					</li>
					<li class="tab">
						<a href="#tab5" data-toggle="tab" aria-expanded="false">
							<span>Animation</span>
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab1">
                <div class="form-group">
                  <div class="radio">
                  <input type="radio" id="body_default" name="body_bgtype" value="" {{ empty($savedsettings['body_bgtype']) ? 'checked' : "" }}>
                  <label for="body_default">None</label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="radio">
                  <input type="radio" id="body_bgimage" name="body_bgtype" value="bgimage" {{ !empty($savedsettings['body_bgtype']) && $savedsettings['body_bgtype']=='bgimage' ? 'checked' : "" }}>
                  <label for="body_bgimage">Background Image</label>
                  </div>
                  <div class="body_option bgimage" style="padding-left: 25px;">
                    <a href="#" class="fileupload btn btn-primary waves-effect waves-light" data-toggle="custommodal" data-target="#media-select"><i class="ion-upload m-r-5"></i>Add Image</a>
                    <input type="text" name="body_bgimage" class="form-control" placeholder="Image url here" value="{{ !empty($savedsettings['body_bgimage']) ? $savedsettings['body_bgimage'] : "" }}" style="max-width: 400px;margin-left: 10px;display: inline-block;">
                  </div>
                </div>
                <div class="form-group">
                  <div class="radio">
                    <input type="radio" id="body_bgcolor" name="body_bgtype" value="bgcolor" {{ !empty($savedsettings['body_bgtype']) && $savedsettings['body_bgtype']=='bgcolor' ? 'checked' : "" }}>
                    <label for="body_bgcolor">Colored</label></div>
                    <div  class="body_option bgcolor" style="padding-left: 15px;    max-width: 400px;">
                        <div data-color-format="rgba" data-color="{{ !empty($savedsettings['body_bgcolor']) ? $savedsettings['body_bgcolor'] : "rgba(255,255,255,1)" }}" class="colorpicker-rgba input-group colorpicker-element">
                          <input type="text" name="body_bgcolor" value="{{ !empty($savedsettings['body_bgcolor']) ? $savedsettings['body_bgcolor'] : "rgba(255,255,255,1)" }}" class="form-control">
                          <span class="input-group-btn add-on">
                            <button class="btn btn-white" type="button">
                              <i style="{{ !empty($savedsettings['body_bgcolor']) ? $savedsettings['body_bgcolor'] : 'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                            </button>
                          </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                  <div class="radio">
                    <input type="radio" id="body_bggradientcolor" name="body_bgtype" value="bggradientcolor" {{ !empty($savedsettings['body_bgtype']) && $savedsettings['body_bgtype']=='bggradientcolor' ? 'checked' : "" }}>
                    <label for="body_bggradientcolor">Gradient Colored</label></div>
                    <div class="body_option bggradientcolor" style="padding-left: 15px;    max-width: 400px;">
                       <div class="m-t-0">
					   From:
                        <div data-color-format="rgba" data-color="{{ !empty($savedsettings['body_bggradientcolor_from']) ? $savedsettings['body_bggradientcolor_from'] : "rgba(255,255,255,1)" }}" class="colorpicker-rgba input-group colorpicker-element">
                          <input type="text" name="body_bggradientcolor_from" value="{{ !empty($savedsettings['body_bggradientcolor_from']) ? $savedsettings['body_bggradientcolor_from'] : "rgba(255,255,255,1)" }}" class="form-control">
                          <span class="input-group-btn add-on">
                            <button class="btn btn-white" type="button">
                              <i style="{{ !empty($savedsettings['body_bggradientcolor_from']) ? $savedsettings['body_bggradientcolor_from'] : 'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
                            </button>
                          </span>
                        </div>
						</div>

						<div class="m-t-20">
							To:
							<div data-color-format="rgba" data-color="{{ !empty($savedsettings['body_bggradientcolor_to']) ? $savedsettings['body_bggradientcolor_to'] : "rgba(255,255,255,1)" }}" class="colorpicker-rgba input-group colorpicker-element">
							  <input type="text" name="body_bggradientcolor_to" value="{{ !empty($savedsettings['body_bggradientcolor_to']) ? $savedsettings['body_bggradientcolor_to'] : "rgba(255,255,255,1)" }}" class="form-control">
							  <span class="input-group-btn add-on">
								<button class="btn btn-white" type="button">
								  <i style="{{ !empty($savedsettings['body_bggradientcolor_to']) ? $savedsettings['body_bggradientcolor_to'] : 'rgba(255,255,255,1)' }};margin-top: 2px;"></i>
								</button>
							  </span>
							</div>
						</div>
						<div class="m-t-20">
							Angle:
							<input type="number" min="0px" name="body_bggradientcolor_angle" value="{{ !empty($savedsettings['body_bggradientcolor_angle']) ? $savedsettings['body_bggradientcolor_angle'] : "0px" }}" class="form-control" placeholder="">
						</div>
                    </div>
                </div>
					</div>
					<div class="tab-pane" id="tab2">
						<div class="repeaters">
							<a href="" class="btn btn-white btn-lg addfont">Add Font</a>
							@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
								@foreach($savedsettings['imported_font_name'] as $key=>$val)
									<div class="repeat row m-t-15">
										<div class="col-sm-5">
												<input type="text" name="imported_font_name[]" value="{{ $val }}" class="form-control" placeholder="Font Name">
											</div>
											<div class="col-sm-5">
												<input type="text" name="imported_font_link[]" value="{{ $savedsettings['imported_font_link'][$key] }}" class="form-control" placeholder="Font Link">
											</div>
										<div class="col-sm-2">
											<button type="button" class="btn btn-white waves-effect btn-repeat-remove">Del</button>
										</div>
									</div>
								@endforeach
							@endif
						</div>



					</div>
					<div class="tab-pane" id="tab3">
							<div class="row">
									<div class="col-md-12">
											<h4 class="text-muted m-b-15 m-t-10 font-15">Font Styles</h4>
											<hr />
											<div class="form-group">
												<div class="row">
												<div class="text-muted m-b-15 m-t-10 font-15 col-md-2">Body Font</div>
												<div class="col-md-3">
													<select name="body_font" data-style="btn-white" class="form-control selectpicker">
														<option value="">Default</option>
														@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
															@foreach($savedsettings['imported_font_name'] as $key=>$val)
																<option value="{{ $val }}" {{ !empty($savedsettings['body_font']) && $savedsettings['body_font']==$val ? 'selected':'' }}>{{ $val }}</option>
															@endforeach
														@endif
													</select>
												</div>
												<div class="col-md-1">
													<input type="text" name="body_font_size" value="{{ !empty($savedsettings['body_font_size']) ? $savedsettings['body_font_size'] : "16px" }}" class="form-control" placeholder="Font Size" title="Font Size">
												</div>
												<div class="col-md-1">
													<input type="text" name="body_line_height" value="{{ !empty($savedsettings['body_line_height']) ? $savedsettings['body_line_height'] : "16px" }}" class="form-control" placeholder="Line Height" title="Line Height">
												</div>
												<div class="col-md-1">
													<input type="text" name="body_letter_spacing" value="{{ !empty($savedsettings['body_letter_spacing']) ? $savedsettings['body_letter_spacing'] : "" }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
												</div>
												<div class="col-md-2">
													<div data-color-format="rgba" data-color="{{ !empty($savedsettings['body_font_color'])?$savedsettings['body_font_color']:'#333333' }}" class="colorpicker-rgba input-group">
																<input type="text" value="{{ !empty($savedsettings['body_font_color'])?$savedsettings['body_font_color']:'#333333' }}" name="body_font_color" class="form-control" placeholder="Font Color">
																<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($savedsettings['body_font_color'])?$savedsettings['body_font_color']:'#333333' }};margin-top: 2px;"></i>
																	</button>
																</span>
															</div>
												</div>
												<div class="col-md-2">
													<select name="body_font_weight" data-style="btn-white" class="form-control selectpicker">
														<option value="">Font Weight</option>
														<option value="300" {{ !empty($savedsettings['body_font_weight']) && $savedsettings['body_font_weight'] == '300'?'selected':'' }}>300 (Light)</option>
														<option value="400" {{ !empty($savedsettings['body_font_weight']) && $savedsettings['body_font_weight'] == '400'?'selected':'' }}>400 (Regular)</option>
														<option value="600" {{ !empty($savedsettings['body_font_weight']) && $savedsettings['body_font_weight'] == '600'?'selected':'' }}>600 (Semi Bold)</option>
														<option value="700" {{ !empty($savedsettings['body_font_weight']) && $savedsettings['body_font_weight'] == '700'?'selected':'' }}>700 (Bold)</option>
													</select>
												</div>
											</div>
											</div>
											<div class="form-group">
												<div class="row">
												<div class="text-muted m-b-15 m-t-10 font-15 col-md-2">Paragraph font</div>
												<div class="col-md-3">
													<select name="p_font" data-style="btn-white" class="form-control selectpicker">
														<option value="">Default</option>
														@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
															@foreach($savedsettings['imported_font_name'] as $key=>$val)
																<option value="{{ $val }}" {{ !empty($savedsettings['p_font']) && $savedsettings['p_font']==$val ? 'selected':'' }}>{{ $val }}</option>
															@endforeach
														@endif
													</select>
												</div>
												<div class="col-md-1">
													<input type="text" name="p_font_size" value="{{ !empty($savedsettings['p_font_size']) ? $savedsettings['p_font_size'] : "" }}" class="form-control" placeholder="Font Size" title="Font Size">
												</div>
												<div class="col-md-1">
													<input type="text" name="p_line_height" value="{{ !empty($savedsettings['p_line_height']) ? $savedsettings['p_line_height'] : "" }}" class="form-control" placeholder="Line Height" title="Line Height">
												</div>
												<div class="col-md-1">
													<input type="text" name="p_letter_spacing" value="{{ !empty($savedsettings['p_letter_spacing']) ? $savedsettings['p_letter_spacing'] : "" }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
												</div>
												<div class="col-md-2">
													<div data-color-format="rgba" data-color="{{ !empty($savedsettings['p_font_color'])?$savedsettings['p_font_color']:'#333333' }}" class="colorpicker-rgba input-group">
																<input type="text" value="{{ !empty($savedsettings['p_font_color'])?$savedsettings['p_font_color']:'#333333' }}" name="p_font_color" class="form-control" placeholder="Font Color">
																<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($savedsettings['p_font_color'])?$savedsettings['p_font_color']:'#333333' }};margin-top: 2px;"></i>
																	</button>
																</span>
															</div>
												</div>
												<div class="col-md-2">
													<select name="p_font_weight" data-style="btn-white" class="form-control selectpicker">
														<option value="">Font Weight</option>
														<option value="300" {{ !empty($savedsettings['p_font_weight']) && $savedsettings['p_font_weight'] == '300'?'selected':'' }}>300 (Light)</option>
														<option value="400" {{ !empty($savedsettings['p_font_weight']) && $savedsettings['p_font_weight'] == '400'?'selected':'' }}>400 (Regular)</option>
														<option value="600" {{ !empty($savedsettings['p_font_weight']) && $savedsettings['p_font_weight'] == '600'?'selected':'' }}>600 (Semi Bold)</option>
														<option value="700" {{ !empty($savedsettings['p_font_weight']) && $savedsettings['p_font_weight'] == '700'?'selected':'' }}>700 (Bold)</option>
													</select>
												</div>
											</div>
											</div>
											<div class="form-group">
												<div class="row">
												<div class="text-muted m-b-15 m-t-10 font-15 col-md-2">Heading 1 font</div>
												<div class="col-md-3">

													<select name="h1_font" data-style="btn-white" class="form-control selectpicker">
														<option value="">Default</option>
														@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
															@foreach($savedsettings['imported_font_name'] as $key=>$val)
																<option value="{{ $val }}" {{ !empty($savedsettings['h1_font']) && $savedsettings['h1_font']==$val ? 'selected':'' }}>{{ $val }}</option>
															@endforeach
														@endif
													</select>
												</div>
												<div class="col-md-1">
													<input type="text" name="h1_font_size" value="{{ !empty($savedsettings['h1_font_size']) ? $savedsettings['h1_font_size'] : "" }}" class="form-control" placeholder="Font Size" title="Font Size">
												</div>
												<div class="col-md-1">
													<input type="text" name="h1_line_height" value="{{ !empty($savedsettings['h1_line_height']) ? $savedsettings['h1_line_height'] : "" }}" class="form-control" placeholder="Line Height" title="Line Height">
												</div>
												<div class="col-md-1">
													<input type="text" name="h1_letter_spacing" value="{{ !empty($savedsettings['h1_letter_spacing']) ? $savedsettings['h1_letter_spacing'] : "" }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
												</div>
												<div class="col-md-2">
													<div data-color-format="rgba" data-color="{{ !empty($savedsettings['h1_font_color'])?$savedsettings['h1_font_color']:'#333333' }}" class="colorpicker-rgba input-group">
																<input type="text" value="{{ !empty($savedsettings['h1_font_color'])?$savedsettings['h1_font_color']:'#333333' }}" name="h1_font_color" class="form-control" placeholder="Font Color">
																<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($savedsettings['h1_font_color'])?$savedsettings['h1_font_color']:'#333333' }};margin-top: 2px;"></i>
																	</button>
																</span>
															</div>
												</div>
												<div class="col-md-2">
													<select name="h1_font_weight" data-style="btn-white" class="form-control selectpicker">
														<option value="">Font Weight</option>
														<option value="300" {{ !empty($savedsettings['h1_font_weight']) && $savedsettings['h1_font_weight'] == '300'?'selected':'' }}>300 (Light)</option>
														<option value="400" {{ !empty($savedsettings['h1_font_weight']) && $savedsettings['h1_font_weight'] == '400'?'selected':'' }}>400 (Regular)</option>
														<option value="600" {{ !empty($savedsettings['h1_font_weight']) && $savedsettings['h1_font_weight'] == '600'?'selected':'' }}>600 (Semi Bold)</option>
														<option value="700" {{ !empty($savedsettings['h1_font_weight']) && $savedsettings['h1_font_weight'] == '700'?'selected':'' }}>700 (Bold)</option>
													</select>
												</div>
											</div>
											</div>

											<div class="form-group">
												<div class="row">
												<div class="text-muted m-b-15 m-t-10 font-15 col-md-2">Heading 2 font</div>
												<div class="col-md-3">

													<select name="h2_font" data-style="btn-white" class="form-control selectpicker">
														<option value="">Default</option>
														@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
															@foreach($savedsettings['imported_font_name'] as $key=>$val)
																<option value="{{ $val }}" {{ !empty($savedsettings['h2_font']) && $savedsettings['h2_font']==$val ? 'selected':'' }}>{{ $val }}</option>
															@endforeach
														@endif
													</select>
												</div>
												<div class="col-md-1">
													<input type="text" name="h2_font_size" value="{{ !empty($savedsettings['h2_font_size']) ? $savedsettings['h2_font_size'] : "" }}" class="form-control" placeholder="Font Size" title="Font Size">
												</div>
												<div class="col-md-1">
													<input type="text" name="h2_line_height" value="{{ !empty($savedsettings['h2_line_height']) ? $savedsettings['h2_line_height'] : "" }}" class="form-control" placeholder="Line Height" title="Line Height">
												</div>
												<div class="col-md-1">
													<input type="text" name="h2_letter_spacing" value="{{ !empty($savedsettings['h2_letter_spacing']) ? $savedsettings['h2_letter_spacing'] : "" }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
												</div>
												<div class="col-md-2">
													<div data-color-format="rgba" data-color="{{ !empty($savedsettings['h2_font_color'])?$savedsettings['h2_font_color']:'#333333' }}" class="colorpicker-rgba input-group">
																<input type="text" value="{{ !empty($savedsettings['h2_font_color'])?$savedsettings['h2_font_color']:'#333333' }}" name="h2_font_color" class="form-control" placeholder="Font Color">
																<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($savedsettings['h2_font_color'])?$savedsettings['h2_font_color']:'#333333' }};margin-top: 2px;"></i>
																	</button>
																</span>
															</div>
												</div>
												<div class="col-md-2">
													<select name="h2_font_weight" data-style="btn-white" class="form-control selectpicker">
														<option value="">Font Weight</option>
														<option value="300" {{ !empty($savedsettings['h2_font_weight']) && $savedsettings['h2_font_weight'] == '300'?'selected':'' }}>300 (Light)</option>
														<option value="400" {{ !empty($savedsettings['h2_font_weight']) && $savedsettings['h2_font_weight'] == '400'?'selected':'' }}>400 (Regular)</option>
														<option value="600" {{ !empty($savedsettings['h2_font_weight']) && $savedsettings['h2_font_weight'] == '600'?'selected':'' }}>600 (Semi Bold)</option>
														<option value="700" {{ !empty($savedsettings['h2_font_weight']) && $savedsettings['h2_font_weight'] == '700'?'selected':'' }}>700 (Bold)</option>
													</select>
												</div>
											</div>
											</div>
											<div class="form-group">
												<div class="row">
												<div class="text-muted m-b-15 m-t-10 font-15 col-md-2">Heading 3 font</div>
												<div class="col-md-3">
													<select name="h3_font" data-style="btn-white" class="form-control selectpicker">
														<option value="">Default</option>
														@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
															@foreach($savedsettings['imported_font_name'] as $key=>$val)
																<option value="{{ $val }}" {{ !empty($savedsettings['h3_font']) && $savedsettings['h3_font']==$val ? 'selected':'' }}>{{ $val }}</option>
															@endforeach
														@endif
													</select>
												</div>
												<div class="col-md-1">
													<input type="text" name="h3_font_size" value="{{ !empty($savedsettings['h3_font_size']) ? $savedsettings['h3_font_size'] : "" }}" class="form-control" placeholder="Font Size" title="Font Size">
												</div>
												<div class="col-md-1">
													<input type="text" name="h3_line_height" value="{{ !empty($savedsettings['h3_line_height']) ? $savedsettings['h3_line_height'] : "" }}" class="form-control" placeholder="Line Height" title="Line Height">
												</div>
												<div class="col-md-1">
													<input type="text" name="h3_letter_spacing" value="{{ !empty($savedsettings['h3_letter_spacing']) ? $savedsettings['h3_letter_spacing'] : "" }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
												</div>
												<div class="col-md-2">
													<div data-color-format="rgba" data-color="{{ !empty($savedsettings['h3_font_color'])?$savedsettings['h3_font_color']:'#333333' }}" class="colorpicker-rgba input-group">
																<input type="text" value="{{ !empty($savedsettings['h3_font_color'])?$savedsettings['h3_font_color']:'#333333' }}" name="h3_font_color" class="form-control" placeholder="Font Color">
																<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($savedsettings['h3_font_color'])?$savedsettings['h3_font_color']:'#333333' }};margin-top: 2px;"></i>
																	</button>
																</span>
															</div>
												</div>
												<div class="col-md-2">
													<select name="h3_font_weight" data-style="btn-white" class="form-control selectpicker">
														<option value="">Font Weight</option>
														<option value="300" {{ !empty($savedsettings['h3_font_weight']) && $savedsettings['h3_font_weight'] == '300'?'selected':'' }}>300 (Light)</option>
														<option value="400" {{ !empty($savedsettings['h3_font_weight']) && $savedsettings['h3_font_weight'] == '400'?'selected':'' }}>400 (Regular)</option>
														<option value="600" {{ !empty($savedsettings['h3_font_weight']) && $savedsettings['h3_font_weight'] == '600'?'selected':'' }}>600 (Semi Bold)</option>
														<option value="700" {{ !empty($savedsettings['h3_font_weight']) && $savedsettings['h3_font_weight'] == '700'?'selected':'' }}>700 (Bold)</option>
													</select>
												</div>
											</div>
											</div>
											<div class="form-group">
												<div class="row">
												<div class="text-muted m-b-15 m-t-10 font-15 col-md-2">Heading 4 font</div>
												<div class="col-md-3">
													<select name="h4_font" data-style="btn-white" class="form-control selectpicker">
														<option value="">Default</option>
														@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
															@foreach($savedsettings['imported_font_name'] as $key=>$val)
																<option value="{{ $val }}" {{ !empty($savedsettings['h4_font']) && $savedsettings['h4_font']==$val ? 'selected':'' }}>{{ $val }}</option>
															@endforeach
														@endif
													</select>
												</div>
												<div class="col-md-1">
													<input type="text" name="h4_font_size" value="{{ !empty($savedsettings['h4_font_size']) ? $savedsettings['h4_font_size'] : "" }}" class="form-control" placeholder="Font Size" title="Font Size">
												</div>
												<div class="col-md-1">
													<input type="text" name="h4_line_height" value="{{ !empty($savedsettings['h4_line_height']) ? $savedsettings['h4_line_height'] : "" }}" class="form-control" placeholder="Line Height" title="Line Height">
												</div>
												<div class="col-md-1">
													<input type="text" name="h4_letter_spacing" value="{{ !empty($savedsettings['h4_letter_spacing']) ? $savedsettings['h4_letter_spacing'] : "" }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
												</div>
												<div class="col-md-2">
													<div data-color-format="rgba" data-color="{{ !empty($savedsettings['h4_font_color'])?$savedsettings['h4_font_color']:'#333333' }}" class="colorpicker-rgba input-group">
																<input type="text" value="{{ !empty($savedsettings['h4_font_color'])?$savedsettings['h4_font_color']:'#333333' }}" name="h4_font_color" class="form-control" placeholder="Font Color">
																<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($savedsettings['h4_font_color'])?$savedsettings['h4_font_color']:'#333333' }};margin-top: 2px;"></i>
																	</button>
																</span>
															</div>
												</div>
												<div class="col-md-2">
													<select name="h4_font_weight" data-style="btn-white" class="form-control selectpicker">
														<option value="">Font Weight</option>
														<option value="300" {{ !empty($savedsettings['h4_font_weight']) && $savedsettings['h4_font_weight'] == '300'?'selected':'' }}>300 (Light)</option>
														<option value="400" {{ !empty($savedsettings['h4_font_weight']) && $savedsettings['h4_font_weight'] == '400'?'selected':'' }}>400 (Regular)</option>
														<option value="600" {{ !empty($savedsettings['h4_font_weight']) && $savedsettings['h4_font_weight'] == '600'?'selected':'' }}>600 (Semi Bold)</option>
														<option value="700" {{ !empty($savedsettings['h4_font_weight']) && $savedsettings['h4_font_weight'] == '700'?'selected':'' }}>700 (Bold)</option>
													</select>
												</div>
											</div>
											</div>
											<div class="form-group">
												<div class="row">
												<div class="text-muted m-b-15 m-t-10 font-15 col-md-2">Heading 5 font</div>
												<div class="col-md-3">

													<select name="h5_font" data-style="btn-white" class="form-control selectpicker">
														<option value="">Default</option>
														@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
															@foreach($savedsettings['imported_font_name'] as $key=>$val)
																<option value="{{ $val }}" {{ !empty($savedsettings['h5_font']) && $savedsettings['h5_font']==$val ? 'selected':'' }}>{{ $val }}</option>
															@endforeach
														@endif
													</select>
												</div>
												<div class="col-md-1">
													<input type="text" name="h5_font_size" value="{{ !empty($savedsettings['h5_font_size']) ? $savedsettings['h5_font_size'] : "" }}" class="form-control" placeholder="Font Size" title="Font Size">
												</div>
												<div class="col-md-1">
													<input type="text" name="h5_line_height" value="{{ !empty($savedsettings['h5_line_height']) ? $savedsettings['h5_line_height'] : "" }}" class="form-control" placeholder="Line Height" title="Line Height">
												</div>
												<div class="col-md-1">
													<input type="text" name="h5_letter_spacing" value="{{ !empty($savedsettings['h5_letter_spacing']) ? $savedsettings['h5_letter_spacing'] : "" }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
												</div>
												<div class="col-md-2">
													<div data-color-format="rgba" data-color="{{ !empty($savedsettings['h5_font_color'])?$savedsettings['h5_font_color']:'#333333' }}" class="colorpicker-rgba input-group">
																<input type="text" value="{{ !empty($savedsettings['h5_font_color'])?$savedsettings['h5_font_color']:'#333333' }}" name="h5_font_color" class="form-control" placeholder="Font Color">
																<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($savedsettings['h5_font_color'])?$savedsettings['h5_font_color']:'#333333' }};margin-top: 2px;"></i>
																	</button>
																</span>
															</div>
												</div>
												<div class="col-md-2">
													<select name="h5_font_weight" data-style="btn-white" class="form-control selectpicker">
														<option value="">Font Weight</option>
														<option value="300" {{ !empty($savedsettings['h5_font_weight']) && $savedsettings['h5_font_weight'] == '300'?'selected':'' }}>300 (Light)</option>
														<option value="400" {{ !empty($savedsettings['h5_font_weight']) && $savedsettings['h5_font_weight'] == '400'?'selected':'' }}>400 (Regular)</option>
														<option value="600" {{ !empty($savedsettings['h5_font_weight']) && $savedsettings['h5_font_weight'] == '600'?'selected':'' }}>600 (Semi Bold)</option>
														<option value="700" {{ !empty($savedsettings['h5_font_weight']) && $savedsettings['h5_font_weight'] == '700'?'selected':'' }}>700 (Bold)</option>
													</select>
												</div>
											</div>
											</div>
											<div class="form-group">
												<div class="row">
												<div class="text-muted m-b-15 m-t-10 font-15 col-md-2">Heading 6 font</div>
												<div class="col-md-3">

													<select name="h6_font" data-style="btn-white" class="form-control selectpicker">
														<option value="">Default</option>
														@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
															@foreach($savedsettings['imported_font_name'] as $key=>$val)
																<option value="{{ $val }}" {{ !empty($savedsettings['h6_font']) && $savedsettings['h6_font']==$val ? 'selected':'' }}>{{ $val }}</option>
															@endforeach
														@endif
													</select>
												</div>
												<div class="col-md-1">
													<input type="text" name="h6_font_size" value="{{ !empty($savedsettings['h6_font_size']) ? $savedsettings['h6_font_size'] : "" }}" class="form-control" placeholder="Font Size" title="Font Size">
												</div>
												<div class="col-md-1">
													<input type="text" name="h6_line_height" value="{{ !empty($savedsettings['h6_line_height']) ? $savedsettings['h6_line_height'] : "" }}" class="form-control" placeholder="Line Height" title="Line Height">
												</div>
												<div class="col-md-1">
													<input type="text" name="h6_letter_spacing" value="{{ !empty($savedsettings['h6_letter_spacing']) ? $savedsettings['h6_letter_spacing'] : "" }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
												</div>
												<div class="col-md-2">
													<div data-color-format="rgba" data-color="{{ !empty($savedsettings['h6_font_color'])?$savedsettings['h6_font_color']:'#333333' }}" class="colorpicker-rgba input-group">
																<input type="text" value="{{ !empty($savedsettings['h6_font_color'])?$savedsettings['h6_font_color']:'#333333' }}" name="h6_font_color" class="form-control" placeholder="Font Color">
																<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($savedsettings['h6_font_color'])?$savedsettings['h6_font_color']:'#333333' }};margin-top: 2px;"></i>
																	</button>
																</span>
															</div>
												</div>
												<div class="col-md-2">
													<select name="h6_font_weight" data-style="btn-white" class="form-control selectpicker">
														<option value="">Font Weight</option>
														<option value="300" {{ !empty($savedsettings['h6_font_weight']) && $savedsettings['h6_font_weight'] == '300'?'selected':'' }}>300 (Light)</option>
														<option value="400" {{ !empty($savedsettings['h6_font_weight']) && $savedsettings['h6_font_weight'] == '400'?'selected':'' }}>400 (Regular)</option>
														<option value="600" {{ !empty($savedsettings['h6_font_weight']) && $savedsettings['h6_font_weight'] == '600'?'selected':'' }}>600 (Semi Bold)</option>
														<option value="700" {{ !empty($savedsettings['h6_font_weight']) && $savedsettings['h6_font_weight'] == '700'?'selected':'' }}>700 (Bold)</option>
													</select>
												</div>
											</div>
											</div>
											<div class="form-group">
												<div class="row">
												<div class="text-muted m-b-15 m-t-10 font-15 col-md-2">Link font</div>
												<div class="col-md-3">
													<select name="link_font" data-style="btn-white" class="form-control selectpicker">
														<option value="">Default</option>
														@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
															@foreach($savedsettings['imported_font_name'] as $key=>$val)
																<option value="{{ $val }}" {{ !empty($savedsettings['link_font']) && $savedsettings['link_font']==$val ? 'selected':'' }}>{{ $val }}</option>
															@endforeach
														@endif
													</select>
												</div>
												<div class="col-md-1">
													<input type="text" name="link_font_size" value="{{ !empty($savedsettings['link_font_size']) ? $savedsettings['link_font_size'] : "" }}" class="form-control" placeholder="Font Size" title="Font Size">
												</div>
												<div class="col-md-1">
													<input type="text" name="link_line_height" value="{{ !empty($savedsettings['link_line_height']) ? $savedsettings['link_line_height'] : "" }}" class="form-control" placeholder="Line Height" title="Line Height">
												</div>
												<div class="col-md-1">
													<input type="text" name="link_letter_spacing" value="{{ !empty($savedsettings['link_letter_spacing']) ? $savedsettings['link_letter_spacing'] : "" }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
												</div>
												<div class="col-md-2">
													<div data-color-format="rgba" data-color="{{ !empty($savedsettings['link_font_color'])?$savedsettings['link_font_color']:'#333333' }}" class="colorpicker-rgba input-group">
																<input type="text" value="{{ !empty($savedsettings['link_font_color'])?$savedsettings['link_font_color']:'#333333' }}" name="link_font_color" class="form-control" placeholder="Font Color">
																<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($savedsettings['link_font_color'])?$savedsettings['link_font_color']:'#333333' }};margin-top: 2px;"></i>
																	</button>
																</span>
															</div>
												</div>
												<div class="col-md-2">
													<select name="link_font_weight" data-style="btn-white" class="form-control selectpicker">
														<option value="">Font Weight</option>
														<option value="300" {{ !empty($savedsettings['link_font_weight']) && $savedsettings['link_font_weight'] == '300'?'selected':'' }}>300 (Light)</option>
														<option value="400" {{ !empty($savedsettings['link_font_weight']) && $savedsettings['link_font_weight'] == '400'?'selected':'' }}>400 (Regular)</option>
														<option value="600" {{ !empty($savedsettings['link_font_weight']) && $savedsettings['link_font_weight'] == '600'?'selected':'' }}>600 (Semi Bold)</option>
														<option value="700" {{ !empty($savedsettings['link_font_weight']) && $savedsettings['link_font_weight'] == '700'?'selected':'' }}>700 (Bold)</option>
													</select>
												</div>
											</div>
											</div>
									</div>
							</div>
							<div class="row">
									<div class="col-md-6">
										<h4 class="text-muted m-b-15 m-t-10 font-15">Default Margins</h4>
										<hr />
										<div class="form-group">
											<div class="row">
													<div class="col-md-4">
														<div class="text-muted m-b-15 m-t-10 font-15">Heading 1 Margin</div>
													</div>
													<div class="col-md-4">
														<input type="text" name="h1_margin_top" value="{{ !empty($savedsettings['h1_margin_top']) ? $savedsettings['h1_margin_top'] : "0px" }}" class="form-control" placeholder="Margin Top" title="Margin Top">
													</div>
													<div class="col-md-4">
														<input type="text" name="h1_margin_bottom" value="{{ !empty($savedsettings['h1_margin_bottom']) ? $savedsettings['h1_margin_bottom'] : "15px" }}" class="form-control" placeholder="Margin Bottom" title="Margin Bottom">
													</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
													<div class="col-md-4">
														<div class="text-muted m-b-15 m-t-10 font-15">Heading 2 Margin</div>
													</div>
													<div class="col-md-4">
														<input type="text" name="h2_margin_top" value="{{ !empty($savedsettings['h2_margin_top']) ? $savedsettings['h2_margin_top'] : "0px" }}" class="form-control" placeholder="Margin Top" title="Margin Top">
													</div>
													<div class="col-md-4">
														<input type="text" name="h2_margin_bottom" value="{{ !empty($savedsettings['h2_margin_bottom']) ? $savedsettings['h2_margin_bottom'] : "15px" }}" class="form-control" placeholder="Margin Bottom" title="Margin Bottom">
													</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
													<div class="col-md-4">
														<div class="text-muted m-b-15 m-t-10 font-15">Heading 3 Margin</div>
													</div>
													<div class="col-md-4">
														<input type="text" name="h3_margin_top" value="{{ !empty($savedsettings['h3_margin_top']) ? $savedsettings['h3_margin_top'] : "0px" }}" class="form-control" placeholder="Margin Top" title="Margin Top">
													</div>
													<div class="col-md-4">
														<input type="text" name="h3_margin_bottom" value="{{ !empty($savedsettings['h3_margin_bottom']) ? $savedsettings['h3_margin_bottom'] : "15px" }}" class="form-control" placeholder="Margin Bottom" title="Margin Bottom">
													</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
													<div class="col-md-4">
														<div class="text-muted m-b-15 m-t-10 font-15">Heading 4 Margin</div>
													</div>
													<div class="col-md-4">
														<input type="text" name="h4_margin_top" value="{{ !empty($savedsettings['h4_margin_top']) ? $savedsettings['h4_margin_top'] : "0px" }}" class="form-control" placeholder="Margin Top" title="Margin Top">
													</div>
													<div class="col-md-4">
														<input type="text" name="h4_margin_bottom" value="{{ !empty($savedsettings['h4_margin_bottom']) ? $savedsettings['h4_margin_bottom'] : "15px" }}" class="form-control" placeholder="Margin Bottom" title="Margin Bottom">
													</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
													<div class="col-md-4">
														<div class="text-muted m-b-15 m-t-10 font-15">Heading 5 Margin</div>
													</div>
													<div class="col-md-4">
														<input type="text" name="h5_margin_top" value="{{ !empty($savedsettings['h5_margin_top']) ? $savedsettings['h5_margin_top'] : "0px" }}" class="form-control" placeholder="Margin Top" title="Margin Top">
													</div>
													<div class="col-md-4">
														<input type="text" name="h5_margin_bottom" value="{{ !empty($savedsettings['h5_margin_bottom']) ? $savedsettings['h5_margin_bottom'] : "15px" }}" class="form-control" placeholder="Margin Bottom" title="Margin Bottom">
													</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
													<div class="col-md-4">
														<div class="text-muted m-b-15 m-t-10 font-15">Heading 6 Margin</div>
													</div>
													<div class="col-md-4">
														<input type="text" name="h6_margin_top" value="{{ !empty($savedsettings['h6_margin_top']) ? $savedsettings['h6_margin_top'] : "0px" }}" class="form-control" placeholder="Margin Top" title="Margin Top">
													</div>
													<div class="col-md-4">
														<input type="text" name="h6_margin_bottom" value="{{ !empty($savedsettings['h6_margin_bottom']) ? $savedsettings['h6_margin_bottom'] : "15px" }}" class="form-control" placeholder="Margin Bottom" title="Margin Bottom">
													</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
													<div class="col-md-4">
														<div class="text-muted m-b-15 m-t-10 font-15">Paragraph Margin</div>
													</div>
													<div class="col-md-4">
														<input type="text" name="p_margin_top" value="{{ !empty($savedsettings['p_margin_top']) ? $savedsettings['p_margin_top'] : "0px" }}" class="form-control" placeholder="Margin Top" title="Margin Top">
													</div>
													<div class="col-md-4">
														<input type="text" name="p_margin_bottom" value="{{ !empty($savedsettings['p_margin_bottom']) ? $savedsettings['p_margin_bottom'] : "15px" }}" class="form-control" placeholder="Margin Bottom" title="Margin Bottom">
													</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
									  <h4 class="text-muted m-b-15 m-t-10 font-15">Default Padding</h4>
									  <hr />
									  <div class="form-group">
									    <div class="row">
									        <div class="col-md-4">
									          <div class="text-muted m-b-15 m-t-10 font-15">Heading 1 Padding</div>
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h1_padding_top" value="{{ !empty($savedsettings['h1_padding_top']) ? $savedsettings['h1_padding_top'] : "0px" }}" class="form-control" placeholder="Padding Top" title="Padding Top">
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h1_padding_bottom" value="{{ !empty($savedsettings['h1_padding_bottom']) ? $savedsettings['h1_padding_bottom'] : "0px" }}" class="form-control" placeholder="Padding Bottom" title="Padding Bottom">
									        </div>
									    </div>
									  </div>
									  <div class="form-group">
									    <div class="row">
									        <div class="col-md-4">
									          <div class="text-muted m-b-15 m-t-10 font-15">Heading 2 Padding</div>
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h2_padding_top" value="{{ !empty($savedsettings['h2_padding_top']) ? $savedsettings['h2_padding_top'] : "0px" }}" class="form-control" placeholder="Padding Top" title="Padding Top">
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h2_padding_bottom" value="{{ !empty($savedsettings['h2_padding_bottom']) ? $savedsettings['h2_padding_bottom'] : "0px" }}" class="form-control" placeholder="Padding Bottom" title="Padding Bottom">
									        </div>
									    </div>
									  </div>
									  <div class="form-group">
									    <div class="row">
									        <div class="col-md-4">
									          <div class="text-muted m-b-15 m-t-10 font-15">Heading 3 Padding</div>
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h3_padding_top" value="{{ !empty($savedsettings['h3_padding_top']) ? $savedsettings['h3_padding_top'] : "0px" }}" class="form-control" placeholder="Padding Top" title="Padding Top">
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h3_padding_bottom" value="{{ !empty($savedsettings['h3_padding_bottom']) ? $savedsettings['h3_padding_bottom'] : "0px" }}" class="form-control" placeholder="Padding Bottom" title="Padding Bottom">
									        </div>
									    </div>
									  </div>
									  <div class="form-group">
									    <div class="row">
									        <div class="col-md-4">
									          <div class="text-muted m-b-15 m-t-10 font-15">Heading 4 Padding</div>
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h4_padding_top" value="{{ !empty($savedsettings['h4_padding_top']) ? $savedsettings['h4_padding_top'] : "0px" }}" class="form-control" placeholder="Padding Top" title="Padding Top">
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h4_padding_bottom" value="{{ !empty($savedsettings['h4_padding_bottom']) ? $savedsettings['h4_padding_bottom'] : "0px" }}" class="form-control" placeholder="Padding Bottom" title="Padding Bottom">
									        </div>
									    </div>
									  </div>
									  <div class="form-group">
									    <div class="row">
									        <div class="col-md-4">
									          <div class="text-muted m-b-15 m-t-10 font-15">Heading 5 Padding</div>
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h5_padding_top" value="{{ !empty($savedsettings['h5_padding_top']) ? $savedsettings['h5_padding_top'] : "0px" }}" class="form-control" placeholder="Padding Top" title="Padding Top">
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h5_padding_bottom" value="{{ !empty($savedsettings['h5_padding_bottom']) ? $savedsettings['h5_padding_bottom'] : "0px" }}" class="form-control" placeholder="Padding Bottom" title="Padding Bottom">
									        </div>
									    </div>
									  </div>
									  <div class="form-group">
									    <div class="row">
									        <div class="col-md-4">
									          <div class="text-muted m-b-15 m-t-10 font-15">Heading 6 Padding</div>
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h6_padding_top" value="{{ !empty($savedsettings['h6_padding_top']) ? $savedsettings['h6_padding_top'] : "0px" }}" class="form-control" placeholder="Padding Top" title="Padding Top">
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="h6_padding_bottom" value="{{ !empty($savedsettings['h6_padding_bottom']) ? $savedsettings['h6_padding_bottom'] : "0px" }}" class="form-control" placeholder="Padding Bottom" title="Padding Bottom">
									        </div>
									    </div>
									  </div>
									  <div class="form-group">
									    <div class="row">
									        <div class="col-md-4">
									          <div class="text-muted m-b-15 m-t-10 font-15">Paragraph Padding</div>
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="p_padding_top" value="{{ !empty($savedsettings['p_padding_top']) ? $savedsettings['p_padding_top'] : "0px" }}" class="form-control" placeholder="Padding Top" title="Padding Top">
									        </div>
									        <div class="col-md-4">
									          <input type="text" name="p_padding_bottom" value="{{ !empty($savedsettings['p_padding_bottom']) ? $savedsettings['p_padding_bottom'] : "0px" }}" class="form-control" placeholder="Padding Bottom" title="Padding Bottom">
									        </div>
									    </div>
									  </div>
									</div>
							</div>
					</div>
					<div class="tab-pane" id="tab4">
							<div class="row">
									<div class="col-md-12">
										<div class="repeaters">
											<a href="" class="btn btn-white btn-lg addcustom">Add Custom Selector</a>
											@if(!empty($savedsettings['classid_font_name']) && sizeof($savedsettings['classid_font_name'])>0)
												@foreach($savedsettings['classid_font_name'] as $key=>$val)
													<div class="repeat row m-t-15">
														<div class="col-sm-2">
															<input type="text" name="classid_font_name[]" value="{{ $val }}" class="form-control" placeholder="Class/ID">
														</div>
														<div class="col-sm-2">

																<select name="classid_font[]" data-style="btn-white" class="form-control selectpicker">
																	<option value="">Default</option>
																	@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
																		@foreach($savedsettings['imported_font_name'] as $key2=>$val2)
																			<option value="{{ $val2 }}" {{ !empty($savedsettings['classid_font'][$key]) && $savedsettings['classid_font'][$key]==$val2 ? 'selected':'' }}>{{ $val2 }}</option>
																		@endforeach
																	@endif
																</select>
														</div>
														<div class="col-sm-1">
															<input type="text" name="classid_font_size[]" class="form-control" placeholder="Font Size" value="{{ !empty($savedsettings['classid_font_size'][$key])? $savedsettings['classid_font_size'][$key] : '' }}" title="Font Size">
														</div>
														<div class="col-md-1">
															<input type="text" name="classid_line_height[]" value="{{ !empty($savedsettings['classid_line_height'][$key])? $savedsettings['classid_line_height'][$key] : '' }}" class="form-control" placeholder="Line Height" title="Line Height">
														</div>
														<div class="col-md-1">
															<input type="text" name="classid_letter_spacing" value="{{ !empty($savedsettings['classid_letter_spacing'][$key])? $savedsettings['classid_letter_spacing'][$key] : '' }}" class="form-control" placeholder="Letter Spacing" title="Letter Spacing">
														</div>
														<div class="col-md-2">
															<div data-color-format="rgba" data-color="{{ !empty($savedsettings['classid_font_color'][$key])?$savedsettings['classid_font_color'][$key]:'#333333' }}" class="colorpicker-rgba input-group">
																		<input type="text" value="{{ !empty($savedsettings['classid_font_color'][$key])?$savedsettings['classid_font_color'][$key]:'#333333' }}" name="classid_font_color[]" class="form-control" placeholder="Font Color">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($savedsettings['classid_font_color'][$key])?$savedsettings['classid_font_color'][$key]:'#333333' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
														</div>
														<div class="col-md-2">
															<select name="classid_font_weight[]" data-style="btn-white" class="form-control selectpicker">
																<option value="">Font Weight</option>
																<option value="300" {{ !empty($savedsettings['classid_font_weight'][$key]) && $savedsettings['classid_font_weight'][$key] == '300'?'selected':'' }}>300 (Light)</option>
																<option value="400" {{ !empty($savedsettings['classid_font_weight'][$key]) && $savedsettings['classid_font_weight'][$key] == '400'?'selected':'' }}>400 (Regular)</option>
																<option value="600" {{ !empty($savedsettings['classid_font_weight'][$key]) && $savedsettings['classid_font_weight'][$key] == '600'?'selected':'' }}>600 (Semi Bold)</option>
																<option value="700" {{ !empty($savedsettings['classid_font_weight'][$key]) && $savedsettings['classid_font_weight'][$key] == '700'?'selected':'' }}>700 (Bold)</option>
															</select>
														</div>
														<div class="col-sm-1">
															<button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button>
														</div>
													</div>
												@endforeach
											@endif
										</div>
									</div>
							</div>
					</div>
					<div class="tab-pane" id="tab5">
						<div class="repeaters">
							<a href="" class="btn btn-white btn-lg addanimation">Add Global Animation</a>
							@if(!empty($savedsettings['animation_classid']) && sizeof($savedsettings['animation_classid'])>0)
								@foreach($savedsettings['animation_classid'] as $key=>$val)
									<div class="repeat row m-t-15">
										<div class="col-sm-2">
											<input type="text" name="animation_classid[]" value="{{ $val }}" class="form-control" placeholder="Class/ID">
										</div>
										<div class="col-sm-2">
											<select name="animation_effect[]" data-style="btn-white" class="form-control selectpicker">
												<option value="fade" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'fade'?'selected':'' }}>fade</option>
												<option value="fade-up" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'fade-up'?'selected':'' }}>fade-up</option>
												<option value="fade-down" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'fade-down'?'selected':'' }}>fade-down</option>
												<option value="fade-left" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'fade-left'?'selected':'' }}>fade-left</option>
												<option value="fade-right" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'fade-right'?'selected':'' }}>fade-right</option>
												<option value="fade-up-right" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'fade-up-right'?'selected':'' }}>fade-up-right</option>
												<option value="fade-up-left" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'fade-up-left'?'selected':'' }}>fade-up-left</option>
												<option value="fade-down-right" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'fade-down-right'?'selected':'' }}>fade-down-right</option>
												<option value="fade-down-left" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'fade-down-left'?'selected':'' }}>fade-down-left</option>
												<option value="flip-up" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'flip-up'?'selected':'' }}>flip-up</option>
												<option value="flip-down" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'flip-down'?'selected':'' }}>flip-down</option>
												<option value="flip-left" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'flip-left'?'selected':'' }}>flip-left</option>
												<option value="flip-right" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'flip-right'?'selected':'' }}>flip-right</option>
												<option value="slide-up" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'slide-up'?'selected':'' }}>slide-up</option>
												<option value="slide-down" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'slide-down'?'selected':'' }}>slide-down</option>
												<option value="slide-left" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'slide-left'?'selected':'' }}>slide-left</option>
												<option value="slide-right" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'slide-right'?'selected':'' }}>slide-right</option>
												<option value="zoom-in" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-in'?'selected':'' }}>zoom-in</option>
												<option value="zoom-in-up" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-in-up'?'selected':'' }}>zoom-in-up</option>
												<option value="zoom-in-down" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-in-down'?'selected':'' }}>zoom-in-down</option>
												<option value="zoom-in-left" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-in-left'?'selected':'' }}>zoom-in-left</option>
												<option value="zoom-in-right" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-in-right'?'selected':'' }}>zoom-in-right</option>
												<option value="zoom-out" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-out'?'selected':'' }}>zoom-out</option>
												<option value="zoom-out-up" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-out-up'?'selected':'' }}>zoom-out-up</option>
												<option value="zoom-out-down" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-out-down'?'selected':'' }}>zoom-out-down</option>
												<option value="zoom-out-left" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-out-left'?'selected':'' }}>zoom-out-left</option>
												<option value="zoom-out-right" {{ !empty($savedsettings['animation_effect'][$key]) && $savedsettings['animation_effect'][$key] == 'zoom-out-right'?'selected':'' }}>zoom-out-right</option>
											</select>
                      <div class="checkbox">
        								<input type="checkbox" id="animateOnce{{ $key }}" name="animation_once[]" {{ !empty($savedsettings['animation_once'][$key])?'checked':'' }}>
        								<label for="animateOnce{{ $key }}">
        								Animate Once
        								</label>
        							</div>
										</div>
										<div class="col-sm-2">
											<select name="animation_easing[]" data-style="btn-white" class="form-control selectpicker">
												<option value="linear" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'linear'?'selected':'' }}>linear</option>
												<option value="ease" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease'?'selected':'' }}>ease</option>
												<option value="ease-in" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in'?'selected':'' }}>ease-in</option>
												<option value="ease-out" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-out'?'selected':'' }}>ease-out</option>
												<option value="ease-in-out" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-out'?'selected':'' }}>ease-in-out</option>
												<option value="ease-in-back" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-back'?'selected':'' }}>ease-in-back</option>
												<option value="ease-out-back" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-out-back'?'selected':'' }}>ease-out-back</option>
												<option value="ease-in-out-back" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-out-back'?'selected':'' }}>ease-in-out-back</option>
												<option value="ease-in-sine" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-sine'?'selected':'' }}>ease-in-sine</option>
												<option value="ease-out-sine" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-out-sine'?'selected':'' }}>ease-out-sine</option>
												<option value="ease-in-out-sine" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-out-sine'?'selected':'' }}>ease-in-out-sine</option>
												<option value="ease-in-quad" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-quad'?'selected':'' }}>ease-in-quad</option>
												<option value="ease-out-quad" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-out-quad'?'selected':'' }}>ease-out-quad</option>
												<option value="ease-in-out-quad" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-out-quad'?'selected':'' }}>ease-in-out-quad</option>
												<option value="ease-in-cubic" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-cubic'?'selected':'' }}>ease-in-cubic</option>
												<option value="ease-out-cubic" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-out-cubic'?'selected':'' }}>ease-out-cubic</option>
												<option value="ease-in-out-cubic" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-out-cubic'?'selected':'' }}>ease-in-out-cubic</option>
												<option value="ease-in-quart" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-quart'?'selected':'' }}>ease-in-quart</option>
												<option value="ease-out-quart" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-out-quart'?'selected':'' }}>ease-out-quart</option>
												<option value="ease-in-out-quart" {{ !empty($savedsettings['animation_easing'][$key]) && $savedsettings['animation_easing'][$key] == 'ease-in-out-quart'?'selected':'' }}>ease-in-out-quart</option>
											</select>
										</div>
										<div class="col-sm-1">
											<input type="text" name="animation_offset[]" class="form-control" placeholder="Offset" value="{{ !empty($savedsettings['animation_offset'][$key])? $savedsettings['animation_offset'][$key] : '200' }}" title="Offset">
										</div>
										<div class="col-sm-1">
											<input type="text" name="animation_delay[]" class="form-control" placeholder="Delay" value="{{ !empty($savedsettings['animation_delay'][$key])? $savedsettings['animation_delay'][$key] : '50' }}" title="Delay">
										</div>
										<div class="col-sm-2">
											<input type="text" name="animation_duration[]" class="form-control" placeholder="Duration" value="{{ !empty($savedsettings['animation_duration'][$key])? $savedsettings['animation_duration'][$key] : '1000' }}" title="Duration">
										</div>
										<div class="col-sm-2">
											<button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button>
										</div>
									</div>
								@endforeach
							@endif
						</div>
					</div>






				</div>

			</div>
    </div>
</div>
{{csrf_field()}}
<input type="hidden" name="settingType" value="styles">
<div class="container-fluid">
	<button type="submit" class="btn btn-primary waves-effect waves-light btn-lg">Save All</button>
</div>
</form>

@include('admin._partials.popupuploader')

<script>
jQuery(document).ready(function(){

	jQuery('.repeaters').on('click', '.addfont', function(){
		var html = '<div class="repeat newrow row m-t-15"><div class="col-sm-5"><input type="text" name="imported_font_name[]" value="" class="form-control" placeholder="Font Name"></div><div class="col-sm-5"><input type="text" name="imported_font_link[]" value="" class="form-control" placeholder="Font Link"></div><div class="col-sm-2"><button type="button" class="btn btn-white waves-effect btn-repeat-remove">Del</button></div></div>';
		jQuery(this).parents('.repeaters').append(html);
		return false;
	})
	jQuery('.repeaters').on('click', '.addanimation', function(){
		var rand = Math.floor((Math.random() * 1000) + 1);
		var html = '<div class="repeat row m-t-15"><div class="col-sm-2"><input type="text" name="animation_classid[]" value="" class="form-control" placeholder="Class/ID"></div><div class="col-sm-2"><select name="animation_effect[]" data-style="btn-white" class="form-control selectpicker"><option value="fade">fade</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-left">fade-left</option><option value="fade-right">fade-right</option><option value="fade-up-right">fade-up-right</option><option value="fade-up-left">fade-up-left</option><option value="fade-down-right">fade-down-right</option><option value="fade-down-left">fade-down-left</option><option value="flip-up">flip-up</option><option value="flip-down">flip-down</option><option value="flip-left">flip-left</option><option value="flip-right">flip-right</option><option value="slide-up">slide-up</option><option value="slide-down">slide-down</option><option value="slide-left">slide-left</option><option value="slide-right">slide-right</option><option value="zoom-in">zoom-in</option><option value="zoom-in-up">zoom-in-up</option><option value="zoom-in-down">zoom-in-down</option><option value="zoom-in-left">zoom-in-left</option><option value="zoom-in-right">zoom-in-right</option><option value="zoom-out">zoom-out</option><option value="zoom-out-up">zoom-out-up</option><option value="zoom-out-down">zoom-out-down</option><option value="zoom-out-left">zoom-out-left</option><option value="zoom-out-right">zoom-out-right</option></select><div class="checkbox"><input type="checkbox" id="animateOnce'+rand+'" name="animation_once[]"><label for="animateOnce'+rand+'">Animate Once</label></div></div><div class="col-sm-2"><select name="animation_easing[]" data-style="btn-white" class="form-control selectpicker"><option value="linear">linear</option><option value="ease">ease</option><option value="ease-in">ease-in</option><option value="ease-out">ease-out</option><option value="ease-in-out">ease-in-out</option><option value="ease-in-back">ease-in-back</option><option value="ease-out-back">ease-out-back</option><option value="ease-in-out-back">ease-in-out-back</option><option value="ease-in-sine">ease-in-sine</option><option value="ease-out-sine">ease-out-sine</option><option value="ease-in-out-sine">ease-in-out-sine</option><option value="ease-in-quad">ease-in-quad</option><option value="ease-out-quad">ease-out-quad</option><option value="ease-in-out-quad">ease-in-out-quad</option><option value="ease-in-cubic">ease-in-cubic</option><option value="ease-out-cubic">ease-out-cubic</option><option value="ease-in-out-cubic">ease-in-out-cubic</option><option value="ease-in-quart">ease-in-quart</option><option value="ease-out-quart">ease-out-quart</option><option value="ease-in-out-quart">ease-in-out-quart</option></select></div><div class="col-sm-1"><input type="text" name="animation_offset[]" class="form-control" placeholder="Offset" value="200" title="Offset"></div><div class="col-sm-1"><input type="text" name="animation_delay[]" class="form-control" placeholder="Delay" value="50" title="Delay"></div><div class="col-sm-2"><input type="text" name="animation_duration[]" class="form-control" placeholder="Duration" value="1000" title="Duration"></div><div class="col-sm-2"><button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button></div></div>';
		jQuery(this).parents('.repeaters').append(html);
		$('.selectpicker').selectpicker();
		return false;
	})
	jQuery('.repeaters').on('click', '.addcustom', function(){
		var html = '<div class="repeat row m-t-15"><div class="col-sm-2"><input type="text" name="classid_font_name[]" class="form-control" placeholder="Class/ID"></div><div class="col-sm-2">';
		html = html+'<select name="classid_font[]" data-style="btn-white" class="form-control selectpicker">';
		html = html+'<option value="">Default</option>';
		@if(!empty($savedsettings['imported_font_name']) && sizeof($savedsettings['imported_font_name'])>0)
			@foreach($savedsettings['imported_font_name'] as $key2=>$val2)
				html = html+'<option value="{{ $val2 }}" {{ !empty($savedsettings['classid_font'][$key]) && $savedsettings['classid_font'][$key]==$val2 ? 'selected':'' }}>{{ $val2 }}</option>';
			@endforeach
		@endif
		html = html+'</select>';
		html = html+'</div><div class="col-sm-1"><input type="text" name="classid_font_size[]" class="form-control" placeholder="Font Size" title="Font Size"></div>';
		html = html+'<div class="col-md-1"><input type="text" name="classid_line_height[]" class="form-control" placeholder="Line Height" title="Line Height"></div>';
		html = html+'<div class="col-md-1"><input type="text" name="classid_letter_spacing" class="form-control" placeholder="Letter Spacing" title="Letter Spacing"></div>';
		html = html+'<div class="col-md-2"><div data-color-format="rgba" data-color="#333333" class="colorpicker-rgba input-group"><input type="text" value="#333333" name="classid_font_color[]" class="form-control" placeholder="Font Color"><span class="input-group-btn add-on"><button class="btn btn-white" type="button"><i style="background-color: #333333;margin-top: 2px;"></i></button></span></div></div>';
		html = html+'<div class="col-md-2"><select name="classid_font_weight[]" data-style="btn-white" class="form-control selectpicker"><option value="">Font Weight</option><option value="300">300 (Light)</option><option value="400">400 (Regular)</option><option value="600">600 (Semi Bold)</option><option value="700">700 (Bold)</option></select></div>';
		html = html+'<div class="col-sm-1"><button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button></div></div>';
		jQuery(this).parents('.repeaters').append(html);
		$('.colorpicker-rgba').colorpicker();
		$('.selectpicker').selectpicker();
		return false;
	})
	jQuery('.repeaters').on('click', '.btn-repeat-remove', function(){
		jQuery(this).parents('.repeat').remove();
		return false;
	})
  jQuery('.allmedia').on('click','.imageholder a',function(){
    jQuery('input[name="body_bgimage"]').val(jQuery(this).attr('href'));
    jQuery('#media-select').removeClass('md-custom-show');
    return false;
  })
})

jQuery(document).ready(function($) {
	$('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
	$('.colorpicker-rgba').colorpicker();

	setInterval(function(){
		jQuery('.body_option').hide();
		jQuery('input[name="body_bgtype"]').each(function(){
			if(jQuery(this).is(":checked")){
				if(jQuery(this).val()!=''){
				jQuery('.body_option.'+jQuery(this).val()).show();
				}
			}
		})
	}, 100)
})
</script>
<script src="{{ asset('assets/private/plugins/bootstrap-formhelpers/prettify.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/private/plugins/bootstrap-formhelpers/bootstrap-formhelpers-googlefonts.codes.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/private/plugins/bootstrap-formhelpers/bootstrap-formhelpers-googlefonts.js') }}" type="text/javascript"></script>
@endsection
