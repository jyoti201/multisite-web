@extends('layouts.app')

@section('extra')


<style>
.btn-repeat-remove{
	display:none;
}
.newrow .btn-repeat-remove{
	display:block;
}
.newrow .btn-repeat-add{
	display:none;
}
</style>

@endsection

@section('content')
<form class="form-horizontal" role="form" id="save" method="post" action="{{ route("settings.savecolors") }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Options <span>Colors Settings</h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
						<div class="alert alert-success">
							{!! \Session::get('success') !!}
						</div>
				</div>
			@endif
			<div class="col-sm-6 m-t-15">
				<div class="card-box">
						@if(!empty($settings) && sizeof($settings)>0)
							@foreach($settings as $key=>$header)
							<div class="form-group">
								<label class="col-md-2 control-label">{{ $header['title'] }}</label>
								<div class="col-md-10">
									<div data-color-format="rgba" data-color="{{ !empty($savedsettings[$key])?$savedsettings[$key]:$header['default'] }}" class="colorpicker-rgba input-group">
											<input type="text" readonly="readonly" value="{{ !empty($savedsettings[$key])?$savedsettings[$key]:$header['default'] }}" name="{{ $key }}" class="form-control" placeholder="{{ !empty($savedsettings[$key])?$savedsettings[$key]:$header['default'] }}">
											<span class="input-group-btn add-on">
												<button class="btn btn-white" type="button">
													<i style="background-color: {{ !empty($savedsettings[$key])?$savedsettings[$key]:$header['default'] }};margin-top: 2px;"></i>
												</button>
											</span>
										</div>
								</div>
							</div>
							@endforeach
						@endif
				</div>
			</div>
			<div class="col-sm-6 m-t-15">
				<div class="card-box">
					<div class="repeaters">
						<a href="" class="btn btn-white btn-lg addcustom">Add Custom Class</a>
						@if(!empty($savedsettings['classid_name']) && sizeof($savedsettings['classid_name'])>0)
							@foreach($savedsettings['classid_name'] as $key=>$val)
								<div class="repeat row m-t-15">
									<div class="col-sm-5">
										<input type="text" name="classid_name[]" value="{{ $val }}" class="form-control" placeholder="Class/ID">
									</div>
									<div class="col-sm-3">
										<select class="form-control" name="type[]">
											<option value="color" {{ !empty($savedsettings['type'][$key]) && $savedsettings['type'][$key]=='color'?'selected':'' }}>Text Color</option>
											<option value="background-color" {{ !empty($savedsettings['type'][$key]) && $savedsettings['type'][$key]=='background-color'?'selected':'' }}>Background Color</option>
										</select>
									</div>
									<div class="col-sm-3">
										<div data-color-format="rgba" data-color="{{ !empty($savedsettings['classid_color'][$key])?$savedsettings['classid_color'][$key]:'rgba(0,0,0,0)' }}" class="colorpicker-rgba input-group">
											<input type="text" readonly="readonly" value="{{ !empty($savedsettings['classid_color'][$key])?$savedsettings['classid_color'][$key]:'rgba(0,0,0,0)' }}" name="classid_color[]" class="form-control" placeholder="{{ !empty($savedsettings['classid_color'][$key])?$savedsettings['classid_color'][$key]:'rgba(0,0,0,0)' }}">
											<span class="input-group-btn add-on">
												<button class="btn btn-white" type="button">
													<i style="background-color: {{ !empty($savedsettings['classid_color'][$key])?$savedsettings['classid_color'][$key]:'rgba(0,0,0,0)' }};margin-top: 2px;"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-sm-1">
										<button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button>
									</div>
								</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>
    </div>
</div>
{{csrf_field()}}
<input type="hidden" name="settingType" value="colors">
<div class="container-fluid">
	<button type="submit" class="btn btn-primary waves-effect waves-light btn-lg">Save All</button>
</div>
</form>
<script>
jQuery(document).ready(function(){
	jQuery('.repeaters').on('click', '.btn-repeat-add', function(){
		jQuery(this).parents('.repeat').clone().addClass('newrow').insertAfter(jQuery(this).parents('.repeat'));
	})
	jQuery('.repeaters').on('click', '.addcustom', function(){
		var html = '<div class="repeat row m-t-15"><div class="col-sm-5"><input type="text" name="classid_name[]" class="form-control" placeholder="Class/ID"></div><div class="col-sm-3"><select class="form-control" name="type[]"><option value="color">Text Color</option><option value="background-color">Background Color</option></select></div><div class="col-sm-3"><div data-color-format="rgba" data-color="rgba(0,0,0,0)" class="colorpicker-rgba input-group"><input type="text" readonly="readonly" value="rgba(0,0,0,0)" name="classid_color[]" class="form-control" placeholder="rgba(0,0,0,0)"><span class="input-group-btn add-on"><button class="btn btn-white" type="button"><i style="background-color: rgba(0,0,0,0);margin-top: 2px;"></i></button></span></div></div><div class="col-sm-1"><button type="button" class="btn btn-white waves-effect btn-repeat-remove" style="display:block">Del</button></div></div>';
		jQuery(this).parents('.repeaters').append(html);
		$('.colorpicker-rgba').colorpicker();
		return false;
	})
	jQuery('.repeaters').on('click', '.btn-repeat-remove', function(){
		jQuery(this).parents('.repeat').remove();
	})
})
jQuery(document).ready(function($) {
	$('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
	$('.colorpicker-rgba').colorpicker();
})
</script>
@endsection
