@extends('layouts.app')

@section('extra')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/codemirror.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/theme/ambiance.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/dialog/dialog.min.css" />
<style>
.panel-color .panel-title span{
	display: block;
    line-height: 1;
    font-size: 14px;
    text-transform: none;
    color: #5fbeaa;
}
.tab-content{
	padding:0 !important;
}
.panel-heading{
	display:none;
}
.panel-body{
	opacity: 0;
	-webkit-transition: all .2s ease-in-out;
  -moz-transition: all .2s ease-in-out;
  -o-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
}
</style>
@endsection

@section('content')

<div class="container-fluid">
<div class="row">
			<div class="col-sm-12">
				<button class="btn btn-default waves-effect waves-light btn-md pull-right" id="save">Update</button>
				<h4 class="page-title">Options <span>Custom Settings</h4>

			</div>
    </div>
	@if (\Session::has('success'))
		<div class="row">
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
		</div>
			@endif
<form method="post" id="header-form" action="{{ route("settings.savecustom") }}" enctype="multipart/form-data">
	{{csrf_field()}}
	<input type="hidden" name="settingType" value="custom">
	<div class="row">
		<div class="col-sm-12 m-t-15">
			<ul class="nav nav-tabs tabs">
				<li class="active tab">
					<a href="#tab1" data-toggle="tab" aria-expanded="false">
						<span class="visible-xs"><i class="fa fa-home"></i></span>
						<span class="hidden-xs">Css</span>
					</a>
				</li>
				<li class="tab">
					<a href="#tab2" data-toggle="tab" aria-expanded="true">
						<span class="visible-xs"><i class="fas fa-scroll"></i></span>
						<span class="hidden-xs">Script</span>
					</a>
				</li>
				<li class="tab">
					<a href="#tab3" data-toggle="tab" aria-expanded="false">
						<span class="visible-xs"><i class="fa fa-user"></i></span>
						<span class="hidden-xs">Html Top</span>
					</a>
				</li>
				<li class="tab">
					<a href="#tab4" data-toggle="tab" aria-expanded="false">
						<span class="visible-xs"><i class="fa fa-cog"></i></span>
						<span class="hidden-xs">Inside &lt;Head&gt; Elements</span>
					</a>
				</li>
				<li class="tab">
					<a href="#tab5" data-toggle="tab" aria-expanded="false">
						<span class="visible-xs"><i class="fa fa-cog"></i></span>
						<span class="hidden-xs">Footer Elements</span>
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">
					<div class="panel panel-color panel-inverse">
						<div class="panel-heading">
							<h3 class="panel-title">Custom Css</h3>
						</div>
						<div class="panel-body p-0 code-edit-wrap">
							<textarea id="code1" name="custom_code_header_css">{!! !empty($savedsettings['custom_code_header_css'])?$savedsettings['custom_code_header_css']:'' !!}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="tab2">
					<div class="panel panel-color panel-inverse">
						<div class="panel-heading">
							<h3 class="panel-title">Custom Script</h3>
						</div>
						<div class="panel-body p-0">
							<textarea id="code2" name="custom_code_header_js">{!! !empty($savedsettings['custom_code_header_js'])?$savedsettings['custom_code_header_js']:'' !!}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="tab3">
					<div class="panel panel-color panel-inverse">
						<div class="panel-heading">
							<h3 class="panel-title">Custom Html <span>Just after &lt;body&gt;</span></h3>
						</div>
						<div class="panel-body p-0">
							<textarea id="code4" name="custom_code_body">{!! !empty($savedsettings['custom_code_body'])?$savedsettings['custom_code_body']:'' !!}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="tab4">
					<div class="panel panel-color panel-inverse">
						<div class="panel-heading">
							<h3 class="panel-title">Custom Head Elements <span>Inside &lt;head&gt;&lt;/head&gt;</span></h3>
						</div>
						<div class="panel-body p-0">
							<textarea id="code3" name="custom_code_header_others">{!! !empty($savedsettings['custom_code_header_others'])?$savedsettings['custom_code_header_others']:'' !!}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="tab5">
					<div class="panel panel-color panel-inverse">
						<div class="panel-heading">
							<h3 class="panel-title">Custom Footer Elements</h3>
						</div>
						<div class="panel-body p-0">
							<textarea id="code5" name="custom_code_footer">{!! !empty($savedsettings['custom_code_footer'])?$savedsettings['custom_code_footer']:'' !!}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<button type="submit" id="saveall" class="btn btn-primary waves-effect waves-light btn-lg">Update</button>
</div>
</form>
<script>
	var resizefunc = [];
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/codemirror.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/mode/xml/xml.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/scroll/annotatescrollbar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/dialog/dialog.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/search/searchcursor.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/search/search.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/search/matchesonscrollbar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.55.0/addon/search/jump-to-line.min.js"></script>
<script>
!function($) {
    "use strict";

    var CodeEditor = function() {};

    CodeEditor.prototype.getSelectedRange = function(editor) {
        return { from: editor.getCursor(true), to: editor.getCursor(false) };
    },
    CodeEditor.prototype.autoFormatSelection = function(editor) {
        var range = this.getSelectedRange(editor);
        editor.autoFormatRange(range.from, range.to);
    },
    CodeEditor.prototype.commentSelection = function(isComment, editor) {
        var range = this.getSelectedRange(editor);
        editor.commentRange(isComment, range.from, range.to);
    },
    CodeEditor.prototype.init = function() {
        var $this = this;
        //init plugin
        CodeMirror.fromTextArea(document.getElementById("code1"), {
            mode: {name: "xml", alignCDATA: true},
            lineNumbers: true,
        });
        //example 2
        CodeMirror.fromTextArea(document.getElementById("code2"), {
            mode: {name: "xml", alignCDATA: true},
            lineNumbers: true
        });
		CodeMirror.fromTextArea(document.getElementById("code3"), {
            mode: {name: "xml", alignCDATA: true},
            lineNumbers: true
        });
		CodeMirror.fromTextArea(document.getElementById("code4"), {
            mode: {name: "xml", alignCDATA: true},
            lineNumbers: true
        });
		CodeMirror.fromTextArea(document.getElementById("code5"), {
            mode: {name: "xml", alignCDATA: true},
            lineNumbers: true
        });

        //CodeMirror.commands["selectAll"](editor);

        //binding controlls
        $('.autoformat').click(function(){
            $this.autoFormatSelection(editor);
        });

        $('.commentbtn').click(function(){
            $this.commentSelection(true, editor);
        });

        $('.uncomment').click(function(){
            $this.commentSelection(false, editor);
        });
    },
    //init
		$('.panel-body').css('opacity', 1);
    $.CodeEditor = new CodeEditor, $.CodeEditor.Constructor = CodeEditor
}(window.jQuery),

//initializing
function($) {
    "use strict";
    $.CodeEditor.init()
}(window.jQuery);
</script>
<script>
update_mirror = function() {
  var codeMirrorContainer = $sec_tab.find(".CodeMirror")[0];
  if (codeMirrorContainer && codeMirrorContainer.CodeMirror) {
    codeMirrorContainer.CodeMirror.refresh();
  }
}
$(document).ready(function(){
	$('#save').click(function(){
		$('#header-form').submit();
	})
	$('.tabs .tab a').on('click', function(){
		$('.panel-body').css('opacity', 0);
		var id = $(this).attr('href');
		setTimeout(function(){
			var codeMirrorContainer = $(id).find(".CodeMirror")[0];
			if (codeMirrorContainer && codeMirrorContainer.CodeMirror) {
			codeMirrorContainer.CodeMirror.refresh();
			$(id).find(".CodeMirror").parents('.panel-body').css('opacity', 1);
			}
		}, 100);
	})
})
</script>
@endsection
