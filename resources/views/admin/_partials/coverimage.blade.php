<script src="{{ asset('assets/private/js/dropzone.js') }}" type="text/javascript"></script>
<div class="md-custom-modal md-custom-effect-1 media-select" id="cover-select">
	<div class="md-custom-content">
		<h3>Media</h3>
		<ul class="nav nav-tabs navtab-bg nav-justified">
			<li><a data-toggle="tab" href="#upload2">Upload Files</a></li>
			<li><a data-toggle="tab" id="medialibrary2" href="#media2" class="active">Media Library</a></li>
		</ul>
		<div class="md-custom-body">
			<div class="tab-content">
				<div id="upload2" class="tab-pane fade">
					<div class="card-box">
						<div class="hidden">
							<div id="preview-template">
								<div class="dz-preview imageholder dz-file-preview">
									<div class="dz-details">
									<div class="thumbnail active"><img data-dz-thumbnail /></div>
									</div>
									<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
									<div class="dz-error-message"><span data-dz-errormessage></span></div>
								</div>
							</div>
						</div>
						<form action="" class="dropzone dropzone2">

						</form>
					</div>
				</div>
				<div id="media2" class="tab-pane fade in active show">
					<div class="allmedia">
					@foreach($uploads as $upload)
						<div class="imageholder">
							<div class="thumbnail"><a href="{{ getImage($upload->slug,'') }}"><img src="{{ getImage($upload->slug,'thumbnail') }}"></a></div>
						</div>
					@endforeach
					</div>
					<div class="row hidden" style="clear:both">
						<div class="col-sm-12 m-t-15">{{ $uploads->links() }}</div>
					</div>
				</div>
			</div>
		</div>
		<div class="md-custom-footer">
		<button class="md-custom-close">Close me!</button>
		</div>
	</div>
</div>
 <div  data-target="#cover-select" class="md-overlay md-custom-overlay"></div>

<script>

Dropzone.autoDiscover = false;
$(function(){
    uploader = new Dropzone(".dropzone2",{
        url: "{{ route('media.modalupload')}}",
        uploadMultiple :false,
        acceptedFiles : "image/*,video/*,audio/*",
        addRemoveLinks: false,
        forceFallback: false,
		thumbnailWidth: 190,
		thumbnailHeight: 190,
		init: function () {
			this.on('addedfile', function (file) {
			   $('#medialibrary2').trigger('click');
			   $('#media2 .allmedia').prepend($(file.previewElement));
			   $('.dropzone2').removeClass('dz-started');
			}),
			this.on("success", function(file, responseText) {
				$('#media2 .dz-success').addClass('recent');
				$('#media2 .dz-success .active').html(responseText);
				$('#media2 .dz-success .thumbnail').removeClass('active');
			}),
			this.on("error", function(file, responseText) {
				alert(responseText.errors.file);
				$('.dz-error').remove();
			})
		},
		previewTemplate: document.getElementById('preview-template').innerHTML,
        maxFilesize:1024,
        parallelUploads: 1,
		headers: {
			'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
		}
    });
});
@if(sizeof($uploads)==15)
$(document).ready(function(){
	$('#media2 .allmedia').scroll(function(){
		$('#media2 .allmedia').infiniteScroll({
			  path: '#media2 .pagination li a[rel="next"]',
			  append: '#media2 .allmedia .imageholder',
			  history: false,
			  prefill: true
			});
	})
})

@endif
$(window).bind("load", function() {
		$('#media2 .allmedia').css('height', ($('#cover-select').height()-183)+'px');
})
$(window).resize(function () {
		$('#media2 .allmedia').css('height', ($('#cover-select').height()-183)+'px');
})
</script>
