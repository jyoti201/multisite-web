<script src="{{ asset('assets/private/js/dropzone.js') }}" type="text/javascript"></script>
<div class="md-custom-modal md-custom-effect-1 media-select" id="media-select">
	<div class="md-custom-content">
			<h3>Media</h3>
			<ul class="nav nav-tabs navtab-bg nav-justified">
				<li><a data-toggle="tab" href="#upload">Upload Files</a></li>
				<li><a data-toggle="tab" id="medialibrary" href="#media" class="active">Media Library</a></li>
			</ul>
			<div class="md-custom-body">
							<div class="tab-content">
								<div id="upload" class="tab-pane fade">
									<div class="card-box">
										<div class="hidden">
											<div id="preview-template">
												<div class="dz-preview imageholder dz-file-preview">
													<div class="dz-details">
													<div class="thumbnail active"><img data-dz-thumbnail /></div>
													</div>
													<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
													<div class="dz-error-message"><span data-dz-errormessage></span></div>
												</div>
											</div>
										</div>
										<form action="" class="dropzone dropzone3">

										</form>
									</div>
								</div>
								<div id="media" class="tab-pane fade in active show">
									<div class="allmedia">
									@foreach($uploads as $upload)
										<div class="imageholder">
											<div class="thumbnail"><a href="{{ getImage($upload->slug,'') }}"><img src="{{ getImage($upload->slug,'thumbnail') }}"></a></div>
										</div>
									@endforeach
									</div>
									<div class="row hidden" style="clear:both">
										<div class="col-sm-12 m-t-15">{{ $uploads->links() }}</div>
									</div>
								</div>
							</div>
			</div>
			<div class="md-custom-footer">
			<button class="md-custom-close">Close me!</button>
			</div>
	</div>
</div>
 <div data-target="#media-select" class="md-overlay  md-custom-overlay"></div>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<script>

Dropzone.autoDiscover = false;
$(function(){
    uploader = new Dropzone(".dropzone3",{
        url: "{{ route('media.modalupload')}}",
        uploadMultiple :false,
        acceptedFiles : "image/*,video/*,audio/*",
        addRemoveLinks: false,
        forceFallback: false,
		thumbnailWidth: 190,
		thumbnailHeight: 190,
		init: function () {
			this.on('addedfile', function (file) {
			   $('#medialibrary').trigger('click');
			   $('#media .allmedia').prepend($(file.previewElement));
			   $('.dropzone3').removeClass('dz-started');
			}),
			this.on("success", function(file, responseText) {
				$('#media .dz-success').addClass('recent');
				$('#media .dz-success .active').html(responseText);
				$('#media .dz-success .thumbnail').removeClass('active');
			}),
			this.on("error", function(file, responseText) {
				alert(responseText.errors.file);
				$('.dz-error').remove();
			})
		},
		previewTemplate: document.getElementById('preview-template').innerHTML,
        maxFilesize:1024,
        parallelUploads: 1,
		headers: {
			'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
		}
    });
});
@if(sizeof($uploads)==15)
$(document).ready(function(){
	$('#media .allmedia').scroll(function(){
		$('#media .allmedia').infiniteScroll({
			  path: '#media .pagination li a[rel="next"]',
			  append: '#media .allmedia .imageholder',
			  history: false,
			  prefill: true
			});
	})
})

@endif
$(window).bind("load", function() {
		$('#media .allmedia').css('height', ($('#media-select').height()-183)+'px');
})
$(window).resize(function () {
		$('#media .allmedia').css('height', ($('#media-select').height()-183)+'px');
})
</script>
