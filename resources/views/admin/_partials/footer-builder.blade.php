<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<script src="{{ asset('assets/public/js/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="{{ asset('assets/public/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/public/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	@if(!empty(getSetting('imported_font_link','styles')))
		@foreach(getSetting('imported_font_link','styles') as $links)
			<link href="{!! $links !!}" rel="stylesheet">
		@endforeach
	@endif
	<link href="{{ asset('assets/public/css/box.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/public/css/widgets.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/public/css/tipuesearch.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/private/css/modal.css') }}" />
	<link href="{{ asset('builder/contentbuilder/contentbuilder.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('builder/contentbox/contentbox.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href="{{ asset('assets/private/css/dropzone.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('html/assets/theme-demo.css') }}" rel="stylesheet">

	<link href="{{ IncludeAsset('public/css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/private/css/builder.css') }}" rel="stylesheet">

<meta name="csrf-token" content="{{ csrf_token() }}">
<script>var csrf_field = '{{csrf_field()}}';</script>
<style>
  #lnkToolOpen{
	  display:none;
  }
	.sticky-wrapper {
    padding-left: 15px;
    padding-right: 15px;
}
.builder_controls ul>li>.dropdown-menu a {
    padding: 8px 15px;
    font-family: "Open Sans", sans-serif !important;
}

.builder_controls .nav>li a{
	background-color: transparent !important;
	padding:0;
	font-size: 15px !important;
	display: block;
	color: #333 !important;
			line-height: 1.5;
}
</style>
</head>
<body>
<div class="container-fluid" style="padding:0;">
    <div class="row no-gutters">
			<div id="builder_controls" class="builder_controls text-center">
				<div class="container-fluid">
					<ul class="nav justify-content-start">
						<li class="dropdown">
							<a href="javascript:void(0);" class="btn btn-primary" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-caret-left"></i></a>
							<ul class="dropdown-menu">
								<li><a href="{{ config('app.url', 'Laravel') }}">Dashboard</a></li>
								<li><a href="{{ route('settings.header') }}">Header</a></li>
								<li><a href="{{ route('menu.index') }}">Menu</a></li>
								<li><a href="{{ route('settings.styles') }}">Styles</a></li>
								<li><a href="{{ route('settings.footer') }}">Footer</a></li>
								<li><a href="{{ route('settings.custom') }}">Custom</a></li>
								<li><a href="{{URL::route('media.index')}}">Media</a></li>
								<li><a href="{{URL::route('pages.index')}}">Pages</a></li>
								<li><a href="{{URL::route('posts.index')}}">Posts</a></li>
			          <li><a href="{{URL::route('sections.index')}}">Sections</a></li>
								<li><a href="{{URL::route('widgets.index')}}">Widgets</a></li>
							</ul>
						</li>
						<li><a href="#" class="btn btn-primary" id="view-html"><i class="cb-icon-code"></i></a></li>

						<li><a href="#" class="btn btn-primary" id="add-section"><i class="cb-icon-plus"></i></a></li>

						<li style="float:right;"><a href="#" class="btn btn-primary" id="save-html" style="background-color: #f05050 !important;border: 1px solid #f05050 !important;"><i class="fa fa-save" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
	</div>
</div>
			@if (\Session::has('success'))
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				<script>
				$(document).ready(function(){
					setTimeout(function(){ $('.alert-success').fadeOut(); }, 2000);
				})
				</script>
			@endif
					<div id="is-wrapper" class="is-wrapper">
					@if(!empty($savedsettings['html']))
							{!! $savedsettings['html'] !!}
					@else
						<div class="is-section is-box is-section-auto is-dark-text is-bg-light">
								<div class="is-overlay">
									<div class="is-overlay-bg" style="display: none;"></div>
									<div class="is-overlay-color" style="display: none;"></div>
									<div class="is-overlay-content"></div>
								</div>
								<div class="is-boxes">
									<div class="is-box-centered is-opacity-80">
										<div class="is-container is-footer is-builder container-fluid" id="contentareapZ9SySj" style=""><div class="widget-cat-basic"><div class="row clearfix">
											<div class="col-md-3">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
											</div>
											<div class="col-md-3">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
											</div>
											<div class="col-md-3">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
											</div>
											<div class="col-md-3">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
											</div>
										</div></div></div>
									</div>
								</div>
							</div>
							@endif
			</div>
<div class="container-fluid">
	<div class="row m-t-15">
	<form id="savefooter" method="post" action="{{ route("settings.savefooter") }}" enctype="multipart/form-data">
	{{csrf_field()}}
	<input type="hidden" name="settingType" value="footer">
	<input type="hidden" id="html" name="html" value="">
	</form>
	<div class="imageselect">
	@include('admin._partials.popupuploader')
	</div>
	<div class="coverselect">
		@include('admin._partials.coverimage')
	</div>
	</div>
</div>
<script src="{{ asset('assets/private/js/classie.js') }}"></script>
<script src="{{ asset('assets/private/js/modalEffects.js') }}"></script>
<script>
  var polyfilter_scriptpath = '{{ asset("assets/private/js") }}/';
</script>
<script src="{{ asset('assets/private/js/cssParser.js') }}"></script>
<script src="{{ asset('builder/contentbuilder/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('builder/contentbuilder/contentbuilder.js') }}" type="text/javascript"></script>
<script src="{{ asset('builder/contentbuilder/saveimages.js') }}" type="text/javascript"></script>
<script src="{{ asset('builder/contentbox/builderbox.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/private/plugins/simplelightbox/simple-lightbox.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/public/js/box.js') }}" type="text/javascript"></script>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<script>
jQuery(document).ready(function($){
	$("#is-wrapper").contentbox({
		zoom: 0.85,
		snippetFile: '{{ route("pages.snippets") }}',
		contentHtmlStart: '<div class="is-container is-footer is-builder container-fluid"><div class="row"><div class="col-md-12">',
		contentHtmlEnd: '</div></div></div>',
		coverImageHandler: '{{ route("pages.savecover") }}',
		//largerImageHandler: 'saveimage-large.php', /* for uploading larger image */
		onRender: function () {
			$('a.is-lightbox').simpleLightbox({ closeText: '<i style="font-size:35px" class="icon ion-ios-close-empty"></i>', navText: ['<i class="icon ion-ios-arrow-left"></i>', '<i class="icon ion-ios-arrow-right"></i>'], disableScroll: false });
			$('.row').each(function(){
				if($(this).find('div').length>0){
					if($(this).attr('contenteditable')){
						$(this).removeAttr('contenteditable');
						$(this).find('div').attr('contenteditable', 'true');
					}
				}
			})
		},
		onImageBrowseClick: function(){
			$('#media-select').addClass('md-custom-show');
			$('#active-input').attr('value','txtImgUrl');

		},
		//imageselect: '{{ route("media.select") }}',
		iconselect: '{{ asset('builder/contentbox/ionicons/selecticon-dark.html') }}',
		snippetCustomCode: true,
		toolbar: 'top',
		snippetCategories: [
				[-1, "All"],
                [120, "Basic"],
				{!! getCustomSnippetCategories() !!}
                [118, "Article"],
                [101, "Headline"],
                [119, "Buttons"],
                [102, "Photos"],
                [103, "Profile"],
                [116, "Contact"],
                [104, "Products"],
                [105, "Features"],
                [106, "Process"],
                [107, "Pricing"],
                [108, "Skills"],
                [109, "Achievements"],
                [110, "Quotes"],
                [111, "Partners"],
                [112, "As Featured On"],
                [113, "Page Not Found"],
                [114, "Coming Soon"],
                [115, "Help, FAQ"],
                ]
	});
	$("#add-section").click(function () {
            $('#is-wrapper').data('contentbox').addSection(); //To add new section, call addSection().
            return false;
    });
	$("#view-html").click(function () {
		$('#is-wrapper').data('contentbox').viewHtml();
		return false;
	});
	$("#save-html").click(function () {
		$('img.ui-resizable').resizable( "destroy" );
		$(this).prop('disabled', true);
		$("#is-wrapper").saveimages({
			handler: '{{ route("pages.saveimage") }}', // or saveimage.php - for saving embedded base64 image to image file
			onComplete: function () {
				//Save Content
				var sHTML = $('#is-wrapper').data('contentbox').html();
				$('#html').val(sHTML);
				$('#savefooter').submit();
			}
		});
		$("#is-wrapper").data('saveimages').save();
		return false;
	});
	$('.imageselect').on('click','.imageholder a',function(){
		$('#lnkImageSettings').trigger('click');
		$('body #chkCrop').prop('checked', true);
		$('#'+$('#active-input').val()).val($(this).attr('href'));
		$('#media-select').removeClass('md-custom-show');
		return false;
	})
	$('.coverselect').on('click','.imageholder a',function(){
		parent.applyBoxImage($(this).attr('href'));
		$('#cover-select').removeClass('md-custom-show');
		return false;
	})
	$('body').on('click', '#btnImgOk', function(){
		if($('input#chkCrop').is(':checked')){
		jQuery("#divToolImg").stop(true, true).fadeOut(0);
		jQuery("#divToolImgSettings").stop(true, true).fadeOut(0);
		jQuery('.overlay-bg').css('width', '100%');
		jQuery('.overlay-bg').css('height', '100%');
		//jQuery('body').css('overflow', 'hidden'); // This makes unwanted zoom-in in iOS Safari
		jQuery("#divToolImgLoader").css('top', jQuery('#divToolImg').css('top'));
		jQuery("#divToolImgLoader").css('left', jQuery('#divToolImg').css('left'));
		jQuery("#divToolImgLoader").css('display', 'block');
		}
	})
	$('body').on('click', '.is-boxes img', function(e){
		$(this).resizable({
			aspectRatio: true,
		});
		$(this).attr('draggable', false);
		e.stopPropagation();
	})
	$(document).click(function(e) {
		$('img.ui-resizable').css('display','inline');
		$('img.ui-resizable').resizable( "destroy" );
		if(!$(e.target).hasClass('ui-draggable') && !$(e.target).parents().hasClass('ui-draggable')){
			$('.customTools').css('opacity', 0);
		}
	});



@if(sizeof($uploads)==15)

	$('#media-select .allmedia').scroll(function(){
		$('#media-select .allmedia').infiniteScroll({
		  path: '#media-select .pagination li a[rel="next"]',
		  append: '#media-select .allmedia .imageholder',
		  history: false,
		  prefill: true
		});
	})
	$('#cover-select .allmedia').scroll(function(){
		$('#cover-select .allmedia').infiniteScroll({
		  path: '#cover-select .pagination li a[rel="next"]',
		  append: '#cover-select .allmedia .imageholder',
		  history: false,
		  prefill: true
		});
	})

@endif

})
</script>
<script src="{{ asset('builder/builderscript.js') }}" type="text/javascript"></script>

@include('admin._partials.buildercustomfeatures')
	<!-- Jquery easing -->
<script type="text/javascript" src="{{ IncludeAsset('public/js/jquery.easing.1.3.min.js') }}"></script>
<!--sticky header-->
<script type="text/javascript" src="{{ IncludeAsset('public/js/jquery.sticky.js') }}"></script>

<script>
  $(document).ready(function(){
    $("#sticky-nav-sticky-wrapper").sticky({bottomSpacing:200});
  });
</script>
</body>
</html>
