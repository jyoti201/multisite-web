<div id="customTools" class="customTools">
		<button id="widgetselect" title="Add Module"><i class="cb-icon-plus"></i></button>
		<button id="widgetselectup" title="Move Up">&uarr;</button>
		<button id="widgetselectdown" title="Move Down">&darr;</button>
		<button id="rowsetting" title="Row Setting"><i class="cb-icon-cog" aria-hidden="true"></i></button>
	</div>
	<div id="spacerTools" class="customTools">
			<button id="spacer-minus" data-min="20" data-max="500" title="Minus"><i class="cb-icon-minus"></i></button>
			<button id="spacer-plus" data-min="20" data-max="500" title="Plus"><i class="cb-icon-plus"></i></button>
	</div>
	<div id="otherTool" class="customTools">
		<button id="styleselectanimate" title="Column Setting"><i class="cb-icon-cog" aria-hidden="true"></i></button>
		<button id="colhtml" title="Column Source"><i class="cb-icon-code"></i></button>
		<button id="colmoveleft" title="Move Left">&larr;</button>
		<button id="colmoveright" title="Move Right">&rarr;</button>
		<button id="colclone" title="Clone"><i class="cb-icon-docs"></i></button>
		<button id="colremove" title="Delete"><i class="cb-icon-cancel"></i></button>
	</div>
	<div class="md-modal" id="md-delcolconfirm" style="max-width: 550px; z-index: 10002;">
		<div class="md-content">
			<div class="md-body">
				<div style="padding:20px 20px 25px;text-align:center;"><p>Are you sure you want to delete this column?</p>
					<button id="colremoveCancel" class="cl-button"> CANCEL </button>
					<button id="colremoveOk" class="cl-button" style="margin-left:12px"> OK </button>
				</div>
			</div>
		</div>
	</div>
	<div class="md-modal  md-custom-modal" id="md-widgetselect" style="max-width: 1422px; z-index: 10002;">
		<div class="md-content">
			<div class="selectcat">

			</div>
			<div class="md-body">

			</div>
			<div class="md-footer" style="position:absolute;width:100%;height:55px;left:0;bottom:0;">
				<button id="widgetselectCancel"> Cancel </button>
			</div>
		</div>
	</div>
	 <div data-target="#md-widgetselect" class="md-overlay  md-custom-overlay"></div>

	<div class="md-modal md-custom-modal md-draggable" id="md-styleselect" style="max-width: 700px; z-index: 10002;">
		<div class="md-content">
			<div class="md-body">
				<div class="md-modal-handle" style="cursor: move;">
						<div class="md-title colsettingonly" style="margin:0 0 12px;">Column Setting</div>
						<div class="md-title rowsettingonly" style="margin:0 0 12px;">Row Setting</div>
				</div>
				<div class="animationList">
					<div class="is-tab-links" style="text-align:center;">
						<a id="col-editbox-1" class="cl-tab active" data-tab-content="col-content-editbox-1" href="#"><span class="colsettingonly">Size & Spacing</span><span class="rowsettingonly">Gutter & Spacing</span></a>
						<a id="col-editbox-2" class="cl-tab" data-tab-content="col-content-editbox-2" href="#">Box Shadow</a>
						<a id="col-editbox-3" class="cl-tab" data-tab-content="col-content-editbox-3" href="#">Border</a>
						<a id="col-editbox-4" class="cl-tab" data-tab-content="col-content-editbox-4" href="#">Background</a>
						<a id="col-editbox-5" class="cl-tab" data-tab-content="col-content-editbox-5" href="#">Advance</a>
					</div>
					<div class="is-tab-contents" style="margin-top:30px;padding:0 30px 30px">

							<div id="col-content-editbox-1" class="clearfix" style="display: table;">

									<div class="is-boxes colsettingonly">
										<div class="is-box-6">
											<div class="is-boxes">
												<div class="is-box-3">Size(ALL):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="colsize_all" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="colsize_all" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="colsize_all" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-input="colsize_all" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Size(SM):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="colsize_sm" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="colsize_sm" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="colsize_sm" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-input="colsize_sm" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Size(MD):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val"  data-max="12" data-min="0" data-input="colsize_md" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="colsize_md" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="colsize_md" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val"  data-max="12" data-min="0" data-input="colsize_md" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Size(LG):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0"  data-input="colsize_lg" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="colsize_lg" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="colsize_lg" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val"  data-input="colsize_lg" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Size(XL):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0"  data-input="colsize_xl" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="colsize_xl" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="colsize_xl" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-input="colsize_xl" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
										</div>
										<div class="is-box-6">
											<div class="is-boxes">
												<div class="is-box-3">Off(ALL):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val"  data-max="12" data-min="0" data-input="coloffset_all" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="coloffset_all" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="coloffset_all" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-input="coloffset_all" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Off(SM):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val"  data-max="12" data-min="0" data-input="coloffset_sm" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="coloffset_sm" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="coloffset_sm" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-input="coloffset_sm" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Off(MD):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val"  data-max="12" data-min="0" data-input="coloffset_md" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="coloffset_md" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="coloffset_md" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-input="coloffset_md" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Off(LG):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0"  data-input="coloffset_lg" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="coloffset_lg" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="coloffset_lg" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-input="coloffset_lg" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Off(XL):</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0"  data-input="coloffset_xl" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-max="12" data-min="0" data-input="coloffset_xl" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" id="coloffset_xl" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-val" data-input="coloffset_xl" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
										</div>
									</div>


									<div class="is-boxes rowsettingonly">
											<div class="is-box-12">
												<div class="is-boxes">
													<div class="is-box-3">Gutter:</div>
													<div class="is-box-2">
														<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="gutter" data-dir="l" data-value="-">-</button>
													</div>
													<div class="is-box-2">
														<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="gutter" data-dir="l" data-value="+">+</button>
													</div>
													<div class="is-box-3" style="vertical-align: middle">
														<input type="text" data-parameter="gutter-l-" style="height: 50px;" readonly>
													</div>
													<div class="is-box-2">
														<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="gutter" data-dir="l" data-value=""><i class="cb-icon-cancel"></i></button>
													</div>
												</div>
											</div>
									</div>

									<div class="is-boxes rowsettingonly">
											<div class="is-box-12">
												<div class="sublabel">Items Vertical Align</div>
													<div class="is-boxes">
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="itemsvalign" value="none" id="itemsvalign-none" checked="">
																			<label for="itemsvalign-none">None</label>
																	</div>
																</div>
																<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="itemsvalign" value="align-items-start" id="itemsvalign-start">
																				<label for="itemsvalign-start">Top</label>
																		</div>
																</div>
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="itemsvalign" value="align-items-end" id="itemsvalign-end">
																			<label for="itemsvalign-end">Bottom</label>
																	</div>
																</div>
													</div>
													<div class="is-boxes">
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="itemsvalign" value="align-items-center" id="itemsvalign-center">
																			<label for="itemsvalign-center">Center</label>
																	</div>
																</div>
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="itemsvalign" value="align-items-baseline" id="itemsvalign-baseline">
																			<label for="itemsvalign-baseline">Baseline</label>
																	</div>
																</div>
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="itemsvalign" value="align-items-stretch" id="itemsvalign-stretch">
																			<label for="itemsvalign-stretch">Stretch</label>
																	</div>
																</div>
												</div>
											</div>
										</div>

										<div class="is-boxes rowsettingonly">
												<div class="is-box-12">
													<div class="sublabel">Items Wrap</div>
														<div class="is-boxes">
																	<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="itemwrap" value="none" id="itemwrap-none" checked="">
																				<label for="itemwrap-none">Default</label>
																		</div>
																	</div>
																	<div class="is-box-4">
																			<div class="switch">
																					<input type="radio" name="itemwrap" value="flex-nowrap" id="itemwrap-nowrap">
																					<label for="itemwrap-nowrap">No Wrap</label>
																			</div>
																	</div>
																	<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="itemwrap" value="flex-wrap" id="itemwrap-wrap">
																				<label for="itemwrap-wrap">Wrap</label>
																		</div>
																	</div>

														</div>
														<div class="is-boxes">
															<div class="is-box-4">
																<div class="switch">
																		<input type="radio" name="itemwrap" value="flex-wrap-reverse" id="itemwrap-wrap-reverse">
																		<label for="itemwrap-wrap-reverse">Wrap Reverse</label>
																</div>
															</div>
														</div>
												</div>
											</div>

											<div class="is-boxes rowsettingonly">
													<div class="is-box-12">
														<div class="sublabel">Items Horizontal Align</div>
															<div class="is-boxes">
																		<div class="is-box-3">
																			<div class="switch">
																					<input type="radio" name="itemshalign" value="none" id="itemshalign-none" checked="">
																					<label for="itemshalign-none">None</label>
																			</div>
																		</div>
																		<div class="is-box-3">
																				<div class="switch">
																						<input type="radio" name="itemshalign" value="justify-content-start" id="itemshalign-content-start">
																						<label for="itemshalign-content-start">Start</label>
																				</div>
																		</div>
																		<div class="is-box-3">
																				<div class="switch">
																						<input type="radio" name="itemshalign" value="justify-content-center" id="itemshalign-content-center">
																						<label for="itemshalign-content-center">Center</label>
																				</div>
																		</div>
															</div>
															<div class="is-boxes">
																		<div class="is-box-3">
																			<div class="switch">
																					<input type="radio" name="itemshalign" value="justify-content-end" id="itemshalign-content-end">
																					<label for="itemshalign-content-end">End</label>
																			</div>
																		</div>
																		<div class="is-box-3">
																			<div class="switch">
																					<input type="radio" name="itemshalign" value="justify-content-around" id="itemshalign-content-around">
																					<label for="itemshalign-content-around">Around</label>
																			</div>
																		</div>
																		<div class="is-box-3">
																			<div class="switch">
																					<input type="radio" name="itemshalign" value="justify-content-between" id="itemshalign-content-between">
																					<label for="itemshalign-content-between">Between</label>
																			</div>
																		</div>
															</div>
													</div>
												</div>

									<div class="is-boxes colsettingonly">
											<div class="is-box-12">
												<div class="sublabel">Vertical Align</div>
													<div class="is-boxes">
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="colvalign" value="none" id="colvalign-none" checked="">
																			<label for="colvalign-none">None</label>
																	</div>
																</div>
																<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="colvalign" value="align-self-start" id="colvalign-start">
																				<label for="colvalign-start">Top</label>
																		</div>
																</div>
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="colvalign" value="align-self-end" id="colvalign-end">
																			<label for="colvalign-end">Bottom</label>
																	</div>
																</div>
													</div>
													<div class="is-boxes">
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="colvalign" value="align-self-center" id="colvalign-center">
																			<label for="colvalign-center">Center</label>
																	</div>
																</div>
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="colvalign" value="align-self-baseline" id="colvalign-baseline">
																			<label for="colvalign-baseline">Baseline</label>
																	</div>
																</div>
																<div class="is-box-4">
																	<div class="switch">
																			<input type="radio" name="colvalign" value="align-self-stretch" id="colvalign-stretch">
																			<label for="colvalign-stretch">Stretch</label>
																	</div>
																</div>
												</div>
											</div>
										</div>




										<div class="is-boxes colsettingonly">
												<div class="is-box-12">
													<div class="sublabel">Horizontal Align</div>
														<div class="is-boxes">
																	<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="colhalign" value="none" id="colhalign-none" checked="">
																				<label for="colhalign-none">None</label>
																		</div>
																	</div>
																	<div class="is-box-4">
																			<div class="switch">
																					<input type="radio" name="colhalign" value="mr-auto" id="colhalign-left">
																					<label for="colhalign-left">Left</label>
																			</div>
																	</div>
																	<div class="is-box-4">
																			<div class="switch">
																					<input type="radio" name="colhalign" value="mx-auto" id="colhalign-center">
																					<label for="colhalign-center">Center</label>
																			</div>
																	</div>
																	<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="colhalign" value="ml-auto" id="colhalign-right">
																				<label for="colhalign-right">Right</label>
																		</div>
																	</div>
														</div>
												</div>
											</div>



									<div class="is-boxes pading-margin" style="margin-bottom: 10px;margin-top:20px;">
										<div class="is-box-6">
											<div class="sublabel">Padding</div>
											<div class="is-boxes">
												<div class="is-box-3">Left:</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="l" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="l" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" data-parameter="padding-l-" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="l" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Right:</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="r" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="r" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" data-parameter="padding-r-" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="r" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Top:</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="t" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="t" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" data-parameter="padding-t-" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="t" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Bottom:</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="b" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="b" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" data-parameter="padding-b-" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="padding" data-min="0" data-max="50" data-dir="b" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
										</div>
										<div class="is-box-6">
											<div class="sublabel">Margin</div>
											<div class="is-boxes">
												<div class="is-box-3">Left:</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="margin" data-min="-50" data-max="50" data-dir="l" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="margin"  data-min="-50" data-max="50" data-dir="l" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" data-parameter="margin-l-" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-parameter="margin"  data-min="-50" data-max="50" data-dir="l" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Right:</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-min="-50" data-max="50" data-parameter="margin" data-dir="r" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css" data-min="-50" data-max="50" data-parameter="margin" data-dir="r" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" data-parameter="margin-r-" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css"  data-min="-50" data-max="50" data-parameter="margin" data-dir="r" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Top:</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css"  data-min="-50" data-max="50" data-parameter="margin" data-dir="t" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css"  data-min="-50" data-max="50" data-parameter="margin" data-dir="t" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" data-parameter="margin-t-" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css"  data-min="-50" data-max="50" data-parameter="margin" data-dir="t" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-3">Bottom:</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css"  data-min="-50" data-max="50" data-parameter="margin" data-dir="b" data-value="-">-</button>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css"  data-min="-50" data-max="50" data-parameter="margin" data-dir="b" data-value="+">+</button>
												</div>
												<div class="is-box-3" style="vertical-align: middle">
													<input type="text" data-parameter="margin-b-" style="height: 50px;" readonly>
												</div>
												<div class="is-box-2">
													<button class="cl-button cl-button-full custom-left-right-css"  data-min="-50" data-max="50" data-parameter="margin" data-dir="b" data-value=""><i class="cb-icon-cancel"></i></button>
												</div>
											</div>
										</div>
									</div>
					</div>




					<div id="col-content-editbox-2" class="shadowmaker clearfix">
											<div class="is-boxes">
												<div class="is-box-12">
													<div class="sublabel">Box Shadow</div>
													<div class="is-boxes">
													<div class="is-box-4">
														<div class="switch">
																<input type="radio" name="colshadow" value="none" id="col-shadow-none" checked>
																<label for="col-shadow-none">None</label>
														</div>
													</div>
													<div class="is-box-4">
															<div class="switch">
																	<input type="radio" name="colshadow" value="" id="col-shadow-out">
																	<label for="col-shadow-out">Outset</label>
															</div>
													</div>
													<div class="is-box-4">
														<div class="switch">
																<input type="radio" name="colshadow" value="inset" id="col-shadow-in">
																<label for="col-shadow-in">Inset</label>
														</div>
													</div>
												</div>
												</div>
											</div>
											<div class="is-boxes">
												<div class="is-box-12">





<div class="is-boxes" style="margin-top:5px;">
			<div class="is-box-3">Horizontal Offset</div>
			<div class="is-box-7"><input type="range" id="col-shadow-hoffset" name="col-shadow-hoffset" min="-80" max="80" value="0"></div>
			<div class="is-box-2" style="text-align:center"><input type="number" id="col-shadow-hoffset-count" value="0" min="-80" max="80" style="height: 40px;padding: 10px;width: 55px;display: inline-block;margin-right: 5px;">px</div>
</div>
<div class="is-boxes" style="margin-top:5px;">
			<div class="is-box-3">Vertical Offset</div>
			<div class="is-box-7"><input type="range" id="col-shadow-voffset" name="col-shadow-voffset" min="-80" max="80" value="0"></div>
			<div class="is-box-2" style="text-align:center"><input type="number" id="col-shadow-voffset-count" min="-80" max="80" value="0" style="height: 40px;padding: 10px;width: 55px;display: inline-block;margin-right: 5px;">px</div>
</div>
<div class="is-boxes" style="margin-top:5px;">
			<div class="is-box-3">Blur</div>
			<div class="is-box-7"><input type="range" id="col-shadow-blur" name="col-shadow-blur" min="0" max="80" value="5"></div>
			<div class="is-box-2" style="text-align:center"><input type="number" id="col-shadow-blur-count" value="" style="height: 40px;padding: 10px;width: 55px;display: inline-block;margin-right: 5px;">px</div>
</div>
<div class="is-boxes" style="margin-top:5px;">
			<div class="is-box-3">Distance</div>
			<div class="is-box-7"><input type="range" id="col-shadow-distance" name="col-shadow-distance" min="0" max="80" value="5"></div>
			<div class="is-box-2" style="text-align:center"><input type="number" id="col-shadow-distance-count" value="" style="height: 40px;padding: 10px;width: 55px;display: inline-block;margin-right: 5px;">px</div>
</div>
<div class="is-boxes" style="margin-top:5px;">
		<div class="is-box-3">Color</div>
		<div class="is-box-9"><input type="text" name="col-shadow-color" id="col-shadow-color" value="#000000" style="height: 40px;padding: 10px;width: 145px;display: inline-block;margin-right: 5px;"></div>
</div>
<div class="is-boxes" style="margin-top:5px;">
		<div class="is-box-3">Opacity</div>
		<div class="is-box-7"><input type="range" id="col-shadow-opacity" name="col-shadow-opacity" min="0" max="100" value="75"></div>
		<div class="is-box-2" style="text-align:center"><input type="number" id="col-shadow-opacity-count" value="75" min="0" max="100" style="height: 40px;padding: 10px;width: 55px;display: inline-block;margin-right: 5px;">%</div>
</div>






												</div>
											</div>
								</div>


						<div id="col-content-editbox-3" class="clearfix">
									<div class="is-boxes">
												<div class="is-box-12">
													<div class="sublabel">Border Type</div>
														<div class="is-boxes">
																	<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="colborder" value="none" id="col-border-none" checked>
																				<label for="col-border-none">None</label>
																		</div>
																	</div>
																	<div class="is-box-4">
																			<div class="switch">
																					<input type="radio" name="colborder" value="solid" id="col-border-solid">
																					<label for="col-border-solid">Solid</label>
																			</div>
																	</div>
																	<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="colborder" value="dashed" id="col-border-dashed">
																				<label for="col-border-dashed">Dashed</label>
																		</div>
																	</div>
														</div>
														<div class="is-boxes">
																	<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="colborder" value="dotted" id="col-border-dotted">
																				<label for="col-border-dotted">Dotted</label>
																		</div>
																	</div>
																	<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="colborder" value="double" id="col-border-double">
																				<label for="col-border-double">Double</label>
																		</div>
																	</div>
																	<div class="is-box-4">
																		<div class="switch">
																				<input type="radio" name="colborder" value="groove" id="col-border-groove">
																				<label for="col-border-groove">Groove</label>
																		</div>
																	</div>
													</div>
												</div>
											</div>

										<div class="is-boxes" style="margin-top:20px;">
											<div class="is-box-12">
												<div class="sublabel">Border Width</div>
													<div class="is-boxes">
														<div class="is-box-2">Left:</div>
														<div class="is-box-3">
															<button class="cl-button cl-button-full custom-left-right-css"  data-min="0" data-max="50" data-parameter="border" data-dir="l" data-value="-">-</button>
														</div>
														<div class="is-box-3">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="l" data-value="+">+</button>
														</div>
														<div class="is-box-2" style="vertical-align: middle">
															<input type="text" data-parameter="border-l-" style="height: 50px;" readonly>
														</div>
														<div class="is-box-2">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="l" data-value=""><i class="cb-icon-cancel"></i></button>
														</div>
													</div>
													<div class="is-boxes">
														<div class="is-box-2">Right:</div>
														<div class="is-box-3">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="r" data-value="-">-</button>
														</div>
														<div class="is-box-3">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="r" data-value="+">+</button>
														</div>
														<div class="is-box-2" style="vertical-align: middle">
															<input type="text" data-parameter="border-r-" style="height: 50px;" readonly>
														</div>
														<div class="is-box-2">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="r" data-value=""><i class="cb-icon-cancel"></i></button>
														</div>
													</div>
													<div class="is-boxes">
														<div class="is-box-2">Top:</div>
														<div class="is-box-3">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="t" data-value="-">-</button>
														</div>
														<div class="is-box-3">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="t" data-value="+">+</button>
														</div>
														<div class="is-box-2" style="vertical-align: middle">
															<input type="text" data-parameter="border-t-" style="height: 50px;" readonly>
														</div>
														<div class="is-box-2">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="t" data-value=""><i class="cb-icon-cancel"></i></button>
														</div>
													</div>
													<div class="is-boxes">
														<div class="is-box-2">Bottom:</div>
														<div class="is-box-3">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="b" data-value="-">-</button>
														</div>
														<div class="is-box-3">
															<button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="50" data-parameter="border" data-dir="b" data-value="+">+</button>
														</div>
														<div class="is-box-2" style="vertical-align: middle">
															<input type="text" data-parameter="border-b-" style="height: 50px;" readonly>
														</div>
														<div class="is-box-2">
															<button class="cl-button cl-button-full custom-left-right-css"  data-min="0" data-max="50" data-parameter="border" data-dir="b" data-value=""><i class="cb-icon-cancel"></i></button>
														</div>
													</div>
												</div>
								</div>
								<div class="is-boxes" style="margin-top:15px;">
										<div class="is-box-12">
												<div class="is-boxes">
													<div class="is-box-3">Border Color</div>
													<div class="is-box-9">
														<input type="text" name="col-border-color" id="col-border-color" value="" style="height: 40px; padding: 10px;">
													</div>
												</div>
										</div>
								</div>


								<div class="is-boxes" style="margin-top:20px;">
								  <div class="is-box-12">
								    <div class="sublabel">Border Radius</div>
								      <div class="is-boxes">
								        <div class="is-box-2">Top Left:</div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="tlpx" data-value="-">-</button>
								        </div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="tlpx" data-value="+">+</button>
								        </div>
												<div class="is-box-2" style="vertical-align: top;">
														<div class="switch" style="margin-bottom: 3px;">
																<input type="radio" name="colradiuspara_tl" value="px" id="colcolradiuspara_tl_px" checked>
																<label for="colcolradiuspara_tl_px" class="change_data_parameter" data-new="border-radius-tlpx-" data-old="border-radius-tlper-" style="height: 23px;line-height: 23px;">PX</label>
														</div>
														<div class="switch">
																<input type="radio" name="colradiuspara_tl" value="per" id="colcolradiuspara_tl_per">
																<label for="colcolradiuspara_tl_per" class="change_data_parameter" data-new="border-radius-tlper-" data-old="border-radius-tlpx-" style="height: 23px;line-height: 23px;">%</label>
														</div>
												</div>
								        <div class="is-box-2" style="vertical-align: middle">
								          <input type="text" data-parameter="border-radius-tlpx-" style="height: 50px;" readonly>
								        </div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css"  data-min="0" data-max="100" data-parameter="border-radius" data-dir="tlpx" data-value=""><i class="cb-icon-cancel"></i></button>
								        </div>
								      </div>
								      <div class="is-boxes">
								        <div class="is-box-2">Top Right:</div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="trpx" data-value="-">-</button>
								        </div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="trpx" data-value="+">+</button>
								        </div>
												<div class="is-box-2" style="vertical-align: top;">
														<div class="switch" style="margin-bottom: 3px;">
																<input type="radio" name="colradiuspara_tr" value="px" id="colcolradiuspara_tr_px" checked>
																<label for="colcolradiuspara_tr_px" class="change_data_parameter" data-new="border-radius-trpx-" data-old="border-radius-trper-" style="height: 23px;line-height: 23px;">PX</label>
														</div>
														<div class="switch">
																<input type="radio" name="colradiuspara_tr" value="per" id="colcolradiuspara_tr_per">
																<label for="colcolradiuspara_tr_per" class="change_data_parameter" data-new="border-radius-trper-" data-old="border-radius-trpx-" style="height: 23px;line-height: 23px;">%</label>
														</div>
												</div>
								        <div class="is-box-2" style="vertical-align: middle">
								          <input type="text" data-parameter="border-radius-trpx-" style="height: 50px;" readonly>
								        </div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="trpx" data-value=""><i class="cb-icon-cancel"></i></button>
								        </div>
								      </div>
								      <div class="is-boxes">
								        <div class="is-box-2">Bottom Left:</div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="blpx" data-value="-">-</button>
								        </div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="blpx" data-value="+">+</button>
								        </div>
												<div class="is-box-2" style="vertical-align: top;">
														<div class="switch" style="margin-bottom: 3px;">
																<input type="radio" name="colradiuspara_bl" value="px" id="colcolradiuspara_bl_px" checked>
																<label for="colcolradiuspara_bl_px" class="change_data_parameter" data-new="border-radius-blpx-" data-old="border-radius-blper-" style="height: 23px;line-height: 23px;">PX</label>
														</div>
														<div class="switch">
																<input type="radio" name="colradiuspara_bl" value="per" id="colcolradiuspara_bl_per">
																<label for="colcolradiuspara_bl_per" class="change_data_parameter" data-new="border-radius-blper-" data-old="border-radius-blpx-" style="height: 23px;line-height: 23px;">%</label>
														</div>
												</div>
								        <div class="is-box-2" style="vertical-align: middle">
								          <input type="text" data-parameter="border-radius-blpx-" style="height: 50px;" readonly>
								        </div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="blpx" data-value=""><i class="cb-icon-cancel"></i></button>
								        </div>
								      </div>
								      <div class="is-boxes">
								        <div class="is-box-2">Bottom Right:</div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="brpx" data-value="-">-</button>
								        </div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="brpx" data-value="+">+</button>
								        </div>
												<div class="is-box-2" style="vertical-align: top;">
														<div class="switch" style="margin-bottom: 3px;">
																<input type="radio" name="colradiuspara_br" value="px" id="colcolradiuspara_br_px" checked>
																<label for="colcolradiuspara_br_px" class="change_data_parameter" data-new="border-radius-brpx-" data-old="border-radius-brper-" style="height: 23px;line-height: 23px;">PX</label>
														</div>
														<div class="switch">
																<input type="radio" name="colradiuspara_br" value="per" id="colcolradiuspara_br_per">
																<label for="colcolradiuspara_br_per" class="change_data_parameter" data-new="border-radius-brper-" data-old="border-radius-brpx-" style="height: 23px;line-height: 23px;">%</label>
														</div>
												</div>
								        <div class="is-box-2" style="vertical-align: middle">
								          <input type="text" data-parameter="border-radius-brpx-" style="height: 50px;" readonly>
								        </div>
								        <div class="is-box-2">
								          <button class="cl-button cl-button-full custom-left-right-css" data-min="0" data-max="100" data-parameter="border-radius" data-dir="brpx" data-value=""><i class="cb-icon-cancel"></i></button>
								        </div>
								      </div>
								    </div>
								</div>


					</div>

<div id="col-content-editbox-4" class="clearfix">
	<div class="is-boxes">
		<div class="is-box-12">
			<div class="is-boxes">
				<div class="is-box-3">Background Color (From)</div>
				<div class="is-box-9">
					<input type="text" name="col-bg-color-from" id="col-bg-color-from" value="" style="height: 40px; padding: 10px;">
				</div>
			</div>
		</div>
		<div class="is-box-12" style="margin-top: 10px;">
			<div class="is-boxes">
				<div class="is-box-3">Background Color (To)</div>
				<div class="is-box-9">
					<input type="text" name="col-bg-color-to" id="col-bg-color-to" value="" style="height: 40px; padding: 10px;">
				</div>
			</div>
		</div>
		<div class="is-box-12" style="margin-top: 10px;">
			<div class="is-boxes">
				<div class="is-box-3">Gradient Angle</div>
				<div class="is-box-9">
					<input type="number" name="col-gradient-angle" id="col-gradient-angle" min="0" value="0" style="height: 40px;width:100px; padding: 10px;">
				</div>
			</div>
		</div>
		<div class="is-box-12" style="margin-top: 10px;">
			<div class="is-boxes">
				<div class="is-box-3">&nbsp;</div>
				<div class="is-box-9">
					<button class="cl-button" id="clearbgcolor">Clear</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="col-content-editbox-5" class="clearfix">
					<div class="is-boxes">
						<div class="is-box-12">
							<div class="is-boxes">
								<div class="is-box-3">ID</div>
								<div class="is-box-9">
									<input type="text" name="colid" id="colid" value="" style="height: 40px; padding: 10px;">
								</div>
							</div>
						</div>
					</div>
					<div class="is-boxes" style="margin-top:20px;">
						<div class="is-box-12">
							<div class="is-boxes">
								<div class="is-box-3">Classes</div>
								<div class="is-box-9">
									<input type="text" name="colclasses" id="colclasses" value="" style="height: 40px; padding: 10px;">
								</div>
							</div>
						</div>
					</div>
					<div class="is-boxes" style="margin-top:20px;">
						<div class="is-box-6">
							<div class="sublabel">Visible</div>
							<div class="is-boxes">
							<div class="is-box-3">
									<div class="switch">
											<input type="checkbox" name="colvh[]" value="d-block d-sm-none" id="col-visible-xs">
											<label for="col-visible-xs">ExS</label>
									</div>
							</div>
							<div class="is-box-3">
								<div class="switch">
										<input type="checkbox" name="colvh[]" value="d-none d-sm-block d-md-none" id="col-visible-sm">
										<label for="col-visible-sm">S</label>
								</div>
							</div>
							<div class="is-box-3">
								<div class="switch">
										<input type="checkbox" name="colvh[]" value="d-none d-md-block d-lg-none" id="col-visible-md">
										<label for="col-visible-md">M</label>
								</div>
							</div>
							<div class="is-box-3">
								<div class="switch">
										<input type="checkbox" name="colvh[]" value="d-none d-lg-block d-xl-none" id="col-visible-lg">
										<label for="col-visible-lg">L</label>
								</div>
							</div>
							<div class="is-box-3">
								<div class="switch">
										<input type="checkbox" name="colvh[]" value="d-none d-xl-block" id="col-visible-xl">
										<label for="col-visible-xl">XL</label>
								</div>
							</div>
						</div>
						</div>
						<div class="is-box-6">
							<div class="sublabel">Hidden</div>
							<div class="is-boxes">
							<div class="is-box-3">
									<div class="switch">
											<input type="checkbox" name="colvh[]" value="d-none d-sm-block" id="col-hidden-xs">
											<label for="col-hidden-xs">ExS</label>
									</div>
							</div>
							<div class="is-box-3">
								<div class="switch">
										<input type="checkbox" name="colvh[]" value="d-sm-none d-md-block" id="col-hidden-sm">
										<label for="col-hidden-sm">S</label>
								</div>
							</div>
							<div class="is-box-3">
								<div class="switch">
										<input type="checkbox" name="colvh[]" value="d-md-none d-lg-block" id="col-hidden-md">
										<label for="col-hidden-md">M</label>
								</div>
							</div>
							<div class="is-box-3">
								<div class="switch">
										<input type="checkbox" name="colvh[]" value="d-lg-none d-xl-block" id="col-hidden-lg">
										<label for="col-hidden-lg">L</label>
								</div>
							</div>
							<div class="is-box-3">
								<div class="switch">
										<input type="checkbox" name="colvh[]" value="d-xl-none" id="col-hidden-xl">
										<label for="col-hidden-xl">XL</label>
								</div>
							</div>
						</div>
						</div>
					</div>

					<div class="is-boxes" style="margin-top: 20px">
							<div class="is-box-12">
								<div class="sublabel">Animation		<button id="styleselectanimate-remove" class="cl-button">Not Set</button></div>
							</div>
					</div>
					<div class="is-boxes">
						<div class="is-box-6">
							<ul>
								<li><label for="animate-fade"><input type="radio" name="animateName" value="fade" id="animate-fade"> fade</label></li>
								<li><label for="animate-fade-up"><input type="radio" name="animateName" value="fade-up" id="animate-fade-up"> fade-up</label></li>
								<li><label for="animate-fade-down"><input type="radio" name="animateName" value="fade-down" id="animate-fade-down"> fade-down</label></li>
								<li><label for="animate-fade-left"><input type="radio" name="animateName" value="fade-left" id="animate-fade-left"> fade-left</label></li>
								<li><label for="animate-fade-right"><input type="radio" name="animateName" value="fade-right" id="animate-fade-right"> fade-right</label></li>
								<li><label for="animate-fade-up-right"><input type="radio" name="animateName" value="fade-up-right" id="animate-fade-up-right"> fade-up-right</label></li>
								<li><label for="animate-fade-up-left"><input type="radio" name="animateName" value="fade-up-left" id="animate-fade-up-left"> fade-up-left</label></li>
								<li><label for="animate-fade-down-right"><input type="radio" name="animateName" value="fade-down-right" id="animate-fade-down-right"> fade-down-right</label></li>
								<li><label for="animate-fade-down-left"><input type="radio" name="animateName" value="fade-down-left" id="animate-fade-down-left"> fade-down-left</label></li>
								<li><label for="animate-flip-up"><input type="radio" name="animateName" value="flip-up" id="animate-flip-up"> flip-up</label></li>
								<li><label for="animate-flip-down"><input type="radio" name="animateName" value="flip-down" id="animate-flip-down"> flip-down</label></li>
								<li><label for="animate-flip-left"><input type="radio" name="animateName" value="flip-left" id="animate-flip-left"> flip-left</label></li>
								<li><label for="animate-flip-right"><input type="radio" name="animateName" value="flip-right" id="animate-flip-right"> flip-right</label></li>
								<li><label for="animate-slide-up"><input type="radio" name="animateName" value="slide-up" id="animate-slide-up"> slide-up</label></li>
								<li><label for="animate-slide-down"><input type="radio" name="animateName" value="slide-down" id="animate-slide-down"> slide-down</label></li>
								<li><label for="animate-slide-left"><input type="radio" name="animateName" value="slide-left" id="animate-slide-left"> slide-left</label></li>
								<li><label for="animate-slide-right"><input type="radio" name="animateName" value="slide-right" id="animate-slide-right"> slide-right</label></li>
								<li><label for="animate-zoom-in"><input type="radio" name="animateName" value="zoom-in" id="animate-zoom-in"> zoom-in</label></li>
								<li><label for="animate-zoom-in-up"><input type="radio" name="animateName" value="zoom-in-up" id="animate-zoom-in-up"> zoom-in-up</label></li>
								<li><label for="animate-zoom-in-down"><input type="radio" name="animateName" value="zoom-in-down" id="animate-zoom-in-down"> zoom-in-down</label></li>
								<li><label for="animate-zoom-in-left"><input type="radio" name="animateName" value="zoom-in-left" id="animate-zoom-in-left"> zoom-in-left</label></li>
								<li><label for="animate-zoom-in-right"><input type="radio" name="animateName" value="zoom-in-right" id="animate-zoom-in-right"> zoom-in-right</label></li>
								<li><label for="animate-zoom-out"><input type="radio" name="animateName" value="zoom-out" id="animate-zoom-out"> zoom-out</label></li>
								<li><label for="animate-zoom-out-up"><input type="radio" name="animateName" value="zoom-out-up" id="animate-zoom-out-up"> zoom-out-up</label></li>
								<li><label for="animate-zoom-out-down"><input type="radio" name="animateName" value="zoom-out-down" id="animate-zoom-out-down"> zoom-out-down</label></li>
								<li><label for="animate-zoom-out-left"><input type="radio" name="animateName" value="zoom-out-left" id="animate-zoom-out-left"> zoom-out-left</label></li>
								<li><label for="animate-zoom-out-right"><input type="radio" name="animateName" value="zoom-out-right" id="animate-zoom-out-right"> zoom-out-right</label></li>
							</ul>
						</div>
						<div class="is-box-6">
							<div class="md-label">Offset:</div> <input type="text" name="animateOffset" value="100" class="inptxt" style="float:right;width:60%">
							<div class="md-label">Delay:</div> <input type="text" name="animateDelay" value="30" class="inptxt" style="float:right;width:60%">
							<div class="md-label">Duration:</div> <input type="text" name="animateDuration" value="600" class="inptxt" style="float:right;width:60%">
							<div class="switch" style="float:right;margin-top:15px;">
									<input type="checkbox" id="animateOnce" name="animateOnce" value="true">
									<label style="padding: 0 15px;" for="animateOnce" class="inpchk"> Animate Once</label>
							</div>
						</div>
					</div>
				</div>


			</div>





			</div>


			</div>
			<div class="md-footer" style="position:absolute;width:100%;height:55px;left:0;bottom:0;">
				<button id="styleselectOk"> Ok </button>
			</div>
		</div>
	</div>




	<div class="md-modal" id="md-addsavedsection">
		<div class="md-content">
			<div class="md-body">
				<div class="custom-section-list">
					{!! getSavedSections() !!}
				</div>
			</div>
		</div>
	</div>

	<div class="md-modal" id="md-makeglobal">
		<div class="md-content">
			<div class="md-body" style="padding-top: 55px;padding-left:15px;padding-right: 15px;">
				<div class="md-modal-handle" style="cursor: move;">
						<div class="md-title" style="margin:0 0 12px;">Make Global</div>
				</div>
				<form method="POST" id="create_section_form" action="">
		        {{ csrf_field() }}
						<div class="row">
							<div class="col-md-12">
								<div class="inlinelabel title">
									<label>Title</label>
									<input type="text" name="title" placeholder="" required>
									<input type="hidden" name="html">
								</div>
							</div>
						</div>
        </form>
				<div style="font-size: 14px;padding-bottom: 10px;">Once the section is saved you have to edit the section in Dashboard > Sections </div>
			</div>
			<div class="modal-footer">
				<button type="button" id="cancelglobal" style="padding:10px;" data-dismiss="modal">Cancel</button>
				<button type="button" id="saveglobal" style="padding:10px;">Save Section</button>
			</div>
		</div>
	</div>
	<script>
	$(document).ready(function(){
		$('#is-wrapper').on('click', ".is-section-global", function (e) {
				e.preventDefault();
				$('#md-makeglobal').css('max-width', '550px');
				$('#md-makeglobal').simplemodal();
				$('#md-makeglobal').data('simplemodal').show();
			  var $activeSection = jQuery(this).parent().parent();
				$('#create_section_form input[name="html"]').val($activeSection[0].outerHTML);
				$('#saveglobal').prop('disabled', false);
				$('#saveglobal').on('click', function(){
					$(this).prop('disabled', true);
					$('#create_section_form input[type="text"]').removeClass('parsley-error');
					var parentEle = $('#md-makeglobal');
					parentEle.find('.help-block').remove();
					$.ajax({
			            type: "POST",
			            url: '{{URL::route('sections.builderstore')}}',
			            data: $("#create_section_form").serialize(),
			            success: function( msg ) {
											var obj = jQuery.parseJSON(msg);
			                if(obj.success === true){
													var newArea = '<div class="is-section is-section-50 is-bg-grey is-boxed-layout is-box saved-section"> <div class="is-boxes"> <div class="is-overlay"><div class="is-overlay-color"></div> <div class="is-overlay-content"></div> </div> <div class="is-box-centered text-center"><div id="save-section-'+makeid()+'">'+obj.shortcode+'</div></div></div></div>';
													$('#is-wrapper').data("contentbox").DuplicateSection(newArea, $activeSection,false,false);
													$activeSection.fadeOut();
													$activeSection.remove();
													$('#md-makeglobal').data('simplemodal').hide();
													$('#saveglobal').prop('disabled', false);
			        				}else{
			        					$.each(obj.errors, function(key,valueObj){
			        						parentEle.find('.'+key).append('<span class="help-block" style="font-size: 14px;font-style: italic"><strong>'+valueObj+'</strong></span>');
			        					});
												$('#saveglobal').prop('disabled', false);
			        				}
			            }
			        });
				})

				$('#cancelglobal').on('click', function(){
						$('#md-makeglobal').data('simplemodal').hide();
				})
		});
		$('#is-wrapper').on('click', ".is-section-removeglobal", function (e) {
				e.preventDefault();
			  var $activeSection = jQuery(this).parent().parent();
				$activeSection.css('opacity', .4);
				console.log($activeSection[0].outerHTML);
				$.ajax({
            type: "POST",
            url: '{{URL::route('sections.getSectionFromHtml')}}',
            data: {sectionhtml:$activeSection[0].outerHTML, "_token": "{{ csrf_token() }}"},
            success: function( msg ) {
								var obj = jQuery.parseJSON(msg);
                if(obj.success === true){
										var newArea = obj.html;
										$('#is-wrapper').data("contentbox").DuplicateSection(newArea, $activeSection,true,true);
										$activeSection.fadeOut();
										$activeSection.remove();
        				}else{
        					$activeSection.css('opacity', '1');
        				}
            }
        });
	})
})
	</script>
