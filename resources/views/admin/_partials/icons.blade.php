<style>
#icon-select .modal-body .icon{
	    display: inline-block;
    margin: 5px;
    font-size: 17px;
}
</style>
<div id="icon-select" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" style="width: 100%;max-width: 900px;">
		<div class="modal-content">
			<div class="modal-header" style="border-bottom:0;padding-bottom:0;height:50px;">
				<h4 class="modal-title" id="full-width-modalLabel" style="line-height: 1.2;">Icons</h4>
				<div class="icon" style="margin-left:10px;"><a href="#" data-icon="" class="btn btn-purple waves-effect waves-light btn-xs">No Icon</a></div>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow: auto;">
				<div id="icons">
					@foreach(getIcons() as $key=>$val)
					<div class="icon"><a href="#" data-icon="{{ $val }}"><i class="{{ $val }}" title="{{ $val }}" alt="{{ $val }}"></i></a></div>
					@endforeach

				</div>
				<div class="panel-group hidden">
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#icons" href="#glyphicons">
						Glyphicons</a>
					  </h4>
					</div>
					<div id="glyphicons" class="panel-collapse collapse">
					  <div class="panel-body">
							<div class="card-box">

                                        <div class="row">
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-glass"><i class="glyphicon glyphicon-glass" title="glyphicon-glass" alt="glyphicon-glass"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-music"><i class="glyphicon glyphicon-music" title="glyphicon-music" alt="glyphicon-music"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-search"><i class="glyphicon glyphicon-search" title="glyphicon-search" alt="glyphicon-search"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-envelope"><i class="glyphicon glyphicon-envelope" title="glyphicon-envelope" alt="glyphicon-envelope"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-heart"><i class="glyphicon glyphicon-heart" title="glyphicon-heart" alt="glyphicon-heart"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-star"><i class="glyphicon glyphicon-star" title="glyphicon-star" alt="glyphicon-star"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-star-empty"><i class="glyphicon glyphicon-star-empty" title="glyphicon-star-empty" alt="glyphicon-star-empty"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-user"><i class="glyphicon glyphicon-user" title="glyphicon-user" alt="glyphicon-user"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-film"><i class="glyphicon glyphicon-film" title="glyphicon-film" alt="glyphicon-film"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-th-large"><i class="glyphicon glyphicon-th-large" title="glyphicon-th-large" alt="glyphicon-th-large"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-th"><i class="glyphicon glyphicon-th" title="glyphicon-th" alt="glyphicon-th"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-th-list"><i class="glyphicon glyphicon-th-list" title="glyphicon-th-list" alt="glyphicon-th-list"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-ok"><i class="glyphicon glyphicon-ok" title="glyphicon-ok" alt="glyphicon-ok"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-remove"><i class="glyphicon glyphicon-remove" title="glyphicon-remove" alt="glyphicon-remove"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-zoom-in"><i class="glyphicon glyphicon-zoom-in" title="glyphicon-zoom-in" alt="glyphicon-zoom-in"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-zoom-out"><i class="glyphicon glyphicon-zoom-out" title="glyphicon-zoom-out" alt="glyphicon-zoom-out"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-off"><i class="glyphicon glyphicon-off" title="glyphicon-off" alt="glyphicon-off"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-signal"><i class="glyphicon glyphicon-signal" title="glyphicon-signal" alt="glyphicon-signal"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-cog"><i class="glyphicon glyphicon-cog" title="glyphicon-cog" alt="glyphicon-cog"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-trash"><i class="glyphicon glyphicon-trash" title="glyphicon-trash" alt="glyphicon-trash"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-home"><i class="glyphicon glyphicon-home" title="glyphicon-home" alt="glyphicon-home"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-file"><i class="glyphicon glyphicon-file" title="glyphicon-file" alt="glyphicon-file"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-time"><i class="glyphicon glyphicon-time" title="glyphicon-time" alt="glyphicon-time"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-road"><i class="glyphicon glyphicon-road" title="glyphicon-road" alt="glyphicon-road"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-download-alt"><i class="glyphicon glyphicon-download-alt" title="glyphicon-download-alt" alt="glyphicon-download-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-download"><i class="glyphicon glyphicon-download" title="glyphicon-download" alt="glyphicon-download"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-upload"><i class="glyphicon glyphicon-upload" title="glyphicon-upload" alt="glyphicon-upload"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-inbox"><i class="glyphicon glyphicon-inbox" title="glyphicon-inbox" alt="glyphicon-inbox"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-play-circle"><i class="glyphicon glyphicon-play-circle" title="glyphicon-play-circle" alt="glyphicon-play-circle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-repeat"><i class="glyphicon glyphicon-repeat" title="glyphicon-repeat" alt="glyphicon-repeat"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-refresh"><i class="glyphicon glyphicon-refresh" title="glyphicon-refresh" alt="glyphicon-refresh"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-list-alt"><i class="glyphicon glyphicon-list-alt" title="glyphicon-list-alt" alt="glyphicon-list-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-lock"><i class="glyphicon glyphicon-lock" title="glyphicon-lock&gt;" alt="glyphicon-lock&gt;"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-flag"><i class="glyphicon glyphicon-flag" title="glyphicon-flag" alt="glyphicon-flag"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-headphones"><i class="glyphicon glyphicon-headphones" title="glyphicon-headphones" alt="glyphicon-headphones"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-volume-off"><i class="glyphicon glyphicon-volume-off" title="glyphicon-volume-off" alt="glyphicon-volume-off"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-volume-down"><i class="glyphicon glyphicon-volume-down" title="glyphicon-volume-down" alt="glyphicon-volume-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-volume-up"><i class="glyphicon glyphicon-volume-up" title="glyphicon-volume-up" alt="glyphicon-volume-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-qrcode"><i class="glyphicon glyphicon-qrcode" title="glyphicon-qrcode" alt="glyphicon-qrcode"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-barcode"><i class="glyphicon glyphicon-barcode" title="glyphicon-barcode" alt="glyphicon-barcode"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-tag"><i class="glyphicon glyphicon-tag" title="glyphicon-tag" alt="glyphicon-tag"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-tags"><i class="glyphicon glyphicon-tags" title="glyphicon-tags" alt="glyphicon-tags"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-book"><i class="glyphicon glyphicon-book" title="glyphicon-book" alt="glyphicon-book"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-bookmark"><i class="glyphicon glyphicon-bookmark" title="glyphicon-bookmark&gt;" alt="glyphicon-bookmark&gt;"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-print"><i class="glyphicon glyphicon-print" title="glyphicon-print" alt="glyphicon-print"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-camera"><i class="glyphicon glyphicon-camera" title="glyphicon-camera&gt;" alt="glyphicon-camera&gt;"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-font"><i class="glyphicon glyphicon-font" title="glyphicon-font" alt="glyphicon-font"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-bold"><i class="glyphicon glyphicon-bold" title="glyphicon-bold" alt="glyphicon-bold"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-italic"><i class="glyphicon glyphicon-italic" title="glyphicon-italic" alt="glyphicon-italic"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-text-height"><i class="glyphicon glyphicon-text-height" title="glyphicon-text-height" alt="glyphicon-text-height"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-text-width"><i class="glyphicon glyphicon-text-width" title="glyphicon-text-width" alt="glyphicon-text-width"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-align-left"><i class="glyphicon glyphicon-align-left" title="glyphicon-align-left" alt="glyphicon-align-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-align-center"><i class="glyphicon glyphicon-align-center" title="glyphicon-align-center" alt="glyphicon-align-center"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-align-right"><i class="glyphicon glyphicon-align-right" title="glyphicon-align-right" alt="glyphicon-align-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-align-justify"><i class="glyphicon glyphicon-align-justify" title="glyphicon-align-justify" alt="glyphicon-align-justify"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-list"><i class="glyphicon glyphicon-list" title="glyphicon-list" alt="glyphicon-list"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-indent-left"><i class="glyphicon glyphicon-indent-left" title="glyphicon-indent-left" alt="glyphicon-indent-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-indent-right"><i class="glyphicon glyphicon-indent-right" title="glyphicon-indent-right" alt="glyphicon-indent-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-facetime-video"><i class="glyphicon glyphicon-facetime-video" title="glyphicon-facetime-video" alt="glyphicon-facetime-video"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-picture"><i class="glyphicon glyphicon-picture" title="glyphicon-picture" alt="glyphicon-picture"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-pencil"><i class="glyphicon glyphicon-pencil" title="glyphicon-pencil" alt="glyphicon-pencil"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-map-marker"><i class="glyphicon glyphicon-map-marker" title="glyphicon-map-marker" alt="glyphicon-map-marker"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-adjust"><i class="glyphicon glyphicon-adjust" title="glyphicon-adjust" alt="glyphicon-adjust"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-tint"><i class="glyphicon glyphicon-tint" title="glyphicon-tint" alt="glyphicon-tint"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-edit"><i class="glyphicon glyphicon-edit" title="glyphicon-edit" alt="glyphicon-edit"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-share"><i class="glyphicon glyphicon-share" title="glyphicon-share" alt="glyphicon-share"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-check"><i class="glyphicon glyphicon-check" title="glyphicon-check" alt="glyphicon-check"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-move"><i class="glyphicon glyphicon-move" title="glyphicon-move" alt="glyphicon-move"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-step-backward"><i class="glyphicon glyphicon-step-backward" title="glyphicon-step-backward" alt="glyphicon-step-backward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-fast-backward"><i class="glyphicon glyphicon-fast-backward" title="glyphicon-fast-backward" alt="glyphicon-fast-backward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-backward"><i class="glyphicon glyphicon-backward" title="glyphicon-backward" alt="glyphicon-backward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-play"><i class="glyphicon glyphicon-play" title="glyphicon-play" alt="glyphicon-play"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-pause"><i class="glyphicon glyphicon-pause" title="glyphicon-pause" alt="glyphicon-pause"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-stop"><i class="glyphicon glyphicon-stop" title="glyphicon-stop" alt="glyphicon-stop"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-forward"><i class="glyphicon glyphicon-forward" title="glyphicon-forward" alt="glyphicon-forward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-fast-forward"><i class="glyphicon glyphicon-fast-forward" title="glyphicon-fast-forward" alt="glyphicon-fast-forward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-step-forward"><i class="glyphicon glyphicon-step-forward" title="glyphicon-step-forward" alt="glyphicon-step-forward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-eject"><i class="glyphicon glyphicon-eject" title="glyphicon-eject" alt="glyphicon-eject"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-chevron-left"><i class="glyphicon glyphicon-chevron-left" title="glyphicon-chevron-left" alt="glyphicon-chevron-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-chevron-right"><i class="glyphicon glyphicon-chevron-right" title="glyphicon-chevron-right" alt="glyphicon-chevron-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-plus-sign"><i class="glyphicon glyphicon-plus-sign" title="glyphicon-plus-sign" alt="glyphicon-plus-sign"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-minus-sign"><i class="glyphicon glyphicon-minus-sign" title="glyphicon-minus-sign" alt="glyphicon-minus-sign"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-remove-sign"><i class="glyphicon glyphicon-remove-sign" title="glyphicon-remove-sign" alt="glyphicon-remove-sign"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-ok-sign"><i class="glyphicon glyphicon-ok-sign" title="glyphicon-ok-sign" alt="glyphicon-ok-sign"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-question-sign"><i class="glyphicon glyphicon-question-sign" title="glyphicon-question-sign" alt="glyphicon-question-sign"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-info-sign"><i class="glyphicon glyphicon-info-sign" title="glyphicon-info-sign" alt="glyphicon-info-sign"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-screenshot"><i class="glyphicon glyphicon-screenshot" title="glyphicon-screenshot" alt="glyphicon-screenshot"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-remove-circle"><i class="glyphicon glyphicon-remove-circle" title="glyphicon-remove-circle" alt="glyphicon-remove-circle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-ok-circle"><i class="glyphicon glyphicon-ok-circle" title="glyphicon-ok-circle" alt="glyphicon-ok-circle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-ban-circle"><i class="glyphicon glyphicon-ban-circle" title="glyphicon-ban-circle" alt="glyphicon-ban-circle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-arrow-left"><i class="glyphicon glyphicon-arrow-left" title="glyphicon-arrow-left" alt="glyphicon-arrow-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-arrow-right"><i class="glyphicon glyphicon-arrow-right" title="glyphicon-arrow-right" alt="glyphicon-arrow-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-arrow-up"><i class="glyphicon glyphicon-arrow-up" title="glyphicon-arrow-up" alt="glyphicon-arrow-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-arrow-down"><i class="glyphicon glyphicon-arrow-down" title="glyphicon-arrow-down" alt="glyphicon-arrow-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-share-alt"><i class="glyphicon glyphicon-share-alt" title="glyphicon-share-alt" alt="glyphicon-share-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-resize-full"><i class="glyphicon glyphicon-resize-full" title="glyphicon-resize-full" alt="glyphicon-resize-full"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-resize-small"><i class="glyphicon glyphicon-resize-small" title="glyphicon-resize-small" alt="glyphicon-resize-small"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-plus"><i class="glyphicon glyphicon-plus" title="glyphicon-plus" alt="glyphicon-plus"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-minus"><i class="glyphicon glyphicon-minus" title="glyphicon-minus" alt="glyphicon-minus"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-asterisk"><i class="glyphicon glyphicon-asterisk" title="glyphicon-asterisk" alt="glyphicon-asterisk"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-exclamation-sign"><i class="glyphicon glyphicon-exclamation-sign" title="glyphicon-exclamation-sign" alt="glyphicon-exclamation-sign"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-gift"><i class="glyphicon glyphicon-gift" title="glyphicon-gift" alt="glyphicon-gift"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-leaf"><i class="glyphicon glyphicon-leaf" title="glyphicon-leaf" alt="glyphicon-leaf"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-fire"><i class="glyphicon glyphicon-fire" title="glyphicon-fire" alt="glyphicon-fire"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-eye-open"><i class="glyphicon glyphicon-eye-open" title="glyphicon-eye-open" alt="glyphicon-eye-open"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-eye-close"><i class="glyphicon glyphicon-eye-close" title="glyphicon-eye-close" alt="glyphicon-eye-close"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-warning-sign"><i class="glyphicon glyphicon-warning-sign" title="glyphicon-warning-sign" alt="glyphicon-warning-sign"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-plane"><i class="glyphicon glyphicon-plane" title="glyphicon-plane" alt="glyphicon-plane"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-calendar"><i class="glyphicon glyphicon-calendar" title="glyphicon-calendar" alt="glyphicon-calendar"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-random"><i class="glyphicon glyphicon-random" title="glyphicon-random" alt="glyphicon-random"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-comment"><i class="glyphicon glyphicon-comment" title="glyphicon-comment" alt="glyphicon-comment"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-magnet"><i class="glyphicon glyphicon-magnet" title="glyphicon-magnet" alt="glyphicon-magnet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-chevron-up"><i class="glyphicon glyphicon-chevron-up" title="glyphicon-chevron-up" alt="glyphicon-chevron-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-chevron-down"><i class="glyphicon glyphicon-chevron-down" title="glyphicon-chevron-down" alt="glyphicon-chevron-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-retweet"><i class="glyphicon glyphicon-retweet" title="glyphicon-retweet" alt="glyphicon-retweet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-shopping-cart"><i class="glyphicon glyphicon-shopping-cart" title="glyphicon-shopping-cart" alt="glyphicon-shopping-cart"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-folder-close"><i class="glyphicon glyphicon-folder-close" title="glyphicon-folder-close" alt="glyphicon-folder-close"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-folder-open"><i class="glyphicon glyphicon-folder-open" title="glyphicon-folder-open" alt="glyphicon-folder-open"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-resize-vertical"><i class="glyphicon glyphicon-resize-vertical" title="glyphicon-resize-vertical" alt="glyphicon-resize-vertical"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-resize-horizontal"><i class="glyphicon glyphicon-resize-horizontal" title="glyphicon-resize-horizontal" alt="glyphicon-resize-horizontal"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-hdd"><i class="glyphicon glyphicon-hdd" title="glyphicon-hdd" alt="glyphicon-hdd"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-bullhorn"><i class="glyphicon glyphicon-bullhorn" title="glyphicon-bullhorn" alt="glyphicon-bullhorn"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-bell"><i class="glyphicon glyphicon-bell" title="glyphicon-bell" alt="glyphicon-bell"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-certificate"><i class="glyphicon glyphicon-certificate" title="glyphicon-certificate" alt="glyphicon-certificate"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-thumbs-up"><i class="glyphicon glyphicon-thumbs-up" title="glyphicon-thumbs-up" alt="glyphicon-thumbs-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-thumbs-down"><i class="glyphicon glyphicon-thumbs-down" title="glyphicon-thumbs-down" alt="glyphicon-thumbs-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-hand-right"><i class="glyphicon glyphicon-hand-right" title="glyphicon-hand-right" alt="glyphicon-hand-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-hand-left"><i class="glyphicon glyphicon-hand-left" title="glyphicon-hand-left" alt="glyphicon-hand-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-hand-up"><i class="glyphicon glyphicon-hand-up" title="glyphicon-hand-up" alt="glyphicon-hand-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-hand-down"><i class="glyphicon glyphicon-hand-down" title="glyphicon-hand-down" alt="glyphicon-hand-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-circle-arrow-right"><i class="glyphicon glyphicon-circle-arrow-right" title="glyphicon-circle-arrow-right" alt="glyphicon-circle-arrow-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-circle-arrow-left"><i class="glyphicon glyphicon-circle-arrow-left" title="glyphicon-circle-arrow-left" alt="glyphicon-circle-arrow-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-circle-arrow-up"><i class="glyphicon glyphicon-circle-arrow-up" title="glyphicon-circle-arrow-up" alt="glyphicon-circle-arrow-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-circle-arrow-down"><i class="glyphicon glyphicon-circle-arrow-down" title="glyphicon-circle-arrow-down" alt="glyphicon-circle-arrow-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-globe"><i class="glyphicon glyphicon-globe" title="glyphicon-globe" alt="glyphicon-globe"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-wrench"><i class="glyphicon glyphicon-wrench" title="glyphicon-wrench" alt="glyphicon-wrench"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-tasks"><i class="glyphicon glyphicon-tasks" title="glyphicon-tasks" alt="glyphicon-tasks"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-filter"><i class="glyphicon glyphicon-filter" title="glyphicon-filter" alt="glyphicon-filter"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-briefcase"><i class="glyphicon glyphicon-briefcase" title="glyphicon-briefcase" alt="glyphicon-briefcase"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-fullscreen"><i class="glyphicon glyphicon-fullscreen" title="glyphicon-fullscreen" alt="glyphicon-fullscreen"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-dashboard"><i class="glyphicon glyphicon-dashboard" title="glyphicon-dashboard" alt="glyphicon-dashboard"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-paperclip"><i class="glyphicon glyphicon-paperclip" title="glyphicon-paperclipn" alt="glyphicon-paperclipn"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-heart-empty"><i class="glyphicon glyphicon-heart-empty" title="glyphicon-heart-empty" alt="glyphicon-heart-empty"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-link"><i class="glyphicon glyphicon-link" title="glyphicon-link" alt="glyphicon-link"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-phone"><i class="glyphicon glyphicon-phone" title="glyphicon-phone" alt="glyphicon-phone"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-pushpin"><i class="glyphicon glyphicon-pushpin" title="glyphicon-pushpin" alt="glyphicon-pushpin"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-euro"><i class="glyphicon glyphicon-euro" title="glyphicon-euro" alt="glyphicon-euro"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-usd"><i class="glyphicon glyphicon-usd" title="glyphicon-usd" alt="glyphicon-usd"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-gbp"><i class="glyphicon glyphicon-gbp" title="glyphicon-gbp" alt="glyphicon-gbp"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sort"><i class="glyphicon glyphicon-sort" title="glyphicon-sort" alt="glyphicon-sort"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sort-by-alphabet"><i class="glyphicon glyphicon-sort-by-alphabet" title="glyphicon-sort-by-alphabet" alt="glyphicon-sort-by-alphabet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sort-by-alphabet-alt"><i class="glyphicon glyphicon-sort-by-alphabet-alt" title="glyphicon-sort-by-alphabet-alt" alt="glyphicon-sort-by-alphabet-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sort-by-order"><i class="glyphicon glyphicon-sort-by-order" title="glyphicon-sort-by-order" alt="glyphicon-sort-by-order"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sort-by-order-alt"><i class="glyphicon glyphicon-sort-by-order-alt" title="glyphicon-sort-by-order-alt" alt="glyphicon-sort-by-order-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sort-by-attributes"><i class="glyphicon glyphicon-sort-by-attributes" title="glyphicon-sort-by-attributes" alt="glyphicon-sort-by-attributes"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sort-by-attributes-alt"><i class="glyphicon glyphicon-sort-by-attributes-alt" title="glyphicon-sort-by-attributes-alt" alt="glyphicon-sort-by-attributes-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-unchecked"><i class="glyphicon glyphicon-unchecked" title="glyphicon-unchecked" alt="glyphicon-unchecked"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-expand"><i class="glyphicon glyphicon-expand" title="glyphicon-expand" alt="glyphicon-expand"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-collapse-down"><i class="glyphicon glyphicon-collapse-down" title="glyphicon-collapse-down" alt="glyphicon-collapse-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-collapse-up"><i class="glyphicon glyphicon-collapse-up" title="glyphicon-collapse-top" alt="glyphicon-collapse-top"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-log-in"><i class="glyphicon glyphicon-log-in" title="glyphicon-log-in" alt="glyphicon-log-in"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-flash"><i class="glyphicon glyphicon-flash" title="glyphicon-flash" alt="glyphicon-flash"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-log-out"><i class="glyphicon glyphicon-log-out" title="glyphicon-log-out" alt="glyphicon-log-out"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-new-window"><i class="glyphicon glyphicon-new-window" title="glyphicon-new-window" alt="glyphicon-new-window"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-record"><i class="glyphicon glyphicon-record" title="glyphicon-record" alt="glyphicon-record"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-save"><i class="glyphicon glyphicon-save" title="glyphicon-save" alt="glyphicon-save"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-open"><i class="glyphicon glyphicon-open" title="glyphicon-open" alt="glyphicon-open"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-saved"><i class="glyphicon glyphicon-saved" title="glyphicon-saved" alt="glyphicon-saved"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-import"><i class="glyphicon glyphicon-import" title="glyphicon-import" alt="glyphicon-import"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-export"><i class="glyphicon glyphicon-export" title="glyphicon-export" alt="glyphicon-export"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-send"><i class="glyphicon glyphicon-send" title="glyphicon-send" alt="glyphicon-send"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-floppy-disk"><i class="glyphicon glyphicon-floppy-disk" title="glyphicon-floppy-disk" alt="glyphicon-floppy-disk"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-floppy-saved"><i class="glyphicon glyphicon-floppy-saved" title="glyphicon-floppy-saved" alt="glyphicon-floppy-saved"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-floppy-remove"><i class="glyphicon glyphicon-floppy-remove" title="glyphicon-floppy-remove" alt="glyphicon-floppy-remove"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-floppy-save"><i class="glyphicon glyphicon-floppy-save" title="glyphicon-floppy-save" alt="glyphicon-floppy-save"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-floppy-open"><i class="glyphicon glyphicon-floppy-open" title="glyphicon-floppy-open" alt="glyphicon-floppy-open"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-credit-card"><i class="glyphicon glyphicon-credit-card" title="glyphicon-credit-card" alt="glyphicon-credit-card"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-transfer"><i class="glyphicon glyphicon-transfer" title="glyphicon-transfer" alt="glyphicon-transfer"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-cutlery"><i class="glyphicon glyphicon-cutlery" title="glyphicon-cutlery" alt="glyphicon-cutlery"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-header"><i class="glyphicon glyphicon-header" title="glyphicon-header" alt="glyphicon-header"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-compressed"><i class="glyphicon glyphicon-compressed" title="glyphicon-compressed" alt="glyphicon-compressed"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-earphone"><i class="glyphicon glyphicon-earphone" title="glyphicon-earphone" alt="glyphicon-earphone"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-phone-alt"><i class="glyphicon glyphicon-phone-alt" title="glyphicon-phone-alt" alt="glyphicon-phone-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-tower"><i class="glyphicon glyphicon-tower" title="glyphicon-tower" alt="glyphicon-tower"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-stats"><i class="glyphicon glyphicon-stats" title="glyphicon-stats" alt="glyphicon-stats"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sd-video"><i class="glyphicon glyphicon-sd-video" title="glyphicon-sd-video" alt="glyphicon-sd-video"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-hd-video"><i class="glyphicon glyphicon-hd-video" title="glyphicon-hd-video" alt="glyphicon-hd-video"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-subtitles"><i class="glyphicon glyphicon-subtitles" title="glyphicon-subtitles" alt="glyphicon-subtitles"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sound-stereo"><i class="glyphicon glyphicon-sound-stereo" title="glyphicon-sound-stereo" alt="glyphicon-sound-stereo"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sound-dolby"><i class="glyphicon glyphicon-sound-dolby" title="glyphicon-sound-dolby" alt="glyphicon-sound-dolby"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sound-5-1"><i class="glyphicon glyphicon-sound-5-1" title="glyphicon-sound-5-1" alt="glyphicon-sound-5-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sound-6-1"><i class="glyphicon glyphicon-sound-6-1" title="glyphicon-sound-6-1" alt="glyphicon-sound-6-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-sound-7-1"><i class="glyphicon glyphicon-sound-7-1" title="glyphicon-sound-7-1" alt="glyphicon-sound-7-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-copyright-mark"><i class="glyphicon glyphicon-copyright-mark" title="glyphicon-copyright-mark" alt="glyphicon-copyright-mark"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-registration-mark"><i class="glyphicon glyphicon-registration-mark" title="glyphicon-registration-mark" alt="glyphicon-registration-mark"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-cloud"><i class="glyphicon glyphicon-cloud" title="glyphicon-cloud" alt="glyphicon-cloud"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-cloud-download"><i class="glyphicon glyphicon-cloud-download" title="glyphicon-cloud-download" alt="glyphicon-cloud-download"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-cloud-upload"><i class="glyphicon glyphicon-cloud-upload" title="glyphicon-cloud-upload" alt="glyphicon-cloud-upload"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-tree-conifer"><i class="glyphicon glyphicon-tree-conifer" title="glyphicon-tree-conifer" alt="glyphicon-tree-conifer"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="glyphicon glyphicon-tree-deciduous"><i class="glyphicon glyphicon-tree-deciduous" title="glyphicon-tree-deciduous" alt="glyphicon-tree-deciduous"></i></a></div>
										</div>
										<!-- End row -->
                                    </div>
					  </div>
					</div>
				  </div>
				  <div class="hidden panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#icons" href="#md">
						Material Design</a>
					  </h4>
					</div>
					<div id="md" class="panel-collapse collapse">
					  <div class="panel-body">
								<div class="card-box">
                                        <!-- Icon sets -->
                                        <section>
                                            <h4 class="m-t-0 header-title"><b>Action</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-3d-rotation"><i class="md md-3d-rotation" title="md-3d-rotation" alt="md-3d-rotation"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-accessibility"><i class="md md-accessibility" title="md-accessibility" alt="md-accessibility"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-account-balance"><i class="md md-account-balance" title="md-account-balance" alt="md-account-balance"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-account-balance-wallet"><i class="md md-account-balance-wallet" title="md-account-balance-wallet" alt="md-account-balance-wallet"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-account-box"><i class="md md-account-box" title="md-account-box" alt="md-account-box"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-account-child"><i class="md md-account-child" title="md-account-child" alt="md-account-child"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-account-circle"><i class="md md-account-circle" title="md-account-circle" alt="md-account-circle"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-add-shopping-cart"><i class="md md-add-shopping-cart" title="md-add-shopping-cart" alt="md-add-shopping-cart"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-alarm"><i class="md md-alarm" title="md-alarm" alt="md-alarm"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-alarm-add"><i class="md md-alarm-add" title="md-alarm-add" alt="md-alarm-add"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-alarm-off"><i class="md md-alarm-off" title="md-alarm-off" alt="md-alarm-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-alarm-on"><i class="md md-alarm-on" title="md-alarm-on" alt="md-alarm-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-android"><i class="md md-android" title="md-android" alt="md-android"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-announcement"><i class="md md-announcement" title="md-announcement" alt="md-announcement"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-aspect-ratio"><i class="md md-aspect-ratio" title="md-aspect-ratio" alt="md-aspect-ratio"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-assessment"><i class="md md-assessment" title="md-assessment" alt="md-assessment"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-assignment"><i class="md md-assignment" title="md-assignment" alt="md-assignment"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-assignment-ind"><i class="md md-assignment-ind" title="md-assignment-ind" alt="md-assignment-ind"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-assignment-late"><i class="md md-assignment-late" title="md-assignment-late" alt="md-assignment-late"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-assignment-return"><i class="md md-assignment-return" title="md-assignment-return" alt="md-assignment-return"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-assignment-returned"><i class="md md-assignment-returned" title="md-assignment-returned" alt="md-assignment-returned"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-assignment-turned-in"><i class="md md-assignment-turned-in" title="md-assignment-turned-in" alt="md-assignment-turned-in"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-autorenew"><i class="md md-autorenew" title="md-autorenew" alt="md-autorenew"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-backup"><i class="md md-backup" title="md-backup" alt="md-backup"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-book"><i class="md md-book" title="md-book" alt="md-book"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-bookmark"><i class="md md-bookmark" title="md-bookmark" alt="md-bookmark"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-bookmark-outline"><i class="md md-bookmark-outline" title="md-bookmark-outline" alt="md-bookmark-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-bug-report"><i class="md md-bug-report" title="md-bug-report" alt="md-bug-report"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cached"><i class="md md-cached" title="md-cached" alt="md-cached"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-class"><i class="md md-class" title="md-class" alt="md-class"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-credit-card"><i class="md md-credit-card" title="md-credit-card" alt="md-credit-card"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-dashboard"><i class="md md-dashboard" title="md-dashboard" alt="md-dashboard"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-delete"><i class="md md-delete" title="md-delete" alt="md-delete"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-description"><i class="md md-description" title="md-description" alt="md-description"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-dns"><i class="md md-dns" title="md-dns" alt="md-dns"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-done"><i class="md md-done" title="md-done" alt="md-done"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-done-all"><i class="md md-done-all" title="md-done-all" alt="md-done-all"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-event"><i class="md md-event" title="md-event" alt="md-event"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-exit-to-app"><i class="md md-exit-to-app" title="md-exit-to-app" alt="md-exit-to-app"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-explore"><i class="md md-explore" title="md-explore" alt="md-explore"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-extension"><i class="md md-extension" title="md-extension" alt="md-extension"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-face-unlock"><i class="md md-face-unlock" title="md-face-unlock" alt="md-face-unlock"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-favorite"><i class="md md-favorite" title="md-favorite" alt="md-favorite"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-favorite-outline"><i class="md md-favorite-outline" title="md-favorite-outline" alt="md-favorite-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-find-in-page"><i class="md md-find-in-page" title="md-find-in-page" alt="md-find-in-page"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-find-replace"><i class="md md-find-replace" title="md-find-replace" alt="md-find-replace"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-flip-to-back"><i class="md md-flip-to-back" title="md-flip-to-back" alt="md-flip-to-back"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-flip-to-front"><i class="md md-flip-to-front" title="md-flip-to-front" alt="md-flip-to-front"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-get-app"><i class="md md-get-app" title="md-get-app" alt="md-get-app"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-grade"><i class="md md-grade" title="md-grade" alt="md-grade"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-group-work"><i class="md md-group-work" title="md-group-work" alt="md-group-work"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-help"><i class="md md-help" title="md-help" alt="md-help"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-highlight-remove"><i class="md md-highlight-remove" title="md-highlight-remove" alt="md-highlight-remove"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-history"><i class="md md-history" title="md-history" alt="md-history"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-home"><i class="md md-home" title="md-home" alt="md-home"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-https"><i class="md md-https" title="md-https" alt="md-https"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-info"><i class="md md-info" title="md-info" alt="md-info"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-info-outline"><i class="md md-info-outline" title="md-info-outline" alt="md-info-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-input"><i class="md md-input" title="md-input" alt="md-input"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-invert-colors"><i class="md md-invert-colors" title="md-invert-colors" alt="md-invert-colors"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-label"><i class="md md-label" title="md-label" alt="md-label"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-label-outline"><i class="md md-label-outline" title="md-label-outline" alt="md-label-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-language"><i class="md md-language" title="md-language" alt="md-language"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-launch"><i class="md md-launch" title="md-launch" alt="md-launch"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-list"><i class="md md-list" title="md-list" alt="md-list"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-lock"><i class="md md-lock" title="md-lock" alt="md-lock"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-lock-open"><i class="md md-lock-open" title="md-lock-open" alt="md-lock-open"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-lock-outline"><i class="md md-lock-outline" title="md-lock-outline" alt="md-lock-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-loyalty"><i class="md md-loyalty" title="md-loyalty" alt="md-loyalty"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-markunread-mailbox"><i class="md md-markunread-mailbox" title="md-markunread-mailbox" alt="md-markunread-mailbox"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-note-add"><i class="md md-note-add" title="md-note-add" alt="md-note-add"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-open-in-browser"><i class="md md-open-in-browser" title="md-open-in-browser" alt="md-open-in-browser"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-open-in-new"><i class="md md-open-in-new" title="md-open-in-new" alt="md-open-in-new"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-open-with"><i class="md md-open-with" title="md-open-with" alt="md-open-with"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-pageview"><i class="md md-pageview" title="md-pageview" alt="md-pageview"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-payment"><i class="md md-payment" title="md-payment" alt="md-payment"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-perm-camera-mic"><i class="md md-perm-camera-mic" title="md-perm-camera-mic" alt="md-perm-camera-mic"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-perm-contact-cal"><i class="md md-perm-contact-cal" title="md-perm-contact-cal" alt="md-perm-contact-cal"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-perm-data-setting"><i class="md md-perm-data-setting" title="md-perm-data-setting" alt="md-perm-data-setting"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-perm-device-info"><i class="md md-perm-device-info" title="md-perm-device-info" alt="md-perm-device-info"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-perm-identity"><i class="md md-perm-identity" title="md-perm-identity" alt="md-perm-identity"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-perm-media"><i class="md md-perm-media" title="md-perm-media" alt="md-perm-media"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-perm-phone-msg"><i class="md md-perm-phone-msg" title="md-perm-phone-msg" alt="md-perm-phone-msg"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-perm-scan-wifi"><i class="md md-perm-scan-wifi" title="md-perm-scan-wifi" alt="md-perm-scan-wifi"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-picture-in-picture"><i class="md md-picture-in-picture" title="md-picture-in-picture" alt="md-picture-in-picture"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-polymer"><i class="md md-polymer" title="md-polymer" alt="md-polymer"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-print"><i class="md md-print" title="md-print" alt="md-print"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-query-builder"><i class="md md-query-builder" title="md-query-builder" alt="md-query-builder"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-question-answer"><i class="md md-question-answer" title="md-question-answer" alt="md-question-answer"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-receipt"><i class="md md-receipt" title="md-receipt" alt="md-receipt"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-redeem"><i class="md md-redeem" title="md-redeem" alt="md-redeem"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-report-problem"><i class="md md-report-problem" title="md-report-problem" alt="md-report-problem"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-restore"><i class="md md-restore" title="md-restore" alt="md-restore"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-room"><i class="md md-room" title="md-room" alt="md-room"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-schedule"><i class="md md-schedule" title="md-schedule" alt="md-schedule"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-search"><i class="md md-search" title="md-search" alt="md-search"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings"><i class="md md-settings" title="md-settings" alt="md-settings"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-applications"><i class="md md-settings-applications" title="md-settings-applications" alt="md-settings-applications"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-backup-restore"><i class="md md-settings-backup-restore" title="md-settings-backup-restore" alt="md-settings-backup-restore"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-bluetooth"><i class="md md-settings-bluetooth" title="md-settings-bluetooth" alt="md-settings-bluetooth"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-cell"><i class="md md-settings-cell" title="md-settings-cell" alt="md-settings-cell"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-display"><i class="md md-settings-display" title="md-settings-display" alt="md-settings-display"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-ethernet"><i class="md md-settings-ethernet" title="md-settings-ethernet" alt="md-settings-ethernet"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-input-antenna"><i class="md md-settings-input-antenna" title="md-settings-input-antenna" alt="md-settings-input-antenna"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-input-component"><i class="md md-settings-input-component" title="md-settings-input-component" alt="md-settings-input-component"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-input-composite"><i class="md md-settings-input-composite" title="md-settings-input-composite" alt="md-settings-input-composite"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-input-hdmi"><i class="md md-settings-input-hdmi" title="md-settings-input-hdmi" alt="md-settings-input-hdmi"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-input-svideo"><i class="md md-settings-input-svideo" title="md-settings-input-svideo" alt="md-settings-input-svideo"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-overscan"><i class="md md-settings-overscan" title="md-settings-overscan" alt="md-settings-overscan"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-phone"><i class="md md-settings-phone" title="md-settings-phone" alt="md-settings-phone"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-power"><i class="md md-settings-power" title="md-settings-power" alt="md-settings-power"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-remote"><i class="md md-settings-remote" title="md-settings-remote" alt="md-settings-remote"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-voice"><i class="md md-settings-voice" title="md-settings-voice" alt="md-settings-voice"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-shop"><i class="md md-shop" title="md-shop" alt="md-shop"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-shopping-basket"><i class="md md-shopping-basket" title="md-shopping-basket" alt="md-shopping-basket"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-shopping-cart"><i class="md md-shopping-cart" title="md-shopping-cart" alt="md-shopping-cart"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-shop-two"><i class="md md-shop-two" title="md-shop-two" alt="md-shop-two"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-speaker-notes"><i class="md md-speaker-notes" title="md-speaker-notes" alt="md-speaker-notes"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-spellcheck"><i class="md md-spellcheck" title="md-spellcheck" alt="md-spellcheck"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-star-rate"><i class="md md-star-rate" title="md-star-rate" alt="md-star-rate"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-stars"><i class="md md-stars" title="md-stars" alt="md-stars"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-store"><i class="md md-store" title="md-store" alt="md-store"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-subject"><i class="md md-subject" title="md-subject" alt="md-subject"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-swap-horiz"><i class="md md-swap-horiz" title="md-swap-horiz" alt="md-swap-horiz"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-swap-vert"><i class="md md-swap-vert" title="md-swap-vert" alt="md-swap-vert"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-swap-vert-circle"><i class="md md-swap-vert-circle" title="md-swap-vert-circle" alt="md-swap-vert-circle"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-system-update-tv"><i class="md md-system-update-tv" title="md-system-update-tv" alt="md-system-update-tv"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tab"><i class="md md-tab" title="md-tab" alt="md-tab"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tab-unselected"><i class="md md-tab-unselected" title="md-tab-unselected" alt="md-tab-unselected"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-theaters"><i class="md md-theaters" title="md-theaters" alt="md-theaters"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-thumb-down"><i class="md md-thumb-down" title="md-thumb-down" alt="md-thumb-down"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-thumbs-up-down"><i class="md md-thumbs-up-down" title="md-thumbs-up-down" alt="md-thumbs-up-down"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-thumb-up"><i class="md md-thumb-up" title="md-thumb-up" alt="md-thumb-up"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-toc"><i class="md md-toc" title="md-toc" alt="md-toc"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-today"><i class="md md-today" title="md-today" alt="md-today"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-track-changes"><i class="md md-track-changes" title="md-track-changes" alt="md-track-changes"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-translate"><i class="md md-translate" title="md-translate" alt="md-translate"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-trending-down"><i class="md md-trending-down" title="md-trending-down" alt="md-trending-down"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-trending-neutral"><i class="md md-trending-neutral" title="md-trending-neutral" alt="md-trending-neutral"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-trending-up"><i class="md md-trending-up" title="md-trending-up" alt="md-trending-up"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-turned-in"><i class="md md-turned-in" title="md-turned-in" alt="md-turned-in"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-turned-in-not"><i class="md md-turned-in-not" title="md-turned-in-not" alt="md-turned-in-not"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-verified-user"><i class="md md-verified-user" title="md-verified-user" alt="md-verified-user"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-agenda"><i class="md md-view-agenda" title="md-view-agenda" alt="md-view-agenda"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-array"><i class="md md-view-array" title="md-view-array" alt="md-view-array"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-carousel"><i class="md md-view-carousel" title="md-view-carousel" alt="md-view-carousel"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-column"><i class="md md-view-column" title="md-view-column" alt="md-view-column"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-day"><i class="md md-view-day" title="md-view-day" alt="md-view-day"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-headline"><i class="md md-view-headline" title="md-view-headline" alt="md-view-headline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-list"><i class="md md-view-list" title="md-view-list" alt="md-view-list"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-module"><i class="md md-view-module" title="md-view-module" alt="md-view-module"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-quilt"><i class="md md-view-quilt" title="md-view-quilt" alt="md-view-quilt"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-stream"><i class="md md-view-stream" title="md-view-stream" alt="md-view-stream"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-view-week"><i class="md md-view-week" title="md-view-week" alt="md-view-week"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-visibility"><i class="md md-visibility" title="md-visibility" alt="md-visibility"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-visibility-off"><i class="md md-visibility-off" title="md-visibility-off" alt="md-visibility-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wallet-giftcard"><i class="md md-wallet-giftcard" title="md-wallet-giftcard" alt="md-wallet-giftcard"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wallet-membership"><i class="md md-wallet-membership" title="md-wallet-membership" alt="md-wallet-membership"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wallet-travel"><i class="md md-wallet-travel" title="md-wallet-travel" alt="md-wallet-travel"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-work"><i class="md md-work" title="md-work" alt="md-work"></i></a></div>
                                            </div>
                                        </section>
                                        <section class="icon-set">
                                            <h4 class="page-header header-title"><b>Alert</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-error"><i class="md md-error" title="md-error" alt="md-error"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-warning"><i class="md md-warning" title="md-warning" alt="md-warning"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Audio/Video</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-album"><i class="md md-album" title="md-album" alt="md-album"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-av-timer"><i class="md md-av-timer" title="md-av-timer" alt="md-av-timer"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-closed-caption"><i class="md md-closed-caption" title="md-closed-caption" alt="md-closed-caption"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-equalizer"><i class="md md-equalizer" title="md-equalizer" alt="md-equalizer"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-explicit"><i class="md md-explicit" title="md-explicit" alt="md-explicit"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-fast-forward"><i class="md md-fast-forward" title="md-fast-forward" alt="md-fast-forward"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-fast-rewind"><i class="md md-fast-rewind" title="md-fast-rewind" alt="md-fast-rewind"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-games"><i class="md md-games" title="md-games" alt="md-games"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-hearing"><i class="md md-hearing" title="md-hearing" alt="md-hearing"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-high-quality"><i class="md md-high-quality" title="md-high-quality" alt="md-high-quality"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-loop"><i class="md md-loop" title="md-loop" alt="md-loop"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-mic"><i class="md md-mic" title="md-mic" alt="md-mic"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-mic-none"><i class="md md-mic-none" title="md-mic-none" alt="md-mic-none"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-mic-off"><i class="md md-mic-off" title="md-mic-off" alt="md-mic-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-movie"><i class="md md-movie" title="md-movie" alt="md-movie"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-my-library-add"><i class="md md-my-library-add" title="md-my-library-add" alt="md-my-library-add"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-my-library-books"><i class="md md-my-library-books" title="md-my-library-books" alt="md-my-library-books"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-my-library-music"><i class="md md-my-library-music" title="md-my-library-music" alt="md-my-library-music"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-new-releases"><i class="md md-new-releases" title="md-new-releases" alt="md-new-releases"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-not-interested"><i class="md md-not-interested" title="md-not-interested" alt="md-not-interested"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-pause"><i class="md md-pause" title="md-pause" alt="md-pause"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-pause-circle-fill"><i class="md md-pause-circle-fill" title="md-pause-circle-fill" alt="md-pause-circle-fill"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-pause-circle-outline"><i class="md md-pause-circle-outline" title="md-pause-circle-outline" alt="md-pause-circle-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-play-arrow"><i class="md md-play-arrow" title="md-play-arrow" alt="md-play-arrow"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-play-circle-fill"><i class="md md-play-circle-fill" title="md-play-circle-fill" alt="md-play-circle-fill"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-play-circle-outline"><i class="md md-play-circle-outline" title="md-play-circle-outline" alt="md-play-circle-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-playlist-add"><i class="md md-playlist-add" title="md-playlist-add" alt="md-playlist-add"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-play-shopping-bag"><i class="md md-play-shopping-bag" title="md-play-shopping-bag" alt="md-play-shopping-bag"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-queue"><i class="md md-queue" title="md-queue" alt="md-queue"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-queue-music"><i class="md md-queue-music" title="md-queue-music" alt="md-queue-music"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-radio"><i class="md md-radio" title="md-radio" alt="md-radio"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-recent-actors"><i class="md md-recent-actors" title="md-recent-actors" alt="md-recent-actors"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-repeat"><i class="md md-repeat" title="md-repeat" alt="md-repeat"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-repeat-one"><i class="md md-repeat-one" title="md-repeat-one" alt="md-repeat-one"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-replay"><i class="md md-replay" title="md-replay" alt="md-replay"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-shuffle"><i class="md md-shuffle" title="md-shuffle" alt="md-shuffle"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-skip-next"><i class="md md-skip-next" title="md-skip-next" alt="md-skip-next"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-skip-previous"><i class="md md-skip-previous" title="md-skip-previous" alt="md-skip-previous"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-snooze"><i class="md md-snooze" title="md-snooze" alt="md-snooze"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-stop"><i class="md md-stop" title="md-stop" alt="md-stop"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-subtitles"><i class="md md-subtitles" title="md-subtitles" alt="md-subtitles"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-surround-sound"><i class="md md-surround-sound" title="md-surround-sound" alt="md-surround-sound"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-videocam"><i class="md md-videocam" title="md-videocam" alt="md-videocam"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-videocam-off"><i class="md md-videocam-off" title="md-videocam-off" alt="md-videocam-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-video-collection"><i class="md md-video-collection" title="md-video-collection" alt="md-video-collection"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-volume-down"><i class="md md-volume-down" title="md-volume-down" alt="md-volume-down"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-volume-mute"><i class="md md-volume-mute" title="md-volume-mute" alt="md-volume-mute"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-volume-off"><i class="md md-volume-off" title="md-volume-off" alt="md-volume-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-volume-up"><i class="md md-volume-up" title="md-volume-up" alt="md-volume-up"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-web"><i class="md md-web" title="md-web" alt="md-web"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Communication</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-business"><i class="md md-business" title="md-business" alt="md-business"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-call"><i class="md md-call" title="md-call" alt="md-call"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-call-end"><i class="md md-call-end" title="md-call-end" alt="md-call-end"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-call-made"><i class="md md-call-made" title="md-call-made" alt="md-call-made"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-call-merge"><i class="md md-call-merge" title="md-call-merge" alt="md-call-merge"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-call-missed"><i class="md md-call-missed" title="md-call-missed" alt="md-call-missed"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-call-received"><i class="md md-call-received" title="md-call-received" alt="md-call-received"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-call-split"><i class="md md-call-split" title="md-call-split" alt="md-call-split"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-chat"><i class="md md-chat" title="md-chat" alt="md-chat"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-clear-all"><i class="md md-clear-all" title="md-clear-all" alt="md-clear-all"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-comment"><i class="md md-comment" title="md-comment" alt="md-comment"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-contacts"><i class="md md-contacts" title="md-contacts" alt="md-contacts"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-dialer-sip"><i class="md md-dialer-sip" title="md-dialer-sip" alt="md-dialer-sip"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-dialpad"><i class="md md-dialpad" title="md-dialpad" alt="md-dialpad"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-dnd-on"><i class="md md-dnd-on" title="md-dnd-on" alt="md-dnd-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-email"><i class="md md-email" title="md-email" alt="md-email"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-forum"><i class="md md-forum" title="md-forum" alt="md-forum"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-import-export"><i class="md md-import-export" title="md-import-export" alt="md-import-export"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-invert-colors-off"><i class="md md-invert-colors-off" title="md-invert-colors-off" alt="md-invert-colors-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-invert-colors-on"><i class="md md-invert-colors-on" title="md-invert-colors-on" alt="md-invert-colors-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-live-help"><i class="md md-live-help" title="md-live-help" alt="md-live-help"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-location-off"><i class="md md-location-off" title="md-location-off" alt="md-location-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-location-on"><i class="md md-location-on" title="md-location-on" alt="md-location-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-message"><i class="md md-message" title="md-message" alt="md-message"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-messenger"><i class="md md-messenger" title="md-messenger" alt="md-messenger"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-no-sim"><i class="md md-no-sim" title="md-no-sim" alt="md-no-sim"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phone"><i class="md md-phone" title="md-phone" alt="md-phone"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-portable-wifi-off"><i class="md md-portable-wifi-off" title="md-portable-wifi-off" alt="md-portable-wifi-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-quick-contacts-dialer"><i class="md md-quick-contacts-dialer" title="md-quick-contacts-dialer" alt="md-quick-contacts-dialer"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-quick-contacts-mail"><i class="md md-quick-contacts-mail" title="md-quick-contacts-mail" alt="md-quick-contacts-mail"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-ring-volume"><i class="md md-ring-volume" title="md-ring-volume" alt="md-ring-volume"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-stay-current-landscape"><i class="md md-stay-current-landscape" title="md-stay-current-landscape" alt="md-stay-current-landscape"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-stay-current-portrait"><i class="md md-stay-current-portrait" title="md-stay-current-portrait" alt="md-stay-current-portrait"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-stay-primary-landscape"><i class="md md-stay-primary-landscape" title="md-stay-primary-landscape" alt="md-stay-primary-landscape"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-stay-primary-portrait"><i class="md md-stay-primary-portrait" title="md-stay-primary-portrait" alt="md-stay-primary-portrait"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-swap-calls"><i class="md md-swap-calls" title="md-swap-calls" alt="md-swap-calls"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-textsms"><i class="md md-textsms" title="md-textsms" alt="md-textsms"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-voicemail"><i class="md md-voicemail" title="md-voicemail" alt="md-voicemail"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-vpn-key"><i class="md md-vpn-key" title="md-vpn-key" alt="md-vpn-key"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Content</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-add"><i class="md md-add" title="md-add" alt="md-add"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-add-box"><i class="md md-add-box" title="md-add-box" alt="md-add-box"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-add-circle"><i class="md md-add-circle" title="md-add-circle" alt="md-add-circle"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-add-circle-outline"><i class="md md-add-circle-outline" title="md-add-circle-outline" alt="md-add-circle-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-archive"><i class="md md-archive" title="md-archive" alt="md-archive"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-backspace"><i class="md md-backspace" title="md-backspace" alt="md-backspace"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-block"><i class="md md-block" title="md-block" alt="md-block"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-clear"><i class="md md-clear" title="md-clear" alt="md-clear"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-content-copy"><i class="md md-content-copy" title="md-content-copy" alt="md-content-copy"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-content-cut"><i class="md md-content-cut" title="md-content-cut" alt="md-content-cut"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-content-paste"><i class="md md-content-paste" title="md-content-paste" alt="md-content-paste"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-create"><i class="md md-create" title="md-create" alt="md-create"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-drafts"><i class="md md-drafts" title="md-drafts" alt="md-drafts"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-list"><i class="md md-filter-list" title="md-filter-list" alt="md-filter-list"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-flag"><i class="md md-flag" title="md-flag" alt="md-flag"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-forward"><i class="md md-forward" title="md-forward" alt="md-forward"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-gesture"><i class="md md-gesture" title="md-gesture" alt="md-gesture"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-inbox"><i class="md md-inbox" title="md-inbox" alt="md-inbox"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-link"><i class="md md-link" title="md-link" alt="md-link"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-mail"><i class="md md-mail" title="md-mail" alt="md-mail"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-markunread"><i class="md md-markunread" title="md-markunread" alt="md-markunread"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-redo"><i class="md md-redo" title="md-redo" alt="md-redo"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-remove"><i class="md md-remove" title="md-remove" alt="md-remove"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-remove-circle"><i class="md md-remove-circle" title="md-remove-circle" alt="md-remove-circle"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-remove-circle-outline"><i class="md md-remove-circle-outline" title="md-remove-circle-outline" alt="md-remove-circle-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-reply"><i class="md md-reply" title="md-reply" alt="md-reply"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-reply-all"><i class="md md-reply-all" title="md-reply-all" alt="md-reply-all"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-report"><i class="md md-report" title="md-report" alt="md-report"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-save"><i class="md md-save" title="md-save" alt="md-save"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-select-all"><i class="md md-select-all" title="md-select-all" alt="md-select-all"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-send"><i class="md md-send" title="md-send" alt="md-send"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sort"><i class="md md-sort" title="md-sort" alt="md-sort"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-text-format"><i class="md md-text-format" title="md-text-format" alt="md-text-format"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-undo"><i class="md md-undo" title="md-undo" alt="md-undo"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Device</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-access-alarm"><i class="md md-access-alarm" title="md-access-alarm" alt="md-access-alarm"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-access-alarms"><i class="md md-access-alarms" title="md-access-alarms" alt="md-access-alarms"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-access-time"><i class="md md-access-time" title="md-access-time" alt="md-access-time"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-add-alarm"><i class="md md-add-alarm" title="md-add-alarm" alt="md-add-alarm"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-airplanemode-off"><i class="md md-airplanemode-off" title="md-airplanemode-off" alt="md-airplanemode-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-airplanemode-on"><i class="md md-airplanemode-on" title="md-airplanemode-on" alt="md-airplanemode-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-20"><i class="md md-battery-20" title="md-battery-20" alt="md-battery-20"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-30"><i class="md md-battery-30" title="md-battery-30" alt="md-battery-30"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-50"><i class="md md-battery-50" title="md-battery-50" alt="md-battery-50"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-60"><i class="md md-battery-60" title="md-battery-60" alt="md-battery-60"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-80"><i class="md md-battery-80" title="md-battery-80" alt="md-battery-80"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-90"><i class="md md-battery-90" title="md-battery-90" alt="md-battery-90"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-alert"><i class="md md-battery-alert" title="md-battery-alert" alt="md-battery-alert"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-charging-20"><i class="md md-battery-charging-20" title="md-battery-charging-20" alt="md-battery-charging-20"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-charging-30"><i class="md md-battery-charging-30" title="md-battery-charging-30" alt="md-battery-charging-30"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-charging-50"><i class="md md-battery-charging-50" title="md-battery-charging-50" alt="md-battery-charging-50"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-charging-60"><i class="md md-battery-charging-60" title="md-battery-charging-60" alt="md-battery-charging-60"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-charging-80"><i class="md md-battery-charging-80" title="md-battery-charging-80" alt="md-battery-charging-80"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-charging-90"><i class="md md-battery-charging-90" title="md-battery-charging-90" alt="md-battery-charging-90"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-charging-full"><i class="md md-battery-charging-full" title="md-battery-charging-full" alt="md-battery-charging-full"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-full"><i class="md md-battery-full" title="md-battery-full" alt="md-battery-full"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-std"><i class="md md-battery-std" title="md-battery-std" alt="md-battery-std"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-battery-unknown"><i class="md md-battery-unknown" title="md-battery-unknown" alt="md-battery-unknown"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-bluetooth"><i class="md md-bluetooth" title="md-bluetooth" alt="md-bluetooth"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-bluetooth-connected"><i class="md md-bluetooth-connected" title="md-bluetooth-connected" alt="md-bluetooth-connected"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-bluetooth-disabled"><i class="md md-bluetooth-disabled" title="md-bluetooth-disabled" alt="md-bluetooth-disabled"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-bluetooth-searching"><i class="md md-bluetooth-searching" title="md-bluetooth-searching" alt="md-bluetooth-searching"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-auto"><i class="md md-brightness-auto" title="md-brightness-auto" alt="md-brightness-auto"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-high"><i class="md md-brightness-high" title="md-brightness-high" alt="md-brightness-high"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-low"><i class="md md-brightness-low" title="md-brightness-low" alt="md-brightness-low"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-medium"><i class="md md-brightness-medium" title="md-brightness-medium" alt="md-brightness-medium"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-data-usage"><i class="md md-data-usage" title="md-data-usage" alt="md-data-usage"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-developer-mode"><i class="md md-developer-mode" title="md-developer-mode" alt="md-developer-mode"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-devices"><i class="md md-devices" title="md-devices" alt="md-devices"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-dvr"><i class="md md-dvr" title="md-dvr" alt="md-dvr"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-gps-fixed"><i class="md md-gps-fixed" title="md-gps-fixed" alt="md-gps-fixed"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-gps-not-fixed"><i class="md md-gps-not-fixed" title="md-gps-not-fixed" alt="md-gps-not-fixed"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-gps-off"><i class="md md-gps-off" title="md-gps-off" alt="md-gps-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-location-disabled"><i class="md md-location-disabled" title="md-location-disabled" alt="md-location-disabled"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-location-searching"><i class="md md-location-searching" title="md-location-searching" alt="md-location-searching"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-multitrack-audio"><i class="md md-multitrack-audio" title="md-multitrack-audio" alt="md-multitrack-audio"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-network-cell"><i class="md md-network-cell" title="md-network-cell" alt="md-network-cell"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-network-wifi"><i class="md md-network-wifi" title="md-network-wifi" alt="md-network-wifi"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-nfc"><i class="md md-nfc" title="md-nfc" alt="md-nfc"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-now-wallpaper"><i class="md md-now-wallpaper" title="md-now-wallpaper" alt="md-now-wallpaper"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-now-widgets"><i class="md md-now-widgets" title="md-now-widgets" alt="md-now-widgets"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-screen-lock-landscape"><i class="md md-screen-lock-landscape" title="md-screen-lock-landscape" alt="md-screen-lock-landscape"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-screen-lock-portrait"><i class="md md-screen-lock-portrait" title="md-screen-lock-portrait" alt="md-screen-lock-portrait"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-screen-lock-rotation"><i class="md md-screen-lock-rotation" title="md-screen-lock-rotation" alt="md-screen-lock-rotation"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-screen-rotation"><i class="md md-screen-rotation" title="md-screen-rotation" alt="md-screen-rotation"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sd-storage"><i class="md md-sd-storage" title="md-sd-storage" alt="md-sd-storage"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-settings-system-daydream"><i class="md md-settings-system-daydream" title="md-settings-system-daydream" alt="md-settings-system-daydream"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-0-bar"><i class="md md-signal-cellular-0-bar" title="md-signal-cellular-0-bar" alt="md-signal-cellular-0-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-1-bar"><i class="md md-signal-cellular-1-bar" title="md-signal-cellular-1-bar" alt="md-signal-cellular-1-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-2-bar"><i class="md md-signal-cellular-2-bar" title="md-signal-cellular-2-bar" alt="md-signal-cellular-2-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-3-bar"><i class="md md-signal-cellular-3-bar" title="md-signal-cellular-3-bar" alt="md-signal-cellular-3-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-4-bar"><i class="md md-signal-cellular-4-bar" title="md-signal-cellular-4-bar" alt="md-signal-cellular-4-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-connected-no-internet-0-bar"><i class="md md-signal-cellular-connected-no-internet-0-bar" title="md-signal-cellular-connected-no-internet-0-bar" alt="md-signal-cellular-connected-no-internet-0-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-connected-no-internet-1-bar"><i class="md md-signal-cellular-connected-no-internet-1-bar" title="md-signal-cellular-connected-no-internet-1-bar" alt="md-signal-cellular-connected-no-internet-1-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-connected-no-internet-2-bar"><i class="md md-signal-cellular-connected-no-internet-2-bar" title="md-signal-cellular-connected-no-internet-2-bar" alt="md-signal-cellular-connected-no-internet-2-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-connected-no-internet-3-bar"><i class="md md-signal-cellular-connected-no-internet-3-bar" title="md-signal-cellular-connected-no-internet-3-bar" alt="md-signal-cellular-connected-no-internet-3-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-connected-no-internet-4-bar"><i class="md md-signal-cellular-connected-no-internet-4-bar" title="md-signal-cellular-connected-no-internet-4-bar" alt="md-signal-cellular-connected-no-internet-4-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-no-sim"><i class="md md-signal-cellular-no-sim" title="md-signal-cellular-no-sim" alt="md-signal-cellular-no-sim"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-null"><i class="md md-signal-cellular-null" title="md-signal-cellular-null" alt="md-signal-cellular-null"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-cellular-off"><i class="md md-signal-cellular-off" title="md-signal-cellular-off" alt="md-signal-cellular-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-wifi-0-bar"><i class="md md-signal-wifi-0-bar" title="md-signal-wifi-0-bar" alt="md-signal-wifi-0-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-wifi-1-bar"><i class="md md-signal-wifi-1-bar" title="md-signal-wifi-1-bar" alt="md-signal-wifi-1-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-wifi-2-bar"><i class="md md-signal-wifi-2-bar" title="md-signal-wifi-2-bar" alt="md-signal-wifi-2-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-wifi-3-bar"><i class="md md-signal-wifi-3-bar" title="md-signal-wifi-3-bar" alt="md-signal-wifi-3-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-wifi-4-bar"><i class="md md-signal-wifi-4-bar" title="md-signal-wifi-4-bar" alt="md-signal-wifi-4-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-signal-wifi-off"><i class="md md-signal-wifi-off" title="md-signal-wifi-off" alt="md-signal-wifi-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-storage"><i class="md md-storage" title="md-storage" alt="md-storage"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-usb"><i class="md md-usb" title="md-usb" alt="md-usb"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wifi-lock"><i class="md md-wifi-lock" title="md-wifi-lock" alt="md-wifi-lock"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wifi-tethering"><i class="md md-wifi-tethering" title="md-wifi-tethering" alt="md-wifi-tethering"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Editor</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-attach-file"><i class="md md-attach-file" title="md-attach-file" alt="md-attach-file"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-attach-money"><i class="md md-attach-money" title="md-attach-money" alt="md-attach-money"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-all"><i class="md md-border-all" title="md-border-all" alt="md-border-all"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-bottom"><i class="md md-border-bottom" title="md-border-bottom" alt="md-border-bottom"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-clear"><i class="md md-border-clear" title="md-border-clear" alt="md-border-clear"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-color"><i class="md md-border-color" title="md-border-color" alt="md-border-color"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-horizontal"><i class="md md-border-horizontal" title="md-border-horizontal" alt="md-border-horizontal"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-inner"><i class="md md-border-inner" title="md-border-inner" alt="md-border-inner"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-left"><i class="md md-border-left" title="md-border-left" alt="md-border-left"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-outer"><i class="md md-border-outer" title="md-border-outer" alt="md-border-outer"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-right"><i class="md md-border-right" title="md-border-right" alt="md-border-right"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-style"><i class="md md-border-style" title="md-border-style" alt="md-border-style"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-top"><i class="md md-border-top" title="md-border-top" alt="md-border-top"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-border-vertical"><i class="md md-border-vertical" title="md-border-vertical" alt="md-border-vertical"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-align-center"><i class="md md-format-align-center" title="md-format-align-center" alt="md-format-align-center"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-align-justify"><i class="md md-format-align-justify" title="md-format-align-justify" alt="md-format-align-justify"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-align-left"><i class="md md-format-align-left" title="md-format-align-left" alt="md-format-align-left"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-align-right"><i class="md md-format-align-right" title="md-format-align-right" alt="md-format-align-right"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-bold"><i class="md md-format-bold" title="md-format-bold" alt="md-format-bold"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-clear"><i class="md md-format-clear" title="md-format-clear" alt="md-format-clear"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-color-fill"><i class="md md-format-color-fill" title="md-format-color-fill" alt="md-format-color-fill"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-color-reset"><i class="md md-format-color-reset" title="md-format-color-reset" alt="md-format-color-reset"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-color-text"><i class="md md-format-color-text" title="md-format-color-text" alt="md-format-color-text"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-indent-decrease"><i class="md md-format-indent-decrease" title="md-format-indent-decrease" alt="md-format-indent-decrease"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-indent-increase"><i class="md md-format-indent-increase" title="md-format-indent-increase" alt="md-format-indent-increase"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-italic"><i class="md md-format-italic" title="md-format-italic" alt="md-format-italic"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-line-spacing"><i class="md md-format-line-spacing" title="md-format-line-spacing" alt="md-format-line-spacing"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-list-bulleted"><i class="md md-format-list-bulleted" title="md-format-list-bulleted" alt="md-format-list-bulleted"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-list-numbered"><i class="md md-format-list-numbered" title="md-format-list-numbered" alt="md-format-list-numbered"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-paint"><i class="md md-format-paint" title="md-format-paint" alt="md-format-paint"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-quote"><i class="md md-format-quote" title="md-format-quote" alt="md-format-quote"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-size"><i class="md md-format-size" title="md-format-size" alt="md-format-size"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-strikethrough"><i class="md md-format-strikethrough" title="md-format-strikethrough" alt="md-format-strikethrough"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-textdirection-l-to-r"><i class="md md-format-textdirection-l-to-r" title="md-format-textdirection-l-to-r" alt="md-format-textdirection-l-to-r"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-textdirection-r-to-l"><i class="md md-format-textdirection-r-to-l" title="md-format-textdirection-r-to-l" alt="md-format-textdirection-r-to-l"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-format-underline"><i class="md md-format-underline" title="md-format-underline" alt="md-format-underline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-functions"><i class="md md-functions" title="md-functions" alt="md-functions"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-insert-chart"><i class="md md-insert-chart" title="md-insert-chart" alt="md-insert-chart"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-insert-comment"><i class="md md-insert-comment" title="md-insert-comment" alt="md-insert-comment"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-insert-drive-file"><i class="md md-insert-drive-file" title="md-insert-drive-file" alt="md-insert-drive-file"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-insert-emoticon"><i class="md md-insert-emoticon" title="md-insert-emoticon" alt="md-insert-emoticon"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-insert-invitation"><i class="md md-insert-invitation" title="md-insert-invitation" alt="md-insert-invitation"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-insert-link"><i class="md md-insert-link" title="md-insert-link" alt="md-insert-link"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-insert-photo"><i class="md md-insert-photo" title="md-insert-photo" alt="md-insert-photo"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-merge-type"><i class="md md-merge-type" title="md-merge-type" alt="md-merge-type"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-mode-comment"><i class="md md-mode-comment" title="md-mode-comment" alt="md-mode-comment"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-mode-edit"><i class="md md-mode-edit" title="md-mode-edit" alt="md-mode-edit"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-publish"><i class="md md-publish" title="md-publish" alt="md-publish"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-vertical-align-bottom"><i class="md md-vertical-align-bottom" title="md-vertical-align-bottom" alt="md-vertical-align-bottom"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-vertical-align-center"><i class="md md-vertical-align-center" title="md-vertical-align-center" alt="md-vertical-align-center"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-vertical-align-top"><i class="md md-vertical-align-top" title="md-vertical-align-top" alt="md-vertical-align-top"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wrap-text"><i class="md md-wrap-text" title="md-wrap-text" alt="md-wrap-text"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>File</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-attachment"><i class="md md-attachment" title="md-attachment" alt="md-attachment"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cloud"><i class="md md-cloud" title="md-cloud" alt="md-cloud"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cloud-circle"><i class="md md-cloud-circle" title="md-cloud-circle" alt="md-cloud-circle"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cloud-done"><i class="md md-cloud-done" title="md-cloud-done" alt="md-cloud-done"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cloud-download"><i class="md md-cloud-download" title="md-cloud-download" alt="md-cloud-download"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cloud-off"><i class="md md-cloud-off" title="md-cloud-off" alt="md-cloud-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cloud-queue"><i class="md md-cloud-queue" title="md-cloud-queue" alt="md-cloud-queue"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cloud-upload"><i class="md md-cloud-upload" title="md-cloud-upload" alt="md-cloud-upload"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-file-download"><i class="md md-file-download" title="md-file-download" alt="md-file-download"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-file-upload"><i class="md md-file-upload" title="md-file-upload" alt="md-file-upload"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-folder"><i class="md md-folder" title="md-folder" alt="md-folder"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-folder-open"><i class="md md-folder-open" title="md-folder-open" alt="md-folder-open"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-folder-shared"><i class="md md-folder-shared" title="md-folder-shared" alt="md-folder-shared"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Hardware</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-cast"><i class="md md-cast" title="md-cast" alt="md-cast"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cast-connected"><i class="md md-cast-connected" title="md-cast-connected" alt="md-cast-connected"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-computer"><i class="md md-computer" title="md-computer" alt="md-computer"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-desktop-mac"><i class="md md-desktop-mac" title="md-desktop-mac" alt="md-desktop-mac"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-desktop-windows"><i class="md md-desktop-windows" title="md-desktop-windows" alt="md-desktop-windows"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-dock"><i class="md md-dock" title="md-dock" alt="md-dock"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-gamepad"><i class="md md-gamepad" title="md-gamepad" alt="md-gamepad"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-headset"><i class="md md-headset" title="md-headset" alt="md-headset"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-headset-mic"><i class="md md-headset-mic" title="md-headset-mic" alt="md-headset-mic"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard"><i class="md md-keyboard" title="md-keyboard" alt="md-keyboard"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-alt"><i class="md md-keyboard-alt" title="md-keyboard-alt" alt="md-keyboard-alt"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-arrow-down"><i class="md md-keyboard-arrow-down" title="md-keyboard-arrow-down" alt="md-keyboard-arrow-down"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-arrow-left"><i class="md md-keyboard-arrow-left" title="md-keyboard-arrow-left" alt="md-keyboard-arrow-left"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-arrow-right"><i class="md md-keyboard-arrow-right" title="md-keyboard-arrow-right" alt="md-keyboard-arrow-right"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-arrow-up"><i class="md md-keyboard-arrow-up" title="md-keyboard-arrow-up" alt="md-keyboard-arrow-up"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-backspace"><i class="md md-keyboard-backspace" title="md-keyboard-backspace" alt="md-keyboard-backspace"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-capslock"><i class="md md-keyboard-capslock" title="md-keyboard-capslock" alt="md-keyboard-capslock"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-control"><i class="md md-keyboard-control" title="md-keyboard-control" alt="md-keyboard-control"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-hide"><i class="md md-keyboard-hide" title="md-keyboard-hide" alt="md-keyboard-hide"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-return"><i class="md md-keyboard-return" title="md-keyboard-return" alt="md-keyboard-return"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-tab"><i class="md md-keyboard-tab" title="md-keyboard-tab" alt="md-keyboard-tab"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-keyboard-voice"><i class="md md-keyboard-voice" title="md-keyboard-voice" alt="md-keyboard-voice"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-laptop"><i class="md md-laptop" title="md-laptop" alt="md-laptop"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-laptop-chromebook"><i class="md md-laptop-chromebook" title="md-laptop-chromebook" alt="md-laptop-chromebook"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-laptop-mac"><i class="md md-laptop-mac" title="md-laptop-mac" alt="md-laptop-mac"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-laptop-windows"><i class="md md-laptop-windows" title="md-laptop-windows" alt="md-laptop-windows"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-memory"><i class="md md-memory" title="md-memory" alt="md-memory"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-mouse"><i class="md md-mouse" title="md-mouse" alt="md-mouse"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phone-android"><i class="md md-phone-android" title="md-phone-android" alt="md-phone-android"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phone-iphone"><i class="md md-phone-iphone" title="md-phone-iphone" alt="md-phone-iphone"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phonelink"><i class="md md-phonelink" title="md-phonelink" alt="md-phonelink"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phonelink-off"><i class="md md-phonelink-off" title="md-phonelink-off" alt="md-phonelink-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-security"><i class="md md-security" title="md-security" alt="md-security"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sim-card"><i class="md md-sim-card" title="md-sim-card" alt="md-sim-card"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-smartphone"><i class="md md-smartphone" title="md-smartphone" alt="md-smartphone"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-speaker"><i class="md md-speaker" title="md-speaker" alt="md-speaker"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tablet"><i class="md md-tablet" title="md-tablet" alt="md-tablet"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tablet-android"><i class="md md-tablet-android" title="md-tablet-android" alt="md-tablet-android"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tablet-mac"><i class="md md-tablet-mac" title="md-tablet-mac" alt="md-tablet-mac"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tv"><i class="md md-tv" title="md-tv" alt="md-tv"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-watch"><i class="md md-watch" title="md-watch" alt="md-watch"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Image</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-add-to-photos"><i class="md md-add-to-photos" title="md-add-to-photos" alt="md-add-to-photos"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-adjust"><i class="md md-adjust" title="md-adjust" alt="md-adjust"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-assistant-photo"><i class="md md-assistant-photo" title="md-assistant-photo" alt="md-assistant-photo"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-audiotrack"><i class="md md-audiotrack" title="md-audiotrack" alt="md-audiotrack"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-blur-circular"><i class="md md-blur-circular" title="md-blur-circular" alt="md-blur-circular"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-blur-linear"><i class="md md-blur-linear" title="md-blur-linear" alt="md-blur-linear"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-blur-off"><i class="md md-blur-off" title="md-blur-off" alt="md-blur-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-blur-on"><i class="md md-blur-on" title="md-blur-on" alt="md-blur-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-1"><i class="md md-brightness-1" title="md-brightness-1" alt="md-brightness-1"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-2"><i class="md md-brightness-2" title="md-brightness-2" alt="md-brightness-2"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-3"><i class="md md-brightness-3" title="md-brightness-3" alt="md-brightness-3"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-4"><i class="md md-brightness-4" title="md-brightness-4" alt="md-brightness-4"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-5"><i class="md md-brightness-5" title="md-brightness-5" alt="md-brightness-5"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-6"><i class="md md-brightness-6" title="md-brightness-6" alt="md-brightness-6"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brightness-7"><i class="md md-brightness-7" title="md-brightness-7" alt="md-brightness-7"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-brush"><i class="md md-brush" title="md-brush" alt="md-brush"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-camera"><i class="md md-camera" title="md-camera" alt="md-camera"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-camera-alt"><i class="md md-camera-alt" title="md-camera-alt" alt="md-camera-alt"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-camera-front"><i class="md md-camera-front" title="md-camera-front" alt="md-camera-front"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-camera-rear"><i class="md md-camera-rear" title="md-camera-rear" alt="md-camera-rear"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-camera-roll"><i class="md md-camera-roll" title="md-camera-roll" alt="md-camera-roll"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-center-focus-strong"><i class="md md-center-focus-strong" title="md-center-focus-strong" alt="md-center-focus-strong"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-center-focus-weak"><i class="md md-center-focus-weak" title="md-center-focus-weak" alt="md-center-focus-weak"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-collections"><i class="md md-collections" title="md-collections" alt="md-collections"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-colorize"><i class="md md-colorize" title="md-colorize" alt="md-colorize"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-color-lens"><i class="md md-color-lens" title="md-color-lens" alt="md-color-lens"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-compare"><i class="md md-compare" title="md-compare" alt="md-compare"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-control-point"><i class="md md-control-point" title="md-control-point" alt="md-control-point"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-control-point-duplicate"><i class="md md-control-point-duplicate" title="md-control-point-duplicate" alt="md-control-point-duplicate"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop"><i class="md md-crop" title="md-crop" alt="md-crop"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-3-2"><i class="md md-crop-3-2" title="md-crop-3-2" alt="md-crop-3-2"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-5-4"><i class="md md-crop-5-4" title="md-crop-5-4" alt="md-crop-5-4"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-7-5"><i class="md md-crop-7-5" title="md-crop-7-5" alt="md-crop-7-5"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-16-9"><i class="md md-crop-16-9" title="md-crop-16-9" alt="md-crop-16-9"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-din"><i class="md md-crop-din" title="md-crop-din" alt="md-crop-din"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-free"><i class="md md-crop-free" title="md-crop-free" alt="md-crop-free"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-landscape"><i class="md md-crop-landscape" title="md-crop-landscape" alt="md-crop-landscape"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-original"><i class="md md-crop-original" title="md-crop-original" alt="md-crop-original"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-portrait"><i class="md md-crop-portrait" title="md-crop-portrait" alt="md-crop-portrait"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-crop-square"><i class="md md-crop-square" title="md-crop-square" alt="md-crop-square"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-dehaze"><i class="md md-dehaze" title="md-dehaze" alt="md-dehaze"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-details"><i class="md md-details" title="md-details" alt="md-details"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-edit"><i class="md md-edit" title="md-edit" alt="md-edit"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-exposure"><i class="md md-exposure" title="md-exposure" alt="md-exposure"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-exposure-minus-1"><i class="md md-exposure-minus-1" title="md-exposure-minus-1" alt="md-exposure-minus-1"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-exposure-minus-2"><i class="md md-exposure-minus-2" title="md-exposure-minus-2" alt="md-exposure-minus-2"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-exposure-zero"><i class="md md-exposure-zero" title="md-exposure-zero" alt="md-exposure-zero"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-exposure-plus-1"><i class="md md-exposure-plus-1" title="md-exposure-plus-1" alt="md-exposure-plus-1"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-exposure-plus-2"><i class="md md-exposure-plus-2" title="md-exposure-plus-2" alt="md-exposure-plus-2"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter"><i class="md md-filter" title="md-filter" alt="md-filter"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-1"><i class="md md-filter-1" title="md-filter-1" alt="md-filter-1"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-2"><i class="md md-filter-2" title="md-filter-2" alt="md-filter-2"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-3"><i class="md md-filter-3" title="md-filter-3" alt="md-filter-3"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-4"><i class="md md-filter-4" title="md-filter-4" alt="md-filter-4"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-5"><i class="md md-filter-5" title="md-filter-5" alt="md-filter-5"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-6"><i class="md md-filter-6" title="md-filter-6" alt="md-filter-6"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-7"><i class="md md-filter-7" title="md-filter-7" alt="md-filter-7"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-8"><i class="md md-filter-8" title="md-filter-8" alt="md-filter-8"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-9"><i class="md md-filter-9" title="md-filter-9" alt="md-filter-9"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-9-plus"><i class="md md-filter-9-plus" title="md-filter-9-plus" alt="md-filter-9-plus"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-b-and-w"><i class="md md-filter-b-and-w" title="md-filter-b-and-w" alt="md-filter-b-and-w"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-center-focus"><i class="md md-filter-center-focus" title="md-filter-center-focus" alt="md-filter-center-focus"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-drama"><i class="md md-filter-drama" title="md-filter-drama" alt="md-filter-drama"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-frames"><i class="md md-filter-frames" title="md-filter-frames" alt="md-filter-frames"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-hdr"><i class="md md-filter-hdr" title="md-filter-hdr" alt="md-filter-hdr"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-none"><i class="md md-filter-none" title="md-filter-none" alt="md-filter-none"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-tilt-shift"><i class="md md-filter-tilt-shift" title="md-filter-tilt-shift" alt="md-filter-tilt-shift"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-filter-vintage"><i class="md md-filter-vintage" title="md-filter-vintage" alt="md-filter-vintage"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-flare"><i class="md md-flare" title="md-flare" alt="md-flare"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-flash-auto"><i class="md md-flash-auto" title="md-flash-auto" alt="md-flash-auto"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-flash-off"><i class="md md-flash-off" title="md-flash-off" alt="md-flash-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-flash-on"><i class="md md-flash-on" title="md-flash-on" alt="md-flash-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-flip"><i class="md md-flip" title="md-flip" alt="md-flip"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-gradient"><i class="md md-gradient" title="md-gradient" alt="md-gradient"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-grain"><i class="md md-grain" title="md-grain" alt="md-grain"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-grid-off"><i class="md md-grid-off" title="md-grid-off" alt="md-grid-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-grid-on"><i class="md md-grid-on" title="md-grid-on" alt="md-grid-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-hdr-off"><i class="md md-hdr-off" title="md-hdr-off" alt="md-hdr-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-hdr-on"><i class="md md-hdr-on" title="md-hdr-on" alt="md-hdr-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-hdr-strong"><i class="md md-hdr-strong" title="md-hdr-strong" alt="md-hdr-strong"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-hdr-weak"><i class="md md-hdr-weak" title="md-hdr-weak" alt="md-hdr-weak"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-healing"><i class="md md-healing" title="md-healing" alt="md-healing"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-image"><i class="md md-image" title="md-image" alt="md-image"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-image-aspect-ratio"><i class="md md-image-aspect-ratio" title="md-image-aspect-ratio" alt="md-image-aspect-ratio"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-iso"><i class="md md-iso" title="md-iso" alt="md-iso"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-landscape"><i class="md md-landscape" title="md-landscape" alt="md-landscape"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-leak-add"><i class="md md-leak-add" title="md-leak-add" alt="md-leak-add"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-leak-remove"><i class="md md-leak-remove" title="md-leak-remove" alt="md-leak-remove"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-lens"><i class="md md-lens" title="md-lens" alt="md-lens"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-looks"><i class="md md-looks" title="md-looks" alt="md-looks"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-looks-1"><i class="md md-looks-1" title="md-looks-1" alt="md-looks-1"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-looks-2"><i class="md md-looks-2" title="md-looks-2" alt="md-looks-2"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-looks-3"><i class="md md-looks-3" title="md-looks-3" alt="md-looks-3"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-looks-4"><i class="md md-looks-4" title="md-looks-4" alt="md-looks-4"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-looks-5"><i class="md md-looks-5" title="md-looks-5" alt="md-looks-5"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-looks-6"><i class="md md-looks-6" title="md-looks-6" alt="md-looks-6"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-loupe"><i class="md md-loupe" title="md-loupe" alt="md-loupe"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-movie-creation"><i class="md md-movie-creation" title="md-movie-creation" alt="md-movie-creation"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-nature"><i class="md md-nature" title="md-nature" alt="md-nature"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-nature-people"><i class="md md-nature-people" title="md-nature-people" alt="md-nature-people"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-navigate-before"><i class="md md-navigate-before" title="md-navigate-before" alt="md-navigate-before"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-navigate-next"><i class="md md-navigate-next" title="md-navigate-next" alt="md-navigate-next"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-palette"><i class="md md-palette" title="md-palette" alt="md-palette"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-panorama"><i class="md md-panorama" title="md-panorama" alt="md-panorama"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-panorama-fisheye"><i class="md md-panorama-fisheye" title="md-panorama-fisheye" alt="md-panorama-fisheye"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-panorama-horizontal"><i class="md md-panorama-horizontal" title="md-panorama-horizontal" alt="md-panorama-horizontal"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-panorama-vertical"><i class="md md-panorama-vertical" title="md-panorama-vertical" alt="md-panorama-vertical"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-panorama-wide-angle"><i class="md md-panorama-wide-angle" title="md-panorama-wide-angle" alt="md-panorama-wide-angle"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-photo"><i class="md md-photo" title="md-photo" alt="md-photo"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-photo-album"><i class="md md-photo-album" title="md-photo-album" alt="md-photo-album"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-photo-camera"><i class="md md-photo-camera" title="md-photo-camera" alt="md-photo-camera"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-photo-library"><i class="md md-photo-library" title="md-photo-library" alt="md-photo-library"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-portrait"><i class="md md-portrait" title="md-portrait" alt="md-portrait"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-remove-red-eye"><i class="md md-remove-red-eye" title="md-remove-red-eye" alt="md-remove-red-eye"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-rotate-left"><i class="md md-rotate-left" title="md-rotate-left" alt="md-rotate-left"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-rotate-right"><i class="md md-rotate-right" title="md-rotate-right" alt="md-rotate-right"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-slideshow"><i class="md md-slideshow" title="md-slideshow" alt="md-slideshow"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-straighten"><i class="md md-straighten" title="md-straighten" alt="md-straighten"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-style"><i class="md md-style" title="md-style" alt="md-style"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-switch-camera"><i class="md md-switch-camera" title="md-switch-camera" alt="md-switch-camera"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-switch-video"><i class="md md-switch-video" title="md-switch-video" alt="md-switch-video"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tag-faces"><i class="md md-tag-faces" title="md-tag-faces" alt="md-tag-faces"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-texture"><i class="md md-texture" title="md-texture" alt="md-texture"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-timelapse"><i class="md md-timelapse" title="md-timelapse" alt="md-timelapse"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-timer"><i class="md md-timer" title="md-timer" alt="md-timer"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-timer-3"><i class="md md-timer-3" title="md-timer-3" alt="md-timer-3"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-timer-10"><i class="md md-timer-10" title="md-timer-10" alt="md-timer-10"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-timer-auto"><i class="md md-timer-auto" title="md-timer-auto" alt="md-timer-auto"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-timer-off"><i class="md md-timer-off" title="md-timer-off" alt="md-timer-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tonality"><i class="md md-tonality" title="md-tonality" alt="md-tonality"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-transform"><i class="md md-transform" title="md-transform" alt="md-transform"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tune"><i class="md md-tune" title="md-tune" alt="md-tune"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wb-auto"><i class="md md-wb-auto" title="md-wb-auto" alt="md-wb-auto"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wb-cloudy"><i class="md md-wb-cloudy" title="md-wb-cloudy" alt="md-wb-cloudy"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wb-incandescent"><i class="md md-wb-incandescent" title="md-wb-incandescent" alt="md-wb-incandescent"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wb-irradescent"><i class="md md-wb-irradescent" title="md-wb-irradescent" alt="md-wb-irradescent"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-wb-sunny"><i class="md md-wb-sunny" title="md-wb-sunny" alt="md-wb-sunny"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Maps</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-beenhere"><i class="md md-beenhere" title="md-beenhere" alt="md-beenhere"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-directions"><i class="md md-directions" title="md-directions" alt="md-directions"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-directions-bike"><i class="md md-directions-bike" title="md-directions-bike" alt="md-directions-bike"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-directions-bus"><i class="md md-directions-bus" title="md-directions-bus" alt="md-directions-bus"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-directions-car"><i class="md md-directions-car" title="md-directions-car" alt="md-directions-car"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-directions-ferry"><i class="md md-directions-ferry" title="md-directions-ferry" alt="md-directions-ferry"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-directions-subway"><i class="md md-directions-subway" title="md-directions-subway" alt="md-directions-subway"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-directions-train"><i class="md md-directions-train" title="md-directions-train" alt="md-directions-train"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-directions-transit"><i class="md md-directions-transit" title="md-directions-transit" alt="md-directions-transit"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-directions-walk"><i class="md md-directions-walk" title="md-directions-walk" alt="md-directions-walk"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-flight"><i class="md md-flight" title="md-flight" alt="md-flight"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-hotel"><i class="md md-hotel" title="md-hotel" alt="md-hotel"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-layers"><i class="md md-layers" title="md-layers" alt="md-layers"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-layers-clear"><i class="md md-layers-clear" title="md-layers-clear" alt="md-layers-clear"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-airport"><i class="md md-local-airport" title="md-local-airport" alt="md-local-airport"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-atm"><i class="md md-local-atm" title="md-local-atm" alt="md-local-atm"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-attraction"><i class="md md-local-attraction" title="md-local-attraction" alt="md-local-attraction"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-bar"><i class="md md-local-bar" title="md-local-bar" alt="md-local-bar"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-cafe"><i class="md md-local-cafe" title="md-local-cafe" alt="md-local-cafe"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-car-wash"><i class="md md-local-car-wash" title="md-local-car-wash" alt="md-local-car-wash"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-convenience-store"><i class="md md-local-convenience-store" title="md-local-convenience-store" alt="md-local-convenience-store"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-drink"><i class="md md-local-drink" title="md-local-drink" alt="md-local-drink"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-florist"><i class="md md-local-florist" title="md-local-florist" alt="md-local-florist"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-gas-station"><i class="md md-local-gas-station" title="md-local-gas-station" alt="md-local-gas-station"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-grocery-store"><i class="md md-local-grocery-store" title="md-local-grocery-store" alt="md-local-grocery-store"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-hospital"><i class="md md-local-hospital" title="md-local-hospital" alt="md-local-hospital"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-hotel"><i class="md md-local-hotel" title="md-local-hotel" alt="md-local-hotel"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-laundry-service"><i class="md md-local-laundry-service" title="md-local-laundry-service" alt="md-local-laundry-service"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-library"><i class="md md-local-library" title="md-local-library" alt="md-local-library"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-mall"><i class="md md-local-mall" title="md-local-mall" alt="md-local-mall"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-movies"><i class="md md-local-movies" title="md-local-movies" alt="md-local-movies"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-offer"><i class="md md-local-offer" title="md-local-offer" alt="md-local-offer"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-parking"><i class="md md-local-parking" title="md-local-parking" alt="md-local-parking"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-pharmacy"><i class="md md-local-pharmacy" title="md-local-pharmacy" alt="md-local-pharmacy"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-phone"><i class="md md-local-phone" title="md-local-phone" alt="md-local-phone"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-pizza"><i class="md md-local-pizza" title="md-local-pizza" alt="md-local-pizza"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-play"><i class="md md-local-play" title="md-local-play" alt="md-local-play"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-post-office"><i class="md md-local-post-office" title="md-local-post-office" alt="md-local-post-office"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-print-shop"><i class="md md-local-print-shop" title="md-local-print-shop" alt="md-local-print-shop"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-restaurant"><i class="md md-local-restaurant" title="md-local-restaurant" alt="md-local-restaurant"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-see"><i class="md md-local-see" title="md-local-see" alt="md-local-see"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-shipping"><i class="md md-local-shipping" title="md-local-shipping" alt="md-local-shipping"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-local-taxi"><i class="md md-local-taxi" title="md-local-taxi" alt="md-local-taxi"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-location-history"><i class="md md-location-history" title="md-location-history" alt="md-location-history"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-map"><i class="md md-map" title="md-map" alt="md-map"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-my-location"><i class="md md-my-location" title="md-my-location" alt="md-my-location"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-navigation"><i class="md md-navigation" title="md-navigation" alt="md-navigation"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-pin-drop"><i class="md md-pin-drop" title="md-pin-drop" alt="md-pin-drop"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-place"><i class="md md-place" title="md-place" alt="md-place"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-rate-review"><i class="md md-rate-review" title="md-rate-review" alt="md-rate-review"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-restaurant-menu"><i class="md md-restaurant-menu" title="md-restaurant-menu" alt="md-restaurant-menu"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-satellite"><i class="md md-satellite" title="md-satellite" alt="md-satellite"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-store-mall-directory"><i class="md md-store-mall-directory" title="md-store-mall-directory" alt="md-store-mall-directory"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-terrain"><i class="md md-terrain" title="md-terrain" alt="md-terrain"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-traffic"><i class="md md-traffic" title="md-traffic" alt="md-traffic"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Navigation</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-apps"><i class="md md-apps" title="md-apps" alt="md-apps"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-cancel"><i class="md md-cancel" title="md-cancel" alt="md-cancel"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-arrow-drop-down-circle"><i class="md md-arrow-drop-down-circle" title="md-arrow-drop-down-circle" alt="md-arrow-drop-down-circle"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-arrow-drop-down"><i class="md md-arrow-drop-down" title="md-arrow-drop-down" alt="md-arrow-drop-down"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-arrow-drop-up"><i class="md md-arrow-drop-up" title="md-arrow-drop-up" alt="md-arrow-drop-up"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-arrow-back"><i class="md md-arrow-back" title="md-arrow-back" alt="md-arrow-back"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-arrow-forward"><i class="md md-arrow-forward" title="md-arrow-forward" alt="md-arrow-forward"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-check"><i class="md md-check" title="md-check" alt="md-check"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-close"><i class="md md-close" title="md-close" alt="md-close"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-chevron-left"><i class="md md-chevron-left" title="md-chevron-left" alt="md-chevron-left"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-chevron-right"><i class="md md-chevron-right" title="md-chevron-right" alt="md-chevron-right"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-expand-less"><i class="md md-expand-less" title="md-expand-less" alt="md-expand-less"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-expand-more"><i class="md md-expand-more" title="md-expand-more" alt="md-expand-more"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-fullscreen"><i class="md md-fullscreen" title="md-fullscreen" alt="md-fullscreen"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-fullscreen-exit"><i class="md md-fullscreen-exit" title="md-fullscreen-exit" alt="md-fullscreen-exit"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-menu"><i class="md md-menu" title="md-menu" alt="md-menu"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-more-horiz"><i class="md md-more-horiz" title="md-more-horiz" alt="md-more-horiz"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-more-vert"><i class="md md-more-vert" title="md-more-vert" alt="md-more-vert"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-refresh"><i class="md md-refresh" title="md-refresh" alt="md-refresh"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-unfold-less"><i class="md md-unfold-less" title="md-unfold-less" alt="md-unfold-less"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-unfold-more"><i class="md md-unfold-more" title="md-unfold-more" alt="md-unfold-more"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Notification</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-adb"><i class="md md-adb" title="md-adb" alt="md-adb"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-bluetooth-audio"><i class="md md-bluetooth-audio" title="md-bluetooth-audio" alt="md-bluetooth-audio"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-disc-full"><i class="md md-disc-full" title="md-disc-full" alt="md-disc-full"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-dnd-forwardslash"><i class="md md-dnd-forwardslash" title="md-dnd-forwardslash" alt="md-dnd-forwardslash"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-do-not-disturb"><i class="md md-do-not-disturb" title="md-do-not-disturb" alt="md-do-not-disturb"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-drive-eta"><i class="md md-drive-eta" title="md-drive-eta" alt="md-drive-eta"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-event-available"><i class="md md-event-available" title="md-event-available" alt="md-event-available"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-event-busy"><i class="md md-event-busy" title="md-event-busy" alt="md-event-busy"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-event-note"><i class="md md-event-note" title="md-event-note" alt="md-event-note"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-folder-special"><i class="md md-folder-special" title="md-folder-special" alt="md-folder-special"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-mms"><i class="md md-mms" title="md-mms" alt="md-mms"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-more"><i class="md md-more" title="md-more" alt="md-more"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-network-locked"><i class="md md-network-locked" title="md-network-locked" alt="md-network-locked"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phone-bluetooth-speaker"><i class="md md-phone-bluetooth-speaker" title="md-phone-bluetooth-speaker" alt="md-phone-bluetooth-speaker"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phone-forwarded"><i class="md md-phone-forwarded" title="md-phone-forwarded" alt="md-phone-forwarded"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phone-in-talk"><i class="md md-phone-in-talk" title="md-phone-in-talk" alt="md-phone-in-talk"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phone-locked"><i class="md md-phone-locked" title="md-phone-locked" alt="md-phone-locked"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phone-missed"><i class="md md-phone-missed" title="md-phone-missed" alt="md-phone-missed"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-phone-paused"><i class="md md-phone-paused" title="md-phone-paused" alt="md-phone-paused"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-play-download"><i class="md md-play-download" title="md-play-download" alt="md-play-download"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-play-install"><i class="md md-play-install" title="md-play-install" alt="md-play-install"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sd-card"><i class="md md-sd-card" title="md-sd-card" alt="md-sd-card"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sim-card-alert"><i class="md md-sim-card-alert" title="md-sim-card-alert" alt="md-sim-card-alert"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sms"><i class="md md-sms" title="md-sms" alt="md-sms"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sms-failed"><i class="md md-sms-failed" title="md-sms-failed" alt="md-sms-failed"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sync"><i class="md md-sync" title="md-sync" alt="md-sync"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sync-disabled"><i class="md md-sync-disabled" title="md-sync-disabled" alt="md-sync-disabled"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-sync-problem"><i class="md md-sync-problem" title="md-sync-problem" alt="md-sync-problem"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-system-update"><i class="md md-system-update" title="md-system-update" alt="md-system-update"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-tap-and-play"><i class="md md-tap-and-play" title="md-tap-and-play" alt="md-tap-and-play"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-time-to-leave"><i class="md md-time-to-leave" title="md-time-to-leave" alt="md-time-to-leave"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-vibration"><i class="md md-vibration" title="md-vibration" alt="md-vibration"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-voice-chat"><i class="md md-voice-chat" title="md-voice-chat" alt="md-voice-chat"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-vpn-lock"><i class="md md-vpn-lock" title="md-vpn-lock" alt="md-vpn-lock"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Social</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-cake"><i class="md md-cake" title="md-cake" alt="md-cake"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-domain"><i class="md md-domain" title="md-domain" alt="md-domain"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-location-city"><i class="md md-location-city" title="md-location-city" alt="md-location-city"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-mood"><i class="md md-mood" title="md-mood" alt="md-mood"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-notifications-none"><i class="md md-notifications-none" title="md-notifications-none" alt="md-notifications-none"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-notifications"><i class="md md-notifications" title="md-notifications" alt="md-notifications"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-notifications-off"><i class="md md-notifications-off" title="md-notifications-off" alt="md-notifications-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-notifications-on"><i class="md md-notifications-on" title="md-notifications-on" alt="md-notifications-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-notifications-paused"><i class="md md-notifications-paused" title="md-notifications-paused" alt="md-notifications-paused"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-pages"><i class="md md-pages" title="md-pages" alt="md-pages"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-party-mode"><i class="md md-party-mode" title="md-party-mode" alt="md-party-mode"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-group"><i class="md md-group" title="md-group" alt="md-group"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-group-add"><i class="md md-group-add" title="md-group-add" alt="md-group-add"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-people"><i class="md md-people" title="md-people" alt="md-people"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-people-outline"><i class="md md-people-outline" title="md-people-outline" alt="md-people-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-person"><i class="md md-person" title="md-person" alt="md-person"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-person-add"><i class="md md-person-add" title="md-person-add" alt="md-person-add"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-person-outline"><i class="md md-person-outline" title="md-person-outline" alt="md-person-outline"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-plus-one"><i class="md md-plus-one" title="md-plus-one" alt="md-plus-one"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-poll"><i class="md md-poll" title="md-poll" alt="md-poll"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-public"><i class="md md-public" title="md-public" alt="md-public"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-school"><i class="md md-school" title="md-school" alt="md-school"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-share"><i class="md md-share" title="md-share" alt="md-share"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-whatshot"><i class="md md-whatshot" title="md-whatshot" alt="md-whatshot"></i></a></div>
                                            </div>
                                        </section>
                                        <section>
                                            <h4 class="page-header header-title"><b>Toggle</b></h4>
                                            <div class="clearfix">
                                                <div class="icon"><a href="#" data-icon="md md-check-box"><i class="md md-check-box" title="md-check-box" alt="md-check-box"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-check-box-outline-blank"><i class="md md-check-box-outline-blank" title="md-check-box-outline-blank" alt="md-check-box-outline-blank"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-radio-button-off"><i class="md md-radio-button-off" title="md-radio-button-off" alt="md-radio-button-off"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-radio-button-on"><i class="md md-radio-button-on" title="md-radio-button-on" alt="md-radio-button-on"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-star"><i class="md md-star" title="md-star" alt="md-star"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-star-half"><i class="md md-star-half" title="md-star-half" alt="md-star-half"></i></a></div>
                                                <div class="icon"><a href="#" data-icon="md md-star-outline"><i class="md md-star-outline" title="md-star-outline" alt="md-star-outline"></i></a></div>
                                            </div>
                                        </section>
                                    </div>
					  </div>
					</div>
				  </div>
				  <div class="hidden panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#icons" href="#ionicons">
						Ion Icons</a>
					  </h4>
					</div>
					<div id="ionicons" class="panel-collapse collapse">
					  <div class="panel-body">
							<div class="card-box">

                                        <div class="row">

                                            <div class="icon"><a href="#" data-icon="ion-ionic"><i class="ion-ionic" title="ion-ionic" alt="ion-ionic"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-up-a"><i class="ion-arrow-up-a" title="ion-arrow-up-a" alt="ion-arrow-up-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-right-a"><i class="ion-arrow-right-a" title="ion-arrow-right-a" alt="ion-arrow-right-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-down-a"><i class="ion-arrow-down-a" title="ion-arrow-down-a" alt="ion-arrow-down-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-left-a"><i class="ion-arrow-left-a" title="ion-arrow-left-a" alt="ion-arrow-left-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-up-b"><i class="ion-arrow-up-b" title="ion-arrow-up-b" alt="ion-arrow-up-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-right-b"><i class="ion-arrow-right-b" title="ion-arrow-right-b" alt="ion-arrow-right-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-down-b"><i class="ion-arrow-down-b" title="ion-arrow-down-b" alt="ion-arrow-down-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-left-b"><i class="ion-arrow-left-b" title="ion-arrow-left-b" alt="ion-arrow-left-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-up-c"><i class="ion-arrow-up-c" title="ion-arrow-up-c" alt="ion-arrow-up-c"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-right-c"><i class="ion-arrow-right-c" title="ion-arrow-right-c" alt="ion-arrow-right-c"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-down-c"><i class="ion-arrow-down-c" title="ion-arrow-down-c" alt="ion-arrow-down-c"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-left-c"><i class="ion-arrow-left-c" title="ion-arrow-left-c" alt="ion-arrow-left-c"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-return-right"><i class="ion-arrow-return-right" title="ion-arrow-return-right" alt="ion-arrow-return-right"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-return-left"><i class="ion-arrow-return-left" title="ion-arrow-return-left" alt="ion-arrow-return-left"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-swap"><i class="ion-arrow-swap" title="ion-arrow-swap" alt="ion-arrow-swap"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-shrink"><i class="ion-arrow-shrink" title="ion-arrow-shrink" alt="ion-arrow-shrink"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-expand"><i class="ion-arrow-expand" title="ion-arrow-expand" alt="ion-arrow-expand"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-move"><i class="ion-arrow-move" title="ion-arrow-move" alt="ion-arrow-move"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-resize"><i class="ion-arrow-resize" title="ion-arrow-resize" alt="ion-arrow-resize"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chevron-up"><i class="ion-chevron-up" title="ion-chevron-up" alt="ion-chevron-up"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chevron-right"><i class="ion-chevron-right" title="ion-chevron-right" alt="ion-chevron-right"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chevron-down"><i class="ion-chevron-down" title="ion-chevron-down" alt="ion-chevron-down"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chevron-left"><i class="ion-chevron-left" title="ion-chevron-left" alt="ion-chevron-left"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-navicon-round"><i class="ion-navicon-round" title="ion-navicon-round" alt="ion-navicon-round"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-navicon"><i class="ion-navicon" title="ion-navicon" alt="ion-navicon"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-drag"><i class="ion-drag" title="ion-drag" alt="ion-drag"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-log-in"><i class="ion-log-in" title="ion-log-in" alt="ion-log-in"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-log-out"><i class="ion-log-out" title="ion-log-out" alt="ion-log-out"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-checkmark-round"><i class="ion-checkmark-round" title="ion-checkmark-round" alt="ion-checkmark-round"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-checkmark"><i class="ion-checkmark" title="ion-checkmark" alt="ion-checkmark"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-checkmark-circled"><i class="ion-checkmark-circled" title="ion-checkmark-circled" alt="ion-checkmark-circled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-close-round"><i class="ion-close-round" title="ion-close-round" alt="ion-close-round"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-close"><i class="ion-close" title="ion-close" alt="ion-close"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-close-circled"><i class="ion-close-circled" title="ion-close-circled" alt="ion-close-circled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-plus-round"><i class="ion-plus-round" title="ion-plus-round" alt="ion-plus-round"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-plus"><i class="ion-plus" title="ion-plus" alt="ion-plus"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-plus-circled"><i class="ion-plus-circled" title="ion-plus-circled" alt="ion-plus-circled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-minus-round"><i class="ion-minus-round" title="ion-minus-round" alt="ion-minus-round"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-minus"><i class="ion-minus" title="ion-minus" alt="ion-minus"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-minus-circled"><i class="ion-minus-circled" title="ion-minus-circled" alt="ion-minus-circled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-information"><i class="ion-information" title="ion-information" alt="ion-information"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-information-circled"><i class="ion-information-circled" title="ion-information-circled" alt="ion-information-circled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-help"><i class="ion-help" title="ion-help" alt="ion-help"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-help-circled"><i class="ion-help-circled" title="ion-help-circled" alt="ion-help-circled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-help-buoy"><i class="ion-help-buoy" title="ion-help-buoy" alt="ion-help-buoy"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-asterisk"><i class="ion-asterisk" title="ion-asterisk" alt="ion-asterisk"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-alert"><i class="ion-alert" title="ion-alert" alt="ion-alert"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-alert-circled"><i class="ion-alert-circled" title="ion-alert-circled" alt="ion-alert-circled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-refresh"><i class="ion-refresh" title="ion-refresh" alt="ion-refresh"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-loop"><i class="ion-loop" title="ion-loop" alt="ion-loop"></i></a></div>

<div class="icon"><a href="#" data-icon="ion-shuffle"><i class="ion-shuffle" title="ion-shuffle" alt="ion-shuffle"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-home"><i class="ion-home" title="ion-home" alt="ion-home"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-search"><i class="ion-search" title="ion-search" alt="ion-search"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-flag"><i class="ion-flag" title="ion-flag" alt="ion-flag"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-star"><i class="ion-star" title="ion-star" alt="ion-star"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-heart"><i class="ion-heart" title="ion-heart" alt="ion-heart"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-heart-broken"><i class="ion-heart-broken" title="ion-heart-broken" alt="ion-heart-broken"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-gear-a"><i class="ion-gear-a" title="ion-gear-a" alt="ion-gear-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-gear-b"><i class="ion-gear-b" title="ion-gear-b" alt="ion-gear-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-toggle-filled"><i class="ion-toggle-filled" title="ion-toggle-filled" alt="ion-toggle-filled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-toggle"><i class="ion-toggle" title="ion-toggle" alt="ion-toggle"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-settings"><i class="ion-settings" title="ion-settings" alt="ion-settings"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-wrench"><i class="ion-wrench" title="ion-wrench" alt="ion-wrench"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-folder"><i class="ion-folder" title="ion-folder" alt="ion-folder"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-hammer"><i class="ion-hammer" title="ion-hammer" alt="ion-hammer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-edit"><i class="ion-edit" title="ion-edit" alt="ion-edit"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-trash-a"><i class="ion-trash-a" title="ion-trash-a" alt="ion-trash-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-trash-b"><i class="ion-trash-b" title="ion-trash-b" alt="ion-trash-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-document"><i class="ion-document" title="ion-document" alt="ion-document"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-document-text"><i class="ion-document-text" title="ion-document-text" alt="ion-document-text"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-clipboard"><i class="ion-clipboard" title="ion-clipboard" alt="ion-clipboard"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-scissors"><i class="ion-scissors" title="ion-scissors" alt="ion-scissors"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-funnel"><i class="ion-funnel" title="ion-funnel" alt="ion-funnel"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-bookmark"><i class="ion-bookmark" title="ion-bookmark" alt="ion-bookmark"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-email"><i class="ion-email" title="ion-email" alt="ion-email"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-folder"><i class="ion-folder" title="ion-folder" alt="ion-folder"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-filing"><i class="ion-filing" title="ion-filing" alt="ion-filing"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-archive"><i class="ion-archive" title="ion-archive" alt="ion-archive"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-reply"><i class="ion-reply" title="ion-reply" alt="ion-reply"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-reply-all"><i class="ion-reply-all" title="ion-reply-all" alt="ion-reply-all"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-forward"><i class="ion-forward" title="ion-forward" alt="ion-forward"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-share"><i class="ion-share" title="ion-share" alt="ion-share"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-paper-airplane"><i class="ion-paper-airplane" title="ion-paper-airplane" alt="ion-paper-airplane"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-link"><i class="ion-link" title="ion-link" alt="ion-link"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-paperclip"><i class="ion-paperclip" title="ion paperclip" alt="ion paperclip"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-compose"><i class="ion-compose" title="ion-compose" alt="ion-compose"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-briefcase"><i class="ion-briefcase" title="ion-briefcase" alt="ion-briefcase"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-medkit"><i class="ion-medkit" title="ion-medkit" alt="ion-medkit"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-at"><i class="ion-at" title="ion-at" alt="ion-at"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-pound"><i class="ion-pound" title="ion-pound" alt="ion-pound"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-quote"><i class="ion-quote" title="ion-quote" alt="ion-quote"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-cloud"><i class="ion-cloud" title="ion-cloud" alt="ion-cloud"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-upload"><i class="ion-upload" title="ion-upload" alt="ion-upload"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-more"><i class="ion-more" title="ion-more" alt="ion-more"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-grid"><i class="ion-grid" title="ion-grid" alt="ion-grid"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-calendar"><i class="ion-calendar" title="ion-calendar" alt="ion-calendar"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-clock"><i class="ion-clock" title="ion-clock" alt="ion-clock"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-compass"><i class="ion-compass" title="ion-compass" alt="ion-compass"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-pinpoint"><i class="ion-pinpoint" title="ion-pinpoint" alt="ion-pinpoint"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-pin"><i class="ion-pin" title="ion-pin" alt="ion-pin"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-navigate"><i class="ion-navigate" title="ion-navigate" alt="ion-navigate"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-location"><i class="ion-location" title="ion-location" alt="ion-location"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-map"><i class="ion-map" title="ion-map" alt="ion-map"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-model-s"><i class="ion-model-s" title="ion-model-s" alt="ion-model-s"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-locked"><i class="ion-locked" title="ion-locked" alt="ion-locked"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-unlocked"><i class="ion-unlocked" title="ion-unlocked" alt="ion-unlocked"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-key"><i class="ion-key" title="ion-key" alt="ion-key"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-graph-up-right"><i class="ion-arrow-graph-up-right" title="ion-arrow-graph-up-right" alt="ion-arrow-graph-up-right"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-graph-down-right"><i class="ion-arrow-graph-down-right" title="ion-arrow-graph-down-right" alt="ion-arrow-graph-down-right"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-arrow-graph-down-left"><i class="ion-arrow-graph-down-left" title="ion-arrow-graph-down-left" alt="ion-arrow-graph-down-left"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-stats-bars"><i class="ion-stats-bars" title="ion-stats-bars" alt="ion-stats-bars"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-connection-bars"><i class="ion-connection-bars" title="ion-connection-bars" alt="ion-connection-bars"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-pie-graph"><i class="ion-pie-graph" title="ion-pie-graph" alt="ion-pie-graph"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chatbubble"><i class="ion-chatbubble" title="ion-chatbubble" alt="ion-chatbubble"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chatbubble-working"><i class="ion-chatbubble-working" title="ion-chatbubble-working" alt="ion-chatbubble-working"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chatbubbles"><i class="ion-chatbubbles" title="ion-chatbubbles" alt="ion-chatbubbles"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chatbox"><i class="ion-chatbox" title="ion-chatbox" alt="ion-chatbox"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chatbox-working"><i class="ion-chatbox-working" title="ion-chatbox-working" alt="ion-chatbox-working"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-chatboxes"><i class="ion-chatboxes" title="ion-chatboxes" alt="ion-chatboxes"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-person"><i class="ion-person" title="ion-person" alt="ion-person"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-person-add"><i class="ion-person-add" title="ion-person-add" alt="ion-person-add"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-person-stalker"><i class="ion-person-stalker" title="ion-person-stalker" alt="ion-person-stalker"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-woman"><i class="ion-woman" title="ion-woman" alt="ion-woman"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-man"><i class="ion-man" title="ion-man" alt="ion-man"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-female"><i class="ion-female" title="ion-female" alt="ion-female"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-male"><i class="ion-male" title="ion-male" alt="ion-male"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-fork"><i class="ion-fork" title="ion-fork" alt="ion-fork"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-knife"><i class="ion-knife" title="ion-knife" alt="ion-knife"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-spoon"><i class="ion-spoon" title="ion-spoon" alt="ion-spoon"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-beer"><i class="ion-beer" title="ion-beer" alt="ion-beer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-wineglass"><i class="ion-wineglass" title="ion-wineglass" alt="ion-wineglass"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-coffee"><i class="ion-coffee" title="ion-coffee" alt="ion-coffee"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-icecream"><i class="ion-icecream" title="ion-icecream" alt="ion-icecream"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-pizza"><i class="ion-pizza" title="ion-pizza" alt="ion-pizza"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-power"><i class="ion-power" title="ion-power" alt="ion-power"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-mouse"><i class="ion-mouse" title="ion-mouse" alt="ion-mouse"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-battery-full"><i class="ion-battery-full" title="ion-battery-full" alt="ion-battery-full"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-battery-half"><i class="ion-battery-half" title="ion-battery-half" alt="ion-battery-half"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-battery-low"><i class="ion-battery-low" title="ion-battery-low" alt="ion-battery-low"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-battery-empty"><i class="ion-battery-empty" title="ion-battery-empty" alt="ion-battery-empty"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-battery-charging"><i class="ion-battery-charging" title="ion-battery-charging" alt="ion-battery-charging"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-wifi"><i class="ion-wifi" title="ion-wifi" alt="ion-wifi"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-bluetooth"><i class="ion-bluetooth" title="ion-bluetooth" alt="ion-bluetooth"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-calculator"><i class="ion-calculator" title="ion-calculator" alt="ion-calculator"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-camera"><i class="ion-camera" title="ion-camera" alt="ion-camera"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-eye"><i class="ion-eye" title="ion-eye" alt="ion-eye"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-eye-disabled"><i class="ion-eye-disabled" title="ion-eye-disabled" alt="ion-eye-disabled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-flash"><i class="ion-flash" title="ion-flash" alt="ion-flash"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-flash-off"><i class="ion-flash-off" title="ion-flash-off" alt="ion-flash-off"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-qr-scanner"><i class="ion-qr-scanner" title="ion-qr-scanner" alt="ion-qr-scanner"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-image"><i class="ion-image" title="ion-image" alt="ion-image"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-images"><i class="ion-images" title="ion-images" alt="ion-images"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-contrast"><i class="ion-contrast" title="ion-contrast" alt="ion-contrast"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-wand"><i class="ion-wand" title="ion-wall" alt="ion-wall"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-aperture"><i class="ion-aperture" title="ion-aperture" alt="ion-aperture"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-monitor"><i class="ion-monitor" title="ion-monitor" alt="ion-monitor"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-laptop"><i class="ion-laptop" title="ion-laptop" alt="ion-laptop"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ipad"><i class="ion-ipad" title="ion-ipad" alt="ion-ipad"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-iphone"><i class="ion-iphone" title="ion-iphone" alt="ion-iphone"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ipod"><i class="ion-ipod" title="ion-ipod" alt="ion-ipod"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-printer"><i class="ion-printer" title="ion-printer" alt="ion-printer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-usb"><i class="ion-usb" title="ion-usb" alt="ion-usb"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-outlet"><i class="ion-outlet" title="ion-outlet" alt="ion-outlet"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-bug"><i class="ion-bug" title="ion-bug" alt="ion-bug"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-code"><i class="ion-code" title="ion-code" alt="ion-code"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-code-working"><i class="ion-code-working" title="ion-code-working" alt="ion-code-working"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-code-download"><i class="ion-code-download" title="ion-code-download" alt="ion-code-download"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-fork-repo"><i class="ion-fork-repo" title="ion-fork-repo" alt="ion-fork-repo"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-network"><i class="ion-network" title="ion-network" alt="ion-network"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-pull-request"><i class="ion-pull-request" title="ion-pull-request" alt="ion-pull-request"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-merge"><i class="ion-merge" title="ion-go" alt="ion-go"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-game-controller-a"><i class="ion-game-controller-a" title="ion-game-controller-a" alt="ion-game-controller-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-game-controller-b"><i class="ion-game-controller-b" title="ion-game-controller-b" alt="ion-game-controller-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-xbox"><i class="ion-xbox" title="ion-xbox" alt="ion-xbox"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-playstation"><i class="ion-playstation" title="ion-playstation" alt="ion-playstation"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-steam"><i class="ion-steam" title="ion-steam" alt="ion-steam"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-closed-captioning"><i class="ion-closed-captioning" title="ion-closed-captioning" alt="ion-closed-captioning"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-videocamera"><i class="ion-videocamera" title="ion-camera" alt="ion-camera"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-film-marker"><i class="ion-film-marker" title="ion-film-marker" alt="ion-film-marker"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-disc"><i class="ion-disc" title="ion-disc" alt="ion-disc"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-headphone"><i class="ion-headphone" title="ion-headphone" alt="ion-headphone"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-music-note"><i class="ion-music-note" title="ion-music-note" alt="ion-music-note"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-radio-waves"><i class="ion-radio-waves" title="ion-radio-waves" alt="ion-radio-waves"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-speakerphone"><i class="ion-speakerphone" title="ion-speakerphone" alt="ion-speakerphone"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-mic-a"><i class="ion-mic-a" title="ion-mic-a" alt="ion-mic-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-mic-b"><i class="ion-mic-b" title="ion-mic-b" alt="ion-mic-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-mic-c"><i class="ion-mic-c" title="ion-mic-c" alt="ion-mic-c"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-volume-high"><i class="ion-volume-high" title="ion-volume-high" alt="ion-volume-high"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-volume-medium"><i class="ion-volume-medium" title="ion-volume-medium" alt="ion-volume-medium"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-volume-low"><i class="ion-volume-low" title="ion-volume-low" alt="ion-volume-low"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-volume-mute"><i class="ion-volume-mute" title="ion-volume-mute" alt="ion-volume-mute"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-levels"><i class="ion-levels" title="ion-levels" alt="ion-levels"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-play"><i class="ion-play" title="ion-play" alt="ion-play"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-pause"><i class="ion-pause" title="ion-pause" alt="ion-pause"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-stop"><i class="ion-stop" title="ion-stop" alt="ion-stop"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-record"><i class="ion-record" title="ion-record" alt="ion-record"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-skip-forward"><i class="ion-skip-forward" title="ion-skip-forward" alt="ion-skip-forward"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-skip-backward"><i class="ion-skip-backward" title="ion-skip-backward" alt="ion-skip-backward"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-eject"><i class="ion-eject" title="ion-eject" alt="ion-eject"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-bag"><i class="ion-bag" title="ion-bag" alt="ion-bag"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-card"><i class="ion-card" title="ion-card" alt="ion-card"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-cash"><i class="ion-cash" title="ion-cash" alt="ion-cash"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-pricetag"><i class="ion-pricetag" title="ion-pricetag" alt="ion-pricetag"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-pricetags"><i class="ion-pricetags" title="ion-pricetags" alt="ion-pricetags"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-thumbsup"><i class="ion-thumbsup" title="ion-thumbsup" alt="ion-thumbsup"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-thumbsdown"><i class="ion-thumbsdown" title="ion-thumbsdown" alt="ion-thumbsdown"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-happy"><i class="ion-happy" title="ion-happy" alt="ion-happy"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-sad"><i class="ion-sad" title="ion-sad" alt="ion-sad"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-trophy"><i class="ion-trophy" title="ion-trophy" alt="ion-trophy"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-podium"><i class="ion-podium" title="ion-podium" alt="ion-podium"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ribbon-a"><i class="ion-ribbon-a" title="ion-ribbon-a" alt="ion-ribbon-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ribbon-b"><i class="ion-ribbon-b" title="ion-ribbon-b" alt="ion-ribbon-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-university"><i class="ion-university" title="ion-university" alt="ion-university"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-magnet"><i class="ion-magnet" title="ion-magnet" alt="ion-magnet"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-beaker"><i class="ion-beaker" title="ion-beaker" alt="ion-beaker"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-flask"><i class="ion-flask" title="ion-flask" alt="ion-flask"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-egg"><i class="ion-egg" title="ion-egg" alt="ion-egg"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-earth"><i class="ion-earth" title="ion-earth" alt="ion-earth"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-planet"><i class="ion-planet" title="ion-planet" alt="ion-planet"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-lightbulb"><i class="ion-lightbulb" title="ion-lightbulb" alt="ion-lightbulb"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-cube"><i class="ion-cube" title="ion-cube" alt="ion-cube"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-leaf"><i class="ion-leaf" title="ion-leaf" alt="ion-leaf"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-waterdrop"><i class="ion-waterdrop" title="ion-waterdrop" alt="ion-waterdrop"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-flame"><i class="ion-flame" title="ion-flame" alt="ion-flame"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-fireball"><i class="ion-fireball" title="ion-fireball" alt="ion-fireball"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-bonfire"><i class="ion-bonfire" title="ion-bonfire" alt="ion-bonfire"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-umbrella"><i class="ion-umbrella" title="ion-umbrella" alt="ion-umbrella"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-nuclear"><i class="ion-nuclear" title="ion-nuclear" alt="ion-nuclear"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-no-smoking"><i class="ion-no-smoking" title="ion-no-smoking" alt="ion-no-smoking"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-thermometer"><i class="ion-thermometer" title="ion-thermometer" alt="ion-thermometer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-speedometer"><i class="ion-speedometer" title="ion-speedometer" alt="ion-speedometer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-plane"><i class="ion-plane" title="ion-plane" alt="ion-plane"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-jet"><i class="ion-jet" title="ion-jet" alt="ion-jet"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-load-a"><i class="ion-load-a" title="ion-load-a" alt="ion-load-a"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-load-b"><i class="ion-load-b" title="ion-load-b" alt="ion-load-b"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-load-c"><i class="ion-load-c" title="ion-load-c" alt="ion-load-c"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-load-d"><i class="ion-load-d" title="ion-load-d" alt="ion-load-d"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-ionic-outline"><i class="ion-ios7-ionic-outline" title="ion-ios7-ionic-outline" alt="ion-ios7-ionic-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-back"><i class="ion-ios7-arrow-back" title="ion-ios7-arrow-back" alt="ion-ios7-arrow-back"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-forward"><i class="ion-ios7-arrow-forward" title="ion-ios7-arrow-forward" alt="ion-ios7-arrow-forward"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-up"><i class="ion-ios7-arrow-up" title="ion-ios7-arrow-up" alt="ion-ios7-arrow-up"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-right"><i class="ion-ios7-arrow-right" title="ion-ios7-arrow-right" alt="ion-ios7-arrow-right"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-down"><i class="ion-ios7-arrow-down" title="ion-ios7-arrow-down" alt="ion-ios7-arrow-down"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-left"><i class="ion-ios7-arrow-left" title="ion-ios7-arrow-left" alt="ion-ios7-arrow-left"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-thin-up"><i class="ion-ios7-arrow-thin-up" title="ion-ios7-arrow-thin-up" alt="ion-ios7-arrow-thin-up"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-thin-right"><i class="ion-ios7-arrow-thin-right" title="ion-ios7-arrow-thin-right" alt="ion-ios7-arrow-thin-right"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-thin-down"><i class="ion-ios7-arrow-thin-down" title="ion-ios7-arrow-thin-down" alt="ion-ios7-arrow-thin-down"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-arrow-thin-left"><i class="ion-ios7-arrow-thin-left" title="ion-ios7-arrow-thin-left" alt="ion-ios7-arrow-thin-left"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-circle-filled"><i class="ion-ios7-circle-filled" title="ion-ios7-circle-filled" alt="ion-ios7-circle-filled"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-circle-outline"><i class="ion-ios7-circle-outline" title="ion-ios7-circle-outline" alt="ion-ios7-circle-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-checkmark-empty"><i class="ion-ios7-checkmark-empty" title="ion-ios7-checkmark-empty" alt="ion-ios7-checkmark-empty"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-checkmark-outline"><i class="ion-ios7-checkmark-outline" title="ion-ios7-checkmark-outline" alt="ion-ios7-checkmark-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-checkmark"><i class="ion-ios7-checkmark" title="ion-ios7-checkmark" alt="ion-ios7-checkmark"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-plus-empty"><i class="ion-ios7-plus-empty" title="ion-ios7-plus-empty" alt="ion-ios7-plus-empty"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-plus-outline"><i class="ion-ios7-plus-outline" title="ion-ios7-plus-outline" alt="ion-ios7-plus-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-plus"><i class="ion-ios7-plus" title="ion-ios7 more" alt="ion-ios7 more"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-close-empty"><i class="ion-ios7-close-empty" title="ion-ios7-close-empty" alt="ion-ios7-close-empty"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-close-outline"><i class="ion-ios7-close-outline" title="ion-ios7-close-outline" alt="ion-ios7-close-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-close"><i class="ion-ios7-close" title="ion-ios7-close" alt="ion-ios7-close"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-minus-empty"><i class="ion-ios7-minus-empty" title="ion-ios7-minus-empty" alt="ion-ios7-minus-empty"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-minus-outline"><i class="ion-ios7-minus-outline" title="ion-ios7-minus-outline" alt="ion-ios7-minus-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-minus"><i class="ion-ios7-minus" title="ion-ios7-minus" alt="ion-ios7-minus"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-information-empty"><i class="ion-ios7-information-empty" title="ion-ios7-information-empty" alt="ion-ios7-information-empty"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-information-outline"><i class="ion-ios7-information-outline" title="ion-ios7-information-outline" alt="ion-ios7-information-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-information"><i class="ion-ios7-information" title="ion-ios7-information" alt="ion-ios7-information"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-help-empty"><i class="ion-ios7-help-empty" title="ion-ios7-help-empty" alt="ion-ios7-help-empty"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-help-outline"><i class="ion-ios7-help-outline" title="ion-ios7-help-outline" alt="ion-ios7-help-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-help"><i class="ion-ios7-help" title="ion-ios7-help" alt="ion-ios7-help"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-search"><i class="ion-ios7-search" title="ion-ios7-search" alt="ion-ios7-search"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-search-strong"><i class="ion-ios7-search-strong" title="ion-ios7-search-strong" alt="ion-ios7-search-strong"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-star"><i class="ion-ios7-star" title="ion-ios7-star" alt="ion-ios7-star"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-star-half"><i class="ion-ios7-star-half" title="ion-ios7-star-half" alt="ion-ios7-star-half"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-star-outline"><i class="ion-ios7-star-outline" title="ion-ios7-star-outline" alt="ion-ios7-star-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-heart"><i class="ion-ios7-heart" title="ion-ios7-heart" alt="ion-ios7-heart"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-heart-outline"><i class="ion-ios7-heart-outline" title="ion-ios7-heart-outline" alt="ion-ios7-heart-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-more"><i class="ion-ios7-more" title="ion-ios7-more" alt="ion-ios7-more"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-more-outline"><i class="ion-ios7-more-outline" title="ion-ios7-more-outline" alt="ion-ios7-more-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-home"><i class="ion-ios7-home" title="ion-ios7-home" alt="ion-ios7-home"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-home-outline"><i class="ion-ios7-home-outline" title="ion-ios7-home-outline" alt="ion-ios7-home-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloud"><i class="ion-ios7-cloud" title="ion-ios7-cloud" alt="ion-ios7-cloud"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloud-outline"><i class="ion-ios7-cloud-outline" title="ion-ios7-cloud-outline" alt="ion-ios7-cloud-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloud-upload"><i class="ion-ios7-cloud-upload" title="ion-ios7-cloud-upload" alt="ion-ios7-cloud-upload"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloud-upload-outline"><i class="ion-ios7-cloud-upload-outline" title="ion-ios7-cloud-upload-outline" alt="ion-ios7-cloud-upload-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloud-download"><i class="ion-ios7-cloud-download" title="ion-ios7-cloud-download" alt="ion-ios7-cloud-download"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloud-download-outline"><i class="ion-ios7-cloud-download-outline" title="ion-ios7-cloud-download-outline" alt="ion-ios7-cloud-download-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-upload"><i class="ion-ios7-upload" title="ion-ios7-upload" alt="ion-ios7-upload"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-upload-outline"><i class="ion-ios7-upload-outline" title="ion-ios7-upload-outline" alt="ion-ios7-upload-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-download"><i class="ion-ios7-download" title="ion-ios7-download" alt="ion-ios7-download"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-download-outline"><i class="ion-ios7-download-outline" title="ion-ios7-download-outline" alt="ion-ios7-download-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-refresh"><i class="ion-ios7-refresh" title="ion-ios7-refresh" alt="ion-ios7-refresh"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-refresh-outline"><i class="ion-ios7-refresh-outline" title="ion-ios7-refresh-outline" alt="ion-ios7-refresh-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-refresh-empty"><i class="ion-ios7-refresh-empty" title="ion-ios7-refresh-empty" alt="ion-ios7-refresh-empty"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-reload"><i class="ion-ios7-reload" title="ion-ios7-reload" alt="ion-ios7-reload"></i></a></div>

<div class="icon"><a href="#" data-icon="ion-ios7-loop-strong"><i class="ion-ios7-loop-strong" title="ion-ios7-loop-strong" alt="ion-ios7-loop-strong"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-loop"><i class="ion-ios7-loop" title="ion-ios7-loop" alt="ion-ios7-loop"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-bookmarks"><i class="ion-ios7-bookmarks" title="ion-ios7-bookmarks" alt="ion-ios7-bookmarks"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-bookmarks-outline"><i class="ion-ios7-bookmarks-outline" title="ion-ios7-bookmarks-outline" alt="ion-ios7-bookmarks-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-flag"><i class="ion-ios7-flag" title="ion-ios7-flag" alt="ion-ios7-flag"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-flag-outline"><i class="ion-ios7-flag-outline" title="ion-ios7-flag-outline" alt="ion-ios7-flag-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-glasses"><i class="ion-ios7-glasses" title="ion-ios7-glasses" alt="ion-ios7-glasses"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-glasses-outline"><i class="ion-ios7-glasses-outline" title="ion-ios7-glasses-outline" alt="ion-ios7-glasses-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-browsers"><i class="ion-ios7-browsers" title="ion-ios7-browsers" alt="ion-ios7-browsers"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-browsers-outline"><i class="ion-ios7-browsers-outline" title="ion-ios7-browsers-outline" alt="ion-ios7-browsers-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-at"><i class="ion-ios7-at" title="ion-ios7-at" alt="ion-ios7-at"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-at-outline"><i class="ion-ios7-at-outline" title="ion-ios7-at-outline" alt="ion-ios7-at-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cart"><i class="ion-ios7-cart" title="ion-ios7-cart" alt="ion-ios7-cart"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cart-outline"><i class="ion-ios7-cart-outline" title="ion-ios7-cart-outline" alt="ion-ios7-cart-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pricetag"><i class="ion-ios7-pricetag" title="ion-ios7-pricetag" alt="ion-ios7-pricetag"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pricetag-outline"><i class="ion-ios7-pricetag-outline" title="ion-ios7-pricetag-outline" alt="ion-ios7-pricetag-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pricetags"><i class="ion-ios7-pricetags" title="ion-ios7-pricetags" alt="ion-ios7-pricetags"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pricetags-outline"><i class="ion-ios7-pricetags-outline" title="ion-ios7-pricetags-outline" alt="ion-ios7-pricetags-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-chatboxes"><i class="ion-ios7-chatboxes" title="ion-ios7-chatboxes" alt="ion-ios7-chatboxes"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-chatboxes-outline"><i class="ion-ios7-chatboxes-outline" title="ion-ios7-chatboxes-outline" alt="ion-ios7-chatboxes-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-chatbubble"><i class="ion-ios7-chatbubble" title="ion-ios7-chatbubble" alt="ion-ios7-chatbubble"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-chatbubble-outline"><i class="ion-ios7-chatbubble-outline" title="ion-ios7-chatbubble-outline" alt="ion-ios7-chatbubble-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cog"><i class="ion-ios7-cog" title="ion-ios7-cog" alt="ion-ios7-cog"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cog-outline"><i class="ion-ios7-cog-outline" title="ion-ios7-cog-outline" alt="ion-ios7-cog-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-gear"><i class="ion-ios7-gear" title="ion-ios7-gear" alt="ion-ios7-gear"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-gear-outline"><i class="ion-ios7-gear-outline" title="ion-ios7-gear-outline" alt="ion-ios7-gear-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-settings"><i class="ion-ios7-settings" title="ion-ios7-settings" alt="ion-ios7-settings"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-settings-strong"><i class="ion-ios7-settings-strong" title="ion-ios7-settings-strong" alt="ion-ios7-settings-strong"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-toggle"><i class="ion-ios7-toggle" title="ion-ios7-toggle" alt="ion-ios7-toggle"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-toggle-outline"><i class="ion-ios7-toggle-outline" title="ion-ios7-toggle-outline" alt="ion-ios7-toggle-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-analytics"><i class="ion-ios7-analytics" title="ion-ios7-analytics" alt="ion-ios7-analytics"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-analytics-outline"><i class="ion-ios7-analytics-outline" title="ion-ios7-analytics-outline" alt="ion-ios7-analytics-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pie"><i class="ion-ios7-pie" title="ion-ios7-pie" alt="ion-ios7-pie"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pie-outline"><i class="ion-ios7-pie-outline" title="ion-ios7-pie-outline" alt="ion-ios7-pie-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pulse"><i class="ion-ios7-pulse" title="ion-ios7-pulse" alt="ion-ios7-pulse"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pulse-strong"><i class="ion-ios7-pulse-strong" title="ion-ios7-pulse-strong" alt="ion-ios7-pulse-strong"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-filing"><i class="ion-ios7-filing" title="ion-ios7-filing" alt="ion-ios7-filing"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-filing-outline"><i class="ion-ios7-filing-outline" title="ion-ios7-filing-outline" alt="ion-ios7-filing-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-box"><i class="ion-ios7-box" title="ion-ios7-box" alt="ion-ios7-box"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-box-outline"><i class="ion-ios7-box-outline" title="ion-ios7-box-outline" alt="ion-ios7-box-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-compose"><i class="ion-ios7-compose" title="ion-ios7-compose" alt="ion-ios7-compose"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-compose-outline"><i class="ion-ios7-compose-outline" title="ion-ios7-compose-outline" alt="ion-ios7-compose-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-trash"><i class="ion-ios7-trash" title="ion-ios7-trash" alt="ion-ios7-trash"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-trash-outline"><i class="ion-ios7-trash-outline" title="ion-ios7-trash-outline" alt="ion-ios7-trash-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-copy"><i class="ion-ios7-copy" title="ion-ios7-copy" alt="ion-ios7-copy"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-copy-outline"><i class="ion-ios7-copy-outline" title="ion-ios7-copy-outline" alt="ion-ios7-copy-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-email"><i class="ion-ios7-email" title="ion-ios7-email" alt="ion-ios7-email"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-email-outline"><i class="ion-ios7-email-outline" title="ion-ios7-email-outline" alt="ion-ios7-email-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-undo"><i class="ion-ios7-undo" title="ion-ios7-undo" alt="ion-ios7-undo"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-undo-outline"><i class="ion-ios7-undo-outline" title="ion-ios7-undo-outline" alt="ion-ios7-undo-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-redo"><i class="ion-ios7-redo" title="ion ios7-ready" alt="ion ios7-ready"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-redo-outline"><i class="ion-ios7-redo-outline" title="ion-ios7-redo-outline" alt="ion-ios7-redo-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-redo-outline"><i class="ion-ios7-redo-outline" title="ion-ios7-redo-outline" alt="ion-ios7-redo-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-paperplane"><i class="ion-ios7-paperplane" title="ion-ios7-paperplane" alt="ion-ios7-paperplane"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-paperplane-outline"><i class="ion-ios7-paperplane-outline" title="ion-ios7-paperplane-outline" alt="ion-ios7-paperplane-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-folder"><i class="ion-ios7-folder" title="ion-ios7-folder" alt="ion-ios7-folder"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-folder-outline"><i class="ion-ios7-folder-outline" title="ion-ios7-folder-outline" alt="ion-ios7-folder-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-paper"><i class="ion-ios7-paper" title="ion-ios7-paper" alt="ion-ios7-paper"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-paper-outline"><i class="ion-ios7-paper-outline" title="ion-ios7-paper-outline" alt="ion-ios7-paper-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-world"><i class="ion-ios7-world" title="ion-ios7-world" alt="ion-ios7-world"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-world-outline"><i class="ion-ios7-world-outline" title="ion-ios7-world-outline" alt="ion-ios7-world-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-alarm"><i class="ion-ios7-alarm" title="ion-ios7-alarm" alt="ion-ios7-alarm"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-alarm-outline"><i class="ion-ios7-alarm-outline" title="ion-ios7-alarm-outline" alt="ion-ios7-alarm-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-speedometer"><i class="ion-ios7-speedometer" title="ion-ios7-speedometer" alt="ion-ios7-speedometer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-speedometer-outline"><i class="ion-ios7-speedometer-outline" title="ion-ios7-speedometer-outline" alt="ion-ios7-speedometer-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-stopwatch"><i class="ion-ios7-stopwatch" title="ion-ios7-stopwatch" alt="ion-ios7-stopwatch"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-stopwatch-outline"><i class="ion-ios7-stopwatch-outline" title="ion-ios7-stopwatch-outline" alt="ion-ios7-stopwatch-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-timer"><i class="ion-ios7-timer" title="ion-ios7-timer" alt="ion-ios7-timer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-timer-outline"><i class="ion-ios7-timer-outline" title="ion-ios7-timer-outline" alt="ion-ios7-timer-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-clock"><i class="ion-ios7-clock" title="ion-ios7-clock" alt="ion-ios7-clock"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-clock-outline"><i class="ion-ios7-clock-outline" title="ion-ios7-clock-outline" alt="ion-ios7-clock-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-time"><i class="ion-ios7-time" title="ion-ios7-time" alt="ion-ios7-time"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-time-outline"><i class="ion-ios7-time-outline" title="ion-ios7-time-outline" alt="ion-ios7-time-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-calendar"><i class="ion-ios7-calendar" title="ion-ios7-calendar" alt="ion-ios7-calendar"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-calendar-outline"><i class="ion-ios7-calendar-outline" title="ion-ios7-calendar-outline" alt="ion-ios7-calendar-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-photos"><i class="ion-ios7-photos" title="ion-ios7-photos" alt="ion-ios7-photos"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-photos-outline"><i class="ion-ios7-photos-outline" title="ion-ios7-photos-outline" alt="ion-ios7-photos-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-albums"><i class="ion-ios7-albums" title="ion-ios7-albums" alt="ion-ios7-albums"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-albums-outline"><i class="ion-ios7-albums-outline" title="ion-ios7-albums-outline" alt="ion-ios7-albums-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-camera"><i class="ion-ios7-camera" title="ion-ios7-camera" alt="ion-ios7-camera"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-camera-outline"><i class="ion-ios7-camera-outline" title="ion-ios7-camera-outline" alt="ion-ios7-camera-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-reverse-camera"><i class="ion-ios7-reverse-camera" title="ion-ios7-reverse-camera" alt="ion-ios7-reverse-camera"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-reverse-camera-outline"><i class="ion-ios7-reverse-camera-outline" title="ion-ios7-reverse-camera-outline" alt="ion-ios7-reverse-camera-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-eye"><i class="ion-ios7-eye" title="ion-ios7-eye" alt="ion-ios7-eye"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-eye-outline"><i class="ion-ios7-eye-outline" title="ion-ios7-eye-outline" alt="ion-ios7-eye-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-bolt"><i class="ion-ios7-bolt" title="ion-ios7-bolt" alt="ion-ios7-bolt"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-bolt-outline"><i class="ion-ios7-bolt-outline" title="ion-ios7-bolt-outline" alt="ion-ios7-bolt-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-barcode"><i class="ion-ios7-barcode" title="ion-ios7-barcode" alt="ion-ios7-barcode"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-barcode-outline"><i class="ion-ios7-barcode-outline" title="ion-ios7-barcode-outline" alt="ion-ios7-barcode-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-briefcase"><i class="ion-ios7-briefcase" title="ion-ios7-briefcase" alt="ion-ios7-briefcase"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-briefcase-outline"><i class="ion-ios7-briefcase-outline" title="ion-ios7-briefcase-outline" alt="ion-ios7-briefcase-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-medkit"><i class="ion-ios7-medkit" title="ion-ios7-medkit" alt="ion-ios7-medkit"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-medkit-outline"><i class="ion-ios7-medkit-outline" title="ion-ios7-medkit-outline" alt="ion-ios7-medkit-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-infinite"><i class="ion-ios7-infinite" title="ion-ios7-infinite" alt="ion-ios7-infinite"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-infinite-outline"><i class="ion-ios7-infinite-outline" title="ion-ios7-infinite-outline" alt="ion-ios7-infinite-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-calculator"><i class="ion-ios7-calculator" title="ion-ios7-calculator" alt="ion-ios7-calculator"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-calculator-outline"><i class="ion-ios7-calculator-outline" title="ion-ios7-calculator-outline" alt="ion-ios7-calculator-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-keypad"><i class="ion-ios7-keypad" title="ion-ios7-keypad" alt="ion-ios7-keypad"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-keypad-outline"><i class="ion-ios7-keypad-outline" title="ion-ios7-keypad-outline" alt="ion-ios7-keypad-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-telephone"><i class="ion-ios7-telephone" title="ion-ios7-telephone" alt="ion-ios7-telephone"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-telephone-outline"><i class="ion-ios7-telephone-outline" title="ion-ios7-telephone-outline" alt="ion-ios7-telephone-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-drag"><i class="ion-ios7-drag" title="ion-ios7-drag" alt="ion-ios7-drag"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-location"><i class="ion-ios7-location" title="ion-ios7-location" alt="ion-ios7-location"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-location-outline"><i class="ion-ios7-location-outline" title="ion-ios7-location-outline" alt="ion-ios7-location-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-navigate"><i class="ion-ios7-navigate" title="ion-ios7-navigate" alt="ion-ios7-navigate"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-navigate-outline"><i class="ion-ios7-navigate-outline" title="ion-ios7-navigate-outline" alt="ion-ios7-navigate-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-locked"><i class="ion-ios7-locked" title="ion-ios7-locked" alt="ion-ios7-locked"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-locked-outline"><i class="ion-ios7-locked-outline" title="ion-ios7-locked-outline" alt="ion-ios7-locked-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-unlocked"><i class="ion-ios7-unlocked" title="ion-ios7-unlocked" alt="ion-ios7-unlocked"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-unlocked-outline"><i class="ion-ios7-unlocked-outline" title="ion-ios7-unlocked-outline" alt="ion-ios7-unlocked-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-monitor"><i class="ion-ios7-monitor" title="ion-ios7-monitor" alt="ion-ios7-monitor"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-monitor-outline"><i class="ion-ios7-monitor-outline" title="ion-ios7-monitor-outline" alt="ion-ios7-monitor-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-printer"><i class="ion-ios7-printer" title="ion-ios7-printer" alt="ion-ios7-printer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-printer-outline"><i class="ion-ios7-printer-outline" title="ion-ios7-printer-outline" alt="ion-ios7-printer-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-person"><i class="ion-ios7-person" title="ion-ios7-person" alt="ion-ios7-person"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-person-outline"><i class="ion-ios7-person-outline" title="ion-ios7-person-outline" alt="ion-ios7-person-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-personadd"><i class="ion-ios7-personadd" title="ion-ios7-personadd" alt="ion-ios7-personadd"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-personadd-outline"><i class="ion-ios7-personadd-outline" title="ion-ios7-personadd-outline" alt="ion-ios7-personadd-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-people"><i class="ion-ios7-people" title="ion-ios7-people" alt="ion-ios7-people"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-people-outline"><i class="ion-ios7-people-outline" title="ion-ios7-people-outline" alt="ion-ios7-people-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-tennisball"><i class="ion-ios7-tennisball" title="ion ios7-tennis ball" alt="ion ios7-tennis ball"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-tennisball-outline"><i class="ion-ios7-tennisball-outline" title="ion ios7 tennis ball outline" alt="ion ios7 tennis ball outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-baseball"><i class="ion-ios7-baseball" title="ion-ios7-baseball" alt="ion-ios7-baseball"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-baseball-outline"><i class="ion-ios7-baseball-outline" title="ion-ios7-baseball-outline" alt="ion-ios7-baseball-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-basketball"><i class="ion-ios7-basketball" title="ion-ios7-basketball" alt="ion-ios7-basketball"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-basketball-outline"><i class="ion-ios7-basketball-outline" title="ion-ios7-basketball-outline" alt="ion-ios7-basketball-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-football"><i class="ion-ios7-football" title="ion-ios7-football" alt="ion-ios7-football"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-football-outline"><i class="ion-ios7-football-outline" title="ion-ios7-football-outline" alt="ion-ios7-football-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-americanfootball"><i class="ion-ios7-americanfootball" title="ion-ios7-americanfootball" alt="ion-ios7-americanfootball"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-americanfootball-outline"><i class="ion-ios7-americanfootball-outline" title="ion-ios7-americanfootball-outline" alt="ion-ios7-americanfootball-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-paw"><i class="ion-ios7-paw" title="ion-ios7-paw" alt="ion-ios7-paw"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-paw-outline"><i class="ion-ios7-paw-outline" title="ion-ios7-paw-outline" alt="ion-ios7-paw-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-sunny"><i class="ion-ios7-sunny" title="ion-ios7-sunny" alt="ion-ios7-sunny"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-sunny-outline"><i class="ion-ios7-sunny-outline" title="ion-ios7-sunny-outline" alt="ion-ios7-sunny-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-partlysunny"><i class="ion-ios7-partlysunny" title="ion-ios7-partlysunny" alt="ion-ios7-partlysunny"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-partlysunny-outline"><i class="ion-ios7-partlysunny-outline" title="ion-ios7-partlysunny-Outline" alt="ion-ios7-partlysunny-Outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloudy"><i class="ion-ios7-cloudy" title="ion-ios7-cloudy" alt="ion-ios7-cloudy"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloudy-outline"><i class="ion-ios7-cloudy-outline" title="ion-ios7-cloudy-outline" alt="ion-ios7-cloudy-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-rainy"><i class="ion-ios7-rainy" title="ion-ios7-rainy" alt="ion-ios7-rainy"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-rainy-outline"><i class="ion-ios7-rainy-outline" title="ion-ios7-rainy-outline" alt="ion-ios7-rainy-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-thunderstorm"><i class="ion-ios7-thunderstorm" title="ion-ios7-thunderstorm" alt="ion-ios7-thunderstorm"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-thunderstorm-outline"><i class="ion-ios7-thunderstorm-outline" title="ion-ios7-thunderstorm-outline" alt="ion-ios7-thunderstorm-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-snowy"><i class="ion-ios7-snowy" title="ion-ios7-snowy" alt="ion-ios7-snowy"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-moon"><i class="ion-ios7-moon" title="ion-ios7-moon" alt="ion-ios7-moon"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-moon-outline"><i class="ion-ios7-moon-outline" title="ion-ios7-moon-outline" alt="ion-ios7-moon-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloudy-night"><i class="ion-ios7-cloudy-night" title="ion-ios7-cloudy-night" alt="ion-ios7-cloudy-night"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-cloudy-night-outline"><i class="ion-ios7-cloudy-night-outline" title="ion-ios7-cloudy-night-outline" alt="ion-ios7-cloudy-night-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-musical-notes"><i class="ion-ios7-musical-notes" title="ion-ios7-musical-notes" alt="ion-ios7-musical-notes"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-musical-note"><i class="ion-ios7-musical-note" title="ion-ios7-musical-note" alt="ion-ios7-musical-note"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-bell"><i class="ion-ios7-bell" title="ion-ios7-bell" alt="ion-ios7-bell"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-bell-outline"><i class="ion-ios7-bell-outline" title="ion-ios7-bell-outline" alt="ion-ios7-bell-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-mic"><i class="ion-ios7-mic" title="ion-ios7-small" alt="ion-ios7-small"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-mic-outline"><i class="ion-ios7-mic-outline" title="ion-ios7-mic-outline" alt="ion-ios7-mic-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-mic-off"><i class="ion-ios7-mic-off" title="ion-ios7-mic-off" alt="ion-ios7-mic-off"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-volume-high"><i class="ion-ios7-volume-high" title="ion-ios7-volume-high" alt="ion-ios7-volume-high"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-volume-low"><i class="ion-ios7-volume-low" title="ion-ios7-volume-low" alt="ion-ios7-volume-low"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-play"><i class="ion-ios7-play" title="ion-ios7-play" alt="ion-ios7-play"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-play-outline"><i class="ion-ios7-play-outline" title="ion-ios7-play-outline" alt="ion-ios7-play-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pause"><i class="ion-ios7-pause" title="ion-ios7-pause" alt="ion-ios7-pause"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-pause-outline"><i class="ion-ios7-pause-outline" title="ion-ios7-pause-outline" alt="ion-ios7-pause-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-recording"><i class="ion-ios7-recording" title="ion-ios7-recording" alt="ion-ios7-recording"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-recording-outline"><i class="ion-ios7-recording-outline" title="ion-ios7-recording-outline" alt="ion-ios7-recording-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-fastforward"><i class="ion-ios7-fastforward" title="ion-iOS7-FastForward" alt="ion-iOS7-FastForward"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-fastforward-outline"><i class="ion-ios7-fastforward-outline" title="ion-iOS7-FastForward-outline" alt="ion-iOS7-FastForward-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-rewind"><i class="ion-ios7-rewind" title="ion-ios7-rewind" alt="ion-ios7-rewind"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-rewind-outline"><i class="ion-ios7-rewind-outline" title="ion-ios7-rewind-outline" alt="ion-ios7-rewind-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-skipbackward"><i class="ion-ios7-skipbackward" title="ion-ios7-skipbackward" alt="ion-ios7-skipbackward"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-skipbackward-outline"><i class="ion-ios7-skipbackward-outline" title="ion-ios7-skipbackward-outline" alt="ion-ios7-skipbackward-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-skipforward"><i class="ion-ios7-skipforward" title="ion-ios7-skipforward" alt="ion-ios7-skipforward"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-skipforward-outline"><i class="ion-ios7-skipforward-outline" title="ion-ios7-skipforward-outline" alt="ion-ios7-skipforward-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-videocam"><i class="ion-ios7-videocam" title="ion-iOS7-videocam" alt="ion-iOS7-videocam"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-videocam-outline"><i class="ion-ios7-videocam-outline" title="ion-ios7-videocam-outline" alt="ion-ios7-videocam-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-film"><i class="ion-ios7-film" title="ion-ios7-film" alt="ion-ios7-film"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-film-outline"><i class="ion-ios7-film-outline" title="ion-ios7-film-outline" alt="ion-ios7-film-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-lightbulb"><i class="ion-ios7-lightbulb" title="ion-ios7-lightbulb" alt="ion-ios7-lightbulb"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-lightbulb-outline"><i class="ion-ios7-lightbulb-outline" title="ion-ios7-lightbulb-outline" alt="ion-ios7-lightbulb-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-wineglass"><i class="ion-ios7-wineglass" title="ion-ios7-wineglass" alt="ion-ios7-wineglass"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-ios7-wineglass-outline"><i class="ion-ios7-wineglass-outline" title="ion-ios7-wineglass-outline" alt="ion-ios7-wineglass-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-system-back"><i class="ion-android-system-back" title="ion-android-system-back" alt="ion-android-system-back"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-system-home"><i class="ion-android-system-home" title="ion-android-system-home" alt="ion-android-system-home"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-system-windows"><i class="ion-android-system-windows" title="ion-android-system-windows" alt="ion-android-system-windows"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-wifi"><i class="ion-android-wifi" title="ion-android-wifi" alt="ion-android-wifi"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-battery"><i class="ion-android-battery" title="ion-android-battery" alt="ion-android-battery"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-storage"><i class="ion-android-storage" title="ion-android-storage" alt="ion-android-storage"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-keypad"><i class="ion-android-keypad" title="ion-android-keypad" alt="ion-android-keypad"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-call"><i class="ion-android-call" title="ion-android-call" alt="ion-android-call"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-arrow-back"><i class="ion-android-arrow-back" title="ion-android-arrow-back" alt="ion-android-arrow-back"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-arrow-forward"><i class="ion-android-arrow-forward" title="ion-android-arrow-forward" alt="ion-android-arrow-forward"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-arrow-up-right"><i class="ion-android-arrow-up-right" title="ion-android-arrow-up-right" alt="ion-android-arrow-up-right"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-arrow-down-right"><i class="ion-android-arrow-down-right" title="ion-android-arrow-down-right" alt="ion-android-arrow-down-right"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-arrow-down-left"><i class="ion-android-arrow-down-left" title="ion-android-arrow-down-left" alt="ion-android-arrow-down-left"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-arrow-up-left"><i class="ion-android-arrow-up-left" title="ion-android-arrow-up-left" alt="ion-android-arrow-up-left"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-sort"><i class="ion-android-sort" title="ion-android-sort" alt="ion-android-sort"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-drawer"><i class="ion-android-drawer" title="ion-android-drawer" alt="ion-android-drawer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-stair-drawer"><i class="ion-android-stair-drawer" title="ion-android-stair-drawer" alt="ion-android-stair-drawer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-more"><i class="ion-android-more" title="ion-android-more" alt="ion-android-more"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-dropdown"><i class="ion-android-dropdown" title="ion-android-dropdown" alt="ion-android-dropdown"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-settings"><i class="ion-android-settings" title="ion-android-settings" alt="ion-android-settings"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-mixer"><i class="ion-android-mixer" title="ion-android-mixer" alt="ion-android-mixer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-display"><i class="ion-android-display" title="ion-android-display" alt="ion-android-display"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-star"><i class="ion-android-star" title="ion-android-star" alt="ion-android-star"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-search"><i class="ion-android-search" title="ion-android-search" alt="ion-android-search"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-microphone"><i class="ion-android-microphone" title="ion-android-microphone" alt="ion-android-microphone"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-checkmark"><i class="ion-android-checkmark" title="ion-android-checkmark" alt="ion-android-checkmark"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-information"><i class="ion-android-information" title="ion-android-information" alt="ion-android-information"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-add"><i class="ion-android-add" title="ion-android-add" alt="ion-android-add"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-remove"><i class="ion-android-remove" title="ion-android-remove" alt="ion-android-remove"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-close"><i class="ion-android-close" title="ion-android-close" alt="ion-android-close"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-trash"><i class="ion-android-trash" title="ion-android-trash" alt="ion-android-trash"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-volume"><i class="ion-android-volume" title="ion-android-volume" alt="ion-android-volume"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-printer"><i class="ion-android-printer" title="ion-android-printer" alt="ion-android-printer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-download"><i class="ion-android-download" title="ion-android-download" alt="ion-android-download"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-hand"><i class="ion-android-hand" title="ion-android-hand" alt="ion-android-hand"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-developer"><i class="ion-android-developer" title="ion-android-developer" alt="ion-android-developer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-calendar"><i class="ion-android-calendar" title="ion-android-calendar" alt="ion-android-calendar"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-clock"><i class="ion-android-clock" title="ion-android-clock" alt="ion-android-clock"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-alarm"><i class="ion-android-alarm" title="ion-android-alarm" alt="ion-android-alarm"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-stopwatch"><i class="ion-android-stopwatch" title="ion-android-stopwatch" alt="ion-android-stopwatch"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-data"><i class="ion-android-data" title="ion-android-data" alt="ion-android-data"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-timer"><i class="ion-android-timer" title="ion-android-timer" alt="ion-android-timer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-camera"><i class="ion-android-camera" title="ion-android-camera" alt="ion-android-camera"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-image"><i class="ion-android-image" title="ion-android-image" alt="ion-android-image"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-location"><i class="ion-android-location" title="ion-android-location" alt="ion-android-location"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-locate"><i class="ion-android-locate" title="ion-android-locate" alt="ion-android-locate"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-earth"><i class="ion-android-earth" title="ion-android-earth" alt="ion-android-earth"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-book"><i class="ion-android-book" title="ion-android-book" alt="ion-android-book"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-note"><i class="ion-android-note" title="ion-android-note" alt="ion-android-note"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-promotion"><i class="ion-android-promotion" title="ion-android-promotion" alt="ion-android-promotion"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-playstore"><i class="ion-android-playstore" title="ion-android-playstore" alt="ion-android-playstore"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-share"><i class="ion-android-share" title="ion-android-share" alt="ion-android-share"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-send"><i class="ion-android-send" title="ion-android-send" alt="ion-android-send"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-mail"><i class="ion-android-mail" title="ion-android-mail" alt="ion-android-mail"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-inbox"><i class="ion-android-inbox" title="ion-android-inbox" alt="ion-android-inbox"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-archive"><i class="ion-android-archive" title="ion-android-archive" alt="ion-android-archive"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-folder"><i class="ion-android-folder" title="ion-android-folder" alt="ion-android-folder"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-forums"><i class="ion-android-forums" title="ion-android-forums" alt="ion-android-forums"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-chat"><i class="ion-android-chat" title="ion-android-chat" alt="ion-android-chat"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-user-menu"><i class="ion-android-user-menu" title="ion-android-user-menu" alt="ion-android-user-menu"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-contact"><i class="ion-android-contact" title="ion-android-contact" alt="ion-android-contact"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-contacts"><i class="ion-android-contacts" title="ion-android-contacts" alt="ion-android-contacts"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-friends"><i class="ion-android-friends" title="ion-android-friends" alt="ion-android-friends"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-add-contact"><i class="ion-android-add-contact" title="ion-android-add-contact" alt="ion-android-add-contact"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-social-user"><i class="ion-android-social-user" title="ion-android-social-user" alt="ion-android-social-user"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-social"><i class="ion-android-social" title="ion-android-social" alt="ion-android-social"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-reminder"><i class="ion-android-reminder" title="ion-android-reminder" alt="ion-android-reminder"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-android-lightbulb"><i class="ion-android-lightbulb" title="ion-android-lightbulb" alt="ion-android-lightbulb"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-twitter"><i class="ion-social-twitter" title="ion-social-twitter" alt="ion-social-twitter"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-twitter-outline"><i class="ion-social-twitter-outline" title="ion-social-twitter-outline" alt="ion-social-twitter-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-facebook"><i class="ion-social-facebook" title="ion-social-facebook" alt="ion-social-facebook"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-facebook-outline"><i class="ion-social-facebook-outline" title="ion-social-facebook-outline" alt="ion-social-facebook-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-googleplus"><i class="ion-social-googleplus" title="ion-social-googleplus" alt="ion-social-googleplus"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-googleplus-outline"><i class="ion-social-googleplus-outline" title="ion-social-googleplus-outline" alt="ion-social-googleplus-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-google"><i class="ion-social-google" title="ion-social-google" alt="ion-social-google"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-google-outline"><i class="ion-social-google-outline" title="ion-social-google-outline" alt="ion-social-google-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-dribbble"><i class="ion-social-dribbble" title="ion-social-dribbble" alt="ion-social-dribbble"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-dribbble-outline"><i class="ion-social-dribbble-outline" title="ion-social-dribbble-outline" alt="ion-social-dribbble-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-github"><i class="ion-social-github" title="ion-social-github" alt="ion-social-github"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-github-outline"><i class="ion-social-github-outline" title="ion-social-github-outline" alt="ion-social-github-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-instagram"><i class="ion-social-instagram" title="ion-social-instagram" alt="ion-social-instagram"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-instagram-outline"><i class="ion-social-instagram-outline" title="ion-social-instagram-outline" alt="ion-social-instagram-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-foursquare"><i class="ion-social-foursquare" title="ion-social-foursquare" alt="ion-social-foursquare"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-foursquare-outline"><i class="ion-social-foursquare-outline" title="ion-social-foursquare-outline" alt="ion-social-foursquare-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-pinterest"><i class="ion-social-pinterest" title="ion-social-pinterest" alt="ion-social-pinterest"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-pinterest-outline"><i class="ion-social-pinterest-outline" title="ion-social-pinterest-outline" alt="ion-social-pinterest-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-rss"><i class="ion-social-rss" title="ion-social-rss" alt="ion-social-rss"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-rss-outline"><i class="ion-social-rss-outline" title="ion-social-rss-outline" alt="ion-social-rss-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-tumblr"><i class="ion-social-tumblr" title="ion-social-tumblr" alt="ion-social-tumblr"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-tumblr-outline"><i class="ion-social-tumblr-outline" title="ion-social-tumblr-outline" alt="ion-social-tumblr-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-wordpress"><i class="ion-social-wordpress" title="ion-social-wordpress" alt="ion-social-wordpress"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-wordpress-outline"><i class="ion-social-wordpress-outline" title="ion-social-wordpress-outline" alt="ion-social-wordpress-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-reddit"><i class="ion-social-reddit" title="ion-social-reddit" alt="ion-social-reddit"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-reddit-outline"><i class="ion-social-reddit-outline" title="ion-social-reddit-outline" alt="ion-social-reddit-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-hackernews"><i class="ion-social-hackernews" title="ion-social-hackernews" alt="ion-social-hackernews"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-hackernews-outline"><i class="ion-social-hackernews-outline" title="ion-social-hackernews-outline" alt="ion-social-hackernews-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-designernews"><i class="ion-social-designernews" title="ion-social-designernews" alt="ion-social-designernews"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-designernews-outline"><i class="ion-social-designernews-outline" title="ion-social-designernews-outline" alt="ion-social-designernews-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-yahoo"><i class="ion-social-yahoo" title="ion-social-yahoo" alt="ion-social-yahoo"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-yahoo-outline"><i class="ion-social-yahoo-outline" title="ion-social-yahoo-outline" alt="ion-social-yahoo-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-buffer"><i class="ion-social-buffer" title="ion-social-buffer" alt="ion-social-buffer"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-buffer-outline"><i class="ion-social-buffer-outline" title="ion-social-buffer-outline" alt="ion-social-buffer-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-skype"><i class="ion-social-skype" title="ion-social-skype" alt="ion-social-skype"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-skype-outline"><i class="ion-social-skype-outline" title="ion-social-skype-outline" alt="ion-social-skype-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-linkedin"><i class="ion-social-linkedin" title="ion-social-linkedin" alt="ion-social-linkedin"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-linkedin-outline"><i class="ion-social-linkedin-outline" title="ion-social-linkedin-outline" alt="ion-social-linkedin-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-usd"><i class="ion-social-usd" title="ion-social-usd" alt="ion-social-usd"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-usd-outline"><i class="ion-social-usd-outline" title="ion-social-usd-outline" alt="ion-social-usd-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-bitcoin"><i class="ion-social-bitcoin" title="ion-social-bitcoin" alt="ion-social-bitcoin"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-bitcoin-outline"><i class="ion-social-bitcoin-outline" title="ion-social-bitcoin-outline" alt="ion-social-bitcoin-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-vimeo"><i class="ion-social-vimeo" title="ion-social-vimeo" alt="ion-social-vimeo"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-vimeo-outline"><i class="ion-social-vimeo-outline" title="ion-social-vimeo-outline" alt="ion-social-vimeo-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-youtube"><i class="ion-social-youtube" title="ion-social-youtube" alt="ion-social-youtube"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-youtube-outline"><i class="ion-social-youtube-outline" title="ion-social-youtube-outline" alt="ion-social-youtube-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-dropbox"><i class="ion-social-dropbox" title="ion-social-dropbox" alt="ion-social-dropbox"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-dropbox-outline"><i class="ion-social-dropbox-outline" title="ion-social-dropbox-outline" alt="ion-social-dropbox-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-apple"><i class="ion-social-apple" title="ion-social-apple" alt="ion-social-apple"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-apple-outline"><i class="ion-social-apple-outline" title="ion-social-apple-outline" alt="ion-social-apple-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-android"><i class="ion-social-android" title="ion-social-android" alt="ion-social-android"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-android"><i class="ion-social-android" title="ion-social-android-outline" alt="ion-social-android-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-windows"><i class="ion-social-windows" title="ion-social-windows" alt="ion-social-windows"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-windows-outline"><i class="ion-social-windows-outline" title="ion-social-windows-outline" alt="ion-social-windows-outline"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-tux"><i class="ion-social-tux" title="ion-social-tux" alt="ion-social-tux"></i></a></div>
<div class="icon"><a href="#" data-icon="ion-social-freebsd-devil"><i class="ion-social-freebsd-devil" title="ion-social-freebsd-devil" alt="ion-social-freebsd-devil"></i></a></div>

                                        </div> <!-- End row -->
                                    </div>
					  </div>
					</div>
				  </div>
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#icons" href="#fontawesome">
						Font awesome</a>
					  </h4>
					</div>
					<div id="fontawesome" class="panel-collapse collapse">
					  <div class="panel-body">
									<div class="card-box">
                                        <section>
		                                    <h4 class="m-t-0 page-header header-title"><b>30 New Icons in 4.6.3 </b><span class="label label-sm label-purple">New</span></h4>
		                                    <div class="row">
		                                        <div class="icon"><a href="#" data-icon="fa fa-american-sign-language-interpreting"><i class="fa fa-american-sign-language-interpreting" title="fa fa-american-sign-language-interpreting" alt="fa fa-american-sign-language-interpreting"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-asl-interpreting"><i class="fa fa-asl-interpreting" title="fa fa-asl-interpreting" alt="fa fa-asl-interpreting"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-assistive-listening-systems"><i class="fa fa-assistive-listening-systems" title="fa fa-assistive-listening-systems" alt="fa fa-assistive-listening-systems"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-audio-description"><i class="fa fa-audio-description" title="fa fa-audio-description" alt="fa fa-audio-description"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-blind"><i class="fa fa-blind" title="fa fa-blind" alt="fa fa-blind"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-braille"><i class="fa fa-braille" title="fa fa-braille" alt="fa fa-braille"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-deaf"><i class="fa fa-deaf" title="fa fa-deaf" alt="fa fa-deaf"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-deafness"><i class="fa fa-deafness" title="fa fa-deafness" alt="fa fa-deafness"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-envira"><i class="fa fa-envira" title="fa fa-envira" alt="fa fa-envira"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-first-order"><i class="fa fa-first-order" title="fa fa-first-order" alt="fa fa-first-order"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gitlab"><i class="fa fa-gitlab" title="fa fa-gitlab" alt="fa fa-gitlab"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-glide"><i class="fa fa-glide" title="fa fa-glide" alt="fa fa-glide"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-glide-g"><i class="fa fa-glide-g" title="fa fa-glide-g" alt="fa fa-glide-g"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-hard-of-hearing"><i class="fa fa-hard-of-hearing" title="fa fa-hard-of-hearing" alt="fa fa-hard-of-hearing"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-low-vision"><i class="fa fa-low-vision" title="fa fa-low-vision" alt="fa fa-low-vision"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-pied-piper"><i class="fa fa-pied-piper" title="fa fa-pied-piper" alt="fa fa-pied-piper"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-question-circle-o"><i class="fa fa-question-circle-o" title="fa fa-question-circle-o" alt="fa fa-question-circle-o"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-sign-language"><i class="fa fa-sign-language" title="fa fa-sign-language" alt="fa fa-sign-language"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-signing"><i class="fa fa-signing" title="fa fa-signing" alt="fa fa-signing"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-snapchat"><i class="fa fa-snapchat" title="fa fa-snapchat" alt="fa fa-snapchat"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-snapchat-ghost"><i class="fa fa-snapchat-ghost" title="fa fa-snapchat-ghost" alt="fa fa-snapchat-ghost"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-snapchat-square"><i class="fa fa-snapchat-square" title="fa fa-snapchat-square" alt="fa fa-snapchat-square"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-themeisle"><i class="fa fa-themeisle" title="fa fa-themeisle" alt="fa fa-themeisle"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-universal-access"><i class="fa fa-universal-access" title="fa fa-universal-access" alt="fa fa-universal-access"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-viadeo"><i class="fa fa-viadeo" title="fa fa-viadeo" alt="fa fa-viadeo"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-viadeo-square"><i class="fa fa-viadeo-square" title="fa fa-viadeo-square" alt="fa fa-viadeo-square"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-volume-control-phone"><i class="fa fa-volume-control-phone" title="fa fa-volume-control-phone" alt="fa fa-volume-control-phone"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-wheelchair-alt"><i class="fa fa-wheelchair-alt" title="fa fa-wheelchair-alt" alt="fa fa-wheelchair-alt"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-wpbeginner"><i class="fa fa-wpbeginner" title="fa fa-wpbeginner" alt="fa fa-wpbeginner"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-wpforms"><i class="fa fa-wpforms" title="fa fa-wpforms" alt="fa fa-wpforms"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-yoast"><i class="fa fa-yoast" title="fa fa-yoast" alt="fa fa-yoast"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-font-awesome"><i class="fa fa-font-awesome" title="fa fa" alt="fa fa"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-font-awesome"><i class="fa fa-font-awesome" title="fa fa-font-awesome" alt="fa fa-font-awesome"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-google-plus-official"><i class="fa fa-google-plus-official" title="fa fa-google-plus-official" alt="fa fa-google-plus-official"></i></a></div>

		                                    </div>
		                                </section>

		                               <section>
		                                    <h4 class="page-header header-title"><b>20 New Icons in 4.5 </b></h4>
		                                    <div class="row">
		                                        <div class="icon"><a href="#" data-icon="fa fa-bluetooth"><i class="fa fa-bluetooth" title="fa fa-bluetooth" alt="fa fa-bluetooth"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bluetooth-b"><i class="fa fa-bluetooth-b" title="fa fa-bluetooth-b" alt="fa fa-bluetooth-b"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-codiepie"><i class="fa fa-codiepie" title="fa fa-codiepie" alt="fa fa-codiepie"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-credit-card-alt"><i class="fa fa-credit-card-alt" title="fa fa-credit-card-alt" alt="fa fa-credit-card-alt"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-edge"><i class="fa fa-edge" title="fa fa-edge" alt="fa fa-edge"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-fort-awesome"><i class="fa fa-fort-awesome" title="fa fa-fort-awesome" alt="fa fa-fort-awesome"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-hashtag"><i class="fa fa-hashtag" title="fa fa-hashtag" alt="fa fa-hashtag"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-mixcloud"><i class="fa fa-mixcloud" title="fa fa-mixcloud" alt="fa fa-mixcloud"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-modx"><i class="fa fa-modx" title="fa fa-modx" alt="fa fa-modx"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-pause-circle"><i class="fa fa-pause-circle" title="fa fa-pause-circle" alt="fa fa-pause-circle"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-pause-circle-o"><i class="fa fa-pause-circle-o" title="fa fa-pause-circle-o" alt="fa fa-pause-circle-o"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-percent"><i class="fa fa-percent" title="fa fa-percent" alt="fa fa-percent"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-product-hunt"><i class="fa fa-product-hunt" title="fa fa-product-hunt" alt="fa fa-product-hunt"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-reddit-alien"><i class="fa fa-reddit-alien" title="fa fa-reddit-alien" alt="fa fa-reddit-alien"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-scribd"><i class="fa fa-scribd" title="fa fa-scribd" alt="fa fa-scribd"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-shopping-bag"><i class="fa fa-shopping-bag" title="fa fa-shopping-bag" alt="fa fa-shopping-bag"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-shopping-basket"><i class="fa fa-shopping-basket" title="fa fa-shopping-basket" alt="fa fa-shopping-basket"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-stop-circle"><i class="fa fa-stop-circle" title="fa fa-stop-circle" alt="fa fa-stop-circle"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-stop-circle-o"><i class="fa fa-stop-circle-o" title="fa fa-stop-circle-o" alt="fa fa-stop-circle-o"></i></a></div>
    <div class="icon"><a href="#" data-icon="fa fa-usb"><i class="fa fa-usb" title="fa fa-usb" alt="fa fa-usb"></i></a></div>

		                                    </div>
		                                </section>

                                        <section>
		                                    <h4 class="page-header header-title"><b>66 Icons in 4.4 </b></h4>
		                                    <div class="row">
		                                        <div class="icon"><a href="#" data-icon="fa fa-500px"><i class="fa fa-500px" title="fa fa-500px" alt="fa fa-500px"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-amazon"><i class="fa fa-amazon" title="fa fa-amazon" alt="fa fa-amazon"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-balance-scale"><i class="fa fa-balance-scale" title="fa fa-balance-scale" alt="fa fa-balance-scale"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-0"><i class="fa fa-battery-0" title="fa fa-battery-0" alt="fa fa-battery-0"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-1"><i class="fa fa-battery-1" title="fa fa-battery-1" alt=" fa fa-battery-1"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-2"><i class="fa fa-battery-2" title="fa fa-battery-2" alt=" fa fa-battery-2"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-3"><i class="fa fa-battery-3" title="fa fa-battery-3" alt=" fa fa-battery-3"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-4"><i class="fa fa-battery-4" title="fa fa-battery-4" alt=" fa fa-battery-4"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-empty"><i class="fa fa-battery-empty" title="fa fa-battery-empty" alt=" fa fa-battery-empty"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-full"><i class="fa fa-battery-full" title="fa fa-battery-full" alt=" fa fa-battery-full"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-half"><i class="fa fa-battery-half" title="fa fa-battery-half" alt=" fa fa-battery-half"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-quarter"><i class="fa fa-battery-quarter" title="fa fa-battery-quarter" alt=" fa fa-battery-quarter"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-battery-three-quarters"><i class="fa fa-battery-three-quarters" title="fa fa-battery-three-quarters" alt=" fa fa-battery-three-quarters"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-black-tie"><i class="fa fa-black-tie" title="fa fa-black-tie" alt=" fa fa-black-tie"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-calendar-check-o"><i class="fa fa-calendar-check-o" title="fa fa-calendar-check-o" alt=" fa fa-calendar-check-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-calendar-minus-o"><i class="fa fa-calendar-minus-o" title="fa fa-calendar-minus-o" alt=" fa fa-calendar-minus-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-calendar-plus-o"><i class="fa fa-calendar-plus-o" title="fa fa-calendar-plus-o" alt=" fa fa-calendar-plus-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-calendar-times-o"><i class="fa fa-calendar-times-o" title="fa fa-calendar-times-o" alt=" fa fa-calendar-times-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cc-diners-club"><i class="fa fa-cc-diners-club" title="fa fa-cc-diners-club" alt=" fa fa-cc-diners-club"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cc-jcb"><i class="fa fa-cc-jcb" title="fa fa-cc-jcb" alt=" fa fa-cc-jcb"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chrome"><i class="fa fa-chrome" title="fa fa-chrome" alt=" fa fa-chrome"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-clone"><i class="fa fa-clone" title="fa fa-clone" alt=" fa fa-clone"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-commenting"><i class="fa fa-commenting" title="fa fa-commenting" alt=" fa fa-commenting"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-commenting-o"><i class="fa fa-commenting-o" title="fa fa-commenting-o" alt=" fa fa-commenting-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-contao"><i class="fa fa-contao" title="fa fa-contao" alt=" fa fa-contao"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-creative-commons"><i class="fa fa-creative-commons" title="fa fa-creative-commons" alt=" fa fa-creative-commons"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-expeditedssl"><i class="fa fa-expeditedssl" title="fa fa-expeditedssl" alt=" fa fa-expeditedssl"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-firefox"><i class="fa fa-firefox" title="fa fa-firefox" alt=" fa fa-firefox"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-fonticons"><i class="fa fa-fonticons" title="fa fa-fonticons" alt=" fa fa-fonticons"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-genderless"><i class="fa fa-genderless" title="fa fa-genderless" alt=" fa fa-genderless"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-get-pocket"><i class="fa fa-get-pocket" title="fa fa-get-pocket" alt=" fa fa-get-pocket"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gg"><i class="fa fa-gg" title="fa fa-gg" alt=" fa fa-gg"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gg-circle"><i class="fa fa-gg-circle" title="fa fa-gg-circle" alt=" fa fa-gg-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-grab-o"><i class="fa fa-hand-grab-o" title="fa fa-hand-grab-o" alt=" fa fa-hand-grab-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-lizard-o"><i class="fa fa-hand-lizard-o" title="fa fa-hand-lizard-o" alt=" fa fa-hand-lizard-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-paper-o"><i class="fa fa-hand-paper-o" title="fa fa-hand-paper-o" alt=" fa fa-hand-paper-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-peace-o"><i class="fa fa-hand-peace-o" title="fa fa-hand-peace-o" alt=" fa fa-hand-peace-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-pointer-o"><i class="fa fa-hand-pointer-o" title="fa fa-hand-pointer-o" alt=" fa fa-hand-pointer-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-rock-o"><i class="fa fa-hand-rock-o" title="fa fa-hand-rock-o" alt=" fa fa-hand-rock-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-scissors-o"><i class="fa fa-hand-scissors-o" title="fa fa-hand-scissors-o" alt=" fa fa-hand-scissors-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-spock-o"><i class="fa fa-hand-spock-o" title="fa fa-hand-spock-o" alt=" fa fa-hand-spock-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-stop-o"><i class="fa fa-hand-stop-o" title="fa fa-hand-stop-o" alt=" fa fa-hand-stop-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hourglass"><i class="fa fa-hourglass" title="fa fa-hourglass" alt=" fa fa-hourglass"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hourglass-1"><i class="fa fa-hourglass-1" title="fa fa-hourglass-1" alt=" fa fa-hourglass-1"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hourglass-2"><i class="fa fa-hourglass-2" title="fa fa-hourglass-2" alt=" fa fa-hourglass-2"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hourglass-3"><i class="fa fa-hourglass-3" title="fa fa-hourglass-3" alt=" fa fa-hourglass-3"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hourglass-end"><i class="fa fa-hourglass-end" title="fa fa-hourglass-end" alt=" fa fa-hourglass-end"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hourglass-half"><i class="fa fa-hourglass-half" title="fa fa-hourglass-half" alt=" fa fa-hourglass-half"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hourglass-o"><i class="fa fa-hourglass-o" title="fa fa-hourglass-o" alt=" fa fa-hourglass-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hourglass-start"><i class="fa fa-hourglass-start" title="fa fa-hourglass-start" alt=" fa fa-hourglass-start"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-houzz"><i class="fa fa-houzz" title="fa fa-houzz" alt=" fa fa-houzz"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-i-cursor"><i class="fa fa-i-cursor" title="fa fa-i-cursor" alt=" fa fa-i-cursor"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-industry"><i class="fa fa-industry" title="fa fa-industry" alt=" fa fa-industry"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-internet-explorer"><i class="fa fa-internet-explorer" title="fa fa-internet-explorer" alt=" fa fa-internet-explorer"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-map"><i class="fa fa-map" title="fa fa-map" alt=" fa fa-map"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-map-o"><i class="fa fa-map-o" title="fa fa-map-o" alt=" fa fa-map-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-map-pin"><i class="fa fa-map-pin" title="fa fa-map-pin" alt=" fa fa-map-pin"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-map-signs"><i class="fa fa-map-signs" title="fa fa-map-signs" alt=" fa fa-map-signs"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mouse-pointer"><i class="fa fa-mouse-pointer" title="fa fa-mouse-pointer" alt=" fa fa-mouse-pointer"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-object-group"><i class="fa fa-object-group" title="fa fa-object-group" alt=" fa fa-object-group"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-object-ungroup"><i class="fa fa-object-ungroup" title="fa fa-object-ungroup" alt=" fa fa-object-ungroup"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-odnoklassniki"><i class="fa fa-odnoklassniki" title="fa fa-odnoklassniki" alt=" fa fa-odnoklassniki"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-odnoklassniki-square"><i class="fa fa-odnoklassniki-square" title="fa fa-odnoklassniki-square" alt=" fa fa-odnoklassniki-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-opencart"><i class="fa fa-opencart" title="fa fa-opencart" alt=" fa fa-opencart"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-opera"><i class="fa fa-opera" title="fa fa-opera" alt=" fa fa-opera"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-optin-monster"><i class="fa fa-optin-monster" title="fa fa-optin-monster" alt=" fa fa-optin-monster"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-registered"><i class="fa fa-registered" title="fa fa-registered" alt=" fa fa-registered"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-safari"><i class="fa fa-safari" title="fa fa-safari" alt=" fa fa-safari"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sticky-note"><i class="fa fa-sticky-note" title="fa fa-sticky-note" alt=" fa fa-sticky-note"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sticky-note-o"><i class="fa fa-sticky-note-o" title="fa fa-sticky-note-o" alt=" fa fa-sticky-note-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-television"><i class="fa fa-television" title="fa fa-television" alt=" fa fa-television"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-trademark"><i class="fa fa-trademark" title="fa fa-trademark" alt=" fa fa-trademark"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tripadvisor"><i class="fa fa-tripadvisor" title="fa fa-tripadvisor" alt=" fa fa-tripadvisor"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tv"><i class="fa fa-tv" title="fa fa-tv" alt=" fa fa-tv"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-vimeo"><i class="fa fa-vimeo" title="fa fa-vimeo" alt=" fa fa-vimeo"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-wikipedia-w"><i class="fa fa-wikipedia-w" title="fa fa-wikipedia-w" alt=" fa fa-wikipedia-w"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-y-combinator"><i class="fa fa-y-combinator" title="fa fa-y-combinator" alt=" fa fa-y-combinator"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-yc"><i class="fa fa-yc" title="fa fa-yc" alt=" fa fa-yc"></i></a></div>

		                                    </div>
		                                </section>

		                                <section>
		                                	<h4 class="page-header header-title"><b>40 Icons in 4.3 </b></h4>

		                                    <div class="row">
		                                        <div class="icon"><a href="#" data-icon="fa fa-bed"><i class="fa fa-bed" title="fa-bed" alt=" fa-bed"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-buysellads"><i class="fa fa-buysellads" title="fa-buysellads" alt="fa-buysellads"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cart-arrow-down"><i class="fa fa-cart-arrow-down" title="fa-cart-arrow-down" alt="fa-cart-arrow-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cart-plus"><i class="fa fa-cart-plus" title="fa-cart-plus" alt="fa-cart-plus"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-connectdevelop"><i class="fa fa-connectdevelop" title="fa-connectdevelop" alt="fa-connectdevelop"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-dashcube"><i class="fa fa-dashcube" title="fa-dashcube" alt="fa-dashcube"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-diamond"><i class="fa fa-diamond" title="fa-diamond" alt="fa-diamond"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-facebook-official"><i class="fa fa-facebook-official" title="fa-facebook-official" alt="fa-facebook-official"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-forumbee "><i class="fa fa-forumbee " title="fa-forumbee " alt="fa-forumbee "></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-heartbeat"><i class="fa fa-heartbeat" title="fa-heartbeat" alt="fa-heartbeat"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bed"><i class="fa fa-bed" title="fa-bed" alt="fa-bed"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-leanpub"><i class="fa fa-leanpub" title="fa-leanpub" alt="fa-leanpub"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mars"><i class="fa fa-mars" title="fa-mars" alt="fa-mars"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mars-double"><i class="fa fa-mars-double" title="fa-mars-double" alt="fa-mars-double"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mars-stroke"><i class="fa fa-mars-stroke" title="fa-mars-stroke" alt="fa-mars-stroke"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mars-stroke-h "><i class="fa fa-mars-stroke-h " title="fa-mars-stroke-h " alt="fa-mars-stroke-h "></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mars-stroke-v"><i class="fa fa-mars-stroke-v" title="fa-mars-stroke-v" alt="fa-mars-stroke-v"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-medium "><i class="fa fa-medium " title="fa-medium " alt="fa-medium "></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mercury"><i class="fa fa-mercury" title="fa-mercury" alt="fa-mercury"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-motorcycle"><i class="fa fa-motorcycle" title="fa-motorcycle" alt="fa-motorcycle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-neuter "><i class="fa fa-neuter " title="fa-neuter " alt="fa-neuter "></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pinterest-p "><i class="fa fa-pinterest-p " title="fa-pinterest-p " alt="fa-pinterest-p "></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sellsy"><i class="fa fa-sellsy" title="fa-sellsy" alt="fa-sellsy"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-server"><i class="fa fa-server" title="fa-server" alt="fa-server"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ship"><i class="fa fa-ship" title="fa-ship" alt="fa-ship"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-shirtsinbulk"><i class="fa fa-shirtsinbulk" title="fa-shirtsinbulk" alt="fa-shirtsinbulk"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-simplybuilt"><i class="fa fa-simplybuilt" title="fa-simplybuilt" alt="fa-simplybuilt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-skyatlas"><i class="fa fa-skyatlas" title="fa-skyatlas" alt="fa-skyatlas"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-street-view"><i class="fa fa-street-view" title="fa-street-view" alt="fa-street-view"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-subway"><i class="fa fa-subway" title="fa-subway" alt="fa-subway"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-train"><i class="fa fa-train" title="fa-train" alt="fa-train"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-transgender"><i class="fa fa-transgender" title="fa-transgender" alt="fa-transgender"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-transgender-alt "><i class="fa fa-transgender-alt " title="fa-transgender-alt
		                                        " alt="fa-transgender-alt
		                                        "></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-user-plus"><i class="fa fa-user-plus" title="fa-user-plus
		                                        " alt="fa-user-plus
		                                        "></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-user-secret"><i class="fa fa-user-secret" title="fa-user-secret" alt="fa-user-secret"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-user-times"><i class="fa fa-user-times" title="fa-user-times
		                                        " alt="fa-user-times
		                                        "></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-venus"><i class="fa fa-venus" title="fa-venus" alt="fa-venus"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-venus-double"><i class="fa fa-venus-double" title="fa-venus-double" alt="fa-venus-double"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-venus-mars "><i class="fa fa-venus-mars " title="fa-venus-mars " alt="fa-venus-mars "></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-viacoin"><i class="fa fa-viacoin" title="fa-viacoin" alt="fa-viacoin"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-whatsapp"><i class="fa fa-whatsapp" title="fa-whatsapp" alt="fa-whatsapp"></i></a></div>


		                                    </div>
		                                </section>

		                                <section id="new-icons">
		                                    <h4 class="page-header header-title"><b>40 Icons in 4.2</b></h4>
		                                    <div class="row">
		                                        <div class="icon"><a href="#" data-icon="fa fa-angellist"><i class="fa fa-angellist" title="fa-angellist" alt="fa-angellist"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-area-chart"><i class="fa fa-area-chart" title="fa-area-chart" alt="fa-area-chart"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-at"><i class="fa fa-at" title="fa-at" alt="fa-at"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bell-slash"><i class="fa fa-bell-slash" title="fa-bell-slash" alt="fa-bell-slash"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bell-slash-o"><i class="fa fa-bell-slash-o" title="fa-bell-slash-o" alt="fa-bell-slash-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bicycle"><i class="fa fa-bicycle" title="fa-bicycle" alt="fa-bicycle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-binoculars"><i class="fa fa-binoculars" title="fa-binoculars" alt="fa-binoculars"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-birthday-cake"><i class="fa fa-birthday-cake" title="fa-birthday-cake" alt="fa-birthday-cake"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bus"><i class="fa fa-bus" title="fa-bus" alt="fa-bus"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-calculator"><i class="fa fa-calculator" title="fa-calculator" alt="fa-calculator"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cc"><i class="fa fa-cc" title="fa-cc" alt="fa-cc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cc-amex"><i class="fa fa-cc-amex" title="fa-cc-amex" alt="fa-cc-amex"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cc-discover"><i class="fa fa-cc-discover" title="fa-cc-discover" alt="fa-cc-discover"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cc-mastercard"><i class="fa fa-cc-mastercard" title="fa-cc-mastercard" alt="fa-cc-mastercard"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cc-paypal"><i class="fa fa-cc-paypal" title="fa-cc-paypal" alt="fa-cc-paypal"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cc-stripe"><i class="fa fa-cc-stripe" title="fa-cc-stripe" alt="fa-cc-stripe"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cc-visa"><i class="fa fa-cc-visa" title="fa-cc-visa" alt="fa-cc-visa"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-copyright"><i class="fa fa-copyright" title="fa-copyright" alt="fa-copyright"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-eyedropper"><i class="fa fa-eyedropper" title="fa-eyedropper" alt="fa-eyedropper"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-futbol-o"><i class="fa fa-futbol-o" title="fa-futbol-o" alt="fa-futbol-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-google-wallet"><i class="fa fa-google-wallet" title="fa-google-wallet" alt="fa-google-wallet"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ils"><i class="fa fa-ils" title="fa-ils" alt="fa-ils"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ioxhost"><i class="fa fa-ioxhost" title="fa-ioxhost" alt="fa-ioxhost"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-lastfm"><i class="fa fa-lastfm" title="fa-lastfm" alt="fa-lastfm"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-lastfm-square"><i class="fa fa-lastfm-square" title="fa-lastfm-square" alt="fa-lastfm-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-line-chart"><i class="fa fa-line-chart" title="fa-line-chart" alt="fa-line-chart"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-meanpath"><i class="fa fa-meanpath" title="fa-meanpath" alt="fa-meanpath"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-newspaper-o"><i class="fa fa-newspaper-o" title="fa-newspaper-o" alt="fa-newspaper-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-paint-brush"><i class="fa fa-paint-brush" title="fa-paint-brush" alt="fa-paint-brush"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-paypal"><i class="fa fa-paypal" title="fa-paypal" alt="fa-paypal"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pie-chart"><i class="fa fa-pie-chart" title="fa-pie-chart" alt="fa-pie-chart"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-plug"><i class="fa fa-plug" title="fa-plug" alt="fa-plug"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-shekel"><i class="fa fa-shekel" title="fa-shekel" alt="fa-shekel"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sheqel"><i class="fa fa-sheqel" title="fa-sheqel" alt="fa-sheqel"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-slideshare"><i class="fa fa-slideshare" title="fa-slideshare" alt="fa-slideshare"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-soccer-ball-o"><i class="fa fa-soccer-ball-o" title="fa-soccer-ball-o" alt="fa-soccer-ball-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-off"><i class="fa fa-toggle-off" title="fa-toggle-off" alt="fa-toggle-off"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-on"><i class="fa fa-toggle-on" title="fa-toggle-on" alt="fa-toggle-on"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-trash"><i class="fa fa-trash" title="fa-trash" alt="fa-trash"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tty"><i class="fa fa-tty" title="fa-tty" alt="fa-tty"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-twitch"><i class="fa fa-twitch" title="fa-twitch" alt="fa-twitch"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-wifi"><i class="fa fa-wifi" title="fa-wifi" alt="fa-wifi"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-yelp"><i class="fa fa-yelp" title="fa-yelp" alt="fa-yelp"></i></a></div>


		                                    </div>
		                                </section>
		                                <section id="web-application">
		                                    <h4 class="page-header header-title"><b>Web Application</b></h4>
		                                    <div class="row">
		                                        <div class="icon"><a href="#" data-icon="fa fa-adjust"><i class="fa fa-adjust" title="fa-adjust" alt="fa-adjust"></i></a></div>
		                                        <div class="icon"><a href="#" data-icon="fa fa-anchor"><i class="fa fa-anchor" title="fa-anchor" alt="fa-anchor"></i></a></div>
		                                        <div class="icon"><a href="#" data-icon="fa fa-archive"><i class="fa fa-archive" title="fa-archive" alt="fa-archive"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrows"><i class="fa fa-arrows" title="fa-arrows" alt="fa-arrows"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrows-h"><i class="fa fa-arrows-h" title="fa-arrows-h" alt="fa-arrows-h"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrows-v"><i class="fa fa-arrows-v" title="fa-arrows-v" alt="fa-arrows-v"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-asterisk"><i class="fa fa-asterisk" title="fa-asterisk" alt="fa-asterisk"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-automobile"><i class="fa fa-automobile" title="fa-automobile" alt="fa-automobile"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ban"><i class="fa fa-ban" title="fa-ban" alt="fa-ban"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bank"><i class="fa fa-bank" title="fa-bank" alt="fa-bank"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bar-chart-o"><i class="fa fa-bar-chart-o" title="fa-bar-chart-o" alt="fa-bar-chart-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-barcode"><i class="fa fa-barcode" title="fa-barcode" alt="fa-barcode"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bars"><i class="fa fa-bars" title="fa-bars" alt="fa-bars"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-beer"><i class="fa fa-beer" title="fa-beer" alt="fa-beer"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bell"><i class="fa fa-bell" title="fa-bell" alt="fa-bell"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bell-o"><i class="fa fa-bell-o" title="fa-bell-o" alt="fa-bell-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bolt"><i class="fa fa-bolt" title="fa-bolt" alt="fa-bolt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bomb"><i class="fa fa-bomb" title="fa-bomb" alt="fa-bomb"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-book"><i class="fa fa-book" title="fa-book" alt="fa-book"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bookmark"><i class="fa fa-bookmark" title="fa-bookmark" alt="fa-bookmark"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bookmark-o"><i class="fa fa-bookmark-o" title="fa-bookmark-o" alt="fa-bookmark-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-briefcase"><i class="fa fa-briefcase" title="fa-briefcase" alt="fa-briefcase"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bug"><i class="fa fa-bug" title="fa-bug" alt="fa-bug"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-building"><i class="fa fa-building" title="fa-building" alt="fa-building"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-building-o"><i class="fa fa-building-o" title="fa-building-o" alt="fa-building-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bullhorn"><i class="fa fa-bullhorn" title="fa-bullhorn" alt="fa-bullhorn"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bullseye"><i class="fa fa-bullseye" title="fa-bullseye" alt="fa-bullseye"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cab"><i class="fa fa-cab" title="fa-cab" alt="fa-cab"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-calendar"><i class="fa fa-calendar" title="fa-calendar" alt="fa-calendar"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-calendar-o"><i class="fa fa-calendar-o" title="fa-calendar-o" alt="fa-calendar-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-camera"><i class="fa fa-camera" title="fa-camera" alt="fa-camera"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-camera-retro"><i class="fa fa-camera-retro" title="fa-camera-retro" alt="fa-camera-retro"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-car"><i class="fa fa-car" title="fa-car" alt="fa-car"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-square-o-down"><i class="fa fa-caret-square-o-down" title="fa-caret-square-o-down" alt="fa-caret-square-o-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-square-o-left"><i class="fa fa-caret-square-o-left" title="fa-caret-square-o-left" alt="fa-caret-square-o-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-square-o-right"><i class="fa fa-caret-square-o-right" title="fa-caret-square-o-right" alt="fa-caret-square-o-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-square-o-up"><i class="fa fa-caret-square-o-up" title="fa-caret-square-o-up" alt="fa-caret-square-o-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-certificate"><i class="fa fa-certificate" title="fa-certificate" alt="fa-certificate"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-check"><i class="fa fa-check" title="fa-check" alt="fa-check"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-check-circle"><i class="fa fa-check-circle" title="fa-check-circle" alt="fa-check-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-check-circle-o"><i class="fa fa-check-circle-o" title="fa-check-circle-o" alt="fa-check-circle-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-check-square"><i class="fa fa-check-square" title="fa-check-square" alt="fa-check-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-check-square-o"><i class="fa fa-check-square-o" title="fa-check-square-o" alt="fa-check-square-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-child"><i class="fa fa-child" title="fa-child" alt="fa-child"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-circle"><i class="fa fa-circle" title="fa-circle" alt="fa-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-circle-o"><i class="fa fa-circle-o" title="fa-circle-o" alt="fa-circle-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-circle-o-notch"><i class="fa fa-circle-o-notch" title="fa-circle-o-notch" alt="fa-circle-o-notch"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-circle-thin"><i class="fa fa-circle-thin" title="fa-circle-thin" alt="fa-circle-thin"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-clock-o"><i class="fa fa-clock-o" title="fa-clock-o" alt="fa-clock-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cloud"><i class="fa fa-cloud" title="fa-cloud" alt="fa-cloud"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cloud-download"><i class="fa fa-cloud-download" title="fa-cloud-download" alt="fa-cloud-download"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cloud-upload"><i class="fa fa-cloud-upload" title="fa-cloud-upload" alt="fa-cloud-upload"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-code"><i class="fa fa-code" title="fa-code" alt="fa-code"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-code-fork"><i class="fa fa-code-fork" title="fa-code-fork" alt="fa-code-fork"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-coffee"><i class="fa fa-coffee" title="fa-coffee" alt="fa-coffee"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cog"><i class="fa fa-cog" title="fa-cog" alt="fa-cog"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cogs"><i class="fa fa-cogs" title="fa-cogs" alt="fa-cogs"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-comment"><i class="fa fa-comment" title="fa-comment" alt="fa-comment"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-comment-o"><i class="fa fa-comment-o" title="fa-comment-o" alt="fa-comment-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-comments"><i class="fa fa-comments" title="fa-comments" alt="fa-comments"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-comments-o"><i class="fa fa-comments-o" title="fa-comments-o" alt="fa-comments-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-compass"><i class="fa fa-compass" title="fa-compass" alt="fa-compass"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-credit-card"><i class="fa fa-credit-card" title="fa-credit-card" alt="fa-credit-card"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-crop"><i class="fa fa-crop" title="fa-crop" alt="fa-crop"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-crosshairs"><i class="fa fa-crosshairs" title="fa-crosshairs" alt="fa-crosshairs"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cube"><i class="fa fa-cube" title="fa-cube" alt="fa-cube"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cubes"><i class="fa fa-cubes" title="fa-cubes" alt="fa-cubes"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cutlery"><i class="fa fa-cutlery" title="fa-cutlery" alt="fa-cutlery"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-dashboard"><i class="fa fa-dashboard" title="fa-dashboard" alt="fa-dashboard"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-database"><i class="fa fa-database" title="fa-database" alt="fa-database"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-desktop"><i class="fa fa-desktop" title="fa-desktop" alt="fa-desktop"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-dot-circle-o"><i class="fa fa-dot-circle-o" title="fa-dot-circle-o" alt="fa-dot-circle-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-download"><i class="fa fa-download" title="fa-download" alt="fa-download"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-edit"><i class="fa fa-edit" title="fa-edit" alt="fa-edit"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ellipsis-h"><i class="fa fa-ellipsis-h" title="fa-ellipsis-h" alt="fa-ellipsis-h"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ellipsis-v"><i class="fa fa-ellipsis-v" title="fa-ellipsis-v" alt="fa-ellipsis-v"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-envelope"><i class="fa fa-envelope" title="fa-envelope" alt="fa-envelope"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-envelope-o"><i class="fa fa-envelope-o" title="fa-envelope-o" alt="fa-envelope-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-envelope-square"><i class="fa fa-envelope-square" title="fa-envelope-square" alt="fa-envelope-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-eraser"><i class="fa fa-eraser" title="fa-eraser" alt="fa-eraser"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-exchange"><i class="fa fa-exchange" title="fa-exchange" alt="fa-exchange"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-exclamation"><i class="fa fa-exclamation" title="fa-exclamation" alt="fa-exclamation"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-exclamation-circle"><i class="fa fa-exclamation-circle" title="fa-exclamation-circle" alt="fa-exclamation-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-exclamation-triangle"><i class="fa fa-exclamation-triangle" title="fa-exclamation-triangle" alt="fa-exclamation-triangle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-external-link"><i class="fa fa-external-link" title="fa-external-link" alt="fa-external-link"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-external-link-square"><i class="fa fa-external-link-square" title="fa-external-link-square" alt="fa-external-link-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-eye"><i class="fa fa-eye" title="fa-eye" alt="fa-eye"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-eye-slash"><i class="fa fa-eye-slash" title="fa-eye-slash" alt="fa-eye-slash"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-fax"><i class="fa fa-fax" title="fa-fax" alt="fa-fax"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-female"><i class="fa fa-female" title="fa-female" alt="fa-female"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-fighter-jet"><i class="fa fa-fighter-jet" title="fa-fighter-jet" alt="fa-fighter-jet"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-archive-o"><i class="fa fa-file-archive-o" title="fa-file-archive-o" alt="fa-file-archive-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-audio-o"><i class="fa fa-file-audio-o" title="fa-file-audio-o" alt="fa-file-audio-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-code-o"><i class="fa fa-file-code-o" title="fa-file-code-o" alt="fa-file-code-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-excel-o"><i class="fa fa-file-excel-o" title="fa-file-excel-o" alt="fa-file-excel-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-image-o"><i class="fa fa-file-image-o" title="fa-file-image-o" alt="fa-file-image-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-movie-o"><i class="fa fa-file-movie-o" title="fa-file-movie-o" alt="fa-file-movie-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-pdf-o"><i class="fa fa-file-pdf-o" title="fa-file-pdf-o" alt="fa-file-pdf-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-photo-o"><i class="fa fa-file-photo-o" title="fa-file-photo-o" alt="fa-file-photo-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-picture-o"><i class="fa fa-file-picture-o" title="fa-file-picture-o" alt="fa-file-picture-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-powerpoint-o"><i class="fa fa-file-powerpoint-o" title="fa-file-powerpoint-o" alt="fa-file-powerpoint-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-sound-o"><i class="fa fa-file-sound-o" title="fa-file-sound-o" alt="fa-file-sound-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-video-o"><i class="fa fa-file-video-o" title="fa-file-video-o" alt="fa-file-video-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-word-o"><i class="fa fa-file-word-o" title="fa-file-word-o" alt="fa-file-word-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-zip-o"><i class="fa fa-file-zip-o" title="fa-file-zip-o" alt="fa-file-zip-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-film"><i class="fa fa-film" title="fa-film" alt="fa-film"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-filter"><i class="fa fa-filter" title="fa-filter" alt="fa-filter"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-fire"><i class="fa fa-fire" title="fa-fire" alt="fa-fire"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-fire-extinguisher"><i class="fa fa-fire-extinguisher" title="fa-fire-extinguisher" alt="fa-fire-extinguisher"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-flag"><i class="fa fa-flag" title="fa-flag" alt="fa-flag"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-flag-checkered"><i class="fa fa-flag-checkered" title="fa-flag-checkered" alt="fa-flag-checkered"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-flag-o"><i class="fa fa-flag-o" title="fa-flag-o" alt="fa-flag-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-flash"><i class="fa fa-flash" title="fa-flash" alt="fa-flash"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-flask"><i class="fa fa-flask" title="fa-flask" alt="fa-flask"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-folder"><i class="fa fa-folder" title="fa-folder" alt="fa-folder"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-folder-o"><i class="fa fa-folder-o" title="fa-folder-o" alt="fa-folder-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-folder-open"><i class="fa fa-folder-open" title="fa-folder-open" alt="fa-folder-open"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-folder-open-o"><i class="fa fa-folder-open-o" title="fa-folder-open-o" alt="fa-folder-open-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-frown-o"><i class="fa fa-frown-o" title="fa-frown-o" alt="fa-frown-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gamepad"><i class="fa fa-gamepad" title="fa-gamepad" alt="fa-gamepad"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gavel"><i class="fa fa-gavel" title="fa-gavel" alt="fa-gavel"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gear"><i class="fa fa-gear" title="fa-gear" alt="fa-gear"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gears"><i class="fa fa-gears" title="fa-gears" alt="fa-gears"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gift"><i class="fa fa-gift" title="fa-gift" alt="fa-gift"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-glass"><i class="fa fa-glass" title="fa-glass" alt="fa-glass"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-globe"><i class="fa fa-globe" title="fa-globe" alt="fa-globe"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-graduation-cap"><i class="fa fa-graduation-cap" title="fa-graduation-cap" alt="fa-graduation-cap"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-group"><i class="fa fa-group" title="fa-group" alt="fa-group"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hdd-o"><i class="fa fa-hdd-o" title="fa-hdd-o" alt="fa-hdd-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-headphones"><i class="fa fa-headphones" title="fa-headphones" alt="fa-headphones"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-heart"><i class="fa fa-heart" title="fa-heart" alt="fa-heart"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-heart-o"><i class="fa fa-heart-o" title="fa-heart-o" alt="fa-heart-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-history"><i class="fa fa-history" title="fa-history" alt="fa-history"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-home"><i class="fa fa-home" title="fa-home" alt="fa-home"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-image"><i class="fa fa-image" title="fa-image" alt="fa-image"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-inbox"><i class="fa fa-inbox" title="fa-inbox" alt="fa-inbox"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-info"><i class="fa fa-info" title="fa-info" alt="fa-info"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-info-circle"><i class="fa fa-info-circle" title="fa-info-circle" alt="fa-info-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-institution"><i class="fa fa-institution" title="fa-institution" alt="fa-institution"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-key"><i class="fa fa-key" title="fa-key" alt="fa-key"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-keyboard-o"><i class="fa fa-keyboard-o" title="fa-keyboard-o" alt="fa-keyboard-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-language"><i class="fa fa-language" title="fa-language" alt="fa-language"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-laptop"><i class="fa fa-laptop" title="fa-laptop" alt="fa-laptop"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-leaf"><i class="fa fa-leaf" title="fa-leaf" alt="fa-leaf"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-legal"><i class="fa fa-legal" title="fa-legal" alt="fa-legal"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-lemon-o"><i class="fa fa-lemon-o" title="fa-lemon-o" alt="fa-lemon-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-level-down"><i class="fa fa-level-down" title="fa-level-down" alt="fa-level-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-level-up"><i class="fa fa-level-up" title="fa-level-up" alt="fa-level-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-life-bouy"><i class="fa fa-life-bouy" title="fa-life-bouy" alt="fa-life-bouy"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-life-ring"><i class="fa fa-life-ring" title="fa-life-ring" alt="fa-life-ring"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-life-saver"><i class="fa fa-life-saver" title="fa-life-saver" alt="fa-life-saver"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-lightbulb-o"><i class="fa fa-lightbulb-o" title="fa-lightbulb-o" alt="fa-lightbulb-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-location-arrow"><i class="fa fa-location-arrow" title="fa-location-arrow" alt="fa-location-arrow"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-lock"><i class="fa fa-lock" title="fa-lock" alt="fa-lock"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-magic"><i class="fa fa-magic" title="fa-magic" alt="fa-magic"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-magnet"><i class="fa fa-magnet" title="fa-magnet" alt="fa-magnet"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mail-forward"><i class="fa fa-mail-forward" title="fa-mail-forward" alt="fa-mail-forward"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mail-reply"><i class="fa fa-mail-reply" title="fa-mail-reply" alt="fa-mail-reply"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mail-reply-all"><i class="fa fa-mail-reply-all" title="fa-mail-reply-all" alt="fa-mail-reply-all"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-male"><i class="fa fa-male" title="fa-male" alt="fa-male"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-map-marker"><i class="fa fa-map-marker" title="fa-map-marker" alt="fa-map-marker"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-meh-o"><i class="fa fa-meh-o" title="fa-meh-o" alt="fa-meh-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-microphone"><i class="fa fa-microphone" title="fa-microphone" alt="fa-microphone"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-microphone-slash"><i class="fa fa-microphone-slash" title="fa-microphone-slash" alt="fa-microphone-slash"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-minus"><i class="fa fa-minus" title="fa-minus" alt="fa-minus"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-minus-circle"><i class="fa fa-minus-circle" title="fa-minus-circle" alt="fa-minus-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-minus-square"><i class="fa fa-minus-square" title="fa-minus-square" alt="fa-minus-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-minus-square-o"><i class="fa fa-minus-square-o" title="fa-minus-square-o" alt="fa-minus-square-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mobile"><i class="fa fa-mobile" title="fa-mobile" alt="fa-mobile"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mobile-phone"><i class="fa fa-mobile-phone" title="fa-mobile-phone" alt="fa-mobile-phone"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-money"><i class="fa fa-money" title="fa-money" alt="fa-money"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-moon-o"><i class="fa fa-moon-o" title="fa-moon-o" alt="fa-moon-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-mortar-board"><i class="fa fa-mortar-board" title="fa-mortar-board" alt="fa-mortar-board"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-music"><i class="fa fa-music" title="fa-music" alt="fa-music"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-navicon"><i class="fa fa-navicon" title="fa-navicon" alt="fa-navicon"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-paper-plane"><i class="fa fa-paper-plane" title="fa-paper-plane" alt="fa-paper-plane"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-paper-plane-o"><i class="fa fa-paper-plane-o" title="fa-paper-plane-o" alt="fa-paper-plane-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-paw"><i class="fa fa-paw" title="fa-paw" alt="fa-paw"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pencil"><i class="fa fa-pencil" title="fa-pencil" alt="fa-pencil"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pencil-square"><i class="fa fa-pencil-square" title="fa-pencil-square" alt="fa-pencil-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pencil-square-o"><i class="fa fa-pencil-square-o" title="fa-pencil-square-o" alt="fa-pencil-square-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-phone"><i class="fa fa-phone" title="fa-phone" alt="fa-phone"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-phone-square"><i class="fa fa-phone-square" title="fa-phone-square" alt="fa-phone-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-photo"><i class="fa fa-photo" title="fa-photo" alt="fa-photo"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-picture-o"><i class="fa fa-picture-o" title="fa-picture-o" alt="fa-picture-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-plane"><i class="fa fa-plane" title="fa-plane" alt="fa-plane"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-plus"><i class="fa fa-plus" title="fa-plus" alt="fa-plus"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-plus-circle"><i class="fa fa-plus-circle" title="fa-plus-circle" alt="fa-plus-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-plus-square"><i class="fa fa-plus-square" title="fa-plus-square" alt="fa-plus-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-plus-square-o"><i class="fa fa-plus-square-o" title="fa-plus-square-o" alt="fa-plus-square-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-power-off"><i class="fa fa-power-off" title="fa-power-off" alt="fa-power-off"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-print"><i class="fa fa-print" title="fa-print" alt="fa-print"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-puzzle-piece"><i class="fa fa-puzzle-piece" title="fa-puzzle-piece" alt="fa-puzzle-piece"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-qrcode"><i class="fa fa-qrcode" title="fa-qrcode" alt="fa-qrcode"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-question"><i class="fa fa-question" title="fa-question" alt="fa-question"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-question-circle"><i class="fa fa-question-circle" title="fa-question-circle" alt="fa-question-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-quote-left"><i class="fa fa-quote-left" title="fa-quote-left" alt="fa-quote-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-quote-right"><i class="fa fa-quote-right" title="fa-quote-right" alt="fa-quote-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-random"><i class="fa fa-random" title="fa-random" alt="fa-random"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-recycle"><i class="fa fa-recycle" title="fa-recycle" alt="fa-recycle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-refresh"><i class="fa fa-refresh" title="fa-refresh" alt="fa-refresh"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-reorder"><i class="fa fa-reorder" title="fa-reorder" alt="fa-reorder"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-reply"><i class="fa fa-reply" title="fa-reply" alt="fa-reply"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-reply-all"><i class="fa fa-reply-all" title="fa-reply-all" alt="fa-reply-all"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-retweet"><i class="fa fa-retweet" title="fa-retweet" alt="fa-retweet"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-road"><i class="fa fa-road" title="fa-road" alt="fa-road"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rocket"><i class="fa fa-rocket" title="fa-rocket" alt="fa-rocket"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rss"><i class="fa fa-rss" title="fa-rss" alt="fa-rss"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rss-square"><i class="fa fa-rss-square" title="fa-rss-square" alt="fa-rss-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-search"><i class="fa fa-search" title="fa-search" alt="fa-search"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-search-minus"><i class="fa fa-search-minus" title="fa-search-minus" alt="fa-search-minus"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-search-plus"><i class="fa fa-search-plus" title="fa-search-plus" alt="fa-search-plus"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-send"><i class="fa fa-send" title="fa-send" alt="fa-send"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-send-o"><i class="fa fa-send-o" title="fa-send-o" alt="fa-send-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-share"><i class="fa fa-share" title="fa-share" alt="fa-share"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-share-alt"><i class="fa fa-share-alt" title="fa-share-alt" alt="fa-share-alt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-share-alt-square"><i class="fa fa-share-alt-square" title="fa-share-alt-square" alt="fa-share-alt-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-share-square"><i class="fa fa-share-square" title="fa-share-square" alt="fa-share-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-share-square-o"><i class="fa fa-share-square-o" title="fa-share-square-o" alt="fa-share-square-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-shield"><i class="fa fa-shield" title="fa-shield" alt="fa-shield"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-shopping-cart"><i class="fa fa-shopping-cart" title="fa-shopping-cart" alt="fa-shopping-cart"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sign-in"><i class="fa fa-sign-in" title="fa-sign-in" alt="fa-sign-in"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sign-out"><i class="fa fa-sign-out" title="fa-sign-out" alt="fa-sign-out"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-signal"><i class="fa fa-signal" title="fa-signal" alt="fa-signal"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sitemap"><i class="fa fa-sitemap" title="fa-sitemap" alt="fa-sitemap"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sliders"><i class="fa fa-sliders" title="fa-sliders" alt="fa-sliders"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-smile-o"><i class="fa fa-smile-o" title="fa-smile-o" alt="fa-smile-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort"><i class="fa fa-sort" title="fa-sort" alt="fa-sort"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-alpha-asc"><i class="fa fa-sort-alpha-asc" title="fa-sort-alpha-asc" alt="fa-sort-alpha-asc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-alpha-desc"><i class="fa fa-sort-alpha-desc" title="fa-sort-alpha-desc" alt="fa-sort-alpha-desc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-amount-asc"><i class="fa fa-sort-amount-asc" title="fa-sort-amount-asc" alt="fa-sort-amount-asc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-amount-desc"><i class="fa fa-sort-amount-desc" title="fa-sort-amount-desc" alt="fa-sort-amount-desc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-asc"><i class="fa fa-sort-asc" title="fa-sort-asc" alt="fa-sort-asc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-desc"><i class="fa fa-sort-desc" title="fa-sort-desc" alt="fa-sort-desc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-down"><i class="fa fa-sort-down" title="fa-sort-down" alt="fa-sort-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-numeric-asc"><i class="fa fa-sort-numeric-asc" title="fa-sort-numeric-asc" alt="fa-sort-numeric-asc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-numeric-desc"><i class="fa fa-sort-numeric-desc" title="fa-sort-numeric-desc" alt="fa-sort-numeric-desc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sort-up"><i class="fa fa-sort-up" title="fa-sort-up" alt="fa-sort-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-space-shuttle"><i class="fa fa-space-shuttle" title="fa-space-shuttle" alt="fa-space-shuttle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-spinner"><i class="fa fa-spinner" title="fa-spinner" alt="fa-spinner"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-spoon"><i class="fa fa-spoon" title="fa-spoon" alt="fa-spoon"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-square"><i class="fa fa-square" title="fa-square" alt="fa-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-square-o"><i class="fa fa-square-o" title="fa-square-o" alt="fa-square-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-star"><i class="fa fa-star" title="fa-star" alt="fa-star"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-star-half"><i class="fa fa-star-half" title="fa-star-half" alt="fa-star-half"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-star-half-empty"><i class="fa fa-star-half-empty" title="fa-star-half-empty" alt="fa-star-half-empty"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-star-half-full"><i class="fa fa-star-half-full" title="fa-star-half-full" alt="fa-star-half-full"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-star-half-o"><i class="fa fa-star-half-o" title="fa-star-half-o" alt="fa-star-half-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-star-o"><i class="fa fa-star-o" title="fa-star-o" alt="fa-star-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-suitcase"><i class="fa fa-suitcase" title="fa-suitcase" alt="fa-suitcase"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-sun-o"><i class="fa fa-sun-o" title="fa-sun-o" alt="fa-sun-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-support"><i class="fa fa-support" title="fa-support" alt="fa-support"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tablet"><i class="fa fa-tablet" title="fa-tablet" alt="fa-tablet"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tachometer"><i class="fa fa-tachometer" title="fa-tachometer" alt="fa-tachometer"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tag"><i class="fa fa-tag" title="fa-tag" alt="fa-tag"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tags"><i class="fa fa-tags" title="fa-tags" alt="fa-tags"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tasks"><i class="fa fa-tasks" title="fa-tasks" alt="fa-tasks"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-taxi"><i class="fa fa-taxi" title="fa-taxi" alt="fa-taxi"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-terminal"><i class="fa fa-terminal" title="fa-terminal" alt="fa-terminal"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-thumb-tack"><i class="fa fa-thumb-tack" title="fa-thumb-tack" alt="fa-thumb-tack"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-thumbs-down"><i class="fa fa-thumbs-down" title="fa-thumbs-down" alt="fa-thumbs-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-thumbs-o-down"><i class="fa fa-thumbs-o-down" title="fa-thumbs-o-down" alt="fa-thumbs-o-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-thumbs-o-up"><i class="fa fa-thumbs-o-up" title="fa-thumbs-o-up" alt="fa-thumbs-o-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-thumbs-up"><i class="fa fa-thumbs-up" title="fa-thumbs-up" alt="fa-thumbs-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ticket"><i class="fa fa-ticket" title="fa-ticket" alt="fa-ticket"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-times"><i class="fa fa-times" title="fa-times" alt="fa-times"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-times-circle"><i class="fa fa-times-circle" title="fa-times-circle" alt="fa-times-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-times-circle-o"><i class="fa fa-times-circle-o" title="fa-times-circle-o" alt="fa-times-circle-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tint"><i class="fa fa-tint" title="fa-tint" alt="fa-tint"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-down"><i class="fa fa-toggle-down" title="fa-toggle-down" alt="fa-toggle-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-left"><i class="fa fa-toggle-left" title="fa-toggle-left" alt="fa-toggle-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-right"><i class="fa fa-toggle-right" title="fa-toggle-right" alt="fa-toggle-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-up"><i class="fa fa-toggle-up" title="fa-toggle-up" alt="fa-toggle-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-trash-o"><i class="fa fa-trash-o" title="fa-trash-o" alt="fa-trash-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tree"><i class="fa fa-tree" title="fa-tree" alt="fa-tree"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-trophy"><i class="fa fa-trophy" title="fa-trophy" alt="fa-trophy"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-truck"><i class="fa fa-truck" title="fa-truck" alt="fa-truck"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-umbrella"><i class="fa fa-umbrella" title="fa-umbrella" alt="fa-umbrella"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-university"><i class="fa fa-university" title="fa-university" alt="fa-university"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-unlock"><i class="fa fa-unlock" title="fa-unlock" alt="fa-unlock"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-unlock-alt"><i class="fa fa-unlock-alt" title="fa-unlock-alt" alt="fa-unlock-alt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-unsorted"><i class="fa fa-unsorted" title="fa-unsorted" alt="fa-unsorted"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-upload"><i class="fa fa-upload" title="fa-upload" alt="fa-upload"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-user"><i class="fa fa-user" title="fa-user" alt="fa-user"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-users"><i class="fa fa-users" title="fa-users" alt="fa-users"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-video-camera"><i class="fa fa-video-camera" title="fa-video-camera" alt="fa-video-camera"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-volume-down"><i class="fa fa-volume-down" title="fa-volume-down" alt="fa-volume-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-volume-off"><i class="fa fa-volume-off" title="fa-volume-off" alt="fa-volume-off"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-volume-up"><i class="fa fa-volume-up" title="fa-volume-up" alt="fa-volume-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-warning"><i class="fa fa-warning" title="fa-warning" alt="fa-warning"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-wheelchair"><i class="fa fa-wheelchair" title="fa-wheelchair" alt="fa-wheelchair"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-wrench"><i class="fa fa-wrench" title="fa-wrench" alt="fa-wrench"></i></a></div>

		                                    </div>

		                                </section>


		                                <section id="file-type">
		                                    <h4 class="page-header header-title"><b>File Type Icons</b></h4>

		                                    <div class="row">

		                                        <div class="icon"><a href="#" data-icon="fa fa-file"><i class="fa fa-file" title="fa-file" alt="fa-file"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-archive-o"><i class="fa fa-file-archive-o" title="fa-file-archive-o" alt="fa-file-archive-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-audio-o"><i class="fa fa-file-audio-o" title="fa-file-audio-o" alt="fa-file-audio-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-code-o"><i class="fa fa-file-code-o" title="fa-file-code-o" alt="fa-file-code-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-excel-o"><i class="fa fa-file-excel-o" title="fa-file-excel-o" alt="fa-file-excel-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-image-o"><i class="fa fa-file-image-o" title="fa-file-image-o" alt="fa-file-image-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-movie-o"><i class="fa fa-file-movie-o" title="fa-file-movie-o" alt="fa-file-movie-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-o"><i class="fa fa-file-o" title="fa-file-o" alt="fa-file-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-pdf-o"><i class="fa fa-file-pdf-o" title="fa-file-pdf-o" alt="fa-file-pdf-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-photo-o"><i class="fa fa-file-photo-o" title="fa-file-photo-o" alt="fa-file-photo-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-picture-o"><i class="fa fa-file-picture-o" title="fa-file-picture-o" alt="fa-file-picture-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-powerpoint-o"><i class="fa fa-file-powerpoint-o" title="fa-file-powerpoint-o" alt="fa-file-powerpoint-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-sound-o"><i class="fa fa-file-sound-o" title="fa-file-sound-o" alt="fa-file-sound-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-text"><i class="fa fa-file-text" title="fa-file-text" alt="fa-file-text"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-text-o"><i class="fa fa-file-text-o" title="fa-file-text-o" alt="fa-file-text-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-video-o"><i class="fa fa-file-video-o" title="fa-file-video-o" alt="fa-file-video-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-word-o"><i class="fa fa-file-word-o" title="fa-file-word-o" alt="fa-file-word-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-zip-o"><i class="fa fa-file-zip-o" title="fa-file-zip-o" alt="fa-file-zip-o"></i></a></div>

		                                    </div>

		                                </section>

		                                <section id="spinner">
		                                    <h4 class="page-header header-title"><b>Spinner Icons</b></h4>

		                                    <div class="alert alert-success">
		                                        These icons work great with the <code>fa-spin</code> class. Check out the
		                                        <a href="" class="alert-link">spinning icons example</a>.
		                                    </div>

		                                    <div class="row">
		                                        <div class="icon"><a href="#" data-icon="fa fa-spin fa-circle-o-notch"><i class="fa fa-spin fa-circle-o-notch" title="fa-circle-o-notch" alt="fa-circle-o-notch"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-spin fa-cog"><i class="fa fa-spin fa-cog" title="fa-cog" alt="fa-cog"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-spin fa-gear"><i class="fa fa-spin fa-gear" title="fa-gear" alt="fa-gear"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-spin fa-refresh"><i class="fa fa-spin fa-refresh" title="fa-refresh" alt="fa-refresh"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-spin fa-spinner"><i class="fa fa-spin fa-spinner" title="fa-spinner" alt="fa-spinner"></i></a></div>

		                                    </div>
		                                </section>

		                                <section id="form-control">
		                                    <h4 class="page-header header-title"><b>Form Control Icons</b></h4>

		                                    <div class="row">



		                                        <div class="icon"><a href="#" data-icon="fa fa-check-square"><i class="fa fa-check-square" title="fa-check-square" alt="fa-check-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-check-square-o"><i class="fa fa-check-square-o" title="fa-check-square-o" alt="fa-check-square-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-circle"><i class="fa fa-circle" title="fa-circle" alt="fa-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-circle-o"><i class="fa fa-circle-o" title="fa-circle-o" alt="fa-circle-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-dot-circle-o"><i class="fa fa-dot-circle-o" title="fa-dot-circle-o" alt="fa-dot-circle-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-minus-square"><i class="fa fa-minus-square" title="fa-minus-square" alt="fa-minus-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-minus-square-o"><i class="fa fa-minus-square-o" title="fa-minus-square-o" alt="fa-minus-square-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-plus-square"><i class="fa fa-plus-square" title="fa-plus-square" alt="fa-plus-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-plus-square-o"><i class="fa fa-plus-square-o" title="fa-plus-square-o" alt="fa-plus-square-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-square"><i class="fa fa-square" title="fa-square" alt="fa-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-square-o"><i class="fa fa-square-o" title="fa-square-o" alt="fa-square-o"></i></a></div>

		                                    </div>
		                                </section>

		                                <section id="currency">
		                                    <h4 class="page-header header-title"><b>Currency Icons</b></h4>

		                                    <div class="row">



		                                        <div class="icon"><a href="#" data-icon="fa fa-bitcoin"><i class="fa fa-bitcoin" title="fa-bitcoin" alt="fa-bitcoin"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-btc"><i class="fa fa-btc" title="fa-btc" alt="fa-btc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cny"><i class="fa fa-cny" title="fa-cny" alt="fa-cny"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-dollar"><i class="fa fa-dollar" title="fa-dollar" alt="fa-dollar"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-eur"><i class="fa fa-eur" title="fa-eur" alt="fa-eur"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-euro"><i class="fa fa-euro" title="fa-euro" alt="fa-euro"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gbp"><i class="fa fa-gbp" title="fa-gbp" alt="fa-gbp"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-inr"><i class="fa fa-inr" title="fa-inr" alt="fa-inr"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-jpy"><i class="fa fa-jpy" title="fa-jpy" alt="fa-jpy"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-krw"><i class="fa fa-krw" title="fa-krw" alt="fa-krw"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-money"><i class="fa fa-money" title="fa-money" alt="fa-money"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rmb"><i class="fa fa-rmb" title="fa-rmb" alt="fa-rmb"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rouble"><i class="fa fa-rouble" title="fa-rouble" alt="fa-rouble"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rub"><i class="fa fa-rub" title="fa-rub" alt="fa-rub"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ruble"><i class="fa fa-ruble" title="fa-ruble" alt="fa-ruble"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rupee"><i class="fa fa-rupee" title="fa-rupee" alt="fa-rupee"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-try"><i class="fa fa-try" title="fa-try" alt="fa-try"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-turkish-lira"><i class="fa fa-turkish-lira" title="fa-turkish-lira" alt="fa-turkish-lira"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-usd"><i class="fa fa-usd" title="fa-usd" alt="fa-usd"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-won"><i class="fa fa-won" title="fa-won" alt="fa-won"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-yen"><i class="fa fa-yen" title="fa-yen" alt="fa-yen"></i></a></div>

		                                    </div>

		                                </section>

		                                <section id="text-editor">
		                                    <h4 class="page-header header-title"><b>Text Editor Icons</b></h4>

		                                    <div class="row">



		                                        <div class="icon"><a href="#" data-icon="fa fa-align-center"><i class="fa fa-align-center" title="fa-align-center" alt="fa-align-center"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-align-justify"><i class="fa fa-align-justify" title="fa-align-justify" alt="fa-align-justify"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-align-left"><i class="fa fa-align-left" title="fa-align-left" alt="fa-align-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-align-right"><i class="fa fa-align-right" title="fa-align-right" alt="fa-align-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bold"><i class="fa fa-bold" title="fa-bold" alt="fa-bold"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chain"><i class="fa fa-chain" title="fa-chain" alt="fa-chain"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chain-broken"><i class="fa fa-chain-broken" title="fa-chain-broken" alt="fa-chain-broken"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-clipboard"><i class="fa fa-clipboard" title="fa-clipboard" alt="fa-clipboard"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-columns"><i class="fa fa-columns" title="fa-columns" alt="fa-columns"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-copy"><i class="fa fa-copy" title="fa-copy" alt="fa-copy"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-cut"><i class="fa fa-cut" title="fa-cut" alt="fa-cut"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-dedent"><i class="fa fa-dedent" title="fa-dedent" alt="fa-dedent"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-eraser"><i class="fa fa-eraser" title="fa-eraser" alt="fa-eraser"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file"><i class="fa fa-file" title="fa-file" alt="fa-file"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-o"><i class="fa fa-file-o" title="fa-file-o" alt="fa-file-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-text"><i class="fa fa-file-text" title="fa-file-text" alt="fa-file-text"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-file-text-o"><i class="fa fa-file-text-o" title="fa-file-text-o" alt="fa-file-text-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-files-o"><i class="fa fa-files-o" title="fa-files-o" alt="fa-files-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-floppy-o"><i class="fa fa-floppy-o" title="fa-floppy-o" alt="fa-floppy-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-font"><i class="fa fa-font" title="fa-font" alt="fa-font"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-header"><i class="fa fa-header" title="fa-header" alt="fa-header"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-indent"><i class="fa fa-indent" title="fa-indent" alt="fa-indent"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-italic"><i class="fa fa-italic" title="fa-italic" alt="fa-italic"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-link"><i class="fa fa-link" title="fa-link" alt="fa-link"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-list"><i class="fa fa-list" title="fa-list" alt="fa-list"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-list-alt"><i class="fa fa-list-alt" title="fa-list-alt" alt="fa-list-alt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-list-ol"><i class="fa fa-list-ol" title="fa-list-ol" alt="fa-list-ol"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-list-ul"><i class="fa fa-list-ul" title="fa-list-ul" alt="fa-list-ul"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-outdent"><i class="fa fa-outdent" title="fa-outdent" alt="fa-outdent"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-paperclip"><i class="fa fa-paperclip" title="fa-paperclip" alt="fa-paperclip"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-paragraph"><i class="fa fa-paragraph" title="fa-paragraph" alt="fa-paragraph"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-paste"><i class="fa fa-paste" title="fa-paste" alt="fa-paste"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-repeat"><i class="fa fa-repeat" title="fa-repeat" alt="fa-repeat"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rotate-left"><i class="fa fa-rotate-left" title="fa-rotate-left" alt="fa-rotate-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rotate-right"><i class="fa fa-rotate-right" title="fa-rotate-right" alt="fa-rotate-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-save"><i class="fa fa-save" title="fa-save" alt="fa-save"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-scissors"><i class="fa fa-scissors" title="fa-scissors" alt="fa-scissors"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-strikethrough"><i class="fa fa-strikethrough" title="fa-strikethrough" alt="fa-strikethrough"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-subscript"><i class="fa fa-subscript" title="fa-subscript" alt="fa-subscript"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-superscript"><i class="fa fa-superscript" title="fa-superscript" alt="fa-superscript"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-table"><i class="fa fa-table" title="fa-table" alt="fa-table"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-text-height"><i class="fa fa-text-height" title="fa-text-height" alt="fa-text-height"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-text-width"><i class="fa fa-text-width" title="fa-text-width" alt="fa-text-width"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-th"><i class="fa fa-th" title="fa-th" alt="fa-th"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-th-large"><i class="fa fa-th-large" title="fa-th-large" alt="fa-th-large"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-th-list"><i class="fa fa-th-list" title="fa-th-list" alt="fa-th-list"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-underline"><i class="fa fa-underline" title="fa-underline" alt="fa-underline"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-undo"><i class="fa fa-undo" title="fa-undo" alt="fa-undo"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-unlink"><i class="fa fa-unlink" title="fa-unlink" alt="fa-unlink"></i></a></div>

		                                    </div>

		                                </section>


		                                <section id="directional">
		                                    <h4 class="page-header header-title"><b>Directional Icons</b></h4>

		                                    <div class="row">



		                                        <div class="icon"><a href="#" data-icon="fa fa-angle-double-down"><i class="fa fa-angle-double-down" title="fa-angle-double-down" alt="fa-angle-double-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-angle-double-left"><i class="fa fa-angle-double-left" title="fa-angle-double-left" alt="fa-angle-double-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-angle-double-right"><i class="fa fa-angle-double-right" title="fa-angle-double-right" alt="fa-angle-double-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-angle-double-up"><i class="fa fa-angle-double-up" title="fa-angle-double-up" alt="fa-angle-double-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-angle-down"><i class="fa fa-angle-down" title="fa-angle-down" alt="fa-angle-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-angle-left"><i class="fa fa-angle-left" title="fa-angle-left" alt="fa-angle-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-angle-right"><i class="fa fa-angle-right" title="fa-angle-right" alt="fa-angle-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-angle-up"><i class="fa fa-angle-up" title="fa-angle-up" alt="fa-angle-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-circle-down"><i class="fa fa-arrow-circle-down" title="fa-arrow-circle-down" alt="fa-arrow-circle-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-circle-left"><i class="fa fa-arrow-circle-left" title="fa-arrow-circle-left" alt="fa-arrow-circle-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-circle-o-down"><i class="fa fa-arrow-circle-o-down" title="fa-arrow-circle-o-down" alt="fa-arrow-circle-o-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-circle-o-left"><i class="fa fa-arrow-circle-o-left" title="fa-arrow-circle-o-left" alt="fa-arrow-circle-o-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-circle-o-right"><i class="fa fa-arrow-circle-o-right" title="fa-arrow-circle-o-right" alt="fa-arrow-circle-o-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-circle-o-up"><i class="fa fa-arrow-circle-o-up" title="fa-arrow-circle-o-up" alt="fa-arrow-circle-o-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-circle-right"><i class="fa fa-arrow-circle-right" title="fa-arrow-circle-right" alt="fa-arrow-circle-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-circle-up"><i class="fa fa-arrow-circle-up" title="fa-arrow-circle-up" alt="fa-arrow-circle-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-down"><i class="fa fa-arrow-down" title="fa-arrow-down" alt="fa-arrow-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-left"><i class="fa fa-arrow-left" title="fa-arrow-left" alt="fa-arrow-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-right"><i class="fa fa-arrow-right" title="fa-arrow-right" alt="fa-arrow-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrow-up"><i class="fa fa-arrow-up" title="fa-arrow-up" alt="fa-arrow-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrows"><i class="fa fa-arrows" title="fa-arrows" alt="fa-arrows"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrows-alt"><i class="fa fa-arrows-alt" title="fa-arrows-alt" alt="fa-arrows-alt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrows-h"><i class="fa fa-arrows-h" title="fa-arrows-h" alt="fa-arrows-h"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-arrows-v"><i class="fa fa-arrows-v" title="fa-arrows-v" alt="fa-arrows-v"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-down"><i class="fa fa-caret-down" title="fa-caret-down" alt="fa-caret-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-left"><i class="fa fa-caret-left" title="fa-caret-left" alt="fa-caret-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-right"><i class="fa fa-caret-right" title="fa-caret-right" alt="fa-caret-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-up"><i class="fa fa-caret-up" title="fa-caret-up" alt="fa-caret-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-square-o-left"><i class="fa fa-caret-square-o-left" title="fa-caret-square-o-left" alt="fa-caret-square-o-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-square-o-right"><i class="fa fa-caret-square-o-right" title="fa-caret-square-o-right" alt="fa-caret-square-o-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-square-o-up"><i class="fa fa-caret-square-o-up" title="fa-caret-square-o-up" alt="fa-caret-square-o-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-caret-square-o-down"><i class="fa fa-caret-square-o-down" title="fa-caret-square-o-down" alt="fa-caret-square-o-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chevron-circle-down"><i class="fa fa-chevron-circle-down" title="fa-chevron-circle-down" alt="fa-chevron-circle-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chevron-circle-left"><i class="fa fa-chevron-circle-left" title="fa-chevron-circle-left" alt="fa-chevron-circle-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chevron-circle-right"><i class="fa fa-chevron-circle-right" title="fa-chevron-circle-right" alt="fa-chevron-circle-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chevron-circle-up"><i class="fa fa-chevron-circle-up" title="fa-chevron-circle-up" alt="fa-chevron-circle-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chevron-down"><i class="fa fa-chevron-down" title="fa-chevron-down" alt="fa-chevron-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chevron-left"><i class="fa fa-chevron-left" title="fa-chevron-left" alt="fa-chevron-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chevron-right"><i class="fa fa-chevron-right" title="fa-chevron-right" alt="fa-chevron-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-chevron-up"><i class="fa fa-chevron-up" title="fa-chevron-up" alt="fa-chevron-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-o-down"><i class="fa fa-hand-o-down" title="fa-hand-o-down" alt="fa-hand-o-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-o-left"><i class="fa fa-hand-o-left" title="fa-hand-o-left" alt="fa-hand-o-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-o-right"><i class="fa fa-hand-o-right" title="fa-hand-o-right" alt="fa-hand-o-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hand-o-up"><i class="fa fa-hand-o-up" title="fa-hand-o-up" alt="fa-hand-o-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-long-arrow-down"><i class="fa fa-long-arrow-down" title="fa-long-arrow-down" alt="fa-long-arrow-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-long-arrow-left"><i class="fa fa-long-arrow-left" title="fa-long-arrow-left" alt="fa-long-arrow-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-long-arrow-right"><i class="fa fa-long-arrow-right" title="fa-long-arrow-right" alt="fa-long-arrow-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-long-arrow-up"><i class="fa fa-long-arrow-up" title="fa-long-arrow-up" alt="fa-long-arrow-up"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-down"><i class="fa fa-toggle-down" title="fa-toggle-down" alt="fa-toggle-down"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-left"><i class="fa fa-toggle-left" title="fa-toggle-left" alt="fa-toggle-left"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-right"><i class="fa fa-toggle-right" title="fa-toggle-right" alt="fa-toggle-right"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-toggle-up"><i class="fa fa-toggle-up" title="fa-toggle-up" alt="fa-toggle-up"></i></a></div>

		                                    </div>

		                                </section>

		                                <section id="video-player">
		                                    <h4 class="page-header header-title"><b>Video Player Icons</b></h4>

		                                    <div class="row">



		                                        <div class="icon"><a href="#" data-icon="fa fa-arrows-alt"><i class="fa fa-arrows-alt" title="fa-arrows-alt" alt="fa-arrows-alt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-backward"><i class="fa fa-backward" title="fa-backward" alt="fa-backward"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-compress"><i class="fa fa-compress" title="fa-compress" alt="fa-compress"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-eject"><i class="fa fa-eject" title="fa-eject" alt="fa-eject"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-expand"><i class="fa fa-expand" title="fa-expand" alt="fa-expand"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-fast-backward"><i class="fa fa-fast-backward" title="fa-fast-backward" alt="fa-fast-backward"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-fast-forward"><i class="fa fa-fast-forward" title="fa-fast-forward" alt="fa-fast-forward"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-forward"><i class="fa fa-forward" title="fa-forward" alt="fa-forward"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pause"><i class="fa fa-pause" title="fa-pause" alt="fa-pause"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-play"><i class="fa fa-play" title="fa-play" alt="fa-play"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-play-circle"><i class="fa fa-play-circle" title="fa-play-circle" alt="fa-play-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-play-circle-o"><i class="fa fa-play-circle-o" title="fa-play-circle-o" alt="fa-play-circle-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-step-backward"><i class="fa fa-step-backward" title="fa-step-backward" alt="fa-step-backward"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-step-forward"><i class="fa fa-step-forward" title="fa-step-forward" alt="fa-step-forward"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-stop"><i class="fa fa-stop" title="fa-stop" alt="fa-stop"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-youtube-play"><i class="fa fa-youtube-play" title="fa-youtube-play" alt="fa-youtube-play"></i></a></div>

		                                    </div>

		                                </section>

		                                <section id="brand">
		                                    <h4 class="page-header header-title"><b>Brand Icons</b></h4>

		                                    <div class="alert alert-success">
		                                        <ul class="margin-bottom-none padding-left-lg">
		                                            <li>All brand icons are trademarks of their respective owners.</li>
		                                            <li>The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.</li>
		                                        </ul>

		                                    </div>
		                                    <div class="row">

		                                        <div class="icon"><a href="#" data-icon="fa fa-adn"><i class="fa fa-adn" title="fa-adn" alt="fa-adn"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-android"><i class="fa fa-android" title="fa-android" alt="fa-android"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-apple"><i class="fa fa-apple" title="fa-apple" alt="fa-apple"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-behance"><i class="fa fa-behance" title="fa-behance" alt="fa-behance"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-behance-square"><i class="fa fa-behance-square" title="fa-behance-square" alt="fa-behance-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bitbucket"><i class="fa fa-bitbucket" title="fa-bitbucket" alt="fa-bitbucket"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bitbucket-square"><i class="fa fa-bitbucket-square" title="fa-bitbucket-square" alt="fa-bitbucket-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-bitcoin"><i class="fa fa-bitcoin" title="fa-bitcoin" alt="fa-bitcoin"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-btc"><i class="fa fa-btc" title="fa-btc" alt="fa-btc"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-codepen"><i class="fa fa-codepen" title="fa-codepen" alt="fa-codepen"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-css3"><i class="fa fa-css3" title="fa-css3" alt="fa-css3"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-delicious"><i class="fa fa-delicious" title="fa-delicious" alt="fa-delicious"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-deviantart"><i class="fa fa-deviantart" title="fa-deviantart" alt="fa-deviantart"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-digg"><i class="fa fa-digg" title="fa-digg" alt="fa-digg"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-dribbble"><i class="fa fa-dribbble" title="fa-dribbble" alt="fa-dribbble"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-dropbox"><i class="fa fa-dropbox" title="fa-dropbox" alt="fa-dropbox"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-drupal"><i class="fa fa-drupal" title="fa-drupal" alt="fa-drupal"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-empire"><i class="fa fa-empire" title="fa-empire" alt="fa-empire"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-facebook"><i class="fa fa-facebook" title="fa-facebook" alt="fa-facebook"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-facebook-square"><i class="fa fa-facebook-square" title="fa-facebook-square" alt="fa-facebook-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-flickr"><i class="fa fa-flickr" title="fa-flickr" alt="fa-flickr"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-foursquare"><i class="fa fa-foursquare" title="fa-foursquare" alt="fa-foursquare"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ge"><i class="fa fa-ge" title="fa-ge" alt="fa-ge"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-git"><i class="fa fa-git" title="fa-git" alt="fa-git"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-git-square"><i class="fa fa-git-square" title="fa-git-square" alt="fa-git-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-github"><i class="fa fa-github" title="fa-github" alt="fa-github"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-github-alt"><i class="fa fa-github-alt" title="fa-github-alt" alt="fa-github-alt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-github-square"><i class="fa fa-github-square" title="fa-github-square" alt="fa-github-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-gittip"><i class="fa fa-gittip" title="fa-gittip" alt="fa-gittip"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-google"><i class="fa fa-google" title="fa-google" alt="fa-google"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-google-plus"><i class="fa fa-google-plus" title="fa-google-plus" alt="fa-google-plus"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-google-plus-square"><i class="fa fa-google-plus-square" title="fa-google-plus-square" alt="fa-google-plus-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hacker-news"><i class="fa fa-hacker-news" title="fa-hacker-news" alt="fa-hacker-news"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-html5"><i class="fa fa-html5" title="fa-html5" alt="fa-html5"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-instagram"><i class="fa fa-instagram" title="fa-instagram" alt="fa-instagram"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-joomla"><i class="fa fa-joomla" title="fa-joomla" alt="fa-joomla"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-jsfiddle"><i class="fa fa-jsfiddle" title="fa-jsfiddle" alt="fa-jsfiddle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-linkedin"><i class="fa fa-linkedin" title="fa-linkedin" alt="fa-linkedin"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-linkedin-square"><i class="fa fa-linkedin-square" title="fa-linkedin-square" alt="fa-linkedin-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-linux"><i class="fa fa-linux" title="fa-linux" alt="fa-linux"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-maxcdn"><i class="fa fa-maxcdn" title="fa-maxcdn" alt="fa-maxcdn"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-openid"><i class="fa fa-openid" title="fa-openid" alt="fa-openid"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pagelines"><i class="fa fa-pagelines" title="fa-pagelines" alt="fa-pagelines"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pied-piper"><i class="fa fa-pied-piper" title="fa-pied-piper" alt="fa-pied-piper"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pied-piper-alt"><i class="fa fa-pied-piper-alt" title="fa-pied-piper-alt" alt="fa-pied-piper-alt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pinterest"><i class="fa fa-pinterest" title="fa-pinterest" alt="fa-pinterest"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-pinterest-square"><i class="fa fa-pinterest-square" title="fa-pinterest-square" alt="fa-pinterest-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-qq"><i class="fa fa-qq" title="fa-qq" alt="fa-qq"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-ra"><i class="fa fa-ra" title="fa-ra" alt="fa-ra"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-rebel"><i class="fa fa-rebel" title="fa-rebel" alt="fa-rebel"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-reddit"><i class="fa fa-reddit" title="fa-reddit" alt="fa-reddit"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-reddit-square"><i class="fa fa-reddit-square" title="fa-reddit-square" alt="fa-reddit-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-renren"><i class="fa fa-renren" title="fa-renren" alt="fa-renren"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-share-alt"><i class="fa fa-share-alt" title="fa-share-alt" alt="fa-share-alt"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-share-alt-square"><i class="fa fa-share-alt-square" title="fa-share-alt-square" alt="fa-share-alt-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-skype"><i class="fa fa-skype" title="fa-skype" alt="fa-skype"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-slack"><i class="fa fa-slack" title="fa-slack" alt="fa-slack"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-soundcloud"><i class="fa fa-soundcloud" title="fa-soundcloud" alt="fa-soundcloud"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-spotify"><i class="fa fa-spotify" title="fa-spotify" alt="fa-spotify"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-stack-exchange"><i class="fa fa-stack-exchange" title="fa-stack-exchange" alt="fa-stack-exchange"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-stack-overflow"><i class="fa fa-stack-overflow" title="fa-stack-overflow" alt="fa-stack-overflow"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-steam"><i class="fa fa-steam" title="fa-steam" alt="fa-steam"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-steam-square"><i class="fa fa-steam-square" title="fa-steam-square" alt="fa-steam-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-stumbleupon"><i class="fa fa-stumbleupon" title="fa-stumbleupon" alt="fa-stumbleupon"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-stumbleupon-circle"><i class="fa fa-stumbleupon-circle" title="fa-stumbleupon-circle" alt="fa-stumbleupon-circle"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tencent-weibo"><i class="fa fa-tencent-weibo" title="fa-tencent-weibo" alt="fa-tencent-weibo"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-trello"><i class="fa fa-trello" title="fa-trello" alt="fa-trello"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tumblr"><i class="fa fa-tumblr" title="fa-tumblr" alt="fa-tumblr"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-tumblr-square"><i class="fa fa-tumblr-square" title="fa-tumblr-square" alt="fa-tumblr-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-twitter"><i class="fa fa-twitter" title="fa-twitter" alt="fa-twitter"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-twitter-square"><i class="fa fa-twitter-square" title="fa-twitter-square" alt="fa-twitter-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-vimeo-square"><i class="fa fa-vimeo-square" title="fa-vimeo-square" alt="fa-vimeo-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-vine"><i class="fa fa-vine" title="fa-vine" alt="fa-vine"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-vk"><i class="fa fa-vk" title="fa-vk" alt="fa-vk"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-wechat"><i class="fa fa-wechat" title="fa-wechat" alt="fa-wechat"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-weibo"><i class="fa fa-weibo" title="fa-weibo" alt="fa-weibo"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-weixin"><i class="fa fa-weixin" title="fa-weixin" alt="fa-weixin"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-windows"><i class="fa fa-windows" title="fa-windows" alt="fa-windows"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-wordpress"><i class="fa fa-wordpress" title="fa-wordpress" alt="fa-wordpress"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-xing"><i class="fa fa-xing" title="fa-xing" alt="fa-xing"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-xing-square"><i class="fa fa-xing-square" title="fa-xing-square" alt="fa-xing-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-yahoo"><i class="fa fa-yahoo" title="fa-yahoo" alt="fa-yahoo"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-youtube"><i class="fa fa-youtube" title="fa-youtube" alt="fa-youtube"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-youtube-play"><i class="fa fa-youtube-play" title="fa-youtube-play" alt="fa-youtube-play"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-youtube-square"><i class="fa fa-youtube-square" title="fa-youtube-square" alt="fa-youtube-square"></i></a></div>

		                                    </div>
		                                </section>

		                                <section id="medical">
		                                    <h4 class="page-header header-title"><b>Medical Icons</b></h4>

		                                    <div class="row">
		                                        <div class="icon"><a href="#" data-icon="fa fa-ambulance"><i class="fa fa-ambulance" title="fa-ambulance" alt="fa-ambulance"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-h-square"><i class="fa fa-h-square" title="fa-h-square" alt="fa-h-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-hospital-o"><i class="fa fa-hospital-o" title="fa-hospital-o" alt="fa-hospital-o"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-medkit"><i class="fa fa-medkit" title="fa-medkit" alt="fa-medkit"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-plus-square"><i class="fa fa-plus-square" title="fa-plus-square" alt="fa-plus-square"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-stethoscope"><i class="fa fa-stethoscope" title="fa-stethoscope" alt="fa-stethoscope"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-user-md"><i class="fa fa-user-md" title="fa-user-md" alt="fa-user-md"></i></a></div>

		                                        <div class="icon"><a href="#" data-icon="fa fa-wheelchair"><i class="fa fa-wheelchair" title="fa-wheelchair" alt="fa-wheelchair"></i></a></div>
		                                    </div>

		                                </section>
		                            </div>
					  </div>
					</div>
				  </div>
				  <div class="hidden panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#icons" href="#themifyicons">
						Themify Icons</a>
					  </h4>
					</div>
					<div id="themifyicons" class="panel-collapse collapse">
					  <div class="panel-body">
							<div class="card-box">
                                        <h4 class="m-t-0 header-title"><b>Arrows &amp; Direction Icons </b></h4>


                                        <div class="row">
											<div class="icon">
												<a href="#" data-icon="ti-arrow-up"><i class="ti-arrow-up" title="ti-arrow-up" alt="ti-arrow-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrow-right"><i class="ti-arrow-right" title="ti-arrow-right" alt="ti-arrow-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrow-left"><i class="ti-arrow-left" title="ti-arrow-left" alt="ti-arrow-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrow-down"><i class="ti-arrow-down" title="ti-arrow-down" alt="ti-arrow-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrows-vertical"><i class="ti-arrows-vertical" title="ti-arrows-vertical" alt="ti-arrows-vertical"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrows-horizontal"><i class="ti-arrows-horizontal" title="ti-arrows-horizontal" alt="ti-arrows-horizontal"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-angle-up"><i class="ti-angle-up" title="ti-angle-up" alt="ti-angle-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-angle-right"><i class="ti-angle-right" title="ti-angle-right" alt="ti-angle-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-angle-left"><i class="ti-angle-left" title="ti-angle-left" alt="ti-angle-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-angle-down"><i class="ti-angle-down" title="ti-angle-down" alt="ti-angle-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-angle-double-up"><i class="ti-angle-double-up" title="ti-angle-double-up" alt="ti-angle-double-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-angle-double-right"><i class="ti-angle-double-right" title="ti-angle-double-right" alt="ti-angle-double-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-angle-double-left"><i class="ti-angle-double-left" title="ti-angle-double-left" alt="ti-angle-double-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-angle-double-down"><i class="ti-angle-double-down" title="ti-angle-double-down" alt="ti-angle-double-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-move"><i class="ti-move" title="ti-move" alt="ti-move"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-fullscreen"><i class="ti-fullscreen" title="ti-fullscreen" alt="ti-fullscreen"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrow-top-right"><i class="ti-arrow-top-right" title="ti-arrow-top-right" alt="ti-arrow-top-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrow-top-left"><i class="ti-arrow-top-left" title="ti-arrow-top-left" alt="ti-arrow-top-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrow-circle-up"><i class="ti-arrow-circle-up" title="ti-arrow-circle-up" alt="ti-arrow-circle-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrow-circle-right"><i class="ti-arrow-circle-right" title="ti-arrow-circle-right" alt="ti-arrow-circle-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrow-circle-left"><i class="ti-arrow-circle-left" title="ti-arrow-circle-left" alt="ti-arrow-circle-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrow-circle-down"><i class="ti-arrow-circle-down" title="ti-arrow-circle-down" alt="ti-arrow-circle-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-arrows-corner"><i class="ti-arrows-corner" title="ti-arrows-corner" alt="ti-arrows-corner"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-split-v"><i class="ti-split-v" title="ti-split-v" alt="ti-split-v"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-split-v-alt"><i class="ti-split-v-alt" title="ti-split-v-alt" alt="ti-split-v-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-split-h"><i class="ti-split-h" title="ti-split-h" alt="ti-split-h"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-hand-point-up"><i class="ti-hand-point-up" title="ti-hand-point-up" alt="ti-hand-point-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-hand-point-right"><i class="ti-hand-point-right" title="ti-hand-point-right" alt="ti-hand-point-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-hand-point-left"><i class="ti-hand-point-left" title="ti-hand-point-left" alt="ti-hand-point-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-hand-point-down"><i class="ti-hand-point-down" title="ti-hand-point-down" alt="ti-hand-point-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-back-right"><i class="ti-back-right" title="ti-back-right" alt="ti-back-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-back-left"><i class="ti-back-left" title="ti-back-left" alt="ti-back-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-exchange-vertical"><i class="ti-exchange-vertical" title="ti-exchange-vertical" alt="ti-exchange-vertical"></i></a></div>


										</div>
										<!-- End row -->




										<h4 class="page-header header-title"><b>Web App Icons </b></h4>
										<div class="row">
											<div class="icon">
												<a href="#" data-icon="ti-wand"><i class="ti-wand" title="ti-wand" alt="ti-wand"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-save"><i class="ti-save" title="ti-save" alt="ti-save"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-save-alt"><i class="ti-save-alt" title="ti-save-alt" alt="ti-save-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-direction"><i class="ti-direction" title="ti-direction" alt="ti-direction"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-direction-alt"><i class="ti-direction-alt" title="ti-direction-alt" alt="ti-direction-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-user"><i class="ti-user" title="ti-user" alt="ti-user"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-link"><i class="ti-link" title="ti-link" alt="ti-link"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-unlink"><i class="ti-unlink" title="ti-unlink" alt="ti-unlink"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-trash"><i class="ti-trash" title="ti-trash" alt="ti-trash"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-target"><i class="ti-target" title="ti-target" alt="ti-target"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-tag"><i class="ti-tag" title="ti-tag" alt="ti-tag"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-desktop"><i class="ti-desktop" title="ti-desktop" alt="ti-desktop"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-tablet"><i class="ti-tablet" title="ti-tablet" alt="ti-tablet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-mobile"><i class="ti-mobile" title="ti-mobile" alt="ti-mobile"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-email"><i class="ti-email" title="ti-email" alt="ti-email"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-star"><i class="ti-star" title="ti-star" alt="ti-star"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-spray"><i class="ti-spray" title="ti-spray" alt="ti-spray"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-signal"><i class="ti-signal" title="ti-signal" alt="ti-signal"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-shopping-cart"><i class="ti-shopping-cart" title="ti-shopping-cart" alt="ti-shopping-cart"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-shopping-cart-full"><i class="ti-shopping-cart-full" title="ti-shopping-cart-full" alt="ti-shopping-cart-full"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-settings"><i class="ti-settings" title="ti-settings" alt="ti-settings"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-search"><i class="ti-search" title="ti-search" alt="ti-search"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-zoom-in"><i class="ti-zoom-in" title="ti-zoom-in" alt="ti-zoom-in"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-zoom-out"><i class="ti-zoom-out" title="ti-zoom-out" alt="ti-zoom-out"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-cut"><i class="ti-cut" title="ti-cut" alt="ti-cut"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-ruler"><i class="ti-ruler" title="ti-ruler" alt="ti-ruler"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-ruler-alt-2"><i class="ti-ruler-alt-2" title="ti-ruler-alt-2" alt="ti-ruler-alt-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-ruler-pencil"><i class="ti-ruler-pencil" title="ti-ruler-pencil" alt="ti-ruler-pencil"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-ruler-alt"><i class="ti-ruler-alt" title="ti-ruler-alt" alt="ti-ruler-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-bookmark"><i class="ti-bookmark" title="ti-bookmark" alt="ti-bookmark"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-bookmark-alt"><i class="ti-bookmark-alt" title="ti-bookmark-alt" alt="ti-bookmark-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-reload"><i class="ti-reload" title="ti-reload" alt="ti-reload"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-plus"><i class="ti-plus" title="ti-plus" alt="ti-plus"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-minus"><i class="ti-minus" title="ti-minus" alt="ti-minus"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-close"><i class="ti-close" title="ti-close" alt="ti-close"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pin"><i class="ti-pin" title="ti-pin" alt="ti-pin"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pencil"><i class="ti-pencil" title="ti-pencil" alt="ti-pencil"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pencil-alt"><i class="ti-pencil-alt" title="ti-pencil-alt" alt="ti-pencil-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-paint-roller"><i class="ti-paint-roller" title="ti-paint-roller" alt="ti-paint-roller"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-paint-bucket"><i class="ti-paint-bucket" title="ti-paint-bucket" alt="ti-paint-bucket"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-na"><i class="ti-na" title="ti-na" alt="ti-na"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-medall"><i class="ti-medall" title="ti-medall" alt="ti-medall"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-medall-alt"><i class="ti-medall-alt" title="ti-medall-alt" alt="ti-medall-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-marker"><i class="ti-marker" title="ti-marker" alt="ti-marker"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-marker-alt"><i class="ti-marker-alt" title="ti-marker-alt" alt="ti-marker-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-lock"><i class="ti-lock" title="ti-lock" alt="ti-lock"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-unlock"><i class="ti-unlock" title="ti-unlock" alt="ti-unlock"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-location-arrow"><i class="ti-location-arrow" title="ti-location-arrow" alt="ti-location-arrow"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout"><i class="ti-layout" title="ti-layout" alt="ti-layout"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layers"><i class="ti-layers" title="ti-layers" alt="ti-layers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layers-alt"><i class="ti-layers-alt" title="ti-layers-alt" alt="ti-layers-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-key"><i class="ti-key" title="ti-key" alt="ti-key"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-image"><i class="ti-image" title="ti-image" alt="ti-image"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-heart"><i class="ti-heart" title="ti-heart" alt="ti-heart"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-heart-broken"><i class="ti-heart-broken" title="ti-heart-broken" alt="ti-heart-broken"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-hand-stop"><i class="ti-hand-stop" title="ti-hand-stop" alt="ti-hand-stop"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-hand-open"><i class="ti-hand-open" title="ti-hand-open" alt="ti-hand-open"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-hand-drag"><i class="ti-hand-drag" title="ti-hand-drag" alt="ti-hand-drag"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-flag"><i class="ti-flag" title="ti-flag" alt="ti-flag"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-flag-alt"><i class="ti-flag-alt" title="ti-flag-alt" alt="ti-flag-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-flag-alt-2"><i class="ti-flag-alt-2" title="ti-flag-alt-2" alt="ti-flag-alt-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-eye"><i class="ti-eye" title="ti-eye" alt="ti-eye"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-import"><i class="ti-import" title="ti-import" alt="ti-import"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-export"><i class="ti-export" title="ti-export" alt="ti-export"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-cup"><i class="ti-cup" title="ti-cup" alt="ti-cup"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-crown"><i class="ti-crown" title="ti-crown" alt="ti-crown"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-comments"><i class="ti-comments" title="ti-comments" alt="ti-comments"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-comment"><i class="ti-comment" title="ti-comment" alt="ti-comment"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-comment-alt"><i class="ti-comment-alt" title="ti-comment-alt" alt="ti-comment-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-thought"><i class="ti-thought" title="ti-thought" alt="ti-thought"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-clip"><i class="ti-clip" title="ti-clip" alt="ti-clip"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-check"><i class="ti-check" title="ti-check" alt="ti-check"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-check-box"><i class="ti-check-box" title="ti-check-box" alt="ti-check-box"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-camera"><i class="ti-camera" title="ti-camera" alt="ti-camera"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-announcement"><i class="ti-announcement" title="ti-announcement" alt="ti-announcement"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-brush"><i class="ti-brush" title="ti-brush" alt="ti-brush"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-brush-alt"><i class="ti-brush-alt" title="ti-brush-alt" alt="ti-brush-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-palette"><i class="ti-palette" title="ti-palette" alt="ti-palette"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-briefcase"><i class="ti-briefcase" title="ti-briefcase" alt="ti-briefcase"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-bolt"><i class="ti-bolt" title="ti-bolt" alt="ti-bolt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-bolt-alt"><i class="ti-bolt-alt" title="ti-bolt-alt" alt="ti-bolt-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-blackboard"><i class="ti-blackboard" title="ti-blackboard" alt="ti-blackboard"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-bag"><i class="ti-bag" title="ti-bag" alt="ti-bag"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-world"><i class="ti-world" title="ti-world" alt="ti-world"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-wheelchair"><i class="ti-wheelchair" title="ti-wheelchair" alt="ti-wheelchair"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-car"><i class="ti-car" title="ti-car" alt="ti-car"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-truck"><i class="ti-truck" title="ti-truck" alt="ti-truck"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-timer"><i class="ti-timer" title="ti-timer" alt="ti-timer"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-ticket"><i class="ti-ticket" title="ti-ticket" alt="ti-ticket"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-thumb-up"><i class="ti-thumb-up" title="ti-thumb-up" alt="ti-thumb-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-thumb-down"><i class="ti-thumb-down" title="ti-thumb-down" alt="ti-thumb-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-stats-up"><i class="ti-stats-up" title="ti-stats-up" alt="ti-stats-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-stats-down"><i class="ti-stats-down" title="ti-stats-down" alt="ti-stats-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-shine"><i class="ti-shine" title="ti-shine" alt="ti-shine"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-shift-right"><i class="ti-shift-right" title="ti-shift-right" alt="ti-shift-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-shift-left"><i class="ti-shift-left" title="ti-shift-left" alt="ti-shift-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-shift-right-alt"><i class="ti-shift-right-alt" title="ti-shift-right-alt" alt="ti-shift-right-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-shift-left-alt"><i class="ti-shift-left-alt" title="ti-shift-left-alt" alt="ti-shift-left-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-shield"><i class="ti-shield" title="ti-shield" alt="ti-shield"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-notepad"><i class="ti-notepad" title="ti-notepad" alt="ti-notepad"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-server"><i class="ti-server" title="ti-server" alt="ti-server"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pulse"><i class="ti-pulse" title="ti-pulse" alt="ti-pulse"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-printer"><i class="ti-printer" title="ti-printer" alt="ti-printer"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-power-off"><i class="ti-power-off" title="ti-power-off" alt="ti-power-off"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-plug"><i class="ti-plug" title="ti-plug" alt="ti-plug"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pie-chart"><i class="ti-pie-chart" title="ti-pie-chart" alt="ti-pie-chart"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-panel"><i class="ti-panel" title="ti-panel" alt="ti-panel"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-package"><i class="ti-package" title="ti-package" alt="ti-package"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-music"><i class="ti-music" title="ti-music" alt="ti-music"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-music-alt"><i class="ti-music-alt" title="ti-music-alt" alt="ti-music-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-mouse"><i class="ti-mouse" title="ti-mouse" alt="ti-mouse"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-mouse-alt"><i class="ti-mouse-alt" title="ti-mouse-alt" alt="ti-mouse-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-money"><i class="ti-money" title="ti-money" alt="ti-money"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-microphone"><i class="ti-microphone" title="ti-microphone" alt="ti-microphone"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-menu"><i class="ti-menu" title="ti-menu" alt="ti-menu"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-menu-alt"><i class="ti-menu-alt" title="ti-menu-alt" alt="ti-menu-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-map"><i class="ti-map" title="ti-map" alt="ti-map"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-map-alt"><i class="ti-map-alt" title="ti-map-alt" alt="ti-map-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-location-pin"><i class="ti-location-pin" title="ti-location-pin" alt="ti-location-pin"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-light-bulb"><i class="ti-light-bulb" title="ti-light-bulb" alt="ti-light-bulb"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-info"><i class="ti-info" title="ti-info" alt="ti-info"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-infinite"><i class="ti-infinite" title="ti-infinite" alt="ti-infinite"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-id-badge"><i class="ti-id-badge" title="ti-id-badge" alt="ti-id-badge"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-hummer"><i class="ti-hummer" title="ti-hummer" alt="ti-hummer"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-home"><i class="ti-home" title="ti-home" alt="ti-home"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-help"><i class="ti-help" title="ti-help" alt="ti-help"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-headphone"><i class="ti-headphone" title="ti-headphone" alt="ti-headphone"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-harddrives"><i class="ti-harddrives" title="ti-harddrives" alt="ti-harddrives"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-harddrive"><i class="ti-harddrive" title="ti-harddrive" alt="ti-harddrive"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-gift"><i class="ti-gift" title="ti-gift" alt="ti-gift"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-game"><i class="ti-game" title="ti-game" alt="ti-game"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-filter"><i class="ti-filter" title="ti-filter" alt="ti-filter"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-files"><i class="ti-files" title="ti-files" alt="ti-files"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-file"><i class="ti-file" title="ti-file" alt="ti-file"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-zip"><i class="ti-zip" title="ti-zip" alt="ti-zip"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-folder"><i class="ti-folder" title="ti-folder" alt="ti-folder"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-envelope"><i class="ti-envelope" title="ti-envelope" alt="ti-envelope"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-dashboard"><i class="ti-dashboard" title="ti-dashboard" alt="ti-dashboard"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-cloud"><i class="ti-cloud" title="ti-cloud" alt="ti-cloud"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-cloud-up"><i class="ti-cloud-up" title="ti-cloud-up" alt="ti-cloud-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-cloud-down"><i class="ti-cloud-down" title="ti-cloud-down" alt="ti-cloud-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-clipboard"><i class="ti-clipboard" title="ti-clipboard" alt="ti-clipboard"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-calendar"><i class="ti-calendar" title="ti-calendar" alt="ti-calendar"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-book"><i class="ti-book" title="ti-book" alt="ti-book"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-bell"><i class="ti-bell" title="ti-bell" alt="ti-bell"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-basketball"><i class="ti-basketball" title="ti-basketball" alt="ti-basketball"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-bar-chart"><i class="ti-bar-chart" title="ti-bar-chart" alt="ti-bar-chart"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-bar-chart-alt"><i class="ti-bar-chart-alt" title="ti-bar-chart-alt" alt="ti-bar-chart-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-archive"><i class="ti-archive" title="ti-archive" alt="ti-archive"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-anchor"><i class="ti-anchor" title="ti-anchor" alt="ti-anchor"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-alert"><i class="ti-alert" title="ti-alert" alt="ti-alert"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-alarm-clock"><i class="ti-alarm-clock" title="ti-alarm-clock" alt="ti-alarm-clock"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-agenda"><i class="ti-agenda" title="ti-agenda" alt="ti-agenda"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-write"><i class="ti-write" title="ti-write" alt="ti-write"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-wallet"><i class="ti-wallet" title="ti-wallet" alt="ti-wallet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-video-clapper"><i class="ti-video-clapper" title="ti-video-clapper" alt="ti-video-clapper"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-video-camera"><i class="ti-video-camera" title="ti-video-camera" alt="ti-video-camera"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-vector"><i class="ti-vector" title="ti-vector" alt="ti-vector"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-support"><i class="ti-support" title="ti-support" alt="ti-support"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-stamp"><i class="ti-stamp" title="ti-stamp" alt="ti-stamp"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-slice"><i class="ti-slice" title="ti-slice" alt="ti-slice"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-shortcode"><i class="ti-shortcode" title="ti-shortcode" alt="ti-shortcode"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-receipt"><i class="ti-receipt" title="ti-receipt" alt="ti-receipt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pin2"><i class="ti-pin2" title="ti-pin2" alt="ti-pin2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pin-alt"><i class="ti-pin-alt" title="ti-pin-alt" alt="ti-pin-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pencil-alt2"><i class="ti-pencil-alt2" title="ti-pencil-alt2" alt="ti-pencil-alt2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-eraser"><i class="ti-eraser" title="ti-eraser" alt="ti-eraser"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-more"><i class="ti-more" title="ti-more" alt="ti-more"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-more-alt"><i class="ti-more-alt" title="ti-more-alt" alt="ti-more-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-microphone-alt"><i class="ti-microphone-alt" title="ti-microphone-alt" alt="ti-microphone-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-magnet"><i class="ti-magnet" title="ti-magnet" alt="ti-magnet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-line-double"><i class="ti-line-double" title="ti-line-double" alt="ti-line-double"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-line-dotted"><i class="ti-line-dotted" title="ti-line-dotted" alt="ti-line-dotted"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-line-dashed"><i class="ti-line-dashed" title="ti-line-dashed" alt="ti-line-dashed"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-ink-pen"><i class="ti-ink-pen" title="ti-ink-pen" alt="ti-ink-pen"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-info-alt"><i class="ti-info-alt" title="ti-info-alt" alt="ti-info-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-help-alt"><i class="ti-help-alt" title="ti-help-alt" alt="ti-help-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-headphone-alt"><i class="ti-headphone-alt" title="ti-headphone-alt" alt="ti-headphone-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-gallery"><i class="ti-gallery" title="ti-gallery" alt="ti-gallery"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-face-smile"><i class="ti-face-smile" title="ti-face-smile" alt="ti-face-smile"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-face-sad"><i class="ti-face-sad" title="ti-face-sad" alt="ti-face-sad"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-credit-card"><i class="ti-credit-card" title="ti-credit-card" alt="ti-credit-card"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-comments-smiley"><i class="ti-comments-smiley" title="ti-comments-smiley" alt="ti-comments-smiley"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-time"><i class="ti-time" title="ti-time" alt="ti-time"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-share"><i class="ti-share" title="ti-share" alt="ti-share"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-share-alt"><i class="ti-share-alt" title="ti-share-alt" alt="ti-share-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-rocket"><i class="ti-rocket" title="ti-rocket" alt="ti-rocket"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-new-window"><i class="ti-new-window" title="ti-new-window" alt="ti-new-window"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-rss"><i class="ti-rss" title="ti-rss" alt="ti-rss"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-rss-alt"><i class="ti-rss-alt" title="ti-rss-alt" alt="ti-rss-alt"></i></a></div>

										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Control Icons</b></h4>

                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="ti-control-stop"><i class="ti-control-stop" title="ti-control-stop" alt="ti-control-stop"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-control-shuffle"><i class="ti-control-shuffle" title="ti-control-shuffle" alt="ti-control-shuffle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-control-play"><i class="ti-control-play" title="ti-control-play" alt="ti-control-play"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-control-pause"><i class="ti-control-pause" title="ti-control-pause" alt="ti-control-pause"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-control-forward"><i class="ti-control-forward" title="ti-control-forward" alt="ti-control-forward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-control-backward"><i class="ti-control-backward" title="ti-control-backward" alt="ti-control-backward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-volume"><i class="ti-volume" title="ti-volume" alt="ti-volume"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-control-skip-forward"><i class="ti-control-skip-forward" title="ti-control-skip-forward" alt="ti-control-skip-forward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-control-skip-backward"><i class="ti-control-skip-backward" title="ti-control-skip-backward" alt="ti-control-skip-backward"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-control-record"><i class="ti-control-record" title="ti-control-record" alt="ti-control-record"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-control-eject"><i class="ti-control-eject" title="ti-control-eject" alt="ti-control-eject"></i></a></div>


										</div>
										<!-- End row -->


										<h4 class="page-header header-title"><b>Text Editor</b></h4>

                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="ti-paragraph"><i class="ti-paragraph" title="ti-paragraph" alt="ti-paragraph"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-uppercase"><i class="ti-uppercase" title="ti-uppercase" alt="ti-uppercase"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-underline"><i class="ti-underline" title="ti-underline" alt="ti-underline"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-text"><i class="ti-text" title="ti-text" alt="ti-text"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-Italic"><i class="ti-Italic" title="ti-Italic" alt="ti-Italic"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-smallcap"><i class="ti-smallcap" title="ti-smallcap" alt="ti-smallcap"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-list"><i class="ti-list" title="ti-list" alt="ti-list"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-list-ol"><i class="ti-list-ol" title="ti-list-ol" alt="ti-list-ol"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-align-right"><i class="ti-align-right" title="ti-align-right" alt="ti-align-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-align-left"><i class="ti-align-left" title="ti-align-left" alt="ti-align-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-align-justify"><i class="ti-align-justify" title="ti-align-justify" alt="ti-align-justify"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-align-center"><i class="ti-align-center" title="ti-align-center" alt="ti-align-center"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-quote-right"><i class="ti-quote-right" title="ti-quote-right" alt="ti-quote-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-quote-left"><i class="ti-quote-left" title="ti-quote-left" alt="ti-quote-left"></i></a></div>

										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Layout Icons</b></h4>

                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="ti-layout-width-full"><i class="ti-layout-width-full" title="ti-layout-width-full" alt="ti-layout-width-full"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-width-default"><i class="ti-layout-width-default" title="ti-layout-width-default" alt="ti-layout-width-default"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-width-default-alt"><i class="ti-layout-width-default-alt" title="ti-layout-width-default-alt" alt="ti-layout-width-default-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-tab"><i class="ti-layout-tab" title="ti-layout-tab" alt="ti-layout-tab"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-tab-window"><i class="ti-layout-tab-window" title="ti-layout-tab-window" alt="ti-layout-tab-window"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-tab-v"><i class="ti-layout-tab-v" title="ti-layout-tab-v" alt="ti-layout-tab-v"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-tab-min"><i class="ti-layout-tab-min" title="ti-layout-tab-min" alt="ti-layout-tab-min"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-slider"><i class="ti-layout-slider" title="ti-layout-slider" alt="ti-layout-slider"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-slider-alt"><i class="ti-layout-slider-alt" title="ti-layout-slider-alt" alt="ti-layout-slider-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-sidebar-right"><i class="ti-layout-sidebar-right" title="ti-layout-sidebar-right" alt="ti-layout-sidebar-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-sidebar-none"><i class="ti-layout-sidebar-none" title="ti-layout-sidebar-none" alt="ti-layout-sidebar-none"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-sidebar-left"><i class="ti-layout-sidebar-left" title="ti-layout-sidebar-left" alt="ti-layout-sidebar-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-placeholder"><i class="ti-layout-placeholder" title="ti-layout-placeholder" alt="ti-layout-placeholder"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-menu"><i class="ti-layout-menu" title="ti-layout-menu" alt="ti-layout-menu"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-menu-v"><i class="ti-layout-menu-v" title="ti-layout-menu-v" alt="ti-layout-menu-v"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-menu-separated"><i class="ti-layout-menu-separated" title="ti-layout-menu-separated" alt="ti-layout-menu-separated"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-menu-full"><i class="ti-layout-menu-full" title="ti-layout-menu-full" alt="ti-layout-menu-full"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-media-right"><i class="ti-layout-media-right" title="ti-layout-media-right" alt="ti-layout-media-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-media-right-alt"><i class="ti-layout-media-right-alt" title="ti-layout-media-right-alt" alt="ti-layout-media-right-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-media-overlay"><i class="ti-layout-media-overlay" title="ti-layout-media-overlay" alt="ti-layout-media-overlay"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-media-overlay-alt"><i class="ti-layout-media-overlay-alt" title="ti-layout-media-overlay-alt" alt="ti-layout-media-overlay-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-media-overlay-alt-2"><i class="ti-layout-media-overlay-alt-2" title="ti-layout-media-overlay-alt-2" alt="ti-layout-media-overlay-alt-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-media-left"><i class="ti-layout-media-left" title="ti-layout-media-left" alt="ti-layout-media-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-media-left-alt"><i class="ti-layout-media-left-alt" title="ti-layout-media-left-alt" alt="ti-layout-media-left-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-media-center"><i class="ti-layout-media-center" title="ti-layout-media-center" alt="ti-layout-media-center"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-media-center-alt"><i class="ti-layout-media-center-alt" title="ti-layout-media-center-alt" alt="ti-layout-media-center-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-list-thumb"><i class="ti-layout-list-thumb" title="ti-layout-list-thumb" alt="ti-layout-list-thumb"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-list-thumb-alt"><i class="ti-layout-list-thumb-alt" title="ti-layout-list-thumb-alt" alt="ti-layout-list-thumb-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-list-post"><i class="ti-layout-list-post" title="ti-layout-list-post" alt="ti-layout-list-post"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-list-large-image"><i class="ti-layout-list-large-image" title="ti-layout-list-large-image" alt="ti-layout-list-large-image"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-line-solid"><i class="ti-layout-line-solid" title="ti-layout-line-solid" alt="ti-layout-line-solid"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-grid4"><i class="ti-layout-grid4" title="ti-layout-grid4" alt="ti-layout-grid4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-grid3"><i class="ti-layout-grid3" title="ti-layout-grid3" alt="ti-layout-grid3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-grid2"><i class="ti-layout-grid2" title="ti-layout-grid2" alt="ti-layout-grid2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-grid2-thumb"><i class="ti-layout-grid2-thumb" title="ti-layout-grid2-thumb" alt="ti-layout-grid2-thumb"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-cta-right"><i class="ti-layout-cta-right" title="ti-layout-cta-right" alt="ti-layout-cta-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-cta-left"><i class="ti-layout-cta-left" title="ti-layout-cta-left" alt="ti-layout-cta-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-cta-center"><i class="ti-layout-cta-center" title="ti-layout-cta-center" alt="ti-layout-cta-center"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-cta-btn-right"><i class="ti-layout-cta-btn-right" title="ti-layout-cta-btn-right" alt="ti-layout-cta-btn-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-cta-btn-left"><i class="ti-layout-cta-btn-left" title="ti-layout-cta-btn-left" alt="ti-layout-cta-btn-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-column4"><i class="ti-layout-column4" title="ti-layout-column4" alt="ti-layout-column4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-column3"><i class="ti-layout-column3" title="ti-layout-column3" alt="ti-layout-column3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-column2"><i class="ti-layout-column2" title="ti-layout-column2" alt="ti-layout-column2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-accordion-separated"><i class="ti-layout-accordion-separated" title="ti-layout-accordion-separated" alt="ti-layout-accordion-separated"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-accordion-merged"><i class="ti-layout-accordion-merged" title="ti-layout-accordion-merged" alt="ti-layout-accordion-merged"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-accordion-list"><i class="ti-layout-accordion-list" title="ti-layout-accordion-list" alt="ti-layout-accordion-list"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-widgetized"><i class="ti-widgetized" title="ti-widgetized" alt="ti-widgetized"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-widget"><i class="ti-widget" title="ti-widget" alt="ti-widget"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-widget-alt"><i class="ti-widget-alt" title="ti-widget-alt" alt="ti-widget-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-view-list"><i class="ti-view-list" title="ti-view-list" alt="ti-view-list"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-view-list-alt"><i class="ti-view-list-alt" title="ti-view-list-alt" alt="ti-view-list-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-view-grid"><i class="ti-view-grid" title="ti-view-grid" alt="ti-view-grid"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-upload"><i class="ti-upload" title="ti-upload" alt="ti-upload"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-download"><i class="ti-download" title="ti-download" alt="ti-download"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-loop"><i class="ti-loop" title="ti-loop" alt="ti-loop"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-sidebar-2"><i class="ti-layout-sidebar-2" title="ti-layout-sidebar-2" alt="ti-layout-sidebar-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-grid4-alt"><i class="ti-layout-grid4-alt" title="ti-layout-grid4-alt" alt="ti-layout-grid4-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-grid3-alt"><i class="ti-layout-grid3-alt" title="ti-layout-grid3-alt" alt="ti-layout-grid3-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-grid2-alt"><i class="ti-layout-grid2-alt" title="ti-layout-grid2-alt" alt="ti-layout-grid2-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-column4-alt"><i class="ti-layout-column4-alt" title="ti-layout-column4-alt" alt="ti-layout-column4-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-column3-alt"><i class="ti-layout-column3-alt" title="ti-layout-column3-alt" alt="ti-layout-column3-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-layout-column2-alt"><i class="ti-layout-column2-alt" title="ti-layout-column2-alt" alt="ti-layout-column2-alt"></i></a></div>


										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Brand Icons</b></h4>

                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="ti-flickr"><i class="ti-flickr" title="ti-flickr" alt="ti-flickr"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-flickr-alt"><i class="ti-flickr-alt" title="ti-flickr-alt" alt="ti-flickr-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-instagram"><i class="ti-instagram" title="ti-instagram" alt="ti-instagram"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-google"><i class="ti-google" title="ti-google" alt="ti-google"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-github"><i class="ti-github" title="ti-github" alt="ti-github"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-facebook"><i class="ti-facebook" title="ti-facebook" alt="ti-facebook"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-dropbox"><i class="ti-dropbox" title="ti-dropbox" alt="ti-dropbox"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-dropbox-alt"><i class="ti-dropbox-alt" title="ti-dropbox-alt" alt="ti-dropbox-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-dribbble"><i class="ti-dribbble" title="ti-dribbble" alt="ti-dribbble"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-apple"><i class="ti-apple" title="ti-apple" alt="ti-apple"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-android"><i class="ti-android" title="ti-android" alt="ti-android"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-yahoo"><i class="ti-yahoo" title="ti-yahoo" alt="ti-yahoo"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-trello"><i class="ti-trello" title="ti-trello" alt="ti-trello"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-stack-overflow"><i class="ti-stack-overflow" title="ti-stack-overflow" alt="ti-stack-overflow"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-soundcloud"><i class="ti-soundcloud" title="ti-soundcloud" alt="ti-soundcloud"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-sharethis"><i class="ti-sharethis" title="ti-sharethis" alt="ti-sharethis"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-sharethis-alt"><i class="ti-sharethis-alt" title="ti-sharethis-alt" alt="ti-sharethis-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-reddit"><i class="ti-reddit" title="ti-reddit" alt="ti-reddit"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-microsoft"><i class="ti-microsoft" title="ti-microsoft" alt="ti-microsoft"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-microsoft-alt"><i class="ti-microsoft-alt" title="ti-microsoft-alt" alt="ti-microsoft-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-linux"><i class="ti-linux" title="ti-linux" alt="ti-linux"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-jsfiddle"><i class="ti-jsfiddle" title="ti-jsfiddle" alt="ti-jsfiddle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-joomla"><i class="ti-joomla" title="ti-joomla" alt="ti-joomla"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-html5"><i class="ti-html5" title="ti-html5" alt="ti-html5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-css3"><i class="ti-css3" title="ti-css3" alt="ti-css3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-drupal"><i class="ti-drupal" title="ti-drupal" alt="ti-drupal"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-wordpress"><i class="ti-wordpress" title="ti-wordpress" alt="ti-wordpress"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-tumblr"><i class="ti-tumblr" title="ti-tumblr" alt="ti-tumblr"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-tumblr-alt"><i class="ti-tumblr-alt" title="ti-tumblr-alt" alt="ti-tumblr-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-skype"><i class="ti-skype" title="ti-skype" alt="ti-skype"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-youtube"><i class="ti-youtube" title="ti-youtube" alt="ti-youtube"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-vimeo"><i class="ti-vimeo" title="ti-vimeo" alt="ti-vimeo"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-vimeo-alt"><i class="ti-vimeo-alt" title="ti-vimeo-alt" alt="ti-vimeo-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-twitter"><i class="ti-twitter" title="ti-twitter" alt="ti-twitter"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-twitter-alt"><i class="ti-twitter-alt" title="ti-twitter-alt" alt="ti-twitter-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-linkedin"><i class="ti-linkedin" title="ti-linkedin" alt="ti-linkedin"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pinterest"><i class="ti-pinterest" title="ti-pinterest" alt="ti-pinterest"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-pinterest-alt"><i class="ti-pinterest-alt" title="ti-pinterest-alt" alt="ti-pinterest-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-themify-logo"><i class="ti-themify-logo" title="ti-themify-logo" alt="ti-themify-logo"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-themify-favicon"><i class="ti-themify-favicon" title="ti-themify-favicon" alt="ti-themify-favicon"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="ti-themify-favicon-alt"><i class="ti-themify-favicon-alt" title="ti-themify-favicon-alt" alt="ti-themify-favicon-alt"></i></a></div>

										</div>
										<!-- End row -->

                                    </div>
					  </div>
					</div>
				  </div>
				  <div class="hidden panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#icons" href="#simplelineicons">
						Simple line Icons</a>
					  </h4>
					</div>
					<div id="simplelineicons" class="panel-collapse collapse">
					  <div class="panel-body">
							<div class="card-box">
                                	<div class="">
                                		<div class="row">
                                			<div class="row">

												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-user"><i class="icon-user" title="icon-user
													" alt="icon-user
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-people"><i class="icon-people" title="icon-people

													" alt="icon-people

													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-user-female"><i class="icon-user-female" title="
															icon-user-female
													" alt="
															icon-user-female
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-user-follow"><i class="icon-user-follow" title="
															icon-user-follow
													" alt="
															icon-user-follow
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-user-following"><i class="icon-user-following" title="
															icon-user-following
													" alt="
															icon-user-following
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-user-unfollow"><i class="icon-user-unfollow" title="
															icon-user-unfollow
													" alt="
															icon-user-unfollow
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-login"><i class="icon-login" title="
															icon-login
													" alt="
															icon-login
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-logout"><i class="icon-logout" title="
															icon-logout
													" alt="
															icon-logout
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-emotsmile"><i class="icon-emotsmile" title="
															icon-emotsmile
													" alt="
															icon-emotsmile
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-phone"><i class="icon-phone" title="
															icon-phone
													" alt="
															icon-phone
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-call-end"><i class="icon-call-end" title="
															icon-call-end
													" alt="
															icon-call-end
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-call-in"><i class="icon-call-in" title="
															icon-call-in
													" alt="
															icon-call-in
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-call-out"><i class="icon-call-out" title="
															icon-call-out
													" alt="
															icon-call-out
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-map"><i class="icon-map" title="
															icon-map
													" alt="
															icon-map
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-location-pin"><i class="icon-location-pin" title="
															icon-location-pin
													" alt="
															icon-location-pin
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-direction"><i class="icon-direction" title="
															icon-direction
													" alt="
															icon-direction
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-directions"><i class="icon-directions" title="
															icon-directions
													" alt="
															icon-directions
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-compass"><i class="icon-compass" title="
															icon-compass
													" alt="
															icon-compass
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-layers"><i class="icon-layers" title="
															icon-layers
													" alt="
															icon-layers
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-menu"><i class="icon-menu" title="
															icon-menu
													" alt="
															icon-menu
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-list"><i class="icon-list" title="
															icon-list
													" alt="
															icon-list
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-options-vertical"><i class="icon-options-vertical" title="
															icon-options-vertical
													" alt="
															icon-options-vertical
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-options"><i class="icon-options" title="
															icon-options
													" alt="
															icon-options
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-arrow-down"><i class="icon-arrow-down" title="
															icon-arrow-down
													" alt="
															icon-arrow-down
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-arrow-left"><i class="icon-arrow-left" title="
															icon-arrow-left
													" alt="
															icon-arrow-left
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-arrow-right"><i class="icon-arrow-right" title="
															icon-arrow-right
													" alt="
															icon-arrow-right
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-arrow-up"><i class="icon-arrow-up" title="
															icon-arrow-up
													" alt="
															icon-arrow-up
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-arrow-up-circle"><i class="icon-arrow-up-circle" title="
															icon-arrow-up-circle
													" alt="
															icon-arrow-up-circle
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-arrow-left-circle"><i class="icon-arrow-left-circle" title="
															icon-arrow-left-circle
													" alt="
															icon-arrow-left-circle
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-arrow-right-circle"><i class="icon-arrow-right-circle" title="
															icon-arrow-right-circle
													" alt="
															icon-arrow-right-circle
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-arrow-down-circle"><i class="icon-arrow-down-circle" title="
															icon-arrow-down-circle
													" alt="
															icon-arrow-down-circle
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-check"><i class="icon-check" title="
															icon-check
													" alt="
															icon-check
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-clock"><i class="icon-clock" title="
															icon-clock
													" alt="
															icon-clock
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-plus"><i class="icon-plus" title="
															icon-plus
													" alt="
															icon-plus
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-close"><i class="icon-close" title="
															icon-close
													" alt="
															icon-close
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-trophy"><i class="icon-trophy" title="
															icon-trophy
													" alt="
															icon-trophy
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-screen-smartphone"><i class="icon-screen-smartphone" title="
															icon-screen-smartphone
													" alt="
															icon-screen-smartphone
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-screen-desktop"><i class="icon-screen-desktop" title="
															icon-screen-desktop
													" alt="
															icon-screen-desktop
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-plane"><i class="icon-plane" title="
															icon-plane
													" alt="
															icon-plane
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-notebook"><i class="icon-notebook" title="
															icon-notebook
													" alt="
															icon-notebook
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-mustache"><i class="icon-mustache" title="
															icon-mustache
													" alt="
															icon-mustache
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-mouse"><i class="icon-mouse" title="
															icon-mouse
													" alt="
															icon-mouse
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-magnet"><i class="icon-magnet" title="
															icon-magnet
													" alt="
															icon-magnet
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-energy"><i class="icon-energy" title="
															icon-energy
													" alt="
															icon-energy
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-disc"><i class="icon-disc" title="
															icon-disc
													" alt="
															icon-disc
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-cursor"><i class="icon-cursor" title="
															icon-cursor
													" alt="
															icon-cursor
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-cursor-move"><i class="icon-cursor-move" title="
															icon-cursor-move
													" alt="
															icon-cursor-move
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-crop"><i class="icon-crop" title="
															icon-crop
													" alt="
															icon-crop
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-chemistry"><i class="icon-chemistry" title="
															icon-chemistry
													" alt="
															icon-chemistry
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-speedometer"><i class="icon-speedometer" title="
															icon-speedometer
													" alt="
															icon-speedometer
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-shield"><i class="icon-shield" title="
															icon-shield
													" alt="
															icon-shield
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-screen-tablet"><i class="icon-screen-tablet" title="
															icon-screen-tablet
													" alt="
															icon-screen-tablet
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-magic-wand"><i class="icon-magic-wand" title="
															icon-magic-wand
													" alt="
															icon-magic-wand
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-hourglass"><i class="icon-hourglass" title="
															icon-hourglass
													" alt="
															icon-hourglass
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-graduation"><i class="icon-graduation" title="
															icon-graduation
													" alt="
															icon-graduation
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-ghost"><i class="icon-ghost" title="
															icon-ghost
													" alt="
															icon-ghost
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-game-controller"><i class="icon-game-controller" title="
															icon-game-controller
													" alt="
															icon-game-controller
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-fire"><i class="icon-fire" title="
															icon-fire
													" alt="
															icon-fire
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-eyeglass"><i class="icon-eyeglass" title="
															icon-eyeglass
													" alt="
															icon-eyeglass
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-envelope-open"><i class="icon-envelope-open" title="
															icon-envelope-open
													" alt="
															icon-envelope-open
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-envolope-letter"><i class="icon-envolope-letter" title="
															icon-envolope-letter
													" alt="
															icon-envolope-letter
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-bell"><i class="icon-bell" title="
															icon-bell
													" alt="
															icon-bell
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-badge"><i class="icon-badge" title="
															icon-badge
													" alt="
															icon-badge
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-anchor"><i class="icon-anchor" title="
															icon-anchor
													" alt="
															icon-anchor
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-wallet"><i class="icon-wallet" title="
															icon-wallet
													" alt="
															icon-wallet
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-vector"><i class="icon-vector" title="
															icon-vector
													" alt="
															icon-vector
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-speech"><i class="icon-speech" title="
															icon-speech
													" alt="
															icon-speech
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-puzzle"><i class="icon-puzzle" title="
															icon-puzzle
													" alt="
															icon-puzzle
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-printer"><i class="icon-printer" title="
															icon-printer
													" alt="
															icon-printer
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-present"><i class="icon-present" title="
															icon-present
													" alt="
															icon-present
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-playlist"><i class="icon-playlist" title="
															icon-playlist
													" alt="
															icon-playlist
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-pin"><i class="icon-pin" title="
															icon-pin
													" alt="
															icon-pin
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-picture"><i class="icon-picture" title="
															icon-picture
													" alt="
															icon-picture
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-handbag"><i class="icon-handbag" title="
															icon-handbag
													" alt="
															icon-handbag
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-globe-alt"><i class="icon-globe-alt" title="
															icon-globe-alt
													" alt="
															icon-globe-alt
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-globe"><i class="icon-globe" title="
															icon-globe
													" alt="
															icon-globe
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-folder-alt"><i class="icon-folder-alt" title="
															icon-folder-alt
													" alt="
															icon-folder-alt
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-folder"><i class="icon-folder" title="
															icon-folder
													" alt="
															icon-folder
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-film"><i class="icon-film" title="
															icon-film
													" alt="
															icon-film
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-feed"><i class="icon-feed" title="
															icon-feed
													" alt="
															icon-feed
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-drop"><i class="icon-drop" title="
															icon-drop
													" alt="
															icon-drop
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-drawar"><i class="icon-drawar" title="
															icon-drawar
													" alt="
															icon-drawar
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-docs"><i class="icon-docs" title="
															icon-docs
													" alt="
															icon-docs
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-doc"><i class="icon-doc" title="
															icon-doc
													" alt="
															icon-doc
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-diamond"><i class="icon-diamond" title="
															icon-diamond
													" alt="
															icon-diamond
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-cup"><i class="icon-cup" title="
															icon-cup
													" alt="
															icon-cup
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-calculator"><i class="icon-calculator" title="
															icon-calculator
													" alt="
															icon-calculator
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-bubbles"><i class="icon-bubbles" title="
															icon-bubbles
													" alt="
															icon-bubbles
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-briefcase"><i class="icon-briefcase" title="
															icon-briefcase
													" alt="
															icon-briefcase
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-book-open"><i class="icon-book-open" title="
															icon-book-open
													" alt="
															icon-book-open
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-basket-loaded"><i class="icon-basket-loaded" title="
															icon-basket-loaded
													" alt="
															icon-basket-loaded
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-basket"><i class="icon-basket" title="
															icon-basket
													" alt="
															icon-basket
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-bag"><i class="icon-bag" title="
															icon-bag
													" alt="
															icon-bag
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-action-undo"><i class="icon-action-undo" title="
															icon-action-undo
													" alt="
															icon-action-undo
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-action-redo"><i class="icon-action-redo" title="
															icon-action-redo
													" alt="
															icon-action-redo
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-wrench"><i class="icon-wrench" title="
															icon-wrench
													" alt="
															icon-wrench
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-umbrella"><i class="icon-umbrella" title="
															icon-umbrella
													" alt="
															icon-umbrella
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-trash"><i class="icon-trash" title="
															icon-trash
													" alt="
															icon-trash
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-tag"><i class="icon-tag" title="
															icon-tag
													" alt="
															icon-tag
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-support"><i class="icon-support" title="
															.icon-support
													" alt="
															.icon-support
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-frame"><i class="icon-frame" title="
															icon-frame
													" alt="
															icon-frame
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-size-fullscreen"><i class="icon-size-fullscreen" title="
															icon-size-fullscreen
													" alt="
															icon-size-fullscreen
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-size-actual"><i class="icon-size-actual" title="
															icon-size-actual
													" alt="
															icon-size-actual
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-shuffle"><i class="icon-shuffle" title="
															icon-shuffle
													" alt="
															icon-shuffle
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-share-alt"><i class="icon-share-alt" title="
															icon-share-alt
													" alt="
															icon-share-alt
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-share"><i class="icon-share" title="
															icon-share
													" alt="
															icon-share
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-rocket"><i class="icon-rocket" title="
															icon-rocket
													" alt="
															icon-rocket
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-question"><i class="icon-question" title="
															icon-question
													" alt="
															icon-question
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-pie-chart"><i class="icon-pie-chart" title="
															icon-pie-chart
													" alt="
															icon-pie-chart
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-pencil"><i class="icon-pencil" title="
															icon-pencil
													" alt="
															icon-pencil
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-note"><i class="icon-note" title="
															icon-note
													" alt="
															icon-note
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-loop"><i class="icon-loop" title="
															icon-loop
													" alt="
															icon-loop
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-home"><i class="icon-home" title="
															icon-home
													" alt="
															icon-home
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-grid"><i class="icon-grid" title="
															icon-grid
													" alt="
															icon-grid
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-graph"><i class="icon-graph" title="
															icon-graph
													" alt="
															icon-graph
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-microphone"><i class="icon-microphone" title="
															icon-microphone
													" alt="
															icon-microphone
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-music-tone-alt"><i class="icon-music-tone-alt" title="
															icon-music-tone-alt
													" alt="
															icon-music-tone-alt
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-music-tone"><i class="icon-music-tone" title="
															icon-music-tone
													" alt="
															icon-music-tone
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-earphones-alt"><i class="icon-earphones-alt" title="
															icon-earphones-alt
													" alt="
															icon-earphones-alt
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-earphones"><i class="icon-earphones" title="
															icon-earphones
													" alt="
															icon-earphones
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-equalizer"><i class="icon-equalizer" title="
															icon-equalizer
													" alt="
															icon-equalizer
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-like"><i class="icon-like" title="
															icon-like
													" alt="
															icon-like
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-dislike"><i class="icon-dislike" title="
															icon-dislike
													" alt="
															icon-dislike
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-control-start"><i class="icon-control-start" title="
															icon-control-start
													" alt="
															icon-control-start
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-control-rewind"><i class="icon-control-rewind" title="
															icon-control-rewind
													" alt="
															icon-control-rewind
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-control-play"><i class="icon-control-play" title="
															icon-control-play
													" alt="
															icon-control-play
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-control-pause"><i class="icon-control-pause" title="
															icon-control-pause
													" alt="
															icon-control-pause
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-control-forward"><i class="icon-control-forward" title="
															icon-control-forward
													" alt="
															icon-control-forward
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-control-end"><i class="icon-control-end" title="
															icon-control-end
													" alt="
															icon-control-end
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-volume-1"><i class="icon-volume-1" title="
															icon-volume-1
													" alt="
															icon-volume-1
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-volume-2"><i class="icon-volume-2" title="
															icon-volume-2
													" alt="
															icon-volume-2
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-volume-off"><i class="icon-volume-off" title="
															icon-volume-off
													" alt="
															icon-volume-off
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-calender"><i class="icon-calender" title="
															icon-calender
													" alt="
															icon-calender
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-bulb"><i class="icon-bulb" title="
															icon-bulb
													" alt="
															icon-bulb
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-chart"><i class="icon-chart" title="
															icon-chart
													" alt="
															icon-chart
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-ban"><i class="icon-ban" title="
															icon-ban
													" alt="
															icon-ban
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-bubble"><i class="icon-bubble" title="
															icon-bubble
													" alt="
															icon-bubble
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-camrecorder"><i class="icon-camrecorder" title="
															icon-camrecorder
													" alt="
															icon-camrecorder
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-camera"><i class="icon-camera" title="
															icon-camera
													" alt="
															icon-camera
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-cloud-download"><i class="icon-cloud-download" title="
															icon-cloud-download
													" alt="
															icon-cloud-download
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-cloud-upload"><i class="icon-cloud-upload" title="
															icon-cloud-upload
													" alt="
															icon-cloud-upload
													"></i></a></div>
												</div>

												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-eye"><i class="icon-eye" title="
															icon-eye
													" alt="
															icon-eye
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-flag"><i class="icon-flag" title="
															icon-flag
													" alt="
															icon-flag
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-heart"><i class="icon-heart" title="
															icon-heart
													" alt="
															icon-heart
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-info"><i class="icon-info" title="
															icon-info
													" alt="
															icon-info
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-key"><i class="icon-key" title="
															icon-key
													" alt="
															icon-key
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-link"><i class="icon-link" title="
															icon-link
													" alt="
															icon-link
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-lock"><i class="icon-lock" title="
															icon-lock
													" alt="
															icon-lock
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-lock-open"><i class="icon-lock-open" title="
															icon-lock-open
													" alt="
															icon-lock-open
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-magnifier"><i class="icon-magnifier" title="
															icon-magnifier
													" alt="
															icon-magnifier
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-magnifier-add"><i class="icon-magnifier-add" title="
															icon-magnifier-add
													" alt="
															icon-magnifier-add
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-magnifier-remove"><i class="icon-magnifier-remove" title="
															icon-magnifier-remove
													" alt="
															icon-magnifier-remove
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-paper-clip"><i class="icon-paper-clip" title="
															icon-paper-clip
													" alt="
															icon-paper-clip
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-paper-plane"><i class="icon-paper-plane" title="
															icon-paper-plane
													" alt="
															icon-paper-plane
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-power"><i class="icon-power" title="
															icon-power
													" alt="
															icon-power
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-refresh"><i class="icon-refresh" title="
															icon-refresh
													" alt="
															icon-refresh
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-reload"><i class="icon-reload" title="
															icon-reload
													" alt="
															icon-reload
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-settings"><i class="icon-settings" title="
															icon-settings
													" alt="
															icon-settings
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-star"><i class="icon-star" title="
															icon-star
													" alt="
															icon-star
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-symble-female"><i class="icon-symble-female" title="
															icon-symble-female
													" alt="
															icon-symble-female
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-symbol-male"><i class="icon-symbol-male" title="
															icon-symbol-male
													" alt="
															icon-symbol-male
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-target"><i class="icon-target" title="
															icon-target
													" alt="
															icon-target
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-credit-card"><i class="icon-credit-card" title="
															icon-credit-card
													" alt="
															icon-credit-card
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-paypal"><i class="icon-paypal" title="
															icon-paypal
													" alt="
															icon-paypal
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-tumblr"><i class="icon-social-tumblr" title="
															icon-social-tumblr
													" alt="
															icon-social-tumblr
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-twitter"><i class="icon-social-twitter" title="
															icon-social-twitter
													" alt="
															icon-social-twitter
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-facebook"><i class="icon-social-facebook" title="
															icon-social-facebook
													" alt="
															icon-social-facebook
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-instagram"><i class="icon-social-instagram" title="
															icon-social-instagram
													" alt="
															icon-social-instagram
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-linkedin"><i class="icon-social-linkedin" title="
															icon-social-linkedin
													" alt="
															icon-social-linkedin
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-pintarest"><i class="icon-social-pintarest" title="
															icon-social-pintarest
													" alt="
															icon-social-pintarest
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-github"><i class="icon-social-github" title="
															icon-social-github
													" alt="
															icon-social-github
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-gplus"><i class="icon-social-gplus" title="
															icon-social-gplus
													" alt="
															icon-social-gplus
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-reddit"><i class="icon-social-reddit" title="
															icon-social-reddit
													" alt="
															icon-social-reddit
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-skype"><i class="icon-social-skype" title="
															icon-social-skype
													" alt="
															icon-social-skype
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-dribbble"><i class="icon-social-dribbble" title="
															icon-social-dribbble
													" alt="
															icon-social-dribbble
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-behance"><i class="icon-social-behance" title="
															icon-social-behance
													" alt="
															icon-social-behance
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-foursqare"><i class="icon-social-foursqare" title="
															icon-social-foursqare
													" alt="
															icon-social-foursqare
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-soundcloud"><i class="icon-social-soundcloud" title="
															icon-social-soundcloud
													" alt="
															icon-social-soundcloud
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-spotify"><i class="icon-social-spotify" title="
															icon-social-spotify
													" alt="
															icon-social-spotify
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-stumbleupon"><i class="icon-social-stumbleupon" title="
															icon-social-stumbleupon
													" alt="
															icon-social-stumbleupon
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-youtube"><i class="icon-social-youtube" title="
															icon-social-youtube
													" alt="
															icon-social-youtube
													"></i></a></div>
												</div>
												<div class="icon">
													<div class="preview">
														<a href="#" data-icon="icon-social-dropbox"><i class="icon-social-dropbox" title="
															icon-social-dropbox
													" alt="
															icon-social-dropbox
													"></i></a></div>
												</div>
											</div>
                                		</div>
                                	</div>
                                </div>
					  </div>
					</div>
				  </div>
				  <div class="hidden panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#icons" href="#weathericons">
						Weather Icons</a>
					  </h4>
					</div>
					<div id="weathericons" class="panel-collapse collapse">
					  <div class="panel-body">
								<div class="card-box">

                                        <h4 class="page-header m-t-0 header-title"><b>26 New 2.0 Icons</b></h4>
                                        <div class="row">
											<div class="icon">
												<a href="#" data-icon="wi wi-day-cloudy-high"><i class="wi wi-day-cloudy-high" title="wi wi-day-cloudy-high" alt="wi wi-day-cloudy-high"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moonrise"><i class="wi wi-moonrise" title="wi wi-moonrise
											" alt="wi wi-moonrise"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-na"><i class="wi wi-na" title="wi wi-na
											" alt="wi wi-na"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-volcano"><i class="wi wi-volcano" title="wi wi-volcano
											" alt="wi wi-volcano"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-light-wind"><i class="wi wi-day-light-wind" title="wi wi-day-light-wind
											" alt="wi wi-day-light-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moonset"><i class="wi wi-moonset" title="wi wi-moonset
											" alt="wi wi-moonset"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-flood"><i class="wi wi-flood" title="wi wi-flood
											" alt="wi wi-flood"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-train"><i class="wi wi-train" title="wi wi-train
											" alt="wi wi-train"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-sleet"><i class="wi wi-day-sleet" title="wi wi-day-sleet
											" alt="wi wi-day-sleet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-sleet"><i class="wi wi-night-sleet" title="wi wi-night-sleet
											" alt="wi wi-night-sleet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-sandstorm"><i class="wi wi-sandstorm" title="wi wi-sandstorm
											" alt="wi wi-sandstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-small-craft-advisory"><i class="wi wi-small-craft-advisory" title="wi wi-small-craft-advisory
											" alt="wi wi-small-craft-advisory"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-haze"><i class="wi wi-day-haze" title="wi wi-day-haze
											" alt="wi wi-day-haze"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-sleet"><i class="wi wi-night-alt-sleet" title="wi wi-night-alt-sleet
											" alt="wi wi-night-alt-sleet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-tsunami"><i class="wi wi-tsunami" title="wi wi-tsunami
											" alt="wi wi-tsunami"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-gale-warning"><i class="wi wi-gale-warning" title="wi wi-gale-warning
											" alt="wi wi-gale-warning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-cloudy-high"><i class="wi wi-night-cloudy-high" title="wi wi-night-cloudy-high
											" alt="wi wi-night-cloudy-high"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-raindrop"><i class="wi wi-raindrop" title="wi wi-raindrop
											" alt="wi wi-raindrop"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-earthquake"><i class="wi wi-earthquake" title="wi wi-earthquake
											" alt="wi wi-earthquake"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-storm-warning"><i class="wi wi-storm-warning" title="wi wi-storm-warning
											" alt="wi wi-storm-warning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-partly-cloudy"><i class="wi wi-night-alt-partly-cloudy" title="wi wi-night-alt-partly-cloudy
											" alt="wi wi-night-alt-partly-cloudy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-barometer"><i class="wi wi-barometer" title="wi wi-barometer
											" alt="wi wi-barometer"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-fire"><i class="wi wi-fire" title="wi wi-fire
											" alt="wi wi-fire"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-hurricane-warning"><i class="wi wi-hurricane-warning" title="wi wi-hurricane-warning
											" alt="wi wi-hurricane-warning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-sleet"><i class="wi wi-sleet" title="wi wi-sleet
											" alt="wi wi-sleet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-humidity"><i class="wi wi-humidity" title="wi wi-humidity
											" alt="wi wi-humidity"></i></a></div>


										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Daytime Icons</b></h4>
                                        <div class="row">
											<div class="icon">
												<a href="#" data-icon="wi wi-day-sunny"><i class="wi wi-day-sunny" title="wi wi-day-sunny" alt="wi wi-day-sunny"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-cloudy"><i class="wi wi-day-cloudy" title="wi wi-day-cloudy" alt="wi wi-day-cloudy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-cloudy-gusts"><i class="wi wi-day-cloudy-gusts" title="wi wi-day-cloudy-gusts" alt="wi wi-day-cloudy-gusts"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-cloudy-windy"><i class="wi wi-day-cloudy-windy" title="wi wi-day-cloudy-windy" alt="wi wi-day-cloudy-windy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-fog"><i class="wi wi-day-fog" title="wi wi-day-fog" alt="wi wi-day-fog"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-hail"><i class="wi wi-day-hail" title="wi wi-day-hail" alt="wi wi-day-hail"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-haze"><i class="wi wi-day-haze" title="wi wi-day-haze" alt="wi wi-day-haze"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-lightning"><i class="wi wi-day-lightning" title="wi wi-day-lightning" alt="wi wi-day-lightning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-rain"><i class="wi wi-day-rain" title="wi wi-day-rain" alt="wi wi-day-rain"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-rain-mix"><i class="wi wi-day-rain-mix" title="wi wi-day-rain-mix" alt="wi wi-day-rain-mix"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-rain-wind"><i class="wi wi-day-rain-wind" title="wi wi-day-rain-wind" alt="wi wi-day-rain-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-showers"><i class="wi wi-day-showers" title="wi wi-day-showers" alt="wi wi-day-showers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-sleet"><i class="wi wi-day-sleet" title="wi wi-day-sleet" alt="wi wi-day-sleet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-sleet-storm"><i class="wi wi-day-sleet-storm" title="wi wi-day-sleet-storm" alt="wi wi-day-sleet-storm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-snow"><i class="wi wi-day-snow" title="wi wi-day-snow" alt="wi wi-day-snow"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-snow-thunderstorm"><i class="wi wi-day-snow-thunderstorm" title="wi wi-day-snow-thunderstorm" alt="wi wi-day-snow-thunderstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-snow-wind"><i class="wi wi-day-snow-wind" title="wi wi-day-snow-wind" alt="wi wi-day-snow-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-sprinkle"><i class="wi wi-day-sprinkle" title="wi wi-day-sprinkle" alt="wi wi-day-sprinkle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-storm-showers"><i class="wi wi-day-storm-showers" title="wi wi-day-storm-showers" alt="wi wi-day-storm-showers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-sunny-overcast"><i class="wi wi-day-sunny-overcast" title="wi wi-day-sunny-overcast" alt="wi wi-day-sunny-overcast"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-thunderstorm"><i class="wi wi-day-thunderstorm" title="wi wi-day-thunderstorm" alt="wi wi-day-thunderstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-windy"><i class="wi wi-day-windy" title="wi wi-day-windy" alt="wi wi-day-windy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-solar-eclipse"><i class="wi wi-solar-eclipse" title="wi wi-solar-eclipse" alt="wi wi-solar-eclipse"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-hot"><i class="wi wi-hot" title="wi wi-hot" alt="wi wi-hot"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-cloudy-high"><i class="wi wi-day-cloudy-high" title="wi wi-day-cloudy-high" alt="wi wi-day-cloudy-high"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-day-light-wind"><i class="wi wi-day-light-wind" title="wi wi-day-light-wind" alt="wi wi-day-light-wind"></i></a></div>

										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Nighttime Icons</b></h4>
                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="wi wi-night-clear"><i class="wi wi-night-clear" title="wi wi-night-clear" alt="wi wi-night-clear"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-cloudy"><i class="wi wi-night-alt-cloudy" title="wi wi-night-alt-cloudy" alt="wi wi-night-alt-cloudy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-cloudy-gusts"><i class="wi wi-night-alt-cloudy-gusts" title="wi wi-night-alt-cloudy-gusts" alt="wi wi-night-alt-cloudy-gusts"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-cloudy-windy"><i class="wi wi-night-alt-cloudy-windy" title="wi wi-night-alt-cloudy-windy" alt="wi wi-night-alt-cloudy-windy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-hail"><i class="wi wi-night-alt-hail" title="wi wi-night-alt-hail" alt="wi wi-night-alt-hail"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-lightning"><i class="wi wi-night-alt-lightning" title="wi wi-night-alt-lightning" alt="wi wi-night-alt-lightning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-rain"><i class="wi wi-night-alt-rain" title="wi wi-night-alt-rain" alt="wi wi-night-alt-rain"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-rain-mix"><i class="wi wi-night-alt-rain-mix" title="wi wi-night-alt-rain-mix" alt="wi wi-night-alt-rain-mix"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-rain-wind"><i class="wi wi-night-alt-rain-wind" title="wi wi-night-alt-rain-wind" alt="wi wi-night-alt-rain-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-showers"><i class="wi wi-night-alt-showers" title="wi wi-night-alt-showers" alt="wi wi-night-alt-showers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-sleet"><i class="wi wi-night-alt-sleet" title="wi wi-night-alt-sleet" alt="wi wi-night-alt-sleet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-sleet-storm"><i class="wi wi-night-alt-sleet-storm" title="wi wi-night-alt-sleet-storm" alt="wi wi-night-alt-sleet-storm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-snow"><i class="wi wi-night-alt-snow" title="wi wi-night-alt-snow" alt="wi wi-night-alt-snow"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-snow-thunderstorm"><i class="wi wi-night-alt-snow-thunderstorm" title="wi wi-night-alt-snow-thunderstorm" alt="wi wi-night-alt-snow-thunderstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-snow-wind"><i class="wi wi-night-alt-snow-wind" title="wi wi-night-alt-snow-wind" alt="wi wi-night-alt-snow-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-sprinkle"><i class="wi wi-night-alt-sprinkle" title="wi wi-night-alt-sprinkle" alt="wi wi-night-alt-sprinkle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-storm-showers"><i class="wi wi-night-alt-storm-showers" title="wi wi-night-alt-storm-showers" alt="wi wi-night-alt-storm-showers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-thunderstorm"><i class="wi wi-night-alt-thunderstorm" title="wi wi-night-alt-thunderstorm" alt="wi wi-night-alt-thunderstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-cloudy"><i class="wi wi-night-cloudy" title="wi wi-night-cloudy" alt="wi wi-night-cloudy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-cloudy-gusts"><i class="wi wi-night-cloudy-gusts" title="wi wi-night-cloudy-gusts" alt="wi wi-night-cloudy-gusts"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-cloudy-windy"><i class="wi wi-night-cloudy-windy" title="wi wi-night-cloudy-windy" alt="wi wi-night-cloudy-windy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-fog"><i class="wi wi-night-fog" title="wi wi-night-fog" alt="wi wi-night-fog"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-hail"><i class="wi wi-night-hail" title="wi wi-night-hail" alt="wi wi-night-hail"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-lightning"><i class="wi wi-night-lightning" title="wi wi-night-lightning" alt="wi wi-night-lightning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-partly-cloudy"><i class="wi wi-night-partly-cloudy" title="wi wi-night-partly-cloudy" alt="wi wi-night-partly-cloudy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-rain"><i class="wi wi-night-rain" title="wi wi-night-rain" alt="wi wi-night-rain"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-rain-mix"><i class="wi wi-night-rain-mix" title="wi wi-night-rain-mix" alt="wi wi-night-rain-mix"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-rain-wind"><i class="wi wi-night-rain-wind" title="wi wi-night-rain-wind" alt="wi wi-night-rain-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-showers"><i class="wi wi-night-showers" title="wi wi-night-showers" alt="wi wi-night-showers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-sleet"><i class="wi wi-night-sleet" title="wi wi-night-sleet" alt="wi wi-night-sleet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-sleet-storm"><i class="wi wi-night-sleet-storm" title="wi wi-night-sleet-storm" alt="wi wi-night-sleet-storm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-snow"><i class="wi wi-night-snow" title="wi wi-night-snow" alt="wi wi-night-snow"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-snow-thunderstorm"><i class="wi wi-night-snow-thunderstorm" title="wi wi-night-snow-thunderstorm" alt="wi wi-night-snow-thunderstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-snow-wind"><i class="wi wi-night-snow-wind" title="wi wi-night-snow-wind" alt="wi wi-night-snow-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-sprinkle"><i class="wi wi-night-sprinkle" title="wi wi-night-sprinkle" alt="wi wi-night-sprinkle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-storm-showers"><i class="wi wi-night-storm-showers" title="wi wi-night-storm-showers" alt="wi wi-night-storm-showers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-thunderstorm"><i class="wi wi-night-thunderstorm" title="wi wi-night-thunderstorm" alt="wi wi-night-thunderstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-lunar-eclipse"><i class="wi wi-lunar-eclipse" title="wi wi-lunar-eclipse" alt="wi wi-lunar-eclipse"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-stars"><i class="wi wi-stars" title="wi wi-stars" alt="wi wi-stars"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-storm-showers"><i class="wi wi-storm-showers" title="wi wi-storm-showers" alt="wi wi-storm-showers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-thunderstorm"><i class="wi wi-thunderstorm" title="wi wi-thunderstorm" alt="wi wi-thunderstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-cloudy-high"><i class="wi wi-night-alt-cloudy-high" title="wi wi-night-alt-cloudy-high" alt="wi wi-night-alt-cloudy-high"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-cloudy-high"><i class="wi wi-night-cloudy-high" title="wi wi-night-cloudy-high" alt="wi wi-night-cloudy-high"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-night-alt-partly-cloudy"><i class="wi wi-night-alt-partly-cloudy" title="wi wi-night-alt-partly-cloudy" alt="wi wi-night-alt-partly-cloudy"></i></a></div>

										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Neutral Icons</b></h4>
                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="wi wi-cloud"><i class="wi wi-cloud" title="wi wi-cloud" alt="wi wi-cloud"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-cloudy"><i class="wi wi-cloudy" title="wi wi-cloudy" alt="wi wi-cloudy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-cloudy-gusts"><i class="wi wi-cloudy-gusts" title="wi wi-cloudy-gusts" alt="wi wi-cloudy-gusts"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-cloudy-windy"><i class="wi wi-cloudy-windy" title="wi wi-cloudy-windy" alt="wi wi-cloudy-windy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-fog"><i class="wi wi-fog" title="wi wi-fog" alt="wi wi-fog"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-hail"><i class="wi wi-hail" title="wi wi-hail" alt="wi wi-hail"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-rain"><i class="wi wi-rain" title="wi wi-rain" alt="wi wi-rain"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-rain-mix"><i class="wi wi-rain-mix" title="wi wi-rain-mix" alt="wi wi-rain-mix"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-rain-wind"><i class="wi wi-rain-wind" title="wi wi-rain-wind" alt="wi wi-rain-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-showers"><i class="wi wi-showers" title="wi wi-showers" alt="wi wi-showers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-sleet"><i class="wi wi-sleet" title="wi wi-sleet" alt="wi wi-sleet"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-snow"><i class="wi wi-snow" title="wi wi-snow" alt="wi wi-snow"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-sprinkle"><i class="wi wi-sprinkle" title="wi wi-sprinkle" alt="wi wi-sprinkle"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-storm-showers"><i class="wi wi-storm-showers" title="wi wi-storm-showers" alt="wi wi-storm-showers"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-thunderstorm"><i class="wi wi-thunderstorm" title="wi wi-thunderstorm" alt="wi wi-thunderstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-snow-wind"><i class="wi wi-snow-wind" title="wi wi-snow-wind" alt="wi wi-snow-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-snow"><i class="wi wi-snow" title="wi wi-snow" alt="wi wi-snow"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-smog"><i class="wi wi-smog" title="wi wi-smog" alt="wi wi-smog"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-smoke"><i class="wi wi-smoke" title="wi wi-smoke" alt="wi wi-smoke"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-lightning"><i class="wi wi-lightning" title="wi wi-lightning" alt="wi wi-lightning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-raindrops"><i class="wi wi-raindrops" title="wi wi-raindrops" alt="wi wi-raindrops"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-raindrop"><i class="wi wi-raindrop" title="wi wi-raindrop" alt="wi wi-raindrop"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-dust"><i class="wi wi-dust" title="wi wi-dust" alt="wi wi-dust"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-snowflake-cold"><i class="wi wi-snowflake-cold" title="wi wi-snowflake-cold" alt="wi wi-snowflake-cold"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-windy"><i class="wi wi-windy" title="wi wi-windy" alt="wi wi-windy"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-strong-wind"><i class="wi wi-strong-wind" title="wi wi-strong-wind" alt="wi wi-strong-wind"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-sandstorm"><i class="wi wi-sandstorm" title="wi wi-sandstorm" alt="wi wi-sandstorm"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-earthquake"><i class="wi wi-earthquake" title="wi wi-earthquake" alt="wi wi-earthquake"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-fire"><i class="wi wi-fire" title="wi wi-fire" alt="wi wi-fire"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-flood"><i class="wi wi-flood" title="wi wi-flood" alt="wi wi-flood"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-meteor"><i class="wi wi-meteor" title="wi wi-meteor" alt="wi wi-meteor"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-tsunami"><i class="wi wi-tsunami" title="wi wi-tsunami" alt="wi wi-tsunami"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-volcano"><i class="wi wi-volcano" title="wi wi-volcano" alt="wi wi-volcano"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-hurricane"><i class="wi wi-hurricane" title="wi wi-hurricane" alt="wi wi-hurricane"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-tornado"><i class="wi wi-tornado" title="wi wi-tornado" alt="wi wi-tornado"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-small-craft-advisory"><i class="wi wi-small-craft-advisory" title="wi wi-small-craft-advisory" alt="wi wi-small-craft-advisory"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-gale-warning"><i class="wi wi-gale-warning" title="wi wi-gale-warning" alt="wi wi-gale-warning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-storm-warning"><i class="wi wi-storm-warning" title="wi wi-storm-warning" alt="wi wi-storm-warning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-hurricane-warning"><i class="wi wi-hurricane-warning" title="wi wi-hurricane-warning" alt="wi wi-hurricane-warning"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-wind-direction"><i class="wi wi-wind-direction" title="wi wi-wind-direction" alt="wi wi-wind-direction"></i></a></div>


										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Miscellaneous Icons</b></h4>
                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="wi wi-alien"><i class="wi wi-alien" title="wi wi-alien" alt="wi wi-alien"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-celsius"><i class="wi wi-celsius" title="wi wi-celsius" alt="wi wi-celsius"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-fahrenheit"><i class="wi wi-fahrenheit" title="wi wi-fahrenheit" alt="wi wi-fahrenheit"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-degrees"><i class="wi wi-degrees" title="wi wi-degrees" alt="wi wi-degrees"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-thermometer"><i class="wi wi-thermometer" title="wi wi-thermometer" alt="wi wi-thermometer"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-thermometer-exterior"><i class="wi wi-thermometer-exterior" title="wi wi-thermometer-exterior" alt="wi wi-thermometer-exterior"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-thermometer-internal"><i class="wi wi-thermometer-internal" title="wi wi-thermometer-internal" alt="wi wi-thermometer-internal"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-cloud-down"><i class="wi wi-cloud-down" title="wi wi-cloud-down" alt="wi wi-cloud-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-cloud-up"><i class="wi wi-cloud-up" title="wi wi-cloud-up" alt="wi wi-cloud-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-cloud-refresh"><i class="wi wi-cloud-refresh" title="wi wi-cloud-refresh" alt="wi wi-cloud-refresh"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-horizon"><i class="wi wi-horizon" title="wi wi-horizon" alt="wi wi-horizon"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-horizon-alt"><i class="wi wi-horizon-alt" title="wi wi-horizon-alt" alt="wi wi-horizon-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-sunrise"><i class="wi wi-sunrise" title="wi wi-sunrise" alt="wi wi-sunrise"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-sunset"><i class="wi wi-sunset" title="wi wi-sunset" alt="wi wi-sunset"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moonrise"><i class="wi wi-moonrise" title="wi wi-moonrise" alt="wi wi-moonrise"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moonset"><i class="wi wi-moonset" title="wi wi-moonset" alt="wi wi-moonset"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-refresh"><i class="wi wi-refresh" title="wi wi-refresh" alt="wi wi-refresh"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-refresh-alt"><i class="wi wi-refresh-alt" title="wi wi-refresh-alt" alt="wi wi-refresh-alt"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-umbrella"><i class="wi wi-umbrella" title="wi wi-umbrella" alt="wi wi-umbrella"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-barometer"><i class="wi wi-barometer" title="wi wi-barometer" alt="wi wi-barometer"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-humidity"><i class="wi wi-humidity" title="wi wi-humidity" alt="wi wi-humidity"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-na"><i class="wi wi-na" title="wi wi-na" alt="wi wi-na"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-train"><i class="wi wi-train" title="wi wi-train" alt="wi wi-train"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi "><i class="wi " title="wi " alt="wi "></i></a></div>


										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Moon Phases Icons</b></h4>
										<div class="alert alert-success">
	                                        The moons are split into 28 icons, to correspond neatly with the 28 day moon cycle. There is a primary
	                                        set and alternate set. The primary set is meant to be interpreted as: where there are pixels, that is the
	                                         illuminated part of the moon. The alternate set is meant to be interpreted as: where there are pixels,
	                                         that is the shadowed part of the moon.
	                                    </div>
                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="wi wi-moon-new"><i class="wi wi-moon-new" title="wi wi-moon-new" alt="wi wi-moon-new"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-cresent-1"><i class="wi wi-moon-waxing-cresent-1" title="wi wi-moon-waxing-cresent-1" alt="wi wi-moon-waxing-cresent-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-cresent-2"><i class="wi wi-moon-waxing-cresent-2" title="wi wi-moon-waxing-cresent-2" alt="wi wi-moon-waxing-cresent-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-cresent-3"><i class="wi wi-moon-waxing-cresent-3" title="wi wi-moon-waxing-cresent-3" alt="wi wi-moon-waxing-cresent-3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-cresent-4"><i class="wi wi-moon-waxing-cresent-4" title="wi wi-moon-waxing-cresent-4" alt="wi wi-moon-waxing-cresent-4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-cresent-5"><i class="wi wi-moon-waxing-cresent-5" title="wi wi-moon-waxing-cresent-5" alt="wi wi-moon-waxing-cresent-5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-cresent-6"><i class="wi wi-moon-waxing-cresent-6" title="wi wi-moon-waxing-cresent-6" alt="wi wi-moon-waxing-cresent-6"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-first-quarter"><i class="wi wi-moon-first-quarter" title="wi wi-moon-first-quarter" alt="wi wi-moon-first-quarter"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-gibbous-1"><i class="wi wi-moon-waxing-gibbous-1" title="wi wi-moon-waxing-gibbous-1" alt="wi wi-moon-waxing-gibbous-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-gibbous-2"><i class="wi wi-moon-waxing-gibbous-2" title="wi wi-moon-waxing-gibbous-2" alt="wi wi-moon-waxing-gibbous-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-gibbous-3"><i class="wi wi-moon-waxing-gibbous-3" title="wi wi-moon-waxing-gibbous-3" alt="wi wi-moon-waxing-gibbous-3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-gibbous-4"><i class="wi wi-moon-waxing-gibbous-4" title="wi wi-moon-waxing-gibbous-4" alt="wi wi-moon-waxing-gibbous-4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-gibbous-5"><i class="wi wi-moon-waxing-gibbous-5" title="wi wi-moon-waxing-gibbous-5" alt="wi wi-moon-waxing-gibbous-5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waxing-gibbous-6"><i class="wi wi-moon-waxing-gibbous-6" title="wi wi-moon-waxing-gibbous-6" alt="wi wi-moon-waxing-gibbous-6"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-full"><i class="wi wi-moon-full" title="wi wi-moon-full" alt="wi wi-moon-full"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-gibbous-1"><i class="wi wi-moon-waning-gibbous-1" title="wi wi-moon-waning-gibbous-1" alt="wi wi-moon-waning-gibbous-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-gibbous-2"><i class="wi wi-moon-waning-gibbous-2" title="wi wi-moon-waning-gibbous-2" alt="wi wi-moon-waning-gibbous-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-gibbous-3"><i class="wi wi-moon-waning-gibbous-3" title="wi wi-moon-waning-gibbous-3" alt="wi wi-moon-waning-gibbous-3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-gibbous-4"><i class="wi wi-moon-waning-gibbous-4" title="wi wi-moon-waning-gibbous-4" alt="wi wi-moon-waning-gibbous-4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-gibbous-5"><i class="wi wi-moon-waning-gibbous-5" title="wi wi-moon-waning-gibbous-5" alt="wi wi-moon-waning-gibbous-5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-gibbous-6"><i class="wi wi-moon-waning-gibbous-6" title="wi wi-moon-waning-gibbous-6" alt="wi wi-moon-waning-gibbous-6"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-third-quarter"><i class="wi wi-moon-third-quarter" title="wi wi-moon-third-quarter" alt="wi wi-moon-third-quarter"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-crescent-1"><i class="wi wi-moon-waning-crescent-1" title="wi wi-moon-waning-crescent-1" alt="wi wi-moon-waning-crescent-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-crescent-2"><i class="wi wi-moon-waning-crescent-2" title="wi wi-moon-waning-crescent-2" alt="wi wi-moon-waning-crescent-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-crescent-3"><i class="wi wi-moon-waning-crescent-3" title="wi wi-moon-waning-crescent-3" alt="wi wi-moon-waning-crescent-3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-crescent-4"><i class="wi wi-moon-waning-crescent-4" title="wi wi-moon-waning-crescent-4" alt="wi wi-moon-waning-crescent-4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-crescent-5"><i class="wi wi-moon-waning-crescent-5" title="wi wi-moon-waning-crescent-5" alt="wi wi-moon-waning-crescent-5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-waning-crescent-6"><i class="wi wi-moon-waning-crescent-6" title="wi wi-moon-waning-crescent-6" alt="wi wi-moon-waning-crescent-6"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-new"><i class="wi wi-moon-alt-new" title="wi wi-moon-alt-new" alt="wi wi-moon-alt-new"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-cresent-1"><i class="wi wi-moon-alt-waxing-cresent-1" title="wi wi-moon-alt-waxing-cresent-1" alt="wi wi-moon-alt-waxing-cresent-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-cresent-2"><i class="wi wi-moon-alt-waxing-cresent-2" title="wi wi-moon-alt-waxing-cresent-2" alt="wi wi-moon-alt-waxing-cresent-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-cresent-3"><i class="wi wi-moon-alt-waxing-cresent-3" title="wi wi-moon-alt-waxing-cresent-3" alt="wi wi-moon-alt-waxing-cresent-3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-cresent-4"><i class="wi wi-moon-alt-waxing-cresent-4" title="wi wi-moon-alt-waxing-cresent-4" alt="wi wi-moon-alt-waxing-cresent-4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-cresent-5"><i class="wi wi-moon-alt-waxing-cresent-5" title="wi wi-moon-alt-waxing-cresent-5" alt="wi wi-moon-alt-waxing-cresent-5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-cresent-6"><i class="wi wi-moon-alt-waxing-cresent-6" title="wi wi-moon-alt-waxing-cresent-6" alt="wi wi-moon-alt-waxing-cresent-6"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-first-quarter"><i class="wi wi-moon-alt-first-quarter" title="wi wi-moon-alt-first-quarter" alt="wi wi-moon-alt-first-quarter"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-gibbous-1"><i class="wi wi-moon-alt-waxing-gibbous-1" title="wi wi-moon-alt-waxing-gibbous-1" alt="wi wi-moon-alt-waxing-gibbous-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-gibbous-2"><i class="wi wi-moon-alt-waxing-gibbous-2" title="wi wi-moon-alt-waxing-gibbous-2" alt="wi wi-moon-alt-waxing-gibbous-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-gibbous-3"><i class="wi wi-moon-alt-waxing-gibbous-3" title="wi wi-moon-alt-waxing-gibbous-3" alt="wi wi-moon-alt-waxing-gibbous-3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-gibbous-4"><i class="wi wi-moon-alt-waxing-gibbous-4" title="wi wi-moon-alt-waxing-gibbous-4" alt="wi wi-moon-alt-waxing-gibbous-4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-gibbous-5"><i class="wi wi-moon-alt-waxing-gibbous-5" title="wi wi-moon-alt-waxing-gibbous-5" alt="wi wi-moon-alt-waxing-gibbous-5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waxing-gibbous-6"><i class="wi wi-moon-alt-waxing-gibbous-6" title="wi wi-moon-alt-waxing-gibbous-6" alt="wi wi-moon-alt-waxing-gibbous-6"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-full"><i class="wi wi-moon-alt-full" title="wi wi-moon-alt-full" alt="wi wi-moon-alt-full"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-gibbous-1"><i class="wi wi-moon-alt-waning-gibbous-1" title="wi wi-moon-alt-waning-gibbous-1" alt="wi wi-moon-alt-waning-gibbous-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-gibbous-2"><i class="wi wi-moon-alt-waning-gibbous-2" title="wi wi-moon-alt-waning-gibbous-2" alt="wi wi-moon-alt-waning-gibbous-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-gibbous-3"><i class="wi wi-moon-alt-waning-gibbous-3" title="wi wi-moon-alt-waning-gibbous-3" alt="wi wi-moon-alt-waning-gibbous-3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-gibbous-4"><i class="wi wi-moon-alt-waning-gibbous-4" title="wi wi-moon-alt-waning-gibbous-4" alt="wi wi-moon-alt-waning-gibbous-4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-gibbous-5"><i class="wi wi-moon-alt-waning-gibbous-5" title="wi wi-moon-alt-waning-gibbous-5" alt="wi wi-moon-alt-waning-gibbous-5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-gibbous-6"><i class="wi wi-moon-alt-waning-gibbous-6" title="wi wi-moon-alt-waning-gibbous-6" alt="wi wi-moon-alt-waning-gibbous-6"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-third-quarter"><i class="wi wi-moon-alt-third-quarter" title="wi wi-moon-alt-third-quarter" alt="wi wi-moon-alt-third-quarter"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-crescent-1"><i class="wi wi-moon-alt-waning-crescent-1" title="wi wi-moon-alt-waning-crescent-1" alt="wi wi-moon-alt-waning-crescent-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-crescent-2"><i class="wi wi-moon-alt-waning-crescent-2" title="wi wi-moon-alt-waning-crescent-2" alt="wi wi-moon-alt-waning-crescent-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-crescent-3"><i class="wi wi-moon-alt-waning-crescent-3" title="wi wi-moon-alt-waning-crescent-3" alt="wi wi-moon-alt-waning-crescent-3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-crescent-4"><i class="wi wi-moon-alt-waning-crescent-4" title="wi wi-moon-alt-waning-crescent-4" alt="wi wi-moon-alt-waning-crescent-4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-crescent-5"><i class="wi wi-moon-alt-waning-crescent-5" title="wi wi-moon-alt-waning-crescent-5" alt="wi wi-moon-alt-waning-crescent-5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-moon-alt-waning-crescent-6"><i class="wi wi-moon-alt-waning-crescent-6" title="wi wi-moon-alt-waning-crescent-6" alt="wi wi-moon-alt-waning-crescent-6"></i></a></div>


										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Time Icons</b></h4>

                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="wi wi-time-1"><i class="wi wi-time-1" title="wi wi-time-1" alt="wi wi-time-1"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-2"><i class="wi wi-time-2" title="wi wi-time-2" alt="wi wi-time-2"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-3"><i class="wi wi-time-3" title="wi wi-time-3" alt="wi wi-time-3"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-4"><i class="wi wi-time-4" title="wi wi-time-4" alt="wi wi-time-4"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-5"><i class="wi wi-time-5" title="wi wi-time-5" alt="wi wi-time-5"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-6"><i class="wi wi-time-6" title="wi wi-time-6" alt="wi wi-time-6"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-7"><i class="wi wi-time-7" title="wi wi-time-7" alt="wi wi-time-7"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-8"><i class="wi wi-time-8" title="wi wi-time-8" alt="wi wi-time-8"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-9"><i class="wi wi-time-9" title="wi wi-time-9" alt="wi wi-time-9"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-10"><i class="wi wi-time-10" title="wi wi-time-10" alt="wi wi-time-10"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-11"><i class="wi wi-time-11" title="wi wi-time-11" alt="wi wi-time-11"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-time-12"><i class="wi wi-time-12" title="wi wi-time-12" alt="wi wi-time-12"></i></a></div>

										</div>
										<!-- End row -->

										<h4 class="page-header header-title"><b>Directional Arrows</b></h4>

                                        <div class="row">

											<div class="icon">
												<a href="#" data-icon="wi wi-direction-up"><i class="wi wi-direction-up" title="wi wi-direction-up" alt="wi wi-direction-up"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-direction-up-right"><i class="wi wi-direction-up-right" title="wi wi-direction-up-right" alt="wi wi-direction-up-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-direction-right"><i class="wi wi-direction-right" title="wi wi-direction-right" alt="wi wi-direction-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-direction-down-right"><i class="wi wi-direction-down-right" title="wi wi-direction-down-right" alt="wi wi-direction-down-right"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-direction-down"><i class="wi wi-direction-down" title="wi wi-direction-down" alt="wi wi-direction-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-direction-down-left"><i class="wi wi-direction-down-left" title="wi wi-direction-down-left" alt="wi wi-direction-down-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-direction-left"><i class="wi wi-direction-left" title="wi wi-direction-left" alt="wi wi-direction-left"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="wi wi-direction-up-left"><i class="wi wi-direction-up-left" title="wi wi-direction-up-left" alt="wi wi-direction-up-left"></i></a></div>


										</div>
										<!-- End row -->

										<!-- End row -->

                                    </div>
					  </div>
					</div>
				  </div>
				  <div class="hidden panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#icons" href="#typicons">
						Typicons</a>
					  </h4>
					</div>
					<div id="typicons" class="panel-collapse collapse">
					  <div class="panel-body">
								<div class="card-box">

                                        <div class="row">
											<div class="icon">
												<a href="#" data-icon="typcn typcn-chart-pie-outline"><i class="typcn typcn-chart-pie-outline" title="typcn typcn-chart-pie-outline" alt="typcn typcn-chart-pie-outline"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-chart-pie"><i class="typcn typcn-chart-pie" title="typcn typcn-chart-pie" alt="typcn typcn-chart-pie"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-chevron-left-outline"><i class="typcn typcn-chevron-left-outline" title="typcn typcn-chevron-left-outline" alt="typcn typcn-chevron-left-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-chevron-left"><i class="typcn typcn-chevron-left" title="typcn typcn-chevron-left" alt="typcn typcn-chevron-left"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-chevron-right-outline"><i class="typcn typcn-chevron-right-outline" title="typcn typcn-chevron-right-outline" alt="typcn typcn-chevron-right-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-chevron-right"><i class="typcn typcn-chevron-right" title="typcn typcn-chevron-right" alt="typcn typcn-chevron-right"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-clipboard"><i class="typcn typcn-clipboard" title="typcn typcn-clipboard" alt="typcn typcn-clipboard"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-cloud-storage"><i class="typcn typcn-cloud-storage" title="typcn typcn-cloud-storage" alt="typcn typcn-cloud-storage"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-cloud-storage-outline"><i class="typcn typcn-cloud-storage-outline" title="typcn typcn-cloud-storage-outline" alt="typcn typcn-cloud-storage-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-code-outline"><i class="typcn typcn-code-outline" title="typcn typcn-code-outline" alt="typcn typcn-code-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-code"><i class="typcn typcn-code" title="typcn typcn-code" alt="typcn typcn-code"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-coffee"><i class="typcn typcn-coffee" title="typcn typcn-coffee" alt="typcn typcn-coffee"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-cog-outline"><i class="typcn typcn-cog-outline" title="typcn typcn-cog-outline" alt="typcn typcn-cog-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-cog"><i class="typcn typcn-cog" title="typcn typcn-cog" alt="typcn typcn-cog"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-compass"><i class="typcn typcn-compass" title="typcn typcn-compass" alt="typcn typcn-compass"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-contacts"><i class="typcn typcn-contacts" title="typcn typcn-contacts" alt="typcn typcn-contacts"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-credit-card"><i class="typcn typcn-credit-card" title="typcn typcn-credit-card" alt="typcn typcn-credit-card"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-css3"><i class="typcn typcn-css3" title="typcn typcn-css3" alt="typcn typcn-css3"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-database"><i class="typcn typcn-database" title="typcn typcn-database" alt="typcn typcn-database"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-delete-outline"><i class="typcn typcn-delete-outline" title="typcn typcn-delete-outline" alt="typcn typcn-delete-outline"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-delete"><i class="typcn typcn-delete" title="typcn typcn-delete" alt="typcn typcn-delete"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-device-desktop"><i class="typcn typcn-device-desktop" title="typcn typcn-device-desktop" alt="typcn typcn-device-desktop"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-device-laptop"><i class="typcn typcn-device-laptop" title="typcn typcn-device-laptop" alt="typcn typcn-device-laptop"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-device-phone"><i class="typcn typcn-device-phone" title="typcn typcn-device-phone" alt="typcn typcn-device-phone"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-device-tablet"><i class="typcn typcn-device-tablet" title="typcn typcn-device-tablet" alt="typcn typcn-device-tablet"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-directions"><i class="typcn typcn-directions" title="typcn typcn-directions" alt="typcn typcn-directions"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-divide-outline"><i class="typcn typcn-divide-outline" title="typcn typcn-divide-outline" alt="typcn typcn-divide-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-divide"><i class="typcn typcn-divide" title="typcn typcn-divide" alt="typcn typcn-divide"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-document-add"><i class="typcn typcn-document-add" title="typcn typcn-document-add" alt="typcn typcn-document-add"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-document-delete"><i class="typcn typcn-document-delete" title="typcn typcn-document-delete" alt="typcn typcn-document-delete"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-document-text"><i class="typcn typcn-document-text" title="typcn typcn-document-text" alt="typcn typcn-document-text"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-document"><i class="typcn typcn-document" title="typcn typcn-document" alt="typcn typcn-document"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-download-outline"><i class="typcn typcn-download-outline" title="typcn typcn-download-outline" alt="typcn typcn-download-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-download"><i class="typcn typcn-download" title="typcn typcn-download" alt="typcn typcn-download"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-dropbox"><i class="typcn typcn-dropbox" title="typcn typcn-dropbox" alt="typcn typcn-dropbox"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-edit"><i class="typcn typcn-edit" title="typcn typcn-edit" alt="typcn typcn-edit"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-eject-outline"><i class="typcn typcn-eject-outline" title="typcn typcn-eject-outline" alt="typcn typcn-eject-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-eject"><i class="typcn typcn-eject" title="typcn typcn-eject" alt="typcn typcn-eject"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-equals-outline"><i class="typcn typcn-equals-outline" title="typcn typcn-equals-outline" alt="typcn typcn-equals-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-equals"><i class="typcn typcn-equals" title="typcn typcn-equals" alt="typcn typcn-equals"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-export-outline"><i class="typcn typcn-export-outline" title="typcn typcn-export-outline" alt="typcn typcn-export-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-export"><i class="typcn typcn-export" title="typcn typcn-export" alt="typcn typcn-export"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-eye-outline"><i class="typcn typcn-eye-outline" title="typcn typcn-eye-outline" alt="typcn typcn-eye-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-eye"><i class="typcn typcn-eye" title="typcn typcn-eye" alt="typcn typcn-eye"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-feather"><i class="typcn typcn-feather" title="typcn typcn-feather" alt="typcn typcn-feather"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-film"><i class="typcn typcn-film" title="typcn typcn-film" alt="typcn typcn-film"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-filter"><i class="typcn typcn-filter" title="typcn typcn-filter" alt="typcn typcn-filter"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-flag-outline"><i class="typcn typcn-flag-outline" title="typcn typcn-flag-outline" alt="typcn typcn-flag-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-flag"><i class="typcn typcn-flag" title="typcn typcn-flag" alt="typcn typcn-flag"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-flash-outline"><i class="typcn typcn-flash-outline" title="typcn typcn-flash-outline" alt="typcn typcn-flash-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-flash"><i class="typcn typcn-flash" title="typcn typcn-flash" alt="typcn typcn-flash"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-flow-children"><i class="typcn typcn-flow-children" title="typcn typcn-flow-children" alt="typcn typcn-flow-children"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-flow-merge"><i class="typcn typcn-flow-merge" title="typcn typcn-flow-merge" alt="typcn typcn-flow-merge"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-flow-parallel"><i class="typcn typcn-flow-parallel" title="typcn typcn-flow-parallel" alt="typcn typcn-flow-parallel"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-flow-switch"><i class="typcn typcn-flow-switch" title="typcn typcn-flow-switch" alt="typcn typcn-flow-switch"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-folder-add"><i class="typcn typcn-folder-add" title="typcn typcn-folder-add" alt="typcn typcn-folder-add"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-folder-delete"><i class="typcn typcn-folder-delete" title="typcn typcn-folder-delete" alt="typcn typcn-folder-delete"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-folder-open"><i class="typcn typcn-folder-open" title="typcn typcn-folder-open" alt="typcn typcn-folder-open"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-folder"><i class="typcn typcn-folder" title="typcn typcn-folder" alt="typcn typcn-folder"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-gift"><i class="typcn typcn-gift" title="typcn typcn-gift" alt="typcn typcn-gift"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-globe-outline"><i class="typcn typcn-globe-outline" title="typcn typcn-globe-outline" alt="typcn typcn-globe-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-globe"><i class="typcn typcn-globe" title="typcn typcn-globe" alt="typcn typcn-globe"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-group-outline"><i class="typcn typcn-group-outline" title="typcn typcn-group-outline" alt="typcn typcn-group-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-group"><i class="typcn typcn-group" title="typcn typcn-group" alt="typcn typcn-group"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-headphones"><i class="typcn typcn-headphones" title="typcn typcn-headphones" alt="typcn typcn-headphones"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-heart-full-outline"><i class="typcn typcn-heart-full-outline" title="typcn typcn-heart-full-outline" alt="typcn typcn-heart-full-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-heart-half-outline"><i class="typcn typcn-heart-half-outline" title="typcn typcn-heart-half-outline" alt="typcn typcn-heart-half-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-heart-outline"><i class="typcn typcn-heart-outline" title="typcn typcn-heart-outline" alt="typcn typcn-heart-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-heart"><i class="typcn typcn-heart" title="typcn typcn-heart" alt="typcn typcn-heart"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-home-outline"><i class="typcn typcn-home-outline" title="typcn typcn-home-outline" alt="typcn typcn-home-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-home"><i class="typcn typcn-home" title="typcn typcn-home" alt="typcn typcn-home"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-html5"><i class="typcn typcn-html5" title="typcn typcn-html5" alt="typcn typcn-html5"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-image-outline"><i class="typcn typcn-image-outline" title="typcn typcn-image-outline" alt="typcn typcn-image-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-image"><i class="typcn typcn-image" title="typcn typcn-image" alt="typcn typcn-image"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-infinity-outline"><i class="typcn typcn-infinity-outline" title="typcn typcn-infinity-outline" alt="typcn typcn-infinity-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-infinity"><i class="typcn typcn-infinity" title="typcn typcn-infinity" alt="typcn typcn-infinity"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-info-large-outline"><i class="typcn typcn-info-large-outline" title="typcn typcn-info-large-outline" alt="typcn typcn-info-large-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-info-large"><i class="typcn typcn-info-large" title="typcn typcn-info-large" alt="typcn typcn-info-large"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-info-outline"><i class="typcn typcn-info-outline" title="typcn typcn-info-outline" alt="typcn typcn-info-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-info"><i class="typcn typcn-info" title="typcn typcn-info" alt="typcn typcn-info"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-input-checked-outline"><i class="typcn typcn-input-checked-outline" title="typcn typcn-input-checked-outline" alt="typcn typcn-input-checked-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-input-checked"><i class="typcn typcn-input-checked" title="typcn typcn-input-checked" alt="typcn typcn-input-checked"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-key-outline"><i class="typcn typcn-key-outline" title="typcn typcn-key-outline" alt="typcn typcn-key-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-key"><i class="typcn typcn-key" title="typcn typcn-key" alt="typcn typcn-key"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-keyboard"><i class="typcn typcn-keyboard" title="typcn typcn-keyboard" alt="typcn typcn-keyboard"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-leaf"><i class="typcn typcn-leaf" title="typcn typcn-leaf" alt="typcn typcn-leaf"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-lightbulb"><i class="typcn typcn-lightbulb" title="typcn typcn-lightbulb" alt="typcn typcn-lightbulb"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-link-outline"><i class="typcn typcn-link-outline" title="typcn typcn-link-outline" alt="typcn typcn-link-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-link"><i class="typcn typcn-link" title="typcn typcn-link" alt="typcn typcn-link"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-location-arrow-outline"><i class="typcn typcn-location-arrow-outline" title="typcn typcn-location-arrow-outline" alt="typcn typcn-location-arrow-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-location-arrow"><i class="typcn typcn-location-arrow" title="typcn typcn-location-arrow" alt="typcn typcn-location-arrow"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-location-outline"><i class="typcn typcn-location-outline" title="typcn typcn-location-outline" alt="typcn typcn-location-outline"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-location"><i class="typcn typcn-location" title="typcn typcn-location" alt="typcn typcn-location"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-lock-closed-outline"><i class="typcn typcn-lock-closed-outline" title="typcn typcn-lock-closed-outline" alt="typcn typcn-lock-closed-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-lock-closed"><i class="typcn typcn-lock-closed" title="typcn typcn-lock-closed" alt="typcn typcn-lock-closed"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-lock-open-outline"><i class="typcn typcn-lock-open-outline" title="typcn typcn-lock-open-outline" alt="typcn typcn-lock-open-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-lock-open"><i class="typcn typcn-lock-open" title="typcn typcn-lock-open" alt="typcn typcn-lock-open"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-mail"><i class="typcn typcn-mail" title="typcn typcn-mail" alt="typcn typcn-mail"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-map"><i class="typcn typcn-map" title="typcn typcn-map" alt="typcn typcn-map"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-eject-outline"><i class="typcn typcn-media-eject-outline" title="typcn typcn-media-eject-outline" alt="typcn typcn-media-eject-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-eject"><i class="typcn typcn-media-eject" title="typcn typcn-media-eject" alt="typcn typcn-media-eject"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-fast-forward-outline"><i class="typcn typcn-media-fast-forward-outline" title="typcn typcn-media-fast-forward-outline" alt="typcn typcn-media-fast-forward-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-fast-forward"><i class="typcn typcn-media-fast-forward" title="typcn typcn-media-fast-forward" alt="typcn typcn-media-fast-forward"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-pause-outline"><i class="typcn typcn-media-pause-outline" title="typcn typcn-media-pause-outline" alt="typcn typcn-media-pause-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-pause"><i class="typcn typcn-media-pause" title="typcn typcn-media-pause" alt="typcn typcn-media-pause"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-play-outline"><i class="typcn typcn-media-play-outline" title="typcn typcn-media-play-outline" alt="typcn typcn-media-play-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-play-reverse-outline"><i class="typcn typcn-media-play-reverse-outline" title=" typcn typcn-media-play-reverse-outline" alt="typcn typcn-media-play-reverse-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-play-reverse"><i class="typcn typcn-media-play-reverse" title="typcn typcn-media-play-reverse" alt="typcn typcn-media-play-reverse"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-play"><i class="typcn typcn-media-play" title="typcn typcn-media-play" alt="typcn typcn-media-play"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-record-outline"><i class="typcn typcn-media-record-outline" title="typcn typcn-media-record-outline" alt="typcn typcn-media-record-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-record"><i class="typcn typcn-media-record" title="typcn typcn-media-record" alt="typcn typcn-media-record"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-rewind-outline"><i class="typcn typcn-media-rewind-outline" title="typcn typcn-media-rewind-outline" alt="typcn typcn-media-rewind-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-rewind"><i class="typcn typcn-media-rewind" title="typcn typcn-media-rewind" alt="typcn typcn-media-rewind"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-stop-outline"><i class="typcn typcn-media-stop-outline" title="typcn typcn-media-stop-outline" alt="typcn typcn-media-stop-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-media-stop"><i class="typcn typcn-media-stop" title="typcn typcn-media-stop" alt="typcn typcn-media-stop"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-message-typing"><i class="typcn typcn-message-typing" title="typcn typcn-message-typing" alt="typcn typcn-message-typing"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-message"><i class="typcn typcn-message" title="typcn typcn-message" alt="typcn typcn-message"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-messages"><i class="typcn typcn-messages" title="typcn typcn-messages" alt="typcn typcn-messages"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-microphone-outline"><i class="typcn typcn-microphone-outline" title="typcn typcn-microphone-outline" alt="typcn typcn-microphone-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-microphone"><i class="typcn typcn-microphone" title="typcn typcn-microphone" alt="typcn typcn-microphone"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-minus-outline"><i class="typcn typcn-minus-outline" title="typcn typcn-minus-outline" alt="typcn typcn-minus-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-minus"><i class="typcn typcn-minus" title="typcn typcn-minus" alt="typcn typcn-minus"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-mortar-board"><i class="typcn typcn-mortar-board" title="typcn typcn-mortar-board" alt="typcn typcn-mortar-board"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-news"><i class="typcn typcn-news" title="typcn typcn-news" alt="typcn typcn-news"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-notes-outline"><i class="typcn typcn-notes-outline" title="typcn typcn-notes-outline" alt="typcn typcn-notes-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-notes"><i class="typcn typcn-notes" title="typcn typcn-notes" alt="typcn typcn-notes"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-pen"><i class="typcn typcn-pen" title="typcn typcn-pen" alt="typcn typcn-pen"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-pencil"><i class="typcn typcn-pencil" title="typcn typcn-pencil" alt="typcn typcn-pencil"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-phone-outline"><i class="typcn typcn-phone-outline" title="typcn typcn-phone-outline" alt="typcn typcn-phone-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-phone"><i class="typcn typcn-phone" title="typcn typcn-phone" alt="typcn typcn-phone"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-pi-outline"><i class="typcn typcn-pi-outline" title="typcn typcn-pi-outline" alt="typcn typcn-pi-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-pi"><i class="typcn typcn-pi" title="typcn typcn-pi" alt="typcn typcn-pi"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-pin-outline"><i class="typcn typcn-pin-outline" title="typcn typcn-pin-outline" alt="typcn typcn-pin-outline"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-pin"><i class="typcn typcn-pin" title="typcn typcn-pin" alt="typcn typcn-pin"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-pipette"><i class="typcn typcn-pipette" title="typcn typcn-pipette" alt="typcn typcn-pipette"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-plane-outline"><i class="typcn typcn-plane-outline" title="typcn typcn-plane-outline" alt="typcn typcn-plane-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-plane"><i class="typcn typcn-plane" title="typcn typcn-plane" alt="typcn typcn-plane"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-plug"><i class="typcn typcn-plug" title="typcn typcn-plug" alt="typcn typcn-plug"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-plus-outline"><i class="typcn typcn-plus-outline" title="typcn typcn-plus-outline" alt="typcn typcn-plus-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-plus"><i class="typcn typcn-plus" title="typcn typcn-plus" alt="typcn typcn-plus"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-point-of-interest-outline"><i class="typcn typcn-point-of-interest-outline" title="typcn typcn-point-of-interest-outline" alt="typcn typcn-point-of-interest-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-point-of-interest"><i class="typcn typcn-point-of-interest" title="typcn typcn-point-of-interest" alt="typcn typcn-point-of-interest"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-power-outline"><i class="typcn typcn-power-outline" title="typcn typcn-power-outline" alt="typcn typcn-power-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-power"><i class="typcn typcn-power" title="typcn typcn-power" alt="typcn typcn-power"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-printer"><i class="typcn typcn-printer" title="typcn typcn-printer" alt="typcn typcn-printer"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-puzzle-outline"><i class="typcn typcn-puzzle-outline" title="typcn typcn-puzzle-outline" alt="typcn typcn-puzzle-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-puzzle"><i class="typcn typcn-puzzle" title="typcn typcn-puzzle" alt="typcn typcn-puzzle"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-radar-outline"><i class="typcn typcn-radar-outline" title="typcn typcn-radar-outline" alt="typcn typcn-radar-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-radar"><i class="typcn typcn-radar" title="typcn typcn-radar" alt="typcn typcn-radar"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-refresh-outline"><i class="typcn typcn-refresh-outline" title="typcn typcn-refresh-outline" alt="typcn typcn-refresh-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-refresh"><i class="typcn typcn-refresh" title="typcn typcn-refresh" alt="typcn typcn-refresh"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-rss-outline"><i class="typcn typcn-rss-outline" title="typcn typcn-rss-outline" alt="typcn typcn-rss-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-rss"><i class="typcn typcn-rss" title="typcn typcn-rss" alt="typcn typcn-rss"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-scissors-outline"><i class="typcn typcn-scissors-outline" title="typcn typcn-scissors-outline" alt="typcn typcn-scissors-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-scissors"><i class="typcn typcn-scissors" title="typcn typcn-scissors" alt="typcn typcn-scissors"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-shopping-bag"><i class="typcn typcn-shopping-bag" title="typcn typcn-shopping-bag" alt="typcn typcn-shopping-bag"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-shopping-cart"><i class="typcn typcn-shopping-cart" title="typcn typcn-shopping-cart" alt="typcn typcn-shopping-cart"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-at-circular"><i class="typcn typcn-social-at-circular" title="typcn typcn-social-at-circular" alt="typcn typcn-social-at-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-dribbble-circular"><i class="typcn typcn-social-dribbble-circular" title="typcn typcn-social-dribbble-circular" alt="typcn typcn-social-dribbble-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-dribbble"><i class="typcn typcn-social-dribbble" title="typcn typcn-social-dribbble" alt="typcn typcn-social-dribbble"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-facebook-circular"><i class="typcn typcn-social-facebook-circular" title="typcn typcn-social-facebook-circular" alt="typcn typcn-social-facebook-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-facebook"><i class="typcn typcn-social-facebook" title="typcn typcn-social-facebook" alt="typcn typcn-social-facebook"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-flickr-circular"><i class="typcn typcn-social-flickr-circular" title="typcn typcn-social-flickr-circular" alt="typcn typcn-social-flickr-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-flickr"><i class="typcn typcn-social-flickr" title="typcn typcn-social-flickr" alt="typcn typcn-social-flickr"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-github-circular"><i class="typcn typcn-social-github-circular" title="typcn typcn-social-github-circular" alt="typcn typcn-social-github-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-github"><i class="typcn typcn-social-github" title="typcn typcn-social-github" alt="typcn typcn-social-github"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-google-plus-circular"><i class="typcn typcn-social-google-plus-circular" title="typcn typcn-social-google-plus-circular" alt="typcn typcn-social-google-plus-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-google-plus"><i class="typcn typcn-social-google-plus" title="typcn typcn-social-google-plus" alt="typcn typcn-social-google-plus"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-instagram-circular"><i class="typcn typcn-social-instagram-circular" title="typcn typcn-social-instagram-circular" alt="typcn typcn-social-instagram-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-instagram"><i class="typcn typcn-social-instagram" title="typcn typcn-social-instagram" alt="typcn typcn-social-instagram"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-last-fm-circular"><i class="typcn typcn-social-last-fm-circular" title="typcn typcn-social-last-fm-circular" alt="typcn typcn-social-last-fm-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-last-fm"><i class="typcn typcn-social-last-fm" title="typcn typcn-social-last-fm" alt="typcn typcn-social-last-fm"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-linkedin-circular"><i class="typcn typcn-social-linkedin-circular" title="typcn typcn-social-linkedin-circular" alt="typcn typcn-social-linkedin-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-linkedin"><i class="typcn typcn-social-linkedin" title="typcn typcn-social-linkedin" alt="typcn typcn-social-linkedin"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-social-pinterest-circular"><i class="typcn typcn-social-pinterest-circular" title="typcn typcn-social-pinterest-circular" alt="typcn typcn-social-pinterest-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-pinterest"><i class="typcn typcn-social-pinterest" title="typcn typcn-social-pinterest" alt="typcn typcn-social-pinterest"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-skype-outline"><i class="typcn typcn-social-skype-outline" title="typcn typcn-social-skype-outline" alt="typcn typcn-social-skype-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-skype"><i class="typcn typcn-social-skype" title="typcn typcn-social-skype" alt="typcn typcn-social-skype"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-tumbler-circular"><i class="typcn typcn-social-tumbler-circular" title="typcn typcn-social-tumbler-circular" alt="typcn typcn-social-tumbler-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-tumbler"><i class="typcn typcn-social-tumbler" title="typcn typcn-social-tumbler" alt="typcn typcn-social-tumbler"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-twitter-circular"><i class="typcn typcn-social-twitter-circular" title="typcn typcn-social-twitter-circular" alt="typcn typcn-social-twitter-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-twitter"><i class="typcn typcn-social-twitter" title="typcn typcn-social-twitter" alt="typcn typcn-social-twitter"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-vimeo-circular"><i class="typcn typcn-social-vimeo-circular" title="typcn typcn-social-vimeo-circular" alt="typcn typcn-social-vimeo-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-vimeo"><i class="typcn typcn-social-vimeo" title="typcn typcn-social-vimeo" alt="typcn typcn-social-vimeo"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-youtube-circular"><i class="typcn typcn-social-youtube-circular" title="typcn typcn-social-youtube-circular" alt="typcn typcn-social-youtube-circular"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-social-youtube"><i class="typcn typcn-social-youtube" title="typcn typcn-social-youtube" alt="typcn typcn-social-youtube"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-sort-alphabetically-outline"><i class="typcn typcn-sort-alphabetically-outline" title="typcn typcn-sort-alphabetically-outline" alt="typcn typcn-sort-alphabetically-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-sort-alphabetically"><i class="typcn typcn-sort-alphabetically" title="typcn typcn-sort-alphabetically" alt="typcn typcn-sort-alphabetically"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-sort-numerically-outline"><i class="typcn typcn-sort-numerically-outline" title="typcn typcn-sort-numerically-outline" alt="typcn typcn-sort-numerically-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-sort-numerically"><i class="typcn typcn-sort-numerically" title="typcn typcn-sort-numerically" alt="typcn typcn-sort-numerically"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-spanner-outline"><i class="typcn typcn-spanner-outline" title="typcn typcn-spanner-outline" alt="typcn typcn-spanner-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-spanner"><i class="typcn typcn-spanner" title="typcn typcn-spanner" alt="typcn typcn-spanner"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-spiral"><i class="typcn typcn-spiral" title="typcn typcn-spiral" alt="typcn typcn-spiral"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-star-full-outline"><i class="typcn typcn-star-full-outline" title="typcn typcn-star-full-outline" alt="typcn typcn-star-full-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-star-half-outline"><i class="typcn typcn-star-half-outline" title="typcn typcn-star-half-outline" alt="typcn typcn-star-half-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-star-half"><i class="typcn typcn-star-half" title="typcn typcn-star-half" alt="typcn typcn-star-half"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-star-outline"><i class="typcn typcn-star-outline" title="typcn typcn-star-outline" alt="typcn typcn-star-outline"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-star"><i class="typcn typcn-star" title="typcn typcn-star" alt="typcn typcn-star"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-starburst-outline"><i class="typcn typcn-starburst-outline" title="typcn typcn-starburst-outline" alt="typcn typcn-starburst-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-starburst"><i class="typcn typcn-starburst" title="typcn typcn-starburst" alt="typcn typcn-starburst"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-stopwatch"><i class="typcn typcn-stopwatch" title="typcn typcn-stopwatch" alt="typcn typcn-stopwatch"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-support"><i class="typcn typcn-support" title="typcn typcn-support" alt="typcn typcn-support"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-tabs-outline"><i class="typcn typcn-tabs-outline" title="typcn typcn-tabs-outline" alt="typcn typcn-tabs-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-tag"><i class="typcn typcn-tag" title="typcn typcn-tag" alt="typcn typcn-tag"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-tags"><i class="typcn typcn-tags" title="typcn typcn-tags" alt="typcn typcn-tags"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-th-large-outline"><i class="typcn typcn-th-large-outline" title="typcn typcn-th-large-outline" alt="typcn typcn-th-large-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-th-large"><i class="typcn typcn-th-large" title="typcn typcn-th-large" alt="typcn typcn-th-large"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-th-list-outline"><i class="typcn typcn-th-list-outline" title="typcn typcn-th-list-outline" alt="typcn typcn-th-list-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-th-list"><i class="typcn typcn-th-list" title="typcn typcn-th-list" alt="typcn typcn-th-list"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-th-menu-outline"><i class="typcn typcn-th-menu-outline" title="typcn typcn-th-menu-outline" alt="typcn typcn-th-menu-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-th-menu"><i class="typcn typcn-th-menu" title="typcn typcn-th-menu" alt="typcn typcn-th-menu"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-th-small-outline"><i class="typcn typcn-th-small-outline" title="typcn typcn-th-small-outline" alt="typcn typcn-th-small-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-th-small"><i class="typcn typcn-th-small" title="typcn typcn-th-small" alt="typcn typcn-th-small"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-thermometer"><i class="typcn typcn-thermometer" title="typcn typcn-thermometer" alt="typcn typcn-thermometer"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-thumbs-down"><i class="typcn typcn-thumbs-down" title="typcn typcn-thumbs-down" alt="typcn typcn-thumbs-down"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-thumbs-ok"><i class="typcn typcn-thumbs-ok" title="typcn typcn-thumbs-ok" alt="typcn typcn-thumbs-ok"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-thumbs-up"><i class="typcn typcn-thumbs-up" title="typcn typcn-thumbs-up" alt="typcn typcn-thumbs-up"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-tick-outline"><i class="typcn typcn-tick-outline" title="typcn typcn-tick-outline" alt="typcn typcn-tick-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-tick"><i class="typcn typcn-tick" title="typcn typcn-tick" alt="typcn typcn-tick"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-ticket"><i class="typcn typcn-ticket" title="typcn typcn-ticket" alt="typcn typcn-ticket"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-time"><i class="typcn typcn-time" title="typcn typcn-time" alt="typcn typcn-time"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-times-outline"><i class="typcn typcn-times-outline" title="typcn typcn-times-outline" alt="typcn typcn-times-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-times"><i class="typcn typcn-times" title="typcn typcn-times" alt="typcn typcn-times"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-trash"><i class="typcn typcn-trash" title="typcn typcn-trash" alt="typcn typcn-trash"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-tree"><i class="typcn typcn-tree" title="typcn typcn-tree" alt="typcn typcn-tree"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-upload-outline"><i class="typcn typcn-upload-outline" title="typcn typcn-upload-outline" alt="typcn typcn-upload-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-upload"><i class="typcn typcn-upload" title="typcn typcn-upload" alt="typcn typcn-upload"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-user-add-outline"><i class="typcn typcn-user-add-outline" title="typcn typcn-user-add-outline" alt="typcn typcn-user-add-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-user-add"><i class="typcn typcn-user-add" title="typcn typcn-user-add" alt="typcn typcn-user-add"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-user-delete-outline"><i class="typcn typcn-user-delete-outline" title="typcn typcn-user-delete-outline" alt="typcn typcn-user-delete-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-user-delete"><i class="typcn typcn-user-delete" title="typcn typcn-user-delete" alt="typcn typcn-user-delete"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-user-outline"><i class="typcn typcn-user-outline" title="typcn typcn-user-outline" alt="typcn typcn-user-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-user"><i class="typcn typcn-user" title="typcn typcn-user" alt="typcn typcn-user"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-vendor-android"><i class="typcn typcn-vendor-android" title="typcn typcn-vendor-android" alt="typcn typcn-vendor-android"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-vendor-apple"><i class="typcn typcn-vendor-apple" title="typcn typcn-vendor-apple" alt="typcn typcn-vendor-apple"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-vendor-microsoft"><i class="typcn typcn-vendor-microsoft" title="typcn typcn-vendor-microsoft" alt="typcn typcn-vendor-microsoft"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-video-outline"><i class="typcn typcn-video-outline" title="typcn typcn-video-outline" alt="typcn typcn-video-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-video"><i class="typcn typcn-video" title="typcn typcn-video" alt="typcn typcn-video"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-volume-down"><i class="typcn typcn-volume-down" title="typcn typcn-volume-down" alt="typcn typcn-volume-down"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-volume-mute"><i class="typcn typcn-volume-mute" title="typcn typcn-volume-mute" alt="typcn typcn-volume-mute"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-volume-up"><i class="typcn typcn-volume-up" title="typcn typcn-volume-up" alt="typcn typcn-volume-up"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-volume"><i class="typcn typcn-volume" title="typcn typcn-volume" alt="typcn typcn-volume"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-warning-outline"><i class="typcn typcn-warning-outline" title="typcn typcn-warning-outline" alt="typcn typcn-warning-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-warning"><i class="typcn typcn-warning" title="typcn typcn-warning" alt="typcn typcn-warning"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-watch"><i class="typcn typcn-watch" title="typcn typcn-watch" alt="typcn typcn-watch"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-waves-outline"><i class="typcn typcn-waves-outline" title="typcn typcn-waves-outline" alt="typcn typcn-waves-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-waves"><i class="typcn typcn-waves" title="typcn typcn-waves" alt="typcn typcn-waves"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-weather-cloudy"><i class="typcn typcn-weather-cloudy" title="typcn typcn-weather-cloudy" alt="typcn typcn-weather-cloudy"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-weather-downpour"><i class="typcn typcn-weather-downpour" title="typcn typcn-weather-downpour" alt="typcn typcn-weather-downpour"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-weather-night"><i class="typcn typcn-weather-night" title="typcn typcn-weather-night" alt="typcn typcn-weather-night"></i></a></div>
											<div class="icon">
												<a href="#" data-icon="typcn typcn-weather-partly-sunny"><i class="typcn typcn-weather-partly-sunny" title="typcn typcn-weather-partly-sunny" alt="typcn typcn-weather-partly-sunny"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-weather-shower"><i class="typcn typcn-weather-shower" title="typcn typcn-weather-shower" alt="typcn typcn-weather-shower"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-weather-snow"><i class="typcn typcn-weather-snow" title="typcn typcn-weather-snow" alt="typcn typcn-weather-snow"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-weather-stormy"><i class="typcn typcn-weather-stormy" title="typcn typcn-weather-stormy" alt="typcn typcn-weather-stormy"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-weather-sunny"><i class="typcn typcn-weather-sunny" title="typcn typcn-weather-sunny" alt="typcn typcn-weather-sunny"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-weather-windy-cloudy"><i class="typcn typcn-weather-windy-cloudy" title="typcn typcn-weather-windy-cloudy" alt="typcn typcn-weather-windy-cloudy"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-weather-windy"><i class="typcn typcn-weather-windy" title="typcn typcn-weather-windy" alt="typcn typcn-weather-windy"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-wi-fi-outline"><i class="typcn typcn-wi-fi-outline" title="typcn typcn-wi-fi-outline" alt="typcn typcn-wi-fi-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-wi-fi"><i class="typcn typcn-wi-fi" title="typcn typcn-wi-fi" alt="typcn typcn-wi-fi"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-wine"><i class="typcn typcn-wine" title="typcn typcn-wine" alt="typcn typcn-wine"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-world-outline"><i class="typcn typcn-world-outline" title="typcn typcn-world-outline" alt="typcn typcn-world-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-world"><i class="typcn typcn-world" title="typcn typcn-world" alt="typcn typcn-world"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-zoom-in-outline"><i class="typcn typcn-zoom-in-outline" title="typcn typcn-zoom-in-outline" alt="typcn typcn-zoom-in-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-zoom-in"><i class="typcn typcn-zoom-in" title="typcn typcn-zoom-in" alt="typcn typcn-zoom-in"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-zoom-out-outline"><i class="typcn typcn-zoom-out-outline" title="typcn typcn-zoom-out-outline" alt="typcn typcn-zoom-out-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-zoom-out"><i class="typcn typcn-zoom-out" title="typcn typcn-zoom-out" alt="typcn typcn-zoom-out"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-zoom-outline"><i class="typcn typcn-zoom-outline" title="typcn typcn-zoom-outline" alt="typcn typcn-zoom-outline"></i></a></div><div class="icon">
												<a href="#" data-icon="typcn typcn-zoom"><i class="typcn typcn-zoom" title="typcn typcn-zoom" alt="typcn typcn-zoom"></i></a></div>

										</div>
										<!-- End row -->

                                    </div>

					  </div>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
