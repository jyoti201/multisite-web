<title>{{ getMetaTitle($page) }}</title>
<meta name="keywords" content="{{ !empty($page->meta_keywords)?$page->meta_keywords:'' }}"/>
<meta name="description" content="{{ !empty($page->meta_description)?$page->meta_description:'' }}"/>
<meta name="copyright"content="{{ getSetting('site_title','host') }}">
<meta name="language" content="en_US">
<meta name="robots" content="index,follow" />
<meta name="author" content="{{ getAdminUser('name') }}, {{ getAdminUser('email') }}">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta name="og:title" content="{{ getMetaTitle($page) }}"/>
@if($type=='post')
<meta name="og:image" content="{{ !empty($page->featured_image)?$page->featured_image:getSetting('logo','header') }}"/>
@elseif($type=='page')
<meta name="og:image" content="{{ getSetting('logo','header') }}"/>
@endif
<meta name="og:site_name" content="{{ getSetting('site_title','host') }}"/>
<meta name="og:description" content="{{ !empty($page->meta_description)?$page->meta_description:'' }}"/>
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:description" content="{{ !empty($page->meta_description)?$page->meta_description:'' }}">
<meta name="twitter:title" content="{{ getMetaTitle($page) }}">
