@extends('layouts.app')

@section('extra')

<link href="{{ asset('assets/private/plugins/cropper/cropper.css') }}" rel="stylesheet" type="text/css" />

<style>
img{
	max-width:100%;
}
.allmedia .imageholder{
	    width: 12.5%;
    float: left;
    padding: 0 10px;
	position: relative;
}
.details{
	font-size: 13px;

}
.modal .modal-dialog .modal-content{
	padding:0;
}
.modal .modal-dialog .modal-content .modal-footer,.modal .modal-dialog .modal-content .modal-header{
	padding:15px;
}
.modal .modal-dialog .modal-content .modal-body {
    padding: 0;
}
.modal-body .col-sm-8{
	padding:15px;
}
.modal-body{
	background: #f3f3f3;
}
.modal-header{
	border-bottom:0;
}
.modal-body .details{
	padding: 15px;
}
.modal-body .details table{
	width:100%;
}
.modal-body .details table td{
	padding:3px 5px;
}
.thumbnail{

}
.imageholder.recent .thumbnail{
	background: #000000;
}
.imageedit{
	display: block;
    position: relative;
    height: 100%;
    vertical-align: bottom;
}
.imageedit img{
	max-height: 100%;
}
.preview img,.imageedit .cropper-container img{
	max-width:none;
}
.preview{
    float: left;
    margin-bottom: .5rem;
    margin-right: .5rem;
    overflow: hidden;
	    position: relative;
			border: 4px double #eaeaea;
}
.preview-lg {
  height: 14rem;
  width: 21rem;
}
.imageedit.loader:before{
	content: '';
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background: rgba(0,0,0,.6);
    z-index: 999;
}
.imageedit.loader:after{
	content:'';
	width: 64px;
	height: 64px;
	position: absolute;
	border: 4px solid #fff;
	opacity: 1;
	border-radius: 50%;
	animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
	z-index: 999;
}
@keyframes lds-ripple {
  0% {
    top: 28px;
    left: 28px;
    width: 0;
    height: 0;
    opacity: 1;
  }
  100% {
    top: -1px;
    left: -1px;
    width: 58px;
    height: 58px;
    opacity: 0;
  }
}

.preview-md {
  height: 4.5rem;
  width: 8rem;
}

.preview-sm {
  height: 2.25rem;
  width: 4rem;
}

.preview-xs {
  height: 1.125rem;
  margin-right: 0;
  width: 2rem;
}
.editdetail .card-box{
	padding: 20px;
}
.buttons{
	padding: 0;
    margin: 0;
    list-style: none;
		display: block;
    padding: 0 !important;
		position: relative;
		border-top: 0;
}
.buttons li{
	display:inline-block;
}
.buttons li a{
	background: #5fbeaa;
    color: #ffffff;
    width: 32px;
    display: block;
    height: 32px;
    text-align: center;
    line-height: 15px;
    border-radius: 3px;
    border: 1px solid #ffffff;
    font-weight: 300;
}
.thumbnail,.thumbnail.active{
	
}
.allmedia .imageholder .thumbnail {
    margin-bottom: 0;
}
.allmedia .imageholder{
	margin-bottom:20px;
}
.thumbnail img{
}
.md-custom-body {
    height: calc(100% - 104px);
}
.md-custom-body > .row{
	height: 100%;
	    margin: 0;
}
.md-custom-body > .row .col-sm-4,.md-custom-body > .row .col-sm-8{
	height: 100%;
}
.md-custom-body > .row .col-sm-4{
	background: #ffffff;
    color: #000;
		padding: 20px;
    overflow-y: auto;
}
</style>
<script src="{{ asset('assets/private/plugins/cropper/cropper.js') }}"></script>

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Media Library
					<span>All Images</span>
					<button class="btn btn-default waves-effect waves-light btn-sm pull-right" data-toggle="collapse" data-target="#openuploader">Add New</button>
				</h4>
			</div>
			<div class="col-sm-12 m-t-15">
				<div class="uploader collapse" id="openuploader">
				<div class="card-box">
					<form action="" class="dropzone">

					</form>
				</div>
				</div>
				<div class="card-box">
					@if (\Session::has('success'))
					<div class="row">
						<div class="col-sm-12 m-t-15">
							<div class="alert alert-success">
							{!! \Session::get('success') !!}
							</div>
						</div>
					</div>
					@endif
					<div class="allmedia row" style="clear:both">
						@include('admin.ajax.media')
					</div>
					<div class="row hidden" style="clear:both">
						<div class="col-sm-12 m-t-15">{{ $uploads->links() }}</div>
					</div>
				</div>
				<div class="hidden">
					<div id="preview-template">
						<div class="dz-preview imageholder dz-file-preview">
						  <div class="dz-details">
							<div class="thumbnail active"><img data-dz-thumbnail /></div>
						  </div>
						  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
						  <div class="dz-error-message"><span data-dz-errormessage></span></div>
						</div>
					</div>
				</div>
			</div>
    </div>
</div>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<script>

    Dropzone.autoDiscover = false;
$(function(){

    uploader = new Dropzone(".dropzone",{
        url: "{{ route('media.upload')}}",
        uploadMultiple :false,
        acceptedFiles : "image/*,video/*,audio/*",
        addRemoveLinks: false,
        forceFallback: false,
		thumbnailWidth: 190,
		thumbnailHeight: 190,
		init: function () {
			this.on('addedfile', function (file) {
			   $('.allmedia').prepend($(file.previewElement));
			   $('.dropzone').removeClass('dz-started');
			}),
			this.on("success", function(file, responseText) {
				$('.dz-success').addClass('recent');
				$('.dz-success .active').html(responseText);
				$('.dz-success .thumbnail').removeClass('active');
			}),
			this.on("error", function(file, responseText) {
				alert(responseText.errors.file);
				$('.dz-error').remove();
			})
		},
		previewTemplate: document.getElementById('preview-template').innerHTML,
        maxFilesize:1024,
        parallelUploads: 1,
		headers: {
			'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
		}
    });

	$(document).ready(function(){
		$('.allmedia').on('click','.editimage', function(){

				var imageID = $(this).attr('data-imageid');
				var imageholderID = '#'+$(this).attr('data-imageholder');
				var imageholder = $(imageholderID);

				imageholder.find('.details').hide();
				imageholder.find('.editdetail').fadeIn();
				imageholder.find('.imageedit').addClass('loader');

				var $image = $('#'+imageID);
			  var $dataX = imageholder.find('.dataX');
			  var $dataY = imageholder.find('.dataY');
			  var $dataHeight = imageholder.find('.dataHeight');
			  var $dataWidth = imageholder.find('.dataWidth');
			  var $dataRotate = imageholder.find('.dataRotate');
			  var $dataScaleX = imageholder.find('.dataScaleX');
			  var $dataScaleY = imageholder.find('.dataScaleY');
			  var options = {
			    preview: imageholderID+' .preview',
			    crop: function (e) {
			      $dataX.val(Math.round(e.detail.x));
			      $dataY.val(Math.round(e.detail.y));
			      $dataHeight.val(Math.round(e.detail.height));
			      $dataWidth.val(Math.round(e.detail.width));
			      $dataRotate.val(e.detail.rotate);
			      $dataScaleX.val(e.detail.scaleX);
			      $dataScaleY.val(e.detail.scaleY);
			    }
			  };
			  var originalImageURL = $image.attr('src');
			  var uploadedImageName = 'cropped.jpg';
			  var uploadedImageType = 'image/jpeg';
			  var uploadedImageURL;


			  // Cropper
			  $image.on({
			    ready: function (e) {
			      console.log(e.type);
				  imageholder.find('.imageedit').removeClass('loader');
			    },
			    cropstart: function (e) {
			      //console.log(e.type, e.detail.action);
			    },
			    cropmove: function (e) {
			      //console.log(e.type, e.detail.action);
			    },
			    cropend: function (e) {
			      //console.log(e.type, e.detail.action);
			    },
			    crop: function (e) {
			      //console.log(e.type);
				  imageholder.find('.imageedit').removeClass('loader');
			    },
			    zoom: function (e) {
			      //console.log(e.type, e.detail.ratio);
			    }
			  }).cropper(options);
				var cropper = $('#'+imageID).data('cropper');
				cropper.crop();
		})
		$('.allmedia').on('click','.canceledit', function(){
			var imageID = $(this).attr('data-imageid');
			var image = document.getElementById(imageID);
			var imageholderID = '#'+$(this).attr('data-imageholder');

			var imageholder = $(imageholderID);
			var cropper = $('#'+imageID).data('cropper');

			cropper.destroy();

			imageholder.find('.details').fadeIn();
			imageholder.find('.editdetail').hide();
			return false;
		})
		$('.allmedia').on('click','.saveimg', function(){
			$(this).prop('disabled', true);
			var imageID = $(this).attr('data-imageid');
			var image = document.getElementById(imageID);
			var imageholderID = '#'+$(this).attr('data-imageholder');

			var imageholder = $(imageholderID);
			var cropper = $('#'+imageID).data('cropper');
			imageholder.find('.imageedit').addClass('loader');
			var image = cropper.getCroppedCanvas().toDataURL('image/jpeg');

			$.ajax({
	            type: "POST",
	            url: '{{URL::route('media.croppedupload')}}',
	            data: {_token:'{{csrf_token()}}', image: image},
	            success: function( msg ) {
					$(this).prop('disabled', false);
	                if(msg == 'success'){
						cropper.destroy();
						imageholder.find('.details').fadeIn();
						imageholder.find('.editdetail').hide();
						imageholder.find('.imageedit').removeClass('loader');
						setTimeout(function(){ location.reload(); }, 500);

					}
	            }
	        });
			return false;
		})
		$('.allmedia').on('click','.md-overlay', function(){
				$($(this).data('target')).removeClass('md-custom-show');
		})
		$('.allmedia').on('click','.buttons a', function(){
			var imageID = $(this).attr('data-imageid');
			var cropper = $('#'+imageID).data('cropper');
			var data = $(this).data();
			switch (data.method) {
				case 'zoom':
					cropper.zoom(data.option);
				    break;
				case 'moveH':
					cropper.move(data.option, 0);
					break;
				case 'moveV':
					cropper.move(0, data.option);
					break;
				case 'rotate':
					cropper.rotate(data.option);
					break;
				case 'scaleX':
					cropper.scale(data.option, 1);
					break;
				case 'scaleY':
					cropper.scale(1, data.option);
					break;
			}
		})
		$('.allmedia').infiniteScroll({
		  path: '.pagination li a[rel="next"]',
		  append: '.allmedia .imageholder',
		  history: false,
		  prefill: true
		});
	})
});
</script>
@endsection
