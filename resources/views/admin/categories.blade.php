@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/private/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

<style>
.table .thumbnail{
	width: 50px;
	height:50px;
}
.table .thumbnail img{
	max-width:100%;
}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Categories <span>All Categories</span><button class="btn btn-default waves-effect waves-light btn-sm pull-right openmodal" data-toggle="modal" data-target="#con-close-modal">Add New Category</button></h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="navigation">
					<ul>
						<li><a href="{{URL::route('categories.index')}}" class="btn {{ !empty($trash)? 'btn-white': 'btn-default' }} waves-effect">All</a></li>
						<li><a href="{{URL::route('categories.trash')}}" class="btn {{ empty($trash)? 'btn-white': 'btn-default' }} waves-effect">Deleted</a></li>
					</ul>
				</div>
				<div class="card-box">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Category Image</th>
								<th>Name</th>
								<th>Slug</th>
								<th>Created on</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach (json_decode($categories) as $category)
							<tr>
								<td>
									@if(!empty($category->category_image))
										<div class="thumbnail"><img src="{{ $category->category_image }}"></div>
									@else

									@endif
								</td>
								<td>
								{{ $category->title }}
								</td>
								<td>{{ $category->slug }}</td>
								<td>Published<br />{{ $category->created_at }}</td>
								<td width="160" align="center">
								@if(empty($trash))
								<a href="{{URL::route('categories.clone', $category->id)}}" class="btn btn-icon waves-effect waves-light btn-warning"> <i class="fa fa-clone"></i> </a>
								<button class="btn btn-icon waves-effect waves-light btn-warning openmodal" data-toggle="modal" data-target="#update-page-{{ $category->id }}"> <i class="fa fa-wrench"></i> </button>
								<a href="javascript:void(0);" class="btn btn-icon waves-effect waves-light btn-danger" onclick="$(this).find('form').submit();"> <i class="fa fa-remove"></i>
									<form action="{{ route('categories.destroy', $category->id) }}" method="POST">
									{{ csrf_field() }}
									<input type="hidden" name="_method" value="DELETE">
									</form>
								</a>
								@else
								<a href="{{ route('category.restore', $category->id) }}" class="btn btn-icon waves-effect waves-light btn-warning"> <i class="fa fa-arrow-left"></i> </a>
								<a href="{{ route('category.forcedelete', $category->id) }}" class="btn btn-icon waves-effect waves-light btn-danger"> <i class="fa fa-remove"></i> </a>
								@endif

								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
    </div>
</div>
@foreach (json_decode($categories) as $category)
<div id="update-page-{{ $category->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Update {{ $category->title }}</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<form method="POST" class="updatepageform" action="">
					{{ csrf_field() }}
					<input type="hidden" name="catid" value="{{ $category->id }}">
					<div class="col-md-12">
						<div class="form-group inlinelabel title">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Title</h4>
							<input type="text" name="title" class="form-control" placeholder="" value="{{ $category->title }}" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group inlinelabel slug">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Slug</h4>
							<input type="text" name="slug" class="form-control" value="{{ $category->slug }}" placeholder="" >
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Description</h4>
							<textarea name="description" class="form-control">{{ !empty($category->description)?$category->description:'Category Description' }}</textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Category Image</h4>




							<div class="choosemedia">
									<div class="addedimage">
												<div class="image {{ !empty($category->category_image) ? 'image-has' : '' }}"  style="{{ !empty($category->category_image)?'background-image:url('.$category->category_image.')':'' }}">
												<a href="#" class="removeimage">Remove</a>
												<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Image</h5></i> </a>
											</div>
											<input type="hidden" name="category_image" value="{{ !empty($category->category_image) ? $category->category_image:'' }}">
									</div>
								</div>
						</div>
					</div>



					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info waves-effect waves-light update">Update category</button>
			</div>
		</div>
	</div>
</div><!-- /.modal -->
@endforeach
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Create New category</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<form method="POST" id="createpageform" action="">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="form-group inlinelabel title">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Title</h4>
							<input type="text" name="title" class="form-control" placeholder="" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group inlinelabel slug">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Slug</h4>
							<input type="text" name="slug" class="form-control" placeholder="" >
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Description</h4>
							<textarea name="description" class="form-control" placeholder="Category Description"></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Category Image</h4>
							<div class="choosemedia">
									<div class="addedimage">
												<div class="image">
												<a href="#" class="removeimage">Remove</a>
												<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Image</h5></i> </a>
											</div>
											<input type="hidden" name="category_image" value="">
									</div>
								</div>
						</div>
					</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" id="save" class="btn btn-info waves-effect waves-light">Add category</button>
			</div>
		</div>
	</div>
</div><!-- /.modal -->


@include('admin._partials.popupuploader')


<script src="{{ asset('assets/private/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.colVis.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('assets/private/pages/datatables.init.js') }}"></script>
<script>
$(document).ready(function(){
	$('input[name="title"]').bind('paste change keyup', function(){
		var title = $(this).val().toLowerCase().replace(/[^a-z0-9\s]/gi, '-').replace(/ /g,'-');
		$(this).parents('form').find('input[name="slug"]').val(title);
	})
	$('#save').on('click', function(){
		$('#createpageform input[type="text"]').removeClass('parsley-error');
		var parentEle = $(this).parents('.modal');
		parentEle.find('.help-block').remove();
		$.ajax({
            type: "POST",
            url: '{{URL::route('categories.store')}}',
            data: $("#createpageform").serialize(),
            success: function( msg ) {
				console.log(msg);
                if(msg == 'success'){
					$('#con-close-modal').find('.modal-body').prepend('<div class="alert alert-success"><strong>Success!</strong> category created.</div>');
					setTimeout(function(){
						window.location.href = "{{URL::route('categories.index')}}";
					}, 1000);
				}else{
					var obj = jQuery.parseJSON(msg);
					$.each(obj.errors, function(key,valueObj){
						parentEle.find('.'+key).append('<span class="help-block"><strong>'+valueObj+'</strong></span>');
					});
				}
            }
        });
	})
	$('.modal .update').on('click', function(){
		$('.modal .update input[type="text"]').removeClass('parsley-error');
		var btn = $(this);
		var parentEle = $(this).parents('.modal');
		parentEle.find('.help-block').remove();
		$.ajax({
            type: "POST",
            url: '{{URL::route('categories.store')}}',
            data: btn.parents('.modal').find('form').serialize(),
            success: function( msg ) {
                if(msg == 'success'){
					btn.parents('.modal').find('.modal-body').prepend('<div class="alert alert-success"><strong>Success!</strong> category updated.</div>');
					setTimeout(function(){
						window.location.href = "{{URL::route('categories.index')}}";
					}, 1000);
				}else{
					var obj = jQuery.parseJSON(msg);
					$.each(obj.errors, function(key,valueObj){
						parentEle.find('.'+key).append('<span class="help-block"><strong>'+valueObj+'</strong></span>');
					});
				}
            }
        });
	})
	$('.openmodal').on('click', function(){
		$('.modal').find('.alert').remove();
	})
	$('.addedimage a').on('click', function(){
		jQuery('.choosemedia').removeClass('selected');
		$(this).parents('.choosemedia').toggleClass('selected');
	})
	$('#media-select').on('click','.thumbnail a', function(){
		var image =$(this).attr('href');
		$('.choosemedia.selected').find('.image').css('background-image', 'url('+image+')');
		$('.choosemedia.selected').find('.image').addClass('image-has');
		$('.choosemedia.selected').find('.image').next('input[type="hidden"]').val(image);
		$('#media-select').removeClass('md-custom-show');
		return false;
	})
	$('.removeimage').click(function(){
		$(this).parents('.image').css('background-image', 'none');
		$(this).parents('.image').removeClass('image-has');
		$(this).parents('.image').next('input[type="hidden"]').val('');
		return false;
	})
	TableManageButtons.init();
})
</script>

@endsection
