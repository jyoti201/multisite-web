@extends('layouts.app')

@section('extra')

<link href="{{ asset('assets/private/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<style>

.modal-dialog{
	width:100%;
	max-width:800px;
}
.thumbnail img{
	max-width:100%;
	margin:0;
}
.widget input[type="radio"]{
	display:none;
}
.widget.thumbnail{
	padding:0;
	border-radius:0;
}
.widget.thumbnail label{
	padding:5px;
	cursor:pointer;
	margin:0;
}
.slides{
	    min-height: 420px;
    overflow: hidden;
    position: relative;
	overflow-y:auto;
}
.slide{
	position:absolute;
	-webkit-transition: all .2s ease-in-out;
  -moz-transition: all .2s ease-in-out;
  -o-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
    width: 100%;
    vertical-align: top;
}
#slide2{
	left: 100%;
	padding: 0 15px;
}
#slide2 .widget{
	display:none;
}
input[type="radio"]:checked+label{
	background-color:#999999;
}
btn.back{
	display:none;
}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Widgets <span>All Widgets</span> <button class="btn btn-default waves-effect waves-light btn-sm pull-right openmodal" data-toggle="modal" data-target="#con-close-modal">Add New Widget</button></h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="card-box">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Title</th>
								<th>Type</th>
								<th>Shortcode</th>
								<th>Created on</th>
							</tr>
						</thead>
						<tbody>
							@foreach (json_decode($savedwidgets) as $widget)
							<tr>
								<td>
								{{ $widget->title }}
								<ul class="buttons">
									<li><a href="{{URL::route('widget.edit', $widget->id)}}"><i class="fas fa-pencil-alt"></i></a></li>
									<li><a href="{{ route('widget.delete', $widget->id) }}"> <i class="fa fa-remove"></i> </a></li>
									<li><a href="{{URL::route('widget.clone', $widget->id)}}"> <i class="fa fa-clone"></i> </a></li>
								</ul>
								</td>
								<td>
								{{ getWidgetTitle($widget->type) }}
								</td>
								<td>{{ $widget->shortcode }}</td>
								<td width="180px">Published<br />{{ $widget->created_at }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
    </div>
</div>

<div id="con-close-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Widget</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
		<div class="row">
			<div class="slides col-12">
			<form method="POST" id="createsaveform" action="{{URL::route('widget.save')}}">
			{{ csrf_field() }}
				<div class="slide row" id="slide1">
					@foreach($widgets as $key=>$widget)
						<div class="col-sm-3">
							<div class="thumbnail widget"><input type="radio" name="type" id="{{ $key }}" value="{{ $key }}"><label for="{{ $key }}"><img src="{{ asset('assets/private/images/widgets/'.$key.'.jpg') }}"></label></div>
						</div>
					@endforeach
				</div>
				<div class="slide row" id="slide2">
						<div class="col-md-12">
							<div class="form-group">
								@foreach($widgets as $key=>$widget)
								<div class="row m-b-15 widget {{ $key }}">
								<div class="col-sm-4">
								<div class="thumbnail"><img src="{{ asset('assets/private/images/widgets/'.$key.'.jpg') }}"></div>
								</div>
								<div class="col-sm-8">
								<h3>{{ $widget }}</h3>
								</div>
								</div>
								@endforeach
							</div>
							<div class="form-group title">
								<label for="field-1" class="control-label">Title</label>
								<input type="text" name="title" class="form-control" placeholder="" value="" required>

							</div>
							<div class="form-group">
							<button type="button" id="save" class="btn btn-info waves-effect waves-light">Add Widget</button>
							</div>
						</div>
				</div>
			</form>
			</div>
		</div>
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-default back">Back</button> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<script src="{{ asset('assets/private/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.colVis.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('assets/private/pages/datatables.init.js') }}"></script>
<script>
$(document).ready(function(){
	TableManageButtons.init();
	$('.widget label').click(function(){
		$('#slide2').css('left','0');
		$('#slide1').css('left','-100%');
		setTimeout(function(){
			var selected = $('input[name="type"]:checked').val();
			$('#slide2 .'+selected).show();
		}, 50);
		$('.btn.back').show();
	})
	$('.btn.back').click(function(){
		$('#slide2').css('left','100%');
		$('#slide1').css('left','0');
		$('#slide2 .widget').hide();
		$('.btn.back').hide();
	})
	$('#save').on('click', function(){
		$('#createsaveform input[type="text"]').removeClass('parsley-error');
		var parentEle = $(this).parents('.modal');
		parentEle.find('.help-block').remove();
		$.ajax({
            type: "POST",
            url: '{{URL::route('widget.save')}}',
            data: $("#createsaveform").serialize(),
            success: function( msg ) {
                if(msg == 'success'){
					$('#con-close-modal').find('.modal-body').prepend('<div class="alert alert-success"><strong>Success!</strong> widget created.</div>');
					setTimeout(function(){
						window.location.href = "{{URL::route('widgets.index')}}";
					}, 1000);
				}else{
					var obj = jQuery.parseJSON(msg);
					$.each(obj.errors, function(key,valueObj){
						parentEle.find('.'+key).append('<span class="help-block"><strong>'+valueObj+'</strong></span>');
					});
				}
            }
        });
	})
})
</script>

@endsection
