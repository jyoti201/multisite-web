@extends('layouts.app')

@section('extra')

<style>

.grid figure{
	float:none;
}
</style>
@endsection

@section('content')
<form class="form-horizontal" id="createForm" role="form" method="POST" action="{{ route('profileSave') }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Profile</h4>
			</div>

			<div class="col-sm-3 m-t-15">
				<div class="card-box">


						<div class="choosemedia">
								<div class="addedimage">
											<div class="image {{ !empty($member['avatar']) ? 'image-has' : '' }}"  style="{{ !empty($member['avatar'])?'background-image:url('.$member['avatar'].')':'background-image:url('.asset('assets/private/images/users/avatar.png').')' }}">
											<a href="#" class="removeimage">Remove</a>
											<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Image</h5></i> </a>
										</div>
										<input type="hidden" name="avatar" value="{!! !empty($member['avatar']) ? $member['avatar'] : '' !!}">
								</div>
							</div>


				</div>
			</div>
			<div class="col-sm-9 m-t-15">
				<div class="card-box">
						@if(count( $errors ) > 0 )
						<div class="m-t-15">
						   @foreach ($errors->all() as $error)
							  <div class="alert alert-danger">
							  {{ $error }}
							  </div>
						  @endforeach
						</div>
						@endif
						@if (\Session::has('success'))
						<div class="m-t-15">
							<div class="alert alert-success">
								{!! \Session::get('success') !!}
							</div>
						</div>
						@endif
						<div class="form-group row">
							<label class="col-md-3 control-label">User Name</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="name" value="{{ isset($member->name) ? $member->name : $member->name }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 control-label">E-Mail Address</label>
							<div class="col-md-9">
								<input type="email" class="form-control" name="email" value="{{ !empty($member->email) ? $member->email : $member->email }}" readonly>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 control-label">Phone Number</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="phone" value="{{ isset($member->phone) ? $member->phone : $member->phone }}">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 control-label">Date of Birth</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="dob" id="datepicker" value="{{ isset($member->dob) ? $member->dob : $member->dob }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 control-label">Password</label>
							<div class="col-md-9">
								<input type="password" class="form-control" name="pass" value="{{ isset($member->pass) ? $member->pass : '' }}">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 control-label">Confirm Password</label>
							<div class="col-md-9">
								<input type="password" class="form-control" name="pass_confirmation" value="{{ isset($member->pass_confirmation) ? $member->pass_confirmation : '' }}">
							</div>
						</div>

						<input type="hidden" class="form-control" name="id" value="{{ isset($member->id) ? $member->id : old('id') }}">

						<div class="form-group row">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>
				</div>
			</div>

    </div>
</div>
</form>
@include('admin._partials.popupuploader')

<script>
$(document).ready(function(){
	$('.addedimage a').on('click', function(){
		jQuery('.choosemedia').removeClass('selected');
		$(this).parents('.choosemedia').toggleClass('selected');
	})
	$('#media-select').on('click','.thumbnail a', function(){
		var image =$(this).attr('href');
		$('.choosemedia.selected').find('.image').css('background-image', 'url('+image+')');
		$('.choosemedia.selected').find('.image').addClass('image-has');
		$('.choosemedia.selected').find('.image').next('input[type="hidden"]').val(image);
		$('#media-select').removeClass('md-custom-show');
		return false;
	})
	$('.removeimage').click(function(){
		$(this).parents('.image').css('background-image', 'none');
		$(this).parents('.image').removeClass('image-has');
		$(this).parents('.image').next('input[type="hidden"]').val('');
		return false;
	})
	$('#save').click(function(){
		$('#widgetform').submit();
	})
})
</script>
@endsection
