<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Images</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""> 
	<link href="{{ asset('assets/private/css/styles.css') }}" rel="stylesheet">
	<script src="{{ asset('assets/public/js/jquery.min.js') }}"></script>
	<style>
	.imageholder{
		width: 25%;
		float: left;
		padding: 0 15px;
	}
	#media-select img{
		margin:0;
	}
	</style>
</head>
<body>
@foreach($uploads as $upload)
	<div class="imageholder">
		<div class="thumbnail"><a href="{{ getImage($upload->slug,'') }}" data-toggle="modal" data-target="#thumbnail-modal-{{ $upload->id }}"><img src="{{ getImage($upload->slug,'thumbnail') }}"></a></div>
	</div>
@endforeach
<script>
jQuery(document).ready(function ($) {
	window.frameElement.style.height = '500px';
	$(".thumbnail a").click(function () {
		selectAsset($(this).attr('href'));
		window.parent.$('#btnImgOk').trigger('click');
		return false;
	});
});
function selectAsset(assetValue) {
	var inp = parent.top.$('#active-input').val();
	parent.top.$('#' + inp).val(assetValue);

	if (window.frameElement.id == 'ifrFileBrowse') parent.top.$("#md-fileselect").data('simplemodal').hide();
	if (window.frameElement.id == 'ifrImageBrowse') parent.top.$("#md-imageselect").data('simplemodal').hide();
}
</script>
</body>
</html>
