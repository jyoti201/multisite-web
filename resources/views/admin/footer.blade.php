@extends('layouts.app')

@section('extra')

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Options <span>Footer Builder</span></h4>
		</div>

			@if (\Session::has('success'))
			<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
			</div>
			@endif
			</div>
			<div class="row m-t-15">
        <div class="col-12">
					<div class="is-wrapper" style="overflow: hidden">
					<iframe src="{{ route('settings.footerbuilder') }}" width="100%" height="1000" frameborder="0">
					<p>Your browser does not support iframes.</p>
					</iframe>
					</div>
        </div>
			</div>
    </div>
</div>

@endsection
