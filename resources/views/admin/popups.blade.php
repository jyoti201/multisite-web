@extends('layouts.app')

@section('extra')

<link href="{{ asset('assets/private/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>


@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Popups <span>All Popups</span><button class="btn btn-default waves-effect waves-light btn-sm pull-right openmodal" data-toggle="modal" data-target="#con-close-modal">Add New Popup</button></h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="navigation">
					<ul>
						<li><a href="{{URL::route('popups.index')}}" class="btn {{ !empty($trash)? 'btn-white': 'btn-default' }} waves-effect">All</a></li>
						<li><a href="{{URL::route('popups.trash')}}" class="btn {{ empty($trash)? 'btn-white': 'btn-default' }} waves-effect">Deleted</a></li>
					</ul>
				</div>
				<div class="card-box">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Name</th>
								<th>Class</th>
								<th>Created on</th>
							</tr>
						</thead>
						<tbody>
							@foreach (json_decode($popups) as $popup)
							<tr>
								<td>
								{{ $popup->title }}
								<ul class="buttons">
									@if(!empty($trash))
									<li><a href="{{ route('popups.restore', $popup->id) }}"> <i class="fa fa-arrow-left"></i> </a></li>
									<li><a href="{{ route('popups.forcedelete', $popup->id) }}"> <i class="fa fa-remove"></i> </a></li>
									@else
									<li><a href="{{URL::route('popups.edit', $popup->id)}}"><i class="fas fa-pencil-alt"></i></a></li>
									<li><a href="{{URL::route('popups.clone', $popup->id)}}"><i class="fa fa-clone"></i></a></li>
									<li><a href="#" data-toggle="modal" data-target="#update-page-{{ $popup->id }}"> <i class="fa fa-wrench"></i> </a></li>
									<li><a href="javascript:void(0);" onclick="$(this).find('form').submit();"> <i class="fa fa-remove"></i>
										<form action="{{ route('popups.destroy', $popup->id) }}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE">
										</form>
									</a>
									</li>
									@endif
								</ul>
								</td>
								<td>{{ $popup->class }}</td>
								<td width="180px">Published<br />{{ $popup->created_at }}</td>

							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
    </div>
</div>
@foreach (json_decode($popups) as $popup)
<div id="update-page-{{ $popup->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Update {{ $popup->title }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<form method="POST" class="updatepageform" action="">
					{{ csrf_field() }}
					<input type="hidden" name="popupid" value="{{ $popup->id }}">
					<div class="col-md-6">
            <div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Trigger</h4>
							<select  data-style="btn-white" class="selectpicker form-control" name="trigger">
								<option value="manual" {{ !empty(unserialize($popup->settings)['trigger']) && unserialize($popup->settings)['trigger']=='manual'?'selected':'' }}>Manual</option>
								<option value="auto" {{ !empty(unserialize($popup->settings)['trigger']) && unserialize($popup->settings)['trigger']=='auto'?'selected':'' }}>Auto</option>
							</select>
						</div>
            <div class="trigger-control collapse auto {{ !empty(unserialize($popup->settings)['trigger']) && unserialize($popup->settings)['trigger']=='auto'?'in':'' }}">
  						<div class="form-group inlinelabel">
  							<h4 class="text-muted m-b-15 m-t-15 font-15">Delay</h4>
  							<input type="number" name="delay" class="form-control" placeholder="ms" value="{{ !empty(unserialize($popup->settings)['delay'])?unserialize($popup->settings)['delay']:'1000' }}">
  						</div>
  						<div class="checkbox">
  							<input name="enablecookie" id="enablecookie" type="checkbox" {{ !empty(unserialize($popup->settings)['enablecookie'])?'checked':'' }}>
  							<label for="enablecookie">
  								Enable Cookie
  							</label>
  						</div>
  						<div class="form-group inlinelabel">
  							<h4 class="text-muted m-b-15 m-t-15 font-15">Cookie Expires (in Days)</h4>
  							<input type="number" name="cookieexpiry" class="form-control" placeholder="days" value="{{ !empty(unserialize($popup->settings)['cookieexpiry'])?unserialize($popup->settings)['cookieexpiry']:'7' }}">
  						</div>
  					</div>
            <div class="form-group inlinelabel title">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Title</h4>
							<input type="text" name="title" class="form-control" placeholder="" value="{{ $popup->title }}" required>
						</div>
            <div class="form-group inlinelabel class">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Class</h4>
							<input type="text" name="class" class="form-control" value="{{ $popup->class }}" placeholder=""  required>
						</div>
            <div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Width</h4>
							<input type="number" name="width" class="form-control" value="{{ !empty(unserialize($popup->settings)['width'])?unserialize($popup->settings)['width']:'' }}" placeholder="px" >
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Popup Background Color</h4>
							<div data-color-format="rgba" data-color="{{ !empty(unserialize($popup->settings)['bg_color'])?unserialize($popup->settings)['bg_color']:'#ffffff' }}" class="colorpicker-rgba input-group">
								<input type="text" name="bg_color" value="{{ !empty(unserialize($popup->settings)['bg_color'])?unserialize($popup->settings)['bg_color']:'#ffffff' }}" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color: {{ !empty(unserialize($popup->settings)['bg_color'])?unserialize($popup->settings)['bg_color']:'#ffffff' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
            <div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Popup Close Button Color</h4>
							<div data-color-format="rgba" data-color="{{ !empty(unserialize($popup->settings)['btn_color'])?unserialize($popup->settings)['btn_color']:'#000000' }}" class="colorpicker-rgba input-group">
								<input type="text" name="btn_color" value="{{ !empty(unserialize($popup->settings)['btn_color'])?unserialize($popup->settings)['btn_color']:'#000000' }}" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color: {{ !empty(unserialize($popup->settings)['btn_color'])?unserialize($popup->settings)['btn_color']:'#000000' }};margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
            <div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Popup Close Button Size</h4>
							<input type="number" name="btn_size" class="form-control" placeholder="px" value="{{ !empty(unserialize($popup->settings)['btn_size'])?unserialize($popup->settings)['btn_size']:'21' }}">
						</div>
            <div class="row">
                <div class="col-6 align-self-center">
      							<div class="checkbox" style="margin:0;">
      								<input name="displayheader" id="displayheader_{{ $popup->id }}" type="checkbox" {{ !empty(unserialize($popup->settings)['displayheader'])?'checked':'' }}>
      								<label for="displayheader_{{ $popup->id }}">
      									Display header
      								</label>
      							</div>
                </div>
                <div class="col-6 align-self-center">
        							<div class="checkbox" style="margin:0;">
        								<input name="displayfooter" id="displayfooter_{{ $popup->id }}" type="checkbox" {{ !empty(unserialize($popup->settings)['displayfooter'])?'checked':'' }}>
        								<label for="displayfooter_{{ $popup->id }}">
        									Display footer
        								</label>
        							</div>
                </div>
  					</div>
					</div>

          <div class="form-group col-md-12">
					<div class="radio switch">
						<input name="position" id="position1_{{ $popup->id }}" type="radio" value="modal-top" {{ !empty(unserialize($popup->settings)['position']) && unserialize($popup->settings)['position']=='modal-top'?'checked':'' }}>
						<label for="position1_{{ $popup->id }}">
							Top
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position2_{{ $popup->id }}" type="radio" value="modal-dialog-centered" {{ empty(unserialize($popup->settings)['position'])?'checked':'' }} {{ !empty(unserialize($popup->settings)['position']) && unserialize($popup->settings)['position']=='modal-dialog-centered'?'checked':'' }}>
						<label for="position2_{{ $popup->id }}">
							Center
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position3_{{ $popup->id }}" type="radio" value="modal-bottom" {{ !empty(unserialize($popup->settings)['position']) && unserialize($popup->settings)['position']=='modal-bottom'?'checked':'' }}>
						<label for="position3_{{ $popup->id }}">
							Bottom
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position4_{{ $popup->id }}" type="radio" value="modal-top-right" {{ !empty(unserialize($popup->settings)['position']) && unserialize($popup->settings)['position']=='modal-top-right'?'checked':'' }}>
						<label for="position4_{{ $popup->id }}">
							Top Right
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position5_{{ $popup->id }}" type="radio" value="modal-top-left" {{ !empty(unserialize($popup->settings)['position']) && unserialize($popup->settings)['position']=='modal-top-left'?'checked':'' }}>
						<label for="position5_{{ $popup->id }}">
							Top Left
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position6_{{ $popup->id }}" type="radio" value="modal-bottom-right" {{ !empty(unserialize($popup->settings)['position']) && unserialize($popup->settings)['position']=='modal-bottom-right'?'checked':'' }}>
						<label for="position6_{{ $popup->id }}">
							Bottom Right
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position7_{{ $popup->id }}" type="radio" value="modal-bottom-left" {{ !empty(unserialize($popup->settings)['position']) && unserialize($popup->settings)['position']=='modal-bottom-left'?'checked':'' }}>
						<label for="position7_{{ $popup->id }}">
							Bottom Left
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position8_{{ $popup->id }}" type="radio" value="modal-right" {{ !empty(unserialize($popup->settings)['position']) && unserialize($popup->settings)['position']=='modal-right'?'checked':'' }}>
						<label for="position8_{{ $popup->id }}">
							Right (Full height)
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position9_{{ $popup->id }}" type="radio" value="modal-left" {{ !empty(unserialize($popup->settings)['position']) && unserialize($popup->settings)['position']=='modal-left'?'checked':'' }}>
						<label for="position9_{{ $popup->id }}">
							Left (Full height)
						</label>
					</div>
				</div>
			</div>
					</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info waves-effect waves-light update">Update Popup</button>
			</div>
		</div>
	</div>
</div><!-- /.modal -->
@endforeach
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Create New Popup</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<form method="POST" id="createpageform" action="">
					{{ csrf_field() }}
					<div class="col-md-6">
						<div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Trigger</h4>
							<select  data-style="btn-white" class="selectpicker form-control" name="trigger">
								<option value="manual">Manual</option>
								<option value="auto">Auto</option>
							</select>
						</div>
            <div class="trigger-control collapse auto">
  						<div class="form-group inlinelabel">
  							<h4 class="text-muted m-b-15 m-t-15 font-15">Delay</h4>
  							<input type="number" name="delay" class="form-control" placeholder="ms" value="1000">
  						</div>
  					</div>
            <div class="form-group inlinelabel title">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Title</h4>
							<input type="text" name="title" class="form-control" placeholder="" required>
						</div>
            <div class="form-group inlinelabel class">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Class</h4>
							<input type="text" name="class" class="form-control" placeholder="" required>
						</div>
            <div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Width</h4>
							<input type="number" name="width" class="form-control" placeholder="px" >
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Popup Background Color</h4>
							<div data-color-format="rgba" data-color="#ffffff" class="colorpicker-rgba input-group">
								<input type="text" name="bg_color" value="#ffffff" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color: #ffffff;margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
            <div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Popup Close Button Color</h4>
							<div data-color-format="rgba" data-color="#000000" class="colorpicker-rgba input-group">
								<input type="text" name="btn_color" value="#000000" class="form-control">
								<span class="input-group-btn add-on">
									<button class="btn btn-white" type="button">
										<i style="background-color: #000000;margin-top: 2px;"></i>
									</button>
								</span>
							</div>
						</div>
            <div class="form-group  inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Popup Close Button Size</h4>
							<input type="number" name="btn_size" class="form-control" placeholder="px" value="21">
						</div>
            <div class="row">
                <div class="col-6 align-self-center">
                    <div class="checkbox" style="margin:0;">
                      <input name="displayheader" id="displayheader_new" type="checkbox">
                      <label for="displayheader_new">
                        Display header
                      </label>
                    </div>
                </div>
                <div class="col-6 align-self-center">
      			        <div class="checkbox" style="margin:0;">
                      <input name="displayfooter" id="displayfooter_new" type="checkbox">
                      <label for="displayfooter_new">
                        Display footer
                      </label>
                    </div>
                </div>
            </div>
					</div>

          <div class="form-group col-md-12">

					<div class="radio switch">
						<input name="position" id="position1_new" type="radio" value="modal-top">
						<label for="position1_new">
							Top
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position2_new" type="radio" value="modal-dialog-centered" checked>
						<label for="position2_new">
							Center
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position3_new" type="radio" value="modal-bottom">
						<label for="position3_new">
							Bottom
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position4_new" type="radio" value="modal-top-right">
						<label for="position4_new">
							Top Right
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position5_new" type="radio" value="modal-top-left">
						<label for="position5_new">
							Top Left
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position6_new" type="radio" value="modal-bottom-right">
						<label for="position6_new">
							Bottom Right
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position7_new" type="radio" value="modal-bottom-left">
						<label for="position7_new">
							Bottom Left
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position8_new" type="radio" value="modal-right">
						<label for="position8_new">
							Right (Full height)
						</label>
					</div>
					<div class="radio switch">
						<input name="position" id="position9_new" type="radio" value="modal-left">
						<label for="position9_new">
							Left (Full height)
						</label>
					</div>
				</div>
			</div>
          </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" id="save" class="btn btn-info waves-effect waves-light">Add Popup</button>
			</div>
		</div>
	</div>
</div><!-- /.modal -->
<script src="{{ asset('assets/private/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.colVis.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('assets/private/popups/datatables.init.js') }}"></script>
<script>
$(document).ready(function() {
	$('.colorpicker-default').colorpicker({
                    format: 'hex'
                });
	$('.colorpicker-rgba').colorpicker();
	$('select[name="trigger"]').on('change', function(){
		$('.trigger-control').removeClass('in');
		$('.trigger-control.'+this.value).addClass('in');
	})
})
$(document).ready(function(){
	$('input[name="title"]').bind('paste change keyup', function(){
		var title = $(this).val().toLowerCase().replace(/[^a-z0-9\s]/gi, '-').replace(/ /g,'-');
		$(this).parents('form').find('input[name="class"]').val(title);
	})
	$('#save').on('click', function(){
		$('#createpageform input[type="text"]').removeClass('parsley-error');
		var parentEle = $(this).parents('.modal');
		parentEle.find('.help-block').remove();
		$.ajax({
            type: "POST",
            url: '{{URL::route('popups.store')}}',
            data: $("#createpageform").serialize(),
            success: function( msg ) {
				console.log(msg);
                if(msg == 'success'){
					$('#con-close-modal').find('.modal-body').prepend('<div class="alert alert-success"><strong>Success!</strong> Popup created.</div>');
					setTimeout(function(){
						window.location.href = "{{URL::route('popups.index')}}";
					}, 1000);
				}else{
					var obj = jQuery.parseJSON(msg);
					$.each(obj.errors, function(key,valueObj){
						parentEle.find('.'+key).append('<span class="help-block"><strong>'+valueObj+'</strong></span>');
					});
				}
            }
        });
	})
	$('.modal .update').on('click', function(){
		$('.modal .update input[type="text"]').removeClass('parsley-error');
		var btn = $(this);
		var parentEle = $(this).parents('.modal');
		parentEle.find('.help-block').remove();
		$.ajax({
            type: "POST",
            url: '{{URL::route('popups.store')}}',
            data: btn.parents('.modal').find('form').serialize(),
            success: function( msg ) {
                if(msg == 'success'){
					btn.parents('.modal').find('.modal-body').prepend('<div class="alert alert-success"><strong>Success!</strong> Popup updated.</div>');
					setTimeout(function(){
						window.location.href = "{{URL::route('popups.index')}}";
					}, 1000);
				}else{
					var obj = jQuery.parseJSON(msg);
					$.each(obj.errors, function(key,valueObj){
						parentEle.find('.'+key).append('<span class="help-block"><strong>'+valueObj+'</strong></span>');
					});
				}
            }
        });
	})
	$('.openmodal').on('click', function(){
		$('.modal').find('.alert').remove();
	})
	TableManageButtons.init();
})

</script>

@endsection
