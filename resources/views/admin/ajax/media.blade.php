
@foreach($uploads as $upload)
	<div class="imageholder">
		<div class="bug-thumbnail"><a href="javascript:void(0)" data-toggle="custommodal" data-target="#thumbnail-modal-{{ $upload->id }}"><img src="{{ getImage($upload->slug,'thumbnail') }}"></a></div>


		<div class="md-custom-modal md-custom-effect-1" id="thumbnail-modal-{{ $upload->id }}">
			<div class="md-custom-content">
					<h3>Media Detail</h3>
					<div class="md-custom-body">


						<div class="row" id="imageedit-{{ $upload->id }}">
							<div class="col-sm-8 text-center">
								<div class="imageedit">
									<img src="{{ getImage($upload->slug,'') }}" id="image-{{ $upload->id }}" class="cropper">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="details">
									File name: {{ $upload->title }}<br />
									File type: {{ $upload->type }}<br />
									Uploaded on: {{ date('F d, Y', strtotime($upload->created_at)) }}<br />
									File size: {{ getFileSize(getImage($upload->slug,'')) }}<br />
									Dimensions: {{ getImageDimension(getImage($upload->slug,'')) }}<br />
									<hr /><br />
									<table class="table-responsive">
										<tr>
											<td>
											<div class="input-group">
												<span class="input-group-addon">URL</span>
												<input type="text" class="form-control" value="{{ $upload->slug }}" readonly>
											</div>
											</td>
										</tr>
										<tr>
											<td>
											<p>&nbsp;</p>
											<p><a href="#" data-imageid="image-{{ $upload->id }}" data-image="{{ getImage($upload->slug,'') }}" data-imageholder="imageedit-{{ $upload->id }}" class="btn btn-sm btn-info editimage">Edit</a>&nbsp;<a href="{{ route('media.delete', $upload->id) }}" class="btn btn-sm btn-danger deleteMedia">Delete</a></p>
											</td>
										</tr>
									</table>
								</div>
								<div class="editdetail" style="display:none;padding: 15px;">
										<div class="card-box">
											<div class="preview preview-lg"></div>
											<div class="preview preview-md"></div>
											<div class="preview preview-sm"></div>
											<div class="clearfix"></div>
										</div>
										<div class="card-box">
											<ul class="buttons">
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="zoom" data-option="0.1"><span class="fa fa-search-plus"></span></a></li>
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="zoom" data-option="-0.1"><span class="fa fa-search-minus"></span></a></li>
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="moveH" data-option="-10"><span class="fa fa-arrow-left"></span></a></li>
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="moveH" data-option="10"><span class="fa fa-arrow-right"></span></a></li>
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="moveV" data-option="-10"><span class="fa fa-arrow-up"></span></a></li>
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="moveV" data-option="10"><span class="fa fa-arrow-down"></span></a></li>
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="rotate" data-option="-45"><span class="fa fa-rotate-left"></span></a></li>
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="rotate" data-option="45"><span class="fa fa-rotate-right"></span></a></li>
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="scaleX" data-option="-1"><span class="fa fa-arrows-h"></span></a></li>
												<li><a href="#" data-imageid="image-{{ $upload->id }}" data-method="scaleY" data-option="-1"><span class="fa fa-arrows-v"></span></a></li>
											</ul>
										</div>
										<div class="card-box">

											<div class="row">
												<div class="col-lg-6 m-t-10">
													<div class="input-group">
													<span class="input-group-addon">X</span>
													<input class="form-control dataX" type="text" placeholder="x">
													</div>
												</div>
												<div class="col-lg-6 m-t-10">
													<div class="input-group">
														<span class="input-group-addon">Y</span>
														<input class="form-control dataY" type="text" placeholder="y">
														</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6 m-t-10">
													<div class="input-group">
														<span class="input-group-addon">W</span>
														<input class="form-control dataWidth" type="text" placeholder="width">
														</div>
												</div>
												<div class="col-lg-6 m-t-10">
													<div class="input-group">
														<span class="input-group-addon">H</span>
														<input class="form-control dataHeight" type="text" placeholder="height">
														</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6 m-t-10">
													<div class="input-group">
														<span class="input-group-addon">Rotate</span>
														<input class="form-control dataRotate" type="text" placeholder="rotate">
														</div>
												</div>

											</div>

										</div>
										<a href="#" data-imageid="image-{{ $upload->id }}" data-image="{{ getImage($upload->slug,'') }}" data-imageholder="imageedit-{{ $upload->id }}" class="btn btn-sm btn-danger canceledit">Cancel</a>&nbsp;<button data-imageid="image-{{ $upload->id }}" data-image="{{ getImage($upload->slug,'') }}" data-imageholder="imageedit-{{ $upload->id }}" class="btn btn-sm btn-danger saveimg">Save</button>

								</div>
							</div>
						</div>


					</div>
					<div class="md-custom-footer">
					<button class="md-custom-close">Close me!</button>
					</div>
			</div>
		</div>
<div  data-target="#thumbnail-modal-{{ $upload->id }}" class="md-overlay  md-custom-overlay"></div>
	</div>
@endforeach
