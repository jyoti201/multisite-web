@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/private/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<style>

.table .thumbnail{
	width: 50px;
	height:50px;
	margin-bottom: 0;
}
.table .thumbnail img{
	max-width:100%;
}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Posts <span>All Posts</span><button class="btn btn-default waves-effect waves-light btn-sm pull-right openmodal" data-toggle="modal" data-target="#con-close-modal">Add New Posts</button></h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="navigation">
					<ul>
						<li><a href="{{URL::route('posts.index')}}" class="btn {{ !empty($trash)? 'btn-white': 'btn-default' }} waves-effect">All</a></li>
						<li><a href="{{URL::route('posts.trash')}}" class="btn {{ empty($trash)? 'btn-white': 'btn-default' }} waves-effect">Deleted</a></li>
					</ul>
				</div>
				<div class="card-box">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Featured Image</th>
								<th>Name</th>
								<th>Slug</th>
								<th>Created on</th>
							</tr>
						</thead>
						<tbody>
							@foreach (json_decode($posts) as $page)
							<tr>
								<td>
									@if(!empty($page->featured_image))
										<div class="thumbnail"><img src="{{ $page->featured_image }}"></div>
									@else

									@endif
								</td>
								<td>
								{{ $page->title }}
								<ul class="buttons">
									@if(!empty($trash))
									<li><a href="{{URL::route('posts.preview-trash', $page->id)}}" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
									<li><a href="{{ route('posts.restore', $page->id) }}"> <i class="fa fa-arrow-left"></i> </a></li>
									<li><a href="{{ route('posts.forcedelete', $page->id) }}"> <i class="fa fa-remove"></i> </a></li>
									@else
									<li><a href="{{URL::route('posts.edit', $page->id)}}"><i class="fas fa-pencil-alt"></i></a></li>
									<li><a href="{{URL::route('posts.preview', $page->id)}}" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
									<li><a href="{{URL::route('posts.clone', $page->id)}}"> <i class="fa fa-clone"></i> </a></li>
									<li><a href="#" data-toggle="modal" data-target="#update-page-{{ $page->id }}"> <i class="fa fa-wrench"></i> </a></li>
									<li>
										<a href="javascript:void(0);" onclick="$(this).find('form').submit();"> <i class="fa fa-remove"></i>
											<form action="{{ route('posts.destroy', $page->id) }}" method="POST">
											{{ csrf_field() }}
											<input type="hidden" name="_method" value="DELETE">
											</form>
										</a>
									</li>
									@endif
								</ul>
								</td>
								<td>{{ $page->slug }}</td>
								<td width="180px">Published<br />{{ $page->created_at }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
    </div>
</div>
@foreach (json_decode($posts) as $page)
<div id="update-page-{{ $page->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Update {{ $page->title }}</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
					<form method="POST" class="updatepageform" action="">
					{{ csrf_field() }}
					<input type="hidden" name="postid" value="{{ $page->id }}">
					<div class="row">
					<div class="col-sm-6">
						<div class="form-group inlinelabel title">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Title</h4>
							<input type="text" name="title" class="form-control" placeholder="" value="{{ $page->title }}" required>
						</div>
						<div class="form-group inlinelabel slug">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Slug</h4>
							<input type="text" name="slug" class="form-control" value="{{ $page->slug }}" placeholder="" >
						</div>
						<div class="form-group inlinelabel">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Excerpt</h4>
							<textarea name="excerpt" class="form-control">{{ !empty($page->excerpt)?$page->excerpt:'Short info from your post' }}</textarea>
						</div>


					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Featured Image</h4>





							<div class="choosemedia">
									<div class="addedimage">
												<div class="image {{ !empty($page->featured_image) ? 'image-has' : '' }}"  style="{{ !empty($page->featured_image)?'background-image:url('.$page->featured_image.')':'' }}">
												<a href="#" class="removeimage">Remove</a>
												<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Image</h5></i> </a>
											</div>
											<input type="hidden" name="featured_image" value="{{ !empty($page->featured_image) ?$page->featured_image:'' }}">
									</div>
								</div>

						</div>

						<div class="form-group">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Categories</h4>
							@if(sizeof($categories)>0)
										@foreach($categories as $category)
										<div class="checkbox">
											<input name="categories[]" id="{{ $page->id }}checkbox{{ $category->slug }}" type="checkbox" value="{{ $category->slug }}" {{ !empty($categories) && checkCategoryExists($category->slug, $page->id) ? 'checked' : '' }}>
											<label for="{{ $page->id }}checkbox{{ $category->slug }}">
												{{ $category->title }}
											</label>
										</div>
										@endforeach
							@else
									No Categories
							@endif
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<div class="checkbox">
								<input name="sidebaroption" id="checkbox1" type="checkbox" data-toggle="collapse" data-target="#sidebar{{ $page->id }}" {{ !empty($page->sidebar) ? 'checked' : '' }}>
								<label for="checkbox1">
									Enable Sidebar
								</label>
							</div>
						</div>

						<div class="form-group collapse {{ !empty($page->sidebar) ? 'in' : '' }}" id="sidebar{{ $page->id }}">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Select Sidebar</h4>
							<select name="sidebar"   data-style="btn-white" class="form-control selectpicker">
								<option value="left" {{ $page->sidebar == 'left' ? 'selected':'' }}>Left Sidebar</option>
								<option value="right" {{ $page->sidebar == 'right' ? 'selected' :'' }}>Right Sidebar</option>
							</select>
						</div>
						<div class="panel-group" id="accordion{{ $page->id }}">
						  <div class="panel panel-default">
							<div class="panel-heading">
							  <h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion{{ $page->id }}" href="#collapse{{ $page->id }}" class="collapsed">
								Meta Information</a>
							  </h4>
							</div>
							<div id="collapse{{ $page->id }}" class="panel-collapse collapse">
							  <div class="panel-body">
								<div class="form-group inlinelabel">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Meta Title</h4>
									<input type="text" name="meta_title" class="form-control" value="{{ $page->meta_title }}" placeholder="" >
								</div>
								<div class="form-group inlinelabel">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Meta Description</h4>
									<textarea name="meta_description" class="form-control">{{ $page->meta_description }}</textarea>
								</div>
								<div class="form-group inlinelabel">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Meta Keywords</h4>
									<input type="text" name="meta_keywords" class="form-control" value="{{ $page->meta_keywords }}" placeholder="" >
								</div>
							  </div>
							</div>
						  </div>
						</div>
					</div>
</div>
					</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info waves-effect waves-light update">Update Posts</button>
			</div>
		</div>
	</div>
</div><!-- /.modal -->
@endforeach
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Create New Posts</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
					<form method="POST" id="createpageform" action="">
					{{ csrf_field() }}
					<div class="row">
							<div class="col-md-6">
								<div class="form-group inlinelabel title">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Title</h4>
									<input type="text" name="title" class="form-control" placeholder="" required>
								</div>
								<div class="form-group inlinelabel slug">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Slug</h4>
									<input type="text" name="slug" class="form-control" placeholder="" >
								</div>
								<div class="form-group inlinelabel">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Excerpt</h4>
									<textarea name="excerpt" class="form-control">Short info from your post</textarea>
								</div>

							</div>
							<div class="col-md-6">
								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Featured Image</h4>


									<div class="choosemedia">
											<div class="addedimage">
														<div class="image">
														<a href="#" class="removeimage">Remove</a>
														<a href="#" data-toggle="custommodal" data-target="#media-select"><i class="fa fa-camera"> <h5>Upload Image</h5></i> </a>
													</div>
													<input type="hidden" name="featured_image" value="">
											</div>
										</div>


								</div>

								<div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Categories</h4>
									@if(sizeof($categories)>0)
												@foreach($categories as $category)
												<div class="checkbox">
													<input name="categories[]" id="checkbox_{{ $category->slug }}" type="checkbox" value="{{ $category->slug }}">
													<label for="checkbox_{{ $category->slug }}">
														{{ $category->title }}
													</label>
												</div>
												@endforeach
									@else
											No Categories
									@endif
								</div>
							</div>

					<div class="col-md-12">
						<div class="form-group">
							<div class="checkbox">
								<input name="sidebaroption" id="checkbox0" type="checkbox" data-toggle="collapse" data-target="#sidebar">
								<label for="checkbox0">
									Enable Sidebar
								</label>
							</div>
						</div>
						<div class="form-group collapse" id="sidebar">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Select Sidebar</h4>
							<select name="sidebar"   data-style="btn-white" class="form-control selectpicker">
								<option value="left">Left Sidebar</option>
								<option value="right">Right Sidebar</option>
							</select>
						</div>
						<div class="panel-group" id="accordion">
						  <div class="panel panel-default">
							<div class="panel-heading">
							  <h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse" class="collapsed">
								Meta Information</a>
							  </h4>
							</div>
							<div id="collapse" class="panel-collapse collapse">
							  <div class="panel-body">
								<div class="form-group inlinelabel">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Meta Title</h4>
									<input type="text" name="meta_title" class="form-control" value="" placeholder="" >
								</div>
								<div class="form-group inlinelabel">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Meta Description</h4>
									<textarea name="meta_description" class="form-control"></textarea>
								</div>
								<div class="form-group inlinelabel">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Meta Keywords</h4>
									<input type="text" name="meta_keywords" class="form-control" value="" placeholder="" >
								</div>
							  </div>
							</div>
						  </div>
						</div>
					</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" id="save" class="btn btn-info waves-effect waves-light">Add Posts</button>
			</div>
		</div>
	</div>
</div><!-- /.modal -->


@include('admin._partials.popupuploader')


<script src="{{ asset('assets/private/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.colVis.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('assets/private/pages/datatables.init.js') }}"></script>
<script>
$(document).ready(function(){
	$('input[name="title"]').bind('paste change keyup', function(){
		var title = $(this).val().toLowerCase().replace(/[^a-z0-9\s]/gi, '-').replace(/ /g,'-');
		$(this).parents('form').find('input[name="slug"]').val(title);
	})
	$('#save').on('click', function(){
		$('#createpageform input[type="text"]').removeClass('parsley-error');
		var parentEle = $(this).parents('.modal');
		parentEle.find('.help-block').remove();
		$.ajax({
            type: "POST",
            url: '{{URL::route('posts.store')}}',
            data: $("#createpageform").serialize(),
            success: function( msg ) {
				console.log(msg);
                if(msg == 'success'){
					$('#con-close-modal').find('.modal-body').prepend('<div class="alert alert-success"><strong>Success!</strong> Posts created.</div>');
					setTimeout(function(){
						window.location.href = "{{URL::route('posts.index')}}";
					}, 1000);
				}else{
					var obj = jQuery.parseJSON(msg);
					$.each(obj.errors, function(key,valueObj){
						parentEle.find('.'+key).append('<span class="help-block"><strong>'+valueObj+'</strong></span>');
					});
				}
            }
        });
	})
	$('.modal .update').on('click', function(){
		$('.modal .update input[type="text"]').removeClass('parsley-error');
		var btn = $(this);
		var parentEle = $(this).parents('.modal');
		parentEle.find('.help-block').remove();
		$.ajax({
            type: "POST",
            url: '{{URL::route('posts.store')}}',
            data: btn.parents('.modal').find('form').serialize(),
            success: function( msg ) {
                if(msg == 'success'){
					btn.parents('.modal').find('.modal-body').prepend('<div class="alert alert-success"><strong>Success!</strong> Posts updated.</div>');
					setTimeout(function(){
						window.location.href = "{{URL::route('posts.index')}}";
					}, 1000);
				}else{
					var obj = jQuery.parseJSON(msg);
					$.each(obj.errors, function(key,valueObj){
						parentEle.find('.'+key).append('<span class="help-block"><strong>'+valueObj+'</strong></span>');
					});
				}
            }
        });
	})
	$('.openmodal').on('click', function(){
		$('.modal').find('.alert').remove();
	})
	$('.addedimage a').on('click', function(){
		jQuery('.choosemedia').removeClass('selected');
		$(this).parents('.choosemedia').toggleClass('selected');
	})
	$('#media-select').on('click','.thumbnail a', function(){
		var image =$(this).attr('href');
		$('.choosemedia.selected').find('.image').css('background-image', 'url('+image+')');
		$('.choosemedia.selected').find('.image').addClass('image-has');
		$('.choosemedia.selected').find('.image').next('input[type="hidden"]').val(image);
		$('#media-select').removeClass('md-custom-show');
		return false;
	})
	$('.removeimage').click(function(){
		$(this).parents('.image').css('background-image', 'none');
		$(this).parents('.image').removeClass('image-has');
		$(this).parents('.image').next('input[type="hidden"]').val('');
		return false;
	})
	TableManageButtons.init();
})
</script>

@endsection
