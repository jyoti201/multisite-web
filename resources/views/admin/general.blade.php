@extends('layouts.app')

@section('extra')



@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Settings <span>General</h4>
			</div>
			@if (\Session::has('success'))
			<div class="col-sm-12 m-t-15">
					<div class="alert alert-success">
						{!! \Session::get('success') !!}
					</div>
			</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="card-box">
          <form  method="post" action="{{ route("settings.saveGeneral") }}">
              {{ csrf_field() }}
					<div class="row">
						<div class="col-6">
								  <div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Site Title:</h4>
									<input type="text" class="form-control" id="site_title" name="site_title" value="{{ !empty($savedsettings['site_title'])? $savedsettings['site_title'] : '' }}">
								  </div>
								  <div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Set Homepage:</h4>
									<select class="selectpicker" data-live-search="true"  data-style="btn-white" name="home">
										<option value="">Select Page</option>
										@foreach($pages as $page)
										<option value="{{ $page->id }}" @if(!empty($savedsettings['home']) && $savedsettings['home'] == $page->id) {{ "selected" }} @endif>{{ $page->title }}</option>
										@endforeach
									</select>
								  </div>
								  <div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Set 404 Page:</h4>
									<select class="selectpicker" data-live-search="true"  data-style="btn-white" name="404">
										<option value="">Select Page</option>
										@foreach($pages as $page)
										<option value="{{ $page->id }}" @if(!empty($savedsettings['404']) && $savedsettings['404'] == $page->id) {{ "selected" }} @endif>{{ $page->title }}</option>
										@endforeach
									</select>
								  </div>
								  <div class="form-group">
									<h4 class="text-muted m-b-15 m-t-15 font-15">Set Blog Page:</h4>
									<select class="selectpicker" data-live-search="true"  data-style="btn-white" name="blog">
										<option value="">Select Page</option>
										@foreach($pages as $page)
										<option value="{{ $page->slug }}" @if(!empty($savedsettings['blog']) && $savedsettings['blog'] == $page->slug) {{ "selected" }} @endif>{{ $page->title }}</option>
										@endforeach
									</select>
								  </div>

								  <button type="submit" class="btn btn-default">Update</button>
								  <input type="hidden" name="settingType" value="general">
						</div>
            <div class="col-6">
                  <div class="form-group">
                  <h4 class="text-muted m-b-15 m-t-15 font-15">Meta Title Format:</h4>
                  <input type="text" class="form-control" id="meta_title_format" name="meta_title_format" value="{{ !empty($savedsettings['meta_title_format'])? $savedsettings['meta_title_format'] : '' }}">
                  </div>

                  <div class="form-group">
                  <h4 class="text-muted m-b-15 m-t-15 font-15">Global Meta Title:</h4>
                  <input type="text" class="form-control" id="global_meta_title" name="global_meta_title" value="{{ !empty($savedsettings['global_meta_title'])? $savedsettings['global_meta_title'] : '' }}">
                  </div>

                  <div class="form-group">
                  <h4 class="text-muted m-b-15 m-t-15 font-15">Global Meta Description:</h4>
                  <textarea class="form-control" id="global_meta_desc" name="global_meta_desc">{{ !empty($savedsettings['global_meta_desc'])? $savedsettings['global_meta_desc'] : '' }}</textarea>
                  </div>

                  <div class="form-group">
                  <h4 class="text-muted m-b-15 m-t-15 font-15">Global Meta Keywords:</h4>
                  <input type="text" class="form-control" id="global_meta_keywords" name="global_meta_keywords" value="{{ !empty($savedsettings['global_meta_keywords'])? $savedsettings['global_meta_keywords'] : '' }}">
                  </div>
            </div>
					</div>
          </form>
				</div>
			</div>
    </div>
</div>
@endsection
