@extends('layouts.app')

@section('extra')

<link href="{{ asset('assets/private/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/private/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Sections <span>All Sections</span><button class="btn btn-default waves-effect waves-light btn-sm pull-right openmodal" data-toggle="modal" data-target="#con-close-modal">Add New Section</button></h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">
				<div class="navigation">
					<ul>
						<li><a href="{{URL::route('sections.index')}}" class="btn {{ !empty($trash)? 'btn-white': 'btn-default' }} waves-effect">All</a></li>
						<li><a href="{{URL::route('sections.trash')}}" class="btn {{ empty($trash)? 'btn-white': 'btn-default' }} waves-effect">Deleted</a></li>
					</ul>
				</div>
				<div class="card-box">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Name</th>
								<th>Shortcode</th>
								<th>Created on</th>
							</tr>
						</thead>
						<tbody>
							@foreach (json_decode($sections) as $section)
							<tr>
								<td>
								{{ $section->title }}
								<ul class="buttons">
									@if(!empty($trash))
									<li><a href="{{URL::route('sections.preview-trash', $section->id)}}" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
									<li><a href="{{ route('sections.restore', $section->id) }}"> <i class="fa fa-arrow-left"></i> </a></li>
									<li><a href="{{ route('sections.forcedelete', $section->id) }}"> <i class="fa fa-remove"></i> </a></li>
									@else
									<li><a href="{{URL::route('sections.edit', $section->id)}}"><i class="fas fa-pencil-alt"></i></a></li>
									<li><a href="{{URL::route('sections.preview', $section->id)}}" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
									<li><a href="{{URL::route('sections.clone', $section->id)}}"><i class="fa fa-clone"></i></a></li>
									<li><a href="#" data-toggle="modal" data-target="#update-page-{{ $section->id }}"> <i class="fa fa-wrench"></i> </a></li>
									<li><a href="javascript:void(0);" onclick="$(this).find('form').submit();"> <i class="fa fa-remove"></i>
										<form action="{{ route('sections.destroy', $section->id) }}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE">
										</form>
									</a>
									</li>
									@endif
								</ul>
								</td>
								<td>{{ $section->shortcode }}</td>
								<td width="180px">Published<br />{{ $section->created_at }}</td>

							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
    </div>
</div>
@foreach (json_decode($sections) as $section)
<div id="update-page-{{ $section->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
        <h4 class="modal-title">Update {{ $section->title }}</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
        <form method="POST" class="updatepageform" action="">
        {{ csrf_field() }}
        <input type="hidden" name="sectionid" value="{{ $section->id }}">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group inlinelabel title">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Title</h4>
							<input type="text" name="title" class="form-control" placeholder="" value="{{ $section->title }}" required>
						</div>
					</div>
				</div>
        </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info waves-effect waves-light update">Update Section</button>
			</div>
		</div>
	</div>
</div><!-- /.modal -->
@endforeach
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Create New Section</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
        <form method="POST" id="createpageform" action="">
        {{ csrf_field() }}
				<div class="row">
					<div class="col-md-12">
						<div class="form-group inlinelabel title">
							<h4 class="text-muted m-b-15 m-t-15 font-15">Title</h4>
							<input type="text" name="title" class="form-control" placeholder="" required>
						</div>
					</div>
				</div>
        </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" id="save" class="btn btn-info waves-effect waves-light">Add Section</button>
			</div>
		</div>
	</div>
</div><!-- /.modal -->
<script src="{{ asset('assets/private/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.colVis.js') }}"></script>
<script src="{{ asset('assets/private/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('assets/private/pages/datatables.init.js') }}"></script>
<script>
$(document).ready(function(){
	$('#save').on('click', function(){
		$('#createpageform input[type="text"]').removeClass('parsley-error');
		var parentEle = $(this).parents('.modal');
		parentEle.find('.help-block').remove();
		$.ajax({
            type: "POST",
            url: '{{URL::route('sections.store')}}',
            data: $("#createpageform").serialize(),
            success: function( msg ) {
                if(msg == 'success'){
        					$('#con-close-modal').find('.modal-body').prepend('<div class="alert alert-success"><strong>Success!</strong> Section created.</div>');
        					setTimeout(function(){
        						window.location.href = "{{URL::route('sections.index')}}";
        					}, 1000);
        				}else{
        					var obj = jQuery.parseJSON(msg);
        					$.each(obj.errors, function(key,valueObj){
        						parentEle.find('.'+key).append('<span class="help-block"><strong>'+valueObj+'</strong></span>');
        					});
        				}
            }
        });
	})
	$('.modal .update').on('click', function(){
		$('.modal .update input[type="text"]').removeClass('parsley-error');
		var btn = $(this);
		var parentEle = $(this).parents('.modal');
		parentEle.find('.help-block').remove();
		$.ajax({
            type: "POST",
            url: '{{URL::route('sections.store')}}',
            data: btn.parents('.modal').find('form').serialize(),
            success: function( msg ) {
                if(msg == 'success'){
					btn.parents('.modal').find('.modal-body').prepend('<div class="alert alert-success"><strong>Success!</strong> Section updated.</div>');
					setTimeout(function(){
						window.location.href = "{{URL::route('sections.index')}}";
					}, 1000);
				}else{
					var obj = jQuery.parseJSON(msg);
					$.each(obj.errors, function(key,valueObj){
						parentEle.find('.'+key).append('<span class="help-block"><strong>'+valueObj+'</strong></span>');
					});
				}
            }
        });
	})
	$('.openmodal').on('click', function(){
		$('.modal').find('.alert').remove();
	})
	TableManageButtons.init();
})
</script>

@endsection
