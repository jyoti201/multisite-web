@extends('layouts.app')

@section('extra')



<style>
.modal-dialog{
  width: 100%;
  max-width: 1000px;
}
.stickycontainer{
	position: relative;
}
#preview{
    text-align: center;
    height: 200px;
    background: #eaeaea;
    position: relative;
}
#preview .activebutton{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
</style>
@endsection

@section('content')
<form method="post" action="{{ URL::route("buttons.save") }}">
{{csrf_field()}}
<input type="hidden" name="id" value="{{ $button->id }}">
<input type="hidden" name="button_class" value="{{ !empty($settings['button_class'])?$settings['button_class']:'' }}">
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Edit Button<span>{{ $button->name }}</span></h4>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@elseif (\Session::has('errors'))
			<div class="col-sm-12 m-t-15">
			<div class="alert alert-danger">
				{!! \Session::get('error') !!}
			</div>
			</div>
			@endif
			<div class="col-sm-8 m-t-15 button_builder">
				<div class="card-box">










					<div class="row">
							<div class="col-md-12">
												<div class="text-left">
															<label class="switch showhide" data-trigger-show="hover" data-trigger-hide="normal">
																<input type="checkbox">
																<span class="slider red"></span><span class="label">Hover</span>
															</label>
												</div>
												<div class="state normal">
														<div class="form-field m-t-10 ">
															<h4 class="text-muted m-b-15 m-t-15 font-15">Background Color</h4>
															<div class="row">
																<div class="col-md-12">
																	<label class="switch showhide" data-trigger-show="gradient_bg" data-trigger-hide="solid_bg">
																		<input type="checkbox" class="input" id="builder_gradientbg" name="builder_gradientbg" {{ !empty($settings['builder_gradientbg']) && $settings['builder_gradientbg']!='false'?'checked':'' }}>
																		<span class="slider red"></span><span class="label">Gradient</span>
																	</label>
																</div>
															</div>
															<div class="row solid_bg m-t-10" style="{{ !empty($settings['builder_gradientbg']) && $settings['builder_gradientbg']!='false'?'display: none;':'display: block;' }}">
																<div class="col-md-12">
																	<div data-color-format="rgba" data-color="{{ !empty($settings['builder_background-color'])?$settings['builder_background-color']:'#cccccc' }}" class="colorpicker-rgba input-group colorpicker-element">
																		<input type="text" value="{{ !empty($settings['builder_background-color'])?$settings['builder_background-color']:'#cccccc' }}" id="builder_background-color" name="builder_background-color" class="form-control">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($settings['builder_background-color'])?$settings['builder_background-color']:'#cccccc' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
																</div>
															</div>
															<div class="row gradient_bg collapse m-t-10"  style="{{ !empty($settings['builder_gradientbg']) && $settings['builder_gradientbg']!='false'?'display: block;':'display: none;' }}">
																<div class="col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">From</h4>
																	<div data-color-format="rgba" data-color="{{ !empty($settings['builder_background-color-from'])?$settings['builder_background-color-from']:'#cccccc' }}" class="colorpicker-rgba input-group colorpicker-element">
																		<input type="text" value="{{ !empty($settings['builder_background-color-from'])?$settings['builder_background-color-from']:'#cccccc' }}" id="builder_background-color-from" name="builder_background-color-from" class="form-control">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($settings['builder_background-color-from'])?$settings['builder_background-color-from']:'#cccccc' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
																</div>
																<div class="col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">To</h4>
																	<div data-color-format="rgba" data-color="{{ !empty($settings['builder_background-color-to'])?$settings['builder_background-color-to']:'#cccccc' }}" class="colorpicker-rgba input-group colorpicker-element">
																		<input type="text" value="{{ !empty($settings['builder_background-color-to'])?$settings['builder_background-color-to']:'#cccccc' }}" id="builder_background-color-to" name="builder_background-color-to" class="form-control">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($settings['builder_background-color-to'])?$settings['builder_background-color-to']:'#cccccc' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Text Color</h4>
																	<div data-color-format="rgba" data-color="{{ !empty($settings['builder_color'])?$settings['builder_color']:'#000000' }}" class="colorpicker-rgba input-group colorpicker-element">
																		<input type="text" value="{{ !empty($settings['builder_color'])?$settings['builder_color']:'#000000' }}" id="builder_color" name="builder_color" class="form-control input">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($settings['builder_color'])?$settings['builder_color']:'#000000' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Font Family</h4>
																	<select id="builder_font-family"  name="builder_font-family" data-style="btn-white" class="form-control input selectpicker">
																		<option value=">Default</option>
									@if(!empty(getSetting('imported_font_name','styles')))
										@foreach(getSetting('imported_font_name','styles') as $val)
											<option value="{{ $val }}" {{ !empty($settings['builder_font-family']) && $settings['builder_font-family']==$val?'selected':'' }}>{{ $val }}</option>
										@endforeach
									@endif
																	</select>
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Font Size (px)</h4>
																	<input type="number" id="builder_font-size" name="builder_font-size" value="{{ !empty($settings['builder_font-size'])?$settings['builder_font-size']:'' }}" min="0" class="form-control input">
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Line Height (px)</h4>
																	<input type="number" id="builder_line-height" name="builder_line-height" value="{{ !empty($settings['builder_line-height'])?$settings['builder_line-height']:'' }}" min="0" class="form-control input">
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Letter Spacing (px)</h4>
																	<input type="number" id="builder_letter-spacing" name="builder_letter-spacing" value="{{ !empty($settings['builder_letter-spacing'])?$settings['builder_letter-spacing']:'' }}" min="0" class="form-control input">
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Word Spacing (px)</h4>
																	<input type="number" id="builder_word-spacing" name="builder_word-spacing" value="{{ !empty($settings['builder_word-spacing'])?$settings['builder_word-spacing']:'' }}" min="0" class="form-control input">
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Text Transform</h4>
																	<select id="builder_text-transform" name="builder_text-transform" data-style="btn-white" class="form-control input selectpicker">
																		<option value="">None</option>
																		<option value="uppercase" {{ !empty($settings['builder_text-transform']) && $settings['builder_text-transform']=='uppercase'?'selected':'' }}>Uppercase</option>
																		<option value="lowercase" {{ !empty($settings['builder_text-transform']) && $settings['builder_text-transform']=='lowercase'?'selected':'' }}>Lowercase</option>
																	</select>
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Font Weight (px)</h4>
																	<select id="builder_font-weight" name="builder_font-weight" data-style="btn-white" class="form-control input selectpicker">
																		<option value=">Default</option>
																		<option value="300" {{ !empty($settings['builder_font-weight']) && $settings['builder_font-weight']=='300'?'selected':'' }}>300 (Light)</option>
																		<option value="400" {{ !empty($settings['builder_font-weight']) && $settings['builder_font-weight']=='400'?'selected':'' }}>400 (Regular)</option>
																		<option value="600" {{ !empty($settings['builder_font-weight']) && $settings['builder_font-weight']=='600'?'selected':'' }}>600 (Semi Bold)</option>
																		<option value="700" {{ !empty($settings['builder_font-weight']) && $settings['builder_font-weight']=='700'?'selected':'' }}>700 (Bold)</option>
																	</select>
																</div>

								<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Border Style</h4>
																	<select id="builder_border-style" name="builder_border-style" data-style="btn-white" class="form-control input selectpicker">
																		<option value="none">None</option>
																		<option value="dotted" {{ !empty($settings['builder_border-style']) && $settings['builder_border-style']=='dotted'?'selected':'' }}>Dotted</option>
																		<option value="dashed" {{ !empty($settings['builder_border-style']) && $settings['builder_border-style']=='dashed'?'selected':'' }}>Dashed</option>
																		<option value="solid" {{ !empty($settings['builder_border-style']) && $settings['builder_border-style']=='solid'?'selected':'' }}>Solid</option>
																		<option value="double" {{ !empty($settings['builder_border-style']) && $settings['builder_border-style']=='double'?'selected':'' }}>Double</option>
																		<option value="groove" {{ !empty($settings['builder_border-style']) && $settings['builder_border-style']=='groove'?'selected':'' }}>Groove</option>
																		<option value="ridge" {{ !empty($settings['builder_border-style']) && $settings['builder_border-style']=='ridge'?'selected':'' }}>Ridge</option>
																		<option value="inset" {{ !empty($settings['builder_border-style']) && $settings['builder_border-style']=='inset'?'selected':'' }}>Inset</option>
																		<option value="outset" {{ !empty($settings['builder_border-style']) && $settings['builder_border-style']=='outset'?'selected':'' }}>Outset</option>
																		<option value="hidden" {{ !empty($settings['builder_border-style']) && $settings['builder_border-style']=='hidden'?'selected':'' }}>Hidden</option>
																	</select>
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Border Color</h4>
																	<div data-color-format="rgba" data-color="{{ !empty($settings['builder_border-color'])?$settings['builder_border-color']:'#000000' }}" class="colorpicker-rgba input-group colorpicker-element">
																		<input type="text" value="{{ !empty($settings['builder_border-color'])?$settings['builder_border-color']:'#000000' }}" id="builder_border-color" name="builder_border-color" class="form-control input">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($settings['builder_border-color'])?$settings['builder_border-color']:'#000000' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
																</div>
														</div>
														<div class="form-field m-t-10 ">
															<h4 class="text-muted m-b-15 m-t-15 font-15">Border Radius (px)</h4>
															<div class="row">
																<div class="col-sm-3">
																	<input type="number" id="builder_border-top-left-radius" value="{{ !empty($settings['builder_border-top-left-radius'])?$settings['builder_border-top-left-radius']:'' }}" name="builder_border-top-left-radius" min="0" class="form-control input" placeholder="Top Left">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_border-top-right-radius" value="{{ !empty($settings['builder_border-top-right-radius'])?$settings['builder_border-top-right-radius']:'' }}" name="builder_border-top-right-radius" min="0" class="form-control input" placeholder="Top Right">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_border-bottom-left-radius" value="{{ !empty($settings['builder_border-bottom-left-radius'])?$settings['builder_border-bottom-left-radius']:'' }}" name="builder_border-bottom-left-radius" min="0" class="form-control input" placeholder="Bottom Left">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_border-bottom-right-radius" value="{{ !empty($settings['builder_border-bottom-right-radius'])?$settings['builder_border-bottom-right-radius']:'' }}" name="builder_border-bottom-right-radius" min="0" class="form-control input" placeholder="Bottom Right">
																</div>
															</div>
														</div>
														<div class="form-field m-t-10 ">
															<h4 class="text-muted m-b-15 m-t-15 font-15">Border Width (px)</h4>
															<div class="row">
																<div class="col-sm-3">
																	<input type="number" id="builder_border-left-width" value="{{ !empty($settings['builder_border-left-width'])?$settings['builder_border-left-width']:'' }}" name="builder_border-left-width" min="0" class="form-control input" placeholder="Border Left">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_border-right-width" value="{{ !empty($settings['builder_border-right-width'])?$settings['builder_border-right-width']:'' }}" name="builder_border-right-width" min="0" class="form-control input" placeholder="Border Right">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_border-top-width" value="{{ !empty($settings['builder_border-top-width'])?$settings['builder_border-top-width']:'' }}" name="builder_border-top-width" min="0" class="form-control input" placeholder="Border Top">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_border-bottom-width" value="{{ !empty($settings['builder_border-bottom-width'])?$settings['builder_border-bottom-width']:'' }}" name="builder_border-bottom-width" min="0" class="form-control input" placeholder="Border Bottom">
																</div>
															</div>
														</div>
														<div class="form-field m-t-10 ">
															<h4 class="text-muted m-b-15 m-t-15 font-15">Padding (px)</h4>
															<div class="row">
																<div class="col-sm-3">
																	<input type="number" id="builder_padding-left" value="{{ !empty($settings['builder_padding-left'])?$settings['builder_padding-left']:'' }}" name="builder_padding-left" min="0" class="form-control input" placeholder="Left">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_padding-right" value="{{ !empty($settings['builder_padding-right'])?$settings['builder_padding-right']:'' }}" name="builder_padding-right" min="0" class="form-control input" placeholder="Right">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_padding-top" value="{{ !empty($settings['builder_padding-top'])?$settings['builder_padding-top']:'' }}" name="builder_padding-top" min="0" class="form-control input" placeholder="Top">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_padding-bottom" value="{{ !empty($settings['builder_padding-bottom'])?$settings['builder_padding-bottom']:'' }}" name="builder_padding-bottom" min="0" class="form-control input" placeholder="Bottom">
																</div>
															</div>
														</div>
													</div>





													<div class="state hover collapse">
														<div class="form-field m-t-10 " style="margin-top: 30px;">
																<a href="#" id="copystyle" class="btn btn-primary">Copy Normal State CSS</a>
														 </div>
														<div class="form-field m-t-10 ">
															<h4 class="text-muted m-b-15 m-t-15 font-15">Background Color</h4>
															<div class="row">
																<div class="col-md-12">
																	<label class="switch showhide" data-trigger-show="hover_gradient_bg" data-trigger-hide="hover_solid_bg">
																		<input type="checkbox" class="input" id="builder_hover_gradientbg" name="builder_hover_gradientbg" {{ !empty($settings['builder_hover_gradientbg']) && $settings['builder_hover_gradientbg']!='false'?'checked':'' }}>
																		<span class="slider red"></span><span class="label">Gradient</span>
																	</label>
																</div>
															</div>
															<div class="row hover_solid_bg m-t-10" style="{{ !empty($settings['builder_hover_gradientbg']) && $settings['builder_hover_gradientbg']!='false'?'display: none;':'display: block;' }}">
																<div class="col-md-12">
																	<div data-color-format="rgba" data-color="{{ !empty($settings['builder_hover_background-color'])?$settings['builder_hover_background-color']:'#cccccc' }}" class="colorpicker-rgba input-group colorpicker-element">
																		<input type="text" value="{{ !empty($settings['builder_hover_background-color'])?$settings['builder_hover_background-color']:'#cccccc' }}" id="builder_hover_background-color" name="builder_hover_background-color" class="form-control">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($settings['builder_hover_background-color'])?$settings['builder_hover_background-color']:'#cccccc' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
																</div>
															</div>
															<div class="row hover_gradient_bg collapse m-t-10"  style="{{ !empty($settings['builder_hover_gradientbg']) && $settings['builder_hover_gradientbg']!='false'?'display: block;':'display: none;' }}">
																<div class="col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">From</h4>
																	<div data-color-format="rgba" data-color="{{ !empty($settings['builder_hover_background-color-from'])?$settings['builder_hover_background-color-from']:'#cccccc' }}" class="colorpicker-rgba input-group colorpicker-element">
																		<input type="text" value="{{ !empty($settings['builder_hover_background-color-from'])?$settings['builder_hover_background-color-from']:'#cccccc' }}" id="builder_hover_background-color-from" name="builder_hover_background-color-from" class="form-control">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($settings['builder_hover_background-color-from'])?$settings['builder_hover_background-color-from']:'#cccccc' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
																</div>
																<div class="col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">To</h4>
																	<div data-color-format="rgba" data-color="{{ !empty($settings['builder_hover_background-color-to'])?$settings['builder_hover_background-color-to']:'#cccccc' }}" class="colorpicker-rgba input-group colorpicker-element">
																		<input type="text" value="{{ !empty($settings['builder_hover_background-color-to'])?$settings['builder_hover_background-color-to']:'#cccccc' }}" id="builder_hover_background-color-to" name="builder_hover_background-color-to" class="form-control">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($settings['builder_hover_background-color-to'])?$settings['builder_hover_background-color-to']:'#cccccc' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Text Color</h4>
																	<div data-color-format="rgba" data-color="{{ !empty($settings['builder_hover_color'])?$settings['builder_hover_color']:'#000000' }}" class="colorpicker-rgba input-group colorpicker-element">
																		<input type="text" value="{{ !empty($settings['builder_hover_color'])?$settings['builder_hover_color']:'#000000' }}" id="builder_hover_color" name="builder_hover_color" class="form-control input">
																		<span class="input-group-btn add-on">
																			<button class="btn btn-white" type="button">
																				<i style="background-color: {{ !empty($settings['builder_hover_color'])?$settings['builder_hover_color']:'#000000' }};margin-top: 2px;"></i>
																			</button>
																		</span>
																	</div>
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Font Family</h4>
																	<select id="builder_hover_font-family" name="builder_hover_font-family" data-style="btn-white" class="form-control input selectpicker">
																		<option value=">Default</option>
																 @if(!empty(getSetting('imported_font_name','styles')))
																		@foreach(getSetting('imported_font_name','styles') as $val)
																			<option value="{{ $val }}" {{ !empty($settings['builder_hover_font-family']) && $settings['builder_hover_font-family']==$val?'selected':'' }}>{{ $val }}</option>
																		@endforeach
																	@endif
																	</select>
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Font Size (px)</h4>
																	<input type="number" id="builder_hover_font-size" value="{{ !empty($settings['builder_hover_font-size'])?$settings['builder_hover_font-size']:'' }}" name="builder_hover_font-size" min="0" class="form-control input">
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Line Height (px)</h4>
																	<input type="number" id="builder_hover_line-height" value="{{ !empty($settings['builder_hover_line-height'])?$settings['builder_hover_line-height']:'' }}" name="builder_hover_line-height" min="0" class="form-control input">
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Letter Spacing (px)</h4>
																	<input type="number" id="builder_hover_letter-spacing" value="{{ !empty($settings['builder_hover_letter-spacing'])?$settings['builder_hover_letter-spacing']:'' }}" name="builder_hover_letter-spacing" min="0" class="form-control input">
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Word Spacing (px)</h4>
																	<input type="number" id="builder_hover_word-spacing" value="{{ !empty($settings['builder_hover_word-spacing'])?$settings['builder_hover_word-spacing']:'' }}" name="builder_hover_word-spacing" min="0" class="form-control input">
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Text Transform</h4>
																	<select id="builder_hover_text-transform" name="builder_hover_text-transform" data-style="btn-white" class="form-control input selectpicker">
																		<option value="">None</option>
																		<option value="uppercase" {{ !empty($settings['builder_hover_text-transform']) && $settings['builder_hover_text-transform']=='uppercase'?'selected':'' }}>Uppercase</option>
																		<option value="lowercase" {{ !empty($settings['builder_hover_text-transform']) && $settings['builder_hover_text-transform']=='lowercase'?'selected':'' }}>Lowercase</option>
																	</select>
																</div>
																<div class="form-field m-t-10  col-md-6">
																	<h4 class="text-muted m-b-15 m-t-15 font-15">Font Weight (px)</h4>
																	<select id="builder_hover_font-weight" name="builder_hover_font-weight" data-style="btn-white" class="form-control input selectpicker">
																		<option value=">Default</option>
																		<option value="300" {{ !empty($settings['builder_hover_font-weight']) && $settings['builder_hover_font-weight']=='300'?'selected':'' }}>300 (Light)</option>
																		<option value="400" {{ !empty($settings['builder_hover_font-weight']) && $settings['builder_hover_font-weight']=='400'?'selected':'' }}>400 (Regular)</option>
																		<option value="600" {{ !empty($settings['builder_hover_font-weight']) && $settings['builder_hover_font-weight']=='600'?'selected':'' }}>600 (Semi Bold)</option>
																		<option value="700" {{ !empty($settings['builder_hover_font-weight']) && $settings['builder_hover_font-weight']=='700'?'selected':'' }}>700 (Bold)</option>
																	</select>
																</div>
															<div class="form-field m-t-10  col-md-6">
																<h4 class="text-muted m-b-15 m-t-15 font-15">Border Style</h4>
																<select id="builder_hover_border-style"  name="builder_hover_border-style" data-style="btn-white" class="form-control input selectpicker">
																	<option value="none">None</option>
																	<option value="dotted" {{ !empty($settings['builder_hover_border-style']) && $settings['builder_hover_border-style']=='dotted'?'selected':'' }}>Dotted</option>
																	<option value="dashed" {{ !empty($settings['builder_hover_border-style']) && $settings['builder_hover_border-style']=='dashed'?'selected':'' }}>Dashed</option>
																	<option value="solid" {{ !empty($settings['builder_hover_border-style']) && $settings['builder_hover_border-style']=='solid'?'selected':'' }}>Solid</option>
																	<option value="double" {{ !empty($settings['builder_hover_border-style']) && $settings['builder_hover_border-style']=='double'?'selected':'' }}>Double</option>
																	<option value="groove" {{ !empty($settings['builder_hover_border-style']) && $settings['builder_hover_border-style']=='groove'?'selected':'' }}>Groove</option>
																	<option value="ridge" {{ !empty($settings['builder_hover_border-style']) && $settings['builder_hover_border-style']=='ridge'?'selected':'' }}>Ridge</option>
																	<option value="inset" {{ !empty($settings['builder_hover_border-style']) && $settings['builder_hover_border-style']=='inset'?'selected':'' }}>Inset</option>
																	<option value="outset" {{ !empty($settings['builder_hover_border-style']) && $settings['builder_hover_border-style']=='outset'?'selected':'' }}>Outset</option>
																	<option value="hidden" {{ !empty($settings['builder_hover_border-style']) && $settings['builder_hover_border-style']=='hidden'?'selected':'' }}>Hidden</option>
																</select>
																</div>
																<div class="form-field m-t-10  col-md-6">
																<h4 class="text-muted m-b-15 m-t-15 font-15">Border Color</h4>
																<div data-color-format="rgba" data-color="{{ !empty($settings['builder_hover_border-color'])?$settings['builder_hover_border-color']:'#000000' }}" class="colorpicker-rgba input-group colorpicker-element">
																	<input type="text" value="{{ !empty($settings['builder_hover_border-color'])?$settings['builder_hover_border-color']:'#000000' }}" id="builder_hover_border-color" name="builder_hover_border-color" class="form-control input">
																	<span class="input-group-btn add-on">
																	<button class="btn btn-white" type="button">
																		<i style="background-color: {{ !empty($settings['builder_hover_border-color'])?$settings['builder_hover_border-color']:'#000000' }};margin-top: 2px;"></i>
																	</button>
																	</span>
																</div>
																</div>
														</div>
														<div class="form-field m-t-10 ">
															<h4 class="text-muted m-b-15 m-t-15 font-15">Border Radius (px)</h4>
															<div class="row">
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_border-top-left-radius" value="{{ !empty($settings['builder_hover_border-top-left-radius'])?$settings['builder_hover_border-top-left-radius']:'' }}" name="builder_hover_border-top-left-radius" min="0" class="form-control input" placeholder="Top Left">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_border-top-right-radius" value="{{ !empty($settings['builder_hover_border-top-right-radius'])?$settings['builder_hover_border-top-right-radius']:'' }}" name="builder_hover_border-top-right-radius" min="0" class="form-control input" placeholder="Top Right">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_border-bottom-left-radius" value="{{ !empty($settings['builder_hover_border-bottom-left-radius'])?$settings['builder_hover_border-bottom-left-radius']:'' }}" name="builder_hover_border-bottom-left-radius" min="0" class="form-control input" placeholder="Bottom Left">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_border-bottom-right-radius" value="{{ !empty($settings['builder_hover_border-bottom-right-radius'])?$settings['builder_hover_border-bottom-right-radius']:'' }}" name="builder_hover_border-bottom-right-radius" min="0" class="form-control input" placeholder="Bottom Right">
																</div>
															</div>
														</div>
														<div class="form-field m-t-10 ">
															<h4 class="text-muted m-b-15 m-t-15 font-15">Border Width (px)</h4>
															<div class="row">
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_border-left-width" value="{{ !empty($settings['builder_hover_border-left-width'])?$settings['builder_hover_border-left-width']:'' }}" name="builder_hover_border-left-width" min="0" class="form-control input" placeholder="Border Left">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_border-right-width" value="{{ !empty($settings['builder_hover_border-right-width'])?$settings['builder_hover_border-right-width']:'' }}" name="builder_hover_border-right-width" min="0" class="form-control input" placeholder="Border Right">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_border-top-width" value="{{ !empty($settings['builder_hover_border-top-width'])?$settings['builder_hover_border-top-width']:'' }}" name="builder_hover_border-top-width" min="0" class="form-control input" placeholder="Border Top">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_border-bottom-width" value="{{ !empty($settings['builder_hover_border-bottom-width'])?$settings['builder_hover_border-bottom-width']:'' }}" name="builder_hover_border-bottom-width" min="0" class="form-control input" placeholder="Border Bottom">
																</div>
															</div>
														</div>
														<div class="form-field m-t-10 ">
															<h4 class="text-muted m-b-15 m-t-15 font-15">Padding (px)</h4>
															<div class="row">
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_padding-left" value="{{ !empty($settings['builder_hover_padding-left'])?$settings['builder_hover_padding-left']:'' }}" name="builder_hover_padding-left" min="0" class="form-control input" placeholder="Left">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_padding-right" value="{{ !empty($settings['builder_hover_padding-right'])?$settings['builder_hover_padding-right']:'' }}" name="builder_hover_padding-right" min="0" class="form-control input" placeholder="Right">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_padding-top" value="{{ !empty($settings['builder_hover_padding-top'])?$settings['builder_hover_padding-top']:'' }}" name="builder_hover_padding-top" min="0" class="form-control input" placeholder="Top">
																</div>
																<div class="col-sm-3">
																	<input type="number" id="builder_hover_padding-bottom" value="{{ !empty($settings['builder_hover_padding-bottom'])?$settings['builder_hover_padding-bottom']:'' }}" name="builder_hover_padding-bottom" min="0" class="form-control input" placeholder="Bottom">
																</div>
															</div>
														</div>
												</div>
												<div class="form-field m-t-10">
													<h4 class="text-muted m-b-15 m-t-15 font-15">Custom Classes</h4>
													<input type="text" id="custom_classes" name="custom_classes" value="{{ !empty($settings['custom_classes'])?$settings['custom_classes']:'' }}" class="form-control" placeholder="separated by space">
												</div>
												<div class="form-field m-t-10">
													<div class="checkbox">
															<input id="enable_transition" name="enable_transition" type="checkbox" {{ !empty($settings['enable_transition'])?'checked':'' }}>
															<label for="enable_transition">
															Enable Transition
															</label>
													</div>
												</div>
									</div>
								</div>
							</div>
							<div class="card-box">
									<div class="state normal">
											<div class="row">
															<div class="col-md-12">
																		<h4 class="text-muted m-b-15 m-t-15 font-15">Icon</h4>
																		<div class="icons icon_select">
                                      @foreach(getIcons() as $key=>$val)
																			     <div class="radio" style="margin:0;"><input type="radio" id="{{ $key }}" value="{{ $val }}" name="icon" {{ !empty($settings['icon']) && $settings['icon']==$val?'checked':'' }}><label for="{{ $key }}"><i class="{{ $val }}"></i></label></div>
                                      @endforeach
																		</div>
															</div>

											</div>
											<div class="row">
												<div class="col-md-12">
														<h4 class="text-muted m-b-15 m-t-15 font-15">Icon Position</h4>
														<select id="icon_position"  name="icon_position" data-style="btn-white" class="form-control input selectpicker">
																	<option value="left" {{ !empty($settings['icon_position']) && $settings['icon_position']=='left'?'selected':'' }}>Left</option>
																	<option value="right" {{ !empty($settings['icon_position']) && $settings['icon_position']=='right'?'selected':'' }}>Right</option>
														</select>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<h4 class="text-muted m-b-15 m-t-15 font-15">Icon Color</h4>
													<div data-color-format="rgba" data-color="{{ !empty($settings['icon_color'])?$settings['icon_color']:'#cccccc' }}" class="colorpicker-rgba input-group colorpicker-element">
														<input type="text" value="{{ !empty($settings['icon_color'])?$settings['icon_color']:'#cccccc' }}" id="icon_color" name="icon_color" class="form-control">
														<span class="input-group-btn add-on">
															<button class="btn btn-white" type="button">
																<i style="background-color: {{ !empty($settings['icon_color'])?$settings['icon_color']:'#cccccc' }};margin-top: 2px;"></i>
															</button>
														</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<h4 class="text-muted m-b-15 m-t-15 font-15">Icon Size</h4>
													<input type="number" id="icon_size" value="{{ !empty($setting['icon_size'])?$setting['icon_size']:'15' }}" name="icon_size" min="0" class="form-control input" placeholder="Icon Size">
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<h4 class="text-muted m-b-15 m-t-15 font-15">Icon Space</h4>
													<input type="number" id="icon_space" value="{{ !empty($setting['icon_space'])?$setting['icon_space']:'10' }}" name="icon_space" min="0" class="form-control input" placeholder="Icon Space">
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="checkbox">
														<input id="icon_showonhover" name="icon_showonhover" type="checkbox">
														<label for="icon_showonhover">
														Icon show on hover
														</label>
													</div>
												</div>
											</div>
								</div>
								<div class="state hover collapse">
									<div class="row">
										<div class="col-md-12">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Icon Color</h4>
											<div data-color-format="rgba" data-color="{{ !empty($settings['hover_icon_color'])?$settings['hover_icon_color']:'#cccccc' }}" class="colorpicker-rgba input-group colorpicker-element">
												<input type="text" value="{{ !empty($settings['hover_icon_color'])?$settings['hover_icon_color']:'#cccccc' }}" id="hover_icon_color" name="hover_icon_color" class="form-control">
												<span class="input-group-btn add-on">
													<button class="btn btn-white" type="button">
														<i style="background-color: {{ !empty($settings['hover_icon_color'])?$settings['hover_icon_color']:'#cccccc' }};margin-top: 2px;"></i>
													</button>
												</span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Icon Size</h4>
											<input type="number" id="hover_icon_size" value="{{ !empty($setting['hover_icon_size'])?$setting['hover_icon_size']:'15' }}" name="hover_icon_size" min="0" class="form-control input" placeholder="Icon Size">
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<h4 class="text-muted m-b-15 m-t-15 font-15">Icon Space</h4>
											<input type="number" id="hover_icon_space" value="{{ !empty($setting['hover_icon_space'])?$setting['hover_icon_space']:'10' }}" name="hover_icon_space" min="0" class="form-control input" placeholder="Icon Space">
										</div>
									</div>
								</div>









				</div>
			</div>
			<div class="col-sm-4 m-t-15">
				<div class="card-box">
						<input type="text" class="form-control" value="{{ $button->shortcode }}" readonly>
				</div>
				<div id="previewcontainer">
							<div class="card-box stickycontainer">
									<div id="preview" class="preview">
											<a href='#' class='activebutton {{ !empty($settings['button_class'])?$settings['button_class']:'' }}'>Button</a>
									</div>
							</div>
							<button type="submit" class="btn btn-primary waves-effect waves-light btn-lg">Save</button>
				</div>
			</div>
    </div>
</div>
<div class="container-fluid">

</div>
</form>
<script>
jQuery(document).ready(function($) {
				setTimeout(function(){
					generateStyles();
				},200);
				$('.colorpicker-default').colorpicker({
													format: 'hex'
											});
				$('.colorpicker-rgba').colorpicker();
				$('.input').bind("change keyup input",function() {
						setTimeout(function(){
							generateStyles();
						},100);
				});
				$('#custom_classes').bind("change keyup input",function() {
						setTimeout(function(){
							generateStyles();
						},100);
				});
				$('#enable_transition').change(function() {
						var check = $(this);
						setTimeout(function(){
								generateStyles();
						},50);
				});
				$('#icon_showonhover').change(function() {
						var check = $(this);
						setTimeout(function(){
								generateStyles();
						},50);
				});
				$('.button_builder .colorpicker-rgba').colorpicker().on('changeColor', function(){
					setTimeout(function(){
						generateStyles();
					},100);
				})
				$("input[name='icon']").change(function() {
						var check = $(this);
						setTimeout(function(){
								generateStyles();
						},50);
				});
				$('#copystyle').click(function(){
					if($('#builder_gradientbg').is(':checked')){
						$('#builder_hover_gradientbg').prop('checked', true);
						$('.hover_solid_bg').hide();
						$('.hover_gradient_bg').show();
					}else{
						$('#builder_hover_gradientbg').prop('checked', false);
						$('.hover_solid_bg').show();
						$('.hover_gradient_bg').hide();
					}

					$('#builder_hover_background-color').val($('#builder_background-color').val());
					$('#builder_hover_background-color-from').val($('#builder_background-color-from').val());
					$('#builder_hover_background-color-to').val($('#builder_background-color-to').val());
					$('#builder_hover_color').val($('#builder_color').val());
					$('#builder_hover_color').trigger('change');

					$('#builder_hover_font-family').val($('#builder_font-family').val());
					$('#builder_hover_font-family').trigger('change');
					$('#builder_hover_font-size').val($('#builder_font-size').val());
					$('#builder_hover_line-height').val($('#builder_line-height').val());
					$('#builder_hover_letter-spacing').val($('#builder_letter-spacing').val());
					$('#builder_hover_word-spacing').val($('#builder_word-spacing').val());
					$('#builder_hover_text-transform').val($('#builder_text-transform').val());
					$('#builder_hover_text-transform').trigger('change');
					$('#builder_hover_font-weight').val($('#builder_font-weight').val());
					$('#builder_hover_font-weight').trigger('change');


					$('#builder_hover_border-style').val($('#builder_border-style').val());
					$('#builder_hover_border-style').trigger('change');
					$('#builder_hover_border-color').val($('#builder_border-color').val());
					$('#builder_hover_border-color').trigger('change');

					$('#builder_hover_border-top-left-radius').val($('#builder_border-top-left-radius').val());
					$('#builder_hover_border-top-right-radius').val($('#builder_border-top-right-radius').val());
					$('#builder_hover_border-bottom-left-radius').val($('#builder_border-bottom-left-radius').val());
					$('#builder_hover_border-bottom-right-radius').val($('#builder_border-bottom-right-radius').val());
					$('#builder_hover_border-left-width').val($('#builder_border-left-width').val());
					$('#builder_hover_border-right-width').val($('#builder_border-right-width').val());
					$('#builder_hover_border-top-width').val($('#builder_border-top-width').val());
					$('#builder_hover_border-bottom-width').val($('#builder_border-bottom-width').val());
					$('#builder_hover_padding-left').val($('#builder_padding-left').val());
					$('#builder_hover_padding-right').val($('#builder_padding-right').val());
					$('#builder_hover_padding-top').val($('#builder_padding-top').val());
					$('#builder_hover_padding-bottom').val($('#builder_padding-bottom').val());
					$('#hover_icon_color').val($('#icon_color').val());
					$('#hover_icon_color').trigger('change');
					$('#hover_icon_size').val($('#icon_size').val());
					$('#hover_icon_space').val($('#icon_space').val());

					setTimeout(function(){
						generateStyles();
					},200);
						return false;
				})
})
function generateStyles(){
	$('head style:last').remove();
	var nopxfields = ['color','background-color','font-family','border-color','border-style','text-transform','font-weight'];
	var buttoncss ='<style>';
	var buttoncsshover = '';
	var button = '';
	var btnclass = '{{ !empty($settings['button_class'])?$settings['button_class']:'' }}';
	var custom_classes = ' '+$('#custom_classes').val();
	button += '<input type="hidden" name="button[]" value="'+btnclass+'">';
	button += '<input type="hidden" name="'+btnclass+'_custom_classes" value="'+custom_classes+'">';
	buttoncss +='.'+btnclass+'{';
	if($('#enable_transition').is(':checked')){
	  buttoncss+='-webkit-transition: all .2s ease-in-out;-moz-transition: all .2s ease-in-out;-o-transition: all .2s ease-in-out;transition: all .2s ease-in-out;';
	  button += '<input type="hidden" name="'+btnclass+'_transition" value="true">';
	}
	buttoncsshover+='.'+btnclass+':hover{';
	  $('.button_builder').find('.input').each(function(){
	    var id = $(this).attr('id');
	    if(typeof id !== 'undefined' && id!=''){
	        id = id.replace('builder_','');
	        if(id=='gradientbg' || id=='hover_gradientbg'){
	            if(id=='gradientbg' && $('#builder_gradientbg').is(':checked')){
	              button +='<input type="hidden" name="'+btnclass+'_'+id+'" value="on">';
	            }
	            if(id=='hover_gradientbg' && $('#builder_hover_gradientbg').is(':checked')){
	              button +='<input type="hidden" name="'+btnclass+'_'+id+'" value="on">';
	            }
	        }else{
	          button +='<input type="hidden" name="'+btnclass+'_'+id+'" value="'+$(this).val()+'">';
	        }

	        if(id.indexOf('hover_') != -1){
	          var hoverid = id.replace('hover_','');
	          if(hoverid == 'gradientbg'){
	            if($('#builder_hover_'+hoverid).is(':checked')){
	                  buttoncsshover+='background-color:'+$('#builder_hover_background-color-from').val();
	                  buttoncsshover+='background-image: -webkit-gradient(linear, left top, left bottom, from('+$('#builder_hover_background-color-from').val()+'), to('+$('#builder_hover_background-color-to').val()+'));';
	                  buttoncsshover+='background-image: -webkit-linear-gradient(top, '+$('#builder_hover_background-color-from').val()+', '+$('#builder_hover_background-color-to').val()+');';
	                  buttoncsshover+='background-image: -moz-linear-gradient(top, '+$('#builder_hover_background-color-from').val()+', '+$('#builder_hover_background-color-to').val()+');';
	                  buttoncsshover+='background-image: -ms-linear-gradient(top, '+$('#builder_hover_background-color-from').val()+', '+$('#builder_hover_background-color-to').val()+');';
	                  buttoncsshover+='background-image: -o-linear-gradient(top, '+$('#builder_hover_background-color-from').val()+', '+$('#builder_hover_background-color-to').val()+');';
	                  buttoncsshover+='background-image: linear-gradient(to bottom, '+$('#builder_hover_background-color-from').val()+', '+$('#builder_hover_background-color-to').val()+');';
	                  buttoncsshover+='filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='+$('#builder_hover_background-color-from').val()+', endColorstr='+$('#builder_hover_background-color-to').val()+');';
	                  button +='<input type="hidden" name="'+btnclass+'_hover_background-color-from" value="'+$('#builder_hover_background-color-from').val()+'">';
	                  button +='<input type="hidden" name="'+btnclass+'_hover_background-color-to" value="'+$('#builder_hover_background-color-to').val()+'">';
	            }else{
	                  buttoncsshover+= 'background-color:'+$('#builder_hover_background-color').val()+';';
	                  buttoncsshover+= 'background-image:none;';
	                  button +='<input type="hidden" name="'+btnclass+'_hover_background-color" value="'+$('#builder_hover_background-color').val()+'">';
	            }
	          }else{
	            if($(this).val()!=''){
	                if(jQuery.inArray(hoverid, nopxfields) != -1) {
	                    buttoncsshover+= hoverid+':'+$(this).val()+';';
	                } else {
	                    buttoncsshover+= hoverid+':'+$(this).val()+'px;';
	                }
	            }
	          }
	        }else{
	          if(id == 'gradientbg'){
	            if($('#builder_'+id).is(':checked')){
	                buttoncss+='background-color:'+$('#builder_background-color-from').val();
	                buttoncss+='background-image: -webkit-gradient(linear, left top, left bottom, from('+$('#builder_background-color-from').val()+'), to('+$('#builder_background-color-to').val()+'));';
	                buttoncss+='background-image: -webkit-linear-gradient(top, '+$('#builder_background-color-from').val()+', '+$('#builder_background-color-to').val()+');';
	                buttoncss+='background-image: -moz-linear-gradient(top, '+$('#builder_background-color-from').val()+', '+$('#builder_background-color-to').val()+');';
	                buttoncss+='background-image: -ms-linear-gradient(top, '+$('#builder_background-color-from').val()+', '+$('#builder_background-color-to').val()+');';
	                buttoncss+='background-image: -o-linear-gradient(top, '+$('#builder_background-color-from').val()+', '+$('#builder_background-color-to').val()+');';
	                buttoncss+='background-image: linear-gradient(to bottom, '+$('#builder_background-color-from').val()+', '+$('#builder_background-color-to').val()+');';
	                buttoncss+='filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='+$('#builder_background-color-from').val()+', endColorstr='+$('#builder_background-color-to').val()+');';
	                button +='<input type="hidden" name="'+btnclass+'_background-color-from" value="'+$('#builder_background-color-from').val()+'">';
	                button +='<input type="hidden" name="'+btnclass+'_background-color-to" value="'+$('#builder_background-color-to').val()+'">';
	            }else{
	              buttoncss+= 'background-color:'+$('#builder_background-color').val()+';';
	              buttoncss+= 'background-image:none;';
	              button +='<input type="hidden" name="'+btnclass+'_background-color" value="'+$('#builder_background-color').val()+'">';
	            }
	          }else{
	            if($(this).val()!=''){
	                if(jQuery.inArray(id, nopxfields) != -1) {
	                    buttoncss+= id+':'+$(this).val()+';';
	                } else {
	                   buttoncss+= id+':'+$(this).val()+'px;';
	                }
	            }
	          }
	        }
	    }
	  })




	buttoncss += '}';
	buttoncsshover += '}';
	buttoncss += buttoncsshover;

	var icon_position = $("#icon_position").val();
	var icon1,icon2;
	var iconcss = '.'+btnclass+' span.buttonicon{';

	iconcss += 'position:absolute;';
	iconcss += 'top:50%;';
	iconcss += 'transform:translateY(-50%);';
	iconcss += 'font-size:'+$("input[name='icon_size']").val()+'px;';
	iconcss += 'color:'+$("input[name='icon_color']").val()+';';
	iconcss += 'margin-left:'+$("input[name='icon_space']").val()+'px;margin-right:'+$("input[name='icon_space']").val()+'px;';
	if($('#enable_transition').is(':checked')){
		iconcss += '-webkit-transition: all .2s ease-in-out;-moz-transition: all .2s ease-in-out;-o-transition: all .2s ease-in-out;transition: all .2s ease-in-out;';
	}
	if($('input[name="icon_showonhover"]').is(":checked")){
		iconcss += 'transform:translateY(-50%) scale(0);';
	}

	var button_withicon_css = '.'+btnclass+'{';
	var button_hover_withicon_css = '.'+btnclass+':hover{';
	$('#preview').find('.activebutton').find('span').remove();
	if(icon_position=='left' && $("input[name='icon']:checked").val()){
		icon1 = '<span class="buttonicon '+$("input[name='icon']:checked").val()+'"></span>';
		$('#preview').find('.activebutton').prepend(icon1);
		var addedpadding = parseInt($("input[name='builder_padding-left']").val())+(parseInt($("input[name='icon_space']").val()))*2;
		addedpadding = addedpadding-5;
		iconcss += 'left:0;';
		if(!$('input[name="icon_showonhover"]').is(":checked")){
			button_withicon_css+='padding-right:'+parseInt($("input[name='builder_padding-right']").val())+'px;padding-left:'+addedpadding+'px;';
		}
		button_hover_withicon_css+='padding-right:'+parseInt($("input[name='builder_hover_padding-right']").val())+'px;padding-left:'+addedpadding+'px;';
	}else if(icon_position=='right' && $("input[name='icon']:checked"). val()){
		icon2 = '<span class="buttonicon '+$("input[name='icon']:checked").val()+'"></span>';
		$('#preview').find('.activebutton').append(icon2);
		var addedpadding = parseInt($("input[name='builder_padding-right']").val())+(parseInt($("input[name='icon_space']").val()))*2;
		addedpadding = addedpadding-5;
		iconcss += 'right:0;';
		if(!$('input[name="icon_showonhover"]').is(":checked")){
			button_withicon_css+='padding-left:'+parseInt($("input[name='builder_padding-left']").val())+'px;padding-right:'+addedpadding+'px;';
		}

		button_hover_withicon_css+='padding-left:'+parseInt($("input[name='builder_hover_padding-left']").val())+'px;padding-right:'+addedpadding+'px;';
	}

	var icon_css_button_hover = '.'+btnclass+':hover span.buttonicon{';

	if($('input[name="icon_showonhover"]').is(":checked")){
		icon_css_button_hover +='transform:translateY(-50%) scale(1);';
	}
	if($('input[name="hover_icon_color"]').val()!=''){
		icon_css_button_hover +='color:'+$('input[name="hover_icon_color"]').val()+';';
	}
	if($('input[name="hover_icon_size"]').val()!=''){
		icon_css_button_hover +='font-size:'+$('input[name="hover_icon_size"]').val()+'px;';
	}
	if($('input[name="hover_icon_space"]').val()!=''){
		icon_css_button_hover += 'margin-left:'+$("input[name='hover_icon_space']").val()+'px;margin-right:'+$("input[name='hover_icon_space']").val()+'px;';
	}


	iconcss += '}';
	button_withicon_css += '}';
	button_hover_withicon_css += '}';
	icon_css_button_hover += '}';

	buttoncss += iconcss;
	buttoncss += button_withicon_css;
	buttoncss += button_hover_withicon_css;
	buttoncss += icon_css_button_hover;

	buttoncss +='</style>';
	//$('#temp_presavedbuttons').val(button);
	$('head').append(buttoncss);
}
</script>
<script src="http://stickyjs.com/jquery.sticky.js"></script>
<script>
    $(window).load(function(){
      $("#previewcontainer").sticky({ topSpacing: 100, center:true });
				$("#previewcontainer").css('width', $('#previewcontainer-sticky-wrapper').width()+'px');
    });
</script>
@endsection
