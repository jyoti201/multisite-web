@extends('layouts.app')

@section('extra')
<link href="{{ asset('assets/private/plugins/nestable/jquery.nestable.css') }}" rel="stylesheet" />

<style>
.panel-title{
	text-transform:none;
}
.dd{
	max-width:400px;
}
.custom-dd .dd-list .dd-item .dd-handle {
    height: 30px;
    border-radius: 0;
    background: #36404a;
	    line-height: 26px;
}
.bootstrap-select.btn-group .dropdown-menu.inner{
	max-height:300px !important;
}
.custom-dd .well{
	margin-bottom:5px;
	    padding: 10px 15px;
}
.dd3-content{
	padding-top:0;
	padding-bottom:0;
}
.dd3-content a{
	display:block;
	    padding: 5px 0;
}
.dd3-content a.deleteitem{
	    position: absolute;
    right: 6px;
    top: 0;
}
.additems .panel-default > .panel-heading{
	padding:0;
}
.additems .panel-default > .panel-heading a{
padding: 10px 20px;
display:block;
}
</style>
<script src="{{ asset('assets/private/plugins/nestable/jquery.nestable.js') }}"></script>
<script>
jQuery(document).ready(function($){
	$('#nestable_list').nestable();
})
</script>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Options <span>Menu Settings</h4>
			</div>
			<div class="col-sm-12 m-t-15">
				<div class="card-box">
					<div class="row">
						<div class="col-sm-3">
							<p class="m-t-5 m-b-0">Select a menu to edit: </p>
						</div>
						<div class="col-sm-4">
							<div class="form-group" style="margin:0;">
							<select class="selectpicker selectmenu" data-live-search="true"  data-style="btn-white">
								<option value="">Select Menu</option>
								@foreach($menus as $menu)
								<option value="{{ $menu->id }}" @if(!empty($currentmenu) && $currentmenu->id == $menu->id) {{ "selected" }} @endif>{{ $menu->name }}</option>
								@endforeach
							</select>
							</div>
						</div>
						<div class="col-sm-5">
							<p class="m-t-5 m-b-0" style="display: inline-block"> Or <a href="{{ route('menu.create') }}">create a new menu</a>.</p>
							@if(!empty($currentmenu))
									<a href="javascript:void(0);" onclick="$(this).find('form').submit();"  class="btn btn-primary pull-right"> Delete Menu
										<form action="{{ route('menu.destroy', $currentmenu) }}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE">
										</form>
									</a>
							@endif
						</div>
					</div>
				</div>
			</div>
			@if (\Session::has('success'))
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
			@endif
			<div class="col-sm-12 m-t-15">

				<div class="row">
					<div class="col-sm-3 additems">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion-test-2" href="#pages" class="" aria-expanded="false">
										Pages
									</a>
								</h4>
							</div>
							<div id="pages" class="panel-collapse collapse show">
								<div class="panel-body">
									@foreach($pages as $page)
									<div class="checkbox">
										<input id="page_{{ $page->title }}" type="checkbox">
										<label for="page_{{ $page->title }}">
										{{ $page->title }}
										</label>
										<input type="hidden" name="title[]" value="{{ $page->title }}">
										<div class="checkbox_data">
											<input type="hidden" name="link[]" value="{{ $page->slug }}">
											<input type="hidden" name="type[]" value="page">
										</div>
									</div>
									@endforeach
								</div>
								<div class="panel-footer">
									<button type="button" class="btn btn-white waves-effect addpages">Add To Menu</button>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion-test-2" href="#custom" class="" aria-expanded="false">
										Custom
									</a>
								</h4>
							</div>
							<div id="custom" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="form-group inlinelabel">
										<label for="title">Title:</label>
										<input type="text" class="form-control" id="title">
									</div>
									<div class="form-group inlinelabel">
										<label for="link">Link:</label>
										<input type="text" class="form-control" id="link">
									</div>
								</div>
								<div class="panel-footer">
									<button type="button" class="btn btn-white waves-effect addcustom">Add To Menu</button>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-9">
						<form method="post" action="
						@if(!empty($currentmenu))
							{{ route("menu.update", $currentmenu->id) }}
						@else
							{{ route("menu.store") }}
						@endif
						">
						{{csrf_field()}}
						@if(!empty($currentmenu))
							<input type='hidden' name='_method' value='PATCH' />
						@endif
						<div class="panel panel-default">
							<div class="panel-heading" style="padding:5px;">
								<h4 class="panel-title">
									<div class="row">
										<div class="col-md-2">
											<p class="m-t-5 m-b-0 ml-3">Menu Name</p>
										</div>
										<div class="col-md-3 mr-auto">
											<input class="form-control" type="text" placeholder="Menu Name Here" name="menu_name" value="@if(!empty($currentmenu)) {{ $currentmenu->name }} @endif" required>
										</div>
										@if(!empty($currentmenu))
											<div class="col-md-1">
												<p class="m-t-5 m-b-0">Shortcode</p>
											</div>
											<div class="col-md-3">
												<input type="text" class="form-control" value='[menu id="{{ $currentmenu->id }}"]' readonly>
											</div>
										@endif
										<div class="col-md-3 text-right">
											@if(!empty($currentmenu))
												<button type="submit" class="btn btn-primary">Update Menu</button>
											@else
												<button type="submit" class="btn btn-primary">Create Menu</button>
											@endif
										</div>
									</div>
								</h4>
							</div>
							<div class="panel-collapse collapse show">
								<div class="panel-body">
									<div class="custom-dd dd" id="nestable_list">
										<ol class="dd-list">
										@if(!empty($currentmenu))
											{!! loopmenu($currentmenu) !!}
										@endif
										</ol>
									</div>
									<input type="hidden" name="order" value="">
								</div>
								<div class="panel-footer text-right">

									@if(!empty($currentmenu))
										<button type="submit" class="btn btn-primary">Update Menu</button>
									@else
										<button type="submit" class="btn btn-primary">Create Menu</button>
									@endif
								</div>
							</div>
						</div>
						</form>
					</div>

				</div>

			</div>

    </div>
</div>
@include('admin._partials.icons')
<script>
jQuery(document).ready(function(){
	jQuery('input[name="order"]').val(JSON.stringify(jQuery('.custom-dd').nestable('serialize')));
	jQuery('.addpages').click(function(){
		jQuery('#pages .checkbox input[type="checkbox"]').each(function(){
			var totalitem = jQuery('.dd-list li').length;
			var nextitem = Math.floor((Math.random() * 100000000) + 1);
			var inputs = '<input type="hidden" name="dataID[]" value="'+nextitem+'"><input type="hidden" name="target[]" value="_self">'+jQuery(this).parents('.checkbox').find('.checkbox_data').html();
			if(jQuery(this).is(":checked")){
				var title = jQuery(this).parents('.checkbox').find('input[name="title[]"]').val();
				var html = '<li class="dd-item" data-id="'+nextitem+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><a href="javascript:void(0)" data-toggle="collapse" data-target="#drop'+nextitem+'">'+title+'</a><a href="javascript:void(0)" data-target="'+nextitem+'" class="deleteitem pull-right"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
				html = html+inputs;
				html = html+'<div id="drop'+nextitem+'" class="collapse"><div class="well"><div class="form-group"><label class="control-label">Title</label><input class="form-control" type="text" name="title[]" value="'+title+'"></div><div class="form-group"><input class="form-control" type="hidden" name="icon[]" value=""><a href="#"  data-toggle="modal" data-target="#icon-select" class="icon-select btn btn-purple waves-effect waves-light btn-xs">Set Icon</a><i class="" style="padding:0 10px;vertical-align: middle;"></i></div><div class="form-group"><div class="form-group"><label class="control-label">Classes</label><input class="form-control" type="text" name="classes[]" value=""></div><div class="form-group"><label class="control-label">Description</label><textarea class="form-control" name="description[]"></textarea></div>';
				html = html+'<div class="checkbox"><input id="newtab'+nextitem+'" type="checkbox" class="target"><label for="newtab'+nextitem+'">Open New Tab</label></div></div></div>';
				html =html+'</li>';
				jQuery('#nestable_list .dd-list').eq(0).append(html);
				jQuery(this).prop("checked", false);
			}
		})
		jQuery('input[name="order"]').val(JSON.stringify(jQuery('.custom-dd').nestable('serialize')));
	})
	jQuery('.panel').on('click','.target',function(){
		if ($(this).is(':checked')) {
			$(this).parents('li').find('input[name="target[]"]').val('_blank');
		}else{
			$(this).parents('li').find('input[name="target[]"]').val('_self');
		}
	})
	jQuery('.addcustom').click(function(){
		var totalitem = jQuery('.dd-list li').length;
		var nextitem = Math.floor((Math.random() * 100000000) + 1);
		var title = jQuery('#custom #title').val();
		var link = jQuery('#custom #link').val();
		var inputs = '<input type="hidden" name="dataID[]" value="'+nextitem+'"><input type="hidden" name="type[]" value="custom"><input type="hidden" name="target[]" value="_self">';
		var html = '<li class="dd-item" data-id="'+nextitem+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><a href="javascript:void(0)" data-toggle="collapse" data-target="#drop'+nextitem+'">'+title+'</a><a href="javascript:void(0)" data-target="'+nextitem+'" class="deleteitem pull-right"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
		html = html+inputs;
		html = html+'<div id="drop'+nextitem+'" class="collapse"><div class="well"><div class="form-group"><label class="control-label">Title</label><input class="form-control" type="text" name="title[]" value="'+title+'"></div><div class="form-group"><input class="form-control" type="hidden" name="icon[]" value=""><a href="#"  data-toggle="modal" data-target="#icon-select" class="icon-select btn btn-purple waves-effect waves-light btn-xs">Set Icon</a><i class="" style="padding:0 10px;vertical-align: middle;"></i></div><div class="form-group"><label class="control-label">Link</label><input class="form-control" type="text" name="link[]" value="'+link+'"></div><div class="form-group"><label class="control-label">Classes</label><input class="form-control" type="text" name="classes[]" value=""></div><div class="form-group"><label class="control-label">Description</label><textarea class="form-control" name="description[]"></textarea></div>';
		html = html+'<div class="checkbox"><input id="newtab'+nextitem+'" type="checkbox" class="target"><label for="newtab'+nextitem+'">Open New Tab</label></div></div></div>';
		html =html+'</li>';

		jQuery('#nestable_list .dd-list').eq(0).append(html);

		jQuery('input[name="order"]').val(JSON.stringify(jQuery('.custom-dd').nestable('serialize')));
		jQuery('#custom #title').val('');
		jQuery('#custom #link').val('');
	})
	jQuery('.custom-dd').on('change', function() {
		jQuery('input[name="order"]').val(JSON.stringify(jQuery('.custom-dd').nestable('serialize')));
	});
	jQuery('.selectmenu').change(function(){
		window.location.href= '{{ route("menu.index") }}/'+jQuery(this).val()+'/edit';
	})
	jQuery('.custom-dd').on('click', '.deleteitem', function(){
		var item = jQuery(this);
		if (confirm('Are you sure ?')) {
			item.parents('.custom-dd').find('li[data-id="'+item.attr('data-target')+'"]').fadeOut();
			setTimeout(function(){ item.parents('.custom-dd').find('li[data-id="'+item.attr('data-target')+'"]').remove(); }, 500);
		}
	})
	jQuery('.icon-select').on('click', function(){
		jQuery('.icon-select').removeClass('selected');
		jQuery(this).addClass('selected');
	})
	jQuery('#icon-select .icon a').on('click', function(){
		var iconclass = jQuery(this).attr('data-icon');
		jQuery('.icon-select.selected').prev('input').val(iconclass);
		jQuery('.icon-select.selected').next('i').attr('class', iconclass);
		jQuery('#icon-select').modal('hide');
		return false;
	})
})
</script>
@endsection
