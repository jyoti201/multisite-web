<?php

return [
    'driver' => 'smtp', //env('MAIL_DRIVER', 'smtp'),
    'host' => 'smtp.sendgrid.net', //env('MAIL_HOST', 'smtp.mailgun.org'),
    'port' => 587, //env('MAIL_PORT', 587),
    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'hello@example.com'),
        'name' => env('MAIL_FROM_NAME', 'Example'),
    ],
    'encryption' => 'tls', //env('MAIL_ENCRYPTION', 'tls'),
    'username' => 'apikey', //env('MAIL_USERNAME'),

    'password' => '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', //env('MAIL_PASSWORD'),

    /*
    |--------------------------------------------------------------------------
    | Sendmail System Path
    |--------------------------------------------------------------------------
    |
    | When using the "sendmail" driver to send e-mails, we will need to know
    | the path to where Sendmail lives on this server. A default path has
    | been provided here, which will work well on most of your systems.
    |
    */

    'sendmail' => '/usr/sbin/sendmail -bs',

    /*
    |--------------------------------------------------------------------------
    | Markdown Mail Settings
    |--------------------------------------------------------------------------
    |
    | If you are using Markdown based email rendering, you may configure your
    | theme and component paths here, allowing you to customize the design
    | of the emails. Or, you may simply stick with the Laravel defaults!
    |
    */

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

];
