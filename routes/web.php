<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
}); */

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/authenticate/remote/{user}/{key}', 'ImpersonateUser@Impersonate');

Route::get('pages/trash', 'PagesController@trash')->name('pages.trash');
Route::get('pages/{id}/forcedelete', 'PagesController@forceDelete')->name('pages.forcedelete');
Route::get('pages/{id}/restore', 'PagesController@restore')->name('pages.restore');
Route::get('pages/{id}/clone', 'PagesController@clone')->name('pages.clone');

Route::resource('pages', 'PagesController');
Route::get('/snippets/snippet', 'PagesController@snippets')->name('pages.snippets');
Route::get('themes', 'ThemesController@index')->name('themes.index');
Route::get('themes/activate/{theme}', 'ThemesController@store')->name('themes.store');
Route::get('themes/remove/{theme}', 'ThemesController@remove')->name('themes.remove');

Route::post('pages/savehtml', 'PagesController@savehtml')->name('pages.savehtml');
Route::post('pages/saveimage', 'PagesController@saveimage')->name('pages.saveimage');
Route::post('pages/savecover', 'PagesController@savecover')->name('pages.savecover');
Route::get('pages/{id}/preview', 'PagesController@preview')->name('pages.preview');
Route::get('pages/{id}/preview-trash', 'PagesController@previewTrash')->name('pages.preview-trash');

Route::get('settings/general', 'SettingsController@general')->name('settings.general');
Route::get('settings/misc', 'PreloadSettings@setting')->name('settings.host');


Route::get('settings/header', 'SettingsController@header')->name('settings.header');
Route::resource('settings/menu', 'MenuController');
Route::get('settings/footer', 'SettingsController@footer')->name('settings.footer');
Route::get('settings/footer/builder', 'SettingsController@footerbuilder')->name('settings.footerbuilder');
Route::get('settings/styles', 'SettingsController@styles')->name('settings.styles');
Route::get('settings/colors', 'SettingsController@colors')->name('settings.colors');
Route::post('settings/header/save', 'SettingsController@saveSettings')->name('settings.saveheader');
Route::post('settings/menu/save', 'SettingsController@saveSettings')->name('settings.savemenu');
Route::post('settings/footer/save', 'SettingsController@saveSettings')->name('settings.savefooter');
Route::post('settings/styles/save', 'SettingsController@saveSettings')->name('settings.saveStyles');
Route::post('settings/colors/save', 'SettingsController@saveSettings')->name('settings.savecolors');
Route::post('settings/general/save', 'SettingsController@saveSettings')->name('settings.saveGeneral');

Route::get('settings/custom', 'SettingsController@custom')->name('settings.custom');
Route::post('settings/custom/save', 'SettingsController@saveSettings')->name('settings.savecustom');


Route::post('settings/saveHost', 'PreloadSettings@saveSettings')->name('settings.saveHost');
Route::get('/publish', 'PreloadSettings@publish')->name('publish');
Route::get('/deleterepo', 'PreloadSettings@deleteRepo')->name('deleteRepo');
Route::get('/download', 'PreloadSettings@downloadhtml')->name('download');

Route::get('settings/media', 'MediaController@index')->name('media.index');
Route::post('settings/media/upload', 'MediaController@upload')->name('media.upload');
Route::post('settings/media/modalupload', 'MediaController@modalupload')->name('media.modalupload');
Route::post('settings/media/croppedupload', 'MediaController@croppedupload')->name('media.croppedupload');


Route::get('settings/mediaselect', 'MediaController@select')->name('media.select'); // Content Builder Image popup
Route::get('settings/media/delete/{id}', 'MediaController@delete')->name('media.delete');


Route::get('/seeder', 'CreateSeeder@index');
Route::post('settings/resetdb', 'CreateSeeder@resetdb');
Route::get('settings/resetdbtest', 'CreateSeeder@resetdb');
Route::post('settings/update', 'CreateSeeder@update')->name('settings.update');
Route::get('settings/updatelocal', 'CreateSeeder@updateLocal')->name('settings.updatelocal');


Route::get('/test', 'CreateSeeder@test');
Route::get('/start', 'HomeController@start')->name('start');

Route::get('/widgets', 'WidgetsController@index')->name('widgets.index');
Route::post('/widgets/save', 'WidgetsController@save')->name('widget.save');

Route::get('/buttons', 'ButtonsController@index')->name('buttons.index');
Route::post('/buttons/save', 'ButtonsController@store')->name('buttons.save');
Route::get('/buttons/{id}/edit', 'ButtonsController@edit')->name('buttons.edit');
Route::get('/buttons/{id}/delete', 'ButtonsController@delete')->name('buttons.delete');

Route::get('widgets/{id}/clone', 'WidgetsController@clone')->name('widget.clone');
Route::get('/widgets/{id}/edit', 'WidgetsController@edit')->name('widget.edit');
Route::get('/widgets/{id}/delete', 'WidgetsController@delete')->name('widget.delete');
Route::post('/widgets/preview', 'WidgetsController@preview')->name('widget.preview');
Route::get('/widgets/slide/addlayer', 'WidgetsController@addlayer')->name('widget.addlayer');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/profile', 'HomeController@profileSave')->name('profileSave');


Route::get('pages/{id}/preview/public', 'CreateSeeder@previewpage');
Route::get('posts/{id}/preview/public', 'CreateSeeder@previewpost');

Route::get('posts/trash', 'PostsController@trash')->name('posts.trash');
Route::get('posts/{id}/forcedelete', 'PostsController@forceDelete')->name('posts.forcedelete');
Route::get('posts/{id}/restore', 'PostsController@restore')->name('posts.restore');

Route::resource('posts', 'PostsController');
Route::post('posts/savehtml', 'PostsController@savehtml')->name('posts.savehtml');
Route::post('posts/saveimage', 'PostsController@saveimage')->name('posts.saveimage');
Route::post('posts/savecover', 'PostsController@savecover')->name('posts.savecover');
Route::get('posts/{id}/preview', 'PostsController@preview')->name('posts.preview');
Route::get('posts/{id}/preview-trash', 'PostsController@previewTrash')->name('posts.preview-trash');
Route::get('posts/{id}/clone', 'PostsController@clone')->name('posts.clone');

Route::get('/demos', 'LoadDemo@LoadDemo')->name('loaddemo');
Route::get('/demo/create', 'LoadDemo@CreateDemo')->name('createdemo');
Route::post('/demo/import', 'LoadDemo@importDemo')->name('importDemo');

Route::resource('categories', 'CategoriesController');
Route::get('categories', 'CategoriesController@index')->name('categories.index');
Route::get('categories/trash', 'CategoriesController@trash')->name('categories.trash');
Route::get('categories/{id}/forcedelete', 'CategoriesController@forceDelete')->name('categories.forcedelete');
Route::get('categories/{id}/restore', 'CategoriesController@restore')->name('categories.restore');
Route::get('categories/{id}/clone', 'CategoriesController@clone')->name('categories.clone');

Route::get('popups/trash', 'PopupController@trash')->name('popups.trash');
Route::get('popups/{id}/forcedelete', 'PopupController@forceDelete')->name('popups.forcedelete');
Route::get('popups/{id}/restore', 'PopupController@restore')->name('popups.restore');
Route::get('popups/{id}/clone', 'PopupController@clone')->name('popups.clone');
Route::post('popups/savehtml', 'PopupController@savehtml')->name('popups.savehtml');
Route::resource('popups', 'PopupController');

Route::get('/user/verify/{token}', 'Auth\VerifyUserController@verifyUser');




Route::get('sections/trash', 'SectionsController@trash')->name('sections.trash');
Route::get('sections/{id}/forcedelete', 'SectionsController@forceDelete')->name('sections.forcedelete');
Route::get('sections/{id}/restore', 'SectionsController@restore')->name('sections.restore');

Route::resource('sections', 'SectionsController');
Route::post('sections/savehtml', 'SectionsController@savehtml')->name('sections.savehtml');
Route::post('sections/builderstore', 'SectionsController@builderstore')->name('sections.builderstore');
Route::post('sections/getsectionfromhtml', 'SectionsController@getSectionFromHtml')->name('sections.getSectionFromHtml');
Route::post('sections/saveimage', 'SectionsController@saveimage')->name('sections.saveimage');
Route::post('sections/savecover', 'SectionsController@savecover')->name('sections.savecover');
Route::get('sections/{id}/preview', 'SectionsController@preview')->name('sections.preview');
Route::get('sections/{id}/preview-trash', 'SectionsController@previewTrash')->name('sections.preview-trash');
Route::get('sections/{id}/clone', 'SectionsController@clone')->name('sections.clone');
