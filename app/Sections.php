<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sections extends Model
{
    use SoftDeletes;
	protected $fillable = [
        'title','html', 'shortcode'
    ];
	protected $dates = ['deleted_at'];
}
