<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
	protected $fillable = [
        'title', 'slug', 'html', 'meta_title', 'meta_description', 'meta_keywords', 'custom_css', 'featured_image', 'excerpt', 'categories'
    ];
	protected $dates = ['deleted_at'];
}
