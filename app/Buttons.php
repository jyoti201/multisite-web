<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buttons extends Model
{
  protected $fillable = [
       'name', 'shortcode', 'setting'
   ];
}
