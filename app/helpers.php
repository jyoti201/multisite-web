<?php
function IncludeAsset($asset){
	$theme = App\theme::where('active', 1)->first();
	return asset('templates/'.$theme['name'].'/'.$asset);
}
function getThemeConfig($theme, $parameter){
	$settings = File::get(public_path('templates/'.$theme.'/config.json'));
	$json = object_to_array(json_decode(utf8_encode($settings)));
	if($parameter=='all'){
		return $json;
	}else{
		if(array_key_exists($parameter, $json)){
		return $json[$parameter];
		}
	}
}
function object_to_array($obj) {
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach ($_arr as $key => $val) {
                $val = (is_array($val) || is_object($val)) ? object_to_array($val) : $val;
                $arr[$key] = $val;
        }
        return $arr;
}
function getImageDimension($url){
	if(file_exists(public_path().$url)){
	list($width, $height) = getimagesize(public_path().$url);
	return $width.' x '.$height;
	}
}
function getFileSize($url){
	if(file_exists(public_path().$url)){
	return FileSizeConvert(filesize(public_path().$url));
	}
}
function FileSizeConvert($bytes)
{
	$decimals="2";
	$size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
	$factor = floor((strlen($bytes) - 1) / 3);
	return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) .' '. @$size[$factor];
}

function getThemeName($theme){
	return getThemeConfig($theme,'name');
}
function getMenuLocations($theme){
	$locations = getThemeConfig($theme,'menu_locations');
	return $locations['menu_locations'];
}
function loopmenu($currentmenu){
	$menu = "";
	if(!empty($currentmenu->order)){
		foreach($currentmenu->order as $key=>$order):
			$menu.=getMenuitems($currentmenu, $order->id);
			$menu.=getMenuitemsChildren($currentmenu, $order);
		endforeach;
	}
	return $menu;
}
function getMenuitems($currentmenu, $dataID){
	$menuitems = "";
	$checktarget = "";
	foreach($currentmenu->menu as $key=>$menu):
		if($dataID == $menu['dataID']){
			$target = '_self';
			if(!empty($menu['target']) && $menu['target']=='_blank'){
				$checktarget = "checked";
				$target = $menu['target'];
			}
			if(!empty($menu['title'])){
				$title = htmlspecialchars($menu['title']);
			}else{
				$title = '';
			}
			if(!empty($menu['link'])){
				$link = $menu['link'];
			}else{
				$link = '';
			}
			if(!empty($menu['icon'])){
				$icon = $menu['icon'];
			}else{
				$icon = '';
			}
			if(!empty($menu['classes'])){
				$classes = $menu['classes'];
			}else{
				$classes = '';
			}
			if(!empty($menu['description'])){
				$description = $menu['description'];
			}else{
				$description = '';
			}

			$menuitems .='<li class="dd-item" data-id="'.$menu['dataID'].'">';
			$menuitems .='<div class="dd-handle dd3-handle"></div><div class="dd3-content"><a href="javascript:void(0)" data-toggle="collapse" data-target="#drop'.$menu['dataID'].'">'.$title.' - '.$menu['type'].'</a><a href="javascript:void(0)" data-target="'.$menu['dataID'].'" class="deleteitem pull-right"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
			$menuitems .='<div id="drop'.$menu['dataID'].'" class="collapse"><div class="well"><div class="form-group"><label class="control-label">Title</label><input class="form-control" type="text" name="title[]" value="'.$menu['title'].'"></div>';
			$menuitems .='<div class="form-group"><input class="form-control" type="hidden" name="icon[]" value="'.$icon.'"><a href="#"  data-toggle="modal" data-target="#icon-select" class="icon-select btn btn-purple waves-effect waves-light btn-xs">Set Icon</a><i class="'.$icon.'" style="padding:0 10px;vertical-align: middle;"></i></div>';
			if($menu['type']=='custom'){
			$menuitems .='<div class="form-group"><label class="control-label">Link</label><input class="form-control" type="text" name="link[]" value="'.$link.'"></div>';
			}else{
			$menuitems .='<input type="hidden" name="link[]" value="'.$link.'">';
			}
			$menuitems .='<div class="form-group"><label class="control-label">Classes</label><input class="form-control" type="text" name="classes[]" value="'.$classes.'"><input type="hidden" name="target[]" value="'.$target.'"></div>';
			$menuitems .='<div class="form-group"><label class="control-label">Description</label><textarea class="form-control" name="description[]">'.$description.'</textarea></div>';
			$menuitems .='<div class="checkbox"><input id="newtab'.$menu['dataID'].'" type="checkbox" class="target" '.$checktarget.'><label for="newtab'.$menu['dataID'].'">Open New Tab</label></div></div></div>';
			$menuitems .='<input type="hidden" name="dataID[]" value="'.$menu['dataID'].'">';
			$menuitems .='<input type="hidden" name="type[]" value="'.$menu['type'].'">';
		}
	endforeach;
	return $menuitems;
}
function getMenuitemsChildren($currentmenu, $order){
	$menu = "";
	if(!empty($order->children)){
		$menu.='<ol class="dd-list">';
		foreach($order->children as $keychild=>$orderchild):
			$menu.=getMenuitems($currentmenu, $orderchild->id);
			$menu.=getMenuitemsChildren($currentmenu, $orderchild);
		endforeach;
		$menu.="</ol></li>";
	}else{
		$menu.="</li>";
	}
	return $menu;
}
function getMenubyID($menuid){
	$menu = App\Menu::find($menuid);
	if($menu){
	$menu['menu'] = unserialize($menu->menu);
	$menu['order'] = unserialize($menu->order);
	return loopmenu_frontend($menu);
	}
}
function loopmenu_frontend($currentmenu){
	$menu = "";
	foreach($currentmenu->order as $key=>$order):
		$menu.=getMenuitems_frontend($currentmenu, $order->id);
		$menu.=getMenuitemsChildren_frontend($currentmenu, $order);
	endforeach;
	return $menu;
}
function getMenuitems_frontend($currentmenu, $dataID){
	$menuitems = "";
	foreach($currentmenu->menu as $key=>$menu):
		if($dataID == $menu['dataID']){
			if(!empty($menu['target'])){
				$target= $menu['target'];
			}else{
				$target= '_self';
			}
			if(!empty($menu['icon'])){
				$icon= '<i class="'.$menu['icon'].'"></i>';
			}else{
				$icon= '';
			}
		$menuitems .='<li class="menu-item">';
		$menuitems .='<a class="nav-link '.$menu['classes'].'" href="'.$menu['link'].'" target="'.$target.'">'.$icon.' '.$menu['title'].'</a><small>'.$menu['description'].'</small>';
		}
	endforeach;
	return $menuitems;
}
function getMenuitemsChildren_frontend($currentmenu, $order){
	$menu = "";
	if(!empty($order->children)){
		$menu.='<ul class="sub-menu">';
		foreach($order->children as $keychild=>$orderchild):
			$menu.=getMenuitems_frontend($currentmenu, $orderchild->id);
			$menu.=getMenuitemsChildren_frontend($currentmenu, $orderchild);
		endforeach;
		$menu.="</ul></li>";
	}else{
		$menu.="</li>";
	}
	return $menu;
}
function filterOutput($string){
	$string = filterSavedSections($string);
	$string = implementSavedSections($string);
	$string = ImplementWidgets($string);
	$string = ImplementAnimation($string);
	return $string;
}
function getImage($image, $type){
	$ext = pathinfo($image, PATHINFO_EXTENSION);
	$file = basename($image, ".".$ext);
	$path = str_replace($file.'.'.$ext,'',$image);
	if($type=='thumbnail'){
		return $path.$file.'_thumb.'.$ext;
	}else{
		return $image;
	}
}
function has_sidebar($pageid){
	$page = App\Pages::find($pageid);
	if($page->sidebar!=''){
		return true;
	}else{
		return false;
	}
}
function getSetting($key,$type){
	$rawsetting = App\Settings::where('type', $type)->first();
	$settings = false;
	if(!empty($rawsetting)){
		$settings = unserialize($rawsetting->setting);
		if($type == 'footer'){
			$settings['html'] = $settings['html'];
		}
		if(array_key_exists($key,$settings)){
			return $settings[$key];
		}else{
			return false;
		}
	}
}
function getMetaTitle($page){
	$generalsetting = App\Settings::where('type', 'general')->first();
	if(!empty($generalsetting->setting)){
		$settings = unserialize($generalsetting->setting);
		if(!empty($page['meta_title'])){
			return str_replace('{site_title}',$page->meta_title,$settings['meta_title_format']);
		}else{
			return str_replace('{site_title}',$settings['global_meta_title'],$settings['meta_title_format']);
		}
	}else{
		return $page->meta_title;
	}
}
function getAdminsetting($setting,$type){
	$user = App\User::find(1);
	if($setting == 'host'){
		$request = json_decode(get_curl_content('http://admin.duktix.com/settings/host/'.$user->email));
		return !empty($request->$type)?$request->$type:'';
	}elseif($setting == 'templates') {
		$request =json_decode(get_curl_content('http://admin.duktix.com/settings/templates/'.$user->email));
		return $request;
	}
}
function getAdminUser($property){
	$user = App\User::find(1);
	return $user->$property;
}
function saveHTML($pageid){
	$currenttheme = App\theme::getCurrentTheme();
	$page = App\Pages::find($pageid);
	if($page){
		$view = view('templates.'.$currenttheme['name'].'.index',['type'=> 'page','page' => $page, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> true]);
		$contents = cleanHTML($view->render());
		file_put_contents(public_path('/html/').$page['slug'].'.html', $contents);
	}
}

function saveallHTML(){
	$currenttheme = App\theme::getCurrentTheme();
	$pages = App\Pages::All();
	foreach($pages as $page){
		$view = view('templates.'.$currenttheme['name'].'.index',['type'=> 'page', 'page' => $page, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> true]);
		$contents = cleanHTML($view->render());
		file_put_contents(public_path('/html/').$page['slug'].'.html', $contents);
	}
	saveallPosts();
	generateXml();
}
function saveallPosts(){
	$currenttheme = App\theme::getCurrentTheme();
	$generalsettings = App\Settings::where('type', "general")->first();
	if($generalsettings)
		$savedsettings = unserialize($generalsettings->setting);
	$posts = App\Post::All();
	foreach($posts as $post){
		$view = view('templates.'.$currenttheme['name'].'.index',['type'=> 'post', 'page' => $post, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> true]);
		$contents = cleanHTML($view->render());
		if(!empty($savedsettings['blog'])){
			recreateBlog($savedsettings['blog']);
			$pre = $savedsettings['blog'].'/';
		}else{
			recreateBlog('');
			$pre ='blog/';
		}
		file_put_contents(public_path('/html/').$pre.$post['slug'].'.html', $contents);
	}
}

function generateXml(){
	$xml = new SimpleXMLElement("<xml version='1.0'/>");
	$track = $xml->addChild('urlset');
	$track->addAttribute("xmlns",'http://www.sitemaps.org/schemas/sitemap/0.9');
	$track->addAttribute("xmlns:xsi",'http://www.w3.org/2001/XMLSchema-instance');
	$track->addAttribute("xsi:schemaLocation",'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');

	$track2 = $track->addChild('url');
	$track2->addChild('loc', getSetting('prod_url','host'));
	$track2->addChild('lastmod', date('Y-m-d'));
	$track2->addChild('changefreq', "always");

	$pages = App\Pages::All();
	foreach($pages as $page){
			$track2 = $track->addChild('url');
	    $track2->addChild('loc', getSetting('prod_url','host')."/".$page->slug);
	    $track2->addChild('lastmod', $page->updated_at);
			$track2->addChild('changefreq', "always");
	}
	$posts = App\Post::All();
	foreach($posts as $post){
			$track2 = $track->addChild('url');
	    $track2->addChild('loc', getSetting('prod_url','host')."/".$post->slug);
	    $track2->addChild('lastmod', $post->updated_at);
			$track2->addChild('changefreq', "always");
	}
	Header('Content-type: text/xml');
	file_put_contents(public_path('/html/').'sitemap.xml', $xml->asXML());
	$str = "Sitemap:".getSetting('prod_url','host')."/sitemap.xml"."\r\n";
	$str .= "User-agent:*"."\r\n";
	$str .= "Disallow:";
	file_put_contents(public_path('/html/').'robots.txt', $str);
}
function deleteOldBlog(){
	$generalsettings = App\Settings::where('type', "general")->first();
	if($generalsettings){
		$savedsettings = unserialize($generalsettings->setting);
		if(!empty($savedsettings['blog'])){
			$blogdir = $savedsettings['blog'].'/';
		}else{
			$blogdir = 'blog'.'/';
		}
		$rootPath = public_path('/html/'.$blogdir);
		try {
			File::deletedirectory($rootPath);
		} catch(RunTimeException $e) {
			dd('Whoops: ' . $e->getMessage());
		}

	}
}

function recreateBlog($new){
	$currenttheme = App\theme::getCurrentTheme();
	if(!empty($new)){
	$blogdir = $new;
	$rootPath = public_path('/html/'.$blogdir.'/');
	$client = Storage::createLocalDriver(['root' => $rootPath]);
	$client->put('index.html', \File::get(substr($rootPath, 0, strlen($rootPath)-1).'.html'));
	}else{
	$blogdir = 'blog';
	$rootPath = public_path('/html/'.$blogdir.'/');
	$client = Storage::createLocalDriver(['root' => $rootPath]);
	$client->put('index.html', '#set a blog page');
	}

	$categories = App\Categories::All();
	$generalsettings = App\Settings::where('type', "general")->first();
	if($generalsettings){
		$savedsettings = unserialize($generalsettings->setting);
		if(!empty($savedsettings['blog'])){
			$blogdir = $savedsettings['blog'];
		}else{
			$blogdir = 'blog';
		}
	}
	foreach($categories as $category){
		$rootPath = public_path('/html/'.$blogdir.'/'.$category->slug.'/');
		$client = Storage::createLocalDriver(['root' => $rootPath]);
		$page = App\Pages::where('slug', $blogdir)->first();
		$contents = '';
		if($page){
			$view = view('templates.'.$currenttheme['name'].'.index',['page' => $page, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> true]);
			$contents = $view->render();
			$match = preg_match('/<div class="post-content widget_(.*?)" id="(.*?)">(.*?)<\/div>/s', $contents, $matches);
			if(sizeof($matches)>0){
			$widget_id = $matches[1];
			$html = getBlogPosts($widget_id, $category->slug);
			$contents = preg_replace('/<div class="post-content widget_(.*?)" id="(.*?)">(.*?)<\/div>/s', $html, $contents);
			}
			$client->put('index.html', $contents);
		}

	}


}
function getBlogPosts($widgetid, $categories){
	if(!empty($categories)){
	$categories_array = explode(',', $categories);
	}
	$setting = App\Widget::find($widgetid);
	$widget = unserialize($setting->setting);
	$noofposts =!empty($widget['noofposts'])?$widget['noofposts']:'';
	$noofcolumns =!empty($widget['noofcolumns'])?$widget['noofcolumns']:'';
	$showfeatured =!empty($widget['showfeatured'])?$widget['showfeatured']:'';
	$showdate =!empty($widget['showdate'])?$widget['showdate']:'';
	$showexcerpt =!empty($widget['showexcerpt'])?$widget['showexcerpt']:'';
	$excerptlength=!empty($widget['excerptlength'])?$widget['excerptlength']:'';
	$dateformat=!empty($widget['dateformat'])?$widget['dateformat']:'';
	$readmore=!empty($widget['readmore'])?$widget['readmore']:'';
	$showpagination=!empty($widget['showpagination'])?$widget['showpagination']:'';
	$perpage=!empty($widget['perpage'])?$widget['perpage']:'';
	$template=!empty($widget['blogstyle'])?$widget['blogstyle']:'';
	$customtemplate = !empty($widget['customtemplate'])?$widget['customtemplate']:'';

	$currenttheme = App\theme::getCurrentTheme();
	if(!empty($noofposts)){
	$posts = App\Post::All()->take($noofposts);
	}elseif(!empty($noofposts) && $noofposts==-1){
	$posts = App\Post::All();
	}

	$html = '';
	$col = 12/$noofcolumns;

	$generalsettings = App\Settings::where('type', "general")->first();
	if($generalsettings)
		$savedsettings = unserialize($generalsettings->setting);
	$pre ='blog/';
	if(!empty($savedsettings['blog'])){
		$pre = $savedsettings['blog'].'/';
	}

	if(empty($template)){
		$template = 'blog';
	}
	$count=0;
	$page = 0;
	$itemcount = array();
	$loop =0;
	foreach($posts as $post){
		if(!empty($categories)){
			$posts_categories = explode(',', $post->categories);
			if(!empty($categories_array) && sizeof(array_intersect($categories_array,$posts_categories))==0){
				break;
			}
		}
		if($count==0){
			$page++;
			$html .= '<div class="post-content row flex-wrap widget_'.$widgetid.'" id="paginated-'.$page.'">';
		}



		$date = \Carbon\Carbon::parse($post['created_at'])->format($dateformat);

		if($customtemplate!=''){
			$contents = $customtemplate;
		}else{
			$view = view('templates.'.$currenttheme['name'].'.'.$template);
			$contents = $view->render();
		}

		$contents = preg_replace('/{title}/',$post['title'],$contents);
		$contents = preg_replace('/{columns}/','col-sm-'.$col,$contents);

		if(!empty($showdate)){
			$contents = preg_replace('/{date}/',$date,$contents);
		}else{
			$contents = preg_replace('/{date}/','',$contents);
		}

		if(!empty($showfeatured)){
			$contents = preg_replace('/{featuredImage}/', '<img src="'.$post['featured_image'].'" />',$contents);
		}else{
			$contents = preg_replace('/{featuredImage}/','',$contents);
		}

		if(!empty($showexcerpt)){
			$excerpt = substr($post['excerpt'], 0, $excerptlength);
			$excerpt .= $readmore;
			$excerpt = preg_replace('/{readmore}/',"<a href='/".$pre.$post['slug']."' class='readmore'>Read More</a>",$excerpt);
			$contents = preg_replace('/{excerpt}/',$excerpt,$contents);
		}else{
			$contents = preg_replace('/{excerpt}/','',$contents);
		}

		$contents = preg_replace('/{link}/',getSetting('prod_url','host').'/'.$pre.$post['slug'] ,$contents);
		$contents = preg_replace('/{featuredImageUrl}/',$post['featured_image'] ,$contents);


		$html .= $contents;


				$count++;
				$loop++;

		if($count==$perpage || ($loop == sizeof($posts) && $count<$perpage)){
			$count=0;
			$html .= '</div>';
		}

	}
	$rand = rand(0, 999999999);
	if($showpagination && $page>1){
		$html .= '<div class="col-md-12"><ul id="pagination-'.$rand.'" class="pagination-sm"></ul></div>';
		$html .= '<script>$(document).ready(function(){ $(".post-content").hide();$("#pagination-'.$rand.'").twbsPagination({ totalPages: '.$page.', visiblePages: 6, next: "Next", prev: "Prev", onPageClick: function (event, page) { $(".post-content").hide();$("#paginated-"+page).show()  } }); }) </script>';
	}
	return $html;
}
function deleteHTML($slug){
	\File::delete(public_path('/html/'.$slug.'.html'));
}
function deleteallHTML(){
	\File::delete(\File::glob(public_path('/html/*.html')));
}
function cleanHTML($content){
	$currenttheme = App\theme::getCurrentTheme();
	$url = '/'.str_replace('/','\/',\Config::get('app.url')).'(\/public)?(\/assets)?(\/public)?/';
	$content = preg_replace($url,getSetting('prod_url','host').'/assets',$content);
	$content = preg_replace('/\/uploads/',getSetting('prod_url','host').'/uploads',$content);
	$content = preg_replace('/\/builder\/(contentbuilder\/)?(contentbox\/)?(images\/)?/','/images/samples/',$content);
	$content = preg_replace('/\/templates\/'.$currenttheme['name'].'\/public/', '/theme/'.$currenttheme['name'],$content);
	$content = preg_replace('/\/assets\/public/','/assets',$content);
	$content = preg_replace('/\/html\/assets/','/assets',$content);
	$content = preg_replace('/<removable>(.*?)<\/removable>/','',$content);

	return $content;
}
function removeAppurl($content){
	$url = '/'.str_replace('/','\/',\Config::get('app.url')).'(\/public\/)?/';
	$content = preg_replace($url,'',$content);
	return $content;
}
function ImplementWidgets($content){
	$match = preg_match_all('/(\[widget type="(.*?)" id="(.*?)"\])/',$content,$matches);
	if($match>0){
		foreach($matches[2] as $key=>$type){
			$id = $matches[3][$key];
			if($id!=''){
				$widget = App\Widget::find($id);
				if(!empty($widget->setting)){
					$settings = unserialize($widget->setting);
					$settings['uniqueid'] = uniqid();
					$view = view('front.widgets.'.$type, ['widget'=>$widget, 'settings' => $settings, 'pages' => App\Pages::All(), 'posts' => App\Post::All()]);
					$content = preg_replace('/(\[widget type="'.$type.'" id="'.$id.'"\])/', $view->render(),$content, 1);
				}
			}
		}
	}

	$match = preg_match_all('/\[menu id="(.*?)"( type="(.*?)")?\]/',$content,$matches);
	if($match>0){
		foreach($matches[2] as $key=>$type){
			$id = $matches[1][$key];
			$type = !empty($matches[3][$key])?$matches[3][$key]:'';
			if($id!='' && empty($type)){
				$content = preg_replace('/(\[menu id="'.$id.'"\])/', '<ul class="shortcode-menu">'.getMenubyID($id).'</ul>', $content, 1);
			}else{
				$content = preg_replace('/(\[menu id="'.$id.'" type="'.$type.'"\])/', '<ul class="shortcode-menu '.$type.'">'.getMenubyID($id).'</ul>', $content, 1);
			}
		}
	}

	return ImplementButtonShortcode($content);
}
function filterSavedSections($content){
	$match = preg_match_all('/<div class=".*?saved-section.*?">.*?<div.*?id="(.*?)">.*?\[section id="(.*?)"\].*?<\/div>.*?<\/div>.*?<\/div>.*?<\/div>/',$content,$matches);
	if($match){
		foreach($matches[0] as $key=>$type){
			$sectionid = $matches[1][$key];
			$id = $matches[2][$key];
			if($id!=''){
				$content = preg_replace('/<div class=".*?saved-section.*?">.*?<div.*?id="'.$sectionid.'">.*?\[section id="(.*?)"\].*?<\/div>.*?<\/div>.*?<\/div>.*?<\/div>/', '[section id="$1"]', $content, 1);
			}
		}
	}
	return $content;
}
function getSectionShortcodeFromHtml($content){
	$match = preg_match_all('/(\[section id="(.*?)"\])/',$content,$matches);
	if($match){
		foreach($matches[0] as $key=>$type){
			$sectionid = $matches[1][$key];
			$id = $matches[2][$key];
			if($id!=''){
				$content = '[section id="'.$id.'"]';
			}
		}
	}
	return $content;
}
function implementSavedSections($content){
	$match = preg_match_all('/(\[section id="(.*?)"\])/',$content,$matches);
	if($match>0){
		foreach($matches[2] as $key=>$type){
			$id = $matches[2][$key];
			if($id!=''){
				$content = preg_replace('/(\[section id="'.$id.'"\])/', getSectionbyID($id), $content);
			}
		}
	}
	return $content;
}
function getSectionbyID($sectionid){
	$section = App\Sections::find($sectionid);
	if($section){
	return $section->html;
	}
}
function ImplementAnimation($content){
	$globalanimations = getSetting('animation_classid','styles');
	if(!empty($globalanimations) && sizeof($globalanimations)>0){
		foreach($globalanimations as $key=>$val){
			if($val!=''){
			$val = str_replace(array('.', '#'), array('', ''), $val);
			$animation = getSetting('animation_effect','styles')[$key];
			$offset = getSetting('animation_offset','styles')[$key];
			$delay = getSetting('animation_delay','styles')[$key];
			$duration = getSetting('animation_duration','styles')[$key];
			$easing = getSetting('animation_easing','styles')[$key];

			$loop = 'false';
			if(!empty(getSetting('animation_once','styles')[$key])){
				$loop = 'true';
			}
			$content = preg_replace('/('.$val.')(.*?)\"/', $val.'$2" data-aos="'.$animation.'" data-aos-easing="'.$easing.'" data-aos-offset="'.$offset.'" data-aos-delay="'.$delay.'" data-aos-duration="'.$duration.'" data-aos-once="'.$loop.'"',$content);
			}
		}
	}
	return $content;
}
function ImplementButtonShortcode($content){
	$match = preg_match_all('/(\[button id="(.*?)"( link="(.*?)")?( title="(.*?)")?\])/',$content,$matches);
	if($match>0){
		foreach($matches[2] as $key=>$id){
			$button = App\Buttons::where('id', $id)->first();
			if($button){
			$savedsettings = unserialize($button->setting);
			$link = $matches[4][$key];
			$title = $matches[6][$key];
			if(!empty($title)){
			$mtitle = preg_replace('/([^A-Za-z0-9\s])/', '\\\\$1', $title, 1);
			$mlink = preg_replace('/([^A-Za-z0-9\s])/', '\\\\$1', $link, 1);
			$pattern = '/(\[button id="'.$id.'" link="'.$mlink.'" title="'.$mtitle.'"\])/';
			$iconleft='';
			$iconright ='';
			if(!empty($savedsettings['icon'])){
				if(!empty($savedsettings['icon_position']) && $savedsettings['icon_position']=='left'){
					$iconleft='<span class="buttonicon '.$savedsettings['icon'].'" style="display: block"></span>';
				}
				if(!empty($savedsettings['icon_position']) && $savedsettings['icon_position']=='right'){
					$iconright = '<span class="buttonicon '.$savedsettings['icon'].'" style="display: block"></span>';
				}
			}

			$content = preg_replace($pattern, '<a href="'.$link.'" class="button '.$savedsettings['button_class'].'">'.$iconleft.$title.$iconright.'</a>', $content);
			}else{
			$title = 'Button';
			$mlink = preg_replace('/([^A-Za-z0-9\s])/', '\\\\$1', $link);
			$pattern = '/(\[button id="'.$id.'" link="'.$mlink.'"\])/';
			$iconleft='';
			$iconright ='';
			if(!empty($savedsettings['icon'])){
				if(!empty($savedsettings['icon_position']) && $savedsettings['icon_position']=='left'){
					$iconleft='<span class="buttonicon '.$savedsettings['icon'].'" style="display: block"></span>';
				}
				if(!empty($savedsettings['icon_position']) && $savedsettings['icon_position']=='right'){
					$iconright = '<span class="buttonicon '.$savedsettings['icon'].'" style="display: block"></span>';
				}
			}
			$content = preg_replace($pattern, '<a href="'.$link.'" class="button '.$savedsettings['button_class'].'">'.$iconleft.$title.$iconright.'</a>', $content);
			}
		  }
		}
	}
	return $content;
}
function listWidgets(){
	$widgets = array('text_widget'=>'Text Widget',
			   'image_widget'=>'Image Widget',
			   'image_scroller_widget'=>'Image Scroller Widget',
			   'image_mapping_widget'=>'Image Mapping Widget',
			   //'parallax_slider_widget'=>'Parallax Slider Widget',
			   'advanced_slider_widget'=>'Advanced Slider Widget',
			   'simple_gallery_widget'=>'Simple Gallery Widget',
			   'isotope_gallery_widget'=>'Isotope Gallery Widget',
			   'social_icons_widget'=>'Social Icons Widget',
			   'social_share_widget'=>'Social Share Widget',
			   'form_widget'=>'Form Widget',
			   'instagram_widget'=>'Instagram Widget',
			   'blog_widget'=>'Blog Widget',
			   'pricing_table' => 'Pricing Table',
			   'countdown_timer' => 'Countdown Timer',
			   'animated_counter' => 'Animated Counter',
			   'site_search' => 'Site Search',
			   'collapse_tab_widget' => 'Collapse/Tab'
			   );
	return $widgets;
}
function WidgetExists($widget){
	$allsavedwidgets=App\Widget::orderBy('id', 'desc')->get();
	$existingwidgets = array();
	foreach ($allsavedwidgets as $savedwidget) {
		$existingwidgets[] = $savedwidget->type;
	}
	if(sizeof($existingwidgets)>0){
		array_unique($existingwidgets);
		if(in_array($widget, $existingwidgets)){
			return true;
		}
	}
	return false;
}
function is_home($slug){
	$generalsettings = App\Settings::where('type', "general")->first();
	if($generalsettings)
		$savedsettings = unserialize($generalsettings->setting);
	if(!empty($savedsettings['home'])){
		$page = App\Pages::find($savedsettings['home']);
		if(!empty($page) && $slug==$page->slug){
			return true;
		}
	}
	return false;
}
function check_load_demo(){
	$posts = App\Post::All();
	$pages = App\Pages::All();
	$widgets = App\Widget::All();
	if($posts->isEmpty() && $pages->isEmpty() && $widgets->isEmpty()){
		return true;
	}
	return false;
}
function getWidgetTitle($type){
	$widgets = listWidgets();
	return !empty($widgets[$type])?$widgets[$type]:'';
}
function getWidgets(){
	$savedwidget = App\Widget::All();
	$startcounter = 402;
	$html = '';
	foreach($savedwidget as $widget){
		$html .= getWidgetSnippet($widget['id'],$widget['title'],$widget['type'],$widget['shortcode'],$startcounter);
		$startcounter++;
	}
	return $html;
}
function getButtons(){
	$buttons = App\Buttons::all();
	$startcounter = 502;
	$html = '';
	if(!empty($buttons)){
	    foreach($buttons as $button){
				if(!empty($button->name)){
					$html .= getButtonSnippet($button->name,$startcounter);
				}
				$startcounter++;
			}
	}
	return $html;
}
function getButtoncss(){
	$buttons = App\Buttons::all();
	$html = '';
	if(!empty($buttons)){
	    foreach($buttons as $button){
			$savedsettings = unserialize($button->setting);
	        $html .= '.'.$button->name.'{';
	          if(!empty($savedsettings['enable_transition'])){
	          $html .='-webkit-transition: all .2s ease-in-out;-moz-transition: all .2s ease-in-out;-o-transition: all .2s ease-in-out;transition: all .2s ease-in-out;';
	          }

	          if(!empty($savedsettings['builder_gradientbg']) && $savedsettings['builder_gradientbg']=='on'){
	            if(!empty($savedsettings['builder_background-color-from'])){
	              $html .='background-color:'.$savedsettings['builder_background-color-from'].';';
	              $html .='background-image: -webkit-gradient(linear, left top, left bottom, from('.$savedsettings['builder_background-color-from'].'), to('.$savedsettings['builder_background-color-to'].'));';
	              $html .='background-image: -webkit-linear-gradient(top, '.$savedsettings['builder_background-color-from'].', '.$savedsettings['builder_background-color-to'].');';
	              $html .='background-image: -moz-linear-gradient(top, '.$savedsettings['builder_background-color-from'].', '.$savedsettings['builder_background-color-to'].');';
	              $html .='background-image: -ms-linear-gradient(top, '.$savedsettings['builder_background-color-from'].', '.$savedsettings['builder_background-color-to'].');';
	              $html .='background-image: -o-linear-gradient(top, '.$savedsettings['builder_background-color-from'].', '.$savedsettings['builder_background-color-to'].');';
	              $html .='background-image: linear-gradient(to bottom, '.$savedsettings['builder_background-color-from'].', '.$savedsettings['builder_background-color-to'].');';
	              $html .='filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='.$savedsettings['builder_background-color-from'].', endColorstr='.$savedsettings['builder_background-color-to'].');';
	            }
	          }

	          if(!empty($savedsettings['builder_background-color']) && empty($savedsettings['builder_gradientbg'])){
	          $html .= 'background-color:'.$savedsettings['builder_background-color'].';';
	          $html .= 'background-image:none;';
	          }

	          if(!empty($savedsettings['builder_color'])){
	          $html .= 'color:'.$savedsettings['builder_color'].';';
	          }

	          if(!empty($savedsettings['builder_font-family'])){
	          $html .= 'font-family:'.$savedsettings['builder_font-family'].';';
	          }

	          if(!empty($savedsettings['builder_font-size'])){
	          $html .= 'font-size:'.$savedsettings['builder_font-size'].'px;';
	          }

	          if(!empty($savedsettings['builder_line-height'])){
	          $html .= 'line-height:'.$savedsettings['builder_line-height'].'px;';
	          }

	          if(!empty($savedsettings['builder_letter-spacing'])){
	          $html .= 'letter-spacing:'.$savedsettings['builder_letter-spacing'].'px;';
	          }

	          if(!empty($savedsettings['builder_word-spacing'])){
	          $html .= 'word-spacing:'.$savedsettings['builder_word-spacing'].'px;';
	          }

	          if(!empty($savedsettings['builder_text-transform'])){
	          $html .= 'text-transform:'.$savedsettings['builder_text-transform'].';';
	          }

	          if(!empty($savedsettings['builder_font-weight'])){
	          $html .= 'font-weight:'.$savedsettings['builder_font-weight'].';';
	          }

	          if(!empty($savedsettings['builder_border-top-left-radius'])){
	          $html .= 'border-top-left-radius:'.$savedsettings['builder_border-top-left-radius'].'px;';
	          }

	          if(!empty($savedsettings['builder_border-top-right-radius'])){
	          $html .= 'border-top-right-radius:'.$savedsettings['builder_border-top-right-radius'].'px;';
	          }

	          if(!empty($savedsettings['builder_border-bottom-left-radius'])){
	          $html .= 'border-bottom-left-radius:'.$savedsettings['builder_border-bottom-left-radius'].'px;';
	          }

	          if(!empty($savedsettings['builder_border-bottom-right-radius'])){
	          $html .= 'border-bottom-right-radius:'.$savedsettings['builder_border-bottom-right-radius'].'px;';
	          }

	          if(!empty($savedsettings['builder_border-left-width'])){
	          $html .= 'border-left-width:'.$savedsettings['builder_border-left-width'].'px;';
	          }

	          if(!empty($savedsettings['builder_border-right-width'])){
	          $html .= 'border-right-width:'.$savedsettings['builder_border-right-width'].'px;';
	          }

	          if(!empty($savedsettings['builder_border-top-width'])){
	          $html .= 'border-top-width:'.$savedsettings['builder_border-top-width'].'px;';
	          }

	          if(!empty($savedsettings['builder_border-bottom-width'])){
	          $html .= 'border-bottom-width:'.$savedsettings['builder_border-bottom-width'].'px;';
	          }

			  if(!empty($savedsettings['builder_border-style'])){
	          $html .= 'border-style:'.$savedsettings['builder_border-style'].';';
	          }

			   if(!empty($savedsettings['builder_border-color'])){
	          $html .= 'border-color:'.$savedsettings['builder_border-color'].';';
	          }

	          if(!empty($savedsettings['builder_padding-left'])){
	          $html .= 'padding-left:'.$savedsettings['builder_padding-left'].'px;';
	          }

	          if(!empty($savedsettings['builder_padding-right'])){
	          $html .= 'padding-right:'.$savedsettings['builder_padding-right'].'px;';
	          }

	          if(!empty($savedsettings['builder_padding-top'])){
	          $html .= 'padding-top:'.$savedsettings['builder_padding-top'].'px;';
	          }

	          if(!empty($savedsettings['builder_padding-bottom'])){
	          $html .= 'padding-bottom:'.$savedsettings['builder_padding-bottom'].'px;';
	          }

						$html .= 'display:inline-block;';
$html .= 'position:relative;';
	          $html .= '}';
	          $html .= '.'.$button->name.':hover{';

	          if(!empty($savedsettings['builder_hover_gradientbg']) && $savedsettings['builder_hover_gradientbg']=='on'){
	            if(!empty($savedsettings['builder_hover_background-color-from'])){
	              $html .='background-color:'.$savedsettings['builder_hover_background-color-from'].';';
	              $html .='background-image: -webkit-gradient(linear, left top, left bottom, from('.$savedsettings['builder_hover_background-color-from'].'), to('.$savedsettings['builder_hover_background-color-to'].'));';
	              $html .='background-image: -webkit-linear-gradient(top, '.$savedsettings['builder_hover_background-color-from'].', '.$savedsettings['builder_hover_background-color-to'].');';
	              $html .='background-image: -moz-linear-gradient(top, '.$savedsettings['builder_hover_background-color-from'].', '.$savedsettings['builder_hover_background-color-to'].');';
	              $html .='background-image: -ms-linear-gradient(top, '.$savedsettings['builder_hover_background-color-from'].', '.$savedsettings['builder_hover_background-color-to'].');';
	              $html .='background-image: -o-linear-gradient(top, '.$savedsettings['builder_hover_background-color-from'].', '.$savedsettings['builder_hover_background-color-to'].');';
	              $html .='background-image: linear-gradient(to bottom, '.$savedsettings['builder_hover_background-color-from'].', '.$savedsettings['builder_hover_background-color-to'].');';
	              $html .='filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='.$savedsettings['builder_hover_background-color-from'].', endColorstr='.$savedsettings['builder_hover_background-color-to'].');';
	            }
	          }

	          if(!empty($savedsettings['builder_hover_background-color']) && empty($savedsettings['builder_hover_gradientbg'])){
	          $html .= 'background-color:'.$savedsettings['builder_hover_background-color'].';';
	          $html .= 'background-image:none;';
	          }

	          if(!empty($savedsettings['builder_hover_color'])){
	          $html .= 'color:'.$savedsettings['builder_hover_color'].';';
	          }

	          if(!empty($savedsettings['builder_hover_font-family'])){
	          $html .= 'font-family:'.$savedsettings['builder_hover_font-family'].';';
	          }

	          if(!empty($savedsettings['builder_hover_font-size'])){
	          $html .= 'font-size:'.$savedsettings['builder_hover_font-size'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_line-height'])){
	          $html .= 'line-height:'.$savedsettings['builder_hover_line-height'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_letter-spacing'])){
	          $html .= 'letter-spacing:'.$savedsettings['builder_hover_letter-spacing'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_word-spacing'])){
	          $html .= 'word-spacing:'.$savedsettings['builder_hover_word-spacing'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_text-transform'])){
	          $html .= 'text-transform:'.$savedsettings['builder_hover_text-transform'].';';
	          }

	          if(!empty($savedsettings['builder_hover_font-weight'])){
	          $html .= 'font-weight:'.$savedsettings['builder_hover_font-weight'].';';
	          }

	          if(!empty($savedsettings['builder_hover_border-top-left-radius'])){
	          $html .= 'border-top-left-radius:'.$savedsettings['builder_hover_border-top-left-radius'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_border-top-right-radius'])){
	          $html .= 'border-top-right-radius:'.$savedsettings['builder_hover_border-top-right-radius'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_border-bottom-left-radius'])){
	          $html .= 'border-bottom-left-radius:'.$savedsettings['builder_hover_border-bottom-left-radius'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_border-bottom-right-radius'])){
	          $html .= 'border-bottom-right-radius:'.$savedsettings['builder_hover_border-bottom-right-radius'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_border-left-width'])){
	          $html .= 'border-left-width:'.$savedsettings['builder_hover_border-left-width'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_border-right-width'])){
	          $html .= 'border-right-width:'.$savedsettings['builder_hover_border-right-width'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_border-top-width'])){
	          $html .= 'border-top-width:'.$savedsettings['builder_hover_border-top-width'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_border-bottom-width'])){
	          $html .= 'border-bottom-width:'.$savedsettings['builder_hover_border-bottom-width'].'px;';
	          }

			  if(!empty($savedsettings['builder_hover_border-style'])){
	          $html .= 'border-style:'.$savedsettings['builder_hover_border-style'].';';
	          }

			   if(!empty($savedsettings['builder_hover_border-color'])){
	          $html .= 'border-color:'.$savedsettings['builder_hover_border-color'].';';
	          }

	          if(!empty($savedsettings['builder_hover_padding-left'])){
	          $html .= 'padding-left:'.$savedsettings['builder_hover_padding-left'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_padding-right'])){
	          $html .= 'padding-right:'.$savedsettings['builder_hover_padding-right'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_padding-top'])){
	          $html .= 'padding-top:'.$savedsettings['builder_hover_padding-top'].'px;';
	          }

	          if(!empty($savedsettings['builder_hover_padding-bottom'])){
	          $html .= 'padding-bottom:'.$savedsettings['builder_hover_padding-bottom'].'px;';
	          }

	          $html .= '}';

			  if(!empty($savedsettings['icon'])){
					ob_start();

					?>
					.<?php echo $button->name ?> span.buttonicon{
						display: none;
						    position: absolute;
							top: 50%;
							transform: translateY(-50%);
							font-size: <?php echo !empty($savedsettings['icon_size'])?$savedsettings['icon_size']:''; ?>px;
							color: <?php echo !empty($savedsettings['icon_color'])?$savedsettings['icon_color']:''; ?>;
							margin-left: <?php echo !empty($savedsettings['icon_space'])?$savedsettings['icon_space']:''; ?>px;
							margin-right: <?php echo !empty($savedsettings['icon_space'])?$savedsettings['icon_space']:''; ?>px;
							<?php if(!empty($savedsettings['enable_transition'])){ ?>
								-webkit-transition: all .2s ease-in-out;
								-moz-transition: all .2s ease-in-out;
								-o-transition: all .2s ease-in-out;
								transition: all .2s ease-in-out;
							<?php } ?>
							<?php if(!empty($savedsettings['icon_showonhover'])){ ?>
							transform: translateY(-50%) scale(0);
							<?php } ?>
							<?php if(!empty($savedsettings['icon_position'])){ ?>
							<?php echo $savedsettings['icon_position'] ?>: 0;
							<?php } ?>
					}
					.<?php echo $button->name ?>:hover span.buttonicon{
						<?php if(!empty($savedsettings['hover_icon_color'])){ ?>
						color: <?php echo $savedsettings['hover_icon_color'] ?>;
						<?php } ?>
						<?php if(!empty($savedsettings['hover_icon_size'])){ ?>
						font-size: <?php echo $savedsettings['hover_icon_size'] ?>px;
						<?php } ?>
						<?php if(!empty($savedsettings['hover_icon_space'])){ ?>
						margin-left: <?php echo $savedsettings['hover_icon_space'] ?>px;
						margin-right: <?php echo $savedsettings['hover_icon_space'] ?>px;
						<?php } ?>
					}
					<?php
					if(!empty($savedsettings['icon_position']) && $savedsettings['icon_position']=='left'){
						$normalpadding = $savedsettings['builder_padding-left']+$savedsettings['icon_space']*2;
						$normalpadding = $normalpadding-5;
					}elseif(!empty($savedsettings['icon_position']) && $savedsettings['icon_position']=='right'){
						$normalpadding = $savedsettings['builder_padding-right']+$savedsettings['icon_space']*2;
						$normalpadding = $normalpadding-5;
					}
					if(!empty($savedsettings['icon_showonhover'])){
					?>
							.<?php echo $button->name ?>:hover {
								padding-<?php echo !empty($savedsettings['icon_position'])?$savedsettings['icon_position']:''; ?>: <?php echo $normalpadding ?>px;
							}
							.<?php echo $button->name ?>:hover span.buttonicon {
								transform: translateY(-50%) scale(1);
							}
					<?php }else{ ?>
							.<?php echo $button->name ?>{
								padding-<?php echo !empty($savedsettings['icon_position'])?$savedsettings['icon_position']:''; ?>: <?php echo $normalpadding ?>px;
							}
							.<?php echo $button->name ?>:hover{
								padding-<?php echo !empty($savedsettings['icon_position'])?$savedsettings['icon_position']:''; ?>: <?php echo $normalpadding ?>px;
							}
					<?php } ?>
					<?php
					$iconcss = ob_get_clean();
					$html .= $iconcss;
			  }
	    }
	}
	return $html;
}
function getBootstrapSnippets(){
	ob_start();
	?>
	<div data-num="900" data-thumb="<?php echo asset('assets/private/images/bootstrap/columns.png'); ?>" data-cat="102">
		<div class="bootstrap">
			<removable contenteditable="false">
				<button class="add_col" contenteditable="false">Add +</button>
			</removable>
			<div class="row">
				<div class="col col-md-12">
					<removable contenteditable="false">
						<button  class="remove_col" contenteditable="false"> x </button><button class="smaller_col" contenteditable="false"> - </button><button class="bigger_col" contenteditable="false"> + </button>
					</removable>
					<p>&nbsp;</p>
				</div>
			</div>
		</div>
	</div>

	<?php
	return ob_get_clean();
}
function getWidgetSnippet($id,$title,$type,$shortcode,$count){
	ob_start();
	?>
	<div data-num="<?php echo $count; ?>" data-thumb="<?php echo asset('assets/private/images/widgets/'.$type.'.jpg'); ?>" data-cat="130">
		<div class="row">
			<div class="col-md-12">
				<?php echo $shortcode ?>
			</div>
		</div>
	</div>
	<?php if($type=='site_search'){
	$settings = App\Widget::where('id', $id)->first();
	$savedsettings = unserialize($settings['setting']);
	?>
	<div data-num="<?php echo $count+200; ?>" data-thumb="<?php echo asset('assets/private/images/widgets/search_results.jpg'); ?>" data-cat="130">
		<div class="row">
			<div class="col-md-12">
				<div id="tipue_search_content"></div>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php
	return ob_get_clean();
}
function getButtonSnippet($name,$count){
	$button = App\Buttons::where('name', $name)->first();
	$savedsettings = unserialize($button->setting);
	ob_start();
	?>
	<div data-num="<?php echo $count; ?>" data-thumb="<?php echo asset('assets/private/images/widgets/custombutton.jpg'); ?>" data-cat="119">
		<div class="row">
			<div class="col-md-12">
			    <a href="#" class="<?php echo $name ?> <?php echo !empty($savedsettings['custom_classes'])?$savedsettings['custom_classes']:''; ?>">
				<?php if(!empty($savedsettings['icon'])){ ?>
					<span class="buttonicon <?php echo !empty($savedsettings['icon'])?$savedsettings['icon']:''; ?>" style="<?php echo !empty($savedsettings['icon_position']) && $savedsettings['icon_position']=='left'?'display: block':''; ?>"></span>
				<?php } ?>
				Button
				<?php if(!empty($savedsettings['icon'])){ ?>
					<span class="buttonicon <?php echo !empty($savedsettings['icon'])?$savedsettings['icon']:''; ?>" style="<?php echo !empty($savedsettings['icon_position']) && $savedsettings['icon_position']=='right'?'display: block':''; ?>"></span>
				<?php } ?>
				</a>
			</div>
		</div>
	</div>
	<?php
	return ob_get_clean();
}

function getCustomSnippetCategories(){
	$savedwidget = App\Widget::All()->count();
	$snippetCategories = '';
	if($savedwidget>0){
		$snippetCategories = '[130, "Widgets"],';
	}
	return $snippetCategories;
}
function getSliderSetting($key,$value){
	$defaults = array(
		'slider_bgtype' => 'transparent',
		'gridwidth'=>  '1240',
		'gridheight'=> '600',
		'sliderType'=> 'standard',
		'sliderLayout'=> 'auto',
		'delay'=> '9000',
		'dottedoverlay'=>'none',
		'shadow'=> '0',
		'spinner'=> 'off',
		'stoploop'=> 'off',
		'stopafterloop'=> '-1',
		'stopatslide'=> '-1',
		'autoheight'=> 'off',
		'hidethumbsonmobile'=> 'off',
		'enablearrow'=> 'true',
		'arrow_style'=> 'gyges',
		'hideonmobile'=> 'true',
		'hideunder'=> '600',
		'hideonleave'=> 'true',
		'l_halign'=> 'left',
		'l_valign'=> 'center',
		'l_horizoff'=> '30',
		'l_vertoff'=> '0',
		'r_halign'=> 'right',
		'r_valign'=> 'center',
		'r_horizoff'=> '30',
		'r_vertoff'=> '0',
		'bgimage' => ''
	);
	if(empty($value)){
		return $defaults[$key];
	}else{
		return $value;
	}
}
function getOptions($key){
	$defaultOptions = array(
		'sliderType' => array('standard'=>'Standard','hero'=>'Hero','carousel'=>'Carousel'),
		'sliderLayout' => array('auto'=>'Auto','fullwidth'=>'Fullwidth','fullscreen'=>'Fullscreen'),
		'shadow'=> array('0'=>'No Shadow','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6'),
		'dottedoverlay'=> array('none'=>'none','twoxtwo'=>'2 x 2 Black','twoxtwowhite'=>'2 x 2 White','threexthree'=>'3 x 3 Black','threexthreewhite'=>'3 x 3 White'),
		'spinner'=> array('off','0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'),
		'arrow_style'=> array('round'=>'Hesperiden','navbar'=>'Gyges','preview1'=>'Hades','preview2'=>'Ares','preview3'=>'Hebe','preview4'=>'Hermes','custom'=>'Custom','round-old'=>'Hephaistos','square-old'=>'Persephone','navbar-old'=>'Erinyen','zeus'=>'Zeus','metis'=>'Metis','dione'=>'Dione','uranus'=>'Uranus'),
		'l_halign'=> array('left'=>'Left','center'=>'Center','right'=>'Right'),
		'l_valign'=> array('top'=>'Top','center'=>'Center','bottom'=>'Bottom'),
		'r_halign'=> array('left'=>'Left','center'=>'Center','right'=>'Right'),
		'r_valign'=> array('top'=>'Top','center'=>'Center','bottom'=>'Bottom'),
		'fontweight' => array('100'=>'100','200'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700','800'=>'800','900'=>'900'),
		'halign' => array('left'=>'Left','right'=>'Right','center'=>'Center'),
		'valign' => array('top'=>'Top','middle'=>'Middle','bottom'=>'Bottom'),
		'slideanimation' => array(
			'Basics' => array('notransition'=>'No Transition', 'fade'=>'Fade','crossfade'=>'Fade Cross','fadethroughdark'=>'Fade Through Black','fadethroughlight'=>'Fade Through Light','fadethroughtransparent'=>'Fade Through Transparent'),
			'Slide Simple' => array('slideup'=> 'Slide To Top', 'slidedown'=>'Slide To Bottom','slideright'=>'Slide To Right','slideleft'=>'Slide To Left','slidehorizontal'=>'Slide Horizontal (Next/Previous)','slidevertical'=>'Slide Vertical (Next/Previous)'),
			'Slide Over'  => array('slideoverup'=>'Slide Over To Top','slideoverdown'=>'Slide Over To Bottom','slideoverright'=>'Slide Over To Right','slideoverleft'=>'Slide Over To Left','slideoverhorizontal'=>'Slide Over Horizontal (Next/Previous)','slideoververtical'=>'Slide Over Vertical (Next/Previous)'),
			'Slide Remove' => array('slideremoveup'=>'Slide Remove To Top','slideremovedown'=>'Slide Remove To Bottom','slideremoveright'=>'Slide Remove To Right','slideremoveleft'=>'Slide Remove To Left','slideremovehorizontal'=>'Slide Remove Horizontal (Next/Previous)','slideremovevertical'=>'Slide Remove Vertical (Next/Previous)'),
			'Sliding Overlays' => array('slidingoverlayup'=>'Sliding Overlays To Top','slidingoverlaydown'=>'Sliding Overlays To Bottom','slidingoverlayright'=>'Sliding Overlays To Right','slidingoverlayleft'=>'Sliding Overlays To Left','slidingoverlayhorizontal'=>'Sliding Overlays Horizontal (Next/Previous)','slidingoverlayvertical'=>'Sliding Overlays Vertical (Next/Previous)'),
			'Slots and Boxes' => array('boxslide'=>'Slide Boxes','slotslide-horizontal'=>'Slide Slots Horizontal','slotslide-vertical'=>'Slide Slots Vertical','boxfade'=>'Fade Boxes','slotfade-horizontal'=>'Fade Slots Horizontal','slotfade-vertical'=>'Fade Slots Vertical'),
			'Fade and Slide' => array('fadefromright'=>'Fade and Slide from Right','fadefromleft'=>'Fade and Slide from Left','fadefromtop'=>'Fade and Slide from Top','fadefrombottom'=>'Fade and Slide from Bottom','fadetoleftfadefromright'=>'To Left From Right','fadetorightfadefromleft'=>'To Right From Left','fadetotopfadefrombottom'=>'To Top From Bottom','fadetobottomfadefromtop'=>'To Bottom From Top'),
			'Parallax' => array('parallaxtoright'=>'Parallax to Right','parallaxtoleft'=>'Parallax to Left','parallaxtotop'=>'Parallax to Top','parallaxtobottom'=>'Parallax to Bottom','parallaxhorizontal'=>'Parallax Horizontal','parallaxvertical'=>'Parallax Vertical'),
			'Zoom Transitions' => array('scaledownfromright'=>'Zoom Out and Fade From Right','scaledownfromleft'=>'Zoom Out and Fade From Left','scaledownfromtop'=>'Zoom Out and Fade From Top','scaledownfrombottom'=>'Zoom Out and Fade From Bottom','zoomout'=>'ZoomOut','zoomin'=>'ZoomIn','slotzoom-horizontal'=>'Zoom Slots Horizontal','slotzoom-vertical'=>'Zoom Slots Vertical'),
			'Curtain Transitions' => array('curtain-1'=>'Curtain from Left','curtain-2'=>'Curtain from Right','curtain-3'=>'Curtain from Middle'),
			'Filter Transitions' => array('grayscale'=>'Grayscale Transition','grayscalecross'=>'Grayscale Cross Transition','brightness'=>'Brightness Transition','brightnesscross'=>'Brightness Cross Transition','blurlight'=>'Blur Light Transition','blurlightcross'=>'Blur Light Cross Transition','blurstrong'=>'Blur Strong Transition','blurstrongcross'=>'Blur Strong Cross Transition'),
			'Premium Transitions' => array('3dcurtain-horizontal'=>'3D Curtain Horizontal','3dcurtain-vertical'=>'3D Curtain Vertical','cube'=>'Cube Vertical','cube-horizontal'=>'Cube Horizontal','incube'=>'In Cube Vertical','incube-horizontal'=>'In Cube Horizontal','turnoff'=>'TurnOff Horizontal','turnoff-vertical'=>'TurnOff Vertical','papercut'=>'Paper Cut','flyin'=>'Fly In'),
			'Random' => array('random-selected'=>'Random of Selected','random-static'=>'Random Flat','random-premium'=>'Random Premium','random'=>'Random Flat and Premium')
		),
		'layeranimation' => array(
			'noanimation' => array('label'=>'No Animation', 'param'=>'[{"delay":0,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'fadein' => array('label'=>'Fade In', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'shortfromtop' => array('label'=>'Short From Top', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'shortfrombottom' => array('label'=>'Short From Bottom', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'),
			'shortfromleft' => array('label'=>'Short From Left', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'shortfromright' => array('label'=>'Short From Right', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"x:50px;opacity:0;","ease":"Power3.easeInOut"}]'),
			'longfromright' => array('label'=>'Long From Right', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'longfromleft' => array('label'=>'Long From Left', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'longfromtop' => array('label'=>'Long From Top', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'longfrombottom' => array('label'=>'Long From Bottom', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'skewfromlongleft' => array('label'=>'Skew From Long Left', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:left;skX:45px;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'skewfromlongright' => array('label'=>'Skew From Long Right', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:right;skX:-85px;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'skewfromshortleft' => array('label'=>'Skew From Short Left', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:-200px;skX:85px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'skewfromshortright' => array('label'=>'Skew From Short Right', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:200px;skX:-85px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'randomrotateandscale' => array('label'=>'Random Rotate And Scale', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:{-250,250};y:{-150,150};rX:{-90,90};rY:{-90,90};rZ:{-360,360};sX:0;sY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'letterflyinfrombottom' => array('label'=>'Letter Fly In From Bottom', 'param'=>'[{"delay":DELAY,"split":"chars","splitdelay":0.05,"speed":SPEEDIN,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'lettersflyinfromleft' => array('label'=>'Letters Fly In From Left', 'param'=>'[{"delay":DELAY,"split":"chars","splitdelay":0.1,"speed":SPEEDIN,"frame":"0","from":"x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'lettersflyinfromright' => array('label'=>'Letters Fly In From Right', 'param'=>'[{"delay":DELAY,"split":"chars","splitdelay":0.05,"speed":SPEEDIN,"frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'lettersflyinfromtop' => array('label'=>'Letters Fly In From Top', 'param'=>'[{"delay":DELAY,"split":"chars","splitdelay":0.05,"speed":SPEEDIN,"frame":"0","from":"y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'maskedzoomout' => array('label'=>'Masked Zoom Out', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'popupsmooth' => array('label'=>'Pop Up Smooth', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'rotateinfrombottom' => array('label'=>'Rotate In From Bottom', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"y:bottom;rZ:90deg;sX:2;sY:2;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'rotateinfromzero' => array('label'=>'Rotate In From Zero', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'slidemaskfrombottom' => array('label'=>'Slide Mask From Bottom', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'slidemaskfromleft' => array('label'=>'Slide Mask From Left', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'slidemaskfromright' => array('label'=>'Slide Mask From Right', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'slidemaskfromtop' => array('label'=>'Slide Mask From Top', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothpopupone' => array('label'=>'Smooth Popup One', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothpopuptwo' => array('label'=>'Smooth Popup Two', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothmaskfromright' => array('label'=>'Smooth Mask From Right', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothmaskfromleft' => array('label'=>'Smooth Mask From Left', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothslidefrombottom' => array('label'=>'Smooth Slide From Bottom', 'param'=>'[{"delay":DELAY,"speed":SPEEDIN,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":SPEEDOUT,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]')
		)
		// https://www.themepunch.com/revsliderjquery-doc/layer-transitions/
	);
	return $defaultOptions[$key];
}
function getformFields(){
	$defaulthtml = '<div class="form-group"><label>Field Label</label><input type="text" class="form-control" name="label[]" value="Name"></div>
		<div class="form-group"><label>Placeholder</label><input type="text" class="form-control" name="placeholder[]"></div>
		<div class="form-group"><label>Description</label><textarea class="form-control" name="description[]"></textarea></div>
		<div class="form-group"><label>Maximum Characters</label><input type="text" class="form-control" name="maxchar[]"></div>
		<div class="form-group"><label><input type="hidden" name="required[]" value=""><input type="checkbox" class="required"> Required</label></div>
		<div class="form-group"><label>Field Label Visibility</label><select class="form-control" name="labelvisibility[]"><option value="visible">Visible</option><option value="hidden">Hidden</option></select></div>
		<div class="form-group"><label>Description Placement</label><select class="form-control" name="descriptionplacement[]"><option value="below">Below</option><option value="above">Above</option></select></div>
		<div class="form-group"><label>Custom Validation Message</label><input type="text" class="form-control" name="custommessage[]"></div>
		<div class="form-group"><label>Custom CSS Class</label><input type="text" class="form-control" name="customcss[]"></div>';
	$defaultOptions = array(
		'standard'=>array(
			'single_line'=>array('title'=>'Single Line Text', 'html'=> '<div class="card"><div class="card-header"><div class="form-group"><label class="field_label">Name</label><div class="help above"></div><input type="text" class="form-control placeholder"><div class="help below"></div></div></div><div class="card-body"><div class="form-group"><label>Default Value</label><input type="text" class="form-control value" name="value[]"></div>'.$defaulthtml.'</div></div>'),
			'paragraph'=>array('title'=>'Paragraph Text', 'html'=> '<div class="card"><div class="card-header"><div class="form-group"><label class="field_label">Name</label><div class="help above"></div><textarea type="text" class="form-control placeholder"></textarea><div class="help below"></div></div></div><div class="card-body"><div class="form-group"><label>Default Value</label><input type="text" class="form-control value" name="value[]"></div>'.$defaulthtml.'</div></div>'),
			'dropdown'=>array('title'=>'Dropdown', 'html'=> '<div class="card"><div class="card-header"><div class="form-group"><label class="field_label">Name</label><div class="help above"></div><select class="form-control"><option value="First Choice">First Choice</option></select><div class="help below"></div></div></div><div class="card-body"><div class="form-group"><label>Default Value</label><input type="text" class="form-control value" value="First Choice" name="value[]"></div>'.$defaulthtml.'</div></div>'),
			'multiselect'=>array('title'=>'Multiselect', 'html'=> '<div class="card"><div class="card-header"><div class="form-group"><label class="field_label">Name</label><div class="help above"></div><select multiple="multiple" size="7" class="form-control"><option value="First Choice">First Choice</option></select><div class="help below"></div></div></div><div class="card-body"><div class="form-group"><label>Default Value</label><input type="text" class="form-control value" value="First Choice" name="value[]"></div>'.$defaulthtml.'</div></div>'),
			'number'=>array('title'=>'Number', 'html'=> '<div class="card"><div class="card-header"><div class="form-group"><label class="field_label">Name</label><div class="help above"></div><input type="number" class="form-control placeholder"><div class="help below"></div></div></div><div class="card-body"><div class="form-group"><label>Default Value</label><input type="text" class="form-control value" name="value[]"></div>'.$defaulthtml.'</div></div>'),
			'checkboxes'=>array('title'=>'Checkboxes', 'html'=> '<div class="card"><div class="card-header"><div class="form-group"><label class="field_label">Name</label><div class="help above"></div><div class="checkbox"><input type="checkbox"> <label>Choice</label></div><div class="help below"></div></div></div><div class="card-body"><div class="form-group"><label>Default Value</label><input type="text" class="form-control value" value="Choice" name="value[]"></div>'.$defaulthtml.'</div></div>'),
			'radio'=>array('title'=>'Radio', 'html'=> '<div class="card"><div class="card-header"><div class="form-group"><label class="field_label">Name</label><div class="help above"></div><div class="radio"><input type="radio"> <label>Choice</label></div><div class="help below"></div></div></div><div class="card-body"><div class="form-group"><label>Default Value</label><input type="text" class="form-control value" value="Choice" name="value[]"></div>'.$defaulthtml.'</div></div>'),
			'hidden'=>array('title'=>'Hidden', 'html'=> '<div class="card"><div class="card-header"><div class="form-group"><div class="help above"></div><input type="text" class="form-control placeholder"><div class="help below"></div></div></div><div class="card-body"><div class="form-group"><label>Default Value</label><input type="text" class="form-control value" name="value[]"></div>'.$defaulthtml.'</div></div>'),
			'html'=>array('title'=>'Html', 'html'=> '<div class="card"><div class="card-header"><div class="form-group"><label class="field_label">Name</label><div class="help above"></div><textarea type="text" class="form-control placeholder"></textarea><div class="help below"></div></div></div><div class="card-body"><div class="form-group"><label>HTML</label><textarea class="form-control value" name="value[]"></textarea></div>'.$defaulthtml.'</div></div>'),
		)
	);
	return $defaultOptions;
}
function getformInput($type, $name, $placeholder='', $value='', $view=false){
	$id = '';
	if($name!=''){
		$id = $name;
		$name = "name='".$name."'";
	}
	$defaultOptions = array(
			'single_line'=>'<input type="text" class="form-control placeholder" '.$name.' id="'.$id.'" placeholder="'.$placeholder.'" value="'.$value.'">',
			'paragraph'=>'<textarea type="text" class="form-control placeholder" '.$name.' id="'.$id.'" placeholder="'.$placeholder.'">'.$value.'</textarea>',
			'number'=>'<input type="number" class="form-control placeholder" '.$name.' id="'.$id.'" placeholder="'.$placeholder.'" value="'.$value.'">',
			'hidden'=>'<input type="text" class="form-control placeholder" '.$name.' id="'.$id.'" value="'.$value.'">',
			'html'=>'<textarea type="text" id="'.$id.'" class="form-control placeholder" '.$name.'>'.$value.'</textarea>',
	);
	if($type == 'dropdown'){
		$html = '<select class="form-control" id="'.$id.'" '.$name.'>';
		$array = explode(',', $value);
		foreach($array as $val){
			$selected = '';
			$options = explode(':', $val);
			if(sizeof($options)>1){
			$default = strpos($options[1],'*');
			if($default!=''){
				$selected = 'selected';
			}
			$html.='<option value="'.$options[0].'" '.$selected.'>'.$options[1].'</option>';
			}else{
			$default = strpos($val,'*');
			if($default!=''){
				$selected = 'selected';
			}
			$html.='<option value="'.$val.'" '.$selected.'>'.$val.'</option>';
			}
		}
		$html .= '</select>';
		$defaultOptions['dropdown'] = $html;
	}
	if($type == 'multiselect'){
		$html = '<select multiple="multiple" id="'.$id.'" size="7" class="form-control" '.$name.'>';
		$array = explode(',', $value);
		foreach($array as $val){
			$selected = '';
			$options = explode(':', $val);
			if(sizeof($options)>1){
				$default = strpos($options[1],'*');
				if($default!=''){
					$selected = 'selected';
				}
				$html.='<option value="'.$options[0].'" '.$selected.'>'.$options[1].'</option>';
			}else{
				$default = strpos($val,'*');
				if($default!=''){
					$selected = 'selected';
				}
				$html.='<option value="'.$val.'" '.$selected.'>'.$val.'</option>';
			}
		}
		$html .= '</select>';
		$defaultOptions['multiselect'] = $html;
	}
	if($type == 'checkboxes'){
		$html = '';
		$array = explode(',', $value);
		foreach($array as $val){
			$selected = '';
			$options = explode(':', $val);
			$rand = mt_rand();
			if(sizeof($options)>1){
				$default = strpos($options[1],'*');
				if($default!=''){
					$selected = 'checked';
				}
				$html.='<div class="checkbox"><input type="checkbox" id="checkbox_'.$rand.'" value="'.$options[0].'" '.$selected.'> <label for="checkbox_'.$rand.'">'.$options[1].'</label></div>';
			}else{
				$default = strpos($val,'*');
				if($default!=''){
					$selected = 'checked';
				}
				$html.='<div class="checkbox"><input type="checkbox" id="checkbox_'.$rand.'" value="'.$val.'" '.$selected.'> <label for="checkbox_'.$rand.'">'.$val.'</label></div>';
			}
		}
		$defaultOptions['checkboxes'] = $html.'<input class="not-visible" type="text" '.$name.'>';
	}
	if($type == 'radio'){
		$html = '';
		$array = explode(',', $value);
		foreach($array as $val){
			$selected = '';
			$options = explode(':', $val);
			$rand = mt_rand();
			if(sizeof($options)>1){
				$default = strpos($options[1],'*');
				if($default!=''){
					$selected = 'checked';
				}
				$html.='<div class="radio"><input type="radio" id="radio_'.$rand.'" value="'.$options[0].'" '.$selected.'> <label for="radio_'.$rand.'">'.$options[1].'</label></div>';
			}else{
				$default = strpos($val,'*');
				if($default!=''){
					$selected = 'checked';
				}
				$html.='<div class="radio"><input type="radio" id="radio_'.$rand.'" value="'.$val.'" '.$selected.'> <label for="radio_'.$rand.'">'.$val.'</label></div>';
			}
		}
		$defaultOptions['radio'] = $html.'<input class="not-visible" type="text" '.$name.'>';
	}
	if($view){
		$defaultOptions['html'] = "<div class='html'>".$value."</div>";
		$defaultOptions['hidden'] = '<input type="hidden" class="form-control" '.$name.' value="'.$value.'">';
	}
	return $defaultOptions[$type];
}
function generateHubspot($id,$submissiontype,$hubspotkey){
	$widget = App\Widget::find($id);
	if($submissiontype=='add'){
		if($widget->type=='form_widget'){
			if(!empty($widget->setting)){
				$settings = unserialize($widget->setting);
				$form = array();
				$form['name'] = \Config::get('app.name').'_Form_'.$widget->id;
				$form['method'] = "";
				$form['redirect'] = !empty($settings['redirect'])?$settings['redirect']:'';
				$form['submitText'] = "Submit";
				$form['notifyRecipients'] = !empty($settings['recipients'])?$settings['recipients']:'';
				$form['migratedFrom'] = "ld";
				$form['ignoreCurrentValues'] = false;
				$form['deletable'] = true;
				$form['formFieldGroups'] = array();
				foreach($settings['type'] as $key=>$field){
					$form['formFieldGroups'][] = array('fields'=>array(
						"name" => !empty($settings['name'][$key])?$settings['name'][$key]:'',
						"label" => !empty($settings['label'][$key])?$settings['label'][$key]:'',
						"type" => "string",
						"fieldType" => "text",
						"description" => "",
						"required" => false,
						"enabled" => true,
						"hidden" => false,
						"defaultValue"=>"",
						"isSmartField"=> false,
						"unselectedLabel"=>"",
						"placeholder"=> !empty($settings['placeholder'][$key])?$settings['placeholder'][$key]:''
					));
				}
				$data_string = json_encode($form);
				$ch = curl_init();
				curl_setopt_array($ch, [
					CURLOPT_URL => "https://api.hubapi.com/forms/v2/forms?hapikey=".$hubspotkey,
					CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Authorization: Bearer '.$hubspotkey),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => $data_string
				]);
				$response = curl_exec($ch);
				curl_close($ch);
				return json_decode($response);
			}
		}
	}else if($submissiontype=='edit'){
		if($widget->type=='form_widget'){
			if(!empty($widget->setting)){
				$settings = unserialize($widget->setting);
				$form = array();
				$form['name'] = \Config::get('app.name').'_Form_'.$widget->id;
				$form['method'] = "";
				$form['redirect'] = !empty($settings['redirect'])?$settings['redirect']:'';
				$form['submitText'] = "Submit";
				$form['notifyRecipients'] = !empty($settings['recipients'])?$settings['recipients']:'';
				$form['migratedFrom'] = "ld";
				$form['ignoreCurrentValues'] = false;
				$form['deletable'] = true;
				$form['formFieldGroups'] = array();
				foreach($settings['type'] as $key=>$field){
					$form['formFieldGroups'][] = array('fields'=>array(
						"name" => !empty($settings['name'][$key])?$settings['name'][$key]:'',
						"label" => !empty($settings['label'][$key])?$settings['label'][$key]:'',
						"type" => "string",
						"fieldType" => "text",
						"description" => "",
						"required" => false,
						"enabled" => true,
						"hidden" => false,
						"defaultValue"=>"",
						"isSmartField"=> false,
						"unselectedLabel"=>"",
						"placeholder"=> !empty($settings['placeholder'][$key])?$settings['placeholder'][$key]:''
					));
				}
				$data_string = json_encode($form);
				$ch = curl_init();
				$guid = !empty($settings['guid'])?$settings['guid']:'';
				curl_setopt_array($ch, [
					CURLOPT_URL => "https://api.hubapi.com/forms/v2/forms/".$guid."?hapikey=".$hubspotkey,
					CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Authorization: Bearer '.$hubspotkey),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => $data_string
				]);
				$response = curl_exec($ch);
				curl_close($ch);
				return json_decode($response);
			}
		}
	}
}
function generateNewsletter($prefix,$html){
	$rootPath = public_path('/html/newsletter');
	$client = Storage::createLocalDriver(['root' => $rootPath]);
	$client->put($prefix.'_email.html', cleanHTML($html));
}

function generateCustomCSS(){
	$view = view('layouts.custom');
	$demo = $view->render();
	$currenttheme = App\theme::getCurrentTheme();
	$view = view('templates.'.$currenttheme['name'].'.customcss');
	$demotheme = $view->render();
	$demo = $demo.$demotheme;
	$contents = preg_replace('/\/?html\//','../',$demo);
	$rootPath = public_path('/html/assets/');
	$client = Storage::createLocalDriver(['root' => $rootPath]);
	$client->put('theme-demo.css', $demo);
	$client->put('theme.css', $contents);
}
function checkCloudflareStatus(){
	if(!empty(getSetting('zone_id', 'cloudflare'))){
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/".getSetting('zone_id', 'cloudflare')."/",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				'X-Auth-Email: '.getSetting('cloud_email', 'cloudflare').'',
				'X-Auth-Key: '.getSetting('cloud_apikey', 'cloudflare').'',
				'Cache-Control: no-cache',
				'Content-Type:application/json',
				'purge_everything: true'
				)
		]);
		$response = json_decode(curl_exec($ch));
		if($response->success){
			return $response->result->status;
		}else{
			return 'fail';
		}
	}
}
function removeHtmlTagsOfField(string $field){
	$content=htmlentities(strip_tags($field), ENT_QUOTES, 'UTF-8');
	$content = trim(preg_replace('/(\[widget.*?\])/', '',$content));
	$content = preg_replace('/\s+/', ' ', $content);
    return $content;
}
function checkCategoryExists($slug, $id){
	$post = App\Post::find($id);
	if(!empty($post->categories)){
			$categories = explode(',',$post->categories);
			if(in_array($slug, $categories)){
				return true;
			}else{
				return false;
			}
	}
}
function getPopups(){
	$popups = App\Popup::All();
	ob_start();
	if(!empty($popups)){
	foreach($popups as $popup){
		?>
		<script>
		jQuery(document).ready(function(){
			jQuery(document).on('click', '.<?php echo $popup->class ?>',function(){
				jQuery('#<?php echo $popup->class ?>').modal('show');
				return false;
			})
			<?php if(unserialize($popup->settings)['trigger']=='auto'){ ?>
					if(!Cookies.get('<?php echo $popup->class ?>')){
					setTimeout(function(){
						jQuery('#<?php echo $popup->class ?>').modal('show');
					},<?php echo !empty(unserialize($popup->settings)['delay'])?unserialize($popup->settings)['delay']:'1000'; ?>);
					}
					<?php if(!empty(unserialize($popup->settings)['enablecookie'])){ ?>
					jQuery('#<?php echo $popup->class ?>').on('hidden.bs.modal', function () {
						Cookies.set('<?php echo $popup->class ?>', 'cookieset', { expires: <?php echo !empty(unserialize($popup->settings)['cookieexpiry'])?unserialize($popup->settings)['cookieexpiry']:'7' ?> });
					});
					<?php }else{ ?>
						Cookies.remove('<?php echo $popup->class ?>');
					<?php } ?>
			<?php } ?>
		})
		</script>
		<div id="<?php echo $popup->class ?>" class="popups modal fade
		<?php
		if(!empty(unserialize($popup->settings)['position'])){
			if(unserialize($popup->settings)['position']=='modal-top'){
				echo "top";
			}elseif(unserialize($popup->settings)['position']=='modal-top'){
				echo "bottom";
			}elseif(unserialize($popup->settings)['position']=='modal-top-right' || unserialize($popup->settings)['position']=='modal-right' || unserialize($popup->settings)['position']=='modal-bottom-right'){
				echo "right";
			}elseif(unserialize($popup->settings)['position']=='modal-top-left' || unserialize($popup->settings)['position']=='modal-left' || unserialize($popup->settings)['position']=='modal-bottom-left'){
				echo "left";
			}
		}
		?>
		" role="dialog">
		  <div class="modal-dialog
		    <?php
			if(!empty(unserialize($popup->settings)['position'])){
				echo unserialize($popup->settings)['position']." ";
				if(unserialize($popup->settings)['position']=='modal-top-right' || unserialize($popup->settings)['position']=='modal-right' || unserialize($popup->settings)['position']=='modal-bottom-right' || unserialize($popup->settings)['position']=='modal-top-left' || unserialize($popup->settings)['position']=='modal-left' || unserialize($popup->settings)['position']=='modal-bottom-left'){
					echo "modal-side ";
				}
				if(unserialize($popup->settings)['position']=='modal-right' || unserialize($popup->settings)['position']=='modal-left'){
					echo "modal-full-height ";
				}
			}
			?>
		  " style="<?php if(!empty(unserialize($popup->settings)['width'])) echo 'width:100%;max-width:'.unserialize($popup->settings)['width'].'px;'; ?>">
		    <div class="modal-content" style="<?php if(!empty(unserialize($popup->settings)['bg_color'])) echo 'background-color:'.unserialize($popup->settings)['bg_color'].';'; ?>">
					<button type="button" class="close" data-dismiss="modal" style="font-size:<?php echo !empty(unserialize($popup->settings)['btn_size'])?unserialize($popup->settings)['btn_size'].'px':'21px' ?>;<?php if(!empty(unserialize($popup->settings)['btn_color'])) echo 'color:'.unserialize($popup->settings)['btn_color'].';'; ?>">&times;</button>
					<?php if(!empty(unserialize($popup->settings)['displayheader'])): ?>
		      <div class="modal-header" <?php if(empty(unserialize($popup->settings)['displayheader'])) echo 'style="border-bottom:0;"'; ?>>
		        <h4 class="modal-title"><?php echo $popup->title ?></h4>
		      </div>
					<?php endif; ?>
		      <div class="modal-body">
						<div class="is-wrapper">
		        <?php echo filterOutput($popup->html) ?>
						</div>
		      </div>
					<?php if(!empty(unserialize($popup->settings)['displayfooter'])): ?>
					<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
					<?php endif; ?>
		    </div>

		  </div>
		</div>
		<?php
		}
	}
	return ob_get_clean();
}
function get_curl_content($url){
	if (!function_exists('curl_init')){
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}


// Firebase Functions
function firebaseAddPagePassword($page, $password){
	$password = base64_encode($password);
	$pagedata = App\Pages::find($page);
	$data = array("password" => $password, "html"=> filterOutput($pagedata->html));
	$url = getSetting('databaseURL', 'firebase').'/pages/'.$page.'.json';
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
	$response = curl_exec($ch);
}
function firebaseDeletePagePassword($page){
	$url = getSetting('databaseURL', 'firebase').'/pages/'.$page.'.json';
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	$response = curl_exec($ch);
}
function firebaseDeleteAllPagesPassword(){
	$url = getSetting('databaseURL', 'firebase').'/pages.json';
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	$response = curl_exec($ch);
}
function getIcons(){
	$icons = array('icon_ion-alert'=>'icon ion-alert', 'icon_ion-alert-circled'=>'icon ion-alert-circled', 'icon_ion-android-add'=>'icon ion-android-add', 'icon_ion-android-add-circle'=>'icon ion-android-add-circle', 'icon_ion-android-alarm-clock'=>'icon ion-android-alarm-clock', 'icon_ion-android-alert'=>'icon ion-android-alert', 'icon_ion-android-apps'=>'icon ion-android-apps', 'icon_ion-android-archive'=>'icon ion-android-archive', 'icon_ion-android-arrow-back'=>'icon ion-android-arrow-back', 'icon_ion-android-arrow-down'=>'icon ion-android-arrow-down', 'icon_ion-android-arrow-dropdown'=>'icon ion-android-arrow-dropdown', 'icon_ion-android-arrow-dropdown-circle'=>'icon ion-android-arrow-dropdown-circle', 'icon_ion-android-arrow-dropleft'=>'icon ion-android-arrow-dropleft', 'icon_ion-android-arrow-dropleft-circle'=>'icon ion-android-arrow-dropleft-circle', 'icon_ion-android-arrow-dropright'=>'icon ion-android-arrow-dropright', 'icon_ion-android-arrow-dropright-circle'=>'icon ion-android-arrow-dropright-circle', 'icon_ion-android-arrow-dropup'=>'icon ion-android-arrow-dropup', 'icon_ion-android-arrow-dropup-circle'=>'icon ion-android-arrow-dropup-circle', 'icon_ion-android-arrow-forward'=>'icon ion-android-arrow-forward', 'icon_ion-android-arrow-up'=>'icon ion-android-arrow-up', 'icon_ion-android-attach'=>'icon ion-android-attach', 'icon_ion-android-bar'=>'icon ion-android-bar', 'icon_ion-android-bicycle'=>'icon ion-android-bicycle', 'icon_ion-android-boat'=>'icon ion-android-boat', 'icon_ion-android-bookmark'=>'icon ion-android-bookmark', 'icon_ion-android-bulb'=>'icon ion-android-bulb', 'icon_ion-android-bus'=>'icon ion-android-bus', 'icon_ion-android-calendar'=>'icon ion-android-calendar', 'icon_ion-android-call'=>'icon ion-android-call', 'icon_ion-android-camera'=>'icon ion-android-camera', 'icon_ion-android-cancel'=>'icon ion-android-cancel', 'icon_ion-android-car'=>'icon ion-android-car', 'icon_ion-android-cart'=>'icon ion-android-cart', 'icon_ion-android-chat'=>'icon ion-android-chat', 'icon_ion-android-checkbox'=>'icon ion-android-checkbox', 'icon_ion-android-checkbox-blank'=>'icon ion-android-checkbox-blank', 'icon_ion-android-checkbox-outline'=>'icon ion-android-checkbox-outline', 'icon_ion-android-checkbox-outline-blank'=>'icon ion-android-checkbox-outline-blank', 'icon_ion-android-checkmark-circle'=>'icon ion-android-checkmark-circle', 'icon_ion-android-clipboard'=>'icon ion-android-clipboard', 'icon_ion-android-close'=>'icon ion-android-close', 'icon_ion-android-cloud'=>'icon ion-android-cloud', 'icon_ion-android-cloud-circle'=>'icon ion-android-cloud-circle', 'icon_ion-android-cloud-done'=>'icon ion-android-cloud-done', 'icon_ion-android-cloud-outline'=>'icon ion-android-cloud-outline', 'icon_ion-android-color-palette'=>'icon ion-android-color-palette', 'icon_ion-android-compass'=>'icon ion-android-compass', 'icon_ion-android-contact'=>'icon ion-android-contact', 'icon_ion-android-contacts'=>'icon ion-android-contacts', 'icon_ion-android-contract'=>'icon ion-android-contract', 'icon_ion-android-create'=>'icon ion-android-create', 'icon_ion-android-delete'=>'icon ion-android-delete', 'icon_ion-android-desktop'=>'icon ion-android-desktop', 'icon_ion-android-document'=>'icon ion-android-document', 'icon_ion-android-done'=>'icon ion-android-done', 'icon_ion-android-done-all'=>'icon ion-android-done-all', 'icon_ion-android-download'=>'icon ion-android-download', 'icon_ion-android-drafts'=>'icon ion-android-drafts', 'icon_ion-android-exit'=>'icon ion-android-exit', 'icon_ion-android-expand'=>'icon ion-android-expand', 'icon_ion-android-favorite'=>'icon ion-android-favorite', 'icon_ion-android-favorite-outline'=>'icon ion-android-favorite-outline', 'icon_ion-android-film'=>'icon ion-android-film', 'icon_ion-android-folder'=>'icon ion-android-folder', 'icon_ion-android-folder-open'=>'icon ion-android-folder-open', 'icon_ion-android-funnel'=>'icon ion-android-funnel', 'icon_ion-android-globe'=>'icon ion-android-globe', 'icon_ion-android-hand'=>'icon ion-android-hand', 'icon_ion-android-hangout'=>'icon ion-android-hangout', 'icon_ion-android-happy'=>'icon ion-android-happy', 'icon_ion-android-home'=>'icon ion-android-home', 'icon_ion-android-image'=>'icon ion-android-image', 'icon_ion-android-laptop'=>'icon ion-android-laptop', 'icon_ion-android-list'=>'icon ion-android-list', 'icon_ion-android-locate'=>'icon ion-android-locate', 'icon_ion-android-lock'=>'icon ion-android-lock', 'icon_ion-android-mail'=>'icon ion-android-mail', 'icon_ion-android-map'=>'icon ion-android-map', 'icon_ion-android-menu'=>'icon ion-android-menu', 'icon_ion-android-microphone'=>'icon ion-android-microphone', 'icon_ion-android-microphone-off'=>'icon ion-android-microphone-off', 'icon_ion-android-more-horizontal'=>'icon ion-android-more-horizontal', 'icon_ion-android-more-vertical'=>'icon ion-android-more-vertical', 'icon_ion-android-navigate'=>'icon ion-android-navigate', 'icon_ion-android-notifications'=>'icon ion-android-notifications', 'icon_ion-android-notifications-none'=>'icon ion-android-notifications-none', 'icon_ion-android-notifications-off'=>'icon ion-android-notifications-off', 'icon_ion-android-open'=>'icon ion-android-open', 'icon_ion-android-options'=>'icon ion-android-options', 'icon_ion-android-people'=>'icon ion-android-people', 'icon_ion-android-person'=>'icon ion-android-person', 'icon_ion-android-person-add'=>'icon ion-android-person-add', 'icon_ion-android-phone-landscape'=>'icon ion-android-phone-landscape', 'icon_ion-android-phone-portrait'=>'icon ion-android-phone-portrait', 'icon_ion-android-pin'=>'icon ion-android-pin', 'icon_ion-android-plane'=>'icon ion-android-plane', 'icon_ion-android-playstore'=>'icon ion-android-playstore', 'icon_ion-android-print'=>'icon ion-android-print', 'icon_ion-android-radio-button-off'=>'icon ion-android-radio-button-off', 'icon_ion-android-radio-button-on'=>'icon ion-android-radio-button-on', 'icon_ion-android-refresh'=>'icon ion-android-refresh', 'icon_ion-android-remove'=>'icon ion-android-remove', 'icon_ion-android-remove-circle'=>'icon ion-android-remove-circle', 'icon_ion-android-restaurant'=>'icon ion-android-restaurant', 'icon_ion-android-sad'=>'icon ion-android-sad', 'icon_ion-android-search'=>'icon ion-android-search', 'icon_ion-android-send'=>'icon ion-android-send', 'icon_ion-android-settings'=>'icon ion-android-settings', 'icon_ion-android-share'=>'icon ion-android-share', 'icon_ion-android-share-alt'=>'icon ion-android-share-alt', 'icon_ion-android-star'=>'icon ion-android-star', 'icon_ion-android-star-half'=>'icon ion-android-star-half', 'icon_ion-android-star-outline'=>'icon ion-android-star-outline', 'icon_ion-android-stopwatch'=>'icon ion-android-stopwatch', 'icon_ion-android-subway'=>'icon ion-android-subway', 'icon_ion-android-sunny'=>'icon ion-android-sunny', 'icon_ion-android-sync'=>'icon ion-android-sync', 'icon_ion-android-textsms'=>'icon ion-android-textsms', 'icon_ion-android-time'=>'icon ion-android-time', 'icon_ion-android-train'=>'icon ion-android-train', 'icon_ion-android-unlock'=>'icon ion-android-unlock', 'icon_ion-android-upload'=>'icon ion-android-upload', 'icon_ion-android-volume-down'=>'icon ion-android-volume-down', 'icon_ion-android-volume-mute'=>'icon ion-android-volume-mute', 'icon_ion-android-volume-off'=>'icon ion-android-volume-off', 'icon_ion-android-volume-up'=>'icon ion-android-volume-up', 'icon_ion-android-walk'=>'icon ion-android-walk', 'icon_ion-android-warning'=>'icon ion-android-warning', 'icon_ion-android-watch'=>'icon ion-android-watch', 'icon_ion-android-wifi'=>'icon ion-android-wifi', 'icon_ion-aperture'=>'icon ion-aperture', 'icon_ion-archive'=>'icon ion-archive', 'icon_ion-arrow-down-a'=>'icon ion-arrow-down-a', 'icon_ion-arrow-down-b'=>'icon ion-arrow-down-b', 'icon_ion-arrow-down-c'=>'icon ion-arrow-down-c', 'icon_ion-arrow-expand'=>'icon ion-arrow-expand', 'icon_ion-arrow-graph-down-left'=>'icon ion-arrow-graph-down-left', 'icon_ion-arrow-graph-down-right'=>'icon ion-arrow-graph-down-right', 'icon_ion-arrow-graph-up-left'=>'icon ion-arrow-graph-up-left', 'icon_ion-arrow-graph-up-right'=>'icon ion-arrow-graph-up-right', 'icon_ion-arrow-left-a'=>'icon ion-arrow-left-a', 'icon_ion-arrow-left-b'=>'icon ion-arrow-left-b', 'icon_ion-arrow-left-c'=>'icon ion-arrow-left-c', 'icon_ion-arrow-move'=>'icon ion-arrow-move', 'icon_ion-arrow-resize'=>'icon ion-arrow-resize', 'icon_ion-arrow-return-left'=>'icon ion-arrow-return-left', 'icon_ion-arrow-return-right'=>'icon ion-arrow-return-right', 'icon_ion-arrow-right-a'=>'icon ion-arrow-right-a', 'icon_ion-arrow-right-b'=>'icon ion-arrow-right-b', 'icon_ion-arrow-right-c'=>'icon ion-arrow-right-c', 'icon_ion-arrow-shrink'=>'icon ion-arrow-shrink', 'icon_ion-arrow-swap'=>'icon ion-arrow-swap', 'icon_ion-arrow-up-a'=>'icon ion-arrow-up-a', 'icon_ion-arrow-up-b'=>'icon ion-arrow-up-b', 'icon_ion-arrow-up-c'=>'icon ion-arrow-up-c', 'icon_ion-asterisk'=>'icon ion-asterisk', 'icon_ion-at'=>'icon ion-at', 'icon_ion-backspace'=>'icon ion-backspace', 'icon_ion-backspace-outline'=>'icon ion-backspace-outline', 'icon_ion-bag'=>'icon ion-bag', 'icon_ion-battery-charging'=>'icon ion-battery-charging', 'icon_ion-battery-empty'=>'icon ion-battery-empty', 'icon_ion-battery-full'=>'icon ion-battery-full', 'icon_ion-battery-half'=>'icon ion-battery-half', 'icon_ion-battery-low'=>'icon ion-battery-low', 'icon_ion-beaker'=>'icon ion-beaker', 'icon_ion-beer'=>'icon ion-beer', 'icon_ion-bluetooth'=>'icon ion-bluetooth', 'icon_ion-bonfire'=>'icon ion-bonfire', 'icon_ion-bookmark'=>'icon ion-bookmark', 'icon_ion-bowtie'=>'icon ion-bowtie', 'icon_ion-briefcase'=>'icon ion-briefcase', 'icon_ion-bug'=>'icon ion-bug', 'icon_ion-calculator'=>'icon ion-calculator', 'icon_ion-calendar'=>'icon ion-calendar', 'icon_ion-camera'=>'icon ion-camera', 'icon_ion-card'=>'icon ion-card', 'icon_ion-cash'=>'icon ion-cash', 'icon_ion-chatbox'=>'icon ion-chatbox', 'icon_ion-chatbox-working'=>'icon ion-chatbox-working', 'icon_ion-chatboxes'=>'icon ion-chatboxes', 'icon_ion-chatbubble'=>'icon ion-chatbubble', 'icon_ion-chatbubble-working'=>'icon ion-chatbubble-working', 'icon_ion-chatbubbles'=>'icon ion-chatbubbles', 'icon_ion-checkmark'=>'icon ion-checkmark', 'icon_ion-checkmark-circled'=>'icon ion-checkmark-circled', 'icon_ion-checkmark-round'=>'icon ion-checkmark-round', 'icon_ion-chevron-down'=>'icon ion-chevron-down', 'icon_ion-chevron-left'=>'icon ion-chevron-left', 'icon_ion-chevron-right'=>'icon ion-chevron-right', 'icon_ion-chevron-up'=>'icon ion-chevron-up', 'icon_ion-clipboard'=>'icon ion-clipboard', 'icon_ion-clock'=>'icon ion-clock', 'icon_ion-close'=>'icon ion-close', 'icon_ion-close-circled'=>'icon ion-close-circled', 'icon_ion-close-round'=>'icon ion-close-round', 'icon_ion-closed-captioning'=>'icon ion-closed-captioning', 'icon_ion-cloud'=>'icon ion-cloud', 'icon_ion-code'=>'icon ion-code', 'icon_ion-code-download'=>'icon ion-code-download', 'icon_ion-code-working'=>'icon ion-code-working', 'icon_ion-coffee'=>'icon ion-coffee', 'icon_ion-compass'=>'icon ion-compass', 'icon_ion-compose'=>'icon ion-compose', 'icon_ion-connection-bars'=>'icon ion-connection-bars', 'icon_ion-contrast'=>'icon ion-contrast', 'icon_ion-crop'=>'icon ion-crop', 'icon_ion-cube'=>'icon ion-cube', 'icon_ion-disc'=>'icon ion-disc', 'icon_ion-document'=>'icon ion-document', 'icon_ion-document-text'=>'icon ion-document-text', 'icon_ion-drag'=>'icon ion-drag', 'icon_ion-earth'=>'icon ion-earth', 'icon_ion-easel'=>'icon ion-easel', 'icon_ion-edit'=>'icon ion-edit', 'icon_ion-egg'=>'icon ion-egg', 'icon_ion-eject'=>'icon ion-eject', 'icon_ion-email'=>'icon ion-email', 'icon_ion-email-unread'=>'icon ion-email-unread', 'icon_ion-erlenmeyer-flask'=>'icon ion-erlenmeyer-flask', 'icon_ion-erlenmeyer-flask-bubbles'=>'icon ion-erlenmeyer-flask-bubbles', 'icon_ion-eye'=>'icon ion-eye', 'icon_ion-eye-disabled'=>'icon ion-eye-disabled', 'icon_ion-female'=>'icon ion-female', 'icon_ion-filing'=>'icon ion-filing', 'icon_ion-film-marker'=>'icon ion-film-marker', 'icon_ion-fireball'=>'icon ion-fireball', 'icon_ion-flag'=>'icon ion-flag', 'icon_ion-flame'=>'icon ion-flame', 'icon_ion-flash'=>'icon ion-flash', 'icon_ion-flash-off'=>'icon ion-flash-off', 'icon_ion-folder'=>'icon ion-folder', 'icon_ion-fork'=>'icon ion-fork', 'icon_ion-fork-repo'=>'icon ion-fork-repo', 'icon_ion-forward'=>'icon ion-forward', 'icon_ion-funnel'=>'icon ion-funnel', 'icon_ion-gear-a'=>'icon ion-gear-a', 'icon_ion-gear-b'=>'icon ion-gear-b', 'icon_ion-grid'=>'icon ion-grid', 'icon_ion-hammer'=>'icon ion-hammer', 'icon_ion-happy'=>'icon ion-happy', 'icon_ion-happy-outline'=>'icon ion-happy-outline', 'icon_ion-headphone'=>'icon ion-headphone', 'icon_ion-heart'=>'icon ion-heart', 'icon_ion-heart-broken'=>'icon ion-heart-broken', 'icon_ion-help'=>'icon ion-help', 'icon_ion-help-buoy'=>'icon ion-help-buoy', 'icon_ion-help-circled'=>'icon ion-help-circled', 'icon_ion-home'=>'icon ion-home', 'icon_ion-icecream'=>'icon ion-icecream', 'icon_ion-image'=>'icon ion-image', 'icon_ion-images'=>'icon ion-images', 'icon_ion-information'=>'icon ion-information', 'icon_ion-information-circled'=>'icon ion-information-circled', 'icon_ion-ionic'=>'icon ion-ionic', 'icon_ion-ios-alarm'=>'icon ion-ios-alarm', 'icon_ion-ios-alarm-outline'=>'icon ion-ios-alarm-outline', 'icon_ion-ios-albums'=>'icon ion-ios-albums', 'icon_ion-ios-albums-outline'=>'icon ion-ios-albums-outline', 'icon_ion-ios-americanfootball'=>'icon ion-ios-americanfootball', 'icon_ion-ios-americanfootball-outline'=>'icon ion-ios-americanfootball-outline', 'icon_ion-ios-analytics'=>'icon ion-ios-analytics', 'icon_ion-ios-analytics-outline'=>'icon ion-ios-analytics-outline', 'icon_ion-ios-arrow-back'=>'icon ion-ios-arrow-back', 'icon_ion-ios-arrow-down'=>'icon ion-ios-arrow-down', 'icon_ion-ios-arrow-forward'=>'icon ion-ios-arrow-forward', 'icon_ion-ios-arrow-left'=>'icon ion-ios-arrow-left', 'icon_ion-ios-arrow-right'=>'icon ion-ios-arrow-right', 'icon_ion-ios-arrow-thin-down'=>'icon ion-ios-arrow-thin-down', 'icon_ion-ios-arrow-thin-left'=>'icon ion-ios-arrow-thin-left', 'icon_ion-ios-arrow-thin-right'=>'icon ion-ios-arrow-thin-right', 'icon_ion-ios-arrow-thin-up'=>'icon ion-ios-arrow-thin-up', 'icon_ion-ios-arrow-up'=>'icon ion-ios-arrow-up', 'icon_ion-ios-at'=>'icon ion-ios-at', 'icon_ion-ios-at-outline'=>'icon ion-ios-at-outline', 'icon_ion-ios-barcode'=>'icon ion-ios-barcode', 'icon_ion-ios-barcode-outline'=>'icon ion-ios-barcode-outline', 'icon_ion-ios-baseball'=>'icon ion-ios-baseball', 'icon_ion-ios-baseball-outline'=>'icon ion-ios-baseball-outline', 'icon_ion-ios-basketball'=>'icon ion-ios-basketball', 'icon_ion-ios-basketball-outline'=>'icon ion-ios-basketball-outline', 'icon_ion-ios-bell'=>'icon ion-ios-bell', 'icon_ion-ios-bell-outline'=>'icon ion-ios-bell-outline', 'icon_ion-ios-body'=>'icon ion-ios-body', 'icon_ion-ios-body-outline'=>'icon ion-ios-body-outline', 'icon_ion-ios-bolt'=>'icon ion-ios-bolt', 'icon_ion-ios-bolt-outline'=>'icon ion-ios-bolt-outline', 'icon_ion-ios-book'=>'icon ion-ios-book', 'icon_ion-ios-book-outline'=>'icon ion-ios-book-outline', 'icon_ion-ios-bookmarks'=>'icon ion-ios-bookmarks', 'icon_ion-ios-bookmarks-outline'=>'icon ion-ios-bookmarks-outline', 'icon_ion-ios-box'=>'icon ion-ios-box', 'icon_ion-ios-box-outline'=>'icon ion-ios-box-outline', 'icon_ion-ios-briefcase'=>'icon ion-ios-briefcase', 'icon_ion-ios-briefcase-outline'=>'icon ion-ios-briefcase-outline', 'icon_ion-ios-browsers'=>'icon ion-ios-browsers', 'icon_ion-ios-browsers-outline'=>'icon ion-ios-browsers-outline', 'icon_ion-ios-calculator'=>'icon ion-ios-calculator', 'icon_ion-ios-calculator-outline'=>'icon ion-ios-calculator-outline', 'icon_ion-ios-calendar'=>'icon ion-ios-calendar', 'icon_ion-ios-calendar-outline'=>'icon ion-ios-calendar-outline', 'icon_ion-ios-camera'=>'icon ion-ios-camera', 'icon_ion-ios-camera-outline'=>'icon ion-ios-camera-outline', 'icon_ion-ios-cart'=>'icon ion-ios-cart', 'icon_ion-ios-cart-outline'=>'icon ion-ios-cart-outline', 'icon_ion-ios-chatboxes'=>'icon ion-ios-chatboxes', 'icon_ion-ios-chatboxes-outline'=>'icon ion-ios-chatboxes-outline', 'icon_ion-ios-chatbubble'=>'icon ion-ios-chatbubble', 'icon_ion-ios-chatbubble-outline'=>'icon ion-ios-chatbubble-outline', 'icon_ion-ios-checkmark'=>'icon ion-ios-checkmark', 'icon_ion-ios-checkmark-empty'=>'icon ion-ios-checkmark-empty', 'icon_ion-ios-checkmark-outline'=>'icon ion-ios-checkmark-outline', 'icon_ion-ios-circle-filled'=>'icon ion-ios-circle-filled', 'icon_ion-ios-circle-outline'=>'icon ion-ios-circle-outline', 'icon_ion-ios-clock'=>'icon ion-ios-clock', 'icon_ion-ios-clock-outline'=>'icon ion-ios-clock-outline', 'icon_ion-ios-close'=>'icon ion-ios-close', 'icon_ion-ios-close-empty'=>'icon ion-ios-close-empty', 'icon_ion-ios-close-outline'=>'icon ion-ios-close-outline', 'icon_ion-ios-cloud'=>'icon ion-ios-cloud', 'icon_ion-ios-cloud-download'=>'icon ion-ios-cloud-download', 'icon_ion-ios-cloud-download-outline'=>'icon ion-ios-cloud-download-outline', 'icon_ion-ios-cloud-outline'=>'icon ion-ios-cloud-outline', 'icon_ion-ios-cloud-upload'=>'icon ion-ios-cloud-upload', 'icon_ion-ios-cloud-upload-outline'=>'icon ion-ios-cloud-upload-outline', 'icon_ion-ios-cloudy'=>'icon ion-ios-cloudy', 'icon_ion-ios-cloudy-night'=>'icon ion-ios-cloudy-night', 'icon_ion-ios-cloudy-night-outline'=>'icon ion-ios-cloudy-night-outline', 'icon_ion-ios-cloudy-outline'=>'icon ion-ios-cloudy-outline', 'icon_ion-ios-cog'=>'icon ion-ios-cog', 'icon_ion-ios-cog-outline'=>'icon ion-ios-cog-outline', 'icon_ion-ios-color-filter'=>'icon ion-ios-color-filter', 'icon_ion-ios-color-filter-outline'=>'icon ion-ios-color-filter-outline', 'icon_ion-ios-color-wand'=>'icon ion-ios-color-wand', 'icon_ion-ios-color-wand-outline'=>'icon ion-ios-color-wand-outline', 'icon_ion-ios-compose'=>'icon ion-ios-compose', 'icon_ion-ios-compose-outline'=>'icon ion-ios-compose-outline', 'icon_ion-ios-contact'=>'icon ion-ios-contact', 'icon_ion-ios-contact-outline'=>'icon ion-ios-contact-outline', 'icon_ion-ios-copy'=>'icon ion-ios-copy', 'icon_ion-ios-copy-outline'=>'icon ion-ios-copy-outline', 'icon_ion-ios-crop'=>'icon ion-ios-crop', 'icon_ion-ios-crop-strong'=>'icon ion-ios-crop-strong', 'icon_ion-ios-download'=>'icon ion-ios-download', 'icon_ion-ios-download-outline'=>'icon ion-ios-download-outline', 'icon_ion-ios-drag'=>'icon ion-ios-drag', 'icon_ion-ios-email'=>'icon ion-ios-email', 'icon_ion-ios-email-outline'=>'icon ion-ios-email-outline', 'icon_ion-ios-eye'=>'icon ion-ios-eye', 'icon_ion-ios-eye-outline'=>'icon ion-ios-eye-outline', 'icon_ion-ios-fastforward'=>'icon ion-ios-fastforward', 'icon_ion-ios-fastforward-outline'=>'icon ion-ios-fastforward-outline', 'icon_ion-ios-filing'=>'icon ion-ios-filing', 'icon_ion-ios-filing-outline'=>'icon ion-ios-filing-outline', 'icon_ion-ios-film'=>'icon ion-ios-film', 'icon_ion-ios-film-outline'=>'icon ion-ios-film-outline', 'icon_ion-ios-flag'=>'icon ion-ios-flag', 'icon_ion-ios-flag-outline'=>'icon ion-ios-flag-outline', 'icon_ion-ios-flame'=>'icon ion-ios-flame', 'icon_ion-ios-flame-outline'=>'icon ion-ios-flame-outline', 'icon_ion-ios-flask'=>'icon ion-ios-flask', 'icon_ion-ios-flask-outline'=>'icon ion-ios-flask-outline', 'icon_ion-ios-flower'=>'icon ion-ios-flower', 'icon_ion-ios-flower-outline'=>'icon ion-ios-flower-outline', 'icon_ion-ios-folder'=>'icon ion-ios-folder', 'icon_ion-ios-folder-outline'=>'icon ion-ios-folder-outline', 'icon_ion-ios-football'=>'icon ion-ios-football', 'icon_ion-ios-football-outline'=>'icon ion-ios-football-outline', 'icon_ion-ios-game-controller-a'=>'icon ion-ios-game-controller-a', 'icon_ion-ios-game-controller-a-outline'=>'icon ion-ios-game-controller-a-outline', 'icon_ion-ios-game-controller-b'=>'icon ion-ios-game-controller-b', 'icon_ion-ios-game-controller-b-outline'=>'icon ion-ios-game-controller-b-outline', 'icon_ion-ios-gear'=>'icon ion-ios-gear', 'icon_ion-ios-gear-outline'=>'icon ion-ios-gear-outline', 'icon_ion-ios-glasses'=>'icon ion-ios-glasses', 'icon_ion-ios-glasses-outline'=>'icon ion-ios-glasses-outline', 'icon_ion-ios-grid-view'=>'icon ion-ios-grid-view', 'icon_ion-ios-grid-view-outline'=>'icon ion-ios-grid-view-outline', 'icon_ion-ios-heart'=>'icon ion-ios-heart', 'icon_ion-ios-heart-outline'=>'icon ion-ios-heart-outline', 'icon_ion-ios-help'=>'icon ion-ios-help', 'icon_ion-ios-help-empty'=>'icon ion-ios-help-empty', 'icon_ion-ios-help-outline'=>'icon ion-ios-help-outline', 'icon_ion-ios-home'=>'icon ion-ios-home', 'icon_ion-ios-home-outline'=>'icon ion-ios-home-outline', 'icon_ion-ios-infinite'=>'icon ion-ios-infinite', 'icon_ion-ios-infinite-outline'=>'icon ion-ios-infinite-outline', 'icon_ion-ios-information'=>'icon ion-ios-information', 'icon_ion-ios-information-empty'=>'icon ion-ios-information-empty', 'icon_ion-ios-information-outline'=>'icon ion-ios-information-outline', 'icon_ion-ios-ionic-outline'=>'icon ion-ios-ionic-outline', 'icon_ion-ios-keypad'=>'icon ion-ios-keypad', 'icon_ion-ios-keypad-outline'=>'icon ion-ios-keypad-outline', 'icon_ion-ios-lightbulb'=>'icon ion-ios-lightbulb', 'icon_ion-ios-lightbulb-outline'=>'icon ion-ios-lightbulb-outline', 'icon_ion-ios-list'=>'icon ion-ios-list', 'icon_ion-ios-list-outline'=>'icon ion-ios-list-outline', 'icon_ion-ios-location'=>'icon ion-ios-location', 'icon_ion-ios-location-outline'=>'icon ion-ios-location-outline', 'icon_ion-ios-locked'=>'icon ion-ios-locked', 'icon_ion-ios-locked-outline'=>'icon ion-ios-locked-outline', 'icon_ion-ios-loop'=>'icon ion-ios-loop', 'icon_ion-ios-loop-strong'=>'icon ion-ios-loop-strong', 'icon_ion-ios-medical'=>'icon ion-ios-medical', 'icon_ion-ios-medical-outline'=>'icon ion-ios-medical-outline', 'icon_ion-ios-medkit'=>'icon ion-ios-medkit', 'icon_ion-ios-medkit-outline'=>'icon ion-ios-medkit-outline', 'icon_ion-ios-mic'=>'icon ion-ios-mic', 'icon_ion-ios-mic-off'=>'icon ion-ios-mic-off', 'icon_ion-ios-mic-outline'=>'icon ion-ios-mic-outline', 'icon_ion-ios-minus'=>'icon ion-ios-minus', 'icon_ion-ios-minus-empty'=>'icon ion-ios-minus-empty', 'icon_ion-ios-minus-outline'=>'icon ion-ios-minus-outline', 'icon_ion-ios-monitor'=>'icon ion-ios-monitor', 'icon_ion-ios-monitor-outline'=>'icon ion-ios-monitor-outline', 'icon_ion-ios-moon'=>'icon ion-ios-moon', 'icon_ion-ios-moon-outline'=>'icon ion-ios-moon-outline', 'icon_ion-ios-more'=>'icon ion-ios-more', 'icon_ion-ios-more-outline'=>'icon ion-ios-more-outline', 'icon_ion-ios-musical-note'=>'icon ion-ios-musical-note', 'icon_ion-ios-musical-notes'=>'icon ion-ios-musical-notes', 'icon_ion-ios-navigate'=>'icon ion-ios-navigate', 'icon_ion-ios-navigate-outline'=>'icon ion-ios-navigate-outline', 'icon_ion-ios-nutrition'=>'icon ion-ios-nutrition', 'icon_ion-ios-nutrition-outline'=>'icon ion-ios-nutrition-outline', 'icon_ion-ios-paper'=>'icon ion-ios-paper', 'icon_ion-ios-paper-outline'=>'icon ion-ios-paper-outline', 'icon_ion-ios-paperplane'=>'icon ion-ios-paperplane', 'icon_ion-ios-paperplane-outline'=>'icon ion-ios-paperplane-outline', 'icon_ion-ios-partlysunny'=>'icon ion-ios-partlysunny', 'icon_ion-ios-partlysunny-outline'=>'icon ion-ios-partlysunny-outline', 'icon_ion-ios-pause'=>'icon ion-ios-pause', 'icon_ion-ios-pause-outline'=>'icon ion-ios-pause-outline', 'icon_ion-ios-paw'=>'icon ion-ios-paw', 'icon_ion-ios-paw-outline'=>'icon ion-ios-paw-outline', 'icon_ion-ios-people'=>'icon ion-ios-people', 'icon_ion-ios-people-outline'=>'icon ion-ios-people-outline', 'icon_ion-ios-person'=>'icon ion-ios-person', 'icon_ion-ios-person-outline'=>'icon ion-ios-person-outline', 'icon_ion-ios-personadd'=>'icon ion-ios-personadd', 'icon_ion-ios-personadd-outline'=>'icon ion-ios-personadd-outline', 'icon_ion-ios-photos'=>'icon ion-ios-photos', 'icon_ion-ios-photos-outline'=>'icon ion-ios-photos-outline', 'icon_ion-ios-pie'=>'icon ion-ios-pie', 'icon_ion-ios-pie-outline'=>'icon ion-ios-pie-outline', 'icon_ion-ios-pint'=>'icon ion-ios-pint', 'icon_ion-ios-pint-outline'=>'icon ion-ios-pint-outline', 'icon_ion-ios-play'=>'icon ion-ios-play', 'icon_ion-ios-play-outline'=>'icon ion-ios-play-outline', 'icon_ion-ios-plus'=>'icon ion-ios-plus', 'icon_ion-ios-plus-empty'=>'icon ion-ios-plus-empty', 'icon_ion-ios-plus-outline'=>'icon ion-ios-plus-outline', 'icon_ion-ios-pricetag'=>'icon ion-ios-pricetag', 'icon_ion-ios-pricetag-outline'=>'icon ion-ios-pricetag-outline', 'icon_ion-ios-pricetags'=>'icon ion-ios-pricetags', 'icon_ion-ios-pricetags-outline'=>'icon ion-ios-pricetags-outline', 'icon_ion-ios-printer'=>'icon ion-ios-printer', 'icon_ion-ios-printer-outline'=>'icon ion-ios-printer-outline', 'icon_ion-ios-pulse'=>'icon ion-ios-pulse', 'icon_ion-ios-pulse-strong'=>'icon ion-ios-pulse-strong', 'icon_ion-ios-rainy'=>'icon ion-ios-rainy', 'icon_ion-ios-rainy-outline'=>'icon ion-ios-rainy-outline', 'icon_ion-ios-recording'=>'icon ion-ios-recording', 'icon_ion-ios-recording-outline'=>'icon ion-ios-recording-outline', 'icon_ion-ios-redo'=>'icon ion-ios-redo', 'icon_ion-ios-redo-outline'=>'icon ion-ios-redo-outline', 'icon_ion-ios-refresh'=>'icon ion-ios-refresh', 'icon_ion-ios-refresh-empty'=>'icon ion-ios-refresh-empty', 'icon_ion-ios-refresh-outline'=>'icon ion-ios-refresh-outline', 'icon_ion-ios-reload'=>'icon ion-ios-reload', 'icon_ion-ios-reverse-camera'=>'icon ion-ios-reverse-camera', 'icon_ion-ios-reverse-camera-outline'=>'icon ion-ios-reverse-camera-outline', 'icon_ion-ios-rewind'=>'icon ion-ios-rewind', 'icon_ion-ios-rewind-outline'=>'icon ion-ios-rewind-outline', 'icon_ion-ios-rose'=>'icon ion-ios-rose', 'icon_ion-ios-rose-outline'=>'icon ion-ios-rose-outline', 'icon_ion-ios-search'=>'icon ion-ios-search', 'icon_ion-ios-search-strong'=>'icon ion-ios-search-strong', 'icon_ion-ios-settings'=>'icon ion-ios-settings', 'icon_ion-ios-settings-strong'=>'icon ion-ios-settings-strong', 'icon_ion-ios-shuffle'=>'icon ion-ios-shuffle', 'icon_ion-ios-shuffle-strong'=>'icon ion-ios-shuffle-strong', 'icon_ion-ios-skipbackward'=>'icon ion-ios-skipbackward', 'icon_ion-ios-skipbackward-outline'=>'icon ion-ios-skipbackward-outline', 'icon_ion-ios-skipforward'=>'icon ion-ios-skipforward', 'icon_ion-ios-skipforward-outline'=>'icon ion-ios-skipforward-outline', 'icon_ion-ios-snowy'=>'icon ion-ios-snowy', 'icon_ion-ios-speedometer'=>'icon ion-ios-speedometer', 'icon_ion-ios-speedometer-outline'=>'icon ion-ios-speedometer-outline', 'icon_ion-ios-star'=>'icon ion-ios-star', 'icon_ion-ios-star-half'=>'icon ion-ios-star-half', 'icon_ion-ios-star-outline'=>'icon ion-ios-star-outline', 'icon_ion-ios-stopwatch'=>'icon ion-ios-stopwatch', 'icon_ion-ios-stopwatch-outline'=>'icon ion-ios-stopwatch-outline', 'icon_ion-ios-sunny'=>'icon ion-ios-sunny', 'icon_ion-ios-sunny-outline'=>'icon ion-ios-sunny-outline', 'icon_ion-ios-telephone'=>'icon ion-ios-telephone', 'icon_ion-ios-telephone-outline'=>'icon ion-ios-telephone-outline', 'icon_ion-ios-tennisball'=>'icon ion-ios-tennisball', 'icon_ion-ios-tennisball-outline'=>'icon ion-ios-tennisball-outline', 'icon_ion-ios-thunderstorm'=>'icon ion-ios-thunderstorm', 'icon_ion-ios-thunderstorm-outline'=>'icon ion-ios-thunderstorm-outline', 'icon_ion-ios-time'=>'icon ion-ios-time', 'icon_ion-ios-time-outline'=>'icon ion-ios-time-outline', 'icon_ion-ios-timer'=>'icon ion-ios-timer', 'icon_ion-ios-timer-outline'=>'icon ion-ios-timer-outline', 'icon_ion-ios-toggle'=>'icon ion-ios-toggle', 'icon_ion-ios-toggle-outline'=>'icon ion-ios-toggle-outline', 'icon_ion-ios-trash'=>'icon ion-ios-trash', 'icon_ion-ios-trash-outline'=>'icon ion-ios-trash-outline', 'icon_ion-ios-undo'=>'icon ion-ios-undo', 'icon_ion-ios-undo-outline'=>'icon ion-ios-undo-outline', 'icon_ion-ios-unlocked'=>'icon ion-ios-unlocked', 'icon_ion-ios-unlocked-outline'=>'icon ion-ios-unlocked-outline', 'icon_ion-ios-upload'=>'icon ion-ios-upload', 'icon_ion-ios-upload-outline'=>'icon ion-ios-upload-outline', 'icon_ion-ios-videocam'=>'icon ion-ios-videocam', 'icon_ion-ios-videocam-outline'=>'icon ion-ios-videocam-outline', 'icon_ion-ios-volume-high'=>'icon ion-ios-volume-high', 'icon_ion-ios-volume-low'=>'icon ion-ios-volume-low', 'icon_ion-ios-wineglass'=>'icon ion-ios-wineglass', 'icon_ion-ios-wineglass-outline'=>'icon ion-ios-wineglass-outline', 'icon_ion-ios-world'=>'icon ion-ios-world', 'icon_ion-ios-world-outline'=>'icon ion-ios-world-outline', 'icon_ion-ipad'=>'icon ion-ipad', 'icon_ion-iphone'=>'icon ion-iphone', 'icon_ion-ipod'=>'icon ion-ipod', 'icon_ion-jet'=>'icon ion-jet', 'icon_ion-key'=>'icon ion-key', 'icon_ion-knife'=>'icon ion-knife', 'icon_ion-laptop'=>'icon ion-laptop', 'icon_ion-leaf'=>'icon ion-leaf', 'icon_ion-levels'=>'icon ion-levels', 'icon_ion-lightbulb'=>'icon ion-lightbulb', 'icon_ion-link'=>'icon ion-link', 'icon_ion-load-a'=>'icon ion-load-a', 'icon_ion-load-b'=>'icon ion-load-b', 'icon_ion-load-c'=>'icon ion-load-c', 'icon_ion-load-d'=>'icon ion-load-d', 'icon_ion-location'=>'icon ion-location', 'icon_ion-lock-combination'=>'icon ion-lock-combination', 'icon_ion-locked'=>'icon ion-locked', 'icon_ion-log-in'=>'icon ion-log-in', 'icon_ion-log-out'=>'icon ion-log-out', 'icon_ion-loop'=>'icon ion-loop', 'icon_ion-magnet'=>'icon ion-magnet', 'icon_ion-male'=>'icon ion-male', 'icon_ion-man'=>'icon ion-man', 'icon_ion-map'=>'icon ion-map', 'icon_ion-medkit'=>'icon ion-medkit', 'icon_ion-merge'=>'icon ion-merge', 'icon_ion-mic-a'=>'icon ion-mic-a', 'icon_ion-mic-b'=>'icon ion-mic-b', 'icon_ion-mic-c'=>'icon ion-mic-c', 'icon_ion-minus'=>'icon ion-minus', 'icon_ion-minus-circled'=>'icon ion-minus-circled', 'icon_ion-minus-round'=>'icon ion-minus-round', 'icon_ion-model-s'=>'icon ion-model-s', 'icon_ion-monitor'=>'icon ion-monitor', 'icon_ion-more'=>'icon ion-more', 'icon_ion-mouse'=>'icon ion-mouse', 'icon_ion-music-note'=>'icon ion-music-note', 'icon_ion-navicon'=>'icon ion-navicon', 'icon_ion-navicon-round'=>'icon ion-navicon-round', 'icon_ion-navigate'=>'icon ion-navigate', 'icon_ion-network'=>'icon ion-network', 'icon_ion-no-smoking'=>'icon ion-no-smoking', 'icon_ion-nuclear'=>'icon ion-nuclear', 'icon_ion-outlet'=>'icon ion-outlet', 'icon_ion-paintbrush'=>'icon ion-paintbrush', 'icon_ion-paintbucket'=>'icon ion-paintbucket', 'icon_ion-paper-airplane'=>'icon ion-paper-airplane', 'icon_ion-paperclip'=>'icon ion-paperclip', 'icon_ion-pause'=>'icon ion-pause', 'icon_ion-person'=>'icon ion-person', 'icon_ion-person-add'=>'icon ion-person-add', 'icon_ion-person-stalker'=>'icon ion-person-stalker', 'icon_ion-pie-graph'=>'icon ion-pie-graph', 'icon_ion-pin'=>'icon ion-pin', 'icon_ion-pinpoint'=>'icon ion-pinpoint', 'icon_ion-pizza'=>'icon ion-pizza', 'icon_ion-plane'=>'icon ion-plane', 'icon_ion-planet'=>'icon ion-planet', 'icon_ion-play'=>'icon ion-play', 'icon_ion-playstation'=>'icon ion-playstation', 'icon_ion-plus'=>'icon ion-plus', 'icon_ion-plus-circled'=>'icon ion-plus-circled', 'icon_ion-plus-round'=>'icon ion-plus-round', 'icon_ion-podium'=>'icon ion-podium', 'icon_ion-pound'=>'icon ion-pound', 'icon_ion-power'=>'icon ion-power', 'icon_ion-pricetag'=>'icon ion-pricetag', 'icon_ion-pricetags'=>'icon ion-pricetags', 'icon_ion-printer'=>'icon ion-printer', 'icon_ion-pull-request'=>'icon ion-pull-request', 'icon_ion-qr-scanner'=>'icon ion-qr-scanner', 'icon_ion-quote'=>'icon ion-quote', 'icon_ion-radio-waves'=>'icon ion-radio-waves', 'icon_ion-record'=>'icon ion-record', 'icon_ion-refresh'=>'icon ion-refresh', 'icon_ion-reply'=>'icon ion-reply', 'icon_ion-reply-all'=>'icon ion-reply-all', 'icon_ion-ribbon-a'=>'icon ion-ribbon-a', 'icon_ion-ribbon-b'=>'icon ion-ribbon-b', 'icon_ion-sad'=>'icon ion-sad', 'icon_ion-sad-outline'=>'icon ion-sad-outline', 'icon_ion-scissors'=>'icon ion-scissors', 'icon_ion-search'=>'icon ion-search', 'icon_ion-settings'=>'icon ion-settings', 'icon_ion-share'=>'icon ion-share', 'icon_ion-shuffle'=>'icon ion-shuffle', 'icon_ion-skip-backward'=>'icon ion-skip-backward', 'icon_ion-skip-forward'=>'icon ion-skip-forward', 'icon_ion-social-android'=>'icon ion-social-android', 'icon_ion-social-android-outline'=>'icon ion-social-android-outline', 'icon_ion-social-angular'=>'icon ion-social-angular', 'icon_ion-social-angular-outline'=>'icon ion-social-angular-outline', 'icon_ion-social-apple'=>'icon ion-social-apple', 'icon_ion-social-apple-outline'=>'icon ion-social-apple-outline', 'icon_ion-social-bitcoin'=>'icon ion-social-bitcoin', 'icon_ion-social-bitcoin-outline'=>'icon ion-social-bitcoin-outline', 'icon_ion-social-buffer'=>'icon ion-social-buffer', 'icon_ion-social-buffer-outline'=>'icon ion-social-buffer-outline', 'icon_ion-social-chrome'=>'icon ion-social-chrome', 'icon_ion-social-chrome-outline'=>'icon ion-social-chrome-outline', 'icon_ion-social-codepen'=>'icon ion-social-codepen', 'icon_ion-social-codepen-outline'=>'icon ion-social-codepen-outline', 'icon_ion-social-css3'=>'icon ion-social-css3', 'icon_ion-social-css3-outline'=>'icon ion-social-css3-outline', 'icon_ion-social-designernews'=>'icon ion-social-designernews', 'icon_ion-social-designernews-outline'=>'icon ion-social-designernews-outline', 'icon_ion-social-dribbble'=>'icon ion-social-dribbble', 'icon_ion-social-dribbble-outline'=>'icon ion-social-dribbble-outline', 'icon_ion-social-dropbox'=>'icon ion-social-dropbox', 'icon_ion-social-dropbox-outline'=>'icon ion-social-dropbox-outline', 'icon_ion-social-euro'=>'icon ion-social-euro', 'icon_ion-social-euro-outline'=>'icon ion-social-euro-outline', 'icon_ion-social-facebook'=>'icon ion-social-facebook', 'icon_ion-social-facebook-outline'=>'icon ion-social-facebook-outline', 'icon_ion-social-foursquare'=>'icon ion-social-foursquare', 'icon_ion-social-foursquare-outline'=>'icon ion-social-foursquare-outline', 'icon_ion-social-freebsd-devil'=>'icon ion-social-freebsd-devil', 'icon_ion-social-github'=>'icon ion-social-github', 'icon_ion-social-github-outline'=>'icon ion-social-github-outline', 'icon_ion-social-google'=>'icon ion-social-google', 'icon_ion-social-google-outline'=>'icon ion-social-google-outline', 'icon_ion-social-googleplus'=>'icon ion-social-googleplus', 'icon_ion-social-googleplus-outline'=>'icon ion-social-googleplus-outline', 'icon_ion-social-hackernews'=>'icon ion-social-hackernews', 'icon_ion-social-hackernews-outline'=>'icon ion-social-hackernews-outline', 'icon_ion-social-html5'=>'icon ion-social-html5', 'icon_ion-social-html5-outline'=>'icon ion-social-html5-outline', 'icon_ion-social-instagram'=>'icon ion-social-instagram', 'icon_ion-social-instagram-outline'=>'icon ion-social-instagram-outline', 'icon_ion-social-javascript'=>'icon ion-social-javascript', 'icon_ion-social-javascript-outline'=>'icon ion-social-javascript-outline', 'icon_ion-social-linkedin'=>'icon ion-social-linkedin', 'icon_ion-social-linkedin-outline'=>'icon ion-social-linkedin-outline', 'icon_ion-social-markdown'=>'icon ion-social-markdown', 'icon_ion-social-nodejs'=>'icon ion-social-nodejs', 'icon_ion-social-octocat'=>'icon ion-social-octocat', 'icon_ion-social-pinterest'=>'icon ion-social-pinterest', 'icon_ion-social-pinterest-outline'=>'icon ion-social-pinterest-outline', 'icon_ion-social-python'=>'icon ion-social-python', 'icon_ion-social-reddit'=>'icon ion-social-reddit', 'icon_ion-social-reddit-outline'=>'icon ion-social-reddit-outline', 'icon_ion-social-rss'=>'icon ion-social-rss', 'icon_ion-social-rss-outline'=>'icon ion-social-rss-outline', 'icon_ion-social-sass'=>'icon ion-social-sass', 'icon_ion-social-skype'=>'icon ion-social-skype', 'icon_ion-social-skype-outline'=>'icon ion-social-skype-outline', 'icon_ion-social-snapchat'=>'icon ion-social-snapchat', 'icon_ion-social-snapchat-outline'=>'icon ion-social-snapchat-outline', 'icon_ion-social-tumblr'=>'icon ion-social-tumblr', 'icon_ion-social-tumblr-outline'=>'icon ion-social-tumblr-outline', 'icon_ion-social-tux'=>'icon ion-social-tux', 'icon_ion-social-twitch'=>'icon ion-social-twitch', 'icon_ion-social-twitch-outline'=>'icon ion-social-twitch-outline', 'icon_ion-social-twitter'=>'icon ion-social-twitter', 'icon_ion-social-twitter-outline'=>'icon ion-social-twitter-outline', 'icon_ion-social-usd'=>'icon ion-social-usd', 'icon_ion-social-usd-outline'=>'icon ion-social-usd-outline', 'icon_ion-social-vimeo'=>'icon ion-social-vimeo', 'icon_ion-social-vimeo-outline'=>'icon ion-social-vimeo-outline', 'icon_ion-social-whatsapp'=>'icon ion-social-whatsapp', 'icon_ion-social-whatsapp-outline'=>'icon ion-social-whatsapp-outline', 'icon_ion-social-windows'=>'icon ion-social-windows', 'icon_ion-social-windows-outline'=>'icon ion-social-windows-outline', 'icon_ion-social-wordpress'=>'icon ion-social-wordpress', 'icon_ion-social-wordpress-outline'=>'icon ion-social-wordpress-outline', 'icon_ion-social-yahoo'=>'icon ion-social-yahoo', 'icon_ion-social-yahoo-outline'=>'icon ion-social-yahoo-outline', 'icon_ion-social-yen'=>'icon ion-social-yen', 'icon_ion-social-yen-outline'=>'icon ion-social-yen-outline', 'icon_ion-social-youtube'=>'icon ion-social-youtube', 'icon_ion-social-youtube-outline'=>'icon ion-social-youtube-outline', 'icon_ion-soup-can'=>'icon ion-soup-can', 'icon_ion-soup-can-outline'=>'icon ion-soup-can-outline', 'icon_ion-speakerphone'=>'icon ion-speakerphone', 'icon_ion-speedometer'=>'icon ion-speedometer', 'icon_ion-spoon'=>'icon ion-spoon', 'icon_ion-star'=>'icon ion-star', 'icon_ion-stats-bars'=>'icon ion-stats-bars', 'icon_ion-steam'=>'icon ion-steam', 'icon_ion-stop'=>'icon ion-stop', 'icon_ion-thermometer'=>'icon ion-thermometer', 'icon_ion-thumbsdown'=>'icon ion-thumbsdown', 'icon_ion-thumbsup'=>'icon ion-thumbsup', 'icon_ion-toggle'=>'icon ion-toggle', 'icon_ion-toggle-filled'=>'icon ion-toggle-filled', 'icon_ion-transgender'=>'icon ion-transgender', 'icon_ion-trash-a'=>'icon ion-trash-a', 'icon_ion-trash-b'=>'icon ion-trash-b', 'icon_ion-trophy'=>'icon ion-trophy', 'icon_ion-tshirt'=>'icon ion-tshirt', 'icon_ion-tshirt-outline'=>'icon ion-tshirt-outline', 'icon_ion-umbrella'=>'icon ion-umbrella', 'icon_ion-university'=>'icon ion-university', 'icon_ion-unlocked'=>'icon ion-unlocked', 'icon_ion-upload'=>'icon ion-upload', 'icon_ion-usb'=>'icon ion-usb', 'icon_ion-videocamera'=>'icon ion-videocamera', 'icon_ion-volume-high'=>'icon ion-volume-high', 'icon_ion-volume-low'=>'icon ion-volume-low', 'icon_ion-volume-medium'=>'icon ion-volume-medium', 'icon_ion-volume-mute'=>'icon ion-volume-mute', 'icon_ion-wand'=>'icon ion-wand', 'icon_ion-waterdrop'=>'icon ion-waterdrop', 'icon_ion-wifi'=>'icon ion-wifi', 'icon_ion-wineglass'=>'icon ion-wineglass', 'icon_ion-woman'=>'icon ion-woman', 'icon_ion-wrench'=>'icon ion-wrench', 'icon_ion-xbox'=>'icon ion-xbox', 'fas_fa-ad'=>'fas fa-ad', 'fas_fa-address-book'=>'fas fa-address-book', 'fas_fa-address-card'=>'fas fa-address-card', 'fas_fa-adjust'=>'fas fa-adjust', 'fas_fa-air-freshener'=>'fas fa-air-freshener', 'fas_fa-align-center'=>'fas fa-align-center', 'fas_fa-align-justify'=>'fas fa-align-justify', 'fas_fa-align-left'=>'fas fa-align-left', 'fas_fa-align-right'=>'fas fa-align-right', 'fas_fa-allergies'=>'fas fa-allergies', 'fas_fa-ambulance'=>'fas fa-ambulance', 'fas_fa-american-sign-language-interpreting'=>'fas fa-american-sign-language-interpreting', 'fas_fa-anchor'=>'fas fa-anchor', 'fas_fa-angle-double-down'=>'fas fa-angle-double-down', 'fas_fa-angle-double-left'=>'fas fa-angle-double-left', 'fas_fa-angle-double-right'=>'fas fa-angle-double-right', 'fas_fa-angle-double-up'=>'fas fa-angle-double-up', 'fas_fa-angle-down'=>'fas fa-angle-down', 'fas_fa-angle-left'=>'fas fa-angle-left', 'fas_fa-angle-right'=>'fas fa-angle-right', 'fas_fa-angle-up'=>'fas fa-angle-up', 'fas_fa-angry'=>'fas fa-angry', 'fas_fa-ankh'=>'fas fa-ankh', 'fas_fa-apple-alt'=>'fas fa-apple-alt', 'fas_fa-archive'=>'fas fa-archive', 'fas_fa-archway'=>'fas fa-archway', 'fas_fa-arrow-alt-circle-down'=>'fas fa-arrow-alt-circle-down', 'fas_fa-arrow-alt-circle-left'=>'fas fa-arrow-alt-circle-left', 'fas_fa-arrow-alt-circle-right'=>'fas fa-arrow-alt-circle-right', 'fas_fa-arrow-alt-circle-up'=>'fas fa-arrow-alt-circle-up', 'fas_fa-arrow-circle-down'=>'fas fa-arrow-circle-down', 'fas_fa-arrow-circle-left'=>'fas fa-arrow-circle-left', 'fas_fa-arrow-circle-right'=>'fas fa-arrow-circle-right', 'fas_fa-arrow-circle-up'=>'fas fa-arrow-circle-up', 'fas_fa-arrow-down'=>'fas fa-arrow-down', 'fas_fa-arrow-left'=>'fas fa-arrow-left', 'fas_fa-arrow-right'=>'fas fa-arrow-right', 'fas_fa-arrow-up'=>'fas fa-arrow-up', 'fas_fa-arrows-alt'=>'fas fa-arrows-alt', 'fas_fa-arrows-alt-h'=>'fas fa-arrows-alt-h', 'fas_fa-arrows-alt-v'=>'fas fa-arrows-alt-v', 'fas_fa-assistive-listening-systems'=>'fas fa-assistive-listening-systems', 'fas_fa-asterisk'=>'fas fa-asterisk', 'fas_fa-at'=>'fas fa-at', 'fas_fa-atlas'=>'fas fa-atlas', 'fas_fa-atom'=>'fas fa-atom', 'fas_fa-audio-description'=>'fas fa-audio-description', 'fas_fa-award'=>'fas fa-award', 'fas_fa-baby'=>'fas fa-baby', 'fas_fa-baby-carriage'=>'fas fa-baby-carriage', 'fas_fa-backspace'=>'fas fa-backspace', 'fas_fa-backward'=>'fas fa-backward', 'fas_fa-bacon'=>'fas fa-bacon', 'fas_fa-bahai'=>'fas fa-bahai', 'fas_fa-balance-scale'=>'fas fa-balance-scale', 'fas_fa-balance-scale-left'=>'fas fa-balance-scale-left', 'fas_fa-balance-scale-right'=>'fas fa-balance-scale-right', 'fas_fa-ban'=>'fas fa-ban', 'fas_fa-band-aid'=>'fas fa-band-aid', 'fas_fa-barcode'=>'fas fa-barcode', 'fas_fa-bars'=>'fas fa-bars', 'fas_fa-baseball-ball'=>'fas fa-baseball-ball', 'fas_fa-basketball-ball'=>'fas fa-basketball-ball', 'fas_fa-bath'=>'fas fa-bath', 'fas_fa-battery-empty'=>'fas fa-battery-empty', 'fas_fa-battery-full'=>'fas fa-battery-full', 'fas_fa-battery-half'=>'fas fa-battery-half', 'fas_fa-battery-quarter'=>'fas fa-battery-quarter', 'fas_fa-battery-three-quarters'=>'fas fa-battery-three-quarters', 'fas_fa-bed'=>'fas fa-bed', 'fas_fa-beer'=>'fas fa-beer', 'fas_fa-bell'=>'fas fa-bell', 'fas_fa-bell-slash'=>'fas fa-bell-slash', 'fas_fa-bezier-curve'=>'fas fa-bezier-curve', 'fas_fa-bible'=>'fas fa-bible', 'fas_fa-bicycle'=>'fas fa-bicycle', 'fas_fa-biking'=>'fas fa-biking', 'fas_fa-binoculars'=>'fas fa-binoculars', 'fas_fa-biohazard'=>'fas fa-biohazard', 'fas_fa-birthday-cake'=>'fas fa-birthday-cake', 'fas_fa-blender'=>'fas fa-blender', 'fas_fa-blender-phone'=>'fas fa-blender-phone', 'fas_fa-blind'=>'fas fa-blind', 'fas_fa-blog'=>'fas fa-blog', 'fas_fa-bold'=>'fas fa-bold', 'fas_fa-bolt'=>'fas fa-bolt', 'fas_fa-bomb'=>'fas fa-bomb', 'fas_fa-bone'=>'fas fa-bone', 'fas_fa-bong'=>'fas fa-bong', 'fas_fa-book'=>'fas fa-book', 'fas_fa-book-dead'=>'fas fa-book-dead', 'fas_fa-book-medical'=>'fas fa-book-medical', 'fas_fa-book-open'=>'fas fa-book-open', 'fas_fa-book-reader'=>'fas fa-book-reader', 'fas_fa-bookmark'=>'fas fa-bookmark', 'fas_fa-border-all'=>'fas fa-border-all', 'fas_fa-border-none'=>'fas fa-border-none', 'fas_fa-border-style'=>'fas fa-border-style', 'fas_fa-bowling-ball'=>'fas fa-bowling-ball', 'fas_fa-box'=>'fas fa-box', 'fas_fa-box-open'=>'fas fa-box-open', 'fas_fa-boxes'=>'fas fa-boxes', 'fas_fa-braille'=>'fas fa-braille', 'fas_fa-brain'=>'fas fa-brain', 'fas_fa-bread-slice'=>'fas fa-bread-slice', 'fas_fa-briefcase'=>'fas fa-briefcase', 'fas_fa-briefcase-medical'=>'fas fa-briefcase-medical', 'fas_fa-broadcast-tower'=>'fas fa-broadcast-tower', 'fas_fa-broom'=>'fas fa-broom', 'fas_fa-brush'=>'fas fa-brush', 'fas_fa-bug'=>'fas fa-bug', 'fas_fa-building'=>'fas fa-building', 'fas_fa-bullhorn'=>'fas fa-bullhorn', 'fas_fa-bullseye'=>'fas fa-bullseye', 'fas_fa-burn'=>'fas fa-burn', 'fas_fa-bus'=>'fas fa-bus', 'fas_fa-bus-alt'=>'fas fa-bus-alt', 'fas_fa-business-time'=>'fas fa-business-time', 'fas_fa-calculator'=>'fas fa-calculator', 'fas_fa-calendar'=>'fas fa-calendar', 'fas_fa-calendar-alt'=>'fas fa-calendar-alt', 'fas_fa-calendar-check'=>'fas fa-calendar-check', 'fas_fa-calendar-day'=>'fas fa-calendar-day', 'fas_fa-calendar-minus'=>'fas fa-calendar-minus', 'fas_fa-calendar-plus'=>'fas fa-calendar-plus', 'fas_fa-calendar-times'=>'fas fa-calendar-times', 'fas_fa-calendar-week'=>'fas fa-calendar-week', 'fas_fa-camera'=>'fas fa-camera', 'fas_fa-camera-retro'=>'fas fa-camera-retro', 'fas_fa-campground'=>'fas fa-campground', 'fas_fa-candy-cane'=>'fas fa-candy-cane', 'fas_fa-cannabis'=>'fas fa-cannabis', 'fas_fa-capsules'=>'fas fa-capsules', 'fas_fa-car'=>'fas fa-car', 'fas_fa-car-alt'=>'fas fa-car-alt', 'fas_fa-car-battery'=>'fas fa-car-battery', 'fas_fa-car-crash'=>'fas fa-car-crash', 'fas_fa-car-side'=>'fas fa-car-side', 'fas_fa-caravan'=>'fas fa-caravan', 'fas_fa-caret-down'=>'fas fa-caret-down', 'fas_fa-caret-left'=>'fas fa-caret-left', 'fas_fa-caret-right'=>'fas fa-caret-right', 'fas_fa-caret-square-down'=>'fas fa-caret-square-down', 'fas_fa-caret-square-left'=>'fas fa-caret-square-left', 'fas_fa-caret-square-right'=>'fas fa-caret-square-right', 'fas_fa-caret-square-up'=>'fas fa-caret-square-up', 'fas_fa-caret-up'=>'fas fa-caret-up', 'fas_fa-carrot'=>'fas fa-carrot', 'fas_fa-cart-arrow-down'=>'fas fa-cart-arrow-down', 'fas_fa-cart-plus'=>'fas fa-cart-plus', 'fas_fa-cash-register'=>'fas fa-cash-register', 'fas_fa-cat'=>'fas fa-cat', 'fas_fa-certificate'=>'fas fa-certificate', 'fas_fa-chair'=>'fas fa-chair', 'fas_fa-chalkboard'=>'fas fa-chalkboard', 'fas_fa-chalkboard-teacher'=>'fas fa-chalkboard-teacher', 'fas_fa-charging-station'=>'fas fa-charging-station', 'fas_fa-chart-area'=>'fas fa-chart-area', 'fas_fa-chart-bar'=>'fas fa-chart-bar', 'fas_fa-chart-line'=>'fas fa-chart-line', 'fas_fa-chart-pie'=>'fas fa-chart-pie', 'fas_fa-check'=>'fas fa-check', 'fas_fa-check-circle'=>'fas fa-check-circle', 'fas_fa-check-double'=>'fas fa-check-double', 'fas_fa-check-square'=>'fas fa-check-square', 'fas_fa-cheese'=>'fas fa-cheese', 'fas_fa-chess'=>'fas fa-chess', 'fas_fa-chess-bishop'=>'fas fa-chess-bishop', 'fas_fa-chess-board'=>'fas fa-chess-board', 'fas_fa-chess-king'=>'fas fa-chess-king', 'fas_fa-chess-knight'=>'fas fa-chess-knight', 'fas_fa-chess-pawn'=>'fas fa-chess-pawn', 'fas_fa-chess-queen'=>'fas fa-chess-queen', 'fas_fa-chess-rook'=>'fas fa-chess-rook', 'fas_fa-chevron-circle-down'=>'fas fa-chevron-circle-down', 'fas_fa-chevron-circle-left'=>'fas fa-chevron-circle-left', 'fas_fa-chevron-circle-right'=>'fas fa-chevron-circle-right', 'fas_fa-chevron-circle-up'=>'fas fa-chevron-circle-up', 'fas_fa-chevron-down'=>'fas fa-chevron-down', 'fas_fa-chevron-left'=>'fas fa-chevron-left', 'fas_fa-chevron-right'=>'fas fa-chevron-right', 'fas_fa-chevron-up'=>'fas fa-chevron-up', 'fas_fa-child'=>'fas fa-child', 'fas_fa-church'=>'fas fa-church', 'fas_fa-circle'=>'fas fa-circle', 'fas_fa-circle-notch'=>'fas fa-circle-notch', 'fas_fa-city'=>'fas fa-city', 'fas_fa-clinic-medical'=>'fas fa-clinic-medical', 'fas_fa-clipboard'=>'fas fa-clipboard', 'fas_fa-clipboard-check'=>'fas fa-clipboard-check', 'fas_fa-clipboard-list'=>'fas fa-clipboard-list', 'fas_fa-clock'=>'fas fa-clock', 'fas_fa-clone'=>'fas fa-clone', 'fas_fa-closed-captioning'=>'fas fa-closed-captioning', 'fas_fa-cloud'=>'fas fa-cloud', 'fas_fa-cloud-download-alt'=>'fas fa-cloud-download-alt', 'fas_fa-cloud-meatball'=>'fas fa-cloud-meatball', 'fas_fa-cloud-moon'=>'fas fa-cloud-moon', 'fas_fa-cloud-moon-rain'=>'fas fa-cloud-moon-rain', 'fas_fa-cloud-rain'=>'fas fa-cloud-rain', 'fas_fa-cloud-showers-heavy'=>'fas fa-cloud-showers-heavy', 'fas_fa-cloud-sun'=>'fas fa-cloud-sun', 'fas_fa-cloud-sun-rain'=>'fas fa-cloud-sun-rain', 'fas_fa-cloud-upload-alt'=>'fas fa-cloud-upload-alt', 'fas_fa-cocktail'=>'fas fa-cocktail', 'fas_fa-code'=>'fas fa-code', 'fas_fa-code-branch'=>'fas fa-code-branch', 'fas_fa-coffee'=>'fas fa-coffee', 'fas_fa-cog'=>'fas fa-cog', 'fas_fa-cogs'=>'fas fa-cogs', 'fas_fa-coins'=>'fas fa-coins', 'fas_fa-columns'=>'fas fa-columns', 'fas_fa-comment'=>'fas fa-comment', 'fas_fa-comment-alt'=>'fas fa-comment-alt', 'fas_fa-comment-dollar'=>'fas fa-comment-dollar', 'fas_fa-comment-dots'=>'fas fa-comment-dots', 'fas_fa-comment-medical'=>'fas fa-comment-medical', 'fas_fa-comment-slash'=>'fas fa-comment-slash', 'fas_fa-comments'=>'fas fa-comments', 'fas_fa-comments-dollar'=>'fas fa-comments-dollar', 'fas_fa-compact-disc'=>'fas fa-compact-disc', 'fas_fa-compass'=>'fas fa-compass', 'fas_fa-compress'=>'fas fa-compress', 'fas_fa-compress-alt'=>'fas fa-compress-alt', 'fas_fa-compress-arrows-alt'=>'fas fa-compress-arrows-alt', 'fas_fa-concierge-bell'=>'fas fa-concierge-bell', 'fas_fa-cookie'=>'fas fa-cookie', 'fas_fa-cookie-bite'=>'fas fa-cookie-bite', 'fas_fa-copy'=>'fas fa-copy', 'fas_fa-copyright'=>'fas fa-copyright', 'fas_fa-couch'=>'fas fa-couch', 'fas_fa-credit-card'=>'fas fa-credit-card', 'fas_fa-crop'=>'fas fa-crop', 'fas_fa-crop-alt'=>'fas fa-crop-alt', 'fas_fa-cross'=>'fas fa-cross', 'fas_fa-crosshairs'=>'fas fa-crosshairs', 'fas_fa-crow'=>'fas fa-crow', 'fas_fa-crown'=>'fas fa-crown', 'fas_fa-crutch'=>'fas fa-crutch', 'fas_fa-cube'=>'fas fa-cube', 'fas_fa-cubes'=>'fas fa-cubes', 'fas_fa-cut'=>'fas fa-cut', 'fas_fa-database'=>'fas fa-database', 'fas_fa-deaf'=>'fas fa-deaf', 'fas_fa-democrat'=>'fas fa-democrat', 'fas_fa-desktop'=>'fas fa-desktop', 'fas_fa-dharmachakra'=>'fas fa-dharmachakra', 'fas_fa-diagnoses'=>'fas fa-diagnoses', 'fas_fa-dice'=>'fas fa-dice', 'fas_fa-dice-d20'=>'fas fa-dice-d20', 'fas_fa-dice-d6'=>'fas fa-dice-d6', 'fas_fa-dice-five'=>'fas fa-dice-five', 'fas_fa-dice-four'=>'fas fa-dice-four', 'fas_fa-dice-one'=>'fas fa-dice-one', 'fas_fa-dice-six'=>'fas fa-dice-six', 'fas_fa-dice-three'=>'fas fa-dice-three', 'fas_fa-dice-two'=>'fas fa-dice-two', 'fas_fa-digital-tachograph'=>'fas fa-digital-tachograph', 'fas_fa-directions'=>'fas fa-directions', 'fas_fa-divide'=>'fas fa-divide', 'fas_fa-dizzy'=>'fas fa-dizzy', 'fas_fa-dna'=>'fas fa-dna', 'fas_fa-dog'=>'fas fa-dog', 'fas_fa-dollar-sign'=>'fas fa-dollar-sign', 'fas_fa-dolly'=>'fas fa-dolly', 'fas_fa-dolly-flatbed'=>'fas fa-dolly-flatbed', 'fas_fa-donate'=>'fas fa-donate', 'fas_fa-door-closed'=>'fas fa-door-closed', 'fas_fa-door-open'=>'fas fa-door-open', 'fas_fa-dot-circle'=>'fas fa-dot-circle', 'fas_fa-dove'=>'fas fa-dove', 'fas_fa-download'=>'fas fa-download', 'fas_fa-drafting-compass'=>'fas fa-drafting-compass', 'fas_fa-dragon'=>'fas fa-dragon', 'fas_fa-draw-polygon'=>'fas fa-draw-polygon', 'fas_fa-drum'=>'fas fa-drum', 'fas_fa-drum-steelpan'=>'fas fa-drum-steelpan', 'fas_fa-drumstick-bite'=>'fas fa-drumstick-bite', 'fas_fa-dumbbell'=>'fas fa-dumbbell', 'fas_fa-dumpster'=>'fas fa-dumpster', 'fas_fa-dumpster-fire'=>'fas fa-dumpster-fire', 'fas_fa-dungeon'=>'fas fa-dungeon', 'fas_fa-edit'=>'fas fa-edit', 'fas_fa-egg'=>'fas fa-egg', 'fas_fa-eject'=>'fas fa-eject', 'fas_fa-ellipsis-h'=>'fas fa-ellipsis-h', 'fas_fa-ellipsis-v'=>'fas fa-ellipsis-v', 'fas_fa-envelope'=>'fas fa-envelope', 'fas_fa-envelope-open'=>'fas fa-envelope-open', 'fas_fa-envelope-open-text'=>'fas fa-envelope-open-text', 'fas_fa-envelope-square'=>'fas fa-envelope-square', 'fas_fa-equals'=>'fas fa-equals', 'fas_fa-eraser'=>'fas fa-eraser', 'fas_fa-ethernet'=>'fas fa-ethernet', 'fas_fa-euro-sign'=>'fas fa-euro-sign', 'fas_fa-exchange-alt'=>'fas fa-exchange-alt', 'fas_fa-exclamation'=>'fas fa-exclamation', 'fas_fa-exclamation-circle'=>'fas fa-exclamation-circle', 'fas_fa-exclamation-triangle'=>'fas fa-exclamation-triangle', 'fas_fa-expand'=>'fas fa-expand', 'fas_fa-expand-alt'=>'fas fa-expand-alt', 'fas_fa-expand-arrows-alt'=>'fas fa-expand-arrows-alt', 'fas_fa-external-link-alt'=>'fas fa-external-link-alt', 'fas_fa-external-link-square-alt'=>'fas fa-external-link-square-alt', 'fas_fa-eye'=>'fas fa-eye', 'fas_fa-eye-dropper'=>'fas fa-eye-dropper', 'fas_fa-eye-slash'=>'fas fa-eye-slash', 'fas_fa-fan'=>'fas fa-fan', 'fas_fa-fast-backward'=>'fas fa-fast-backward', 'fas_fa-fast-forward'=>'fas fa-fast-forward', 'fas_fa-fax'=>'fas fa-fax', 'fas_fa-feather'=>'fas fa-feather', 'fas_fa-feather-alt'=>'fas fa-feather-alt', 'fas_fa-female'=>'fas fa-female', 'fas_fa-fighter-jet'=>'fas fa-fighter-jet', 'fas_fa-file'=>'fas fa-file', 'fas_fa-file-alt'=>'fas fa-file-alt', 'fas_fa-file-archive'=>'fas fa-file-archive', 'fas_fa-file-audio'=>'fas fa-file-audio', 'fas_fa-file-code'=>'fas fa-file-code', 'fas_fa-file-contract'=>'fas fa-file-contract', 'fas_fa-file-csv'=>'fas fa-file-csv', 'fas_fa-file-download'=>'fas fa-file-download', 'fas_fa-file-excel'=>'fas fa-file-excel', 'fas_fa-file-export'=>'fas fa-file-export', 'fas_fa-file-image'=>'fas fa-file-image', 'fas_fa-file-import'=>'fas fa-file-import', 'fas_fa-file-invoice'=>'fas fa-file-invoice', 'fas_fa-file-invoice-dollar'=>'fas fa-file-invoice-dollar', 'fas_fa-file-medical'=>'fas fa-file-medical', 'fas_fa-file-medical-alt'=>'fas fa-file-medical-alt', 'fas_fa-file-pdf'=>'fas fa-file-pdf', 'fas_fa-file-powerpoint'=>'fas fa-file-powerpoint', 'fas_fa-file-prescription'=>'fas fa-file-prescription', 'fas_fa-file-signature'=>'fas fa-file-signature', 'fas_fa-file-upload'=>'fas fa-file-upload', 'fas_fa-file-video'=>'fas fa-file-video', 'fas_fa-file-word'=>'fas fa-file-word', 'fas_fa-fill'=>'fas fa-fill', 'fas_fa-fill-drip'=>'fas fa-fill-drip', 'fas_fa-film'=>'fas fa-film', 'fas_fa-filter'=>'fas fa-filter', 'fas_fa-fingerprint'=>'fas fa-fingerprint', 'fas_fa-fire'=>'fas fa-fire', 'fas_fa-fire-alt'=>'fas fa-fire-alt', 'fas_fa-fire-extinguisher'=>'fas fa-fire-extinguisher', 'fas_fa-first-aid'=>'fas fa-first-aid', 'fas_fa-fish'=>'fas fa-fish', 'fas_fa-fist-raised'=>'fas fa-fist-raised', 'fas_fa-flag'=>'fas fa-flag', 'fas_fa-flag-checkered'=>'fas fa-flag-checkered', 'fas_fa-flag-usa'=>'fas fa-flag-usa', 'fas_fa-flask'=>'fas fa-flask', 'fas_fa-flushed'=>'fas fa-flushed', 'fas_fa-folder'=>'fas fa-folder', 'fas_fa-folder-minus'=>'fas fa-folder-minus', 'fas_fa-folder-open'=>'fas fa-folder-open', 'fas_fa-folder-plus'=>'fas fa-folder-plus', 'fas_fa-font'=>'fas fa-font', 'fas_fa-football-ball'=>'fas fa-football-ball', 'fas_fa-forward'=>'fas fa-forward', 'fas_fa-frog'=>'fas fa-frog', 'fas_fa-frown'=>'fas fa-frown', 'fas_fa-frown-open'=>'fas fa-frown-open', 'fas_fa-funnel-dollar'=>'fas fa-funnel-dollar', 'fas_fa-futbol'=>'fas fa-futbol', 'fas_fa-gamepad'=>'fas fa-gamepad', 'fas_fa-gas-pump'=>'fas fa-gas-pump', 'fas_fa-gavel'=>'fas fa-gavel', 'fas_fa-gem'=>'fas fa-gem', 'fas_fa-genderless'=>'fas fa-genderless', 'fas_fa-ghost'=>'fas fa-ghost', 'fas_fa-gift'=>'fas fa-gift', 'fas_fa-gifts'=>'fas fa-gifts', 'fas_fa-glass-cheers'=>'fas fa-glass-cheers', 'fas_fa-glass-martini'=>'fas fa-glass-martini', 'fas_fa-glass-martini-alt'=>'fas fa-glass-martini-alt', 'fas_fa-glass-whiskey'=>'fas fa-glass-whiskey', 'fas_fa-glasses'=>'fas fa-glasses', 'fas_fa-globe'=>'fas fa-globe', 'fas_fa-globe-africa'=>'fas fa-globe-africa', 'fas_fa-globe-americas'=>'fas fa-globe-americas', 'fas_fa-globe-asia'=>'fas fa-globe-asia', 'fas_fa-globe-europe'=>'fas fa-globe-europe', 'fas_fa-golf-ball'=>'fas fa-golf-ball', 'fas_fa-gopuram'=>'fas fa-gopuram', 'fas_fa-graduation-cap'=>'fas fa-graduation-cap', 'fas_fa-greater-than'=>'fas fa-greater-than', 'fas_fa-greater-than-equal'=>'fas fa-greater-than-equal', 'fas_fa-grimace'=>'fas fa-grimace', 'fas_fa-grin'=>'fas fa-grin', 'fas_fa-grin-alt'=>'fas fa-grin-alt', 'fas_fa-grin-beam'=>'fas fa-grin-beam', 'fas_fa-grin-beam-sweat'=>'fas fa-grin-beam-sweat', 'fas_fa-grin-hearts'=>'fas fa-grin-hearts', 'fas_fa-grin-squint'=>'fas fa-grin-squint', 'fas_fa-grin-squint-tears'=>'fas fa-grin-squint-tears', 'fas_fa-grin-stars'=>'fas fa-grin-stars', 'fas_fa-grin-tears'=>'fas fa-grin-tears', 'fas_fa-grin-tongue'=>'fas fa-grin-tongue', 'fas_fa-grin-tongue-squint'=>'fas fa-grin-tongue-squint', 'fas_fa-grin-tongue-wink'=>'fas fa-grin-tongue-wink', 'fas_fa-grin-wink'=>'fas fa-grin-wink', 'fas_fa-grip-horizontal'=>'fas fa-grip-horizontal', 'fas_fa-grip-lines'=>'fas fa-grip-lines', 'fas_fa-grip-lines-vertical'=>'fas fa-grip-lines-vertical', 'fas_fa-grip-vertical'=>'fas fa-grip-vertical', 'fas_fa-guitar'=>'fas fa-guitar', 'fas_fa-h-square'=>'fas fa-h-square', 'fas_fa-hamburger'=>'fas fa-hamburger', 'fas_fa-hammer'=>'fas fa-hammer', 'fas_fa-hamsa'=>'fas fa-hamsa', 'fas_fa-hand-holding'=>'fas fa-hand-holding', 'fas_fa-hand-holding-heart'=>'fas fa-hand-holding-heart', 'fas_fa-hand-holding-usd'=>'fas fa-hand-holding-usd', 'fas_fa-hand-lizard'=>'fas fa-hand-lizard', 'fas_fa-hand-middle-finger'=>'fas fa-hand-middle-finger', 'fas_fa-hand-paper'=>'fas fa-hand-paper', 'fas_fa-hand-peace'=>'fas fa-hand-peace', 'fas_fa-hand-point-down'=>'fas fa-hand-point-down', 'fas_fa-hand-point-left'=>'fas fa-hand-point-left', 'fas_fa-hand-point-right'=>'fas fa-hand-point-right', 'fas_fa-hand-point-up'=>'fas fa-hand-point-up', 'fas_fa-hand-pointer'=>'fas fa-hand-pointer', 'fas_fa-hand-rock'=>'fas fa-hand-rock', 'fas_fa-hand-scissors'=>'fas fa-hand-scissors', 'fas_fa-hand-spock'=>'fas fa-hand-spock', 'fas_fa-hands'=>'fas fa-hands', 'fas_fa-hands-helping'=>'fas fa-hands-helping', 'fas_fa-handshake'=>'fas fa-handshake', 'fas_fa-hanukiah'=>'fas fa-hanukiah', 'fas_fa-hard-hat'=>'fas fa-hard-hat', 'fas_fa-hashtag'=>'fas fa-hashtag', 'fas_fa-hat-cowboy'=>'fas fa-hat-cowboy', 'fas_fa-hat-cowboy-side'=>'fas fa-hat-cowboy-side', 'fas_fa-hat-wizard'=>'fas fa-hat-wizard', 'fas_fa-hdd'=>'fas fa-hdd', 'fas_fa-heading'=>'fas fa-heading', 'fas_fa-headphones'=>'fas fa-headphones', 'fas_fa-headphones-alt'=>'fas fa-headphones-alt', 'fas_fa-headset'=>'fas fa-headset', 'fas_fa-heart'=>'fas fa-heart', 'fas_fa-heart-broken'=>'fas fa-heart-broken', 'fas_fa-heartbeat'=>'fas fa-heartbeat', 'fas_fa-helicopter'=>'fas fa-helicopter', 'fas_fa-highlighter'=>'fas fa-highlighter', 'fas_fa-hiking'=>'fas fa-hiking', 'fas_fa-hippo'=>'fas fa-hippo', 'fas_fa-history'=>'fas fa-history', 'fas_fa-hockey-puck'=>'fas fa-hockey-puck', 'fas_fa-holly-berry'=>'fas fa-holly-berry', 'fas_fa-home'=>'fas fa-home', 'fas_fa-horse'=>'fas fa-horse', 'fas_fa-horse-head'=>'fas fa-horse-head', 'fas_fa-hospital'=>'fas fa-hospital', 'fas_fa-hospital-alt'=>'fas fa-hospital-alt', 'fas_fa-hospital-symbol'=>'fas fa-hospital-symbol', 'fas_fa-hot-tub'=>'fas fa-hot-tub', 'fas_fa-hotdog'=>'fas fa-hotdog', 'fas_fa-hotel'=>'fas fa-hotel', 'fas_fa-hourglass'=>'fas fa-hourglass', 'fas_fa-hourglass-end'=>'fas fa-hourglass-end', 'fas_fa-hourglass-half'=>'fas fa-hourglass-half', 'fas_fa-hourglass-start'=>'fas fa-hourglass-start', 'fas_fa-house-damage'=>'fas fa-house-damage', 'fas_fa-hryvnia'=>'fas fa-hryvnia', 'fas_fa-i-cursor'=>'fas fa-i-cursor', 'fas_fa-ice-cream'=>'fas fa-ice-cream', 'fas_fa-icicles'=>'fas fa-icicles', 'fas_fa-icons'=>'fas fa-icons', 'fas_fa-id-badge'=>'fas fa-id-badge', 'fas_fa-id-card'=>'fas fa-id-card', 'fas_fa-id-card-alt'=>'fas fa-id-card-alt', 'fas_fa-igloo'=>'fas fa-igloo', 'fas_fa-image'=>'fas fa-image', 'fas_fa-images'=>'fas fa-images', 'fas_fa-inbox'=>'fas fa-inbox', 'fas_fa-indent'=>'fas fa-indent', 'fas_fa-industry'=>'fas fa-industry', 'fas_fa-infinity'=>'fas fa-infinity', 'fas_fa-info'=>'fas fa-info', 'fas_fa-info-circle'=>'fas fa-info-circle', 'fas_fa-italic'=>'fas fa-italic', 'fas_fa-jedi'=>'fas fa-jedi', 'fas_fa-joint'=>'fas fa-joint', 'fas_fa-journal-whills'=>'fas fa-journal-whills', 'fas_fa-kaaba'=>'fas fa-kaaba', 'fas_fa-key'=>'fas fa-key', 'fas_fa-keyboard'=>'fas fa-keyboard', 'fas_fa-khanda'=>'fas fa-khanda', 'fas_fa-kiss'=>'fas fa-kiss', 'fas_fa-kiss-beam'=>'fas fa-kiss-beam', 'fas_fa-kiss-wink-heart'=>'fas fa-kiss-wink-heart', 'fas_fa-kiwi-bird'=>'fas fa-kiwi-bird', 'fas_fa-landmark'=>'fas fa-landmark', 'fas_fa-language'=>'fas fa-language', 'fas_fa-laptop'=>'fas fa-laptop', 'fas_fa-laptop-code'=>'fas fa-laptop-code', 'fas_fa-laptop-medical'=>'fas fa-laptop-medical', 'fas_fa-laugh'=>'fas fa-laugh', 'fas_fa-laugh-beam'=>'fas fa-laugh-beam', 'fas_fa-laugh-squint'=>'fas fa-laugh-squint', 'fas_fa-laugh-wink'=>'fas fa-laugh-wink', 'fas_fa-layer-group'=>'fas fa-layer-group', 'fas_fa-leaf'=>'fas fa-leaf', 'fas_fa-lemon'=>'fas fa-lemon', 'fas_fa-less-than'=>'fas fa-less-than', 'fas_fa-less-than-equal'=>'fas fa-less-than-equal', 'fas_fa-level-down-alt'=>'fas fa-level-down-alt', 'fas_fa-level-up-alt'=>'fas fa-level-up-alt', 'fas_fa-life-ring'=>'fas fa-life-ring', 'fas_fa-lightbulb'=>'fas fa-lightbulb', 'fas_fa-link'=>'fas fa-link', 'fas_fa-lira-sign'=>'fas fa-lira-sign', 'fas_fa-list'=>'fas fa-list', 'fas_fa-list-alt'=>'fas fa-list-alt', 'fas_fa-list-ol'=>'fas fa-list-ol', 'fas_fa-list-ul'=>'fas fa-list-ul', 'fas_fa-location-arrow'=>'fas fa-location-arrow', 'fas_fa-lock'=>'fas fa-lock', 'fas_fa-lock-open'=>'fas fa-lock-open', 'fas_fa-long-arrow-alt-down'=>'fas fa-long-arrow-alt-down', 'fas_fa-long-arrow-alt-left'=>'fas fa-long-arrow-alt-left', 'fas_fa-long-arrow-alt-right'=>'fas fa-long-arrow-alt-right', 'fas_fa-long-arrow-alt-up'=>'fas fa-long-arrow-alt-up', 'fas_fa-low-vision'=>'fas fa-low-vision', 'fas_fa-luggage-cart'=>'fas fa-luggage-cart', 'fas_fa-magic'=>'fas fa-magic', 'fas_fa-magnet'=>'fas fa-magnet', 'fas_fa-mail-bulk'=>'fas fa-mail-bulk', 'fas_fa-male'=>'fas fa-male', 'fas_fa-map'=>'fas fa-map', 'fas_fa-map-marked'=>'fas fa-map-marked', 'fas_fa-map-marked-alt'=>'fas fa-map-marked-alt', 'fas_fa-map-marker'=>'fas fa-map-marker', 'fas_fa-map-marker-alt'=>'fas fa-map-marker-alt', 'fas_fa-map-pin'=>'fas fa-map-pin', 'fas_fa-map-signs'=>'fas fa-map-signs', 'fas_fa-marker'=>'fas fa-marker', 'fas_fa-mars'=>'fas fa-mars', 'fas_fa-mars-double'=>'fas fa-mars-double', 'fas_fa-mars-stroke'=>'fas fa-mars-stroke', 'fas_fa-mars-stroke-h'=>'fas fa-mars-stroke-h', 'fas_fa-mars-stroke-v'=>'fas fa-mars-stroke-v', 'fas_fa-mask'=>'fas fa-mask', 'fas_fa-medal'=>'fas fa-medal', 'fas_fa-medkit'=>'fas fa-medkit', 'fas_fa-meh'=>'fas fa-meh', 'fas_fa-meh-blank'=>'fas fa-meh-blank', 'fas_fa-meh-rolling-eyes'=>'fas fa-meh-rolling-eyes', 'fas_fa-memory'=>'fas fa-memory', 'fas_fa-menorah'=>'fas fa-menorah', 'fas_fa-mercury'=>'fas fa-mercury', 'fas_fa-meteor'=>'fas fa-meteor', 'fas_fa-microchip'=>'fas fa-microchip', 'fas_fa-microphone'=>'fas fa-microphone', 'fas_fa-microphone-alt'=>'fas fa-microphone-alt', 'fas_fa-microphone-alt-slash'=>'fas fa-microphone-alt-slash', 'fas_fa-microphone-slash'=>'fas fa-microphone-slash', 'fas_fa-microscope'=>'fas fa-microscope', 'fas_fa-minus'=>'fas fa-minus', 'fas_fa-minus-circle'=>'fas fa-minus-circle', 'fas_fa-minus-square'=>'fas fa-minus-square', 'fas_fa-mitten'=>'fas fa-mitten', 'fas_fa-mobile'=>'fas fa-mobile', 'fas_fa-mobile-alt'=>'fas fa-mobile-alt', 'fas_fa-money-bill'=>'fas fa-money-bill', 'fas_fa-money-bill-alt'=>'fas fa-money-bill-alt', 'fas_fa-money-bill-wave'=>'fas fa-money-bill-wave', 'fas_fa-money-bill-wave-alt'=>'fas fa-money-bill-wave-alt', 'fas_fa-money-check'=>'fas fa-money-check', 'fas_fa-money-check-alt'=>'fas fa-money-check-alt', 'fas_fa-monument'=>'fas fa-monument', 'fas_fa-moon'=>'fas fa-moon', 'fas_fa-mortar-pestle'=>'fas fa-mortar-pestle', 'fas_fa-mosque'=>'fas fa-mosque', 'fas_fa-motorcycle'=>'fas fa-motorcycle', 'fas_fa-mountain'=>'fas fa-mountain', 'fas_fa-mouse'=>'fas fa-mouse', 'fas_fa-mouse-pointer'=>'fas fa-mouse-pointer', 'fas_fa-mug-hot'=>'fas fa-mug-hot', 'fas_fa-music'=>'fas fa-music', 'fas_fa-network-wired'=>'fas fa-network-wired', 'fas_fa-neuter'=>'fas fa-neuter', 'fas_fa-newspaper'=>'fas fa-newspaper', 'fas_fa-not-equal'=>'fas fa-not-equal', 'fas_fa-notes-medical'=>'fas fa-notes-medical', 'fas_fa-object-group'=>'fas fa-object-group', 'fas_fa-object-ungroup'=>'fas fa-object-ungroup', 'fas_fa-oil-can'=>'fas fa-oil-can', 'fas_fa-om'=>'fas fa-om', 'fas_fa-otter'=>'fas fa-otter', 'fas_fa-outdent'=>'fas fa-outdent', 'fas_fa-pager'=>'fas fa-pager', 'fas_fa-paint-brush'=>'fas fa-paint-brush', 'fas_fa-paint-roller'=>'fas fa-paint-roller', 'fas_fa-palette'=>'fas fa-palette', 'fas_fa-pallet'=>'fas fa-pallet', 'fas_fa-paper-plane'=>'fas fa-paper-plane', 'fas_fa-paperclip'=>'fas fa-paperclip', 'fas_fa-parachute-box'=>'fas fa-parachute-box', 'fas_fa-paragraph'=>'fas fa-paragraph', 'fas_fa-parking'=>'fas fa-parking', 'fas_fa-passport'=>'fas fa-passport', 'fas_fa-pastafarianism'=>'fas fa-pastafarianism', 'fas_fa-paste'=>'fas fa-paste', 'fas_fa-pause'=>'fas fa-pause', 'fas_fa-pause-circle'=>'fas fa-pause-circle', 'fas_fa-paw'=>'fas fa-paw', 'fas_fa-peace'=>'fas fa-peace', 'fas_fa-pen'=>'fas fa-pen', 'fas_fa-pen-alt'=>'fas fa-pen-alt', 'fas_fa-pen-fancy'=>'fas fa-pen-fancy', 'fas_fa-pen-nib'=>'fas fa-pen-nib', 'fas_fa-pen-square'=>'fas fa-pen-square', 'fas_fa-pencil-alt'=>'fas fa-pencil-alt', 'fas_fa-pencil-ruler'=>'fas fa-pencil-ruler', 'fas_fa-people-carry'=>'fas fa-people-carry', 'fas_fa-pepper-hot'=>'fas fa-pepper-hot', 'fas_fa-percent'=>'fas fa-percent', 'fas_fa-percentage'=>'fas fa-percentage', 'fas_fa-person-booth'=>'fas fa-person-booth', 'fas_fa-phone'=>'fas fa-phone', 'fas_fa-phone-alt'=>'fas fa-phone-alt', 'fas_fa-phone-slash'=>'fas fa-phone-slash', 'fas_fa-phone-square'=>'fas fa-phone-square', 'fas_fa-phone-square-alt'=>'fas fa-phone-square-alt', 'fas_fa-phone-volume'=>'fas fa-phone-volume', 'fas_fa-photo-video'=>'fas fa-photo-video', 'fas_fa-piggy-bank'=>'fas fa-piggy-bank', 'fas_fa-pills'=>'fas fa-pills', 'fas_fa-pizza-slice'=>'fas fa-pizza-slice', 'fas_fa-place-of-worship'=>'fas fa-place-of-worship', 'fas_fa-plane'=>'fas fa-plane', 'fas_fa-plane-arrival'=>'fas fa-plane-arrival', 'fas_fa-plane-departure'=>'fas fa-plane-departure', 'fas_fa-play'=>'fas fa-play', 'fas_fa-play-circle'=>'fas fa-play-circle', 'fas_fa-plug'=>'fas fa-plug', 'fas_fa-plus'=>'fas fa-plus', 'fas_fa-plus-circle'=>'fas fa-plus-circle', 'fas_fa-plus-square'=>'fas fa-plus-square', 'fas_fa-podcast'=>'fas fa-podcast', 'fas_fa-poll'=>'fas fa-poll', 'fas_fa-poll-h'=>'fas fa-poll-h', 'fas_fa-poo'=>'fas fa-poo', 'fas_fa-poo-storm'=>'fas fa-poo-storm', 'fas_fa-poop'=>'fas fa-poop', 'fas_fa-portrait'=>'fas fa-portrait', 'fas_fa-pound-sign'=>'fas fa-pound-sign', 'fas_fa-power-off'=>'fas fa-power-off', 'fas_fa-pray'=>'fas fa-pray', 'fas_fa-praying-hands'=>'fas fa-praying-hands', 'fas_fa-prescription'=>'fas fa-prescription', 'fas_fa-prescription-bottle'=>'fas fa-prescription-bottle', 'fas_fa-prescription-bottle-alt'=>'fas fa-prescription-bottle-alt', 'fas_fa-print'=>'fas fa-print', 'fas_fa-procedures'=>'fas fa-procedures', 'fas_fa-project-diagram'=>'fas fa-project-diagram', 'fas_fa-puzzle-piece'=>'fas fa-puzzle-piece', 'fas_fa-qrcode'=>'fas fa-qrcode', 'fas_fa-question'=>'fas fa-question', 'fas_fa-question-circle'=>'fas fa-question-circle', 'fas_fa-quidditch'=>'fas fa-quidditch', 'fas_fa-quote-left'=>'fas fa-quote-left', 'fas_fa-quote-right'=>'fas fa-quote-right', 'fas_fa-quran'=>'fas fa-quran', 'fas_fa-radiation'=>'fas fa-radiation', 'fas_fa-radiation-alt'=>'fas fa-radiation-alt', 'fas_fa-rainbow'=>'fas fa-rainbow', 'fas_fa-random'=>'fas fa-random', 'fas_fa-receipt'=>'fas fa-receipt', 'fas_fa-record-vinyl'=>'fas fa-record-vinyl', 'fas_fa-recycle'=>'fas fa-recycle', 'fas_fa-redo'=>'fas fa-redo', 'fas_fa-redo-alt'=>'fas fa-redo-alt', 'fas_fa-registered'=>'fas fa-registered', 'fas_fa-remove-format'=>'fas fa-remove-format', 'fas_fa-reply'=>'fas fa-reply', 'fas_fa-reply-all'=>'fas fa-reply-all', 'fas_fa-republican'=>'fas fa-republican', 'fas_fa-restroom'=>'fas fa-restroom', 'fas_fa-retweet'=>'fas fa-retweet', 'fas_fa-ribbon'=>'fas fa-ribbon', 'fas_fa-ring'=>'fas fa-ring', 'fas_fa-road'=>'fas fa-road', 'fas_fa-robot'=>'fas fa-robot', 'fas_fa-rocket'=>'fas fa-rocket', 'fas_fa-route'=>'fas fa-route', 'fas_fa-rss'=>'fas fa-rss', 'fas_fa-rss-square'=>'fas fa-rss-square', 'fas_fa-ruble-sign'=>'fas fa-ruble-sign', 'fas_fa-ruler'=>'fas fa-ruler', 'fas_fa-ruler-combined'=>'fas fa-ruler-combined', 'fas_fa-ruler-horizontal'=>'fas fa-ruler-horizontal', 'fas_fa-ruler-vertical'=>'fas fa-ruler-vertical', 'fas_fa-running'=>'fas fa-running', 'fas_fa-rupee-sign'=>'fas fa-rupee-sign', 'fas_fa-sad-cry'=>'fas fa-sad-cry', 'fas_fa-sad-tear'=>'fas fa-sad-tear', 'fas_fa-satellite'=>'fas fa-satellite', 'fas_fa-satellite-dish'=>'fas fa-satellite-dish', 'fas_fa-save'=>'fas fa-save', 'fas_fa-school'=>'fas fa-school', 'fas_fa-screwdriver'=>'fas fa-screwdriver', 'fas_fa-scroll'=>'fas fa-scroll', 'fas_fa-sd-card'=>'fas fa-sd-card', 'fas_fa-search'=>'fas fa-search', 'fas_fa-search-dollar'=>'fas fa-search-dollar', 'fas_fa-search-location'=>'fas fa-search-location', 'fas_fa-search-minus'=>'fas fa-search-minus', 'fas_fa-search-plus'=>'fas fa-search-plus', 'fas_fa-seedling'=>'fas fa-seedling', 'fas_fa-server'=>'fas fa-server', 'fas_fa-shapes'=>'fas fa-shapes', 'fas_fa-share'=>'fas fa-share', 'fas_fa-share-alt'=>'fas fa-share-alt', 'fas_fa-share-alt-square'=>'fas fa-share-alt-square', 'fas_fa-share-square'=>'fas fa-share-square', 'fas_fa-shekel-sign'=>'fas fa-shekel-sign', 'fas_fa-shield-alt'=>'fas fa-shield-alt', 'fas_fa-ship'=>'fas fa-ship', 'fas_fa-shipping-fast'=>'fas fa-shipping-fast', 'fas_fa-shoe-prints'=>'fas fa-shoe-prints', 'fas_fa-shopping-bag'=>'fas fa-shopping-bag', 'fas_fa-shopping-basket'=>'fas fa-shopping-basket', 'fas_fa-shopping-cart'=>'fas fa-shopping-cart', 'fas_fa-shower'=>'fas fa-shower', 'fas_fa-shuttle-van'=>'fas fa-shuttle-van', 'fas_fa-sign'=>'fas fa-sign', 'fas_fa-sign-in-alt'=>'fas fa-sign-in-alt', 'fas_fa-sign-language'=>'fas fa-sign-language', 'fas_fa-sign-out-alt'=>'fas fa-sign-out-alt', 'fas_fa-signal'=>'fas fa-signal', 'fas_fa-signature'=>'fas fa-signature', 'fas_fa-sim-card'=>'fas fa-sim-card', 'fas_fa-sitemap'=>'fas fa-sitemap', 'fas_fa-skating'=>'fas fa-skating', 'fas_fa-skiing'=>'fas fa-skiing', 'fas_fa-skiing-nordic'=>'fas fa-skiing-nordic', 'fas_fa-skull'=>'fas fa-skull', 'fas_fa-skull-crossbones'=>'fas fa-skull-crossbones', 'fas_fa-slash'=>'fas fa-slash', 'fas_fa-sleigh'=>'fas fa-sleigh', 'fas_fa-sliders-h'=>'fas fa-sliders-h', 'fas_fa-smile'=>'fas fa-smile', 'fas_fa-smile-beam'=>'fas fa-smile-beam', 'fas_fa-smile-wink'=>'fas fa-smile-wink', 'fas_fa-smog'=>'fas fa-smog', 'fas_fa-smoking'=>'fas fa-smoking', 'fas_fa-smoking-ban'=>'fas fa-smoking-ban', 'fas_fa-sms'=>'fas fa-sms', 'fas_fa-snowboarding'=>'fas fa-snowboarding', 'fas_fa-snowflake'=>'fas fa-snowflake', 'fas_fa-snowman'=>'fas fa-snowman', 'fas_fa-snowplow'=>'fas fa-snowplow', 'fas_fa-socks'=>'fas fa-socks', 'fas_fa-solar-panel'=>'fas fa-solar-panel', 'fas_fa-sort'=>'fas fa-sort', 'fas_fa-sort-alpha-down'=>'fas fa-sort-alpha-down', 'fas_fa-sort-alpha-down-alt'=>'fas fa-sort-alpha-down-alt', 'fas_fa-sort-alpha-up'=>'fas fa-sort-alpha-up', 'fas_fa-sort-alpha-up-alt'=>'fas fa-sort-alpha-up-alt', 'fas_fa-sort-amount-down'=>'fas fa-sort-amount-down', 'fas_fa-sort-amount-down-alt'=>'fas fa-sort-amount-down-alt', 'fas_fa-sort-amount-up'=>'fas fa-sort-amount-up', 'fas_fa-sort-amount-up-alt'=>'fas fa-sort-amount-up-alt', 'fas_fa-sort-down'=>'fas fa-sort-down', 'fas_fa-sort-numeric-down'=>'fas fa-sort-numeric-down', 'fas_fa-sort-numeric-down-alt'=>'fas fa-sort-numeric-down-alt', 'fas_fa-sort-numeric-up'=>'fas fa-sort-numeric-up', 'fas_fa-sort-numeric-up-alt'=>'fas fa-sort-numeric-up-alt', 'fas_fa-sort-up'=>'fas fa-sort-up', 'fas_fa-spa'=>'fas fa-spa', 'fas_fa-space-shuttle'=>'fas fa-space-shuttle', 'fas_fa-spell-check'=>'fas fa-spell-check', 'fas_fa-spider'=>'fas fa-spider', 'fas_fa-spinner'=>'fas fa-spinner', 'fas_fa-splotch'=>'fas fa-splotch', 'fas_fa-spray-can'=>'fas fa-spray-can', 'fas_fa-square'=>'fas fa-square', 'fas_fa-square-full'=>'fas fa-square-full', 'fas_fa-square-root-alt'=>'fas fa-square-root-alt', 'fas_fa-stamp'=>'fas fa-stamp', 'fas_fa-star'=>'fas fa-star', 'fas_fa-star-and-crescent'=>'fas fa-star-and-crescent', 'fas_fa-star-half'=>'fas fa-star-half', 'fas_fa-star-half-alt'=>'fas fa-star-half-alt', 'fas_fa-star-of-david'=>'fas fa-star-of-david', 'fas_fa-star-of-life'=>'fas fa-star-of-life', 'fas_fa-step-backward'=>'fas fa-step-backward', 'fas_fa-step-forward'=>'fas fa-step-forward', 'fas_fa-stethoscope'=>'fas fa-stethoscope', 'fas_fa-sticky-note'=>'fas fa-sticky-note', 'fas_fa-stop'=>'fas fa-stop', 'fas_fa-stop-circle'=>'fas fa-stop-circle', 'fas_fa-stopwatch'=>'fas fa-stopwatch', 'fas_fa-store'=>'fas fa-store', 'fas_fa-store-alt'=>'fas fa-store-alt', 'fas_fa-stream'=>'fas fa-stream', 'fas_fa-street-view'=>'fas fa-street-view', 'fas_fa-strikethrough'=>'fas fa-strikethrough', 'fas_fa-stroopwafel'=>'fas fa-stroopwafel', 'fas_fa-subscript'=>'fas fa-subscript', 'fas_fa-subway'=>'fas fa-subway', 'fas_fa-suitcase'=>'fas fa-suitcase', 'fas_fa-suitcase-rolling'=>'fas fa-suitcase-rolling', 'fas_fa-sun'=>'fas fa-sun', 'fas_fa-superscript'=>'fas fa-superscript', 'fas_fa-surprise'=>'fas fa-surprise', 'fas_fa-swatchbook'=>'fas fa-swatchbook', 'fas_fa-swimmer'=>'fas fa-swimmer', 'fas_fa-swimming-pool'=>'fas fa-swimming-pool', 'fas_fa-synagogue'=>'fas fa-synagogue', 'fas_fa-sync'=>'fas fa-sync', 'fas_fa-sync-alt'=>'fas fa-sync-alt', 'fas_fa-syringe'=>'fas fa-syringe', 'fas_fa-table'=>'fas fa-table', 'fas_fa-table-tennis'=>'fas fa-table-tennis', 'fas_fa-tablet'=>'fas fa-tablet', 'fas_fa-tablet-alt'=>'fas fa-tablet-alt', 'fas_fa-tablets'=>'fas fa-tablets', 'fas_fa-tachometer-alt'=>'fas fa-tachometer-alt', 'fas_fa-tag'=>'fas fa-tag', 'fas_fa-tags'=>'fas fa-tags', 'fas_fa-tape'=>'fas fa-tape', 'fas_fa-tasks'=>'fas fa-tasks', 'fas_fa-taxi'=>'fas fa-taxi', 'fas_fa-teeth'=>'fas fa-teeth', 'fas_fa-teeth-open'=>'fas fa-teeth-open', 'fas_fa-temperature-high'=>'fas fa-temperature-high', 'fas_fa-temperature-low'=>'fas fa-temperature-low', 'fas_fa-tenge'=>'fas fa-tenge', 'fas_fa-terminal'=>'fas fa-terminal', 'fas_fa-text-height'=>'fas fa-text-height', 'fas_fa-text-width'=>'fas fa-text-width', 'fas_fa-th'=>'fas fa-th', 'fas_fa-th-large'=>'fas fa-th-large', 'fas_fa-th-list'=>'fas fa-th-list', 'fas_fa-theater-masks'=>'fas fa-theater-masks', 'fas_fa-thermometer'=>'fas fa-thermometer', 'fas_fa-thermometer-empty'=>'fas fa-thermometer-empty', 'fas_fa-thermometer-full'=>'fas fa-thermometer-full', 'fas_fa-thermometer-half'=>'fas fa-thermometer-half', 'fas_fa-thermometer-quarter'=>'fas fa-thermometer-quarter', 'fas_fa-thermometer-three-quarters'=>'fas fa-thermometer-three-quarters', 'fas_fa-thumbs-down'=>'fas fa-thumbs-down', 'fas_fa-thumbs-up'=>'fas fa-thumbs-up', 'fas_fa-thumbtack'=>'fas fa-thumbtack', 'fas_fa-ticket-alt'=>'fas fa-ticket-alt', 'fas_fa-times'=>'fas fa-times', 'fas_fa-times-circle'=>'fas fa-times-circle', 'fas_fa-tint'=>'fas fa-tint', 'fas_fa-tint-slash'=>'fas fa-tint-slash', 'fas_fa-tired'=>'fas fa-tired', 'fas_fa-toggle-off'=>'fas fa-toggle-off', 'fas_fa-toggle-on'=>'fas fa-toggle-on', 'fas_fa-toilet'=>'fas fa-toilet', 'fas_fa-toilet-paper'=>'fas fa-toilet-paper', 'fas_fa-toolbox'=>'fas fa-toolbox', 'fas_fa-tools'=>'fas fa-tools', 'fas_fa-tooth'=>'fas fa-tooth', 'fas_fa-torah'=>'fas fa-torah', 'fas_fa-torii-gate'=>'fas fa-torii-gate', 'fas_fa-tractor'=>'fas fa-tractor', 'fas_fa-trademark'=>'fas fa-trademark', 'fas_fa-traffic-light'=>'fas fa-traffic-light', 'fas_fa-trailer'=>'fas fa-trailer', 'fas_fa-train'=>'fas fa-train', 'fas_fa-tram'=>'fas fa-tram', 'fas_fa-transgender'=>'fas fa-transgender', 'fas_fa-transgender-alt'=>'fas fa-transgender-alt', 'fas_fa-trash'=>'fas fa-trash', 'fas_fa-trash-alt'=>'fas fa-trash-alt', 'fas_fa-trash-restore'=>'fas fa-trash-restore', 'fas_fa-trash-restore-alt'=>'fas fa-trash-restore-alt', 'fas_fa-tree'=>'fas fa-tree', 'fas_fa-trophy'=>'fas fa-trophy', 'fas_fa-truck'=>'fas fa-truck', 'fas_fa-truck-loading'=>'fas fa-truck-loading', 'fas_fa-truck-monster'=>'fas fa-truck-monster', 'fas_fa-truck-moving'=>'fas fa-truck-moving', 'fas_fa-truck-pickup'=>'fas fa-truck-pickup', 'fas_fa-tshirt'=>'fas fa-tshirt', 'fas_fa-tty'=>'fas fa-tty', 'fas_fa-tv'=>'fas fa-tv', 'fas_fa-umbrella'=>'fas fa-umbrella', 'fas_fa-umbrella-beach'=>'fas fa-umbrella-beach', 'fas_fa-underline'=>'fas fa-underline', 'fas_fa-undo'=>'fas fa-undo', 'fas_fa-undo-alt'=>'fas fa-undo-alt', 'fas_fa-universal-access'=>'fas fa-universal-access', 'fas_fa-university'=>'fas fa-university', 'fas_fa-unlink'=>'fas fa-unlink', 'fas_fa-unlock'=>'fas fa-unlock', 'fas_fa-unlock-alt'=>'fas fa-unlock-alt', 'fas_fa-upload'=>'fas fa-upload', 'fas_fa-user'=>'fas fa-user', 'fas_fa-user-alt'=>'fas fa-user-alt', 'fas_fa-user-alt-slash'=>'fas fa-user-alt-slash', 'fas_fa-user-astronaut'=>'fas fa-user-astronaut', 'fas_fa-user-check'=>'fas fa-user-check', 'fas_fa-user-circle'=>'fas fa-user-circle', 'fas_fa-user-clock'=>'fas fa-user-clock', 'fas_fa-user-cog'=>'fas fa-user-cog', 'fas_fa-user-edit'=>'fas fa-user-edit', 'fas_fa-user-friends'=>'fas fa-user-friends', 'fas_fa-user-graduate'=>'fas fa-user-graduate', 'fas_fa-user-injured'=>'fas fa-user-injured', 'fas_fa-user-lock'=>'fas fa-user-lock', 'fas_fa-user-md'=>'fas fa-user-md', 'fas_fa-user-minus'=>'fas fa-user-minus', 'fas_fa-user-ninja'=>'fas fa-user-ninja', 'fas_fa-user-nurse'=>'fas fa-user-nurse', 'fas_fa-user-plus'=>'fas fa-user-plus', 'fas_fa-user-secret'=>'fas fa-user-secret', 'fas_fa-user-shield'=>'fas fa-user-shield', 'fas_fa-user-slash'=>'fas fa-user-slash', 'fas_fa-user-tag'=>'fas fa-user-tag', 'fas_fa-user-tie'=>'fas fa-user-tie', 'fas_fa-user-times'=>'fas fa-user-times', 'fas_fa-users'=>'fas fa-users', 'fas_fa-users-cog'=>'fas fa-users-cog', 'fas_fa-utensil-spoon'=>'fas fa-utensil-spoon', 'fas_fa-utensils'=>'fas fa-utensils', 'fas_fa-vector-square'=>'fas fa-vector-square', 'fas_fa-venus'=>'fas fa-venus', 'fas_fa-venus-double'=>'fas fa-venus-double', 'fas_fa-venus-mars'=>'fas fa-venus-mars', 'fas_fa-vial'=>'fas fa-vial', 'fas_fa-vials'=>'fas fa-vials', 'fas_fa-video'=>'fas fa-video', 'fas_fa-video-slash'=>'fas fa-video-slash', 'fas_fa-vihara'=>'fas fa-vihara', 'fas_fa-voicemail'=>'fas fa-voicemail', 'fas_fa-volleyball-ball'=>'fas fa-volleyball-ball', 'fas_fa-volume-down'=>'fas fa-volume-down', 'fas_fa-volume-mute'=>'fas fa-volume-mute', 'fas_fa-volume-off'=>'fas fa-volume-off', 'fas_fa-volume-up'=>'fas fa-volume-up', 'fas_fa-vote-yea'=>'fas fa-vote-yea', 'fas_fa-vr-cardboard'=>'fas fa-vr-cardboard', 'fas_fa-walking'=>'fas fa-walking', 'fas_fa-wallet'=>'fas fa-wallet', 'fas_fa-warehouse'=>'fas fa-warehouse', 'fas_fa-water'=>'fas fa-water', 'fas_fa-wave-square'=>'fas fa-wave-square', 'fas_fa-weight'=>'fas fa-weight', 'fas_fa-weight-hanging'=>'fas fa-weight-hanging', 'fas_fa-wheelchair'=>'fas fa-wheelchair', 'fas_fa-wifi'=>'fas fa-wifi', 'fas_fa-wind'=>'fas fa-wind', 'fas_fa-window-close'=>'fas fa-window-close', 'fas_fa-window-maximize'=>'fas fa-window-maximize', 'fas_fa-window-minimize'=>'fas fa-window-minimize', 'fas_fa-window-restore'=>'fas fa-window-restore', 'fas_fa-wine-bottle'=>'fas fa-wine-bottle', 'fas_fa-wine-glass'=>'fas fa-wine-glass', 'fas_fa-wine-glass-alt'=>'fas fa-wine-glass-alt', 'fas_fa-won-sign'=>'fas fa-won-sign', 'fas_fa-wrench'=>'fas fa-wrench', 'fas_fa-x-ray'=>'fas fa-x-ray', 'fas_fa-yen-sign'=>'fas fa-yen-sign', 'fas_fa-yin-yang'=>'fas fa-yin-yang', 'far_fa-address-book'=>'far fa-address-book', 'far_fa-address-card'=>'far fa-address-card', 'far_fa-angry'=>'far fa-angry', 'far_fa-arrow-alt-circle-down'=>'far fa-arrow-alt-circle-down', 'far_fa-arrow-alt-circle-left'=>'far fa-arrow-alt-circle-left', 'far_fa-arrow-alt-circle-right'=>'far fa-arrow-alt-circle-right', 'far_fa-arrow-alt-circle-up'=>'far fa-arrow-alt-circle-up', 'far_fa-bell'=>'far fa-bell', 'far_fa-bell-slash'=>'far fa-bell-slash', 'far_fa-bookmark'=>'far fa-bookmark', 'far_fa-building'=>'far fa-building', 'far_fa-calendar'=>'far fa-calendar', 'far_fa-calendar-alt'=>'far fa-calendar-alt', 'far_fa-calendar-check'=>'far fa-calendar-check', 'far_fa-calendar-minus'=>'far fa-calendar-minus', 'far_fa-calendar-plus'=>'far fa-calendar-plus', 'far_fa-calendar-times'=>'far fa-calendar-times', 'far_fa-caret-square-down'=>'far fa-caret-square-down', 'far_fa-caret-square-left'=>'far fa-caret-square-left', 'far_fa-caret-square-right'=>'far fa-caret-square-right', 'far_fa-caret-square-up'=>'far fa-caret-square-up', 'far_fa-chart-bar'=>'far fa-chart-bar', 'far_fa-check-circle'=>'far fa-check-circle', 'far_fa-check-square'=>'far fa-check-square', 'far_fa-circle'=>'far fa-circle', 'far_fa-clipboard'=>'far fa-clipboard', 'far_fa-clock'=>'far fa-clock', 'far_fa-clone'=>'far fa-clone', 'far_fa-closed-captioning'=>'far fa-closed-captioning', 'far_fa-comment'=>'far fa-comment', 'far_fa-comment-alt'=>'far fa-comment-alt', 'far_fa-comment-dots'=>'far fa-comment-dots', 'far_fa-comments'=>'far fa-comments', 'far_fa-compass'=>'far fa-compass', 'far_fa-copy'=>'far fa-copy', 'far_fa-copyright'=>'far fa-copyright', 'far_fa-credit-card'=>'far fa-credit-card', 'far_fa-dizzy'=>'far fa-dizzy', 'far_fa-dot-circle'=>'far fa-dot-circle', 'far_fa-edit'=>'far fa-edit', 'far_fa-envelope'=>'far fa-envelope', 'far_fa-envelope-open'=>'far fa-envelope-open', 'far_fa-eye'=>'far fa-eye', 'far_fa-eye-slash'=>'far fa-eye-slash', 'far_fa-file'=>'far fa-file', 'far_fa-file-alt'=>'far fa-file-alt', 'far_fa-file-archive'=>'far fa-file-archive', 'far_fa-file-audio'=>'far fa-file-audio', 'far_fa-file-code'=>'far fa-file-code', 'far_fa-file-excel'=>'far fa-file-excel', 'far_fa-file-image'=>'far fa-file-image', 'far_fa-file-pdf'=>'far fa-file-pdf', 'far_fa-file-powerpoint'=>'far fa-file-powerpoint', 'far_fa-file-video'=>'far fa-file-video', 'far_fa-file-word'=>'far fa-file-word', 'far_fa-flag'=>'far fa-flag', 'far_fa-flushed'=>'far fa-flushed', 'far_fa-folder'=>'far fa-folder', 'far_fa-folder-open'=>'far fa-folder-open', 'far_fa-frown'=>'far fa-frown', 'far_fa-frown-open'=>'far fa-frown-open', 'far_fa-futbol'=>'far fa-futbol', 'far_fa-gem'=>'far fa-gem', 'far_fa-grimace'=>'far fa-grimace', 'far_fa-grin'=>'far fa-grin', 'far_fa-grin-alt'=>'far fa-grin-alt', 'far_fa-grin-beam'=>'far fa-grin-beam', 'far_fa-grin-beam-sweat'=>'far fa-grin-beam-sweat', 'far_fa-grin-hearts'=>'far fa-grin-hearts', 'far_fa-grin-squint'=>'far fa-grin-squint', 'far_fa-grin-squint-tears'=>'far fa-grin-squint-tears', 'far_fa-grin-stars'=>'far fa-grin-stars', 'far_fa-grin-tears'=>'far fa-grin-tears', 'far_fa-grin-tongue'=>'far fa-grin-tongue', 'far_fa-grin-tongue-squint'=>'far fa-grin-tongue-squint', 'far_fa-grin-tongue-wink'=>'far fa-grin-tongue-wink', 'far_fa-grin-wink'=>'far fa-grin-wink', 'far_fa-hand-lizard'=>'far fa-hand-lizard', 'far_fa-hand-paper'=>'far fa-hand-paper', 'far_fa-hand-peace'=>'far fa-hand-peace', 'far_fa-hand-point-down'=>'far fa-hand-point-down', 'far_fa-hand-point-left'=>'far fa-hand-point-left', 'far_fa-hand-point-right'=>'far fa-hand-point-right', 'far_fa-hand-point-up'=>'far fa-hand-point-up', 'far_fa-hand-pointer'=>'far fa-hand-pointer', 'far_fa-hand-rock'=>'far fa-hand-rock', 'far_fa-hand-scissors'=>'far fa-hand-scissors', 'far_fa-hand-spock'=>'far fa-hand-spock', 'far_fa-handshake'=>'far fa-handshake', 'far_fa-hdd'=>'far fa-hdd', 'far_fa-heart'=>'far fa-heart', 'far_fa-hospital'=>'far fa-hospital', 'far_fa-hourglass'=>'far fa-hourglass', 'far_fa-id-badge'=>'far fa-id-badge', 'far_fa-id-card'=>'far fa-id-card', 'far_fa-image'=>'far fa-image', 'far_fa-images'=>'far fa-images', 'far_fa-keyboard'=>'far fa-keyboard', 'far_fa-kiss'=>'far fa-kiss', 'far_fa-kiss-beam'=>'far fa-kiss-beam', 'far_fa-kiss-wink-heart'=>'far fa-kiss-wink-heart', 'far_fa-laugh'=>'far fa-laugh', 'far_fa-laugh-beam'=>'far fa-laugh-beam', 'far_fa-laugh-squint'=>'far fa-laugh-squint', 'far_fa-laugh-wink'=>'far fa-laugh-wink', 'far_fa-lemon'=>'far fa-lemon', 'far_fa-life-ring'=>'far fa-life-ring', 'far_fa-lightbulb'=>'far fa-lightbulb', 'far_fa-list-alt'=>'far fa-list-alt', 'far_fa-map'=>'far fa-map', 'far_fa-meh'=>'far fa-meh', 'far_fa-meh-blank'=>'far fa-meh-blank', 'far_fa-meh-rolling-eyes'=>'far fa-meh-rolling-eyes', 'far_fa-minus-square'=>'far fa-minus-square', 'far_fa-money-bill-alt'=>'far fa-money-bill-alt', 'far_fa-moon'=>'far fa-moon', 'far_fa-newspaper'=>'far fa-newspaper', 'far_fa-object-group'=>'far fa-object-group', 'far_fa-object-ungroup'=>'far fa-object-ungroup', 'far_fa-paper-plane'=>'far fa-paper-plane', 'far_fa-pause-circle'=>'far fa-pause-circle', 'far_fa-play-circle'=>'far fa-play-circle', 'far_fa-plus-square'=>'far fa-plus-square', 'far_fa-question-circle'=>'far fa-question-circle', 'far_fa-registered'=>'far fa-registered', 'far_fa-sad-cry'=>'far fa-sad-cry', 'far_fa-sad-tear'=>'far fa-sad-tear', 'far_fa-save'=>'far fa-save', 'far_fa-share-square'=>'far fa-share-square', 'far_fa-smile'=>'far fa-smile', 'far_fa-smile-beam'=>'far fa-smile-beam', 'far_fa-smile-wink'=>'far fa-smile-wink', 'far_fa-snowflake'=>'far fa-snowflake', 'far_fa-square'=>'far fa-square', 'far_fa-star'=>'far fa-star', 'far_fa-star-half'=>'far fa-star-half', 'far_fa-sticky-note'=>'far fa-sticky-note', 'far_fa-stop-circle'=>'far fa-stop-circle', 'far_fa-sun'=>'far fa-sun', 'far_fa-surprise'=>'far fa-surprise', 'far_fa-thumbs-down'=>'far fa-thumbs-down', 'far_fa-thumbs-up'=>'far fa-thumbs-up', 'far_fa-times-circle'=>'far fa-times-circle', 'far_fa-tired'=>'far fa-tired', 'far_fa-trash-alt'=>'far fa-trash-alt', 'far_fa-user'=>'far fa-user', 'far_fa-user-circle'=>'far fa-user-circle', 'far_fa-window-close'=>'far fa-window-close', 'far_fa-window-maximize'=>'far fa-window-maximize', 'far_fa-window-minimize'=>'far fa-window-minimize', 'far_fa-window-restore'=>'far fa-window-restore', 'fab_fa-500px'=>'fab fa-500px', 'fab_fa-accessible-icon'=>'fab fa-accessible-icon', 'fab_fa-accusoft'=>'fab fa-accusoft', 'fab_fa-acquisitions-incorporated'=>'fab fa-acquisitions-incorporated', 'fab_fa-adn'=>'fab fa-adn', 'fab_fa-adobe'=>'fab fa-adobe', 'fab_fa-adversal'=>'fab fa-adversal', 'fab_fa-affiliatetheme'=>'fab fa-affiliatetheme', 'fab_fa-airbnb'=>'fab fa-airbnb', 'fab_fa-algolia'=>'fab fa-algolia', 'fab_fa-alipay'=>'fab fa-alipay', 'fab_fa-amazon'=>'fab fa-amazon', 'fab_fa-amazon-pay'=>'fab fa-amazon-pay', 'fab_fa-amilia'=>'fab fa-amilia', 'fab_fa-android'=>'fab fa-android', 'fab_fa-angellist'=>'fab fa-angellist', 'fab_fa-angrycreative'=>'fab fa-angrycreative', 'fab_fa-angular'=>'fab fa-angular', 'fab_fa-app-store'=>'fab fa-app-store', 'fab_fa-app-store-ios'=>'fab fa-app-store-ios', 'fab_fa-apper'=>'fab fa-apper', 'fab_fa-apple'=>'fab fa-apple', 'fab_fa-apple-pay'=>'fab fa-apple-pay', 'fab_fa-artstation'=>'fab fa-artstation', 'fab_fa-asymmetrik'=>'fab fa-asymmetrik', 'fab_fa-atlassian'=>'fab fa-atlassian', 'fab_fa-audible'=>'fab fa-audible', 'fab_fa-autoprefixer'=>'fab fa-autoprefixer', 'fab_fa-avianex'=>'fab fa-avianex', 'fab_fa-aviato'=>'fab fa-aviato', 'fab_fa-aws'=>'fab fa-aws', 'fab_fa-bandcamp'=>'fab fa-bandcamp', 'fab_fa-battle-net'=>'fab fa-battle-net', 'fab_fa-behance'=>'fab fa-behance', 'fab_fa-behance-square'=>'fab fa-behance-square', 'fab_fa-bimobject'=>'fab fa-bimobject', 'fab_fa-bitbucket'=>'fab fa-bitbucket', 'fab_fa-bitcoin'=>'fab fa-bitcoin', 'fab_fa-bity'=>'fab fa-bity', 'fab_fa-black-tie'=>'fab fa-black-tie', 'fab_fa-blackberry'=>'fab fa-blackberry', 'fab_fa-blogger'=>'fab fa-blogger', 'fab_fa-blogger-b'=>'fab fa-blogger-b', 'fab_fa-bluetooth'=>'fab fa-bluetooth', 'fab_fa-bluetooth-b'=>'fab fa-bluetooth-b', 'fab_fa-bootstrap'=>'fab fa-bootstrap', 'fab_fa-btc'=>'fab fa-btc', 'fab_fa-buffer'=>'fab fa-buffer', 'fab_fa-buromobelexperte'=>'fab fa-buromobelexperte', 'fab_fa-buy-n-large'=>'fab fa-buy-n-large', 'fab_fa-buysellads'=>'fab fa-buysellads', 'fab_fa-canadian-maple-leaf'=>'fab fa-canadian-maple-leaf', 'fab_fa-cc-amazon-pay'=>'fab fa-cc-amazon-pay', 'fab_fa-cc-amex'=>'fab fa-cc-amex', 'fab_fa-cc-apple-pay'=>'fab fa-cc-apple-pay', 'fab_fa-cc-diners-club'=>'fab fa-cc-diners-club', 'fab_fa-cc-discover'=>'fab fa-cc-discover', 'fab_fa-cc-jcb'=>'fab fa-cc-jcb', 'fab_fa-cc-mastercard'=>'fab fa-cc-mastercard', 'fab_fa-cc-paypal'=>'fab fa-cc-paypal', 'fab_fa-cc-stripe'=>'fab fa-cc-stripe', 'fab_fa-cc-visa'=>'fab fa-cc-visa', 'fab_fa-centercode'=>'fab fa-centercode', 'fab_fa-centos'=>'fab fa-centos', 'fab_fa-chrome'=>'fab fa-chrome', 'fab_fa-chromecast'=>'fab fa-chromecast', 'fab_fa-cloudscale'=>'fab fa-cloudscale', 'fab_fa-cloudsmith'=>'fab fa-cloudsmith', 'fab_fa-cloudversify'=>'fab fa-cloudversify', 'fab_fa-codepen'=>'fab fa-codepen', 'fab_fa-codiepie'=>'fab fa-codiepie', 'fab_fa-confluence'=>'fab fa-confluence', 'fab_fa-connectdevelop'=>'fab fa-connectdevelop', 'fab_fa-contao'=>'fab fa-contao', 'fab_fa-cotton-bureau'=>'fab fa-cotton-bureau', 'fab_fa-cpanel'=>'fab fa-cpanel', 'fab_fa-creative-commons'=>'fab fa-creative-commons', 'fab_fa-creative-commons-by'=>'fab fa-creative-commons-by', 'fab_fa-creative-commons-nc'=>'fab fa-creative-commons-nc', 'fab_fa-creative-commons-nc-eu'=>'fab fa-creative-commons-nc-eu', 'fab_fa-creative-commons-nc-jp'=>'fab fa-creative-commons-nc-jp', 'fab_fa-creative-commons-nd'=>'fab fa-creative-commons-nd', 'fab_fa-creative-commons-pd'=>'fab fa-creative-commons-pd', 'fab_fa-creative-commons-pd-alt'=>'fab fa-creative-commons-pd-alt', 'fab_fa-creative-commons-remix'=>'fab fa-creative-commons-remix', 'fab_fa-creative-commons-sa'=>'fab fa-creative-commons-sa', 'fab_fa-creative-commons-sampling'=>'fab fa-creative-commons-sampling', 'fab_fa-creative-commons-sampling-plus'=>'fab fa-creative-commons-sampling-plus', 'fab_fa-creative-commons-share'=>'fab fa-creative-commons-share', 'fab_fa-creative-commons-zero'=>'fab fa-creative-commons-zero', 'fab_fa-critical-role'=>'fab fa-critical-role', 'fab_fa-css3'=>'fab fa-css3', 'fab_fa-css3-alt'=>'fab fa-css3-alt', 'fab_fa-cuttlefish'=>'fab fa-cuttlefish', 'fab_fa-d-and-d'=>'fab fa-d-and-d', 'fab_fa-d-and-d-beyond'=>'fab fa-d-and-d-beyond', 'fab_fa-dailymotion'=>'fab fa-dailymotion', 'fab_fa-dashcube'=>'fab fa-dashcube', 'fab_fa-delicious'=>'fab fa-delicious', 'fab_fa-deploydog'=>'fab fa-deploydog', 'fab_fa-deskpro'=>'fab fa-deskpro', 'fab_fa-dev'=>'fab fa-dev', 'fab_fa-deviantart'=>'fab fa-deviantart', 'fab_fa-dhl'=>'fab fa-dhl', 'fab_fa-diaspora'=>'fab fa-diaspora', 'fab_fa-digg'=>'fab fa-digg', 'fab_fa-digital-ocean'=>'fab fa-digital-ocean', 'fab_fa-discord'=>'fab fa-discord', 'fab_fa-discourse'=>'fab fa-discourse', 'fab_fa-dochub'=>'fab fa-dochub', 'fab_fa-docker'=>'fab fa-docker', 'fab_fa-draft2digital'=>'fab fa-draft2digital', 'fab_fa-dribbble'=>'fab fa-dribbble', 'fab_fa-dribbble-square'=>'fab fa-dribbble-square', 'fab_fa-dropbox'=>'fab fa-dropbox', 'fab_fa-drupal'=>'fab fa-drupal', 'fab_fa-dyalog'=>'fab fa-dyalog', 'fab_fa-earlybirds'=>'fab fa-earlybirds', 'fab_fa-ebay'=>'fab fa-ebay', 'fab_fa-edge'=>'fab fa-edge', 'fab_fa-elementor'=>'fab fa-elementor', 'fab_fa-ello'=>'fab fa-ello', 'fab_fa-ember'=>'fab fa-ember', 'fab_fa-empire'=>'fab fa-empire', 'fab_fa-envira'=>'fab fa-envira', 'fab_fa-erlang'=>'fab fa-erlang', 'fab_fa-ethereum'=>'fab fa-ethereum', 'fab_fa-etsy'=>'fab fa-etsy', 'fab_fa-evernote'=>'fab fa-evernote', 'fab_fa-expeditedssl'=>'fab fa-expeditedssl', 'fab_fa-facebook'=>'fab fa-facebook', 'fab_fa-facebook-f'=>'fab fa-facebook-f', 'fab_fa-facebook-messenger'=>'fab fa-facebook-messenger', 'fab_fa-facebook-square'=>'fab fa-facebook-square', 'fab_fa-fantasy-flight-games'=>'fab fa-fantasy-flight-games', 'fab_fa-fedex'=>'fab fa-fedex', 'fab_fa-fedora'=>'fab fa-fedora', 'fab_fa-figma'=>'fab fa-figma', 'fab_fa-firefox'=>'fab fa-firefox', 'fab_fa-firefox-browser'=>'fab fa-firefox-browser', 'fab_fa-first-order'=>'fab fa-first-order', 'fab_fa-first-order-alt'=>'fab fa-first-order-alt', 'fab_fa-firstdraft'=>'fab fa-firstdraft', 'fab_fa-flickr'=>'fab fa-flickr', 'fab_fa-flipboard'=>'fab fa-flipboard', 'fab_fa-fly'=>'fab fa-fly', 'fab_fa-font-awesome'=>'fab fa-font-awesome', 'fab_fa-font-awesome-alt'=>'fab fa-font-awesome-alt', 'fab_fa-font-awesome-flag'=>'fab fa-font-awesome-flag', 'fab_fa-fonticons'=>'fab fa-fonticons', 'fab_fa-fonticons-fi'=>'fab fa-fonticons-fi', 'fab_fa-fort-awesome'=>'fab fa-fort-awesome', 'fab_fa-fort-awesome-alt'=>'fab fa-fort-awesome-alt', 'fab_fa-forumbee'=>'fab fa-forumbee', 'fab_fa-foursquare'=>'fab fa-foursquare', 'fab_fa-free-code-camp'=>'fab fa-free-code-camp', 'fab_fa-freebsd'=>'fab fa-freebsd', 'fab_fa-fulcrum'=>'fab fa-fulcrum', 'fab_fa-galactic-republic'=>'fab fa-galactic-republic', 'fab_fa-galactic-senate'=>'fab fa-galactic-senate', 'fab_fa-get-pocket'=>'fab fa-get-pocket', 'fab_fa-gg'=>'fab fa-gg', 'fab_fa-gg-circle'=>'fab fa-gg-circle', 'fab_fa-git'=>'fab fa-git', 'fab_fa-git-alt'=>'fab fa-git-alt', 'fab_fa-git-square'=>'fab fa-git-square', 'fab_fa-github'=>'fab fa-github', 'fab_fa-github-alt'=>'fab fa-github-alt', 'fab_fa-github-square'=>'fab fa-github-square', 'fab_fa-gitkraken'=>'fab fa-gitkraken', 'fab_fa-gitlab'=>'fab fa-gitlab', 'fab_fa-gitter'=>'fab fa-gitter', 'fab_fa-glide'=>'fab fa-glide', 'fab_fa-glide-g'=>'fab fa-glide-g', 'fab_fa-gofore'=>'fab fa-gofore', 'fab_fa-goodreads'=>'fab fa-goodreads', 'fab_fa-goodreads-g'=>'fab fa-goodreads-g', 'fab_fa-google'=>'fab fa-google', 'fab_fa-google-drive'=>'fab fa-google-drive', 'fab_fa-google-play'=>'fab fa-google-play', 'fab_fa-google-plus'=>'fab fa-google-plus', 'fab_fa-google-plus-g'=>'fab fa-google-plus-g', 'fab_fa-google-plus-square'=>'fab fa-google-plus-square', 'fab_fa-google-wallet'=>'fab fa-google-wallet', 'fab_fa-gratipay'=>'fab fa-gratipay', 'fab_fa-grav'=>'fab fa-grav', 'fab_fa-gripfire'=>'fab fa-gripfire', 'fab_fa-grunt'=>'fab fa-grunt', 'fab_fa-gulp'=>'fab fa-gulp', 'fab_fa-hacker-news'=>'fab fa-hacker-news', 'fab_fa-hacker-news-square'=>'fab fa-hacker-news-square', 'fab_fa-hackerrank'=>'fab fa-hackerrank', 'fab_fa-hips'=>'fab fa-hips', 'fab_fa-hire-a-helper'=>'fab fa-hire-a-helper', 'fab_fa-hooli'=>'fab fa-hooli', 'fab_fa-hornbill'=>'fab fa-hornbill', 'fab_fa-hotjar'=>'fab fa-hotjar', 'fab_fa-houzz'=>'fab fa-houzz', 'fab_fa-html5'=>'fab fa-html5', 'fab_fa-hubspot'=>'fab fa-hubspot', 'fab_fa-ideal'=>'fab fa-ideal', 'fab_fa-imdb'=>'fab fa-imdb', 'fab_fa-instagram'=>'fab fa-instagram', 'fab_fa-instagram-square'=>'fab fa-instagram-square', 'fab_fa-intercom'=>'fab fa-intercom', 'fab_fa-internet-explorer'=>'fab fa-internet-explorer', 'fab_fa-invision'=>'fab fa-invision', 'fab_fa-ioxhost'=>'fab fa-ioxhost', 'fab_fa-itch-io'=>'fab fa-itch-io', 'fab_fa-itunes'=>'fab fa-itunes', 'fab_fa-itunes-note'=>'fab fa-itunes-note', 'fab_fa-java'=>'fab fa-java', 'fab_fa-jedi-order'=>'fab fa-jedi-order', 'fab_fa-jenkins'=>'fab fa-jenkins', 'fab_fa-jira'=>'fab fa-jira', 'fab_fa-joget'=>'fab fa-joget', 'fab_fa-joomla'=>'fab fa-joomla', 'fab_fa-js'=>'fab fa-js', 'fab_fa-js-square'=>'fab fa-js-square', 'fab_fa-jsfiddle'=>'fab fa-jsfiddle', 'fab_fa-kaggle'=>'fab fa-kaggle', 'fab_fa-keybase'=>'fab fa-keybase', 'fab_fa-keycdn'=>'fab fa-keycdn', 'fab_fa-kickstarter'=>'fab fa-kickstarter', 'fab_fa-kickstarter-k'=>'fab fa-kickstarter-k', 'fab_fa-korvue'=>'fab fa-korvue', 'fab_fa-laravel'=>'fab fa-laravel', 'fab_fa-lastfm'=>'fab fa-lastfm', 'fab_fa-lastfm-square'=>'fab fa-lastfm-square', 'fab_fa-leanpub'=>'fab fa-leanpub', 'fab_fa-less'=>'fab fa-less', 'fab_fa-line'=>'fab fa-line', 'fab_fa-linkedin'=>'fab fa-linkedin', 'fab_fa-linkedin-in'=>'fab fa-linkedin-in', 'fab_fa-linode'=>'fab fa-linode', 'fab_fa-linux'=>'fab fa-linux', 'fab_fa-lyft'=>'fab fa-lyft', 'fab_fa-magento'=>'fab fa-magento', 'fab_fa-mailchimp'=>'fab fa-mailchimp', 'fab_fa-mandalorian'=>'fab fa-mandalorian', 'fab_fa-markdown'=>'fab fa-markdown', 'fab_fa-mastodon'=>'fab fa-mastodon', 'fab_fa-maxcdn'=>'fab fa-maxcdn', 'fab_fa-mdb'=>'fab fa-mdb', 'fab_fa-medapps'=>'fab fa-medapps', 'fab_fa-medium'=>'fab fa-medium', 'fab_fa-medium-m'=>'fab fa-medium-m', 'fab_fa-medrt'=>'fab fa-medrt', 'fab_fa-meetup'=>'fab fa-meetup', 'fab_fa-megaport'=>'fab fa-megaport', 'fab_fa-mendeley'=>'fab fa-mendeley', 'fab_fa-microblog'=>'fab fa-microblog', 'fab_fa-microsoft'=>'fab fa-microsoft', 'fab_fa-mix'=>'fab fa-mix', 'fab_fa-mixcloud'=>'fab fa-mixcloud', 'fab_fa-mixer'=>'fab fa-mixer', 'fab_fa-mizuni'=>'fab fa-mizuni', 'fab_fa-modx'=>'fab fa-modx', 'fab_fa-monero'=>'fab fa-monero', 'fab_fa-napster'=>'fab fa-napster', 'fab_fa-neos'=>'fab fa-neos', 'fab_fa-nimblr'=>'fab fa-nimblr', 'fab_fa-node'=>'fab fa-node', 'fab_fa-node-js'=>'fab fa-node-js', 'fab_fa-npm'=>'fab fa-npm', 'fab_fa-ns8'=>'fab fa-ns8', 'fab_fa-nutritionix'=>'fab fa-nutritionix', 'fab_fa-odnoklassniki'=>'fab fa-odnoklassniki', 'fab_fa-odnoklassniki-square'=>'fab fa-odnoklassniki-square', 'fab_fa-old-republic'=>'fab fa-old-republic', 'fab_fa-opencart'=>'fab fa-opencart', 'fab_fa-openid'=>'fab fa-openid', 'fab_fa-opera'=>'fab fa-opera', 'fab_fa-optin-monster'=>'fab fa-optin-monster', 'fab_fa-orcid'=>'fab fa-orcid', 'fab_fa-osi'=>'fab fa-osi', 'fab_fa-page4'=>'fab fa-page4', 'fab_fa-pagelines'=>'fab fa-pagelines', 'fab_fa-palfed'=>'fab fa-palfed', 'fab_fa-patreon'=>'fab fa-patreon', 'fab_fa-paypal'=>'fab fa-paypal', 'fab_fa-penny-arcade'=>'fab fa-penny-arcade', 'fab_fa-periscope'=>'fab fa-periscope', 'fab_fa-phabricator'=>'fab fa-phabricator', 'fab_fa-phoenix-framework'=>'fab fa-phoenix-framework', 'fab_fa-phoenix-squadron'=>'fab fa-phoenix-squadron', 'fab_fa-php'=>'fab fa-php', 'fab_fa-pied-piper'=>'fab fa-pied-piper', 'fab_fa-pied-piper-alt'=>'fab fa-pied-piper-alt', 'fab_fa-pied-piper-hat'=>'fab fa-pied-piper-hat', 'fab_fa-pied-piper-pp'=>'fab fa-pied-piper-pp', 'fab_fa-pied-piper-square'=>'fab fa-pied-piper-square', 'fab_fa-pinterest'=>'fab fa-pinterest', 'fab_fa-pinterest-p'=>'fab fa-pinterest-p', 'fab_fa-pinterest-square'=>'fab fa-pinterest-square', 'fab_fa-playstation'=>'fab fa-playstation', 'fab_fa-product-hunt'=>'fab fa-product-hunt', 'fab_fa-pushed'=>'fab fa-pushed', 'fab_fa-python'=>'fab fa-python', 'fab_fa-qq'=>'fab fa-qq', 'fab_fa-quinscape'=>'fab fa-quinscape', 'fab_fa-quora'=>'fab fa-quora', 'fab_fa-r-project'=>'fab fa-r-project', 'fab_fa-raspberry-pi'=>'fab fa-raspberry-pi', 'fab_fa-ravelry'=>'fab fa-ravelry', 'fab_fa-react'=>'fab fa-react', 'fab_fa-reacteurope'=>'fab fa-reacteurope', 'fab_fa-readme'=>'fab fa-readme', 'fab_fa-rebel'=>'fab fa-rebel', 'fab_fa-red-river'=>'fab fa-red-river', 'fab_fa-reddit'=>'fab fa-reddit', 'fab_fa-reddit-alien'=>'fab fa-reddit-alien', 'fab_fa-reddit-square'=>'fab fa-reddit-square', 'fab_fa-redhat'=>'fab fa-redhat', 'fab_fa-renren'=>'fab fa-renren', 'fab_fa-replyd'=>'fab fa-replyd', 'fab_fa-researchgate'=>'fab fa-researchgate', 'fab_fa-resolving'=>'fab fa-resolving', 'fab_fa-rev'=>'fab fa-rev', 'fab_fa-rocketchat'=>'fab fa-rocketchat', 'fab_fa-rockrms'=>'fab fa-rockrms', 'fab_fa-safari'=>'fab fa-safari', 'fab_fa-salesforce'=>'fab fa-salesforce', 'fab_fa-sass'=>'fab fa-sass', 'fab_fa-schlix'=>'fab fa-schlix', 'fab_fa-scribd'=>'fab fa-scribd', 'fab_fa-searchengin'=>'fab fa-searchengin', 'fab_fa-sellcast'=>'fab fa-sellcast', 'fab_fa-sellsy'=>'fab fa-sellsy', 'fab_fa-servicestack'=>'fab fa-servicestack', 'fab_fa-shirtsinbulk'=>'fab fa-shirtsinbulk', 'fab_fa-shopify'=>'fab fa-shopify', 'fab_fa-shopware'=>'fab fa-shopware', 'fab_fa-simplybuilt'=>'fab fa-simplybuilt', 'fab_fa-sistrix'=>'fab fa-sistrix', 'fab_fa-sith'=>'fab fa-sith', 'fab_fa-sketch'=>'fab fa-sketch', 'fab_fa-skyatlas'=>'fab fa-skyatlas', 'fab_fa-skype'=>'fab fa-skype', 'fab_fa-slack'=>'fab fa-slack', 'fab_fa-slack-hash'=>'fab fa-slack-hash', 'fab_fa-slideshare'=>'fab fa-slideshare', 'fab_fa-snapchat'=>'fab fa-snapchat', 'fab_fa-snapchat-ghost'=>'fab fa-snapchat-ghost', 'fab_fa-snapchat-square'=>'fab fa-snapchat-square', 'fab_fa-soundcloud'=>'fab fa-soundcloud', 'fab_fa-sourcetree'=>'fab fa-sourcetree', 'fab_fa-speakap'=>'fab fa-speakap', 'fab_fa-speaker-deck'=>'fab fa-speaker-deck', 'fab_fa-spotify'=>'fab fa-spotify', 'fab_fa-squarespace'=>'fab fa-squarespace', 'fab_fa-stack-exchange'=>'fab fa-stack-exchange', 'fab_fa-stack-overflow'=>'fab fa-stack-overflow', 'fab_fa-stackpath'=>'fab fa-stackpath', 'fab_fa-staylinked'=>'fab fa-staylinked', 'fab_fa-steam'=>'fab fa-steam', 'fab_fa-steam-square'=>'fab fa-steam-square', 'fab_fa-steam-symbol'=>'fab fa-steam-symbol', 'fab_fa-sticker-mule'=>'fab fa-sticker-mule', 'fab_fa-strava'=>'fab fa-strava', 'fab_fa-stripe'=>'fab fa-stripe', 'fab_fa-stripe-s'=>'fab fa-stripe-s', 'fab_fa-studiovinari'=>'fab fa-studiovinari', 'fab_fa-stumbleupon'=>'fab fa-stumbleupon', 'fab_fa-stumbleupon-circle'=>'fab fa-stumbleupon-circle', 'fab_fa-superpowers'=>'fab fa-superpowers', 'fab_fa-supple'=>'fab fa-supple', 'fab_fa-suse'=>'fab fa-suse', 'fab_fa-swift'=>'fab fa-swift', 'fab_fa-symfony'=>'fab fa-symfony', 'fab_fa-teamspeak'=>'fab fa-teamspeak', 'fab_fa-telegram'=>'fab fa-telegram', 'fab_fa-telegram-plane'=>'fab fa-telegram-plane', 'fab_fa-tencent-weibo'=>'fab fa-tencent-weibo', 'fab_fa-the-red-yeti'=>'fab fa-the-red-yeti', 'fab_fa-themeco'=>'fab fa-themeco', 'fab_fa-themeisle'=>'fab fa-themeisle', 'fab_fa-think-peaks'=>'fab fa-think-peaks', 'fab_fa-trade-federation'=>'fab fa-trade-federation', 'fab_fa-trello'=>'fab fa-trello', 'fab_fa-tripadvisor'=>'fab fa-tripadvisor', 'fab_fa-tumblr'=>'fab fa-tumblr', 'fab_fa-tumblr-square'=>'fab fa-tumblr-square', 'fab_fa-twitch'=>'fab fa-twitch', 'fab_fa-twitter'=>'fab fa-twitter', 'fab_fa-twitter-square'=>'fab fa-twitter-square', 'fab_fa-typo3'=>'fab fa-typo3', 'fab_fa-uber'=>'fab fa-uber', 'fab_fa-ubuntu'=>'fab fa-ubuntu', 'fab_fa-uikit'=>'fab fa-uikit', 'fab_fa-umbraco'=>'fab fa-umbraco', 'fab_fa-uniregistry'=>'fab fa-uniregistry', 'fab_fa-unity'=>'fab fa-unity', 'fab_fa-untappd'=>'fab fa-untappd', 'fab_fa-ups'=>'fab fa-ups', 'fab_fa-usb'=>'fab fa-usb', 'fab_fa-usps'=>'fab fa-usps', 'fab_fa-ussunnah'=>'fab fa-ussunnah', 'fab_fa-vaadin'=>'fab fa-vaadin', 'fab_fa-viacoin'=>'fab fa-viacoin', 'fab_fa-viadeo'=>'fab fa-viadeo', 'fab_fa-viadeo-square'=>'fab fa-viadeo-square', 'fab_fa-viber'=>'fab fa-viber', 'fab_fa-vimeo'=>'fab fa-vimeo', 'fab_fa-vimeo-square'=>'fab fa-vimeo-square', 'fab_fa-vimeo-v'=>'fab fa-vimeo-v', 'fab_fa-vine'=>'fab fa-vine', 'fab_fa-vk'=>'fab fa-vk', 'fab_fa-vnv'=>'fab fa-vnv', 'fab_fa-vuejs'=>'fab fa-vuejs', 'fab_fa-waze'=>'fab fa-waze', 'fab_fa-weebly'=>'fab fa-weebly', 'fab_fa-weibo'=>'fab fa-weibo', 'fab_fa-weixin'=>'fab fa-weixin', 'fab_fa-whatsapp'=>'fab fa-whatsapp', 'fab_fa-whatsapp-square'=>'fab fa-whatsapp-square', 'fab_fa-whmcs'=>'fab fa-whmcs', 'fab_fa-wikipedia-w'=>'fab fa-wikipedia-w', 'fab_fa-windows'=>'fab fa-windows', 'fab_fa-wix'=>'fab fa-wix', 'fab_fa-wizards-of-the-coast'=>'fab fa-wizards-of-the-coast', 'fab_fa-wolf-pack-battalion'=>'fab fa-wolf-pack-battalion', 'fab_fa-wordpress'=>'fab fa-wordpress', 'fab_fa-wordpress-simple'=>'fab fa-wordpress-simple', 'fab_fa-wpbeginner'=>'fab fa-wpbeginner', 'fab_fa-wpexplorer'=>'fab fa-wpexplorer', 'fab_fa-wpforms'=>'fab fa-wpforms', 'fab_fa-wpressr'=>'fab fa-wpressr', 'fab_fa-xbox'=>'fab fa-xbox', 'fab_fa-xing'=>'fab fa-xing', 'fab_fa-xing-square'=>'fab fa-xing-square', 'fab_fa-y-combinator'=>'fab fa-y-combinator', 'fab_fa-yahoo'=>'fab fa-yahoo', 'fab_fa-yammer'=>'fab fa-yammer', 'fab_fa-yandex'=>'fab fa-yandex', 'fab_fa-yandex-international'=>'fab fa-yandex-international', 'fab_fa-yarn'=>'fab fa-yarn', 'fab_fa-yelp'=>'fab fa-yelp', 'fab_fa-yoast'=>'fab fa-yoast', 'fab_fa-youtube'=>'fab fa-youtube', 'fab_fa-youtube-square'=>'fab fa-youtube-square', 'fab_fa-zhihu'=>'fab fa-zhihu');
	return $icons;
}
function getSavedSections(){
	$sections = App\Sections::All();
	ob_start();
	foreach($sections as $key=>$section){
		?>
		<div data-shortcode='<?php echo $section->shortcode ?>'><img src="http://web.duktix.com/builder/contentbox/images/customsection.jpg"><span><?php echo $section->title ?></span></div>
		<?php
	}
	return ob_get_clean();
}
function fixSerialize($string){
    return $repaired = preg_replace_callback(
        '/s:\d+:"(.*?)";/s',
        //  ^^^- matched/consumed but not captured because not used in replacement
        function ($m) {
            return "s:" . strlen($m[1]) . ":\"{$m[1]}\";";
        },
        $string
    );
}
?>
