<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Categories extends Model
{
  use SoftDeletes;
protected $fillable = [
      'title', 'slug', 'description', 'category_image'
  ];
protected $dates = ['deleted_at'];
}
