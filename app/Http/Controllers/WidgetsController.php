<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Widget;
use App\Upload;
use App\Categories;
use App\Pages;
use App\Posts;
use App\theme;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class WidgetsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		$this->middleware('checkhost');
    }
	public function Index()
    {
		$currenttheme = theme::getCurrentTheme();
		$themesettings = getThemeConfig($currenttheme['name'],'all');

		$widgets = listWidgets();
        return view('admin.widgets', ['savedwidgets' => Widget::orderBy('id', 'desc')->get(),'widgets'=>$widgets, 'categories' => Categories::All(), 'themesettings' => $themesettings, 'pages' => Pages::All()]);
    }
	public function save(Request $request){
		if($request->id){
			$widget = Widget::find($request->id);
			$rules = [];
		}else{
			$widget = new Widget;
			$widget->setting = '';
			$widget->shortcode = '';
			$rules = [
					'title' => 'required',
					'type' => 'required'
			];
			$widget->title = $request->title.' - '.$request->type;
			$widget->type = $request->type;
		}

		$messages = [
			'title.required' => 'Please enter a title',
			'type.required' => 'Please select widget type'
		];
		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()){
			return json_encode(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			if($request->id){
				$inputs = array();
				foreach($request->all() as $key=>$val){
					$inputs[$key] = $val;
				}

				$html = $request->newsletter_html;
				$activeslide = $request->activeslide;
				$widget->setting =serialize($inputs);
				$widget->save();
				if($request->widgettype=='form_widget' && $request->notification_type=='hubspot'){
					if($request->guid==''){
						$hubspotdata = generateHubspot($request->id, 'add', $request->hubspotkey);
						$inputs['guid'] = $hubspotdata->guid;
						$inputs['portalid'] = $hubspotdata->portalId;
						$widget->setting =serialize($inputs);
						$widget->save();
					}else if($request->guid!='' && $request->notification_type=='hubspot'){
						generateHubspot($request->id, 'edit', $request->hubspotkey);
					}
				}
				if($request->widgettype=='form_widget'){
					generateNewsletter('form_'.$request->id,$request->newsletter_html);
				}
				return Redirect::back()->with('success', 'Widget Updated.')->with('activeslide', $activeslide);
			}else{
				$widget->save();
				$shortcode = Widget::find($widget->id);
				$shortcode->setting ='';
				if($shortcode->type=='form_widget'){
					$settings = unserialize(fixSerialize($shortcode->setting));
					generateNewsletter('form_'.$widget->id,$settings['newsletter_html']);
				}
				$shortcode->shortcode = '[widget type="'.$shortcode->type.'" id="'.$shortcode->id.'"]';
				$shortcode->save();
				return 'success';
			}

		}
	}
	public function edit($id)
    {
		$currenttheme = theme::getCurrentTheme();
		$themesettings = getThemeConfig($currenttheme['name'],'all');

		$widget = Widget::find($id);
		if($widget){
			$uploads = Upload::simplePaginate(15);
			if(!empty($widget->setting)){
				$settings = unserialize(fixSerialize($widget->setting));
				return view('admin.widgets.'.$widget->type,['widget' => $widget, 'settings' => $settings, 'uploads' => $uploads, 'categories' => Categories::All(), 'themesettings' => $themesettings, 'pages' => Pages::All()]);
			}else{
				return view('admin.widgets.'.$widget->type,['widget' => $widget, 'uploads' => $uploads, 'categories' => Categories::All(), 'themesettings' => $themesettings, 'pages' => Pages::All()]);
			}
		}else{
			return abort(404);
		}
	}
	public function preview(Request $request)
    {
		$widget = Widget::find($request->id);
		if($request){
			$settings = array();
			foreach($request->all() as $key=>$request){
				$settings[$key] = $request;
			}
      $settings['uniqueid'] = uniqid();
			return view('front.widgets.'.$widget->type, ['widget'=>$widget, 'settings' => $settings, 'pages' => Pages::All(), 'posts' => Posts::All()]);
		}
	}
	public function delete($id)
    {
		$widget = Widget::find($id);

		//Hubspot form delete
		if($widget->type=='form_widget' && !empty($widget->setting)){
			$settings = unserialize(fixSerialize($widget->setting));
			$guid = !empty($settings['guid'])?$settings['guid']:'';
			$ch = curl_init();
			curl_setopt_array($ch, [
				CURLOPT_URL => "https://api.hubapi.com/forms/v2/forms/".$guid."?hapikey=".\Config::get('app.hubspotkey'),
				CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => '',
				CURLOPT_CUSTOMREQUEST => "DELETE",
			]);
			$response = curl_exec($ch);
			curl_close($ch);
		}
		//Hubspot form delete
		$widget->delete();
		return Redirect::back()->with('success', 'Widget Deleted.');
	}
	public function addlayer(Request $request){
		$slide = $request->slide; // ID OF SLIDER
		$layer = $request->layer;
		$timeline = $request->timeline;
		$type = $request->type;

		$prelayer_container['text'] = '<div class="tp-mask-wrap" style="position:absolute;left:0;top:0;"><div class="tp-caption NotGeneric-Title tp-resizeme" data-id="slide-'.$slide.'-layer-'.$layer.'" style="z-index: 5;visibility:inherit; white-space: nowrap; font-size: 30px; line-height: 30px;">Caption Text</div></div>';

		$prelayer_container['button'] = '<div class="tp-mask-wrap" style="position:absolute;left:0;top:0;"><div class="tp-caption tp-resizeme" data-id="slide-'.$slide.'-layer-'.$layer.'" style="z-index: 9;visibility:inherit;white-space: nowrap; font-weight: 800;padding:10px 20px;text-align:center;background-color:rgba(41, 46, 49, 1.00);border-color:rgba(255, 255, 255, 0);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Button</div></div>';

		$prelayer_elements['text'] = '<li data-rel="slide-'.$slide.'-layer-'.$layer.'">';
		$prelayer_elements['text'].='<table><tr><td width="100">Text</td>';
		$prelayer_elements['text'].='<td width="400">Caption Text</td>';
		$prelayer_elements['text'].='<td><input type="text" name="slide_'.$slide.'[hidden_layer_start][]" value="'.$timeline.'"></td></tr></table>';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_id][]" value="slide-'.$slide.'-layer-'.$layer.'">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_type][]" value="text">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_rel][]" value="">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_title][]" value="">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_class][]" value="">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_halign][]" value="left">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_hoffset][]" value="0">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_valign][]" value="top">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_voffset][]" value="0">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_fontsize][]" value="30px">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_lineheight][]" value="30px">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_color][]" value="#ffffff">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_fontweight][]" value="800">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_fontfamily][]" value="">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_bgcolor][]" value="#00000000">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_pleft][]" value="0">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_pright][]" value="0">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_ptop][]" value="10px">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_pbottom][]" value="10px">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_text][]" value="Caption Text">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_animation][]" value="noanimation">';
    $prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_animation_speed_in][]" value="300">';
    $prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_animation_speed_out][]" value="300">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_target][]" value="">';
		$prelayer_elements['text'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_link][]" value="">';
		$prelayer_elements['text'].='</li>';

		$prelayer_elements['button'] = '<li data-rel="slide-'.$slide.'-layer-'.$layer.'">';
		$prelayer_elements['button'].='<table><tr><td width="100">Button</td>';
		$prelayer_elements['button'].='<td width="400">Button</td>';
		$prelayer_elements['button'].='<td><input type="text" name="slide_'.$slide.'[hidden_layer_start][]" value="'.$timeline.'"></td></tr></table>';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_id][]" value="slide-'.$slide.'-layer-'.$layer.'">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_type][]" value="button">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_rel][]" value="">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_title][]" value="">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_class][]" value="">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_halign][]" value="left">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_hoffset][]" value="0">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_valign][]" value="top">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_voffset][]" value="0">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_fontsize][]" value="30px">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_lineheight][]" value="30px">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_color][]" value="rgba(255,255,255,1.00)">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_fontweight][]" value="800">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_fontfamily][]" value="">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_bgcolor][]" value="rgba(41, 46, 49, 1.00)">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_pleft][]" value="20px">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_pright][]" value="20px">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_ptop][]" value="10px">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_pbottom][]" value="10px">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_text][]" value="Button">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_animation][]" value="noanimation">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_target][]" value="_self">';
		$prelayer_elements['button'].='<input type="hidden" name="slide_'.$slide.'[hidden_layer_link][]" value="http://">';
		$prelayer_elements['button'].='</li>';

		$html = array();
		$html['container'] = $prelayer_container[$type];
		$html['element'] = $prelayer_elements[$type];
		return json_encode($html);
	}
  public function clone($id)
    {
		$currenttheme = theme::getCurrentTheme();
		$widget = Widget::find($id);
    $duplicate = $widget->replicate();
    $duplicate->title  = $widget->title . " - Copy";
    $duplicate->type  = $widget->type;
    $duplicate->setting = $widget->setting;
    $duplicate->save();

    if($duplicate->type=='form_widget'){
      $settings = unserialize($duplicate->setting);
      generateNewsletter('form_'.$duplicate->id,$settings['newsletter_html']);
    }
    $duplicate->shortcode = '[widget type="'.$duplicate->type.'" id="'.$duplicate->id.'"]';
    $duplicate->save();
		return Redirect::back()->with('success', 'Post Cloned.');
	}
}
