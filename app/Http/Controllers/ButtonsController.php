<?php

namespace App\Http\Controllers;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;
use App\Buttons;
use App\theme;
use App\Pages;
use App\Settings;
use App\Menu;
use App\Upload;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ButtonsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkhost');
    }
    public function index()
    {
		generateCustomCSS();
      $currenttheme = theme::getCurrentTheme();
      $themesettings = getThemeConfig($currenttheme['name'],'all');
      return view('admin.buttons', ['savedbuttons' => Buttons::orderBy('id', 'desc')->get(), 'themesettings' => $themesettings, 'pages' => Pages::All()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$inputs = array();
		foreach($request->all() as $key=>$val){
			$inputs[$key] = $val;
		}
      if(!empty($request->id)){
        $button = Buttons::find($request->id);
        $rules = [
      			'button_class' => 'required',
      		];
      		$messages = [
          			'button_class.required' => 'Something went wrong',
          		];
          	$validator = Validator::make($request->all(), $rules, $messages);
      		if ($validator->fails()){
          			return Redirect::route('buttons.edit', [$request->id])->with('errors', $validator->getMessageBag()->toArray());
          	}else{
      			     $button->setting =serialize($inputs);
                 $button->save();
                 return Redirect::route('buttons.edit', [$request->id])->with('success', 'Button Updated.');
      		}
          generateCustomCSS();
      }else{
        $button = new Buttons;
        $button->name = $request->button_class;
        $rules = [
            'button_class' => 'required',
        ];
        $messages = [
    			'button_class.required' => 'Something went wrong',
    		];
    		$validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()){
          return Redirect::route('buttons.edit', [$button->id])->with('errors', $validator->getMessageBag()->toArray());
    		}else{
    		   $button->setting =serialize($inputs);
    		   $button->shortcode ='';
    		  $button->save();
          $button->shortcode = '[button id="'.$button->id.'" link="" title=""]';
          $button->save();
          return Redirect::route('buttons.edit', [$button->id])->with('success', 'Button Added.');
        }
        generateCustomCSS();
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Buttons  $buttons
     * @return \Illuminate\Http\Response
     */
    public function show(Buttons $buttons)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Buttons  $buttons
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      generateCustomCSS();
      $currenttheme = theme::getCurrentTheme();
      $button = Buttons::find($id);
      if($button){
        if(!empty($button->setting)){
          $settings = unserialize($button->setting);
          return view('admin.buttonedit',['button' => $button, 'settings' => $settings]);
        }else{
          return view('admin.buttonedit',['button' => $button]);
        }
      }else{
        return abort(404);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Buttons  $buttons
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buttons $buttons)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Buttons  $buttons
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $button = Buttons::find($id);
        $button->delete();
    		return Redirect::back()->with('success', 'Button Deleted.');
    }
}
