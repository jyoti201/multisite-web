<?php

namespace App\Http\Controllers;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Illuminate\Http\Request;

use App\theme;
use App\Pages;
use App\Settings;
use App\Menu;
use App\Upload;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		$this->middleware('checkhost');
    }
	public function header()
    {
		$currenttheme = theme::getCurrentTheme();
		$settings = getThemeConfig($currenttheme['name'],'all');
		$headersettings = Settings::where('type', "header")->first();
		$uploads = Upload::simplePaginate(15);

		if($headersettings){
			$savedsettings = unserialize($headersettings->setting);
			return view('admin.header', ['settings' => $settings, 'menus' => Menu::All(), 'savedsettings'=> $savedsettings, 'uploads' => $uploads]);
		}else{
			return view('admin.header', ['settings' => $settings,'menus' => Menu::All(), 'savedsettings'=> $settings, 'uploads' => $uploads]);
		}
    }
	public function custom()
    {
		$currenttheme = theme::getCurrentTheme();
		$settings = getThemeConfig($currenttheme['name'],'all');
		$headersettings = Settings::where('type', "custom")->first();
		$uploads = Upload::simplePaginate(15);

		if($headersettings){
			$savedsettings = unserialize($headersettings->setting);
			return view('admin.custom', ['settings' => $settings, 'menus' => Menu::All(), 'savedsettings'=> $savedsettings, 'uploads' => $uploads]);
		}else{
			return view('admin.custom', ['settings' => $settings,'menus' => Menu::All(), 'savedsettings'=> $settings, 'uploads' => $uploads]);
		}
    }
	public function footer()
    {
		$currenttheme = theme::getCurrentTheme();
		$settings = getThemeConfig($currenttheme['name'],'all');
		$footersettings = Settings::where('type', "footer")->first();
		$uploads = Upload::simplePaginate(15);

		if($footersettings){
			$savedsettings = unserialize($footersettings->setting);
			return view('admin.footer', ['settings' => $settings, 'menus' => Menu::All(), 'savedsettings'=> $savedsettings, 'uploads' => $uploads]);
		}else{
			return view('admin.footer', ['settings' => $settings,'menus' => Menu::All(), 'savedsettings'=> $settings, 'uploads' => $uploads]);
		}
    }
	public function footerbuilder()
    {
		$currenttheme = theme::getCurrentTheme();
		$settings = getThemeConfig($currenttheme['name'],'all');
		$footersettings = Settings::where('type', "footer")->first();
		$uploads = Upload::simplePaginate(15);

		if($footersettings){
			$savedsettings = unserialize($footersettings->setting);
			return view('admin._partials.footer-builder', ['settings' => $settings, 'menus' => Menu::All(), 'savedsettings'=> $savedsettings, 'uploads' => $uploads]);
		}else{
			return view('admin._partials.footer-builder', ['settings' => $settings,'menus' => Menu::All(), 'savedsettings'=> $settings, 'uploads' => $uploads]);
		}
    }
	public function styles()
    {
		$currenttheme = theme::getCurrentTheme();
		$settings = getThemeConfig($currenttheme['name'],'all');
		$fontsettings = Settings::where('type', "styles")->first();
    $uploads = Upload::simplePaginate(15);

		if($fontsettings){
			$savedsettings = unserialize($fontsettings->setting);
      generateCustomCSS();
			return view('admin.styles', ['settings' => $settings, 'menus' => Menu::All(), 'savedsettings'=> $savedsettings, 'uploads' => $uploads]);
		}else{
			return view('admin.styles', ['settings' => $settings,'menus' => Menu::All(), 'savedsettings'=> $settings, 'uploads' => $uploads]);
		}
    }
	public function colors()
    {
		$currenttheme = theme::getCurrentTheme();
		$colors = getThemeConfig($currenttheme['name'],'colors');
		$colorsettings = Settings::where('type', "colors")->first();

		if($colorsettings){
			$savedsettings = unserialize($colorsettings->setting);
			return view('admin.colors', ['settings' => $colors, 'savedsettings'=> $savedsettings]);
		}else{
			return view('admin.colors', ['settings' => $colors]);
		}
    }
	public function general()
    {
		$generalsettings = Settings::where('type', "general")->first();
		if($generalsettings){
			$savedsettings = unserialize($generalsettings->setting);
			return view('admin.general', ['pages' => Pages::All(), 'savedsettings'=> $savedsettings]);
		}else{
			return view('admin.general', ['pages' => Pages::All()]);
		}
    }
	public function saveSettings(Request $requests)
    {
		$predefined = array('logo', 'favicon', 'header_bg_color', 'header_menu_items_bg_color', 'header_menu_items_bg_color_hover', 'header_menu_items_color', 'header_menu_items_color_hover', 'custom_code_header_css', 'custom_code_header_js', 'custom_code_header_others', 'custom_code_body');
		$inputs = array();

		$currenttheme = theme::getCurrentTheme();
		$settings = getThemeConfig($currenttheme['name'],'all');
		$headersettings = Settings::where('type', "header")->first();
		if($headersettings)
			$savedsettings = unserialize($headersettings->setting);

  		if($requests->settingType=='header'){
  			$getfileinputs = array('logo', 'favicon');
  			foreach($settings['logos'] as $key=>$val){
  				$getfileinputs[] = $key;
  			}
  			foreach($requests->all() as $key=>$request){
  				if($key!="logo"){
  					$fileinput = substr($key, -4);
  					if($fileinput != "logo"){
  					$inputs[$key] = $request;
  					}
  				}
  			}
  			foreach($getfileinputs as $file){
  				/*
  				if($requests->hasFile($file)){
  					 $validator = Validator::make($requests->all(), [
  						$file => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
  					]);
  					if ($validator->fails()) {
  						dd($validator);
  					}else{
  						$imagefile = substr($requests->file($file)->getClientOriginalName(), 0, strrpos($requests->file($file)->getClientOriginalName(), "."));
  						$path = "/html/uploads/" . date("Y") . '/' . date("m") . "/";
  						$imageName = $imagefile.'_'.rand().'.'.$requests->file($file)->getClientOriginalExtension();
  						$requests->file($file)->move(public_path($path), $imageName);
  						$inputs[$file] = $path . "$imageName";
  					}
  				}elseif(isset($savedsettings[$file]) && $savedsettings[$file]!=''){
  					$inputs[$file] = $savedsettings[$file];
  				}*/
  				if(!empty($requests->$file)){
  				$inputs[$file] = $requests->$file;
  				}else{
  				$inputs[$file] = '';
  				}
  			}
  			$settings = Settings::updateOrCreate(
  				['type' => 'header'],
  				['type' => 'header', 'setting' => serialize($inputs)]
  			);
  			if($settings){
  				//saveallHTML();
  				generateCustomCSS();
  				return Redirect::back()->with('success', 'Settings successfully updated.');
  			}
  		}
  		if($requests->settingType=='custom'){
  			foreach($requests->all() as $key=>$request){
  				$inputs[$key] = $request;
  			}
  			$settings = Settings::updateOrCreate(
  				['type' => 'custom'],
  				['type' => 'custom', 'setting' => serialize($inputs)]
  			);
  			if($settings){
  				//saveallHTML();
  				generateCustomCSS();
  				return Redirect::back()->with('success', 'Settings successfully updated.');
  			}
  		}
  		if($requests->settingType=='footer'){
  			foreach($requests->all() as $key=>$request){
  				$inputs[$key] = $request;
  			}
  			$settings = Settings::updateOrCreate(
  				['type' => 'footer'],
  				['type' => 'footer', 'setting' => serialize($inputs)]
  			);
  			if($settings){
  				//saveallHTML();
  				return Redirect::back()->with('success', 'Settings successfully updated.');
  			}
  		}
  		if($requests->settingType=='styles'){
  			foreach($requests->all() as $key=>$request){
  				$inputs[$key] = $request;
  			}
  			$settings = Settings::updateOrCreate(
  				['type' => 'styles'],
  				['type' => 'styles', 'setting' => serialize($inputs)]
  			);
  			if($settings){
  				//saveallHTML();
  				generateCustomCSS();
  				return Redirect::back()->with('success', 'Settings successfully updated.');
  			}
  		}
  		if($requests->settingType=='colors'){
  			foreach($requests->all() as $key=>$request){
  				$inputs[$key] = $request;
  			}
  			$settings = Settings::updateOrCreate(
  				['type' => 'colors'],
  				['type' => 'colors', 'setting' => serialize($inputs)]
  			);
  			if($settings){
  				//saveallHTML();
  				generateCustomCSS();
  				return Redirect::back()->with('success', 'Settings successfully updated.');
  			}
  		}
		  if($requests->settingType=='general'){
      			foreach($requests->all() as $key=>$request){
      				$inputs[$key] = $request;
      			}
      			deleteOldBlog();
      			$settings = Settings::updateOrCreate(
      				['type' => 'general'],
      				['type' => 'general', 'setting' => serialize($inputs)]
      			);
      			if($settings){
      				if($inputs['blog']!=''){
      					recreateBlog($inputs['blog']);
      				}
      				saveallHTML();
      				return Redirect::back()->with('success', 'Settings successfully updated.');
      			}
		  }
    }
}
