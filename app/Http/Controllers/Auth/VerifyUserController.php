<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\VerifyUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VerifyUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function verifyUser($token)
      {
          $verifyUser = VerifyUser::where('token', $token)->first();
          if(isset($verifyUser) ){
              $user = $verifyUser->user;
              if(!$user->verified) {
                  $verifyUser->user->verified = 1;
                  $verifyUser->user->save();
                  if($verifyUser->user->role=='site-admin'){
                    $verifyUser->user->active = 1;
                    $verifyUser->user->save();
                    $status = "Your e-mail is verified. You can now login.";
                  }else{
                    $status = "Your e-mail is verified. Please wait for admin approval.";
                  }
              }elseif(!$user->active) {
                $status = "Your e-mail is already verified. Please wait for admin approval.";
              }else{
                  $status = "Your e-mail is already verified. You can now login.";
              }
          }else{
              return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
          }

          return redirect('/login')->with('status', $status);
      }
}
