<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Popup;
use App\theme;
use App\Settings;
use App\Upload;
use Image;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;

class PopupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkhost');
    }
    public function Index()
      {
          return view('admin.popups', ['popups' => Popup::All()]);
      }
  	public function trash()
      {
          return view('admin.popups', ['popups' => Popup::onlyTrashed()->get(), 'trash' => true]);
      }
      public function restore($id)
        {
    		$currenttheme = theme::getCurrentTheme();
    		$popup = Popup::onlyTrashed()->where('id', $id)->first();
    		$popup->restore();
    		return Redirect::back()->with('success', 'Popup restored.');
    	}
      public function clone($id)
        {
    		$currenttheme = theme::getCurrentTheme();
    		$popup = Popup::find($id);
        $duplicate = $popup->replicate();
        $duplicate->title  = $popup->title . " - Copy";
        $duplicate->class  = $popup->class . "-".$popup->id;
        $duplicate->save();
    		return Redirect::back()->with('success', 'Popup Cloned.');
    	}
    	public function forceDelete($id)
        {
    		$currenttheme = theme::getCurrentTheme();
    		$popup = Popup::onlyTrashed()->where('id', $id)->first();
    		$popup->forceDelete();
    		return Redirect::back()->with('success', 'Popup permanently deleted.');
    	}
      public function destroy($id)
        {
    		    $popup = Popup::find($id);
            $popup->delete();
    		return Redirect::back()->with('success', 'Popup successfully trashed.');
    	}
      public function store(Request $request)
        {
    		if($request->popupid){
    			$popup = Popup::find($request->popupid);
    			$rules = [
    					'title' => 'required',
    					'class' => 'required|regex:/^[a-z0-9\-]+$/u|unique:popups,class,'.$popup->id
    			];
    		}else{
    			$popup = new Popup;
    			$popup->html = '';
    			$rules = [
    					'title' => 'required',
    					'class' => 'required|regex:/^[a-z0-9\-]+$/u|unique:popups,class'
    			];
    		}

    		$messages = [
    			'title.required' => 'Please enter a title',
    			'class.required' => 'Please enter a slug',
    			'class.unique' => 'Popup already exists',
    			'class.regex' => 'Should Contain only a-z,0-9 and -',
    		];
    		$validator = Validator::make($request->all(), $rules, $messages);
    		if ($validator->fails()){
    			return json_encode(array('errors' => $validator->getMessageBag()->toArray()));
    		}else{
    			$popup->title = $request->title;
    			$popup->class = $request->class;
    		  if($request->html){
            $popup->html = $request->html;
          }
          $exclude = array('title', 'class', 'html');
          foreach($request->all() as $key=>$val){
            if(!in_array($key, $exclude)){
              $inputs[$key] = $val;
            }
  				}
          $popup->settings =serialize($inputs);
    			$popup->save();
    			//saveHTML($popup->id);
    			return 'success';
    		}
        }
		public function edit($id)
		{
			$currenttheme = theme::getCurrentTheme();
			$popup = Popup::find($id);
			if($popup){
				$uploads = Upload::simplePaginate(15);
				return view('templates.'.$currenttheme['name'].'.index',['type'=> 'popup', 'page' => $popup, 'theme' => $currenttheme['name'], 'preview'=> false, 'publish'=> false, 'uploads' => $uploads]);
			}else{
				return abort(404);
			}
		}
		public function savehtml(Request $request)
		{
			$popup = Popup::find($request->pageid);
			$popup->html = $request->hidContent;
			$popup->save();
			//saveHTML($page->id);
			return Redirect::back()->with('success', 'Popup successfully updated.');
		}
}
