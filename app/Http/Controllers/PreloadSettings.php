<?php
namespace App\Http\Controllers;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use File;
use Illuminate\Http\Request;
use Carbon\Carbon;
use ZipArchive;
use App\theme;
use App\Pages;
use App\Settings;
use App\Menu;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use Illuminate\Support\Facades\Storage;

class PreloadSettings extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	public function setting()
    {
		$hostsettings = Settings::where('type', "host")->first();

		if($hostsettings){
			$savedsettings = unserialize($hostsettings->setting);
			return view('admin.settings', ['savedsettings'=> $savedsettings]);
		}else{
			return view('admin.settings');
		}
    }
    public function saveSettings(Request $requests)
    {
		if($requests->settingType=='host'){
			foreach($requests->all() as $key=>$request){
				$inputs[$key] = $request;
			}
			$ht = array(
					'name' => \Config::get('app.name'),
					'description'  => '',
					'private' => false,
					'has_pages' => true
			);
			$data_string = json_encode($ht);
			$ch = curl_init();
			curl_setopt_array($ch, [
				CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
				CURLOPT_URL => "https://api.github.com/orgs/".strtolower($inputs['org'])."/repos?access_token=".$inputs['token'],
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $data_string
			]);
			$response = curl_exec($ch);
			$response = json_decode($response);
			if(!empty($response->id)){
				$inputs['repoid'] = $response->id;
				if($inputs['prod_url']==""){
					$inputs['prod_url'] = 'https://'.getAdminsetting('host','org').'.github.io/'.\Config::get('app.name');
				}
				$settings = Settings::updateOrCreate(
					['type' => 'host'],
					['type' => 'host', 'setting' => serialize($inputs)]
				);
				if($settings){
					if($inputs['prod_url']!=""){
						$url = preg_replace("(^https?://)", "", $inputs['prod_url']);
						$addionalcommand = 'rm CNAME;';
						$finalurl = explode('/',$url);
						if(sizeof($finalurl)==1){
						$addionalcommand .= 'echo "'.$url.'" >> CNAME;';
						}
					}
					$process = new Process('cd '.base_path().'/public/html;echo "# Page Not Found" >> 404.html;echo "# web" >> index.html;'.$addionalcommand.'git init;git remote add origin https://'.$inputs['token'].':x-oauth-basic@github.com/'.$inputs['org'].'/'.\Config::get('app.name').'.git;git config user.email "'.$inputs['email'].'";git config user.name "'.$inputs['user'].'";');
					$process->start();
					$process->wait(function ($type, $buffer) {
						if (Process::ERR === $type) {
						echo 'ERR2 > '.$buffer;
						} else {
							echo 'OUT2 > '.$buffer;
						}
					});
					if($process->isSuccessful()){
						$theme = Theme::firstOrCreate([
						    'active' => '1',
						    'name'   => 'Default',
						]);
						$sourceDir = base_path().'/public/templates/'.$theme->name.'/public/';
						$destinationDir = base_path().'/public/html/assets/theme/'.$theme->name;
						File::copyDirectory($sourceDir, $destinationDir);

						$sourceDir = base_path().'/public/assets/public/';
						$destinationDir = base_path().'/public/html/assets/';
						File::copyDirectory($sourceDir, $destinationDir);


						$process = new Process('cd '.base_path().'/public/html;find . -type f -name "*.php" -exec rm {} +');
						$process->start();
						$process->wait(function ($type, $buffer) {
							if (Process::ERR === $type) {
							echo 'ERR2 > '.$buffer;
							} else {
								echo 'OUT2 > '.$buffer;
							}
						});

						generateCustomCSS();

						return Redirect::back()->with('success', 'Start Creating your site :)');
					}
				}
			}else{
				return Redirect::back()->with('error', 'Something has gone wrong please contact support: '.$response->message);
			}
		}
		if($requests->settingType=='cloudflareConnect'){
			foreach($requests->all() as $key=>$request){
				$inputs[$key] = $request;
			}
			$apikey = $inputs['cloud_apikey'];
			$email = $inputs['cloud_email'];
			$domain = preg_replace('#^https?://(www.)?#', '', rtrim($inputs['cloud_domain'],'/')); //$inputs['cloud_domain']
			//$serverip = '185.189.109.153';
			$servercname = getAdminsetting('host','org').'.github.io/'.config('app.name');
			$ht = array(
					'name' => $domain,
					'paused' => false,
					'jump_start'  => false,
			);
			if(!empty($email) && !empty($apikey)){
				$data_string = json_encode($ht);
				$ch = curl_init();
				curl_setopt_array($ch, [
					CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
					CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => $data_string,
					CURLOPT_HTTPHEADER => array(
						'X-Auth-Email: '.$email.'',
						'X-Auth-Key: '.$apikey.'',
						'Cache-Control: no-cache',
						'Content-Type:application/json',
						'purge_everything: true'
						)
				]);
				$response = json_decode(curl_exec($ch));
				if($response->success===true){
					$inputs['zone_id'] = $response->result->id;
					if($inputs['zone_id']){
										$ht = array(
												'type' => "CNAME",
												'name' => 'www',
												'content'  => $servercname,
												"ttl" => 1,
												"priority"=> 10,
												"proxied"=> false
										);
										$data_string = json_encode($ht);
										$ch = curl_init();
										curl_setopt_array($ch, [
											CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
											CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/".$inputs['zone_id'].'/dns_records',
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_FOLLOWLOCATION => true,
											CURLOPT_POST => true,
											CURLOPT_POSTFIELDS => $data_string,
											CURLOPT_HTTPHEADER => array(
												'X-Auth-Email: '.$email.'',
												'X-Auth-Key: '.$apikey.'',
												'Cache-Control: no-cache',
												'Content-Type:application/json',
												'purge_everything: true'
												)
										]);
										$response = json_decode(curl_exec($ch));
										$settings = Settings::updateOrCreate(
											['type' => 'cloudflare'],
											['type' => 'cloudflare', 'setting' => serialize($inputs)]
										);
										return Redirect::to(URL::previous() . $inputs['activetab'])->with('success', 'Cloudflare connection was successful!!');
					}
				}else{
					return Redirect::to(URL::previous() . $inputs['activetab'])->with('error', 'Something has gone wrong!! Please check your cloudflare email or API key');
				}
			}else{
				return Redirect::to(URL::previous() . $inputs['activetab'])->with('error', 'Both Email and API key are required'); //Redirect::back()->with('error', 'Both Email and API key are required');
			}
		}
		if($requests->settingType=='cloudflareDisconnect'){
			foreach($requests->all() as $key=>$request){
				$inputs[$key] = $request;
			}
			$apikey = $inputs['cloud_apikey'];
			$email = $inputs['cloud_email'];
			$zoneid = $inputs['zone_id'];
			if(!empty($email) && !empty($apikey)){
				$ch = curl_init();
				curl_setopt_array($ch, [
					CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
					CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/".$zoneid,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_CUSTOMREQUEST => 'DELETE',
					CURLOPT_HTTPHEADER => array(
						'X-Auth-Email: '.$email.'',
						'X-Auth-Key: '.$apikey.'',
						'Cache-Control: no-cache',
						'Content-Type:application/json',
						'purge_everything: true'
						)
				]);
				$response = json_decode(curl_exec($ch));
				if($response->success===true){
					$settings = Settings::where('type', 'cloudflare')->delete();
					return Redirect::to(URL::previous() . $inputs['activetab'])->with('success', 'Cloudflare Disconnected!!');
				}else{
					return Redirect::to(URL::previous() . $inputs['activetab'])->with('error', 'Something has gone wrong!! Please Contact Administrator');
				}
			}
		}
		if($requests->settingType=='cloudflareMinify'){
				$cloudflaresettings = Settings::where('type', 'cloudflare')->first();
				$ht['value'] = array(
						'css' => "off",
						'html' => 'off',
						'js'  => 'off'
				);
				if($cloudflaresettings){
					$savedsettings = unserialize($cloudflaresettings->setting);
				}
				$apikey = $savedsettings['cloud_apikey'];
				$email = $savedsettings['cloud_email'];
				$zoneid = $savedsettings['zone_id'];
				$savedsettings['cloudflare_minify_html'] = 'off';
				$savedsettings['cloudflare_minify_js'] = 'off';
				$savedsettings['cloudflare_minify_css'] = 'off';
				if($requests->cloudflare_minify_html && $requests->cloudflare_minify_html=='on'){
					$ht['value']['html'] = 'on';
					$savedsettings['cloudflare_minify_html'] = 'on';
				}
				if($requests->cloudflare_minify_js && $requests->cloudflare_minify_js=='on'){
					$ht['value']['js'] = 'on';
					$savedsettings['cloudflare_minify_js'] = 'on';
				}
				if($requests->cloudflare_minify_css && $requests->cloudflare_minify_css=='on'){
					$ht['value']['css'] = 'on';
					$savedsettings['cloudflare_minify_css'] = 'on';
				}
				$data_string = json_encode($ht);
				$ch = curl_init();
				curl_setopt_array($ch, [
					CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
					CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/".$zoneid.'/settings/minify',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_CUSTOMREQUEST => 'PATCH',
					CURLOPT_POSTFIELDS => $data_string,
					CURLOPT_HTTPHEADER => array(
						'X-Auth-Email: '.$email.'',
						'X-Auth-Key: '.$apikey.'',
						'Cache-Control: no-cache',
						'Content-Type:application/json',
						'purge_everything: true'
						)
				]);
				$response = json_decode(curl_exec($ch));
				if($response->success===true){
						$settings = Settings::updateOrCreate(
							['type' => 'cloudflare'],
							['type' => 'cloudflare', 'setting' => serialize($savedsettings)]
						);
						return Redirect::to(URL::previous() . $requests->activetab)->with('success', 'Cloudflare Minify was successful!!');
				}else{
					return Redirect::to(URL::previous() . $requests->activetab)->with('error', 'Something has gone wrong!! Please Try again or Contact Administrator');
				}
		}
		if($requests->settingType=='cloudflareDevelopement'){
			$cloudflaresettings = Settings::where('type', 'cloudflare')->first();
			$ht = array(
					'value' => "off",
			);
			if($cloudflaresettings){
				$savedsettings = unserialize($cloudflaresettings->setting);
			}
			$apikey = $savedsettings['cloud_apikey'];
			$email = $savedsettings['cloud_email'];
			$zoneid = $savedsettings['zone_id'];
			$savedsettings['cloudflare_developement_mode'] = 'off';
			if($requests->cloudflare_developement_mode && $requests->cloudflare_developement_mode=='on'){
				$ht['value'] = 'on';
				$savedsettings['cloudflare_developement_mode'] = 'on';
			}
			$data_string = json_encode($ht);
			$ch = curl_init();
			curl_setopt_array($ch, [
				CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
				CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/".$zoneid.'/settings/development_mode',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_CUSTOMREQUEST => 'PATCH',
				CURLOPT_POSTFIELDS => $data_string,
				CURLOPT_HTTPHEADER => array(
					'X-Auth-Email: '.$email.'',
					'X-Auth-Key: '.$apikey.'',
					'Cache-Control: no-cache',
					'Content-Type:application/json',
					'purge_everything: true'
					)
			]);
			$response = json_decode(curl_exec($ch));
			if($response->success===true){
					$settings = Settings::updateOrCreate(
						['type' => 'cloudflare'],
						['type' => 'cloudflare', 'setting' => serialize($savedsettings)]
					);
					return Redirect::to(URL::previous() . $requests->activetab)->with('success', 'Cloudflare Developement Mode is active!!');
			}else{
				return Redirect::to(URL::previous() . $requests->activetab)->with('error', 'Something has gone wrong!! Please Try again or Contact Administrator');
			}
		}
		if($requests->settingType=='cloudflareSSL'){
					$cloudflaresettings = Settings::where('type', 'cloudflare')->first();
					$ht = array(
							'value' => "off",
					);
					if($cloudflaresettings){
						$savedsettings = unserialize($cloudflaresettings->setting);
					}
					$apikey = $savedsettings['cloud_apikey'];
					$email = $savedsettings['cloud_email'];
					$zoneid = $savedsettings['zone_id'];
					$savedsettings['cloudflare_ssl'] = 'off';
					if($requests->cloudflare_ssl && $requests->cloudflare_ssl=='on'){
						$ht['value'] = 'full';
						$savedsettings['cloudflare_ssl'] = 'on';
					}
					$data_string = json_encode($ht);
					$ch = curl_init();
					curl_setopt_array($ch, [
						CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
						CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/".$zoneid.'/settings/ssl',
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_CUSTOMREQUEST => 'PATCH',
						CURLOPT_POSTFIELDS => $data_string,
						CURLOPT_HTTPHEADER => array(
							'X-Auth-Email: '.$email.'',
							'X-Auth-Key: '.$apikey.'',
							'Cache-Control: no-cache',
							'Content-Type:application/json',
							'purge_everything: true'
							)
					]);
					$response = json_decode(curl_exec($ch));
					if($response->success===true){

							$settings = Settings::updateOrCreate(
								['type' => 'cloudflare'],
								['type' => 'cloudflare', 'setting' => serialize($savedsettings)]
							);
							return Redirect::to(URL::previous() . $requests->activetab)->with('success', 'SSl is enabled!!');
					}else{
						return Redirect::to(URL::previous() . $requests->activetab)->with('error', 'Something has gone wrong!! Please Try again or Contact Administrator');
					}
		}
		if($requests->settingType=='cloudflareAlwaysSSl'){
			$cloudflaresettings = Settings::where('type', 'cloudflare')->first();
			$ht = array(
					'value' => "off",
			);
			if($cloudflaresettings){
				$savedsettings = unserialize($cloudflaresettings->setting);
			}
			$apikey = $savedsettings['cloud_apikey'];
			$email = $savedsettings['cloud_email'];
			$zoneid = $savedsettings['zone_id'];
			$savedsettings['cloudflare_ssl_always'] = 'off';
			if($requests->cloudflare_ssl_always && $requests->cloudflare_ssl_always=='on'){
				$ht['value'] = 'on';
				$savedsettings['cloudflare_ssl_always'] = 'on';
			}
			$data_string = json_encode($ht);
			$ch = curl_init();
			curl_setopt_array($ch, [
				CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
				CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/".$zoneid.'/settings/always_use_https',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_CUSTOMREQUEST => 'PATCH',
				CURLOPT_POSTFIELDS => $data_string,
				CURLOPT_HTTPHEADER => array(
					'X-Auth-Email: '.$email.'',
					'X-Auth-Key: '.$apikey.'',
					'Cache-Control: no-cache',
					'Content-Type:application/json',
					'purge_everything: true'
					)
			]);
			$response = json_decode(curl_exec($ch));
			if($response->success===true){
					$settings = Settings::updateOrCreate(
						['type' => 'cloudflare'],
						['type' => 'cloudflare', 'setting' => serialize($savedsettings)]
					);
					return Redirect::to(URL::previous() . $requests->activetab)->with('success', 'Cloudflare setting updated!!');
			}else{
				return Redirect::to(URL::previous() . $requests->activetab)->with('error', 'Something has gone wrong!! Please Try again or Contact Administrator');
			}
		}
		if($requests->settingType=='cloudflareForcessl'){
			$cloudflaresettings = Settings::where('type', 'cloudflare')->first();
			$ht = array(
					'value' => "off",
			);
			if($cloudflaresettings){
				$savedsettings = unserialize($cloudflaresettings->setting);
			}
			$apikey = $savedsettings['cloud_apikey'];
			$email = $savedsettings['cloud_email'];
			$zoneid = $savedsettings['zone_id'];
			$savedsettings['cloudflare_force_ssl'] = 'off';
			if($requests->cloudflare_force_ssl && $requests->cloudflare_force_ssl=='on'){
				$ht['value'] = 'on';
				$savedsettings['cloudflare_force_ssl'] = 'on';
			}
			$data_string = json_encode($ht);
			$ch = curl_init();
			curl_setopt_array($ch, [
				CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
				CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/".$zoneid.'/settings/automatic_https_rewrites',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_CUSTOMREQUEST => 'PATCH',
				CURLOPT_POSTFIELDS => $data_string,
				CURLOPT_HTTPHEADER => array(
					'X-Auth-Email: '.$email.'',
					'X-Auth-Key: '.$apikey.'',
					'Cache-Control: no-cache',
					'Content-Type:application/json',
					'purge_everything: true'
					)
			]);
			$response = json_decode(curl_exec($ch));
			if($response->success===true){
					$settings = Settings::updateOrCreate(
						['type' => 'cloudflare'],
						['type' => 'cloudflare', 'setting' => serialize($savedsettings)]
					);
					return Redirect::to(URL::previous() . $requests->activetab)->with('success', 'Cloudflare Force SSl is active!!');
			}else{
				return Redirect::to(URL::previous() . $requests->activetab)->with('error', 'Something has gone wrong!! Please Try again or Contact Administrator');
			}
		}
		if($requests->settingType=='cloudflarePurgeCache'){
			$cloudflaresettings = Settings::where('type', 'cloudflare')->first();
			$ht = array(
					'purge_everything' => true,
			);
			if($cloudflaresettings){
				$savedsettings = unserialize($cloudflaresettings->setting);
			}
			$apikey = $savedsettings['cloud_apikey'];
			$email = $savedsettings['cloud_email'];
			$zoneid = $savedsettings['zone_id'];
			$data_string = json_encode($ht);
			$ch = curl_init();
			curl_setopt_array($ch, [
				CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
				CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/".$zoneid.'/purge_cache',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_CUSTOMREQUEST => 'DELETE',
				CURLOPT_POSTFIELDS => $data_string,
				CURLOPT_HTTPHEADER => array(
					'X-Auth-Email: '.$email.'',
					'X-Auth-Key: '.$apikey.'',
					'Cache-Control: no-cache',
					'Content-Type:application/json',
					'purge_everything: true'
					)
			]);
			$response = json_decode(curl_exec($ch));
			if($response->success===true){
					return Redirect::to(URL::previous() . $requests->activetab)->with('success', 'Cache Cleared!!');
			}else{
				return Redirect::to(URL::previous() . $requests->activetab)->with('error', 'Something has gone wrong!! Please Try again or Contact Administrator');
			}
		}
		if($requests->settingType=='firebase'){
			$firebasesettings = Settings::where('type', 'firebase')->first();
			$savedsettings = array();
			if($firebasesettings){
				$savedsettings = unserialize($firebasesettings->setting);
			}
			$savedsettings['apiKey'] = $requests->apiKey;
			$savedsettings['authDomain'] = $requests->authDomain;
			$savedsettings['databaseURL'] = $requests->databaseURL;
			$savedsettings['projectId'] = $requests->projectId;
			$savedsettings['storageBucket'] = $requests->storageBucket;
			$savedsettings['messagingSenderId'] = $requests->messagingSenderId;
			$savedsettings['appId'] = $requests->appId;
			$savedsettings['pagePrivacy'] = $requests->pagePrivacy;
			$savedsettings['formEntrySave']= $requests->formEntrySave;
			$savedsettings['activetab']= $requests->activetab;
			$settings = Settings::updateOrCreate(
				['type' => 'firebase'],
				['type' => 'firebase', 'setting' => serialize($savedsettings)]
			);
			if(empty($requests->pagePrivacy)){
				Pages::whereNotNull('password')->update(['password'=> '']);
				firebaseDeleteAllPagesPassword();
			}
			return Redirect::to(URL::previous() . $savedsettings['activetab'])->with('success', 'Success!! Please enable features');
		}
	}
	public function deleteRepo()
    {
		$hostsettings = Settings::where('type', "host")->first();
		if($hostsettings){
			$savedsettings = unserialize($hostsettings->setting);
		}
		$process = new Process('cd '.base_path().'/public/html;rm -rf *;');
		$process->start();
		$process->wait(function ($type, $buffer) {
			if (Process::ERR === $type) {
			echo 'ERR2 > '.$buffer;
			} else {
				echo 'OUT2 > '.$buffer;
			}
		});
		if($process->isSuccessful()){
				$ch = curl_init();
				$ht = array(
				);
				$data_string = json_encode($ht);
				curl_setopt_array($ch, [
					CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
					CURLOPT_URL => "https://api.github.com/repos/".strtolower($savedsettings['org'])."/".\Config::get('app.name')."?access_token=".$savedsettings['token'],
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_POST => true,
					CURLOPT_CUSTOMREQUEST => 'DELETE',
					CURLOPT_POSTFIELDS => $data_string
				]);
				$response = curl_exec($ch);
				unset($savedsettings['repoid']);
				$settings = Settings::updateOrCreate(
					['type' => 'host'],
					['type' => 'host', 'setting' => serialize($savedsettings)]
				);
				return Redirect::back()->with('success', 'Repo removed');
		}
	}
	public function publish()
    {
		$process = new Process('cd '.base_path().'/public/html;git pull origin gh-pages');
		$process->run();

		saveallHTML();


		$sourceDir = base_path().'/public/uploads/';
		$destinationDir = base_path().'/public/html/uploads/';
		File::copyDirectory($sourceDir, $destinationDir);

		$generalsettings = Settings::where('type', "general")->first();
		if($generalsettings)
			$savedsettings = unserialize($generalsettings->setting);

		if(!empty($savedsettings['home'])){
			$page = Pages::find($savedsettings['home']);
			if (!File::move(public_path('/html/'.$page->slug.'.html'), public_path('/html/index.html')))
			{
				return Redirect::back()->with('publisherror', 'We were not able to set your homepage. Please try again');
			}
			saveHTML($page->id);
		}
		if(!empty($savedsettings['404'])){
			$page = Pages::find($savedsettings['404']);
			if (!File::move(public_path('/html/'.$page->slug.'.html'), public_path('/html/404.html')))
			{
				return Redirect::back()->with('publisherror', 'We were not able to set your 404 page. Please try again');
			}
			saveHTML($page->id);
		}


		$process = new Process('cd '.base_path().'/public/html;git reset && git add *;');
		$process->start();
		$process->wait(function ($type, $buffer) {
			echo "";
		});
		if($process->isSuccessful()){
			$rand = rand(0, 999999999);
			$process = new Process('cd '.base_path().'/public/html;git commit -a -m "'.$rand.' - '.date('d/m/Y').'";git push -f origin master && git checkout -B gh-pages;git push origin gh-pages');
			$process->start();
			$process->wait(function ($type, $buffer) {
				echo '';
				/*if (Process::ERR === $type) {
						echo 'ERR2 > '.$buffer;
						} else {
							echo 'OUT2 > '.$buffer;
						}*/
			});
			if($process->isSuccessful()){
				$process = new Process('cd '.base_path().'/public;rm -rf html-download;');
				$process->start();
				$process->wait(function ($type, $buffer) {
					echo "";
				});
				return Redirect::back()->with('publishsuccess', 'Site Published');
			}
		}
	}
	public function downloadhtml()
    {
			/*$zipper = new \Chumper\Zipper\Zipper;
			$files = glob(public_path('js/*'));
			$zipper->make(public_path('html/html.zip'))->add($files);
			$zipper->close();
				$files = File::allFiles(public_path('/html'));
				$filesarray = array();
				foreach ($files as $file) {
					$filesarray[] = $file->getpathname();
				}
				\Zipper::make(public_path('html/html.zip'))->add($files)->close(); */
		$process = new Process('cd '.base_path().'/public;rm -rf html-download;');
		$process->start();
		$process->wait(function ($type, $buffer) {
			echo "";
		});
		if($process->isSuccessful()){
			$sourceDir = base_path().'/public/html/';
			$destinationDir = base_path().'/public/html-download/';
			File::copyDirectory($sourceDir, $destinationDir);

			$files = File::allFiles(public_path('/html-download'));
			$filesarray = array();
			foreach ($files as $file) {
				if($file->getextension()=='html'){
					$content = File::get($file->getpathname());
					$content = preg_replace('/'.str_replace('/','\/',getSetting('prod_url','host')).'.*?assets\/?/','assets/',$content);
					$content = preg_replace('/'.str_replace('/','\/',getSetting('prod_url','host')).'.*?uploads\/?/','uploads/',$content);
					File::put($file->getpathname(), $content);
				}
			}
			$process = new Process('cd '.base_path().'/public/html-download;rm htmlfiles.zip;zip -r htmlfiles *;');
			$process->start();
			$process->wait(function ($type, $buffer) {
				echo "";
			});
			if($process->isSuccessful()){
				return response()->download(public_path('html-download/htmlfiles.zip'));
			}
		}
	}

}
