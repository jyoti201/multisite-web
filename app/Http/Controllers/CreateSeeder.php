<?php

namespace App\Http\Controllers;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;
use Artisan;
use File;
use App\Pages;
use App\Post;
use App\theme;

class CreateSeeder extends Controller
{
    public function Index()
    {
		exec('php artisan db:seed --class=UsersTableSeeder');
		exec('git push origin master');
	}
	public function resetdb(Request $request)
    {
		$request=json_decode($request[0]);
		$key = str_replace('base64:','', \Config::get('app.key'));
		if($request->key == $key){
			Artisan::call('migrate:refresh', [
			'--force' => true,
			]);
			/*Artisan::call('db:seed', [
				'--class' => 'UsersTableSeeder',
			]);*/
			$ee = \DB::table('users')->insert(array (
				0 =>
				array (
					'id' => $request->id,
					'name' => $request->name,
					'email' => $request->email,
					'password' => $request->pass,
					'dob' => '',
					'phone' => '',
					'avatar' => '',
					'role' => 'site-admin',
					'verified'=>true,
					'active'=>true,
					'remember_token' => NULL,
				),
			));
			$themesdir = File::directories(resource_path('views/templates'));
			$themes = array();
			foreach($themesdir as $themedir){
				$active = 0;
				if(basename($themedir) == 'Default'){
					$active = 1;
				}
				$themes[] = array('name' => basename($themedir),'active' => $active);
			}
			$ee = \DB::table('themes')->insert($themes);
			dd('success');
		}
	}
	public function update()
    {
		//$user = \Config::get('app.name');
		/*$sourceDir = base_path().'/public/uploads/';
		$destinationDir = base_path().'/public/html/uploads-temp/';
		File::copyDirectory($sourceDir, $destinationDir);*/

		$process = new Process('cd '.base_path().';git fetch --all;git reset --hard origin/master;git clean -df;git pull origin master;php artisan migrate;');
		$process->start();
		$process->wait(function ($type, $buffer) {
			if (Process::ERR === $type) {
			echo 'ERR2 > '.$buffer;
			} else {
				echo 'OUT2 > '.$buffer;
			}
		});
		/*
		if($process->isSuccessful()){
			$sourceDir = base_path().'/public/html/uploads-temp/';
			$destinationDir = base_path().'/public/uploads/';
			File::copyDirectory($sourceDir, $destinationDir);
		}*/
		/*
		$txt = "APP_NAME=".$user."\n";
		$txt .= "APP_ENV=local\n";
		$txt .= "APP_KEY=base64:mIURF19F3JXJVJ+rqYyJcq1Au+NK4GyykUfau3heyN8=\n";
		$txt .= "APP_DEBUG=true\n";
		$txt .= "APP_LOG_LEVEL=debug\n";
		$txt .= "APP_URL=http://".$user.".frezit.com\n\n";

		$txt .= "DB_CONNECTION=mysql\n";
		$txt .= "DB_HOST=127.0.0.1\n";
		$txt .= "DB_PORT=3306\n";
		$txt .= "DB_DATABASE=multisite_".$user."\n";
		$txt .= "DB_USERNAME=root\n";
		$txt .= "DB_PASSWORD=JvnPXKeXry0R\n\n";

		$txt .= "BROADCAST_DRIVER=log\n";
		$txt .= "CACHE_DRIVER=file\n";
		$txt .= "SESSION_DRIVER=file\n";
		$txt .= "QUEUE_DRIVER=sync\n\n";

		$txt .= "REDIS_HOST=127.0.0.1\n";
		$txt .= "REDIS_PASSWORD=null\n";
		$txt .= "REDIS_PORT=6379\n\n";

		$txt .= "MAIL_DRIVER=smtp\n";
		$txt .= "MAIL_HOST=smtp.mailtrap.io\n";
		$txt .= "MAIL_PORT=2525\n";
		$txt .= "MAIL_USERNAME=null\n";
		$txt .= "MAIL_PASSWORD=null\n";
		$txt .= "MAIL_ENCRYPTION=null\n";

		$txt .= "PUSHER_APP_ID=\n";
		$txt .= "PUSHER_APP_KEY=\n";
		$txt .= "PUSHER_APP_SECRET=\n";

		file_put_contents(base_path().'/.env', $txt); */
		dd('success');
    }
	function updateLocal(){
		$process = new Process('cd '.base_path().';git reset --hard origin/master;git clean -df;git pull origin master;php artisan migrate;');
		$process->start();
		$process->wait(function ($type, $buffer) {
			if (Process::ERR === $type) {
			echo 'ERR2 > '.$buffer;
			} else {
				echo 'OUT2 > '.$buffer;
			}
		});
	}
	public function test()
    {
		/* Git Hub Account Connect */
		/* $username='lakhi111';
		$password='98592lakhi#';

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
		curl_setopt($ch, CURLOPT_URL, 'https://api.github.com/user');
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
		$result=curl_exec ($ch);
		curl_close ($ch);

		print_r($status_code);
		print_r($result); */


		/* Netlify Site Create */

		/* $ht = array(
            'name' => 'new_123_site_dul2',
            'custom_domain'  => 'testing3.frezit.com',
            'force_ssl' => false,
			'processing_settings' => "{'css': {'bundle': true, 'minify': true}, 'js': {'bundle': true, 'minify': true}, 'html': {'pretty_urls': true, 'canonical_urls': true}, 'images': {'optimize': true}}"
        );
		$data_string = $ht;
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => "https://api.netlify.com/api/v1/sites",
			CURLOPT_HTTPHEADER => array('Authorization: Bearer 1c92f880b36f62d566af97e1ad8d970c2dffd4cc62178779cdf474c945363d48'),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data_string
		]);
		echo $response = curl_exec($ch); */


		/* Netlify Update Zip */
		/*$ht = array(
            'name' => 'new_123_site_dul2',
            'custom_domain'  => 'testing3.frezit.com',
            'force_ssl' => false,
			'processing_settings' => "{'css': {'bundle': true, 'minify': true}, 'js': {'bundle': true, 'minify': true}, 'html': {'pretty_urls': true, 'canonical_urls': true}, 'images': {'optimize': true}}",
			'files' => 'http://web.frezit.com/index.zip'
        );
		$data_string = $ht;
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => "https://api.netlify.com/api/v1/sites/ceaad3f9-de7a-482c-8760-ac046257979f",
			CURLOPT_HTTPHEADER => array('Content-Type: application/zip', 'http://web.frezit.com/index.zip', 'Authorization: Bearer 1c92f880b36f62d566af97e1ad8d970c2dffd4cc62178779cdf474c945363d48'),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_CUSTOMREQUEST => 'PUT',
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data_string
		]);
		echo $response = curl_exec($ch);*/


		/* create repo in github */
		/* $ht = array(
            'name' => 'Testing2',
            'description'  => '',
            'private' => false,
			'has_pages' => true
        );
		$data_string = json_encode($ht);
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
			CURLOPT_URL => "https://api.github.com/orgs/frezit/repos?access_token=6fd1e99d169c12d7273eb19c15671bd70c7890a1",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data_string
		]);
		echo $response = curl_exec($ch); */

    }
	public function previewpage($id)
    {
		$currenttheme = theme::getCurrentTheme();
		$page = Pages::find($id);
		if($page){
			return view('templates.'.$currenttheme['name'].'.index',['type'=>'page', 'page' => $page, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> false]);
		}else{
			return abort(404);
		}
	}
  public function previewpost($id)
    {
      $currenttheme = theme::getCurrentTheme();
  		$page = Post::find($id);
  		if($page){
  			return view('templates.'.$currenttheme['name'].'.index',['type'=>'post', 'page' => $page, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> false]);
  		}else{
  			return abort(404);
  		}
	}
}
