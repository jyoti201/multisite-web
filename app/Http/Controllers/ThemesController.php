<?php

namespace App\Http\Controllers;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;
use App\theme;
use File;
use Storage;
use Illuminate\Support\Facades\Redirect;

class ThemesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
    {
        $this->middleware('auth');
		$this->middleware('checkhost');
    }
	
    public function index()
    {
		$themes = File::directories(resource_path('views/templates'));
		$themesinfo = array();
		$currenttheme = Theme::getCurrentTheme();
		foreach($themes as $theme){
			if($currenttheme['name']==basename($theme)){
			$themesinfo[] = array('name'=>basename($theme),'active'=> 1);
			}else{
			$themesinfo[] = array('name'=>basename($theme),'active'=> 0);
			}
		}
		return view('admin.themes', ['themes' => $themesinfo]);
		//return view('admin.themes', ['themes' => Theme::All()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$themes = File::directories(resource_path('views/templates'));
		$themesinfo = array();
		$currenttheme = Theme::getCurrentTheme();
		$installedthemes=array();
		foreach($themes as $theme){
			$installedthemes[] = basename($theme);
		}
		if(in_array($request->theme, $installedthemes)){
			Theme::where('active', '=', 1)->update(['active' => '0']);
			Theme::updateOrCreate(
				['name' => $request->theme],
				['name' => $request->theme, 'active' => '1']
			);
			
			$process = new Process('cd '.base_path().'/public/templates && rm -rf * && mkdir '.$request->theme);
			$process->start();
			$process->wait(function ($type, $buffer) {
				if (Process::ERR === $type) {
						echo 'ERR2 > '.$buffer;
						} else {
							echo 'OUT2 > '.$buffer;
						}
			});
			if($process->isSuccessful()){
				foreach($installedthemes as $installedtheme){
					$sourceDir = base_path().'/resources/views/templates/'.$installedtheme.'/';
					$destinationDir = base_path().'/public/templates/'.$installedtheme.'/';
					File::copyDirectory($sourceDir, $destinationDir);
				}
				
				$process = new Process('cd '.base_path().'/public/templates && rm -rf *.php;');
				$process->start();
				$process->wait(function ($type, $buffer) {
					if (Process::ERR === $type) {
							echo 'ERR2 > '.$buffer;
							} else {
								echo 'OUT2 > '.$buffer;
							}
				});
				if($process->isSuccessful()){
				return Redirect::back()->with('success', 'Theme '.getThemeName($request->theme).' Activated');
				}
			}
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
