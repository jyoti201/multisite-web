<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Sections;
use App\theme;
use App\Settings;
use App\Upload;
use Image;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
class SectionsController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  $this->middleware('checkhost');
  }
public function Index()
  {
      return view('admin.sections', ['sections' => Sections::All()]);
  }
public function trash()
  {
      return view('admin.sections', ['sections' => Sections::onlyTrashed()->get(), 'trash' => true]);
  }
public function store(Request $request)
  {
    if($request->sectionid){
			$section = Sections::find($request->sectionid);
			$rules = [
					'title' => 'required',
			];
		}else{
			$section = new Sections;
			$section->html = '';
			$rules = [
					'title' => 'required',
			];
		}

		$messages = [
			'title.required' => 'Please enter a title',
		];
		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()){
			return json_encode(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$section->title = $request->title;
      $section->save();
			$section->shortcode = '[section id="'.$section->id.'"]';
			$section->save();
			return 'success';
		}
  }
public function snippets(){
  $currenttheme = theme::getCurrentTheme();
  return view('templates.'.$currenttheme['name'].'.snippet', ['theme' => $currenttheme['name']]);
}
public function edit($id)
  {
  $currenttheme = theme::getCurrentTheme();
  $section = Sections::find($id);
  if($section){
    $uploads = Upload::simplePaginate(15);
    return view('templates.'.$currenttheme['name'].'.index',['type'=> 'section', 'page' => $section, 'theme' => $currenttheme['name'], 'preview'=> false, 'publish'=> false,'uploads' => $uploads]);
  }else{
    return abort(404);
  }
}
public function preview($id)
  {
  $currenttheme = theme::getCurrentTheme();
  $section = Sections::find($id);
  if($section){
    return view('templates.'.$currenttheme['name'].'.index',['type'=> 'section', 'page' => $section, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> false]);
  }else{
    return abort(404);
  }
}
public function previewTrash($id)
  {
  $currenttheme = theme::getCurrentTheme();
  $section = Sections::onlyTrashed()->where('id', $id)->first();
  if($section){
    return view('templates.'.$currenttheme['name'].'.index',['type'=> 'section', 'page' => $section, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> false]);
  }else{
    return abort(404);
  }
}
public function restore($id)
  {
  $currenttheme = theme::getCurrentTheme();
  $section = Sections::onlyTrashed()->where('id', $id)->first();
  $section->restore();
  return Redirect::back()->with('success', 'Section restored.');
}
public function clone($id)
  {
  $currenttheme = theme::getCurrentTheme();
  $section = Sections::find($id);
  $duplicate = $section->replicate();
  $duplicate->title  = $section->title . " - Copy";

  $duplicate->save();
  $duplicate->shortcode  = '[section id="'.$duplicate->id.'"]';
  $duplicate->save();
  return Redirect::back()->with('success', 'Section Cloned.');
}
public function forceDelete($id)
  {
  $currenttheme = theme::getCurrentTheme();
  $section = Sections::onlyTrashed()->where('id', $id)->first();
  $section->forceDelete();
  return Redirect::back()->with('success', 'Section permanently deleted.');
}

public function show($id)
  {

}
public function savehtml(Request $request)
  {
  $section = Sections::find($request->pageid);
  $section->html = $request->hidContent;
  $section->save();
  //saveHTML($section->id);
  return Redirect::back()->with('success', 'Section successfully updated.');
}
public function destroy($id)
  {
  $section = Sections::find($id);
      $section->delete();
  return Redirect::back()->with('success', 'Section successfully trashed.');
}
public function builderstore(Request $request)
  {
    if($request->sectionid){
			$section = Sections::find($request->sectionid);
			$rules = [
					'title' => 'required',
			];
		}else{
			$section = new Sections;
			$section->html = '';
			$rules = [
					'title' => 'required',
			];
		}

		$messages = [
			'title.required' => 'Please enter a title',
		];
		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()){
			return json_encode(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$section->title = $request->title;
      $section->html = $request->html;
      $section->save();
			$section->shortcode = '[section id="'.$section->id.'"]';
			$section->save();
			return json_encode(array('success' => true, 'shortcode'=>$section->shortcode));
		}
  }
  public function getSectionFromHtml(Request $request)
    {
      if($request->sectionhtml){
        $shortcode = getSectionShortcodeFromHtml($request->sectionhtml);
        $html = implementSavedSections($shortcode);
  			return json_encode(array('success' => true, 'html'=>$html));
      }else{
        return json_encode(array('errors' => true));
      }
    }
}
