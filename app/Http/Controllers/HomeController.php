<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use App\User;
use Auth;
use App\Pages;
use App\Posts;
use App\Widget;
use App\Upload;
use App\Menu;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$hostsettings = Settings::where('type', "host")->first();
		if(empty($hostsettings)){
			return redirect('/start');
		}else{
		return view('home');
		}
    }
	public function profile()
    {
		$uploads = Upload::simplePaginate(15);
		return view('admin.profile', ['member' => Auth::user(), 'uploads' => $uploads]);
	}
	public function profileSave(Request $request)
    {
		$uploads = Upload::simplePaginate(15);
		$user = User::find($request->id);
		if($request->pass!=''){
			$rules = [
				'pass' => 'confirmed',
			];
			$messages = [
				'pass.confirmed' => 'Password Not matched!!',
			];
		}else{
			$rules = [
			];
			$messages = [
			];
		}
		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()){
			return Redirect::back()->withErrors($validator->getMessageBag()->toArray());
		}else{
			if(!empty($request->pass)){
				$user->password = bcrypt($request->pass);
			}
			$user->avatar = !empty($request->avatar)?$request->avatar:'';
			$user->phone = !empty($request->phone)?$request->phone:'';
			$user->save();
			return Redirect::back()->with('success', 'Profile Updated');
		}
	}
	public function start()
  {
		$hostsettings = Settings::where('type', "host")->first();
		if(empty($hostsettings)){
			return view('publish');
		}else{
			return redirect()->to('/');
		}
  }
 
}
