<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Pages;
use App\theme;
use App\Settings;
use App\Upload;
use Image;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		$this->middleware('checkhost');
    }
	public function Index()
    {
        return view('admin.pages', ['pages' => Pages::All()]);
    }
	public function trash()
    {
        return view('admin.pages', ['pages' => Pages::onlyTrashed()->get(), 'trash' => true]);
    }
	public function store(Request $request)
    {
		if($request->pageid){
			$page = Pages::find($request->pageid);
			$rules = [
					'title' => 'required',
					'slug' => 'required|regex:/^[a-z0-9\-]+$/u|unique:pages,slug,'.$page->id
			];
		}else{
			$page = new Pages;
			$page->html = '';
			$page->meta_title = '';
			$page->meta_description = '';
			$page->meta_keywords = '';
			$page->custom_css = '';
      $page->password = '';
			$page->sidebar = '';
			$rules = [
					'title' => 'required',
					'slug' => 'required|regex:/^[a-z0-9\-]+$/u|unique:pages,slug'
			];
		}

		$messages = [
			'title.required' => 'Please enter a title',
			'slug.required' => 'Please enter a slug',
			'slug.unique' => 'Page already exists',
			'slug.regex' => 'Should Contain only a-z,0-9 and -',
		];
		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()){
			return json_encode(array('errors' => $validator->getMessageBag()->toArray()));
		}else{
			$page->title = $request->title;
			$page->slug = $request->slug;

			if(!empty($request->meta_title)){
				$page->meta_title = $request->meta_title;
      }else{
        $page->meta_title = '';
      }
			if(!empty($request->meta_description)){
				$page->meta_description = $request->meta_description;
      }else{
        if(getSetting('global_meta_desc','general')){
        $page->meta_description = getSetting('global_meta_desc','general');
        }
      }
			if(!empty($request->meta_keywords)){
				$page->meta_keywords = $request->meta_keywords;
      }else{
        if(getSetting('global_meta_keywords','general')){
        $page->meta_keywords = getSetting('global_meta_keywords','general');
        }
      }
      if(!empty($request->passwordprotect) && !empty($request->password)){
				$page->password = $request->password;
      }else{
        $page->password = '';
      }
			if(!empty($request->sidebaroption))
				$page->sidebar = $request->sidebar;

			$page->save();
			//saveHTML($page->id);
      if(!empty($request->passwordprotect) && !empty($request->password)){
        firebaseAddPagePassword($page->id, $request->password);
      }else{
        firebaseDeletePagePassword($page->id);
      }
			return 'success';
		}
    }
	public function snippets(){
		$currenttheme = theme::getCurrentTheme();
		return view('templates.'.$currenttheme['name'].'.snippet', ['theme' => $currenttheme['name']]);
	}
	public function edit($id)
    {
		$currenttheme = theme::getCurrentTheme();
		$page = Pages::find($id);
		if($page){
			$uploads = Upload::simplePaginate(15);
			return view('templates.'.$currenttheme['name'].'.index',['type'=> 'page', 'page' => $page, 'theme' => $currenttheme['name'], 'preview'=> false, 'publish'=> false,'uploads' => $uploads]);
		}else{
			return abort(404);
		}
	}
	public function preview($id)
    {
		$currenttheme = theme::getCurrentTheme();
		$page = Pages::find($id);
		if($page){
			return view('templates.'.$currenttheme['name'].'.index',['type'=> 'page', 'page' => $page, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> false]);
		}else{
			return abort(404);
		}
	}
	public function previewTrash($id)
    {
		$currenttheme = theme::getCurrentTheme();
		$page = Pages::onlyTrashed()->where('id', $id)->first();
		if($page){
			return view('templates.'.$currenttheme['name'].'.index',['type'=> 'page', 'page' => $page, 'theme' => $currenttheme['name'], 'preview'=> true, 'publish'=> false]);
		}else{
			return abort(404);
		}
	}
	public function restore($id)
    {
		$currenttheme = theme::getCurrentTheme();
		$page = Pages::onlyTrashed()->where('id', $id)->first();
		$page->restore();
		return Redirect::back()->with('success', 'Page restored.');
	}
  public function clone($id)
    {
		$currenttheme = theme::getCurrentTheme();
		$page = Pages::find($id);
    $duplicate = $page->replicate();
    $duplicate->title  = $page->title . " - Copy";
    $duplicate->slug  = $page->slug . "-".$page->id;
    $duplicate->save();
		return Redirect::back()->with('success', 'Page Cloned.');
	}
	public function forceDelete($id)
    {
		$currenttheme = theme::getCurrentTheme();
		$page = Pages::onlyTrashed()->where('id', $id)->first();
		$page->forceDelete();
		deleteHTML($page->slug);
		return Redirect::back()->with('success', 'Page permanently deleted.');
	}
	public function savecover(Request $request)
    {
		$this->validate($request, [
            'fileCover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
		$file = substr($request->fileCover->getClientOriginalName(), 0, strrpos($request->fileCover->getClientOriginalName(), "."));
		$path = "/uploads/" . date("Y") . '/' . date("m") . "/";
		$filenamewithoutext = $file.'_'.rand();
        $imageName = $filenamewithoutext.'.'.$request->fileCover->getClientOriginalExtension();
        $upload_success = $request->fileCover->move(public_path($path), $imageName);
		if( $upload_success ) {
			$upload = new Upload;
			$upload->type = $request->fileCover->getClientOriginalExtension();
			$upload->description = '';
			$upload->title = $file;
			$upload->slug = $path.$imageName;
			$upload->save();

			$img = Image::make(public_path($path).$imageName);
			$img->resize(190, 190);
			$img->save(public_path($path).$filenamewithoutext.'_thumb'.'.'.$request->fileCover->getClientOriginalExtension());
		}


        return "<html><body onload=\"parent.applyBoxImage('" . $path . "$imageName" . "')\"></body></html>";
	}
	public function show($id)
    {

	}
	public function saveimage(Request $request)
    {
		header('Cache-Control: no-cache, must-revalidate');
		$path = "/" . date("Y") . '/' . date("m") . "/";
        $count = (int)$request->get('count');
        $b64str = $request->get('hidimg-'.$count);
        $imgname = 'image'; //$request->get('hidname-'.$count)
        $imgtype = $request->get('hidtype-'.$count);
        //Generate random file name here
        if ($imgtype == 'png') {
            $image = $request->get('hidname-'.$count).'-'.base_convert(rand(), 10, 36).'.png';
        } else {
            $image = $imgname.'-'.base_convert(rand(), 10, 36).'.jpg';
        }

        // Save image
        Storage::disk('uploads')->put($path.$image, base64_decode($b64str));
		$url = url('/uploads'.$path.$image);
		echo "<html><body onload=\"parent.document.getElementById('img-".$count."').setAttribute('src','".$url."');  parent.document.getElementById('img-".$count."').removeAttribute('id') \"></body></html>";
	}
	public function savehtml(Request $request)
    {
		$page = Pages::find($request->pageid);
		$page->html = $request->hidContent;
		$page->save();
		//saveHTML($page->id);
		return Redirect::back()->with('success', 'Page successfully updated.');
	}
	public function destroy($id)
    {
		$page = Pages::find($id);
        $page->delete();
		return Redirect::back()->with('success', 'Page successfully trashed.');
	}
}
