<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Categories;
use App\theme;
use App\Settings;
use App\Upload;
use Image;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkhost');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $uploads = Upload::simplePaginate(15);
      return view('admin.categories', ['categories' => Categories::All(), 'uploads' => $uploads]);
    }
    public function trash()
      {
        $uploads = Upload::simplePaginate(15);
          return view('admin.categories', ['categories' => Categories::onlyTrashed()->get(), 'uploads' => $uploads, 'trash' => true]);
      }
      public function restore($id)
        {
    		$currenttheme = theme::getCurrentTheme();
    		$category = Categories::onlyTrashed()->where('id', $id)->first();
    		$category->restore();
    		return Redirect::back()->with('success', 'Category restored.');
    	}
      public function clone($id)
        {
    		$currenttheme = theme::getCurrentTheme();
    		$category = Categories::find($id);
        $duplicate = $category->replicate();
        $duplicate->title  = $category->title . " - Copy";
        $duplicate->slug  = $category->slug . "-".$category->id;
        $duplicate->save();
    		return Redirect::back()->with('success', 'Category Cloned.');
    	}
    	public function forceDelete($id)
        {
    		$currenttheme = theme::getCurrentTheme();
    		$category = Categories::onlyTrashed()->where('id', $id)->first();
    		$category->forceDelete();
    		return Redirect::back()->with('success', 'Category permanently deleted.');
    	}
      public function store(Request $request)
        {
          if($request->catid){
      			$category = Categories::find($request->catid);
      			$rules = [
      					'title' => 'required',
      					'slug' => 'required|regex:/^[a-z0-9\-]+$/u|unique:categories,slug,'.$category->id
      			];
      		}else{
      			$category = new Categories;
      			$category->description = '';
      			$category->category_image = '';
      			$rules = [
      					'title' => 'required',
      					'slug' => 'required|regex:/^[a-z0-9\-]+$/u|unique:categories,slug'
      			];
      		}

      		$messages = [
      			'title.required' => 'Please enter a title',
      			'slug.required' => 'Please enter a slug',
      			'slug.unique' => 'Category already exists',
      			'slug.regex' => 'Should Contain only a-z,0-9 and -',
      		];
      		$validator = Validator::make($request->all(), $rules, $messages);
      		if ($validator->fails()){
      			return json_encode(array('errors' => $validator->getMessageBag()->toArray()));
      		}else{
      			$category->title = $request->title;
      			$category->slug = $request->slug;
      			$category->description = $request->description;
      			$category->category_image = $request->category_image;
            
            if(empty($category->description)){
      				$category->description = '';
      			}
      			if(empty($category->category_image)){
      				$category->category_image = '';
      			}

      			$category->save();
      			//saveHTML($category->id);
      			return 'success';
      		}
        }
        public function destroy($id)
          {
      		$category = Categories::find($id);
              $category->delete();
      		return Redirect::back()->with('success', 'Category successfully trashed.');
      	}
}
