<?php

namespace App\Http\Controllers;
use App\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Support\Facades\Redirect;
class MediaController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
		$this->middleware('checkhost');
    }
    public function index(Request $request)
    {
			$uploads = Upload::orderBy('id', 'desc')->simplePaginate(8);
			return view('admin.media', ['uploads' => $uploads]);
    }
   public function upload(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
		$file = substr($request->file->getClientOriginalName(), 0, strrpos($request->file->getClientOriginalName(), "."));
		$path = "/uploads/" . date("Y") . '/' . date("m") . "/";
		$filenamewithoutext = $file.'_'.rand();
        $imageName = $filenamewithoutext.'.'.$request->file->getClientOriginalExtension();
        $upload_success = $request->file->move(public_path($path), $imageName);
		if( $upload_success ) {
			$upload = new Upload;
			$upload->type = $request->file->getClientOriginalExtension();
			$upload->description = '';
			$upload->title = $file;
			$upload->slug = $path.$imageName;
			$upload->save();

			$img = Image::make(public_path($path).$imageName);
			$img->fit(190);
			$img->save(public_path($path).$filenamewithoutext.'_thumb'.'.'.$request->file->getClientOriginalExtension());
			ob_start();
			$rand = rand();
			?>
				<a href="javascript:void(0)" data-toggle="custommodal" data-target="#thumbnail-modal-<?php echo $rand ?>"><img src="<?php echo $path.$filenamewithoutext.'_thumb'.'.'.$request->file->getClientOriginalExtension() ?>"></a>
				<div class="md-custom-modal md-custom-effect-1" id="thumbnail-modal-<?php echo $rand ?>">
					<div class="md-custom-content">
							<h3>Media Detail</h3>
							<div class="md-custom-body">


								<div class="row" id="imageedit-<?php echo $rand ?>">
									<div class="col-sm-8 text-center">
										<div class="imageedit">
											<img src="<?php echo $path.$imageName ?>" id="image-<?php echo $rand ?>" class="cropper" style="position:relative;top:0;transform:none">
										</div>
										<p>&nbsp;</p>
									</div>
									<div class="col-sm-4">
										<div class="details">
											File name: <?php echo $upload->title ?><br />
											File type: <?php echo $upload->type ?><br />
											Uploaded on: Just Now<br />
											File size: <?php echo getFileSize($path.$imageName) ?><br />
											Dimensions: <?php echo getImageDimension($path.$imageName) ?><br />
											<hr /><br />
											<table class="table-responsive">
												<tr>
													<td>
													<div class="input-group">
														<span class="input-group-addon">URL</span>
														<input type="text" class="form-control" value="<?php echo $path.$imageName ?>" readonly>
													</div>
													</td>
												</tr>
												<tr>
													<td>
													<p>&nbsp;</p>
													<p><a href="#" data-imageid="image-<?php echo $rand ?>" data-image="<?php echo $path.$imageName ?>" data-imageholder="imageedit-<?php echo $rand ?>" class="btn btn-sm btn-info editimage">Edit</a></p>
													</td>
												</tr>
											</table>
										</div>
										<div class="editdetail" style="display:none;padding: 15px;">
												<div class="card-box">
													<div class="preview preview-lg"></div>
													<div class="preview preview-md"></div>
													<div class="preview preview-sm"></div>
													<div class="clearfix"></div>
												</div>
												<div class="card-box">
													<ul class="buttons">
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="zoom" data-option="0.1"><span class="fa fa-search-plus"></span></a></li>
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="zoom" data-option="-0.1"><span class="fa fa-search-minus"></span></a></li>
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="moveH" data-option="-10"><span class="fa fa-arrow-left"></span></a></li>
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="moveH" data-option="10"><span class="fa fa-arrow-right"></span></a></li>
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="moveV" data-option="-10"><span class="fa fa-arrow-up"></span></a></li>
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="moveV" data-option="10"><span class="fa fa-arrow-down"></span></a></li>
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="rotate" data-option="-45"><span class="fa fa-rotate-left"></span></a></li>
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="rotate" data-option="45"><span class="fa fa-rotate-right"></span></a></li>
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="scaleX" data-option="-1"><span class="fa fa-arrows-h"></span></a></li>
														<li><a href="#" data-imageid="image-<?php echo $rand ?>" data-method="scaleY" data-option="-1"><span class="fa fa-arrows-v"></span></a></li>
													</ul>
												</div>
												<div class="card-box">

													<div class="row">
														<div class="col-lg-6 m-t-10">
															<div class="input-group">
															<span class="input-group-addon">X</span>
															<input class="form-control dataX" type="text" placeholder="x">
															</div>
														</div>
														<div class="col-lg-6 m-t-10">
															<div class="input-group">
																<span class="input-group-addon">Y</span>
																<input class="form-control dataY" type="text" placeholder="y">
																</div>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6 m-t-10">
															<div class="input-group">
																<span class="input-group-addon">W</span>
																<input class="form-control dataWidth" type="text" placeholder="width">
																</div>
														</div>
														<div class="col-lg-6 m-t-10">
															<div class="input-group">
																<span class="input-group-addon">H</span>
																<input class="form-control dataHeight" type="text" placeholder="height">
																</div>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6 m-t-10">
															<div class="input-group">
																<span class="input-group-addon">Rotate</span>
																<input class="form-control dataRotate" type="text" placeholder="rotate">
																</div>
														</div>

													</div>

												</div>
												<a href="#" data-imageid="image-<?php echo $rand ?>" data-image="{{ getImage($upload->slug,'') }}" data-imageholder="imageedit-<?php echo $rand ?>" class="btn btn-sm btn-danger canceledit">Cancel</a>&nbsp;<button data-imageid="image-<?php echo $rand ?>" data-image="{{ getImage($upload->slug,'') }}" data-imageholder="imageedit-<?php echo $rand ?>" class="btn btn-sm btn-danger saveimg">Save</button>

										</div>
									</div>
								</div>


							</div>
							<div class="md-custom-footer">
							<button class="md-custom-close">Close me!</button>
							</div>
					</div>
				</div>
				<div data-target="#thumbnail-modal-<?php echo $rand ?>" class="md-overlay md-custom-overlay"></div>
			<?php
			return ob_get_clean();
        } else {
        	echo 'fail';
        }
    }
	public function modalupload(Request $request){
		$this->validate($request, [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
		$file = substr($request->file->getClientOriginalName(), 0, strrpos($request->file->getClientOriginalName(), "."));
		$path = "/uploads/" . date("Y") . '/' . date("m") . "/";
		$filenamewithoutext = $file.'_'.rand();
        $imageName = $filenamewithoutext.'.'.$request->file->getClientOriginalExtension();
        $upload_success = $request->file->move(public_path($path), $imageName);
		if( $upload_success ) {
			$upload = new Upload;
			$upload->type = $request->file->getClientOriginalExtension();
			$upload->description = '';
			$upload->title = $file;
			$upload->slug = $path.$imageName;
			$upload->save();

			$img = Image::make(public_path($path).$imageName);
			$img->fit(190);
			$img->save(public_path($path).$filenamewithoutext.'_thumb'.'.'.$request->file->getClientOriginalExtension());
			ob_start();
			$rand = rand();
			?>
			<a href="<?php echo $path.$imageName ?>"><img src="<?php echo $path.$filenamewithoutext.'_thumb'.'.'.$request->file->getClientOriginalExtension() ?>"></a>
			<?php
			return ob_get_clean();
        } else {
        	echo 'fail';
        }
	}
	public function select()
    {
        return view('admin.mediapopup', ['preview'=> false, 'uploads' => Upload::All()]);
    }
	public function delete($id)
    {
		$media = Upload::findOrFail($id);
		$media->delete();
        return Redirect::back()->with('success', $media->title.' has been deleted.');
    }
	public function croppedupload(Request $request)
    {
				$path = "/" . date("Y") . '/' . date("m") . "/";
				$image = 'cropped'.'-'.base_convert(rand(), 10, 36);
				$img = $request->image;
				$image_parts = explode(";base64,", $img);
				$image_type_aux = explode("image/", $image_parts[0]);
				$image_type = $image_type_aux[1];
				$data = base64_decode($image_parts[1]);
				Storage::disk('uploads')->put($path.$image.'.png', $data);
				Storage::disk('uploads')->put($path.$image.'_thumb.png', $data);
				$upload = new Upload;
				$upload->type = $image_type;
				$upload->description = '';
				$upload->title = $image;
				$upload->slug = '/uploads'.$path.$image.'.png';
				$upload->save();
				return 'success';
    }
}
