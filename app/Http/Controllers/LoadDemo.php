<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Pages;
use App\Post;
use App\Widget;
use App\Settings;
use App\Menu;
use App\Upload;
use App\Buttons;
use App\Popup;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
class LoadDemo extends Controller
{
    public function __construct()
    {

    }
	public function LoadDemo()
    {
		return view('admin.demos');
    }
	public function CreateDemo()
    {
		$posts = Post::All();
		$pages = Pages::All();
		$widgets = Widget::All();
		$settings = Settings::All();
		$menus = Menu::All();
		$uploads = Upload::All();
		$buttons = Buttons::All();
		$popups = Popup::All();

		$contents = view('admin.xml.demoxml', ['posts' => $posts, 'pages' => $pages, 'widgets' => $widgets, 'buttons' => $buttons, 'popups' => $popups, 'settings'=>$settings, 'menus'=>$menus, 'uploads'=>$uploads]);
		$contents = '<?xml version="1.0" encoding="UTF-8"?>'.$contents;
		file_put_contents(public_path('/data.xml'), $contents);
		return view('admin.xml.demoxml', ['posts' => $posts, 'pages' => $pages, 'widgets' => $widgets, 'settings'=>$settings, 'menus'=>$menus, 'uploads'=>$uploads]);
		//return response(view('admin.xml.demoxml', ['posts' => $posts, 'pages' => $pages, 'widgets' => $widgets]), '200')->header('Content-Type', 'text/xml');
    }
	 public function importDemo(Request $request){
      $appurl = $request->url;
	  $url = 'http://admin.duktix.com/demos/'.$request->host.'.xml';
      $data = simplexml_load_file($url);
      if(!empty($data->posts->post)){
          foreach($data->posts->post as $key => $value) {
			$existing = Post::where('slug', $value->slug)->first();
			if($existing){
				$existing->slug = $existing->slug.'_copy';
				$existing->save();
			}
            $post = new Post;
            $post->title = $value->title;
            $post->slug = $value->slug;
            $post->html = $value->htmlcontent;
            $post->meta_title = $value->meta_title;
            $post->meta_description = $value->meta_description;
            $post->meta_keywords = $value->meta_keywords;
            $post->custom_css = $value->custom_css;
            $post->sidebar = $value->sidebar;
            $post->featured_image = $value->featured_image;
            $post->excerpt = $value->excerpt;

            $post->save();
          }
      }
      if(!empty($data->pages->page)){
          foreach($data->pages->page as $key => $value) {
			  $existing = Pages::where('slug', $value->slug)->first();
              if($existing){
                $existing->slug = $existing->slug.'_copy';
                $existing->save();
              }
              $page = new Pages;
              $page->title = $value->title;
        	  $page->slug = $value->slug;
              $page->html = $value->htmlcontent;
              $page->meta_title = $value->meta_title;
              $page->meta_description = $value->meta_description;
              $page->meta_keywords = $value->meta_keywords;
              $page->custom_css = $value->custom_css;
              $page->sidebar = $value->sidebar;
              $page->save();
          }
      }
      if(!empty($data->widgets->widget)){
          foreach($data->widgets->widget as $key => $value) {
              $existing = Widget::find($value->id);
              if($existing){
                $rand = rand(0, 999999);
                $existing->id = $rand;
                $existing->shortcode = '[widget type="'.$existing->type.'" id="'.$existing->id.'"]';
                $existing->save();
              }
              $widget = new Widget;
              $widget->id = $value->id;
              $widget->setting = trim($value->setting);
        	  $widget->shortcode = $value->shortcode;
              $widget->title = $value->title;
              $widget->type = $value->type;
              $widget->save();
          }
      }
	  if(!empty($data->buttons->button)){
          foreach($data->buttons->button as $key => $value) {
              $existing = Buttons::find($value->id);
              if($existing){
                $rand = rand(0, 999999);
				$existing->name = $existing->name.'_copy';
                $existing->id = $rand;
                $existing->shortcode = str_replace('id="'.$value->id.'"', 'id="'.$existing->id.'"', $value->shortcode);
                $existing->save();
              }
              $button = new Buttons;
              $button->id = $value->id;
              $button->setting = trim($value->setting);
        	  $button->shortcode = $value->shortcode;
              $button->name = $value->name;
              $button->save();
          }
      }
	  if(!empty($data->popups->popup)){
          foreach($data->popups->popup as $key => $value) {
			  $existing = Popup::where('class', $value->class)->first();
              if($existing){
                $existing->class = $existing->class.'_copy';
                $existing->save();
              }
              $popup = new Popup;
              $popup->title = $value->title;
        	  $popup->class = $value->class;
              $popup->html = $value->htmlcontent;
              $popup->settings = $value->settings;
              $popup->save();
          }
      }
      if(!empty($data->menus->menu)){
          foreach($data->menus->menu as $key => $value) {
              $existing = Menu::find($value->id);
              if($existing){
                $rand = rand(0, 999999);
                $existing->id = $rand;
                $existing->save();
              }
              $menu = new Menu;
              $menu->id = $value->id;
              $menu->name = $value->name;
        	  $menu->menu = trim($value->menu);
              $menu->order = trim($value->order);
              $menu->save();
          }
      }

	  if(!empty($data->uploads->upload)){
          foreach($data->uploads->upload as $key => $value) {
              $upload = new Upload;
              $upload->title = $value->title;
        	  $upload->description = $value->description;
              $upload->slug = $value->slug;
			  $upload->type = $value->type;

			  $imagename = basename($value->slug);
			  $ext = substr($imagename,strrpos($imagename,'.',-1),strlen($imagename));
			  $name = str_replace($ext, '', $imagename);

			  $image = file_get_contents(str_replace(' ', '%20', $value->downloadurl));
			  $rootPath = public_path(str_replace($imagename, '', $value->slug));
			  $client = Storage::createLocalDriver(['root' => $rootPath]);
			  $client->put($imagename, $image);

			  $thumb = $name.'_thumb'.$ext;
			  $image = file_get_contents(str_replace(' ', '%20', $appurl.str_replace($imagename,'',$value->slug).$thumb));
			  $rootPath = public_path(str_replace($imagename, '', $value->slug));
			  $client = Storage::createLocalDriver(['root' => $rootPath]);
			  $client->put($thumb, $image);

              $upload->save();
          }
      }
	  if(!empty($data->settings->setting)){
          foreach($data->settings->setting as $key => $value) {
				foreach(json_decode($value->setting) as $key=>$request){
					$inputs[$key] = $request;
				}
				$settings = Settings::updateOrCreate(
					['type' => $value->type],
					['type' => $value->type, 'setting' => serialize($inputs)]
				);
          }
      }
	return Redirect::back()->with('success', 'Import Success!! Please update all settings to see changes');
  }
}
