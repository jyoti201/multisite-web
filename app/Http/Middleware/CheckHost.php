<?php

namespace App\Http\Middleware;

use Closure;
use App\Settings;
use App\theme;
use Illuminate\Support\Facades\Route;

class CheckHost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    		$hostsettings = Settings::where('type', "host")->first();
    		if(empty($hostsettings)){
    			return redirect('/start');
    		}
    		$currenttheme = theme::getCurrentTheme();
    		if(empty($currenttheme) && Route::currentRouteName()!='themes.index' && Route::currentRouteName()!='themes.store'){
    			return redirect()->route('themes.index')->with('error', 'No Theme is Active!! Please activate a Theme.');
    		}
        return $next($request);
    }
}
