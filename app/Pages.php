<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Pages extends Model
{
	use SoftDeletes;
	protected $fillable = [
        'site_id', 'title', 'slug', 'html', 'meta_title', 'meta_description', 'meta_keywords', 'custom_css'
    ];
	protected $dates = ['deleted_at'];
}
