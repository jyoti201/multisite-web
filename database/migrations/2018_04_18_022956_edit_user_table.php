<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
    			$table->string('dob');
    			$table->string('phone');
    			$table->text('avatar');
          $table->boolean('verified')->default(false);
          $table->boolean('active')->default(false);
    		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'dob'))
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->dropColumn('dob');
            });
        }
        if (Schema::hasColumn('users', 'phone'))
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->dropColumn('phone');
            });
        }
        if (Schema::hasColumn('users', 'avatar'))
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->dropColumn('avatar');
            });
        }
        if (Schema::hasColumn('users', 'verified'))
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->dropColumn('verified');
            });
        }
        if (Schema::hasColumn('users', 'verified'))
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->dropColumn('active');
            });
        }
    }
}
