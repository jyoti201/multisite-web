<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
			$table->text('title');
			$table->text('slug');
			$table->text('html');
			$table->string('sidebar');
			$table->text('meta_title');
			$table->text('meta_description');
			$table->text('meta_keywords');
			$table->text('custom_css');
			$table->text('featured_image');
			$table->text('excerpt');
            $table->timestamp('created_at')->useCurrent();
			$table->timestamp('updated_at')->nullable();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
