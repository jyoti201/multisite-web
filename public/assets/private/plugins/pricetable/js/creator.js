var pause_menu = {
    pause_button : { someProperty : "prop1", someOther : "prop2" },
    resume_button : { resumeProp : "prop", resumeProp2 : false },
    quit_button : false
};
var url = {
	first:{
		ptc_1: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_2: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_3: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_4: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_5: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_6: { text: 'SIGN UP', link: '', target: '_self'}
	}, second: {
		ptc_1: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_2: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_3: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_4: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_5: { text: 'SIGN UP', link: '', target: '_self'},
		ptc_6: { text: 'SIGN UP', link: '', target: '_self'}
	}
};

var icons = {
	ptc_1: 'fa fa-times',
    ptc_2: 'fa fa-cog',
    ptc_3: 'fas fa-trash-alt',
    ptc_4: 'fa fa-plus',
    ptc_5: 'fa fa-bus',
    ptc_6: 'fa fa-car'
};
var stars = {
    ptc_1: 1,
    ptc_2: 2,
    ptc_3: 3,
    ptc_4: 4,
    ptc_5: 5,
    ptc_6: 6
};

$(document).ready(function () {
    if($('.ptc-table').length==0){
      currentItems = 3;
    }
    var cur = 12/$('.ptc-table').length;
    var classes = "col-md-"+cur+" col-xs-12";
    if(cur==3){
      classes +=' col-sm-6';
    }else if (cur==5) {
      classes +=' col-sm-4';
    }
    var responsive = [
        classes,
        "col-lg-12 col-md-12 col-sm-12 col-xs-12",
        "col-lg-6 col-md-6 col-sm-6 col-xs-12",
        "col-lg-4 col-md-4 col-sm-4 col-xs-12",
        "col-lg-3 col-md-3 col-sm-6 col-xs-12",
        "col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-2-4 ptc-small",
        "col-lg-2 col-md-4 col-sm-6 col-xs-12 ptc-smaller"
    ];

    var titles = [
            '',
            "FREE",
            "BASIC",
            "STANDARD",
            "BUSINESS",
            "PREMIUM",
            "PROFESSIONAL"
        ];

    var schemes = [
        {
            "name": "original",
            "colors": [
                "#068894",
                "#e84c3d",
                "#ebaf3c",
                "#736357",
                "#514051",
                "#222122"
            ]
        },
        {
            "name": "autumn",
            "colors": [
				"#9b5651",
				"#ae695a",
				"#e89c84",
				"#f5c972",
				"#eab06e",
				"#c3b274"
            ]
        },
        {
            "name": "retro",
            "colors": [
				"#7c786a",
				"#8cccc0",
				"#bfd174",
				"#fae46f",
				"#f97a6e",
				"#e6bcd8"
            ]
        },
        {
            "name": "sunset",
            "colors": [
				"#f37257",
				"#f68d5c",
				"#f4d27a",
				"#afc1cc",
				"#7895a2",
				"#517281"
            ]
        },
        {
            "name": "candy",
            "colors": [
				"#fc7b77",
				"#ff9996",
				"#feb7b5",
				"#fecea9",
				"#a0d5e0",
				"#7fcfe0"
            ]
        },
		{
            "name": "ocean",
            "colors": [
				"#159594",
				"#09aea7",
				"#a9cf9e",
				"#c7a885",
				"#fe7040",
				"#8a697a"
            ]
        },
		{
            "name": "earth",
            "colors": [
				"#828373",
				"#656f66",
				"#7aada6",
				"#b7bcb6",
				"#4e3d36",
				"#323232"
            ]
        },
		{
            "name": "modern",
            "colors": [
				"#2e2e31",
				"#4e4e56",
				"#da635d",
				"#c4b9ab",
				"#b1938b",
				"#81665f"
            ]
        }
    ];






    initSchemes();
	initFonts();

    function init() {

        $('.item-options').hide();
        clicked = false;
        initTables();
        initFeatures();
        initSorting();
        toggleMenu();
        initStars();
        initTitles();

    };

    function initTitles(){
        for (i = 1; i <= currentItems; i++) {
            $('#ptc_'+i).find('.ptc-title').html(titles[i]);
        }
    }

    function initTables() {
        for (i = 1; i <= currentItems; i++) {
            $('.ptc-tables').append("<div id='ptc_" + i + "' datacolor='" + schemes[currentScheme].colors[i - 1] + "' class='ptc-table col-lg-2 col-md-4 col-sm-6 col-xs-12'>" + templates[newTemplate].html + "</div>");
        }

        updateTemplate();
        updateFeatures();
    };

    function initSchemes() {
        for (i = 0; i < schemes.length; i++) {
            $('.owl-schemes').append("<div class='item-colors'>"+insertColors(i)+"</div>");
        }
        $('.ptc-tables').addClass(schemes[0].name);
    };

	function initFonts() {
        for (i = 0; i < fonts.length; i++) {
            $('.owl-fonts').append("<div style='font-family: "+fonts[i].name+" '>"+fonts[i].name+"</div>");
        }
        $('.ptc-tables').css( { 'font-family': fonts[currentFont].name, 'font-size': fonts[currentFont].size }  );

    };


	function insertColors(i){

		var colors = '';
		for (j = 0; j <= 5; j++) {
			var color = '<div class="color-square" style="background:' + schemes[i].colors[j] + ';"></div>';
			colors = colors + color;
		}

		return colors;
	}

    function initFeatures() {
        for (i = 1; i <= currentFeatures; i++) {
            $('.ptc-features').append("<li class='ptc-feature'><a href='#' contenteditable='true' spellcheck='false'>FEATURE</a></li>");
        }
        $('.ptc-feature').append('<div class="remove-feature"><i class="fas fa-trash-alt"></i></div>');
    }

    function initStars() {
        for (i = 1; i <= currentItems; i++) {
            for (j = 1; j <= i; j++) {
                $('#ptc_' + i).find('.ptc-stars').append('<span class="fa fa-star"></span>');
            }
        }
    }


    $(document).on('mouseenter', '.ptc-feature', function () {
        var index = parseInt($(this).index());
        $('.ptc-feature:nth-child(' + (index + 1) + ') .remove-feature').css('opacity', 1);
    });

    $(document).on('mouseleave', '.ptc-feature', function () {
        var index = parseInt($(this).index());
        $('.ptc-feature:nth-child(' + (index + 1) + ') .remove-feature').css('opacity', 0);
    });

    $(document).on('click', '.ptc-tables', function () {
        clicked = true;
    });


    //Add item
    $(document).on('click', '.btn-add', function () {
        var freeID;
        currentItems = $('.ptc-table').length;
        if (currentItems < 6) {
            currentItems++;
            for (i = 1; i <= 6; i++) {
                if (!$('#ptc_' + i).length) {
                    freeID = i;
                    break;
                }
            }
            $('.ptc-tables').append("<div id='ptc_" + freeID + "' class='ptc-table col-lg-2 col-md-4 col-sm-6 col-xs-12'>" + templates[newTemplate].html + "</div>");

			updateTemplate();

		   for (i = 1; i <= currentFeatures; i++) {
                $('#ptc_' + freeID).find('.ptc-features').append("<li class='ptc-feature'><a href='#' contenteditable='true' spellcheck='false'>FEATURE</a></li>");
            }
            $('#ptc_' + freeID).attr('datacolor', schemes[currentScheme].colors[freeID - 1]);

            for (i = 1; i <= stars['ptc_' + freeID]; i++) {
                $('#ptc_' + freeID).find('.ptc-stars').append('<span class="fa fa-star"></span>');
            }

            changeCurrency();
            changePer();
            updateFeatures();

        }
    });


	$(document).on('click', '.btn-generate', function () {

		$.prompt("Code generator is available after purchase. !");

	});



    //Remove item
    $(document).on('click', '.remove-item', function () {

        var elem = $(this);
        var itemToRemove = '#' + elem.closest('[id]').attr('id');

        zoomedItems.splice($.inArray(itemToRemove, zoomedItems), 0);

        elem.closest('.ptc-table').remove();
        currentItems--;
		$('.item-options').hide();

        updateTemplate();

    });

    //Remove feature
    $(document).on('click', '.remove-feature', function () {
        $(this).parent('li.ptc-feature').remove();
    });

    //Add feature
    $(document).on('click', '.btn-feature', function () {
        var selected_exists = false;
        var selected_id = '';
        $('.ptc-table').each(function(){
            if($(this).hasClass('selected')){
              selected_exists = true;
              selected_id = $(this).attr('id');
            }
        })
        if(selected_exists===true){
            $('#'+selected_id).find('.ptc-features').append("<li class='ptc-feature'><a href='#' contenteditable='true' spellcheck='false'>FEATURE</a></li>");
        }else{
            $('.ptc-features').append("<li class='ptc-feature'><a href='#' contenteditable='true' spellcheck='false'>FEATURE</a></li>");
            currentFeatures++;
            updateFeatures();
        }
    });

    function updateFeatures() {
        $('.remove-feature').remove();
        $('.ptc-feature').each(function(){
          $(this).append('<div class="remove-feature"><i class="fas fa-trash-alt"></i></div>');
        })
    };

    //Count Items and update responsiveness
    function updateTemplate() {
        var $item = $('.ptc-table');
        currentItems = $('.ptc-table').length;
        $item.removeClass();
        $item.addClass('ptc-table ' + responsive[currentItems]);
        $('.remove-item').remove();
        $item.append('<div class="remove-item"><i class="fas fa-trash-alt"></i></div>');

        $.each(zoomedItems, function (index, value) {
            $(value).addClass('ptc-zoom');
        });

		if(button_1 == false){
			$('.ptc-price .ptc-button').addClass('ptc-hide');
		}

		if(button_2 == false){
			$('.ptc-footer .ptc-button').addClass('ptc-hide');
		}

		if(spaces == false){
			$('.ptc-table').addClass('ptc-no-space');
		}

        if(hover == true){
            $('.ptc-table').addClass(currentHover);
            $('.ptc-tables').find('.ptc-zoom').removeClass(currentHover);
        }

    };

    function changeTemplate() {
        $('.ptc-tables').html('');
        zoomedItems = [];
        init();
        $('.ptc-tables').removeClass( templates[currentTemplate].name.toLowerCase() );
        $('.ptc-tables').addClass( templates[newTemplate].name.toLowerCase() );
        currentTemplate = newTemplate;

    };

    function changeCurrency() {
        $('.ptc-currency').html(currentCurrency);
    };

    function changeHover() {
        $('.ptc-table').removeClass(currentHover);
        $('.ptc-table').addClass(newHover);
        currentHover = newHover;
    };

    function changePer() {
        $('.ptc-text').html(currentPer);
    };

    function changeScheme() {
        var scheme = schemes[currentScheme].name;
        $tables = $('.ptc-tables');
        $tables.removeClass();
        $tables.attr('class', 'ptc-tables ' + scheme +' '+templates[currentTemplate].name.toLowerCase() );
    }

    //Side Menu ( Show/Hide )
    $(document).on('click', '.toggle', toggleMenu);


    function toggleMenu() {

        $('.ptc-tables').toggleClass('flex');
        $('body').toggleClass('squeeze');
        $('aside').toggleClass('show');
        if (sortingCtrl == 1) {
            $("#sortable").sortable("enable");
            $('body').removeClass('preview-mode');
            $(".mode").html("EDIT MODE");
            $("#sortable").enableSelection();
            $(".remove-item, .remove-feature, .selected-item, .handle").show();
            sortingCtrl = 0;
        } else {
            $("#sortable").sortable("disable");
            $('body').addClass('preview-mode');
            $(".mode").html("PREVIEW MODE");
            $("#sortable").disableSelection();
            $(".remove-item, .remove-feature, .selected-item, .handle").hide();
            sortingCtrl = 1;
        }
    };

    init();

    $('.note').on('ifChecked', function(){
        $('#' + currentId + ' .ptc-note').removeClass('ptc-hide');
        $('#' + currentId + ' .ptc-note').addClass('ptc-show');
        $('.icon-set').show();
    });


    $('.note').on('ifUnchecked', function(){
        $('#' + currentId + ' .ptc-note').removeClass('ptc-show');
        $('#' + currentId + ' .ptc-note').addClass('ptc-hide');
        $('.icon-set').hide();
    });

    $('.zoom').on('ifUnchecked', function(){
        var itemToRemove = '#' + currentId;
        $('#' + currentId).removeClass('ptc-zoom');
        zoomedItems.splice($.inArray(itemToRemove, zoomedItems), 0);
    });

    $('.zoom').on('ifChecked', function(){
        $('#' + currentId).removeClass(currentHover);
        $('#' + currentId).addClass('ptc-zoom');
        zoomedItems.push('#' + currentId);
    });

    $('.hover-effect').on('ifChecked', function(){
        $('.hover-select').show();
        hover = true;
    });

    $('.hover-effect').on('ifUnchecked', function(){
        $('.hover-select').hide();
        $('.ptc-table').removeClass(currentHover);
        hover = false;
    });


    $('.button-1').on('ifChecked', function(){
        $('.ptc-price .ptc-button').removeClass('ptc-hide');
        $('.ptc-price .ptc-button').addClass('ptc-show-inline');
        button_1 = true;
    });

    $('.button-1').on('ifUnchecked', function(){
        $('.ptc-price .ptc-button').removeClass('ptc-show-inline');
        $('.ptc-price .ptc-button').addClass('ptc-hide');
        button_1 = false;
    });

    $('.button-2').on('ifChecked', function(){
        $('.ptc-footer .ptc-button').removeClass('ptc-hide');
        $('.ptc-footer .ptc-button').addClass('ptc-show-inline');
        button_2 = true;
    });

    $('.button-2').on('ifUnchecked', function(){
        $('.ptc-footer .ptc-button').removeClass('ptc-show-inline');
        $('.ptc-footer .ptc-button').addClass('ptc-hide');
        button_2 = false;
    });

	$('.spacing').on('ifChecked', function(){
        $('.ptc-table').addClass('ptc-no-space');
		spaces = false;
    });

    $('.spacing').on('ifUnchecked', function(){
        $('.ptc-table').removeClass('ptc-no-space');
		spaces = true;
    });



    function initSorting() {

        var $sortItem = $(".ptc-tables");

        $sortItem.sortable(
            {
                appendTo: '.ptc-tables',
                items: ".ptc-table",
                opacity: 0.8,
                tolerance: "pointer",
                forceHelperSize: true,
                forcePlaceholderSize: true,
                handle: ".handle",
                helper: "original",
                scroll: false,
                axis: "x",
                cancel: ".remove",
                update: function (event, ui) {

                }
            }
        );

    }

	$(document).on('click', '.ptc-button', function () {


			var elemID = $(this).closest('[id]').attr('id');
			var $elemID = $(this).closest('[id]');
			var parent = $(this).parent().attr('class');
			var button;
			if(parent.contains('ptc-price')){
				button = 'first';
			}else{
				button = 'second';
			}

            $.prompt("<input type='text' name='button_text' placeholder='text' value='"+url[button][elemID].text+"' autofocus></input><input type='text' name='button_url' placeholder='http://...' value='"+url[button][elemID].link+"'></input><input type='radio' name='groupTarget' value='_self'><label>current window</label><input type='radio' name='groupTarget' value='_blank'><label>new window</label> ", {
				title: 'BUTTON URL',
				defaultButton: 1,
				buttons: { Ok : true },
                focus: 1,
                opacity: 0.84,
                overlayspeed: 'fast',
                submit: function (e, v, m, f) {
					url[button][elemID].text = $('.jqimessage input[name ="button_text"]').val();
					url[button][elemID].link = $('.jqimessage input[name ="button_url"]').val();
					url[button][elemID].target = $("input:radio[name ='groupTarget']:checked").val();

					if(parent.contains('ptc-price')){
						$elemID.find('.ptc-price').find('.ptc-button').attr( {'href': url[button][elemID].link, 'target': url[button][elemID].target} );
						$elemID.find('.ptc-price').find('.ptc-button').text( url[button][elemID].text );
					}else{
						$elemID.find('.ptc-footer').find('.ptc-button').attr( {'href': url[button][elemID].link, 'target': url[button][elemID].target} );
						$elemID.find('.ptc-footer').find('.ptc-button').text( url[button][elemID].text );
					}


                }
            }).on('impromptu:loaded', function(e){
					if( url[button][elemID].target == "_self"){
						$("input:radio[name ='groupTarget']").filter('[value=_self]').prop('checked', true);
					}else{
						$("input:radio[name ='groupTarget']").filter('[value=_blank]').prop('checked', true);
					}

			});

    });



    $(document).on('click', '.template .sbToggle, .template .sbSelector', function () {

        $('#template_select').selectbox("close");
        $('.nav-content').hide();

        if (clicked) {
            $.prompt("If you change the template you lose your current settings. Continue?", {
                title: 'NOTICE',
				buttons: { Cancel: false, Continue: true},
                focus: 1,
                opacity: 0.84,
                overlayspeed: 'fast',
                submit: function (e, v, m, f) {
                    if (v) {
                        $('.nav-content').show();
                        $('#template_select').selectbox("open");
                    } else {
                        $('#template_select').selectbox("close");
                    }
                }
            });
        } else {
            $('.nav-content').show();
            $('#template_select').selectbox("open");
        }

    });

	$('#scrollbox').enscroll({
		showOnHover: true,
		verticalTrackClass: 'track',
		verticalHandleClass: 'handler',
        easingDuration: 200
	});

	$('.nav-content').enscroll({
		showOnHover: true,
		verticalTrackClass: 'track-content',
		verticalHandleClass: 'handler',
        easingDuration: 200
	});


    $("#template_select").selectbox({
        onChange: function (val, inst) {
            newTemplate = val;
            changeTemplate();
        },
        speed: 0
    });

	$("#font_select").selectbox({
        onChange: function (val, inst) {
            currentFont = val;

        },
        speed: 0
    });

    $("#hover_select").selectbox({
        onChange: function (val, inst) {
            newHover = val;
            changeHover(val);
        },
        speed: 0
    });

    $("#currency_select").selectbox({
        onChange: function (val, inst) {
            currentCurrency = val;
            changeCurrency(val);
        },
        speed: 0
    });

    $("#per_select").selectbox({
        onChange: function (val, inst) {
            currentPer = val;
            changePer();
        },
        speed: 0
    });

    $("#icon_select").selectbox({
        onChange: function (val, inst) {

        },
        speed: 0
    });

    var owlSchemes = $('.owl-schemes');

    owlSchemes.owlCarousel({
        items: 1,
		nav: true,
		navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
    });

    owlSchemes.on('changed.owl.carousel', function (event) {
        currentScheme = event.item.index;
        $(".ptc-tables *[style]").removeAttr('style');
        changeScheme();
        $('.color-box').hide();
        for (i = 1; i <= currentItems; i++) {
            $('#ptc_' + i).attr('datacolor', schemes[currentScheme].colors[i - 1]);
        }

    });

	var owlFonts = $('.owl-fonts');

    owlFonts.owlCarousel({
        items: 1,
		nav: true,
		navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
    });

    owlFonts.on('changed.owl.carousel', function (event) {
		currentFont = event.item.index;

		var font = fonts[currentFont].name;
        $('.ptc-tables').removeAttr('style');
		$('.ptc-tables').css({ 'font-family': fonts[currentFont].name, 'font-size': fonts[currentFont].size });

    });


	$(document).on('click', '.icon-set li', function () {

		var newIcon = $(this).attr('data-icon');
		$('#' + currentId).find('.ptc-note i').attr('class', newIcon );
		icons[currentId] = newIcon;

	});

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-grey',
        radioClass: 'iradio_square-grey',
        increaseArea: '20%' // optional
    });

    $(document).on('click', '.ptc-table', function () {

        currentId = $(this).attr('id');
		currentIcon = $(this).find('.ptc-note-icon i').attr('class');

        $('.rating').val(stars[currentId]);

        $('.selected-item').remove();
        $('.ptc-table').removeClass('selected');
        $('#' + currentId).append('<div class="selected-item c-top" style="border-color: '+creatorColor+'"></div>');
        $('#' + currentId).addClass('selected');
        $('.item-options').show();
        var pattern = /[0-9]+/g;

        currentSelected = (currentId.match(pattern));
        itemColor = $(this).attr('datacolor');

        if ($('#' + currentId + ' .ptc-note').hasClass('ptc-show') == true) {
            $('.note').iCheck('check');
			$('.icon-set').show();
        } else {
            $('.note').iCheck('uncheck');
			$('.icon-set').hide();
        }


        if ($('#' + currentId).hasClass('ptc-zoom') == true) {
            $('.zoom').iCheck('check');
        } else {
            $('.zoom').iCheck('uncheck');
        }


        $('#colorpicker').spectrum({
            color: itemColor,
            showInput: true,
            clickoutFiresChange: true,
            showInitial: true,
            showButtons: false,
            containerClassName: 'creator-picker',
            preferredFormat: "hex",
            move: function(color) {
                color = color.toHexString();
                $itemID = $('#' + currentId);
                $itemID.attr('datacolor', color);

                $itemID.find('.ptc-arrow-down.ptc-first').css('border-top-color', color);
                $itemID.find('.ptc-price').css('background-color', ColorLuminance(color, 0.3));
                $itemID.find('.ptc-arrow-down.ptc-second').css('border-top-color', ColorLuminance(color, 0.3));
                $itemID.find('.ptc-stars').css('color', ColorLuminance(color, 0.4));
                $itemID.find('.ptc-note-exposed').css('border-top', '45px solid ' + ColorLuminance(color, -0.2));
                $itemID.find('.ptc-note-fold').css('border-left', '45px solid ' + ColorLuminance(color, 0.3));
                $itemID.find('.ptc-header, .ptc-footer .ptc-button').css('background', color);
            }
        });





    });



    $('.rating').stepper({
        incrementButton: '<i class="fa fa-angle-up"></i>',
        decrementButton: '<i class="fa fa-angle-down"></i>',
            onStep: function (val, up) {
            $('#' + currentId + ' .ptc-stars').html('');
            stars[currentId] = val;
            for (i = 0; i < stars[currentId]; i++) {
                $('#' + currentId).find('.ptc-stars').append('<span class="fa fa-star"></span>');
            }
        }
    });

	updateHeight();

	$('#colorpicker_hue').spectrum({
		color: '#068894',
		showInput: true,
		clickoutFiresChange: true,
		showInitial: true,
		showButtons: false,
		containerClassName: 'creator-picker',
		replacerClassName: 'creator-picker-hue',
		preferredFormat: "hex",
		move: function(color) {

            $('.ptc-tables').attr('class', 'ptc-tables '+templates[currentTemplate].name.toLowerCase() );
            color = color.toHexString();
			for (i = 1; i <= 6; i++) {

					var color = ColorLuminance(color, -(i/(currentItems*3)));
					$itemID = $('.ptc-table:nth-child('+i+')');
					$itemID.attr('datacolor', color);
					$itemID.find('.ptc-header, .ptc-footer .ptc-button').css('background-color', color);
					$itemID.find('.ptc-arrow-down.ptc-first').css('border-top-color', color);
					$itemID.find('.ptc-price').css('background-color', ColorLuminance(color, 0.3));
					$itemID.find('.ptc-arrow-down.ptc-second').css('border-top-color', ColorLuminance(color, 0.3));
					$itemID.find('.ptc-stars').css('color', ColorLuminance(color, 0.3));
					$itemID.find('.ptc-note-exposed').css('border-top', '45px solid ' + ColorLuminance(color, -0.2));
					$itemID.find('.ptc-note-fold').css('border-left', '45px solid ' + ColorLuminance(color, 0.3));

			}

		}
	});


    function ColorLuminance(hex, lum) {

        // validate hex string
        hex = String(hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        lum = lum || 0;

        // convert to decimal and change luminosity
        var rgb = "#", c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i * 2, 2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ("00" + c).substr(c.length);
        }

        return rgb;
    }


    $('#colorpicker_bg').spectrum({
		color: '#252525',
		showInput: true,
		clickoutFiresChange: true,
		showInitial: true,
		showButtons: false,
		containerClassName: 'creator-picker',
		replacerClassName: 'creator-picker-bg',
		preferredFormat: "hex",
		move: function(color) {

            color = color.toHexString();
			creatorColor = ColorLuminance(color, 0.5);
			$('body').css('background-color', color);
			$('.creator-picker-bg, .c-top').css('border-color', ColorLuminance(color, 0.5));
			$('.toggle').css('color', ColorLuminance(color, 0.5));
			$('.toggle, .mode, .remove-item i').css('color', ColorLuminance(color, 0.5));
			$('.handle-bg').css('background-color', ColorLuminance(color, 0.5));
			$('.handle-bg i').css('color', ColorLuminance(color, 0.1));
		}
	});




});



	function updateHeight(){
		var site = $(window).height();
		var navTitle = $('.nav-title').outerHeight();
		var navFooter = $('.nav-footer').outerHeight();
		var contentNav = site-navTitle-navFooter;
		$('.nav-content').css( 'height' , contentNav+'px' );
		$('.main').css( 'min-height' , site+'px' );
	}

//Browser Window Resize
$(window).resize(function () {
    sortingCtrl = 1;

    $("#sortable").sortable("disable");
    $("#sortable").disableSelection();
    $(".remove-item, .remove-feature, .selected-item, .handle").hide();
    $('.ptc-tables').removeClass('flex');
    $('body').removeClass('squeeze');
    $('body').addClass('preview-mode');
    $('aside').removeClass('show');
    $(".mode").html("PREVIEW MODE");
	updateHeight();
});
