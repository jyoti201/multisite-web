/**
 * modalEffects.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
var ModalEffects = (function() {

	function init() {

		var overlay = document.querySelector( '.md-custom-overlay' );

		[].slice.call( document.querySelectorAll( '.md-custom-trigger' ) ).forEach( function( el, i ) {

			var modal = document.querySelector( '#' + el.getAttribute( 'data-modal' ) ),
				close = modal.querySelector( '.md-custom-close' );

			function removeModal( hasPerspective ) {
				classie.remove( modal, 'md-custom-show' );

				if( hasPerspective ) {
					classie.remove( document.documentElement, 'md-custom-perspective' );
				}
			}

			function removeModalHandler() {
				removeModal( classie.has( el, 'md-custom-setperspective' ) );
			}

			el.addEventListener( 'click', function( ev ) {
				classie.add( modal, 'md-custom-show' );
				overlay.removeEventListener( 'click', removeModalHandler );
				overlay.addEventListener( 'click', removeModalHandler );

				if( classie.has( el, 'md-custom-setperspective' ) ) {
					setTimeout( function() {
						classie.add( document.documentElement, 'md-custom-perspective' );
					}, 25 );
				}
			});

			close.addEventListener( 'click', function( ev ) {
				ev.stopPropagation();
        ev.removeClass('md-custom-show');
				removeModalHandler();
			});

		} );
    $('body').on('click', '.md-custom-close', function(){
        $(this).parents('.md-custom-modal').removeClass('md-custom-show');
        return false;
    })
	}

	init();

})();
