.navbar-custom{
		{{ !empty(getSetting('header_bg_color','header')) ? 'background-color:'.getSetting('header_bg_color','header').';' : '' }}
}
@if(!empty(getSetting('transparent_header','header')))
	.navbar-absolute-top{
		position: absolute;
		right: 0;
		left: 0;
		z-index: 1030;
		background-color: transparent;
	}
@endif
@if(!empty(getSetting('shrink_header','header')))
	.shrink{
		padding-top:0;
		padding-bottom:0;
	}
@endif


.header3 .slide-toggle span,.header4 .slide-toggle span{
	{{ !empty(getSetting('slide_toggle_bg_color','header')) ? 'background-color:'.getSetting('slide_toggle_bg_color','header').';' : '' }}
}

.sticky-wrapper.is-sticky .navbar-custom{
	{{ !empty(getSetting('static_background_color','header')) ? 'background-color:'.getSetting('static_background_color','header').';' : '' }}
}
.sticky-wrapper.is-sticky .navbar-custom .navbar-nav li a{
	{{ !empty(getSetting('static_menu_item_color','header')) ? 'color:'.getSetting('static_menu_item_color','header').';' : '' }}
}
.sticky-wrapper.is-sticky .navbar-custom .navbar-nav li:hover > a{
	{{ !empty(getSetting('static_menu_item_color_hover','header')) ? 'color:'.getSetting('static_menu_item_color_hover','header').';' : '' }}
}
.header3 .sliding .sliding-menu,.header4 .sliding .sliding-menu{
	{{ !empty(getSetting('slide_menu_bg_color','header')) ? 'background-color:'.getSetting('slide_menu_bg_color','header').';' : 'background-color:#eaeaea;' }}
	{{ !empty(getSetting('slide_menu_text_color','header')) ? 'color:'.getSetting('slide_menu_text_color','header').';' : 'color:#000000;' }}
	{{ !empty(getSetting('slide_menu_width','header')) ? 'width:'.getSetting('slide_menu_width','header').'px;' : 'width:250px;' }}
}
