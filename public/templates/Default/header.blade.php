
<div class="navbar
@if(!empty(getSetting('shrink_header','header')))
	@if(!empty(getSetting('shrink_home_only','header')) && is_home($page->slug))
		shrinkable
	@elseif(empty(getSetting('shrink_home_only','header')))
		shrinkable
	@endif
@endif
@if(!empty(getSetting('transparent_header','header')))
	@if(!empty(getSetting('transparent_home_only','header')) && is_home($page->slug))
		transparent navbar-absolute-top
	@elseif(empty(getSetting('transparent_home_only','header')))
		transparent navbar-absolute-top
	@endif
@endif
@if(!empty(getSetting('static_header','header')))
	@if(!empty(getSetting('static_home_only','header')) && is_home($page->slug))
		sticky
	@elseif(empty(getSetting('static_home_only','header')))
		sticky
	@endif
@endif
navbar-custom"
@if(!empty(getSetting('static_header','header')))
	@if(!empty(getSetting('static_home_only','header')) && is_home($page->slug))
		id="sticky-nav"
	@elseif(empty(getSetting('static_home_only','header')))
		id="sticky-nav"
	@endif
@endif
role="navigation" >
	<div class="{!! !empty(getSetting('fullwidth_header','header')) ? 'container-fluid' : 'container' !!}">
		@if(!empty(getSetting('header','header')) && getSetting('header','header')=='header1')
		<div class="row">
			<div class="col-md-12 valign">
				<div class="navbar-header valign-mobile-col-1">
					<a class="navbar-brand logo" href="/">
						@if(!empty(getSetting('logo','header')))
						<img src="{{ getSetting('logo','header') }}" class="img-responsive">
						@endif
					</a>
				</div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-collapse collapse {{ !empty(getSetting('menustyle','header'))? getSetting('menustyle','header'): ''  }} valign-mobile-row-break" id="navbar-menu1">
					<ul class="nav navbar-nav navbar-right">
						{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
					</ul>
				</div>
			</div>
		</div>
		@elseif(!empty(getSetting('header','header')) && getSetting('header','header')=='header2')
		<div class="row valign">
			<div class="col-md-5 hidden-sm  hidden-xs">
				<div class="navbar-collapse {{ !empty(getSetting('menustyle','header'))? getSetting('menustyle','header'): ''  }}" id="navbar-menu">
					<ul class="nav navbar-nav pull-right">
						{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
					</ul>
				</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-12 valign-mobile-row-break text-center">
				<a class="logo" href="/">
					@if(!empty(getSetting('logo','header')))
					<img src="{{ getSetting('logo','header') }}" class="img-responsive">
					@endif
				</a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				</button>
			</div>
			<div class="col-md-5 col-sm-8 col-xs-12 mobclear valign-mobile-row-break">
				<div class="navbar-collapse collapse {{ !empty(getSetting('menustyle','header'))? getSetting('menustyle','header'): ''  }}" id="navbar-menu2">
					<ul class="nav navbar-nav navbar-right visible-sm visible-xs">
						{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
					</ul>
					<ul class="nav navbar-nav navbar-right">
						{!! getMenubyID(getSetting('menu_location_menu2','header')) !!}
					</ul>
				</div>
			</div>
		</div>
		@elseif(!empty(getSetting('header','header')) && getSetting('header','header')=='header3')
		<div class="row valign">
			<div class="col-md-6 valign-col-1 col-sm-6">
				<a class="logo" href="/">
					@if(!empty(getSetting('logo','header')))
					<img src="{{ getSetting('logo','header') }}" class="img-responsive">
					@endif
				</a>
			</div>
			<div class="col-md-6 text-right">
				<a href="#" class="slide-toggle" id="slide-toggle">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</a>
			</div>
		</div>
		<div class="sliding" id="sliding">
			<div class="sliding-overlay"></div>
			<div class="sliding-menu">
				<a href="#" class="slide-toggle active" id="slide-toggle-close">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</a>
				@if(getSetting('slide_toggle_custom','header'))
					{!! ImplementWidgets(getSetting('slide_toggle_custom','header')) !!}
				@else
				<ul class="nav navbar-nav">
				{!! getMenubyID(getSetting('menu_location_slide','header')) !!}
				</ul>
				@endif
			</div>
		</div>
		@elseif(!empty(getSetting('header','header')) && getSetting('header','header')=='header4')
		<div class="row valign">
			<div class="col-sm-5 valign-mobile-col-1 valign">
				<div class="navbar-header valign-col-1">
					<a href="#" class="slide-toggle" id="slide-toggle">
						<span class="icon-bar top"></span>
						<span class="icon-bar middle"></span>
						<span class="icon-bar bottom"></span>
					</a>
					<a class="logo" href="index.html">
						@if(!empty(getSetting('logo','header')))
						<img src="{{ getSetting('logo','header') }}" class="img-responsive">
						@endif
					</a>
				</div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
			</div>
			<div class="col-sm-7 mobclear valign-mobile-row-break">
				<div class="navbar-collapse collapse {{ !empty(getSetting('menustyle','header'))? getSetting('menustyle','header'): ''  }}" id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						{!! getMenubyID(getSetting('menu_location_menu1','header')) !!}
					</ul>
				</div>
			</div>
		</div>
		<div class="sliding" id="sliding">
			<div class="sliding-overlay"></div>
			<div class="sliding-menu">
				<a href="#" class="slide-toggle active" id="slide-toggle-close">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</a>
				@if(getSetting('slide_toggle_custom','header'))
					{!! ImplementWidgets(getSetting('slide_toggle_custom','header')) !!}
				@else
				<ul class="nav navbar-nav">
				{!! getMenubyID(getSetting('menu_location_slide','header')) !!}
				</ul>
				@endif
			</div>
		</div>
		@endif
	</div>
</div>
