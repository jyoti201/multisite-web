@if(!empty(getSetting('html','footer')))

	{!! getSetting('html','footer') !!}

@endif

<!-- Back to top -->
<a href="#" class="back-to-top" id="back-to-top"> <i class="fa fa-angle-up"></i> </a>
<script>
jQuery(document).ready(function(){
	jQuery('#slide-toggle').click(function(){
		if(jQuery('#sliding').hasClass('on')){
			jQuery('#sliding').fadeOut();
			jQuery('#sliding').removeClass('on');
		}else{
			jQuery('#sliding').fadeIn();
			jQuery('#sliding').addClass('on');
		}
		return false;
	})
	jQuery('#slide-toggle-close,.sliding-overlay').click(function(){
			jQuery('#sliding').fadeOut();
			jQuery('#sliding').removeClass('on');
		return false;
	})
	@if(!empty(getSetting('shrink_header','header')))
		jQuery(document).on("scroll", function(){
				if(jQuery(document).scrollTop() > 100){
				jQuery(".shrinkable").addClass("shrink");
				}else{
				jQuery(".shrinkable").removeClass("shrink");
				}
		});
	@endif
})
</script>


<!-- Jquery easing -->
<script type="text/javascript" src="{{ IncludeAsset('public/js/jquery.easing.1.3.min.js') }}"></script>
<!--sticky header-->
<script type="text/javascript" src="{{ IncludeAsset('public/js/jquery.sticky.js') }}"></script>

<!--common script for all pages-->
<script src="{{ IncludeAsset('public/js/jquery.app.js') }}"></script>
