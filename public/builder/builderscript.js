jQuery(document).ready(function($){
var c;
	c = setInterval(function(){
		if($('#divTool').html()!=''){
			$('#md-widgetselect').find('.md-body').html($('#divTool').html());
			var html ="";
			$('#divSnippetCatOptions').find('div').each(function(){

				html += '<button data-val="'+$(this).attr('data-value')+'">'+$(this).text()+'</button>';
			})
			$('#md-widgetselect').find('.selectcat').html(html);
			clearInterval(c);
		}
	}, 50);
	var activx,activecol;
	$('#is-wrapper').on('click','.ui-draggable',function (e) {
		$('.customTools').hide();
		var top = $(this).offset().top - 30;
		var left = $(this).offset().left;


		$('.customTools').show();

		if($(this).find('.row').length>0){
			$('#rowsetting').show();
		}else{
			$('#rowsetting').hide();
		}
		if($(this).find('[class*="col-"]').length>0){
			$('#otherTool').show();
		}else{
			$('#otherTool').hide();
		}

		if($(this).find('.spacer').length>0){
			$('#spacerTools').css('opacity', 1);
			$('#spacerTools').show();
		}else{
			$('#spacerTools').css('opacity', 0);
			$('#spacerTools').hide();
		}

		setTimeout(function(){
			$('#customTools').css('top', top+'px');
			$('#customTools').css('left', left+'px');
			$('#customTools').css('opacity', 1);
		}, 50);
		activx = $(this);

		if(activx.find('[class*="col-"]').length<=1){
			$('#colremove').hide();
		}else{
			$('#colremove').show();
		}
	});
	$('#is-wrapper').on('click','.spacer', function(e){
		var spacerleft = $(this).offset().left;
		var spacertop = $(this).offset().top;
		var spacerheight = $(this).height();
		var spacerwidth = $(this).width();
		$('#spacerTools').css('top', spacertop+'px');
		$('#spacerTools').css('left', spacerleft+'px');
		$('#spacerTools').css('width', spacerwidth+'px');
		$('#spacerTools').css('height', spacerheight+'px');
		$('#spacerTools').css('opacity', 1);
		$('#spacerTools').show();
	})
	$('#is-wrapper').on('click','[class*="col"]', function(e){
		var top = $(this).offset().top + $(this).height();
		var left = $(this).offset().left + parseInt($(this).css('padding-left'));
		top = top+parseInt($(this).css('padding-top'))+parseInt($(this).css('padding-bottom'));
		$('#otherTool').show();
		setTimeout(function(){
		$('#otherTool').css('top', top+'px');
		$('#otherTool').css('left', left+'px');
		$('#otherTool').css('opacity', 1);
		}, 50);
		activecol = $(this);
	})
	$('#rowsetting').click(function(){
		activecol = activx.find('.row');
		$('#styleselectanimate').trigger('click');
		$('#md-styleselect .rowsettingonly').show();
		$('#md-styleselect .colsettingonly').hide();
	})
	$('#is-wrapper').on('click','.is-builder.empty',function () {
		$('#widgetselect').trigger('click');
		var currentempty = $(this);
		$('#divSnippetList').addClass('firstui');
		$('#divSnippetList div').unbind('click');
		$('#md-widgetselect').on('click', '#divSnippetList.firstui div', function(){
			var html = $.parseHTML('<div class="ui-draggable">'+$('#snip'+$(this).data('snip')).text()+'<div class="row-tool" style="right: auto; left: -37px; display: none;"><div class="row-handle ui-sortable-handle"><i class="cb-icon-move"></i></div><div class="row-html"><i class="cb-icon-code"></i></div><div class="row-copy"><i class="cb-icon-plus"></i></div><div class="row-remove"><i class="cb-icon-cancel"></i></div></div></div>');
			currentempty.html(html);
			currentempty.data('contentbuilder').applyBehavior();
			currentempty.removeClass('empty');
			$('#divSnippetList').removeClass('firstui');
			$('#md-widgetselect').removeClass('md-show');
		})
	})
	$('#widgetselect').click(function(){
		$('#md-widgetselect').addClass('md-show');
		$('.customTools').css('opacity', 0);
			$('.customTools').hide();
	})
	$('#widgetselectCancel').click(function(){
		$('#md-widgetselect').removeClass('md-show');
	})
	$('#md-widgetselect .selectcat').on('click', 'button', function(){
		$('#md-widgetselect .selectcat button').removeClass('active');
    if($(this).data('val')==-1){
    $('#divSnippetList div').fadeIn();
    }else{
    $('#divSnippetList div').hide();
    $('#divSnippetList div[data-cat="'+$(this).data('val')+'"]').fadeIn();
    }
		$(this).addClass('active');
	})
  $('#widgetselectup').click(function(){
      activx.insertBefore(activx.prev('.ui-draggable'));
      $('.customTools').css('opacity', 0);
	$('.customTools').hide();
  })
  $('#widgetselectdown').click(function(){
      activx.insertAfter(activx.next('.ui-draggable'));
      $('.customTools').css('opacity', 0);
		$('.customTools').hide();
  })
$('#colmoveleft').click(function(){
		activecol.insertBefore(activecol.prev('[class*="col-"]'));
		$('.customTools').css('opacity', 0);
	$('.customTools').hide();
})
$('#colhtml').click(function(){
		var html=activecol.html();
		jQuery('#txtHtml').val(html);
		jQuery('#md-html').css('width', '80%');
		jQuery('#md-html').simplemodal({isModal:true});
		jQuery('#md-html').data('simplemodal').show();

		jQuery('#txtHtml').css('display', 'none');
															if(jQuery('#txtHtml').data('CodeMirrorInstance')){
																			var $htmlEditor = $('#txtHtml').data('CodeMirrorInstance');
																			$htmlEditor.setValue(html);
															}else{
 															var myTextArea = jQuery("#txtHtml")[0];
															var $htmlEditor = CodeMirror.fromTextArea(myTextArea, {
		                                value: html,
		                                mode: "text/html",
		                                lineWrapping: true,
		                                lineNumbers: true,
		                                tabMode: "indent"
		                            });
																$htmlEditor.on("change", function(cm, change) {
                                    jQuery('#txtHtml').val(cm.getValue());
                                });
															}
																jQuery('#txtHtml').data('CodeMirrorInstance', $htmlEditor);
																$('#btnHtmlOk').off('click');
						                    $('#btnHtmlOk').on('click', function (e) {
						                        var $htmlEditor = $('#txtHtml').data('CodeMirrorInstance');
						                        $('#txtHtml').val($htmlEditor.getValue());
						                        activecol.html($('#txtHtml').val());
						                        $('#md-html').data('simplemodal').hide();
						                        activx.parents(".is-builder").data('contentbuilder').applyBehavior();
						                        activx.parents(".is-builder").data('contentbuilder').blockChanged();
						                        activx.parents(".is-builder").data('contentbuilder').settings.onRender();
						                        activx.parents(".is-builder").data('contentbuilder').settings.onChange();
						                        saveForUndo();
						                    });
						                    jQuery('#btnHtmlCancel').off('click');
						                    jQuery('#btnHtmlCancel').on('click', function (e) {
						                        jQuery('#md-html').data('simplemodal').hide();
						                    });
		$('.customTools').css('opacity', 0);
		$('.customTools').hide();
})
$('#colremove').click(function(){
	jQuery("#md-delcolconfirm").simplemodal();
	jQuery("#md-delcolconfirm").data("simplemodal").show();
})
$('#colremoveOk').click(function(){
	activecol.fadeOut();
	activecol.remove();
	jQuery("#md-delcolconfirm").data("simplemodal").hide();
})
$('#colremoveCancel').click(function(){
	jQuery("#md-delcolconfirm").data("simplemodal").hide();
})
$('#colmoveright').click(function(){
		activecol.insertAfter(activecol.next('[class*="col-"]'));
		$('.customTools').css('opacity', 0);
	$('.customTools').hide();
})
$('#colclone').click(function(){
		activecol.clone().insertAfter(activecol);
		$('.customTools').css('opacity', 0);
	$('.customTools').hide();
})
	$('#spacer-minus').click(function(){
      var height = activx.find('.spacer').height();
			var max= $(this).data('max');
			var min= $(this).data('min');
			if(height<min){
				return false;
			}else{
				for (var i = min; i <= max; i=i+10) {
					activx.find('.spacer').removeClass('height-'+i);
				}

				height = height-10;
				activx.find('.spacer').addClass('height-'+height);
			}
  })
	$('#spacer-plus').click(function(){
      var height = activx.find('.spacer').height();
			var max= $(this).data('max');
			var min= $(this).data('min');
			if(height>max){
				return false;
			}else{
				for (var i = min; i <= max; i=i+10) {
					activx.find('.spacer').removeClass('height-'+i);
				}

				height = height+10;
				activx.find('.spacer').addClass('height-'+height);
			}
  })

	$('#md-widgetselect').on('click', '#divSnippetList:not(.firstui) div', function(){
		var html = $.parseHTML('<div class="ui-draggable">'+$('#snip'+$(this).data('snip')).text()+'<div class="row-tool" style="right: auto; left: -37px; display: none;"><div class="row-handle ui-sortable-handle"><i class="cb-icon-move"></i></div><div class="row-html"><i class="cb-icon-code"></i></div><div class="row-copy"><i class="cb-icon-plus"></i></div><div class="row-remove"><i class="cb-icon-cancel"></i></div></div></div>');
		$(html).insertAfter(activx);
    activx.parents(".is-builder").data('contentbuilder').applyBehavior();
		$('#md-widgetselect').removeClass('md-show');
		$('.row').each(function(){
				if($(this).find('div[class*="col"]').length>0){
					if($(this).attr('contenteditable')){
						$(this).removeAttr('contenteditable');
						$(this).find('div[class*="col"]').attr('contenteditable', 'true');
					}
				}
			})
	})
	$('#styleselectanimate').click(function(){
		$('#md-styleselect .rowsettingonly').hide();
		$('#md-styleselect .colsettingonly').show();
		jQuery("#md-styleselect").simplemodal();
        jQuery("#md-styleselect").data("simplemodal").show();
		$('*[data-aos]').each(function(){
			$(this).addClass('aos-animate');
		})
		if(activecol.hasClass('aos-init')){
			$('#md-styleselect').find('input[name="animateOffset"]').val(activecol.attr('data-aos-offset'));
			$('#md-styleselect').find('input[name="animateDelay"]').val(activecol.attr('data-aos-delay'));
			$('#md-styleselect').find('input[name="animateDuration"]').val(activecol.attr('data-aos-duration'));
			if(activecol.attr('data-aos-once')=='true'){
				$('#md-styleselect').find('input[name="animateOnce"]').prop("checked", true);
			}
			$('#md-styleselect').find('input[name="animateName"][value="'+activecol.attr('data-aos')+'"]').prop("checked", true);
		}else{
			$('#md-styleselect').find('input[name="animateOffset"]').val('200');
			$('#md-styleselect').find('input[name="animateDelay"]').val('50');
			$('#md-styleselect').find('input[name="animateDuration"]').val('1000');
			$('#md-styleselect').find('input[name="animateOnce"]').prop("checked", false);
			$('#md-styleselect').find('input[name="animateName"]').prop("checked", false);
		}
		var classes = activecol.attr('class');
		var classes_arr;
		$('.animationList .pading-margin input[type="text"]').val('');
		$('input[type=text][readonly]').val('');
		if(classes!=''){
			classes_arr = classes.split(' ');
			for(var i=0;i<classes_arr.length;i++){
				var inputclass = classes_arr[i].replace(/\d+/g, '');
				inputclass = inputclass.replace(/--/g, '-');
				var radiusinputclass = inputclass.replace('border-radius-','').replace('-','');
				if(radiusinputclass=='tlper' || radiusinputclass=='tlpx' || radiusinputclass=='trper' || radiusinputclass=='trpx' || radiusinputclass=='blper' || radiusinputclass=='blpx' || radiusinputclass=='brper' || radiusinputclass=='brpx'){

					var radiusinputtype = radiusinputclass.replace('per','').replace('px','');
					var radiusclass = radiusinputclass.replace('tl','').replace('tr','').replace('bl','').replace('br','');
					console.log('#colradiuspara_'+radiusinputtype+'_'+radiusclass);
					$('#colcolradiuspara_'+radiusinputtype+'_'+radiusclass).prop('checked', true);
					$('#colcolradiuspara_'+radiusinputtype+'_'+radiusclass).parents('.is-boxes').find('.custom-left-right-css').attr('data-dir', radiusinputtype+radiusclass);
					$('#colcolradiuspara_'+radiusinputtype+'_'+radiusclass).parents('.is-boxes').find('input[type="text"]').attr('data-parameter', 'border-radius-'+radiusinputtype+radiusclass+'-');
				}

				$('input[data-parameter="'+inputclass+'"]').val(classes_arr[i].replace(inputclass,''));
			}
		}

		if(classes!=''){
			classes_arr = classes.split(' ');
			var existingclassno_all=0,existingclassno_sm=0,existingclassno_xl=0,existingclassno_lg=0,existingclassno_md=0;
			for(var i=0;i<classes_arr.length;i++){
				if(classes_arr[i].match(/col-sm/g)){
					existingclassno_sm = classes_arr[i].match(/\d+/);
				}
				if(classes_arr[i].match(/col-md/g)){
					existingclassno_md = classes_arr[i].match(/\d+/);
				}
				if(classes_arr[i].match(/col-xl/g)){
					existingclassno_xl = classes_arr[i].match(/\d+/);
				}
				if(classes_arr[i].match(/col-lg/g)){
					existingclassno_lg = classes_arr[i].match(/\d+/);
				}
				if(classes_arr[i].match(/col/g)){
					existingclassno_all = classes_arr[i].match(/\d+/);
				}
			}
			if(typeof existingclassno_all !== "undefined" && existingclassno_all !== false && existingclassno_all !== null){
				existingclassno_all = 0;
			}
			//	alert(existingclassno_all);

			$('#colsize_all').val(existingclassno_all);
			$('#colsize_sm').val(existingclassno_sm);
			$('#colsize_md').val(existingclassno_md);
			$('#colsize_xl').val(existingclassno_xl);
			$('#colsize_lg').val(existingclassno_lg);

			var existingclassno_all=0,existingclassno_sm=0,existingclassno_xl=0,existingclassno_lg=0,existingclassno_md=0;
			for(var i=0;i<classes_arr.length;i++){
				if(classes_arr[i].match(/offset-sm/g)){
					existingclassno_sm = classes_arr[i].match(/\d+/);
				}
				if(classes_arr[i].match(/offset-md/g)){
					existingclassno_md = classes_arr[i].match(/\d+/);
				}
				if(classes_arr[i].match(/offset-xl/g)){
					existingclassno_xl = classes_arr[i].match(/\d+/);
				}
				if(classes_arr[i].match(/offset-lg/g)){
					existingclassno_lg = classes_arr[i].match(/\d+/);
				}
				if(classes_arr[i].match(/offset/g)){
					existingclassno_all = classes_arr[i].match(/\d+/);
				}
			}
			if(typeof existingclassno_all !== "undefined" && existingclassno_all !== false && existingclassno_all !== null){
				existingclassno_all = 0;
			}
			$('#coloffset_all').val(existingclassno_all);
			$('#coloffset_sm').val(existingclassno_sm);
			$('#coloffset_md').val(existingclassno_md);
			$('#coloffset_xl').val(existingclassno_xl);
			$('#coloffset_lg').val(existingclassno_lg);
		}

		var borderclass ='none';
		if(classes!=''){
			classes_arr = classes.split(' ');
			for(var i=0;i<classes_arr.length;i++){
				if(classes_arr[i].match(/border-style/g)){
					borderclass = classes_arr[i].replace('border-style-','');
					break;
				}
			}
		}
		$('#col-content-editbox-3 input[value="'+borderclass+'"]').prop('checked', true);

		if(activecol.css('border-color')!=''){
			$('#col-border-color').val(activecol.css('border-color'));
			$('#col-border-color').css('background-color', activecol.css('border-color'));
		}else{
			$('#col-border-color').val('#000000');
			$('#col-border-color').css('background-color', '#000000');
		}

		if(activecol.hasClass('d-block') && activecol.hasClass('d-sm-none')){
			$('#col-content-editbox-5 .switch input[value="visible-xs"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="visible-xs"]').prop('checked', false);
		}
		if(activecol.hasClass('d-none') && activecol.hasClass('d-sm-block') && activecol.hasClass('d-md-none')){
			$('#col-content-editbox-5 .switch input[value="visible-sm"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="visible-sm"]').prop('checked', false);
		}
		if(activecol.hasClass('d-none') && activecol.hasClass('d-md-block') && activecol.hasClass('d-lg-none')){
			$('#col-content-editbox-5 .switch input[value="visible-md"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="visible-md"]').prop('checked', false);
		}
		if(activecol.hasClass('d-none') && activecol.hasClass('d-lg-block') && activecol.hasClass('d-xl-none')){
			$('#col-content-editbox-5 .switch input[value="visible-lg"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="visible-lg"]').prop('checked', false);
		}
		if(activecol.hasClass('d-none') && activecol.hasClass('d-xl-block')){
			$('#col-content-editbox-5 .switch input[value="visible-xl"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="visible-xl"]').prop('checked', false);
		}

		if(activecol.hasClass('d-none') && activecol.hasClass('d-sm-block')){
			$('#col-content-editbox-5 .switch input[value="hidden-xs"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="hidden-xs"]').prop('checked', false);
		}
		if(activecol.hasClass('d-sm-none') && activecol.hasClass('d-md-block')){
			$('#col-content-editbox-5 .switch input[value="hidden-sm"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="hidden-sm"]').prop('checked', false);
		}
		if(activecol.hasClass('d-md-none') && activecol.hasClass('d-lg-block')){
			$('#col-content-editbox-5 .switch input[value="hidden-md"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="hidden-md"]').prop('checked', false);
		}
		if(activecol.hasClass('d-lg-none') && activecol.hasClass('d-xl-block')){
			$('#col-content-editbox-5 .switch input[value="hidden-lg"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="hidden-lg"]').prop('checked', false);
		}
		if(activecol.hasClass('d-xl-none')){
			$('#col-content-editbox-5 .switch input[value="hidden-xl"]').prop('checked', true);
		}else{
			$('#col-content-editbox-5 .switch input[value="hidden-xl"]').prop('checked', false);
		}



		var attr = activecol.attr('data-id');
		if(attr!='') {
				$('#colid').val(attr);
		}else{
			$('#colid').val('');
		}

		var existingclasses = activecol.attr('data-classes');
		if(existingclasses!='') {
				$('#colclasses').val(existingclasses);
		}else{
			$('#colclasses').val('');
		}

		if(activecol.css('box-shadow')!=''){
			$('#col-shadow-none').prop('checked', false);
			$('#col-shadow-in').prop('checked', false);
			$('#col-shadow-out').prop('checked', false);
			var hoffset=0,voffset=0,blur=5,distance=5,color='rgb(0,0,0)',opacity=75,type='none';
			if(activecol.css('box-shadow')=='none'){
				type='none';
			}else{
				var shadow = activecol.css('box-shadow').split(' ');
				opacity = parseFloat(shadow[3]);
				hoffset = shadow[4].replace('px', '');
				voffset = shadow[5].replace('px', '');
				blur = shadow[6].replace('px', '');
				distance = shadow[7].replace('px', '');
				color = shadow[0].replace('rgba','rgb')+shadow[1]+shadow[2].replace(',','')+')';
				if(shadow.length==9){
					type = 'in';
				}else{
					type = 'out';
				}
			}
			$('#col-shadow-hoffset').val(hoffset);
			$('#col-shadow-hoffset-count').val(hoffset);
			$('#col-shadow-voffset').val(voffset);
			$('#col-shadow-voffset-count').val(voffset);
			$('#col-shadow-blur').val(blur);
			$('#col-shadow-blur-count').val(blur);
			$('#col-shadow-color').val(rgb2hex(color));
			$('#col-shadow-color').css('background-color', rgb2hex(color));
			$('#col-shadow-distance').val(distance);
			$('#col-shadow-distance-count').val(distance);
			$('#col-shadow-opacity').val(opacity);
			$('#col-shadow-opacity-count').val(opacity);
			if(type=='in'){
				$('#col-shadow-in').prop('checked', true);
			}else if(type=='out'){
				$('#col-shadow-out').prop('checked', true);
			}else if(type=='none'){
				$('#col-shadow-none').prop('checked', true);
			}
		}


		if(activecol.attr('data-bg-color-from')){
			$('#col-bg-color-from').val(activecol.attr('data-bg-color-from'));
			$('#col-bg-color-from').css('background-color', activecol.attr('data-bg-color-from'));
		}else{
			$('#col-bg-color-from').val('');
			$('#col-bg-color-from').css('background-color', 'rgba(150, 150, 150, 0.17)');
		}
		if(activecol.attr('data-bg-color-to')){
			$('#col-bg-color-to').val(activecol.attr('data-bg-color-to'));
			$('#col-bg-color-to').css('background-color', activecol.attr('data-bg-color-to'));
		}else{
			$('#col-bg-color-to').val('');
			$('#col-bg-color-to').css('background-color', 'rgba(150, 150, 150, 0.17)');
		}
		var attr = activecol.attr('data-bg-gradient-angle');
		if (typeof attr !== "undefined" && attr !== false && attr !== null) {
			$('#col-gradient-angle').val(activecol.attr('data-bg-gradient-angle'));
		}else{
			$('#col-gradient-angle').val(0);
		}




		var alignclass ='none';
		if(classes!=''){
			var classes_arr = classes.split(' ');
			for(var i=0;i<classes_arr.length;i++){
				if(classes_arr[i].match(/align-self/g)){
					alignclass = classes_arr[i];
					break;
				}
			}
		}
		console.log(alignclass);
		$('#col-content-editbox-1 input[name="colvalign"][value="'+alignclass+'"]').prop('checked', true);


		var alignclass ='none';
		if(classes!=''){
			var classes_arr = classes.split(' ');
			for(var i=0;i<classes_arr.length;i++){
				if(classes_arr[i].match(/-auto/g)){
					alignclass = classes_arr[i];
					break;
				}
			}
		}
		//console.log(alignclass);
		$('#col-content-editbox-1 input[name="colhalign"][value="'+alignclass+'"]').prop('checked', true);


		var alignclass ='none';
		if(classes!=''){
			var classes_arr = classes.split(' ');
			for(var i=0;i<classes_arr.length;i++){
				if(classes_arr[i].match(/justify-content/g)){
					alignclass = classes_arr[i];
					break;
				}
			}
		}
		//console.log(alignclass);
		$('#col-content-editbox-1 input[name="itemshalign"][value="'+alignclass+'"]').prop('checked', true);


		var alignclass ='none';
		if(classes!=''){
			var classes_arr = classes.split(' ');
			for(var i=0;i<classes_arr.length;i++){
				if(classes_arr[i].match(/align-items/g)){
					alignclass = classes_arr[i];
					break;
				}
			}
		}
		$('#col-content-editbox-1 input[name="itemsvalign"][value="'+alignclass+'"]').prop('checked', true);



		var alignclass ='none';
		if(classes!=''){
			var classes_arr = classes.split(' ');
			for(var i=0;i<classes_arr.length;i++){
				if(classes_arr[i].match(/flex-/g)){
					alignclass = classes_arr[i];
					break;
				}
			}
		}
		$('#col-content-editbox-1 input[name="itemwrap"][value="'+alignclass+'"]').prop('checked', true);

	})
	$('#styleselectanimate-remove').click(function(){
		activecol.removeAttr('data-aos');
		activecol.removeAttr('data-aos-offset');
		activecol.removeAttr('data-aos-delay');
		activecol.removeAttr('data-aos-duration');
		activecol.removeAttr('data-aos-once');
		activecol.removeClass('aos-init');
		activecol.removeClass('aos-animate');
		$('#md-styleselect').find('input[name="animateOffset"]').val('200');
		$('#md-styleselect').find('input[name="animateDelay"]').val('50');
		$('#md-styleselect').find('input[name="animateDuration"]').val('1000');
		$('#md-styleselect').find('input[name="animateOnce"]').prop("checked", false);
		$('#md-styleselect').find('input[name="animateName"]').prop("checked", false);
		$('#otherTool').hide();
	})
	$('#styleselectCancel').click(function(){
		$('#md-styleselect').removeClass('md-show');
	})
  $('#styleselectOk').click(function(){

		if($('#colclasses').val()!=''){
				var oldclasses = activecol.attr('data-classes');
				if(typeof oldclasses !== "undefined" && oldclasses !== false && oldclasses !== null){
					activecol.attr('class', activecol.attr('class').replace(oldclasses,''));
				}
				activecol.attr('class', activecol.attr('class')+' '+$('#colclasses').val());
				activecol.attr('data-classes', $('#colclasses').val());
		}else{
			var oldclasses = activecol.attr('data-classes');
			if(typeof oldclasses !== "undefined" && oldclasses !== false && oldclasses !== null){
				activecol.attr('class', activecol.attr('class').replace(oldclasses,''));
			}
			activecol.removeAttr('data-classes');
		}


		if($('#colid').val()!=''){
				activecol.attr('id', $('#colid').val());
				activecol.attr('data-id', $('#colid').val());
		}else{
			activecol.removeAttr('id');
			activecol.removeAttr('data-id');
		}


	  $('*[data-aos]').each(function(){
			$(this).addClass('aos-animate');
		})
		$('#md-styleselect').removeClass('md-show');
		$('.md-overlay').hide();
	})
	$('#md-styleselect input').on('click keyup mouseup change', function(){
		if($('#md-styleselect').find('input[name="animateName"]').val()!=''){
		activecol.attr('data-aos', $('#md-styleselect').find('input[name="animateName"]:checked').val());
		}
		if($('#md-styleselect').find('input[name="animateOffset"]').val()!=''){
		activecol.attr('data-aos-offset', $('#md-styleselect').find('input[name="animateOffset"]').val());
		}
		if($('#md-styleselect').find('input[name="animateDelay"]').val()!=''){
		activecol.attr('data-aos-delay', $('#md-styleselect').find('input[name="animateDelay"]').val());
		}
		if($('#md-styleselect').find('input[name="animateDuration"]').val()!=''){
		activecol.attr('data-aos-duration', $('#md-styleselect').find('input[name="animateDuration"]').val());
		}
		if($('#md-styleselect').find('input[name="animateOnce"]').is(":checked")){
		activecol.attr('data-aos-once', 'true');
		}else{
		activecol.removeAttr('data-aos-once');
		}
	})
	$('.animationList ul li input').on('click keyup mouseup change', function(){
		activecol.removeClass('aos-animate');
		setTimeout(function(){
			activecol.addClass('aos-animate');
      AOS.refresh();
		}, 1500);
	})
	jQuery(".custom-left-right-css").click(function () {
		var s = jQuery(this).attr("data-value");
		var p = jQuery(this).attr("data-parameter");
		var d = jQuery(this).attr("data-dir");
		var min = jQuery(this).attr("data-min");
		var max = jQuery(this).attr("data-max");
		var curval = $('input[data-parameter="'+p+'-'+d+'-'+'"]').val();
		if (s == "-") {
				var para = curval;
				var prevpara = parseInt(para)-1;

				if (activecol.hasClass(p+"-"+d+"-"+min)) { return false; }
				else if (activecol.hasClass(p+"-"+d+"-"+para)) { activecol.removeClass(p+"-"+d+"-"+para); activecol.addClass(p+"-"+d+"-"+prevpara) }
				else { activecol.addClass(p+"-"+d+"-0"); }
		}
		if (s == "+") {
			var para = curval;
			var prevpara = parseInt(para)+1;
			if (activecol.hasClass(p+"-"+d+"-"+max)) { return false; }
			else if (activecol.hasClass(p+"-"+d+"-"+para)) { activecol.removeClass(p+"-"+d+"-"+para); activecol.addClass(p+"-"+d+"-"+prevpara) }
			else { activecol.addClass(p+"-"+d+"-0"); }
		}
		if (s == "") {
				for (var i = min; i <= max; i++) {
					activecol.removeClass(p+"-"+d+"-"+i);
				}
				$('input[data-parameter="'+p+"-"+d+'-"]').val('');
		}

		var classes = activecol.attr('class');
		var classes_arr;
		if(classes!=''){
			classes_arr = classes.split(' ');
			for(var i=0;i<classes_arr.length;i++){
				var inputclass = classes_arr[i].replace(/\d+/g, '');
				inputclass = inputclass.replace('--', '-');
				$('input[data-parameter="'+inputclass+'"]').val(classes_arr[i].replace(inputclass,''));
			}
		}

		return false;
	});
	$(document).tooltip({
      position: {
        my: "center bottom-5",
        at: "center top"
      }
    });

	$('#add-saved-section').on('click', function(){
		jQuery("#md-addsavedsection").simplemodal();
		jQuery("#md-addsavedsection").data("simplemodal").show();
		$('#is-wrapper').data("contentbox").addSavedSection();
	})


	$('.change_data_parameter').on('click', function(){
		$('input[data-parameter="'+$(this).data('old')+'"]').attr('data-parameter', $(this).data('new'));
		var existingclasses = activecol.attr('class');
		existingclasses = existingclasses.replace($(this).data('old'),$(this).data('new'));

		$(this).parents('.is-boxes').find('.custom-left-right-css').attr('data-dir', $(this).data('new').replace($(this).parents('.is-boxes').find('.custom-left-right-css').attr('data-parameter'),'').replace(/-/g,''));
		activecol.attr('class', existingclasses);
	})
	$('#col-content-editbox-2 input').on('blur change click keyup', function(){
			if($(this).attr('name')=='colshadow' && $(this).val()=='none'){
				activecol.css('box-shadow', 'none');
			}else{
				if($(this).attr('id')=='col-shadow-hoffset-count'){
					$("#col-shadow-hoffset").val($(this).val());
				}
				if($(this).attr('id')=='col-shadow-voffset-count'){
					$("#col-shadow-voffset").val($(this).val());
				}
				if($(this).attr('id')=='col-shadow-blur-count'){
					$("#col-shadow-blur").val($(this).val());
				}
				if($(this).attr('id')=='col-shadow-distance-count'){
					$("#col-shadow-distance").val($(this).val());
				}
				if($(this).attr('id')=='col-shadow-opacity-count'){
					$("#col-shadow-opacity").val($(this).val());
				}
				boxShadow(activecol);
			}
	})
	$('#col-shadow-color').on('blur change paste keyup', function(){
			boxShadow(activecol);
	})
	$('#col-shadow-color').colorPicker({
			renderCallback: function ($elm, toggled) {
					if (toggled === true) {
					} else if (toggled === false) {
					} else {
						if($elm.text!=''){
							boxShadow(activecol);
						}
					}
			}
	})
	$('#col-border-color').colorPicker({
			renderCallback: function ($elm, toggled) {
					if (toggled === true) {
					} else if (toggled === false) {
					} else {
						if($elm.text!=''){
							activecol.css('border-color', $elm.text);
						}
					}
			}
	})
	$('#col-border-color').on('blur change paste keyup', function(){
			activecol.css('border-color', $(this).val());
	})
	$('#col-bg-color-from').colorPicker({
			renderCallback: function ($elm, toggled) {
					if (toggled === true) {
					} else if (toggled === false) {
					} else {
						activecol.attr('data-bg-color-from', $elm.text);
						setColBgColor(activecol);
					}
			}
	})
	$('#col-bg-color-to').colorPicker({
			renderCallback: function ($elm, toggled) {
					if (toggled === true) {
					} else if (toggled === false) {
					} else {
						activecol.attr('data-bg-color-to', $elm.text);
						setColBgColor(activecol);
					}
			}
	})

$('#col-gradient-angle').on('blur change keyup paste click', function(){
	activecol.attr('data-bg-gradient-angle', $(this).val());
	setColBgColor(activecol);
})
$('#col-bg-color-from').on('blur change paste keyup', function(){
	activecol.attr('data-bg-color-from', $(this).val());
	setColBgColor(activecol);
})
$('#col-bg-color-to').on('blur change paste keyup', function(){
	activecol.attr('data-bg-color-to', $(this).val());
	setColBgColor(activecol);
})
	$('.custom-val').on('click', function(){
		 var inputid = $(this).data('input');
		 var inputval = $('#'+inputid).val();
		 var dataval = $(this).data('value');
		 var inputmax = $(this).data('max');
		 var inputmin = $(this).data('min');
		 if(dataval=='-'){
			 if($('#'+inputid).val()<=inputmin){
				 return false;
			 }else{
			 	$('#'+inputid).val(parseInt($('#'+inputid).val())-1);
			}
		 }else if (dataval=='+') {
			 if($('#'+inputid).val()>=inputmax){
				 return false;
			 }else{
		 		$('#'+inputid).val(parseInt($('#'+inputid).val())+1);
			}
		 }else if (dataval=='') {
		 		$('#'+inputid).val(0);
		 }
		 setColClasses(activecol);
	})
$('#clearbgcolor').on('click', function(){
	activecol.removeAttr('data-bg-color-to');
	activecol.removeAttr('data-bg-color-from');
	activecol.removeAttr('data-bg-gradient-angle');
	jQuery('#col-bg-color-to').val('');
	jQuery('#col-bg-color-to').css('background-color','');
	jQuery('#col-bg-color-from').val('');
	jQuery('#col-bg-color-from').css('background-color','');
	jQuery('#col-gradient-angle').val('0');
	setColBgColor(activecol);
})
	$('#col-content-editbox-3 input[name="colborder"]').click(function(){
		activecol.removeClass('border-style-solid');
		activecol.removeClass('border-style-dashed');
		activecol.removeClass('border-style-dotted');
		activecol.removeClass('border-style-double');
		activecol.removeClass('border-style-groove');
		if($(this).val()!='none'){
			activecol.addClass('border-style-'+$(this).val());
			activecol.css('border-color', 'none');
		}
	})
	$('#col-content-editbox-1 input[name="colvalign"]').click(function(){
		activecol.removeClass('align-self-start');
		activecol.removeClass('align-self-end');
		activecol.removeClass('align-self-center');
		activecol.removeClass('align-self-baseline');
		activecol.removeClass('align-self-stretch');
		if($(this).val()!='none'){
			activecol.addClass($(this).val());
		}
	})
	$('#col-content-editbox-1 input[name="colhalign"]').click(function(){
		activecol.removeClass('ml-auto');
		activecol.removeClass('mr-auto');
		activecol.removeClass('mx-auto');
		if($(this).val()!='none'){
			activecol.addClass($(this).val());
		}
	})

	$('#col-content-editbox-1 input[name="itemshalign"]').click(function(){
		activecol.removeClass('justify-content-start');
		activecol.removeClass('justify-content-center');
		activecol.removeClass('justify-content-end');
		activecol.removeClass('justify-content-around');
		activecol.removeClass('justify-content-between');
		if($(this).val()!='none'){
			activecol.addClass($(this).val());
		}
	})
	$('#col-content-editbox-1 input[name="itemsvalign"]').click(function(){
		activecol.removeClass('align-items-start');
		activecol.removeClass('align-items-end');
		activecol.removeClass('align-items-center');
		activecol.removeClass('align-items-baseline');
		activecol.removeClass('align-items-stretch');
		if($(this).val()!='none'){
			activecol.addClass($(this).val());
		}
	})

	$('#col-content-editbox-1 input[name="itemwrap"]').click(function(){
		activecol.removeClass('flex-nowrap');
		activecol.removeClass('flex-wrap');
		activecol.removeClass('flex-wrap-reverse');
		if($(this).val()!='none'){
			activecol.addClass($(this).val());
		}
	})

	$('#col-content-editbox-5 input[name="colvh[]"]').click(function(){
		activecol.removeClass('d-none');
		activecol.removeClass('d-sm-block');
		activecol.removeClass('d-sm-none');
		activecol.removeClass('d-md-block');
		activecol.removeClass('d-md-none');
		activecol.removeClass('d-lg-block');
		activecol.removeClass('d-lg-none');
		activecol.removeClass('d-xl-block');
		activecol.removeClass('d-xl-none');
		activecol.removeClass('d-block');
		$('#col-content-editbox-5 input[name="colvh[]"]').each(function(){
			if($(this).prop("checked") == true){
				activecol.addClass($(this).val());
			}
		})
	})
	$('#is-wrapper').on('mouseover', '.row-tool,#otherTool,#customTools', function(e) {
		$('img.ui-resizable').resizable( "destroy" );
	});
  $('#otherTool,#customTools').on('mouseover', function(e) {
		$('img.ui-resizable').resizable( "destroy" );
	});

	$('#md-img').find('.md-tabs').append('<span id="tabImgShape" class="">CHANGE SHAPE</span>');
	$('#md-img').find('.md-body').append('<div id="divImgShape" style="box-sizing: border-box; padding: 10px; display: none;"><div style="padding:12px 0 0;width:100%;text-align:center;margin-bottom: 50px;">DIMENSION (WxH): &nbsp;  &nbsp; <input type="text" id="btnImgDimChange_w" class="inptxt" style="display:inline-block;margin-left:10px;width:80px"><input type="text" id="btnImgDimChange_h" class="inptxt" style="display:inline-block;margin-left:10px;width:80px"><br><button id="btnImgDimChange" style="padding: 0 30px;line-height: 35px;height: 35px;margin-left:12px;margin-top:12px;"> CHANGE </button><br></div><div style="padding:12px 0 0;width:100%;text-align:center;max-height: 200px;margin-bottom: 0px;overflow: auto;"><span class=""></span><span class="image-wide-bottom-trapezoid"></span><span class="image-wide-top-trapezoid"></span><span class="image-wide-right-trapezoid"></span><span class="image-wide-left-trapezoid"></span><span class="image-polygon-top-left"></span><span class="image-polygon-top-right"></span><span class="image-polygon-bottom-left"></span><span class="image-polygon-bottom-right"></span><span class="image-vertical-skew-right"></span><span class="image-vertical-skew-left"></span><span class="image-horizontal-skew-right"></span><span class="image-horizontal-skew-left"></span><span class="image-chevron-right"></span><span class="image-chevron-left"></span><span class="image-chevron-up"></span><span class="image-chevron-down"></span><span class="image-vertical-hexagon"></span><span class="image-horizonal-hexagon"></span><span class="image-warp-square"></span><span class="image-diamond"></span><span class="image-star"></span><span class="image-slant-up-right"></span><span class="image-slant-up-left"></span><span class="image-slant-down-right"></span><span class="image-slant-down-left"></span><span class="image-slant-right-top"></span><span class="image-slant-right-bottom"></span><span class="image-slant-left-top"></span><span class="image-slant-left-bottom"></span><span class="image-point-up"></span><span class="image-point-down"></span><span class="image-point-right"></span><span class="image-point-left"></span></div></div>');

	$('#md-img').on('click', '#tabImgShape', function(){
		$('#md-img').find('.md-tabs').find('span').removeClass('active');
		$('#divImgPl').hide();
		$('#divImgLnk').hide();
		$('#divImgShape').fadeIn();
		$(this).addClass('active');
	})
	$('#md-img').on('click', '#tabImgLnk, #tabImgPl', function(){
		$('#tabImgShape').removeClass('active');
		$('#divImgShape').hide();
	})

	$('#md-img').find('#divImgShape').on('click', 'span', function(){
		if($('img.ui-resizable').length<=0){
			alert('Please select image to apply effect');
		}
		$('img.ui-resizable').removeClass('image-polygon-top-left');
		$('img.ui-resizable').removeClass('image-polygon-bottom-left');
		$('img.ui-resizable').removeClass('image-polygon-top-right');
		$('img.ui-resizable').removeClass('image-polygon-bottom-right');
		$('img.ui-resizable').removeClass('image-slant-right-top');
		$('img.ui-resizable').removeClass('image-slant-right-bottom');
		$('img.ui-resizable').removeClass('image-slant-left-top');
		$('img.ui-resizable').removeClass('image-slant-left-bottom');
		$('img.ui-resizable').removeClass('image-slant-up-right');
		$('img.ui-resizable').removeClass('image-slant-up-left');
		$('img.ui-resizable').removeClass('image-slant-down-right');
		$('img.ui-resizable').removeClass('image-slant-down-left');
		$('img.ui-resizable').removeClass('image-star');
		$('img.ui-resizable').removeClass('image-diamond');
		$('img.ui-resizable').removeClass('image-warp-square');
		$('img.ui-resizable').removeClass('image-point-left');
		$('img.ui-resizable').removeClass('image-point-right');
		$('img.ui-resizable').removeClass('image-point-up');
		$('img.ui-resizable').removeClass('image-point-down');
		$('img.ui-resizable').removeClass('image-chevron-up');
		$('img.ui-resizable').removeClass('image-chevron-down');
		$('img.ui-resizable').removeClass('image-chevron-right');
		$('img.ui-resizable').removeClass('image-chevron-left');
		$('img.ui-resizable').removeClass('image-wide-bottom-trapezoid');
		$('img.ui-resizable').removeClass('image-wide-top-trapezoid');
		$('img.ui-resizable').removeClass('image-wide-right-trapezoid');
		$('img.ui-resizable').removeClass('image-wide-left-trapezoid');
		$('img.ui-resizable').removeClass('image-vertical-hexagon');
		$('img.ui-resizable').removeClass('image-horizontal-hexagon');
		$('img.ui-resizable').removeClass('image-vertical-skew-right');
		$('img.ui-resizable').removeClass('image-vertical-skew-left');
		$('img.ui-resizable').removeClass('image-horizontal-skew-right');
		$('img.ui-resizable').removeClass('image-horizontal-skew-left');
		if($(this).attr('class')!=''){
		$('img.ui-resizable').addClass($(this).attr('class'));
		}
	})

$( ".md-draggable" ).draggable({ handle: ".md-modal-handle", containment: 'window',
    scroll: false});



})
$(window).on("load resize scroll",function(e){
    $('.customTools').css('opacity', 0);
		$('.customTools').hide();
		$('.row-tool').hide();
		$('.row-html').attr('title', 'View Source');
		$('.row-copy').attr('title', 'Clone');
		$('.row-handle').attr('title', 'Move');
		$('.row-remove').attr('title', 'Delete');
		$('.is-section-remove').attr('title', 'Remove Section');
		$('.is-section-edit').attr('title', 'Section Edit');
		$('#lnkeditbox').attr('title', 'Box Features');
		$('#divboxtool').attr('title', 'Add Background Image');
		$(document).tooltip({
	      position: {
	        my: "center bottom-5",
	        at: "center top"
	      }
	    });


});




// Box shadow

function boxShadow(activecol){
	var x=$("#col-shadow-hoffset").val(),a=$("#col-shadow-voffset").val(),o=$("#col-shadow-blur").val(),p=$("#col-shadow-distance").val();$("#col-shadow-hoffsetval").val(x),$("#col-shadow-voffsetval").val(a),$("#col-shadow-blurval").val(o),$("#col-shadow-distanceval").val(p);var s=$("#col-shadow-color").val(),b=$("#col-shadow-opacity").val(),z=$("input[name='colshadow']:checked").val();
	b=parseInt(b)/100;var l=hexToRgb(s),v="rgba("+l.r+", "+l.g+", "+l.b+", "+b+")",e="-moz-box-shadow: "+x+"px "+a+"px "+o+"px "+p+"px "+v+";<br>-webkit-box-shadow: "+x+"px "+a+"px "+o+"px "+p+"px "+v+";<br>box-shadow: "+x+"px "+a+"px "+o+"px "+p+"px "+v+";";
	console.log(e);
	activecol.css({"-webkit-box-shadow": z+" "+x+"px "+a+"px "+o+"px "+p+"px "+v, "-moz-box-shadow": z+" "+x+"px "+a+"px "+o+"px "+p+"px "+v, "box-shadow": z+" "+x+"px "+a+"px "+o+"px "+p+"px "+v});
	$('#col-shadow-hoffset-count').val(x);
	$('#col-shadow-voffset-count').val(a);
	$('#col-shadow-blur-count').val(o);
	$('#col-shadow-distance-count').val(p);
	$('#col-shadow-opacity-count').val(b*100);
}
function hexToRgb(x){var a=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(x);return a?{r:parseInt(a[1],16),g:parseInt(a[2],16),b:parseInt(a[3],16)}:null}
function rgb2hex(rgb){
 rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
 return (rgb && rgb.length === 4) ? "#" +
  ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
}
function setColBgColor(activecol){
		var s = activecol.attr('data-bg-color-from');
		var s2 = activecol.attr('data-bg-color-to');
		var deg = activecol.attr('data-bg-gradient-angle');


		if(typeof s !== "undefined" && s !== false && s !== null){
				if(typeof s2 !== "undefined" && s2 !== false && s2 !== null){

					if(s!=s2){
						if(typeof deg !== "undefined" && deg !== false && deg !== null){

						}else{
							deg = 0;
						}

						activecol.css({"background":s, "background":"-moz-linear-gradient("+deg+"deg, "+s+" 0%, "+s2+" 100%)", "background":"-webkit-linear-gradient("+deg+"deg, "+s+" 0%, "+s2+" 100%)", "background":"linear-gradient("+deg+"deg, "+s+" 0%, "+s2+" 100%)", "filter":"progid:DXImageTransform.Microsoft.gradient(startColorstr='"+s+"',endColorstr='"+s2+"',GradientType=1)"});
					}else{
						activecol.css("background", s);
					}
				}else{
					activecol.css("background", s);
				}
		}else{
						activecol.css("background", '');
		}
}
function setColClasses(activecol){
	var classes = activecol.attr('class');
	var classes_arr;
	if(classes!=''){
		classes_arr = classes.split(' ');
		for(var i=0;i<classes_arr.length;i++){
			if(classes_arr[i].match(/col-sm/g)){
				activecol.removeClass(classes_arr[i]);
				if($('#colsize_sm').val()>0){
					activecol.addClass('col-sm-'+$('#colsize_sm').val());
				}
			}else{
				if($('#colsize_sm').val()>0){
					activecol.addClass('col-sm-'+$('#colsize_sm').val());
				}
			}
			if(classes_arr[i].match(/col-md/g)){
				activecol.removeClass(classes_arr[i]);
				if($('#colsize_md').val()>0){
					activecol.addClass('col-md-'+$('#colsize_md').val());
				}
			}else{
				if($('#colsize_md').val()>0){
					activecol.addClass('col-md-'+$('#colsize_md').val());
				}
			}
			if(classes_arr[i].match(/col-xl/g)){
				activecol.removeClass(classes_arr[i]);
				if($('#colsize_xl').val()>0){
					activecol.addClass('col-xs-'+$('#colsize_xl').val());
				}
			}else{
				if($('#colsize_xl').val()>0){
					activecol.addClass('col-xl-'+$('#colsize_xl').val());
				}
			}
			if(classes_arr[i].match(/col-lg/g)){
				activecol.removeClass(classes_arr[i]);
				if($('#colsize_lg').val()>0){
					activecol.addClass('col-lg-'+$('#colsize_lg').val());
				}
			}else{
				if($('#colsize_lg').val()>0){
					activecol.addClass('col-lg-'+$('#colsize_lg').val());
				}
			}
			if(classes_arr[i].match(/col/g)){
				activecol.removeClass('col');
				activecol.removeClass('col-1');
				activecol.removeClass('col-2');
				activecol.removeClass('col-3');
				activecol.removeClass('col-4');
				activecol.removeClass('col-5');
				activecol.removeClass('col-6');
				activecol.removeClass('col-7');
				activecol.removeClass('col-8');
				activecol.removeClass('col-9');
				activecol.removeClass('col-10');
				activecol.removeClass('col-11');
				activecol.removeClass('col-12');
				if($('#colsize_all').val()>0){
					activecol.addClass('col-'+$('#colsize_all').val());
				}else{
					activecol.addClass('col');
				}
			}
		}
		for(var i=0;i<classes_arr.length;i++){
			if(classes_arr[i].match(/offset-sm/g)){
				activecol.removeClass(classes_arr[i]);
				if($('#coloffset_sm').val()>0){
					activecol.addClass('offset-sm-'+$('#coloffset_sm').val());
				}
			}else{
				if($('#coloffset_sm').val()>0){
					activecol.addClass('offset-sm-'+$('#coloffset_sm').val());
				}
			}
			if(classes_arr[i].match(/offset-md/g)){
				activecol.removeClass(classes_arr[i]);
				if($('#coloffset_md').val()>0){
					activecol.addClass('offset-md-'+$('#coloffset_md').val());
				}
			}else{
				if($('#coloffset_md').val()>0){
					activecol.addClass('offset-md-'+$('#coloffset_md').val());
				}
			}
			if(classes_arr[i].match(/offset-xl/g)){
				activecol.removeClass(classes_arr[i]);
				if($('#coloffset_xl').val()>0){
					activecol.addClass('offset-xl-'+$('#coloffset_xl').val());
				}
			}else{
				if($('#coloffset_xl').val()>0){
					activecol.addClass('offset-xl-'+$('#coloffset_xl').val());
				}
			}
			if(classes_arr[i].match(/offset-lg/g)){
				activecol.removeClass(classes_arr[i]);
				if($('#coloffset_lg').val()>0){
					activecol.addClass('offset-lg-'+$('#coloffset_lg').val());
				}
			}else{
				if($('#coloffset_lg').val()>0){
					activecol.addClass('offset-lg-'+$('#coloffset_lg').val());
				}
			}
			if(classes_arr[i].match(/offset/g)){
				activecol.removeClass('offset-1');
				activecol.removeClass('offset-2');
				activecol.removeClass('offset-3');
				activecol.removeClass('offset-4');
				activecol.removeClass('offset-5');
				activecol.removeClass('offset-6');
				activecol.removeClass('offset-7');
				activecol.removeClass('offset-8');
				activecol.removeClass('offset-9');
				activecol.removeClass('offset-10');
				activecol.removeClass('offset-11');
				activecol.removeClass('offset-12');
				if($('#coloffset_all').val()>0){
					activecol.addClass('offset-'+$('#coloffset_all').val());
				}
			}else{
				if($('#coloffset_all').val()>0){
					activecol.addClass('offset-'+$('#coloffset_all').val());
				}
			}
		}
	}
}
